import MaterialTabs from "@material-ui/core/Tabs";
import MaterialTab from "@material-ui/core/Tab";
import styled from "styled-components";

export const Tabs = styled(MaterialTabs)`
  .MuiTabs-indicator {
    background-color: var(--dkv-link-primary-color);
  }
`;

export const Tab = styled(MaterialTab)`
  font-family: inherit !important;
  font-size: 1.1428571428571428rem !important;
  line-height: 1.5 !important;
  text-transform: initial !important;
  width: auto !important;
  min-width: unset !important;
  color: var(--dkv-nav-primary-color) !important;
  opacity: 1 !important;

  &.Mui-selected {
    color: var(--dkv-link-primary-color) !important;
  }

  &.Mui-disabled {
    opacity: 0.3 !important;
  }
`;
