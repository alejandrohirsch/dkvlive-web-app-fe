import React from "react";
import styled from "styled-components";

const Container = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  background-color: rgba(255, 255, 255, 0.8);
  z-index: 10;
  border-radius: var(--dkv-ui-loading-border-radius, none);
  display: flex;
  justify-content: center;
  align-items: center;

  div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: var(--dkv-ui-loading-size, 64px);
    height: var(--dkv-ui-loading-size, 64px);
    margin: auto;
    border: var(--dkv-ui-loading-border, 4px) solid var(--dkv-ui-loading-color, #f18400);
    border-radius: 50%;
    animation: ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: var(--dkv-ui-loading-color, #f18400) transparent transparent transparent;
  }

  div:nth-child(1) {
    animation-delay: -0.45s;
  }

  div:nth-child(2) {
    animation-delay: -0.3s;
  }

  div:nth-child(3) {
    animation-delay: -0.15s;
  }

  @keyframes ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

interface LoadingProps {
  inprogress?: boolean;
  ariaProps?: boolean;
  ariaLabel?: string;
  zIndex?: number;
}

export const Loading = ({inprogress, ariaProps, ariaLabel, zIndex}: LoadingProps) => {
  if (!inprogress) {
    return null;
  }

  const aria = ariaProps
    ? {
        role: "alert",
        "aria-busy": true,
        "aria-live": "assertive" as "assertive" | "off" | "polite" | undefined,
        "aria-label": ariaLabel ? ariaLabel : "loading",
      }
    : {};

  const style = zIndex ? {zIndex: zIndex} : undefined;

  return (
    <Container {...aria} style={style}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </Container>
  );
};

export default Loading;
