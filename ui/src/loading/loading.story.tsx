import React from "react";

import {storiesOf} from "@storybook/react";

import Loading from "./index";

const stories = storiesOf("Loading", module);

stories.add("inprogress", () => (
  <div style={{width: 200, height: 200, position: "relative"}}>
    <Loading inprogress />
  </div>
));
