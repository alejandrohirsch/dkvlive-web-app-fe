import React from "react";

import {storiesOf} from "@storybook/react";

import Button from "./index";

const stories = storiesOf("Button", module);

stories.add("default", () => (
  <>
    <Button>default</Button>
    <p />
    <Button disabled>default</Button>
  </>
));

stories.add("secondary", () => (
  <>
    <Button secondary>secondary</Button>
    <p />
    <Button secondary disabled>
      secondary
    </Button>
  </>
));

stories.add("compressed", () => (
  <>
    <Button compressed>compressed</Button>
    <p />
    <Button compressed disabled>
      compressed
    </Button>
    <p />
    <Button compressed secondary>
      compressed secondary
    </Button>
    <p />
    <Button compressed secondary disabled>
      compressed secondary
    </Button>
  </>
));

stories.add("special", () => (
  <>
    <Button special>special</Button>
    <p />
    <Button special disabled>
      special
    </Button>
  </>
));
