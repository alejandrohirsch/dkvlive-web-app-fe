import React from "react";
import styled, {css} from "styled-components";

// -----------------------------------------------------------------------------------------------------------
interface ButtonFieldProps {
  secondary?: boolean;
  special?: boolean;
  compressed?: boolean;
  disabled?: boolean;
  autoWidth?: boolean;
}

const ButtonField = styled.button<ButtonFieldProps>`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  user-select: none;
  border: 0;
  outline: 0;
  cursor: pointer;
  overflow: hidden;
  font-size: 1.1em;
  width: 100%;
  transition: background-color linear 0.1s, color linear 0.1s, border-color linear 0.1s;
  line-height: 1.5;
  font-size: 1.14286em;
  font-weight: 500;
  font-family: inherit;
  border-radius: 20px;
  padding: 7px 24px;
  background-color: var(--dkv-ui-button-background-color, #f18400);
  color: var(--dkv-ui-button-color, #fff);
  border: solid 1px var(--dkv-ui-button-border-color, #f18400);
  flex-shrink: 0;
  min-width: 104px;

  ${(props) =>
    props.autoWidth &&
    css`
      width: auto;
      min-width: unset;
    `}

  &:hover,
  &:focus {
    background-color: var(--dkv-ui-button-hover-background-color, #f39a2e);
    color: var(--dkv-ui-button-hover-color, #fff);
    border-color: var(--dkv-ui-button-hover-border-color, #f39a2e);
  }

  &:active {
    background-color: var(--dkv-ui-button-active-background-color, #c66d00);
    color: var(--dkv-ui-button-active-color, #fff);
    border-color: var(--dkv-ui-button-active-border-color, #c66d00);
  }

  ${(props) =>
    props.compressed &&
    css`
      padding: 3px 24px;
    `}

  ${(props) =>
    props.secondary &&
    css`
      border-color: var(--dkv-ui-button-secondary-border-color, #dbdbdb);
      background-color: var(--dkv-ui-button-secondary-background-color, transparent);
      color: var(--dkv-ui-button-secondary-color, #666666);

      &:hover,
      &:focus {
        border-color: var(--dkv-ui-button-secondary-hover-border-color, #666666);
        background-color: var(--dkv-ui-button-secondary-hover-background-color, transparent);
        color: var(--dkv-ui-button-secondary-hover-color, #666666);
      }

      &:active {
        background-color: var(--dkv-ui-button-secondary-active-background-color, #666666);
        color: var(--dkv-ui-button-secondary-active-color, #fff);
        border-color: var(--dkv-ui-button-secondary-active-border-color, #666666);
      }
    `}

    ${(props) =>
      props.special &&
      css`
        background-color: var(--dkv-ui-button-background-color, transparent);
        color: var(--dkv-ui-button-special-color, #f18400);
        border: solid 1px var(--dkv-ui-button-special-border-color, #f18400);
      `}

  ${(props) =>
    props.disabled &&
    css`
      border-color: var(--dkv-ui-button-disabled-border-color, #dbdbdb);
      background-color: var(--dkv-ui-button-disabled-background-color, #fff);
      color: var(--dkv-ui-button-disabled-color, #c1c1c1);

      &:hover {
        border-color: var(--dkv-ui-button-disabled-border-color, #dbdbdb);
        background-color: var(--dkv-ui-button-disabled-background-color, #fff);
        color: var(--dkv-ui-button-disabled-color, #c1c1c1);
      }
    `}
`;

// -----------------------------------------------------------------------------------------------------------
interface ButtonProps extends React.InputHTMLAttributes<HTMLButtonElement> {
  ariaLabel?: string;
  type?: "button" | "submit" | "reset" | undefined;

  autoWidth?: boolean;
  secondary?: boolean;
  special?: boolean;
  compressed?: boolean;
}

export const Button = ({
  children,
  ariaLabel,
  secondary,
  special,
  compressed,
  ...buttonProps
}: ButtonProps) => {
  return (
    <ButtonField
      secondary={secondary}
      special={special}
      compressed={compressed}
      {...buttonProps}
      aria-label={ariaLabel}
    >
      {children}
    </ButtonField>
  );
};

export default Button;
