import MaterialTextField from "@material-ui/core/TextField";
import styled from "styled-components";

export const TextField = styled(MaterialTextField)`
  font-family: inherit !important;
  color: var(--dkv-ui-input-color, #666666) !important;
  width: 100%;

  * {
    font-family: inherit !important;
  }

  [class^="icon-"],
  [class*=" icon-"] {
    font-family: "DKV" !important;
  }

  .MuiInputBase-root {
    color: var(--dkv-ui-input-color, #666666);
  }

  .MuiInputLabel-root {
    font-size: 1rem;
    color: var(--dkv-ui-input-color, #666666);
  }

  .MuiFormLabel-root.Mui-focused {
    color: var(--dkv-ui-input-color, #666666);
  }

  .MuiInputLabel-shrink {
    transform: none;
    color: var(--dkv-ui-input-active-label-color, #c1c1c1);
  }

  .MuiInputBase-input {
    font-size: 1.1428571428571428rem;
    padding: 8px 0 10px;
  }

  .MuiInput-underline:before {
    border-color: var(--dkv-ui-input-border-color, #efefef);
    border-width: 2px;
  }

  .MuiInput-underline:hover:not(.Mui-disabled):before {
    border-color: var(--dkv-ui-input-hover-border-color, #666666);
  }

  .MuiInput-underline:after {
    border-color: var(--dkv-ui-input-hover-border-color, #666666);
  }

  .MuiOutlinedInput-input {
    font-size: 1.1428571428571428rem;
    padding: 8px 16px;
  }

  .MuiOutlinedInput-root {
    height: 48px;
  }

  .MuiInputLabel-outlined {
    top: -3px;
  }

  .MuiInputLabel-outlined.MuiInputLabel-shrink {
    transform: translate(15px, -5px);
  }

  .MuiOutlinedInput-notchedOutline {
    span {
      font-size: 1rem;
    }
  }

  .MuiOutlinedInput-notchedOutline {
    border-color: var(--dkv-ui-input-border-color, #efefef);
    border-width: 2px;
  }

  .MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline,
  .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline {
    border-color: var(--dkv-ui-input-hover-border-color, #666666);
  }

  .MuiFormHelperText-contained {
    position: absolute;
    bottom: 0px;
    left: 0;
  }
`;
