const numberFormatters = {};

export const formatNumber = (locale: string, number: number, fractionDigits = 0): number => {
  let nf = numberFormatters[`${locale}${fractionDigits}`];
  if (!nf) {
    nf = Intl.NumberFormat(locale, {minimumFractionDigits: fractionDigits});
    numberFormatters[`${locale}${fractionDigits}`] = nf;
  }

  return nf.format(number.toFixed(fractionDigits));
};
