export const debounce = (func: (...args: any[]) => void, delay: number) => {
  let timeout: number | null;

  return function(...args: any[]) {
    if (timeout) {
      clearTimeout(timeout);
    }

    timeout = setTimeout(() => {
      timeout = null;
      func(...args);
    }, delay);
  };
};
