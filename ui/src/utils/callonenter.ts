import {KeyboardEventHandler} from "react";

export function callOnEnter(fn: (e: React.KeyboardEvent<Element>) => void): KeyboardEventHandler {
  return (e) => {
    const code = e.which || e.keyCode;
    if (code !== 13) {
      return;
    }

    e.stopPropagation();
    e.preventDefault();

    fn(e);
  };
}
