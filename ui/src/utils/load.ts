/**
 * loadScript loads an script and returns a promise.
 */
export function loadScript(src: string, async?: boolean) {
  return new Promise((resolve, reject) => {
    const s = document.createElement("script");
    s.src = src;
    s.type = "text/javascript";
    if (async) {
      s.async = true;
    }

    s.onload = () => resolve();
    s.onerror = (err) => reject(err);
    s.addEventListener("load", resolve);
    s.addEventListener("error", reject);

    document.head.appendChild(s);
  });
}

/**
 * loadScripts loads multiple scripts and returns a promise.
 *
 * For Example:
 * ```
 * loadScripts(["script1.js", "script2.js"]).then(() => {})
 * ```
 */
export function loadScripts(scripts: string[]) {
  return Promise.all(scripts.map((src) => loadScript(src)));
}

// loadCSS adds an link tag to the header.
export function loadCSS(href: string) {
  const link = document.createElement("link");
  link.setAttribute("rel", "stylesheet");
  link.setAttribute("type", "text/css");
  link.setAttribute("href", href);

  document.head.append(link);
}
