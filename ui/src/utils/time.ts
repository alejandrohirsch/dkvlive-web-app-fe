export function formatSeconds(s: number) {
  if (s < 0) {
    return `00:00:00`;
  }

  let hours: any = Math.floor(s / 3600);
  if (hours < 10) {
    hours = `0${hours}`;
  }

  let minutes: any = Math.floor((s / 60) % 60);
  if (minutes < 10) {
    minutes = `0${minutes}`;
  }

  let seconds: any = s % 60;
  if (seconds < 10) {
    seconds = `0${seconds}`;
  }

  return `${hours}:${minutes}:${seconds}`;
}
