import React, {useCallback, useRef, useState} from "react";

import {storiesOf} from "@storybook/react";

import Input from "./index";

const stories = storiesOf("Input", module);

stories.add("simple", () => {
  return <Input />;
});

stories.add("controlled", () => {
  const [value, setValue] = useState("");
  const onChangeValue = useCallback((e) => setValue(e.target.value), []);

  return <Input value={value} onChange={onChangeValue} />;
});

stories.add("auto validate", () => {
  return <Input autoValidate={true} required={true} />;
});

stories.add("disabled", () => {
  return <Input disabled={true} />;
});

stories.add("invalid with error", () => {
  return <Input value="invalid@@invalid" invalid={true} errorMessage="E-Mail invalid" />;
});

stories.add("input element access", () => {
  const inputElement = useRef<HTMLInputElement | null>(null);
  const inputElementChange = useCallback((el) => {
    inputElement.current = el;
  }, []);

  return <Input inputElement={inputElementChange} />;
});
