import React, {useCallback, useEffect, useRef, useState} from "react";
import styled, {css} from "styled-components";

// -----------------------------------------------------------------------------------------------------------
interface InputContainerProps {
  disabled?: boolean;
}

export const InputContainer = styled.div<InputContainerProps>`
  display: flex;
  flex-direction: column;

  ${(props) =>
    props.disabled &&
    css`
      opacity: 0.5;
    `}
`;

// -----------------------------------------------------------------------------------------------------------
const Error = styled.p`
  color: var(--dkv-ui-input-error-color, #e44c4d);
  margin: 2px 0 0 8px;
  font-size: 0.9em;
`;

// -----------------------------------------------------------------------------------------------------------
interface InputFieldProps {
  focused?: boolean;
  invalid?: boolean;
  filled?: boolean;
}

const InputField = styled.input<InputFieldProps>`
  width: 100%;
  border: none;
  outline: none;
  -webkit-appearance: none;
  color: var(--dkv-ui-input-color, #666666);
  line-height: 1.5;
  font-size: 1.14286em;
  font-family: inherit; 
  border-radius: 2px;
  font-weight: 500;
  padding: 7px 16px;
  border: solid 1px var(--dkv-ui-input-border-color, #dbdbdb);
  background-color: var(--dkv-ui-input-background-color, transparent);
  transition: background-color linear 0.1s, border-color linear 0.1s;

  &:hover {
    border-color:var(--dkv-ui-input-hover-border-color, #666666);
  }

  /* ${(props) =>
    props.filled &&
    css`
      color: var(--dkv-ui-input-filled-color, #353535);
      border: 1px solid var(--dkv-ui-input-filled-color, #353535);
    `} */

  ${(props) =>
    props.focused &&
    css`
      border-color: var(--dkv-ui-input-focused-border-color, #666666);
    `}

  ${(props) =>
    props.invalid &&
    css`
      border-color: var(--dkv-ui-input-error-color, #e44c4d);
    `}

  ::placeholder {
    color: var(--dkv-ui-input-placeholder-color, #c1c1c1);
    opacity: 1;
  }

  :-ms-input-placeholder {
    color: var(--dkv-ui-input-placeholder-color, #c1c1c1);
    opacity: 1;
  }

  ::-ms-input-placeholder {
    color: var(--dkv-ui-input-placeholder-color, #c1c1c1);
    opacity: 1;
  }
`;

export const InputLabel = styled.label<InputFieldProps>`
  margin-bottom: 4px;
  font-size: 1em;
  line-height: 1.5;
  color: var(--dkv-ui-input-label-color, #666666);

  ${(props) =>
    props.invalid &&
    css`
      color: var(--dkv-ui-input-error-color, #e44c4d);
    `}
`;

// -----------------------------------------------------------------------------------------------------------
interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  autoValidate?: boolean;
  invalid?: boolean;
  errorMessage?: string;
  inputElement?: (el: HTMLInputElement | null) => void;
  label?: string;
}

// @Deprecated, use TextField
export const Input = ({
  onBlur,
  onFocus,
  onKeyUp,
  autoValidate,
  disabled,
  invalid,
  errorMessage,
  inputElement,
  className,
  placeholder,
  required,
  value,
  label,
  ...inputProps
}: InputProps) => {
  // focus
  const [focused, setFocused] = useState(false);

  const handleBlur = useCallback(
    (e) => {
      setFocused(false);

      if (onBlur) {
        onBlur(e);
      }
    },
    [onBlur]
  );

  const handleFocus = useCallback(
    (e) => {
      setFocused(true);

      if (onFocus) {
        onFocus(e);
      }
    },
    [onFocus]
  );

  // validate
  const inputRef = useRef<HTMLInputElement>(null);
  const [inputInvalid, setInputInvalid] = useState(false);

  const handleKeyUp = useCallback(
    (e) => {
      if (inputRef.current !== null) {
        setInputInvalid(!inputRef.current.checkValidity());
      }

      if (onKeyUp) {
        onKeyUp(e);
      }
    },
    [onKeyUp]
  );

  // set inputElement
  useEffect(() => {
    if (inputElement) {
      inputElement(inputRef.current);
    }
  }, [inputElement]);

  // render
  return (
    <InputContainer disabled={disabled} className={className}>
      {label && <InputLabel invalid={invalid || inputInvalid}>{label}</InputLabel>}

      <InputField
        focused={focused}
        invalid={invalid || inputInvalid}
        // filled={value ? true : false}
        ref={inputRef}
        disabled={disabled}
        onBlur={handleBlur}
        onFocus={handleFocus}
        onKeyUp={autoValidate ? handleKeyUp : onKeyUp}
        placeholder={placeholder}
        required={required}
        aria-required={required}
        value={value}
        {...inputProps}
      />
      {errorMessage && <Error>{errorMessage}</Error>}
    </InputContainer>
  );
};

// @Deprecated, use TextField
export default Input;
