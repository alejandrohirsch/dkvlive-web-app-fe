import React, {useState} from "react";
import {Checkbox} from "../checkbox";
import {TextField} from "../textfield";
import {callOnEnter} from "../utils";
import Autocomplete from "@material-ui/lab/Autocomplete";
import styled from "styled-components";
import {useTranslation} from "react-i18next";

// -----------------------------------------------------------------------------------------------------------
export const AutocompleteInput: any = styled(Autocomplete)`
  .MuiOutlinedInput-root {
    height: auto !important;
    min-height: 48px !important;
  }

  .MuiAutocomplete-inputRoot[class*="MuiOutlinedInput-root"] {
    padding-left: 7px !important;
    padding-bottom: 3px !important;
  }

  .MuiAutocomplete-tag {
    border-radius: 2px !important;
    border: solid 1px #98bace !important;
    background-color: #e9f3f9 !important;
    color: #323232;
    font-size: 1rem;
  }

  .MuiChip-deleteIcon {
    width: 16px !important;
    height: 16px !important;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface TagInputProps {
  value: string[];
  onChange: (tags: string[]) => void;
  preventDefaultOnAdd?: boolean;
  required?: boolean;
  options: string[];
  label: string;
  className?: any;
  placeholder?: string;
}

const TagInput: React.FC<TagInputProps> = ({
  options,
  placeholder,
  label,
  value,
  onChange,
  className,
  preventDefaultOnAdd,
  required,
}) => {
  const [inputValue, setInputValue] = useState("");
  const {t} = useTranslation();

  const addTag = (e: any) => {
    if (preventDefaultOnAdd) {
      e.preventDefault();
    }
    if (e.target.value) {
      onChange([...value, e.target.value]);
      setInputValue("");
    }
  };

  // render
  return (
    <AutocompleteInput
      value={value}
      onChange={(_: any, newValue: string[]) => onChange(newValue)}
      multiple
      options={options}
      className={className}
      disableCloseOnSelect
      getOptionLabel={(option: any) => option}
      renderOption={(option: any, {selected}: {selected: boolean}) => (
        <>
          <Checkbox style={{marginRight: 8}} checked={selected} />
          {option}
        </>
      )}
      renderInput={(params: any) => (
        <TextField
          {...params}
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
          variant="outlined"
          label={label}
          placeholder={placeholder}
          onKeyDown={callOnEnter(addTag)}
          required={required && !value.length}
        />
      )}
      clearText={t("Leeren")}
      closeText={t("Schließen")}
      loadingText={t("Lädt...")}
      noOptionsText={t("Keine Optionen")}
      openText={t("Öffnen")}
    />
  );
};

export default TagInput;
