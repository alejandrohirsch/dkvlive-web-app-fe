import React, {useCallback} from "react";
import Select, {Props} from "react-select";
import {InputContainer, InputLabel} from "../input";

// -----------------------------------------------------------------------------------------------------------
const selectStyles = {
  control: (provided: any, state: any) => ({
    ...provided,
    width: "100%",
    outline: "none",
    color: "var(--dkv-ui-input-color, #666666)",
    lineHeight: "1.5",
    fontSize: "1.14286em",
    fontFamily: "inherit",
    borderRadius: state.menuIsOpen ? "2px 2px 0 0" : "2px",
    fontWeight: "500",
    padding: "0 0 0 16px",
    border: state.menuIsOpen
      ? "solid 1px var(--dkv-ui-input-hover-border-color, #666666)"
      : "solid 1px var(--dkv-ui-input-border-color, #dbdbdb)",
    backgroundColor: "var(--dkv-ui-input-background-color, transparent)",
    transition: "background-color linear 0.1s, border-color linear 0.1s",
    height: "40px",
    boxShadow: "none",

    "&:hover": {
      borderColor: "var(--dkv-ui-input-hover-border-color, #666666)",
      borderRadius: "2px 2px 0 0",

      ".dkv-select__dropdown-indicator": {
        color: "var(--dkv-ui-input-hover-border-color, #666666)",
      },
    },
  }),
  valueContainer: (provided: any) => ({
    ...provided,
    padding: "0",
  }),
  singleValue: (provided: any) => ({
    ...provided,
    color: "inherit",
    marginLeft: 0,
  }),
  indicatorSeparator: () => ({
    display: "none",
  }),
  menuList: (provided: any) => ({
    ...provided,
    paddingTop: "0",
    paddingBottom: "0",
    marginBottom: "0",
  }),
  menu: (provided: any) => ({
    ...provided,
    marginTop: 0,
    boxShadow: "none",
    fontSize: "16px",
    color: "var(--dkv-ui-input-color, #666666)",
    borderRight: "1px solid",
    borderBottom: "1px solid",
    borderLeft: "1px solid",
    borderColor: "var(--dkv-ui-input-hover-border-color, #666666)",
    borderRadius: "0 0 2px 2px",
  }),
  option: (provided: any, state: any) => ({
    ...provided,
    padding: "11px 16px",
    transition: "background-color linear 0.1s, color linear 0.1s",
    color:
      state.isFocused || state.isSelected
        ? "var(--dkv-ui-autocomplete-item-selected-color, #fff)"
        : "inherit",
    backgroundColor:
      state.isFocused || state.isSelected
        ? "var(--dkv-ui-autocomplete-item-focused-background-color, #2e6b90)"
        : "transparant",
  }),
  input: (provided: any) => ({
    ...provided,
    margin: "0",
    color: "inherit",
    input: {
      font: "inherit",
    },
  }),
};

// -----------------------------------------------------------------------------------------------------------
interface OptionType {
  label: string;
  value: string;
}

interface AutoCompleteProps extends Omit<Props, "onChange"> {
  label?: string;
  disabled?: boolean;
  className?: string;
  onChange: (value: OptionType) => void;
}

// @Deprecated
export const AutoComplete: React.FC<AutoCompleteProps> = ({
  label,
  disabled,
  className,
  onChange,
  ...rest
}) => {
  const onValueChange = useCallback(
    (value: any) => {
      onChange(value);
    },
    [onChange]
  );

  return (
    <InputContainer disabled={disabled} className={className}>
      {label && <InputLabel>{label}</InputLabel>}
      <Select
        styles={selectStyles}
        classNamePrefix="dkv-select"
        menuPosition="fixed"
        onChange={onValueChange}
        {...rest}
      />
    </InputContainer>
  );
};

// @Deprecated
export default AutoComplete as any;
