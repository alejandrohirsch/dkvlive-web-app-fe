import React from "react";
import MaterialSelect, {SelectProps} from "@material-ui/core/Select";
import styled from "styled-components";
import {FormControl, InputLabel} from "@material-ui/core";

// -----------------------------------------------------------------------------------------------------------
const StyledSelect = styled(MaterialSelect)`
  font-family: inherit !important;
  color: var(--dkv-ui-input-color, #666666) !important;
  width: 100%;
  height: 48px;

  * {
    font-family: inherit !important;
  }

  [class^="icon-"],
  [class*=" icon-"] {
    font-family: "DKV" !important;
  }

  .MuiInputBase-root {
    color: var(--dkv-ui-input-color, #666666);
  }

  .MuiInputBase-input {
    font-size: 1.1428571428571428rem;
    padding: 8px 16px;
  }

  .MuiOutlinedInput-notchedOutline {
    span {
      font-size: 1rem;
    }
  }

  .MuiOutlinedInput-notchedOutline {
    border-color: var(--dkv-ui-input-border-color, #efefef);
    border-width: 2px;
  }

  .MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline,
  .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline {
    border-color: var(--dkv-ui-input-hover-border-color, #666666) !important;
  }
`;

const StyledFormControl = styled(FormControl)`
  font-family: inherit !important;
  color: var(--dkv-ui-input-color, #666666) !important;

  * {
    font-family: inherit !important;
  }

  [class^="icon-"],
  [class*=" icon-"] {
    font-family: "DKV" !important;
  }
`;

const StyledInputLabel = styled(InputLabel)`
  font-family: inherit !important;
  font-size: 1rem;
  transform: translate(14px, -6px) !important;
  color: var(--dkv-ui-input-active-label-color, #c1c1c1) !important;

  * {
    font-family: inherit !important;
  }

  [class^="icon-"],
  [class*=" icon-"] {
    font-family: "DKV" !important;
  }

  &.Mui-focused {
    color: var(--dkv-ui-input-hover-border-color, #666666) !important;
  }
`;

// -----------------------------------------------------------------------------------------------------------
export const Select = (props: SelectProps) => {
  return (
    <StyledFormControl variant="outlined" style={props.style} className={props.className}>
      <StyledInputLabel id={`dkv-select-${props.name}`}>{props.label}</StyledInputLabel>
      <StyledSelect
        labelId={`dkv-select-${props.name}`}
        id={`dkv-select-${props.name}`}
        name={props.name}
        value={props.value}
        onChange={props.onChange}
        label={props.label}
        multiple={props.multiple}
        renderValue={props.renderValue}
        required={props.required}
        disabled={props.disabled}
      >
        {props.children}
      </StyledSelect>
    </StyledFormControl>
  );
};

export default Select;
