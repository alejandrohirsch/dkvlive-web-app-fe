import React, {CSSProperties} from "react";
import styled, {css} from "styled-components";

// -----------------------------------------------------------------------------------------------------------
interface ContainerProps {
  error?: boolean;
  warning?: boolean;
  success?: boolean;
}

const Container = styled.div<ContainerProps>`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 15px;
  z-index: 10;
  background-color: var(--dkv-ui-message-background-color, #e9f3f9);
  color: var(--dkv-ui-message-color, #323232);
  font-size: 1.14286em;
  line-height: 1.5;
  border: 1px solid var(--dkv-ui-message-border-color, #98bace);
  border-left: 4px solid var(--dkv-ui-message-border-highlight-color, #7ba2b8);

  span {
    margin-left: 16px;
  }

  span:first-of-type {
    color: var(--dkv-ui-message-border-highlight-color, #7ba2b8);
    margin-left: 0;
  }

  ${(props) => {
    if (props.error) {
      return css`
        border-color: var(--dkv-ui-message-border-color-error, #f09fa0);
        border-left-color: var(--dkv-ui-message-border-highlight-color-error, #e44c4d);
        background-color: var(--dkv-ui-message-background-color-error, #fdf3f3);

        color: var(--dkv-ui-message-color-error, #323232);

        span:first-of-type {
          color: var(--dkv-ui-message-border-highlight-color-error, #e44c4d);
        }
      `;
    }

    if (props.warning) {
      return css`
        border-color: var(--dkv-ui-message-border-color-warning, #fed585);
        border-left-color: var(--dkv-ui-message-border-highlight-color-warning, #ffa700);
        background-color: var(--dkv-ui-message-background-color-warning, #fef7e6);

        color: var(--dkv-ui-message-color-warning, #323232);

        span:first-of-type {
          color: var(--dkv-ui-message-border-highlight-color-warning, #ffa700);
        }
      `;
    }

    if (props.success) {
      return css`
        border-color: var(--dkv-ui-message-border-color-success, #a6dea8);
        border-left-color: var(--dkv-ui-message-border-highlight-color-success, #58be58);
        background-color: var(--dkv-ui-message-background-color-success, #f5fff5);

        color: var(--dkv-ui-message-color-success, #323232);

        span:first-of-type {
          color: var(--dkv-ui-message-border-highlight-color-success, #58be58);
        }
      `;
    }

    return null;
  }}
`;

// -----------------------------------------------------------------------------------------------------------
interface MessageProps {
  text: string;
  className?: string;
  error?: boolean;
  warning?: boolean;
  success?: boolean;
  style?: CSSProperties;
}

export const Message = ({text, error, warning, success, className, style}: MessageProps) => {
  return (
    <Container className={className} error={error} warning={warning} success={success} style={style}>
      {error && <span className="icon-ico_warning" style={{fontSize: 24}} />}
      {warning && <span className="icon-ico_alert" style={{fontSize: 24}} />}
      {success && <span className="icon-ico_check" style={{fontSize: 24}} />}
      {!error && !warning && !success && <span className="icon-ico_info" style={{fontSize: 24}} />}

      <span>{text}</span>
    </Container>
  );
};

export default Message;
