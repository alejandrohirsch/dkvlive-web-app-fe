import React from "react";

import {storiesOf} from "@storybook/react";

import Message from "./index";

const stories = storiesOf("Message", module);

stories.add("standard", () => (
  <div style={{width: 200, height: 200}}>
    <Message text="Hello World!" />
  </div>
));

stories.add("error", () => (
  <div style={{width: 200, height: 200}}>
    <Message text="Hello World!" error />
  </div>
));

stories.add("warning", () => (
  <div style={{width: 200, height: 200}}>
    <Message text="Hello World!" warning />
  </div>
));

stories.add("success", () => (
  <div style={{width: 200, height: 200}}>
    <Message text="Hello World!" success />
  </div>
));
