import React from "react";
import styled from "styled-components";

// -----------------------------------------------------------------------------------------------------------
const StyledTagList = styled.ul`
  display: flex;
  flex-flow: row;

  li {
    border-radius: 4px;
    color: #ffffff;
    padding: 2px 5px;
    margin-left: 4px;
    font-size: 12px;
    white-space: nowrap;
  }
`;

// -----------------------------------------------------------------------------------------------------------
const generateTagColor = (name: string) => {
  const colorPalette = [
    "#b3e7e8",
    "#b6a5e0",
    "#a8c2e6",
    "#ffa797",
    "#dcd6e8",
    "#b5e5a3",
    "#e4d2ca",
    "#ccdbe4",
    "#e3f297",
    "#97cee6",
    "#a5a5d3",
    "#f1cea1",
  ];
  const hash = name.split("").reduce(function(a, b) {
    a = (a << 5) - a + b.charCodeAt(0);
    return a & a;
  }, 0);
  return colorPalette[Math.abs(hash) % colorPalette.length];
};

// -----------------------------------------------------------------------------------------------------------
interface TagListProps {
  tags: {id?: string; name: string}[];
  className?: any;
}

const TagList: React.FC<TagListProps> = ({tags, className}) => {
  // render
  return (
    <div style={{position: "relative"}}>
      <StyledTagList className={className}>
        {tags
          ? tags.map((tag) => (
              <li
                key={tag.id}
                style={{
                  backgroundColor: generateTagColor(tag.name),
                  color: "#363636",
                }}
              >
                {tag.name}
              </li>
            ))
          : null}
      </StyledTagList>
    </div>
  );
};

export default TagList;
