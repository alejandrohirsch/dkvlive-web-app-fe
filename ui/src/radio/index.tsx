import {
  RadioGroup as MaterialRadioGroup,
  Radio as MaterialRadio,
  FormControlLabel as MaterialFormControlLabel,
} from "@material-ui/core";
import styled from "styled-components";

export const Radio = styled(MaterialRadio)`
  .MuiSvgIcon-root {
    width: 24px;
    height: 24px;
    color: #dbdbdb;
  }

  &.Mui-checked {
    .MuiSvgIcon-root {
      color: #004b78;
    }
  }

  &.MuiRadio-colorSecondary.Mui-checked {
    color: #004b78;
  }

  &.MuiRadio-colorSecondary:hover,
  &.MuiRadio-colorSecondary.Mui-checked:hover {
    background-color: rgba(0, 75, 120, 0.04);
  }
`;

export const FormControlLabel = styled(MaterialFormControlLabel)`
  font-family: inherit !important;

  [class^="icon-"],
  [class*=" icon-"] {
    font-family: "DKV" !important;
  }

  .MuiFormControlLabel-label {
    font-size: 1.2857142857142858rem;
    color: #666;
    font-weight: 500;
  }
`;

export const RadioGroup = MaterialRadioGroup;
