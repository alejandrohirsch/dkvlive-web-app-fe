import React from "react";
import {Checkbox as PrimeCheckbox} from "primereact/checkbox";
import styled from "styled-components";

// -----------------------------------------------------------------------------------------------------------
export const Checkbox = styled(PrimeCheckbox)`
  flex-shrink: 0;

  .p-checkbox-box {
    background-color: #ffffff;
    text-align: center !important;
    transition: background-color 0.1s, border-color 0.1s, box-shadow 0.1s;
    width: 24px !important;
    height: 24px !important;
    border-radius: 2px !important;
    border: solid 1px #dbdbdb;

    &.p-highlight {
      border-color: #004b78;
      background-color: #ccdbe4;
      color: #004b78;
    }

    &:not(.p-disabled):hover {
      border-color: #666666;
    }
  }

  .p-checkbox-icon {
    display: block;
    height: 100%;
    font-size: 20px;
  }
`;

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  display: flex;
  flex-flow: row nowrap;

  label {
    font-size: 1.1428571428571428rem;
    color: #666;
    line-height: 1.13;
    font-weight: 500;
    margin-left: 8px;
    cursor: pointer;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface CheckboxProps {
  id?: string;
  value?: any;
  name?: string;
  checked?: boolean;
  style?: object;
  className?: string;
  disabled?: boolean;
  required?: boolean;
  readOnly?: boolean;
  ariaLabelledBy?: string;
  onMouseDown?(event: Event): void;
  onContextMenu?(event: Event): void;
  onChange?(e: {
    originalEvent: Event;
    value: any;
    checked: boolean;
    target: {type: string; name: string; id: string; value: any; checked: boolean};
  }): void;
}

interface CheckboxFieldProps extends CheckboxProps {
  label?: string;
}

export const CheckboxField: React.FC<CheckboxFieldProps> = ({className, label, id, ...checkboxProps}) => {
  return (
    <Container className={className}>
      <div>
        <Checkbox inputId={id} {...checkboxProps} />
      </div>
      <label htmlFor={id}>{label}</label>
    </Container>
  );
};

export default CheckboxField;
