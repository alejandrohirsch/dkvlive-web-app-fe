import React, {useCallback, useEffect, useRef} from "react";
import styled, {css} from "styled-components";

import {callOnEnter} from "../utils/callonenter";

// -----------------------------------------------------------------------------------------------------------
interface ContainerProps {
  input?: boolean;
  alignRight?: boolean;
}

const Container = styled.div<ContainerProps>`
  display: inline-block;
  position: relative;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  ${(props) =>
    props.input &&
    css`
      &:after {
        content: "";
        position: absolute;
        bottom: 0;
        left: 0;
        right: 8px;
        height: 1px;
        background-color: #444;
      }
    `}

  ${(props) =>
    props.alignRight &&
    css`
      text-align: right;

      input {
        text-align: right;
      }

      &:after {
        right: 0;
      }
    `}
`;

const Input = styled.input`
  width: 100%;
  border: none;
  outline: none;
  -webkit-appearance: none;
  font-family: inherit;
  font-size: inherit;
  font: inherit;
  background-color: transparent;
  color: #444;

  ::placeholder {
    color: #b5b5b5;
    opacity: 1;
  }

  :-ms-input-placeholder {
    color: #b5b5b5;
    opacity: 1;
  }

  ::-ms-input-placeholder {
    color: #b5b5b5;
    opacity: 1;
  }
`;

// -----------------------------------------------------------------------------------------------------------
export interface InlineInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  editMode: boolean;
  setEditMode: (name: string, editMode: boolean) => void;
  onInputEl?: (el: HTMLInputElement) => void;
  alignRight?: boolean;
  onChange: (
    eventOrPath: string | React.ChangeEvent<any>,
    eventValue?: any
  ) => void | ((eventOrTextValue: string | React.ChangeEvent<any>) => void);
}

export const InlineInput = ({
  value,
  placeholder,
  editMode,
  setEditMode,
  name,
  onInputEl,
  alignRight,
  ...inputProps
}: InlineInputProps) => {
  // edit mode
  const onDblClick = useCallback(() => {
    setEditMode(name || "_", true);
  }, [name, setEditMode]);

  // auto selection
  const inputRef = useRef(null);
  useEffect(() => {
    if (editMode && inputRef.current !== null) {
      const inputEl = (inputRef.current as unknown) as HTMLInputElement;
      inputEl.setSelectionRange(0, inputEl.value.length);
      inputEl.select();

      if (onInputEl) {
        onInputEl(inputEl);
      }
    }
  }, [editMode, onInputEl]);

  // render
  if (editMode) {
    return (
      <Container input={true} alignRight={alignRight}>
        <Input
          type="text"
          value={value}
          name={name}
          placeholder={placeholder}
          ref={inputRef}
          {...inputProps}
        />
      </Container>
    );
  }

  return (
    <Container
      onDoubleClick={onDblClick}
      role="button"
      tabIndex={0}
      onKeyDown={callOnEnter(onDblClick)}
      title={value ? value.toString() : ""}
      alignRight={alignRight}
    >
      {value}
    </Container>
  );
};

export default InlineInput;
