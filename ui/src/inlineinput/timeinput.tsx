import React, {useCallback, useEffect, useRef, useState} from "react";

import InlineInput, {InlineInputProps} from "./input";

export const TimeInlineInput = (props: InlineInputProps) => {
  // ref
  const elRef = useRef<HTMLInputElement>(null);

  const setInputRef = useCallback((el: HTMLInputElement) => {
    // @ts-ignore
    elRef.current = el;
  }, []);

  // change
  const cursorRef = useRef(0);
  const [changeCount, setChangeCount] = useState(0);
  const [value, setValue] = useState(props.value);

  const onChange = useCallback((e: any) => {
    const time = (e.value || (e.target ? e.target.value : "")).split(":");
    if (!time[1]) {
      time[1] = "00";
    }

    if (time[0].length >= 2) {
      time[0] = time[0].slice(0, 2);
    } else {
      time[0] = time[0] + "0";
    }

    if (time[1].length >= 2) {
      time[1] = time[1].slice(0, 2);
    } else {
      time[1] = time[1] + "0";
    }

    let cursorPos = elRef.current ? elRef.current.selectionEnd : 0;

    if (time[0] > 23) {
      cursorPos = 3;
      time[0] = 23;
    }

    if (time.length > 1) {
      if (time[1] > 59) {
        cursorPos = 5;
        time[1] = 59;
      }
    }

    // @ts-ignore
    cursorRef.current = cursorPos;
    setChangeCount((c) => c + 1);
    setValue(time.splice(0, 2).join(":"));
  }, []);

  useEffect(() => {
    if (!elRef.current) {
      return;
    }

    const pos =
      (value as string).charAt(cursorRef.current) === ":" ? cursorRef.current + 1 : cursorRef.current;
    elRef.current.selectionStart = elRef.current.selectionEnd = pos;
  }, [changeCount, value]);

  const {value: propsValue, onChange: propsOnChange, name: propsName} = props;
  useEffect(() => {
    if (value !== propsValue) {
      propsOnChange(propsName || "", value);
    }
  }, [value, propsValue, propsOnChange, propsName]);

  // render
  return (
    <InlineInput
      {...props}
      value={value}
      onChange={onChange}
      placeholder={"hh:mm"}
      onInputEl={setInputRef}
      alignRight={true}
    />
  );
};

export default TimeInlineInput;
