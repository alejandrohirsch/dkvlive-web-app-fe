const express = require("express");
const path = require("path");
const helmet = require("helmet");
const compression = require("compression");
const morgan = require("morgan");

const indexFile = path.join(__dirname, "build", "index.html");

const excludeFromCache = {
  "index.html": true,
  "robots.txt": true,
  "service-worker.js": true,
  "manifest.json": true,
  "asset-manifest.json": true,
};

const app = express();
app.use(helmet());
app.use(compression());
app.use(
  morgan(
    '{"time":":date[iso]","level":"info","msg":":method :url HTTP/:http-version","remote_addr":":remote-addr","status":":status","body_bytes_sent":":res[content-length]","referer":":referrer","user_agent":":user-agent","client_ip":":req[x-client-ip]","request_time":":response-time"}'
  )
);

app.use(
  express.static(path.join(__dirname, "build"), {
    maxAge: "31d",
    setHeaders: function(res, file) {
      if (excludeFromCache[path.basename(file)]) {
        res.setHeader("Cache-Control", "no-store");
      } else if (file.match(/static\/(js|css|media)\/\S+\.(js|css|map|png|gif|ttf|woff|eot|svg|jpg)/g)) {
        res.setHeader("Cache-Control", "max-age=31536000, immutable");
      }
    },
  })
);

app.get("/health-check", (_, res) => res.sendStatus(200));

app.get("*", function(_, res) {
  res.sendFile(indexFile);
});

app.listen(8080, "0.0.0.0");
