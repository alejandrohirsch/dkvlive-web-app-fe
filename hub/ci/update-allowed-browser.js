const fs = require("fs");
const {exec} = require("child_process");

fs.readFile(`${__dirname}/../public/index.html`, "utf8", (err, data) => {
  if (err) {
    throw err;
  }

  exec("yarn exec --silent -- browserslist-useragent-regexp --allowHigherVersions", (err, stdout) => {
    if (err) {
      throw err;
    }

    fs.writeFile(
      `${__dirname}/../public/index.html`,
      data.replace(
        "</noscript>",
        `</noscript><script>if (!${stdout.trim()}.test(navigator.userAgent)) window.location.href = "/outdatedbrowser.html"</script>`
      ),
      "utf8",
      (err) => {
        if (err) {
          throw err;
        }
      }
    );
  });
});
