#!/bin/bash
set -e

echo "VERSION: $1"

CONTAINER_ID=$(docker load --input ./container-image.docker 2>/dev/null | awk '/Loaded image ID:/{print $NF}')

# default "master" tag
IMAGE_NAME_DEFAULT="$CONTAINER_REGISTRY_NAME/dkvlive/hub:master"
echo "IMAGE DEFAULT: $IMAGE_NAME_DEFAULT"

docker tag $CONTAINER_ID $IMAGE_NAME_DEFAULT
docker push $IMAGE_NAME_DEFAULT

# version tagged
# IMAGE_NAME="$CONTAINER_REGISTRY_NAME/dkvlive/hub:$1"
# echo "IMAGE: $IMAGE_NAME"

# docker tag $IMAGE_NAME_DEFAULT $IMAGE_NAME
# docker push $IMAGE_NAME
