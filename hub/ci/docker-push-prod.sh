#!/bin/bash
set -e

HUBVERSION="${DKV_LIVE_FRONTEND_HUB_VERSION}-prod.${VERSION}.${BITBUCKET_BUILD_NUMBER}"
echo "VERSION: $HUBVERSION"

CONTAINER_ID=$(docker load --input ./container-image.docker 2>/dev/null | awk '/Loaded image ID:/{print $NF}')

IMAGE_NAME="$CONTAINER_REGISTRY_NAME/dkvlive/hub:$HUBVERSION"
LATEST_IMAGE_NAME="$CONTAINER_REGISTRY_NAME/dkvlive/hub:latest"

echo "IMAGE: $IMAGE_NAME"

# version tagged
docker tag $CONTAINER_ID $IMAGE_NAME
docker push $IMAGE_NAME

# latest tag
docker tag $IMAGE_NAME $LATEST_IMAGE_NAME
docker push $LATEST_IMAGE_NAME