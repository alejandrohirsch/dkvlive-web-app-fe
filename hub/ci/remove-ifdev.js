const replace = require("replace-in-file");

const options = {
  files: ["src/**/*.ts*", "src/**/*.scss"],
  from: /{?\/\*\s{0,}#if-dev([\s\S]*?)###\s{0,}\*\/}?/g,
  to: "",
};

replace(options, (error) => {
  if (!error) {
    return 0;
  }

  return console.error("Error occurred:", error);
});
