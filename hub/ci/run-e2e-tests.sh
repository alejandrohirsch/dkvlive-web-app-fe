#!/bin/bash
set -e

CONTAINER_ID=$(docker load --input ./container-image.docker 2>/dev/null | awk '/Loaded image ID:/{print $NF}')
TIMESTAMP=$(date +%s)
CONTAINER_NAME=$(echo "$BITBUCKET_REPO_SLUG-$TIMESTAMP" | awk '{print tolower($0)}')

cd e2e
yarn install --frozen-lockfile

docker run -d --rm --name $CONTAINER_NAME $CONTAINER_ID
CONTAINER_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $CONTAINER_NAME)

BUILD_EMAIL="fp@styletronic.com"

TEST_OK=0
timeout 300 bash ./run_tests.sh "${BITBUCKET_BRANCH}-${BITBUCKET_COMMIT}" http://$CONTAINER_IP:8080 "${BITBUCKET_REPO_FULL_NAME}" "${BRANCH_NAME}" ${BITBUCKET_COMMIT} ${BUILD_EMAIL} localhost || TEST_OK=$?
if [ $TEST_OK -ne 0 ]; then docker logs $CONTAINER_NAME || true; fi

docker stop $CONTAINER_NAME || true

if [ $TEST_OK -ne 0 ]; then exit 1; fi