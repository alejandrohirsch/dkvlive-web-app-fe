#!/bin/bash

HUBVERSION="${DKV_LIVE_FRONTEND_HUB_VERSION}-sta.${VERSION}.${BITBUCKET_BUILD_NUMBER}"

echo "HUB VERSION: $HUBVERSION"

sed -i -e "s/0.0.0/${HUBVERSION}/g" $1/src/config.ts
sed -i -e "s/{{ VERSION }}/${HUBVERSION}/g" $1/public/index.html
sed -i -e "s/frontend.dev.dkv.live/frontend.sta.dkv.live/g" $1/src/config.ts
sed -i -e "s/websocket.dev.dkv.live/websocket.sta.dkv.live/g" $1/src/config.ts
sed -i -e "s/http:\/\/local.dkv.live:3000/https:\/\/sta.dkv.live/g" $1/public/assets/map/truck.yaml
sed -i -e "s/v=0.0.0/v=${HUBVERSION}/g" $1/public/assets/map/truck.yaml

node $1/ci/update-allowed-browser.js
