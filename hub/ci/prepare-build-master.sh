#!/bin/bash

VERSION="$DKV_LIVE_FRONTEND_HUB_VERSION-master.$BITBUCKET_BUILD_NUMBER"
echo "VERSION: $VERSION"

sed -i -e "s/0.0.0/$VERSION/g" $1/src/config.ts
sed -i -e "s/{{ VERSION }}/$VERSION/g" $1/public/index.html
sed -i -e "s/http:\/\/local.dkv.live:3000/https:\/\/dev.dkv.live/g" $1/public/assets/map/truck.yaml
sed -i -e "s/v=0.0.0/v=$VERSION/g" $1/public/assets/map/truck.yaml

node $1/ci/update-allowed-browser.js
