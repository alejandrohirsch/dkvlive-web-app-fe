import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import "dayjs/locale/de";
import {resolveUserDateFormat} from "app/modules/login/state/login/selectors";
dayjs.extend(utc);

export function formatDate(date: string | Date, utc = false) {
  if (!date) {
    return "";
  }

  return dayjs(date, {utc: utc}).format(resolveUserDateFormat());
}

export function formatDateShort(date: string | Date, utc = false) {
  if (!date) {
    return "";
  }

  let format = resolveUserDateFormat(false);
  format = format.replace(".YYYY", "");
  format = format.replace("YYYY", "");

  return dayjs(date, {utc: utc}).format(format);
}

export function formatToHour(date: string | Date, utc = false) {
  if (!date) {
    return "";
  }

  return dayjs(date, {utc: utc}).format("HH:mm");
}

export const formatMinutes = (minutes: number): string => {
  const h = Math.floor(minutes / 60);
  const m = minutes % 60;
  return `${h < 10 ? "0" + h : h}:${m < 10 ? "0" + m : m}`;
};
