import {LoginAction} from "./login/types";

export type LoginActionTypes = LoginAction;
