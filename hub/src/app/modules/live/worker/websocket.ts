interface MessageEvent extends Event {
  data: {
    event: string;
    payload: any;
  };
}

interface ConnectParams {
  wsURL: string;
  sess: string;
}

// eslint-disable-next-line
const ctx: Worker = self as any;

let utf8Decoder: any;

const scope = typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : this;

if (scope["TextDecoder"]) {
  utf8Decoder = new TextDecoder();
} else {
  utf8Decoder = {
    decode: function(buffer: ArrayBuffer) {
      const arr = new Uint8Array(buffer);
      let d = "";

      for (let i = 0; i < arr.length; i += 1) {
        d += String.fromCharCode(arr[i]);
      }

      return d;
    },
  };
}

let connectParams: ConnectParams;
let ws: WebSocket | undefined;

let restarts = false;
let tries = 0;

let assetUpdateTimeout: number | undefined;

// connect
function connect() {
  if (!connectParams) {
    return;
  }

  tries++;

  ws = new WebSocket(`${connectParams.wsURL}?sess=${connectParams.sess}`);

  ws.onopen = function() {
    tries = 0;
    ctx.postMessage(JSON.stringify({event: "CONNECTED"}));

    assetUpdateTimeout = setTimeout(sendAssetData, 1000);
  };

  ws.onclose = () => {
    ctx.postMessage(JSON.stringify({event: "DISCONNECTED"}));

    if (assetUpdateTimeout) {
      clearTimeout(assetUpdateTimeout);
      assetUpdateTimeout = undefined;
    }

    ws = undefined;
    reconnect();
  };

  ws.onerror = () => {
    ctx.postMessage(JSON.stringify({event: "DISCONNECTED"}));

    if (assetUpdateTimeout) {
      clearTimeout(assetUpdateTimeout);
      assetUpdateTimeout = undefined;
    }

    ws = undefined;
    reconnect();
  };

  ws.onmessage = (evt: any) => {
    new Response(evt.data)
      .arrayBuffer()
      .then((buf: ArrayBuffer) => {
        const type = new Uint8Array(buf.slice(0, 1))[0];
        const payload = utf8Decoder.decode(buf.slice(1, buf.byteLength));
        switch (type) {
          case 1: // AssetUpdate
            handleAssetUpdate(payload);
            break;
        }
      })
      .catch((err: Error) => {
        console.error(err);
      });
  };
}

// reconnect
function reconnect() {
  if (!restarts) {
    restarts = true;

    setTimeout(
      () => {
        restarts = false;
        connect();
      },
      tries > 10 ? 60000 : 5000
    );
  }
}

// handleConnect
function handleConnect(payload: {wsURL: string; sess: string}) {
  if (ws) {
    return;
  }

  connectParams = payload;
  connect();
}

// handleEventTypes
function handleEventTypes(payload: string[]) {
  if (!ws) {
    return;
  }

  ws.send("eventtypes;" + payload.join(";"));
}

// handleAssetUpdate
let assetBuffer = {};
function handleAssetUpdate(data: string) {
  const assetID = data.slice(0, 24);
  assetBuffer[assetID] = data.slice(24);
}

function sendAssetData() {
  if (Object.keys(assetBuffer).length > 0) {
    ctx.postMessage(JSON.stringify({event: "ASSETUPDATE", data: Object.values(assetBuffer)}));
    assetBuffer = {};
  }

  assetUpdateTimeout = setTimeout(sendAssetData, 1000);
}

// onmessage
ctx.onmessage = function(e: MessageEvent) {
  switch (e.data.event) {
    case "connect":
      handleConnect(e.data.payload);
      break;
    case "eventtypes":
      handleEventTypes(e.data.payload);
      break;
  }
};

export {};
