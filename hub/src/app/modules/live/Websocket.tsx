import React, {useEffect, useState} from "react";
import {Dispatch} from "redux";
import {assetsUpdateAction} from "./state/assets/actions";
import {useDispatch, useSelector} from "react-redux";
import config from "config";
// eslint-disable-next-line
import WebsocketWorker from "worker-loader!./worker/websocket";
import {eventTypesSelector} from "./state/websocket/selectors";

// -----------------------------------------------------------------------------------------------------------
const createHandleWebsocketMessage = (dispatch: Dispatch, setConnected: (connected: boolean) => void) => (m: {
  data: string;
}) => {
  const message = JSON.parse(m.data);

  switch (message.event) {
    case "CONNECTED":
      setConnected(true);
      break;
    case "DISCONNECTED":
      setConnected(false);
      break;
    case "ASSETUPDATE":
      handleAssetUpdate(dispatch, message.data);
      break;
    
  }
};

// -----------------------------------------------------------------------------------------------------------
function handleAssetUpdate(dispatch: Dispatch, data: string[]) {
  try {
    const assets = data.map((a) => JSON.parse(a));

    dispatch(assetsUpdateAction(assets));
  } catch (err) {
    console.error(err, data);
  }
}

// -----------------------------------------------------------------------------------------------------------
const websocketWorker = new WebsocketWorker();

// -----------------------------------------------------------------------------------------------------------
const Websocket: React.FC = React.memo(() => {
  const dispatch = useDispatch();

  // connect
  const [connected, setConnected] = useState(false);

  useEffect(() => {
    websocketWorker.onmessage = createHandleWebsocketMessage(dispatch, setConnected);

    websocketWorker.postMessage({
      event: "connect",
      payload: {wsURL: config.WS_URL, sess: localStorage.getItem("dkv-sess")},
    });

    return () => {
      websocketWorker.onmessage = null;
    };
  }, [dispatch]);

  // event types
  const eventTypes = useSelector(eventTypesSelector);

  useEffect(() => {
    if (!connected) {
      return;
    }

    websocketWorker.postMessage({
      event: "eventtypes",
      payload: eventTypes,
    });
  }, [eventTypes, connected]);

  return null;
});

export default Websocket;
