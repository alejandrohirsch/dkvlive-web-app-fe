import {AssetsActions} from "./assets/types";
import {WebsocketActions} from "./websocket/types";

export type LiveActionTypes = AssetsActions | WebsocketActions;
