import {IModule} from "redux-dynamic-modules";
import {LiveActionTypes} from "./actions";
import {ThunkAction} from "redux-thunk";
import {LiveState, reducer} from "./reducer";

export interface LiveModuleState {
  live: LiveState;
}

export type LiveThunkResult<R> = ThunkAction<R, LiveModuleState, void, LiveActionTypes>;

export const LiveModule: IModule<LiveModuleState> = {
  id: "live",
  reducerMap: {
    live: reducer,
  },
};
