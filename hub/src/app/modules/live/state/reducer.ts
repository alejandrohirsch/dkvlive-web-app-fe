import {createReducer} from "@reduxjs/toolkit";
import {
  ASSETS_LOADED,
  LOADING_ASSETS,
  LOADING_ASSETS_FAILED,
  ASSETS_UPDATE,
  MAP_FILTER_TOGGLE_TAG,
  MAP_FILTER_SET_SEARCH,
} from "./assets/types";
import {
  handleAssetsLoaded,
  handleLoadingAssets,
  AssetData,
  handleLoadingAssetsFailed,
  handleAssetsUpdate,
  handleMapFilerToggleTag,
  handleMapFilterSetSearch,
} from "./assets/reducer";
import {WebsocketData, handleAddWebsocketEventType} from "./websocket/reducer";
import {ADD_WEBSOCKET_EVENT_TYPE} from "./websocket/types";

// -----------------------------------------------------------------------------------------------------------
export interface LiveState {
  assets: AssetData;
  websocket: WebsocketData;
}

const initialState: LiveState = {
  assets: {
    loading: true,
    mapFilter: {
      inactiveTags: {},
    },
  },
  websocket: {
    eventTypes: {},
  },
};

export const reducer = createReducer(initialState, {
  // assets
  [LOADING_ASSETS]: handleLoadingAssets,
  [LOADING_ASSETS_FAILED]: handleLoadingAssetsFailed,
  [ASSETS_LOADED]: handleAssetsLoaded,
  [ASSETS_UPDATE]: handleAssetsUpdate,

  [MAP_FILTER_TOGGLE_TAG]: handleMapFilerToggleTag,
  [MAP_FILTER_SET_SEARCH]: handleMapFilterSetSearch,

  // websocket
  [ADD_WEBSOCKET_EVENT_TYPE]: handleAddWebsocketEventType,
});
