import {LiveModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const websocketDataSelector = (state: LiveModuleState) => state.live.websocket;

// event types
export const eventTypesSelector = createSelector(websocketDataSelector, (data) =>
  data.eventTypes ? Object.keys(data.eventTypes) : []
);
