// -----------------------------------------------------------------------------------------------------------
export const ADD_WEBSOCKET_EVENT_TYPE = "live/addWebsocketEventType";

export interface AddWebsocketEventTypeAction {
  type: typeof ADD_WEBSOCKET_EVENT_TYPE;
  payload: string;
}

// -----------------------------------------------------------------------------------------------------------
export type WebsocketActions = AddWebsocketEventTypeAction;
