import {AddWebsocketEventTypeAction, ADD_WEBSOCKET_EVENT_TYPE} from "./types";

// -----------------------------------------------------------------------------------------------------------
export function addWebsocketEventTypeAction(eventType: string): AddWebsocketEventTypeAction {
  return {
    type: ADD_WEBSOCKET_EVENT_TYPE,
    payload: eventType,
  };
}
