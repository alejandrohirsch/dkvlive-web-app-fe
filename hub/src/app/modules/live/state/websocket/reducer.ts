import {LiveState} from "../reducer";
import {AddWebsocketEventTypeAction} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface WebsocketData {
  eventTypes: {[eventType: string]: boolean};
}

// -----------------------------------------------------------------------------------------------------------
export function handleAddWebsocketEventType(state: LiveState, action: AddWebsocketEventTypeAction) {
  const data = state.websocket;
  data.eventTypes = {...data.eventTypes, [action.payload]: true};
}
