import {LiveThunkResult} from "../module";
import {Asset} from "./reducer";
import httpClient from "services/http";
import {
  LoadingAssetsAction,
  LOADING_ASSETS,
  AssetsLoadedAction,
  ASSETS_LOADED,
  LoadingAssetsFailedAction,
  LOADING_ASSETS_FAILED,
  ASSETS_UPDATE,
  AssetsUpdateAction,
  MapFilterToggleTagAction,
  MAP_FILTER_TOGGLE_TAG,
  MapFilterSetSearchAction,
  MAP_FILTER_SET_SEARCH,
} from "./types";
import {addWebsocketEventTypeAction} from "../websocket/actions";

// -----------------------------------------------------------------------------------------------------------
function loadingAssetsAction(): LoadingAssetsAction {
  return {
    type: LOADING_ASSETS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingAssetsFailedAction(errorText?: string): LoadingAssetsFailedAction {
  return {
    type: LOADING_ASSETS_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function assetsLoadedAction(items: Asset[]): AssetsLoadedAction {
  return {
    type: ASSETS_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function assetsUpdateAction(items: Asset[]): AssetsUpdateAction {
  return {
    type: ASSETS_UPDATE,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function toggleMapFilterTagAction(tag: string): MapFilterToggleTagAction {
  return {
    type: MAP_FILTER_TOGGLE_TAG,
    payload: tag,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function setMapFilterSearch(search: string): MapFilterSetSearchAction {
  return {
    type: MAP_FILTER_SET_SEARCH,
    payload: search,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadAssets(): LiveThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingAssetsAction());
    dispatch(addWebsocketEventTypeAction("assetupdate"));

    const [{data}, err] = await httpClient.get("/management/assets");
    if (err !== null) {
      // @todo: log
      dispatch(loadingAssetsFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(assetsLoadedAction(data));
  };
}

export function loadAssetsIfNeeded(): LiveThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().live.assets;
    const isLoaded = data.items !== undefined && data.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadAssets());
  };
}
