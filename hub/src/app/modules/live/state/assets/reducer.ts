import {LiveState} from "../reducer";
import {
  AssetsLoadedAction,
  LoadingAssetsFailedAction,
  AssetsUpdateAction,
  MapFilterToggleTagAction,
  MapFilterSetSearchAction,
} from "./types";
import dayjs from "dayjs";
import {merge} from "lodash";
import {resolveUserDateFormat, resolveUserDayFormat} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
export interface Asset {
  id: string;
  division_id: string;
  organization_id: string;
  cluster_id: string;
  name: string;
  license_plate: string;
  license_plate_normalized: string;
  vin: string;
  vin_normalized: string;
  asset_type_id: string;
  registration_country: string;
  data_category_device_restriction: DataCategoryDeviceRestriction;
  data_category_support: DataCategorySupport;
  created_at: string;
  modified_at: string;
  vehicle_data: VehicleData;
  gnss: GNSS;
  canbus: CANBus;
  calc: Calc;
  tags: Tag[];
  service_cards: Array<string>;
  inspection_dates?: Array<InspectionDate>;
  driver1?: Driver;
  driver2?: Driver;

  fuelcards?: Array<string>;
  serviceinterval?: string;
  nextservice?: string;
  _status?: string;
  _prevTrack?: number;
  _currentTrack?: number;
  _displayName?: string;
}

interface VehicleData {
  classification: string;
  pollutant_category: string;
  fuel_capacity: number;
  type_of_drive: string;
  permissible_gross_weight: number;
  max_permissible_gross_weight: number;
  carrying_weight: number;
  number_of_axles: number;
  trailerable: boolean;
  mileage_offset: number;
  engine_hours: number;
}

interface Tag {
  id: string;
  name: string;
}

interface InspectionDate {
  id: string;
  inspection_type_id: string;
  due_date?: string;
  reminder_date?: string;
  done: boolean;
  execution_date?: string;
}

interface DataCategoryDeviceRestriction {
  gnss_device_id: string | null;
  canbus_device_id: string | null;
  ebs_device_id: string | null;
  thermo_device_id: string | null;
  rdl_device_id: string | null;
}

interface DataCategorySupport {
  gnss?: boolean;
  canbus?: boolean;
  ebs?: boolean;
  thermo?: boolean;
  rdl?: boolean;
}

interface GNSS {
  ts: string | null;
  _ts: string | null;
  satellites: number | null;
  altitude: number | null;
  latitude: number | null;
  longitude: number | null;
  speed: number | null;
  track: number | null;
  address: GeoAddress | null;
}

interface CANBus {
  ts: string | null;
  _ts: string | null;
  acc_pedal: number | null;
  aftertreatment_level_1: number | null;
  air_pressure_1: number | null;
  air_pressure_2: number | null;
  ambient_air_temp: number | null;
  break_pedal: boolean | null;
  clutch_pedal: boolean | null;
  cruise_control: boolean | null;
  current_gear: number | null;
  engine_hours: number | null;
  engine_load: number | null;
  engine_speed: number | null;
  engine_temp: number | null;
  engine_torque: number | null;
  fuel_level: number | null;
  fuel_rate: number | null;
  fuel_used: number | null;
  fuel_used_hr: number | null;
  gross_weight: number | null;
  inst_fuel_eco: number | null;
  parking_break: boolean | null;
  retarder_actual_torque: number | null;
  retarder_selection: number | null;
  retarder_torque_mode: number | null;
  selected_gear: number | null;
  service_distance: number | null;
  truck_axle_weight_1: number | null;
  truck_axle_weight_2: number | null;
  truck_axle_weight_3: number | null;
  truck_axle_weight_4: number | null;
  truck_axle_weight_5: number | null;
  vehicle_distance: number | null;
  wheel_vehicle_speed: number | null;
  driver_1_identification: number | null;
  driver_1_working_state: number | null;
  driver_1_time_state: number | null;
  driver_1_card_present: number | null;
  driver_2_identification: number | null;
  driver_2_working_state: number | null;
  driver_2_time_state: number | null;
  driver_2_card_present: number | null;
  tachograph_performance: number | null;
  vehicle_motion: number | null;
  vehicle_overspeed: number | null;
  direction_indicator: number | null;
  tacho_vehicle_speed: number | null;
}

interface GeoAddress {
  label: string | null;
  country_code: string | null;
  country_name: string | null;
  state: string | null;
  county: string | null;
  city: string | null;
  district: string | null;
  street: string | null;
  postal_code: string | null;
  house_number: string | null;
}

interface Calc {
  speed?: number;
  connection_status?: number;
  device_error_code?: number;
}

interface Driver {
  id: string;
  card: DriverCard;
  driver_id_full: string;
  issuing_member_state: number;
  issuing_member_state_alpha: string;
  driver_id: string;
  card_number: string;
  first_name: string;
  last_name: string;
  date_of_birth: string;
  phone_number: string;
  display_name: string;
}

interface DriverCard {
  driver_id_full: string;
  issuing_member_state: number;
  issuing_member_state_alpha: string;
  driver_id: string;
  card_number: string;
}

interface MapFilter {
  inactiveTags: {[key: string]: boolean};
  search?: string;
}

export interface AssetData {
  loading: boolean;
  mapFilter: MapFilter;
  loaded?: boolean;
  error?: boolean;
  errorText?: string;
  items?: {[id: string]: Asset};
  itemsTags?: {[key: string]: string};
  mapItems?: {[id: string]: boolean};
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingAssets(state: LiveState) {
  const data = state.assets;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingAssetsFailed(state: LiveState, action: LoadingAssetsFailedAction) {
  const data = state.assets;

  data.loading = false;
  data.error = true;
  data.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleAssetsLoaded(state: LiveState, action: AssetsLoadedAction) {
  const data = state.assets;

  data.loading = false;
  data.loaded = true;

  const items = {};
  const mapItems = {};
  const tags = {};
  action.payload.sort(sortMapAssets).forEach((a) => {
    updateAssetData(a);
    items[a.id] = a;

    a.tags.forEach((t: any) => (tags[t.name] = t.id));
    mapItems[a.id] = showOnMap(a, data.mapFilter);
  });

  data.items = items;
  data.itemsTags = tags;
  data.mapItems = mapItems;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleAssetsUpdate(state: LiveState, action: AssetsUpdateAction) {
  const data = state.assets;

  const items = {};

  let mapItems = data.mapItems ?? {};
  let mapItemsChanged = false;
  let rebuildMapItems = false;

  const tags = data.itemsTags ?? {};
  let tagsChanged = false;

  action.payload.forEach((a) => {
    const i = data.items ? data.items[a.id] : null;
    if (!i) {
      return;
    }

    a = merge(i, a);
    updateAssetData(a);

    items[a.id] = a;
    a.tags.forEach((t: any) => {
      if (!tags[t.name]) {
        tags[t.name] = t.id;
        tagsChanged = true;
      }
    });

    const isMapItem = showOnMap(a, data.mapFilter);
    if (mapItems[a.id] === undefined) {
      rebuildMapItems = true;
    } else if (mapItems[a.id] !== isMapItem) {
      mapItemsChanged = true;
      mapItems[a.id] = isMapItem;
    }
  });

  data.items = {...data.items, ...items};

  if (tagsChanged) {
    data.itemsTags = {...tags};
  }

  if (rebuildMapItems) {
    mapItems = resolveMapItems(data.items, data.mapFilter);
    mapItemsChanged = true;
  }

  if (mapItemsChanged) {
    data.mapItems = {...mapItems};
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleMapFilerToggleTag(state: LiveState, action: MapFilterToggleTagAction) {
  const filter = state.assets.mapFilter;

  if (filter.inactiveTags[action.payload]) {
    delete filter.inactiveTags[action.payload];
  } else {
    filter.inactiveTags[action.payload] = true;
  }

  state.assets.mapFilter = {...filter};

  state.assets.mapItems = resolveMapItems(state.assets.items ?? {}, filter);
}

// -----------------------------------------------------------------------------------------------------------
export function handleMapFilterSetSearch(state: LiveState, action: MapFilterSetSearchAction) {
  const filter = state.assets.mapFilter;

  filter.search = action.payload;

  state.assets.mapFilter = {...filter};

  state.assets.mapItems = resolveMapItems(state.assets.items ?? {}, filter);
}

// -----------------------------------------------------------------------------------------------------------
function updateAssetData(a: Asset) {
  // status
  let status = "offline";
  switch (a.calc.connection_status) {
    case 0:
      status = "offline";
      break;
    case 1:
      status = "standby";
      break;
    case 2:
      status = "online";
      break;
    case 3:
      status = "online";
      break;
    case 4:
      status = "offline"; // @todo: error
      break;
  }

  a._status = status;

  // dates
  if (a.canbus) {
    if (a.canbus.ts) {
      a.canbus._ts = dayjs(a.canbus.ts).format(resolveUserDateFormat());
    } else {
      a.canbus._ts = null;
    }
  }

  if (a.gnss.ts) {
    a.gnss._ts = dayjs(a.gnss.ts).format(resolveUserDateFormat());
  } else {
    a.gnss._ts = null;
  }

  a._prevTrack = a._currentTrack ?? a.gnss.track ?? undefined;
  a._currentTrack = a.gnss.track ?? undefined;

  let displayName = "";
  if (a.name) {
    displayName = a.name;

    if (a.license_plate) {
      displayName = `${displayName} (${a.license_plate})`;
    }
  } else if (a.license_plate) {
    displayName = a.license_plate;
  }

  a._displayName = displayName;

  // @todo: remove
  const fuelcardnumbers = [
    "704310 115695 60295",
    "704310 415678 50090",
    "704310 215811 24307",
    "704310 514680 11819",
    "704310 215686 53895",
    "704310 913695 84371",
    "704310 215686 51543",
  ];

  if (!a.fuelcards) {
    a.fuelcards = [];
    const fuelcardAmount = Math.ceil(Math.random() * 3);
    for (let i = 0; i < fuelcardAmount; i++) {
      a.fuelcards.push(fuelcardnumbers[Math.floor(Math.random() * fuelcardnumbers.length)]);
    }
  }

  if (!a.nextservice) {
    const inDays = Math.floor(Math.random() * (256 - 32 + 1)) + 32;
    const d = dayjs().add(inDays, "day");
    a.nextservice = d.format(resolveUserDayFormat());
  }
}

const showOnMap = (i: Asset, filter: MapFilter) => {
  // gnss data
  if (i.gnss.ts === null || (i.gnss.latitude === 0 && i.gnss.longitude === 0)) {
    return false;
  }

  // search
  if (filter.search) {
    const search = filter.search.toLowerCase();
    const driverName = i.driver1 ? i.driver1.display_name : "";

    if (
      !i.name.toLowerCase().includes(search) &&
      !i.license_plate_normalized.toLowerCase().includes(search) &&
      !driverName.toLowerCase().includes(search)
    ) {
      return false;
    }
  }

  // tags
  if (i.tags) {
    let inactive = 0;
    for (const t of i.tags) {
      if (filter.inactiveTags[t.name]) {
        inactive++;
      }
    }

    if (inactive === i.tags.length) {
      return false;
    }
  }

  return true;
};

const resolveMapItems = (items: {[key: string]: Asset}, filter: MapFilter) => {
  const mapItems = {};

  Object.values(items)
    .sort(sortMapAssets)
    .forEach((a) => {
      mapItems[a.id] = showOnMap(a, filter);
    });

  return mapItems;
};

const sortMapAssets = (a: Asset, b: Asset) => {
  const nameA = (a.name ?? a.license_plate_normalized).toLowerCase();
  const nameB = (b.name ?? b.license_plate_normalized).toLowerCase();

  if (nameA < nameB) {
    return -1;
  }

  if (nameA > nameB) {
    return 1;
  }

  return 0;
};
