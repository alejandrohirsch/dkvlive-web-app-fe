import {Asset} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ASSETS = "map/loadingAssets";

export interface LoadingAssetsAction {
  type: typeof LOADING_ASSETS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ASSETS_FAILED = "map/loadingAssetsFailed";

export interface LoadingAssetsFailedAction {
  type: typeof LOADING_ASSETS_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const ASSETS_LOADED = "map/assetsLoaded";

export interface AssetsLoadedAction {
  type: typeof ASSETS_LOADED;
  payload: Asset[];
}

// -----------------------------------------------------------------------------------------------------------
export const ASSETS_UPDATE = "map/assetsUpdate";

export interface AssetsUpdateAction {
  type: typeof ASSETS_UPDATE;
  payload: Asset[];
}

// -----------------------------------------------------------------------------------------------------------
export const MAP_FILTER_TOGGLE_TAG = "map/mapFilterToggleTag";

export interface MapFilterToggleTagAction {
  type: typeof MAP_FILTER_TOGGLE_TAG;
  payload: string;
}

// -----------------------------------------------------------------------------------------------------------
export const MAP_FILTER_SET_SEARCH = "map/mapFilterSetSearch";

export interface MapFilterSetSearchAction {
  type: typeof MAP_FILTER_SET_SEARCH;
  payload: string;
}

// -----------------------------------------------------------------------------------------------------------
export type AssetsActions =
  | LoadingAssetsAction
  | LoadingAssetsFailedAction
  | AssetsLoadedAction
  | AssetsUpdateAction
  | MapFilterToggleTagAction
  | MapFilterSetSearchAction;
