import {LiveModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const assetsItemsSelector = (state: LiveModuleState) => state.live.assets.items ?? {};

// map filter
export const mapFilterSelector = (state: LiveModuleState) => state.live.assets.mapFilter;

// map items
export const mapItemsSelector = (state: LiveModuleState) => state.live.assets.mapItems ?? {};

// list
export const assetsListSelector = createSelector(assetsItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// map list
export const mapAssetsListSelector = createSelector(mapItemsSelector, (items) =>
  items ? Object.keys(items).filter((k) => items[k] === true) : []
);

export const mapFilterActiveSelector = createSelector(mapFilterSelector, (filter) => {
  return (filter.search && filter.search !== "") || Object.keys(filter.inactiveTags).length > 0;
});

// asset
export const assetSelector = (id: string) =>
  createSelector(assetsItemsSelector, (items) => (items ? items[id] : undefined));

// tags
export const assetTagsSelector = (state: LiveModuleState) => state.live.assets.itemsTags ?? {};
