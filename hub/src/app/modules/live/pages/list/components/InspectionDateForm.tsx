import React, {useState} from "react";
import {
  WindowContentContainer,
  WindowHeadline,
  WindowContent,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import {TextField, Message, Button} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components";
import {useFormik} from "formik";
import httpClient from "services/http";
import {useDispatch} from "react-redux";
import {loadAssets} from "app/modules/live/state/assets/actions";
import {MuiPickersUtilsProvider, KeyboardDatePicker} from "@material-ui/pickers";
import DayjsUtils from "@date-io/dayjs";
import {resolveUserDateLocale, resolveUserDayFormat} from "app/modules/login/state/login/selectors";
import dayjs from "dayjs";

// -----------------------------------------------------------------------------------------------------------
export const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

export const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

export const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

// -----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
`;

const ErrorMessage = styled(Message)`
  margin-bottom: 24px;
`;

// -----------------------------------------------------------------------------------------------------------
interface InspectionDateData {
  due_date: string;
  reminder_date: string;
}

interface InspectionDateFormProps extends WindowContentProps {
  inspectionDate: {
    id: string;
    due_date: string;
    reminder_date: string;
  };
}

const InspectionDateForm: React.FC<InspectionDateFormProps> = React.memo(
  ({inspectionDate, setLoading, forceCloseWindow}) => {
    const {t} = useTranslation();
    const dispatch = useDispatch();

    // form
    const [error, setError] = useState("");

    const form = useFormik<InspectionDateData>({
      initialValues: {
        due_date:
          !inspectionDate.due_date || inspectionDate.due_date === "0001-01-01T00:00:00Z"
            ? ""
            : inspectionDate.due_date,
        reminder_date:
          !inspectionDate.reminder_date || inspectionDate.reminder_date === "0001-01-01T00:00:00Z"
            ? ""
            : inspectionDate.reminder_date,
      },
      onSubmit: (values) => {
        const inspectionDateData = {
          due_date: values.due_date,
          reminder_date: values.reminder_date || undefined,
        };

        setLoading(true, true, t("wird gespeichert"));
        setError("");

        const submit = async () => {
          const [, err] = await httpClient.put(
            `/management/inspections/${inspectionDate.id}`,
            JSON.stringify(inspectionDateData),
            {
              headers: {
                "Content-Type": "application/json",
              },
            }
          );
          if (err !== null) {
            setError("Änderung fehlgeschlagen");
            setLoading(false);
            form.setSubmitting(false);
            return;
          }

          dispatch(loadAssets());
          setLoading(false);
          form.setSubmitting(false);
          forceCloseWindow();
        };

        submit();
      },
    });

    // render
    return (
      <WindowContentContainer minWidth="700px" full>
        <WindowHeadline padding>
          <h1>{t("Service Termin ändern")}</h1>
        </WindowHeadline>

        <WindowContent padding>
          <Form onSubmit={form.handleSubmit}>
            {error && <ErrorMessage text={t(error)} error />}
            <FormGrid>
              <MuiPickersUtilsProvider utils={DayjsUtils} locale={resolveUserDateLocale()}>
                <KeyboardDatePicker
                  disableToolbar
                  name="due_date"
                  label={t("Fälligkeit")}
                  variant="inline"
                  format={resolveUserDayFormat()}
                  margin="none"
                  value={form.values.due_date ? dayjs(form.values.due_date).toDate() : null}
                  minDate={dayjs()
                    .startOf("day")
                    .toDate()}
                  onChange={(date: any) => {
                    form.setFieldValue("due_date", dayjs(date).format());
                  }}
                  TextFieldComponent={TextField as any}
                  invalidDateMessage={t("Ungültiges Datum")}
                  keyboardIcon={<i className="icon-ico_cal-week" />}
                  PopoverProps={{
                    className: "dkv-datetimepicker",
                  }}
                  inputVariant="outlined"
                  disabled={form.isSubmitting}
                  required
                />
                <KeyboardDatePicker
                  disableToolbar
                  name="reminder_date"
                  label={t("Errinnerung")}
                  variant="inline"
                  format={resolveUserDayFormat()}
                  margin="none"
                  value={form.values.reminder_date ? dayjs(form.values.reminder_date).toDate() : null}
                  minDate={dayjs()
                    .startOf("day")
                    .toDate()}
                  maxDate={form.values.due_date ? dayjs(form.values.due_date).toDate() : null}
                  onChange={(date: any) => {
                    form.setFieldValue("reminder_date", dayjs(date).format());
                  }}
                  TextFieldComponent={TextField as any}
                  invalidDateMessage={t("Ungültiges Datum")}
                  keyboardIcon={<i className="icon-ico_cal-week" />}
                  PopoverProps={{
                    className: "dkv-datetimepicker",
                  }}
                  inputVariant="outlined"
                  disabled={form.isSubmitting}
                />
              </MuiPickersUtilsProvider>
            </FormGrid>

            <WindowActionButtons>
              <Button type="button" secondary onClick={forceCloseWindow}>
                {t("Abbrechen")}
              </Button>

              <Button type="submit" disabled={form.isSubmitting}>
                {t("Speichern")}
              </Button>
            </WindowActionButtons>
          </Form>
        </WindowContent>
      </WindowContentContainer>
    );
  }
);

export default InspectionDateForm;
