import React from "react";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components";
import {Asset} from "app/modules/live/state/assets/reducer";
import {countries} from "services/countries";
import {formatNumber} from "dkv-live-frontend-ui";
import {resolveUserLanguage} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
  margin-right: 1em;
`;

const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

// -----------------------------------------------------------------------------------------------------------

interface AssetInformationProps {
  asset?: Asset;
  assetTypes?: any;
}

const AssetInformation: React.FC<AssetInformationProps> = ({asset, assetTypes}) => {
  const {t} = useTranslation();

  const locale = resolveUserLanguage();

  return (
    <div style={{display: "flex"}}>
      <div style={{flexBasis: "50%"}}>
        <Table>
          <tbody>
            <tr>
              <TableLabel>{t("Fahrzeugtyp")}:</TableLabel>
              <TableValue>
                {asset && assetTypes ? t(assetTypes[asset.asset_type_id]) : t("wird geladen...")}
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Kennzeichen")}:</TableLabel>
              <TableValue>{asset ? asset.license_plate : ""}</TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Fahrgestellnummer")}:</TableLabel>
              <TableValue>{asset ? asset.vin : ""}</TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Zulassungsland")}:</TableLabel>
              <TableValue>{asset ? t(countries[asset.registration_country]) : ""}</TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Fahrzeugklassifizierung")}:</TableLabel>
              <TableValue>
                {asset && asset.vehicle_data ? t(asset.vehicle_data.classification) : ""}
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Schadstoffklasse")}:</TableLabel>
              <TableValue>
                {asset && asset.vehicle_data ? t(asset.vehicle_data.pollutant_category) : ""}
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Tankvolumen")}:</TableLabel>
              <TableValue>
                {asset && asset.vehicle_data
                  ? formatNumber(locale, asset.vehicle_data.fuel_capacity ?? 0) + " l"
                  : ""}
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Antriebsart")}:</TableLabel>
              <TableValue>
                {asset && asset.vehicle_data ? t(asset.vehicle_data.type_of_drive) : ""}
              </TableValue>
            </tr>
          </tbody>
        </Table>
      </div>
      <div style={{flexBasis: "50%"}}>
        <Table>
          <tbody>
            <tr>
              <TableLabel>{t("Zulässiges Gesamtgewicht")}:</TableLabel>
              <TableValue>
                {asset && asset.vehicle_data
                  ? formatNumber(locale, asset.vehicle_data.permissible_gross_weight ?? 0) + " kg"
                  : ""}
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Max zul. Gesamtgewicht")}:</TableLabel>
              <TableValue>
                {asset && asset.vehicle_data
                  ? formatNumber(locale, asset.vehicle_data.max_permissible_gross_weight ?? 0) + " kg"
                  : ""}
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Tragegewicht")}:</TableLabel>
              <TableValue>
                {asset && asset.vehicle_data
                  ? formatNumber(locale, asset.vehicle_data.carrying_weight ?? 0) + " kg"
                  : ""}
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Achsenanzahl")}:</TableLabel>
              <TableValue>{asset && asset.vehicle_data ? asset.vehicle_data.number_of_axles : ""}</TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Anhängerfähig")}:</TableLabel>
              <TableValue>
                {asset && asset.vehicle_data ? (asset.vehicle_data.trailerable ? t("Ja") : t("Nein")) : ""}
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Kilometerstand Offset")}:</TableLabel>
              <TableValue>
                {asset && asset.vehicle_data
                  ? formatNumber(locale, asset.vehicle_data.mileage_offset ?? 0) + " km"
                  : ""}
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Motorstunden")}:</TableLabel>
              <TableValue>
                {asset && asset.vehicle_data
                  ? formatNumber(locale, asset.vehicle_data.engine_hours ?? 0) + " h"
                  : ""}
              </TableValue>
            </tr>
          </tbody>
        </Table>
      </div>
    </div>
  );
};

export default AssetInformation;
