import React, {useState} from "react";
import {
  WindowContentContainer,
  WindowHeadline,
  WindowContent,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import {MenuItem} from "@material-ui/core";
import {TextField, Message, Button, Select} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components";
import {useFormik} from "formik";
import httpClient from "services/http";
import {useDispatch} from "react-redux";
import {loadAssets} from "app/modules/live/state/assets/actions";
import {Asset} from "app/modules/live/state/assets/reducer";
import {countries} from "services/countries";

// -----------------------------------------------------------------------------------------------------------
export const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

export const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

export const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

// -----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
`;

const ErrorMessage = styled(Message)`
  margin-bottom: 24px;
`;

// -----------------------------------------------------------------------------------------------------------
interface AssetData {
  name: string;
  license_plate: string;
  vin: string;
  asset_type_id: string;
  registration_country: string;
  classification: string;
  pollutant_category: string;
  fuel_capacity: number;
  type_of_drive: string;
  permissible_gross_weight: number;
  max_permissible_gross_weight: number;
  carrying_weight: number;
  number_of_axles: number;
  trailerable: boolean;
  mileage_offset: number;
  engine_hours: number;
}

interface AssetFormProps extends WindowContentProps {
  asset: Asset;
  assetTypes: any;
}

const AssetForm: React.FC<AssetFormProps> = React.memo(
  ({asset, assetTypes, setLoading, forceCloseWindow}) => {
    const {t} = useTranslation();
    const dispatch = useDispatch();

    // form
    const [error, setError] = useState("");

    // data
    const initialValues = {
      name: asset.name,
      license_plate: asset.license_plate,
      vin: asset.vin,
      asset_type_id: asset.asset_type_id,
      registration_country: asset.registration_country,
      classification: asset.vehicle_data.classification,
      pollutant_category: asset.vehicle_data.pollutant_category,
      fuel_capacity: asset.vehicle_data.fuel_capacity,
      type_of_drive: asset.vehicle_data.type_of_drive,
      permissible_gross_weight: asset.vehicle_data.permissible_gross_weight,
      max_permissible_gross_weight: asset.vehicle_data.max_permissible_gross_weight,
      carrying_weight: asset.vehicle_data.carrying_weight,
      number_of_axles: asset.vehicle_data.number_of_axles,
      trailerable: asset.vehicle_data.trailerable,
      mileage_offset: asset.vehicle_data.mileage_offset,
      engine_hours: asset.vehicle_data.engine_hours,
    };

    const form = useFormik<AssetData>({
      initialValues: initialValues ?? {
        name: "",
        license_plate: "",
        vin: "",
        registration_country: "",
        classification: "",
        pollutant_category: "",
        fuel_capacity: 0,
        type_of_drive: "",
        permissible_gross_weight: 0,
        max_permissible_gross_weight: 0,
        carrying_weight: 0,
        number_of_axles: 0,
        trailerable: false,
        mileage_offset: 0,
        engine_hours: 0,
      },
      onSubmit: (values) => {
        const assetData = {
          name: values.name,
          license_plate: values.license_plate,
          vin: values.vin,
          asset_type_id: values.asset_type_id,
          registration_country: values.registration_country,
          vehicle_data: {
            classification: values.classification,
            pollutant_category: values.pollutant_category,
            fuel_capacity: values.fuel_capacity,
            type_of_drive: values.type_of_drive,
            permissible_gross_weight: values.permissible_gross_weight,
            max_permissible_gross_weight: values.max_permissible_gross_weight,
            carrying_weight: values.carrying_weight,
            number_of_axles: values.number_of_axles,
            trailerable: Boolean(values.trailerable),
            mileage_offset: values.mileage_offset,
            engine_hours: values.engine_hours,
          },
        };

        setLoading(true, true, t("wird gespeichert"));
        setError("");

        const submit = async () => {
          const [, err] = await httpClient.put(`/management/assets/${asset.id}`, JSON.stringify(assetData), {
            headers: {
              "Content-Type": "application/json",
            },
          });
          if (err !== null) {
            setError("Änderung fehlgeschlagen");
            setLoading(false);
            form.setSubmitting(false);
            return;
          }

          dispatch(loadAssets());
          setLoading(false);
          form.setSubmitting(false);
          forceCloseWindow();
        };

        submit();
      },
    });

    // render
    return (
      <WindowContentContainer minWidth="700px" full>
        <WindowHeadline padding>
          <h1>{t("Fahrzeug ändern")}</h1>
        </WindowHeadline>

        <WindowContent padding>
          <Form onSubmit={form.handleSubmit}>
            {error && <ErrorMessage text={t(error)} error />}
            <FormGrid>
              <TextField
                name="name"
                label={t("Name")}
                type="text"
                value={form.values.name}
                onChange={form.handleChange}
                disabled={form.isSubmitting}
                variant="outlined"
              />
              <TextField
                name="license_plate"
                label={t("Kennzeichen")}
                type="text"
                value={form.values.license_plate}
                onChange={form.handleChange}
                disabled={form.isSubmitting}
                variant="outlined"
              />
              <Select
                name="asset_type_id"
                value={form.values.asset_type_id || " "}
                onChange={form.handleChange}
                label={t("Fahrzeugart")}
                style={{width: "100%"}}
                disabled={form.isSubmitting || !assetTypes || !Object.keys(assetTypes)}
                required
              >
                {Object.keys(assetTypes).map((id: string) => {
                  return (
                    <MenuItem key={id} value={id}>
                      {t(assetTypes[id])}
                    </MenuItem>
                  );
                })}
              </Select>
              <TextField
                name="vin"
                label={t("Fahrgestellnumer")}
                type="text"
                value={form.values.vin}
                onChange={form.handleChange}
                disabled={form.isSubmitting}
                variant="outlined"
                required
              />
              <Select
                name="registration_country"
                value={form.values.registration_country || " "}
                onChange={form.handleChange}
                label={t("Zulassungsland")}
                style={{width: "100%"}}
                disabled={form.isSubmitting}
                required
              >
                {Object.keys(countries).map((cc) => (
                  <MenuItem key={cc} value={cc}>
                    {t(countries[cc])}
                  </MenuItem>
                ))}
              </Select>

              <Select
                name="trailerable"
                value={form.values.trailerable ? 1 : 0}
                onChange={form.handleChange}
                label={t("Anhängerfähig")}
                style={{width: "100%"}}
                disabled={form.isSubmitting}
              >
                {[1, 0].map((value: number) => {
                  return (
                    <MenuItem key={value} value={value}>
                      {t(value ? "Ja" : "Nein")}
                    </MenuItem>
                  );
                })}
              </Select>
              <Select
                name="classification"
                value={form.values.classification || " "}
                onChange={form.handleChange}
                label={t("Fahrzeugklassifikation")}
                style={{width: "100%"}}
                disabled={form.isSubmitting}
              >
                {["N1: <=3,5t", "N2: >3,5 und <=12t", "N3: >12t"].map((value: string) => {
                  return (
                    <MenuItem key={value} value={value}>
                      {t(value)}
                    </MenuItem>
                  );
                })}
              </Select>
              <Select
                name="pollutant_category"
                value={form.values.pollutant_category || " "}
                onChange={form.handleChange}
                label={t("Schadstoffklasse")}
                style={{width: "100%"}}
                disabled={form.isSubmitting}
              >
                {["Euro I", "Euro II", "Euro III", "Euro IV", "Euro V", "Euro VI", "EEV"].map(
                  (value: string) => {
                    return (
                      <MenuItem key={value} value={value}>
                        {t(value)}
                      </MenuItem>
                    );
                  }
                )}
              </Select>
              <TextField
                name="mileage_offset"
                label={t("Kilometerstand Offset")}
                type="number"
                value={form.values.mileage_offset}
                onChange={form.handleChange}
                disabled={form.isSubmitting}
                variant="outlined"
              />
              <TextField
                name="fuel_capacity"
                label={t("Tankvolumen")}
                type="number"
                value={form.values.fuel_capacity}
                onChange={form.handleChange}
                disabled={form.isSubmitting}
                variant="outlined"
              />
              <Select
                name="type_of_drive"
                value={form.values.type_of_drive || " "}
                onChange={form.handleChange}
                label={t("Antriebsart")}
                style={{width: "100%"}}
                disabled={form.isSubmitting}
              >
                {["Diesel", "Benzin", "Erdgas", "LPG", "Elektro", "Hybrid", "Sonstiges"].map(
                  (value: string) => {
                    return (
                      <MenuItem key={value} value={value}>
                        {t(value)}
                      </MenuItem>
                    );
                  }
                )}
              </Select>
              <TextField
                name="number_of_axles"
                label={t("Achsenanzahl")}
                type="number"
                value={form.values.number_of_axles}
                onChange={form.handleChange}
                disabled={form.isSubmitting}
                variant="outlined"
              />
              <TextField
                name="permissible_gross_weight"
                label={t("Zulässiges Gesamtgewicht")}
                type="number"
                value={form.values.permissible_gross_weight}
                onChange={form.handleChange}
                disabled={form.isSubmitting}
                variant="outlined"
              />
              <TextField
                name="max_permissible_gross_weight"
                label={t("Maximal zul. Gesamtgewicht")}
                type="number"
                value={form.values.max_permissible_gross_weight}
                onChange={form.handleChange}
                disabled={form.isSubmitting}
                variant="outlined"
              />
              <TextField
                name="engine_hours"
                label={t("Motorstunden")}
                type="number"
                value={form.values.engine_hours}
                onChange={form.handleChange}
                disabled={form.isSubmitting}
                variant="outlined"
              />
              <TextField
                name="carrying_weight"
                label={t("Tragegewicht")}
                type="number"
                value={form.values.carrying_weight}
                onChange={form.handleChange}
                disabled={form.isSubmitting}
                variant="outlined"
              />
            </FormGrid>

            <WindowActionButtons>
              <Button type="button" secondary onClick={forceCloseWindow}>
                {t("Abbrechen")}
              </Button>

              <Button type="submit" disabled={form.isSubmitting}>
                {t("Speichern")}
              </Button>
            </WindowActionButtons>
          </Form>
        </WindowContent>
      </WindowContentContainer>
    );
  }
);

export default AssetForm;
