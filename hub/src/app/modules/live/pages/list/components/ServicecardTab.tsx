import React, {useEffect, useState} from "react";
import httpClient from "services/http";
import {useTranslation} from "react-i18next";
import styled from "styled-components";
import {Asset} from "app/modules/live/state/assets/reducer";
import {TextField} from "dkv-live-frontend-ui";
import {Close as CloseIcon} from "@styled-icons/material/Close";
import {useDispatch} from "react-redux";
import {loadAssets} from "app/modules/live/state/assets/actions";
import Autocomplete from "@material-ui/lab/Autocomplete";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------

const SplitTab = styled.div`
  display: flex;
  flex-grow: 1;
`;

const SectionContainer = styled.div`
  flex-basis: 50%;
  padding: 10px;
`;

const SectionTitle = styled.div`
  position: relative;
  color: var(--dkv-grey_70);
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 20px;
`;

const TitleIcon = styled.div`
  font-size: 24px;
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
`;

const TitleText = styled.div`
  margin-left: 30px;
`;

const ListEntryContainer = styled.div`
  display: flex;
  border-radius: 2px;
  border: solid 1px #98bace;
  background-color: #e9f3f9;
  margin-bottom: 5px;
  justify-content: space-between;
  padding: 10px;
`;

const ListEntryInfo = styled.div`
  display: flex;
  flex-direction: column;
`;

const ListEntryRemoveIconContainer = styled.div`
  display: flex;
`;

const ListEntryRemoveRemoveIcon = styled(CloseIcon)`
  margin: auto;
  &:hover {
    cursor: pointer;
  }
`;

const ListEntryTitle = styled.div`
  display: flex;
  font-weight: bold;
  margin: auto 0;
`;

// -----------------------------------------------------------------------------------------------------------

interface ServicecardTabProps {
  asset?: Asset;
}

const ServicecardTab: React.FC<ServicecardTabProps> = ({asset}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // loading
  const [loading, setLoading] = useState<boolean>(false);

  // servicecards
  const [servicecards, setServicecards] = useState(new Array<any>());

  // autocomplete input value
  const [inputValue, setInputValue] = useState("");

  useEffect(() => {
    //retrieve the devices for this division
    const loadServicecards = async () => {
      const [{data}, err] = await httpClient.get("/dkvcustomer/servicecards/findAllForUser");
      if (err !== null) {
        // setError("Servicekarten-Laden fehlgeschlagen");
        return;
      }
      setServicecards(data);
    };
    loadServicecards();
  }, []);

  const assignServiceCard = async (cardnumber: string) => {
    if (asset) {
      setLoading(true);
      const [res, err] = await httpClient.post(
        "/management/assets/" + asset.id + "/servicecards/" + cardnumber
      );
      if (err !== null) {
        // setError("Geräte-Laden fehlgeschlagen");
        setLoading(false);
        return;
      }
      console.info("Successfully assigned servicecard to asset", res);
      dispatch(loadAssets());
      setLoading(false);
    }
  };

  const deleteServiceCard = async (cardnumber: string) => {
    if (asset) {
      setLoading(true);
      const [res, err] = await httpClient.delete(
        "/management/assets/" + asset.id + "/servicecards/" + cardnumber
      );
      if (err !== null) {
        // setError("Geräte-Laden fehlgeschlagen");
        setLoading(false);
        return;
      }
      console.info("Successfully deleted servicecard from asset", res);
      dispatch(loadAssets());
      setLoading(false);
    }
  };

  return (
    <SplitTab>
      <SectionContainer>
        <SectionTitle>
          <TitleIcon className="icon-ico_creditcard" />
          <TitleText>{t("Zugewiesene Tankkarten") + ":"}</TitleText>
        </SectionTitle>
        <div>
          {asset
            ? asset.service_cards && asset.service_cards.length
              ? asset.service_cards.map((servicecard) => {
                  return (
                    <ListEntryContainer key={servicecard}>
                      <ListEntryInfo>
                        <ListEntryTitle>{servicecard}</ListEntryTitle>
                      </ListEntryInfo>
                      <ListEntryRemoveIconContainer>
                        {hasPermission("management:EditAssets") && (
                          <ListEntryRemoveRemoveIcon
                            size={24}
                            onClick={() => deleteServiceCard(servicecard)}
                          />
                        )}
                      </ListEntryRemoveIconContainer>
                    </ListEntryContainer>
                  );
                })
              : t("Keine Tankkarten zugewiesen!")
            : t("Tankkarten werden geladen...")}
        </div>
      </SectionContainer>
      {hasPermission("management:EditAssets") && (
        <SectionContainer>
          <SectionTitle>{t("Tankkarte zuweisen") + ":"}</SectionTitle>
          <div>
            {servicecards && servicecards.length ? (
              <Autocomplete
                value={""}
                inputValue={inputValue}
                onInputChange={(_: any, newInputValue: any | null) => {
                  setInputValue(newInputValue || "");
                }}
                onChange={(_: any, newValue: any | null) => {
                  if (newValue) {
                    assignServiceCard(newValue.cardnumber);
                    setInputValue("");
                  }
                }}
                autoHighlight
                options={
                  asset?.service_cards?.length
                    ? servicecards.filter((sc) => !asset.service_cards.includes(sc.cardnumber))
                    : servicecards
                }
                getOptionLabel={(sc: any) => sc.cardnumber ?? ""}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Tankkarte"
                    variant="outlined"
                    inputProps={{
                      ...params.inputProps,
                      autoComplete: "new-password",
                    }}
                  />
                )}
                placeholder="704310 123456 12345"
                disabled={loading}
                clearText={t("Leeren")}
                closeText={t("Schließen")}
                loadingText={t("Lädt...")}
                noOptionsText={t("Keine Optionen")}
                openText={t("Öffnen")}
              />
            ) : (
              <div>
                <div>
                  {t(
                    "Es konnten keine Tankkarten gefunden werden. Sollten Sie noch keine besitzen, können Sie diese hier anfordern."
                  )}
                </div>
                <div style={{height: "0.5em"}} />
                <div>
                  {t(
                    "Falls Sie bereits Tankkarten besitzen, diese aber nicht angezeigt bekommen, kontaktieren Sie bitte unseren Kundensupport."
                  )}
                </div>
              </div>
            )}
          </div>
        </SectionContainer>
      )}
    </SplitTab>
  );
};

export default ServicecardTab;
