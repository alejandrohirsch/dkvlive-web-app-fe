import React from "react";
import {
  WindowContentContainer,
  WindowHeadline,
  WindowContent,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import {Button} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import styled from "styled-components";

// -----------------------------------------------------------------------------------------------------------

const WarningContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const WarningIcon = styled.div`
  margin: auto;
  font-size: 64px;
  color: var(--dkv-highlight-warning-color);
  margin-bottom: 10px;
`;

const WarningText = styled.div`
  margin: auto;
  max-width: 300px;
  text-align: center;
`;

// -----------------------------------------------------------------------------------------------------------

interface AssignWarningProps extends WindowContentProps {
  onAccept: () => void;
}

const AssignWarning: React.FC<AssignWarningProps> = React.memo(({forceCloseWindow, onAccept}) => {
  const {t} = useTranslation();

  // render
  return (
    <WindowContentContainer minWidth="400px" full>
      <WindowHeadline padding>
        <h1>{t("Sind sie sicher?")}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <WarningContainer>
          <WarningIcon className="icon-ico_alert" />
          <WarningText>
            {t(
              "Dieses Gerät ist bereits mit einem Fahrzeug verbunden! Die bestehende " +
                "Verbindung würde somit aufgehoben. Möchten Sie wirklich fortfahren?"
            )}
          </WarningText>
        </WarningContainer>

        <WindowActionButtons>
          <Button type="button" secondary onClick={forceCloseWindow}>
            {t("Nein")}
          </Button>

          <Button
            type="submit"
            onClick={() => {
              forceCloseWindow();
              onAccept();
            }}
          >
            {t("Ja")}
          </Button>
        </WindowActionButtons>
      </WindowContent>
    </WindowContentContainer>
  );
});

export default AssignWarning;
