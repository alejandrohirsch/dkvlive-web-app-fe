import React, {useState, useEffect, useRef} from "react";
import {Asset} from "../../../state/assets/reducer";
import Window, {
  WindowContentContainer,
  WindowHeadline,
  WindowTabsContainer,
  WindowTabs,
  WindowContentProps,
  WindowActions,
} from "../../../../../components/Window";
import {Tab, loadScript, loadScripts, Loading} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components";
import {HereMap} from "../../map/Map";
import config from "config";
import {createAssetMarker, updateAssetMarker} from "../../map/components/MapAssets";
import {useSelector} from "react-redux";
import {assetSelector} from "app/modules/live/state/assets/selectors";
import {ToolbarButtonIcon, ToolbarButton} from "app/components/DataTable";
import AssetForm from "./AssetForm";
import httpClient from "services/http";
import InspectionTab from "./InspectionTab";
import AssetInformation from "./AssetInformation";
import ServicecardTab from "./ServicecardTab";
import DevicesTab from "./DevicesTab";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------

const MapContainer = styled.div`
  flex-grow: 1;
  margin-bottom: 20px;
`;

interface WindowTabProps {
  hidden: boolean;
}

const WindowTab = styled.div<WindowTabProps>`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  ${(props) =>
    props.hidden &&
    css`
      display: none;
    `}
`;

interface AssetWindowContentBoxProps {
  padding?: boolean;
}

const AssetWindowContent = styled.div<AssetWindowContentBoxProps>`
  display: flex;
  flex-grow: 1;
  ${(props) =>
    props.padding &&
    css`
      padding: 40px 32px 40px 32px;
    `}
`;
// -----------------------------------------------------------------------------------------------------------
interface AssetDetailsProps extends WindowContentProps {
  assetID: string;
}

const AssetDetails: React.FC<AssetDetailsProps> = React.memo(({assetID}) => {
  const {t} = useTranslation();

  // tab
  const [tab, setTab] = useState("data");

  const handleTabChange = (_: React.ChangeEvent<unknown>, newTab: string) => {
    setTab(newTab);
  };

  // eidt
  const [showEditWindow, setShowEditWindow] = useState<Asset | null>(null);
  const onCloseEditWindow = () => setShowEditWindow(null);
  const openEditWindow = () => setShowEditWindow(asset ?? null);

  // loading
  const [loading, setLoading] = useState<boolean>(false);

  // asset types
  const [assetTypes, setAssetTypes] = useState<any | null>(null);

  //inspection types
  const [inspectionTypes, setInspectionTypes] = useState<Array<any>>([]);

  // data
  const asset = useSelector(assetSelector(assetID || ""));

  const mapContainerRef = useRef<HTMLDivElement>(null);
  const mapRef = useRef<HereMap>();
  const markerRef = useRef<any>();

  //Initialize asset detail component
  useEffect(() => {
    const initialize = async () => {
      setLoading(true);
      // console.log("Intialize asset map...");
      if ((window as any).H === undefined) {
        const mapLibraryBaseURL = "https://js.api.here.com/v3/3.1"; //
        await loadScript(`${mapLibraryBaseURL}/mapsjs-core.js`);
        await loadScripts([
          `${mapLibraryBaseURL}/mapsjs-service.js`,
          `${mapLibraryBaseURL}/mapsjs-ui.js`,
          `${mapLibraryBaseURL}/mapsjs-mapevents.js`,
          `${mapLibraryBaseURL}/mapsjs-clustering.js`,
        ]);
      }

      const platform = new H.service.Platform({
        apikey: config.HERE_API_KEY,
      });

      const defaultLayers = platform.createDefaultLayers();

      const map = new H.Map(mapContainerRef.current, defaultLayers.vector.normal.map, {
        zoom: 15,
        pixelRatio: window.devicePixelRatio || 1,
      });
      const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map), {
        enabled: H.mapevents.Behavior.Feature.PINCH_ZOOM | H.mapevents.Behavior.Feature.WHEEL_ZOOM,
      });
      const ui = null;

      mapRef.current = {map, ui, defaultLayers, platform, behavior};

      //retrieve the asset types
      const [{data}, err] = await httpClient.get("/management/assets/types");
      if (err !== null) {
        // setError("Geräte-Laden fehlgeschlagen");
        setLoading(false);
        return;
      }
      setAssetTypes(
        data.reduce((map: any, type: any) => {
          map[type.id] = type.name;
          return map;
        }, {})
      );
      setLoading(false);
    };
    initialize();
  }, []);

  useEffect(() => {
    if (asset) {
      //retrieve the inspection types for this asset
      const loadInspectionTypes = async () => {
        const [{data}, err] = await httpClient.get(
          "/management/inspections/types/findAllForAsset/" + asset.id
        );
        if (err !== null) {
          // setError("Geräte-Laden fehlgeschlagen");
          return;
        }
        setInspectionTypes(data);
      };
      loadInspectionTypes();
      //draw asset on map and center
      if (mapRef.current) {
        const {map, ui} = mapRef.current;
        if (!markerRef.current) {
          markerRef.current = createAssetMarker(asset, map, ui);
          map.addObject(markerRef.current);
        } else {
          updateAssetMarker(markerRef.current, asset, map, ui);
        }
        // console.log("New asset (" + markerRef.current + ") position", markerRef.current.getGeometry());
        map.setCenter(markerRef.current.getGeometry());
      }
    } else {
      if (mapRef.current) {
        const {map} = mapRef.current;
        map.removeObject(markerRef.current);
      }
    }
  }, [asset, mapRef]);

  // render
  return (
    <WindowContentContainer minWidth="938px" minHeight="846px" full>
      <Loading inprogress={loading} zIndex={5001} />
      <WindowHeadline padding>
        <h1>{asset ? asset.name : t("Fahrzeug bearbeiten")}</h1>
      </WindowHeadline>

      <WindowTabsContainer>
        <WindowTabs value={tab} onChange={handleTabChange} variant="scrollable">
          <Tab label={t("Fahrzeugdaten")} value="data" />
          <Tab label={t("Tankkarte")} value="servicecards" />
          {inspectionTypes && inspectionTypes.length ? (
            <Tab label={t("Service")} value="inspectiondates" />
          ) : (
            ""
          )}
          <Tab label={t("Geräte")} value="devices" />
          {/* <Tab label={t("Zug")} value="train" /> */}
        </WindowTabs>
      </WindowTabsContainer>

      <AssetWindowContent padding>
        <WindowTab hidden={tab !== "data"}>
          <div style={{display: "flex", flexDirection: "column", flexGrow: 1}}>
            <MapContainer ref={mapContainerRef} />
            <AssetInformation asset={asset} assetTypes={assetTypes} />
            <WindowActions>
              {hasPermission("management:EditAssets") && (
                <ToolbarButton type="button" onClick={openEditWindow}>
                  <ToolbarButtonIcon>
                    <span className="icon-ico_edit" />
                  </ToolbarButtonIcon>
                  {t("Bearbeiten")}
                </ToolbarButton>
              )}
            </WindowActions>
          </div>
        </WindowTab>

        <WindowTab hidden={tab !== "servicecards"}>
          <ServicecardTab asset={asset} />
        </WindowTab>

        <WindowTab hidden={tab !== "inspectiondates"}>
          <InspectionTab asset={asset} />
        </WindowTab>
        <WindowTab hidden={tab !== "devices"}>
          <DevicesTab asset={asset} />
        </WindowTab>
        <WindowTab hidden={tab !== "train"}>
          <div style={{margin: "auto", fontSize: "18px"}}>{t("Diese Funktion kommt demnächst...")}</div>
        </WindowTab>
      </AssetWindowContent>

      {showEditWindow && (
        <Window onClose={onCloseEditWindow} headline={t("Fahrer")}>
          {(props) => <AssetForm {...props} asset={showEditWindow} assetTypes={assetTypes} />}
        </Window>
      )}
    </WindowContentContainer>
  );
});

export default AssetDetails;
