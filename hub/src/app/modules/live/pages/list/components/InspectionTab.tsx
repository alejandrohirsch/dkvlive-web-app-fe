import React, {useEffect, useState} from "react";
import httpClient from "services/http";
import {Message, TextField, Loading} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components";
import dayjs from "dayjs";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DayjsUtils from "@date-io/dayjs";
import {
  resolveUserDateLocale,
  resolveUserDayFormat,
  hasPermission,
} from "app/modules/login/state/login/selectors";
import {useDispatch} from "react-redux";
import {loadAssets} from "app/modules/live/state/assets/actions";
import Window from "app/components/Window";
import DeleteWarning from "./DeleteWarning";
import InspectionDateForm from "./InspectionDateForm";
import {Tooltip} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";

// -----------------------------------------------------------------------------------------------------------

const InspectionContainer = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: column;
`;

const InspectionHeader = styled.div`
  display: flex;
  flex-direction: column;
`;

const InspectionHeaderTitle = styled.div`
  margin-bottom: 20px;
  font-weight: bold;
  font-size: 16px;
`;

const InspectionList = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const InspectionCategory = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 10px 10px 0;
  flex-basis: 30%;
  flex-grow: 1;
`;

const InspectionCategoryTitle = styled.div`
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 1em;
`;

const InspectionDateContainer = styled.div`
  display: flex-grid;
  flex-direction: column;
`;

const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);
  min-width: 100px;

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

const TableActions = styled.td`
  padding-bottom: 8px;
  padding-left: 16px;
  display: flex;
  min-width: 100px;
`;

const TableIcon = styled.div`
  padding: 0 5px 0 5px;
  color: var(--dkv-grey_90);
  ${(props) =>
    typeof props.onClick === "function" &&
    css`
      &:hover {
        cursor: pointer;
      }
    `}
`;

const NoInspectionMessage = styled.div`
  flex-grow: 1;
  margin: auto;
  text-align: center;
  font-size: 16px;
  max-width: 600px;
`;

const DatePicker = styled.div`
  display: flex;
  flex-basis: 60%;
  flex-shrink: 1;
  .MuiTextField-root {
    margin-left: 1em !important;
    flex-shrink: 1;
  }
`;

const PickerUtil = styled(MuiPickersUtilsProvider)`
  display: flex;
  flex-shrink: 1;
`;

const RoundButtonIcon = styled.div`
  width: 36px;
  height: 36px;
  border-radius: 50%;
  background-color: #efefef;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 6px;
  transition: color linear 0.1s, background-color linear 0.1s;

  span {
    color: #666666;
    font-size: 16px;
    transition: color linear 0.1s;
  }
`;

interface RoundButtonProps {
  disabled?: boolean;
}

const RoundButton = styled.button<RoundButtonProps>`
  user-select: none;
  border: 0;
  outline: 0;
  overflow: hidden;
  font: inherit;
  background-color: transparent;
  display: flex;
  flex-direction: column;
  align-items: center;
  font-size: 12px;
  color: #666666;
  margin-left: 8px;
  transition: color linear 0.1s;

  ${(props) => {
    if (props.disabled) {
      return css`
        color: #ccc;

        ${RoundButtonIcon} {
          background-color: #eee;

          span {
            color: #ccc;
          }
        }
      `;
    }
    return css`
      &:hover,
      &:focus {
        color: #2e6b90;
        cursor: pointer;

        ${RoundButtonIcon} {
          background-color: #2e6b90;

          span {
            color: #fff;
          }
        }
      }
    `;
  }}
`;

const ErrorMessage = styled(Message)`
  margin-bottom: 24px;
`;

// -----------------------------------------------------------------------------------------------------------

interface InspectionDate {
  id: string;
  inspection_type_id: string;
  due_date?: string;
  reminder_date?: string;
  done: boolean;
  execution_date?: string;
}

interface InspectionTabProps {
  asset?: {id: string; inspection_dates?: Array<InspectionDate>};
}

const InspectionTab: React.FC<InspectionTabProps> = ({asset}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // loading
  const [loading, setLoading] = useState<boolean>(false);

  //error
  const [error, setError] = useState("");

  // inspection types
  const [inspectionTypes, setInspectionTypes] = useState({});
  const [unsetInspectionTypes, setUnsetInspectionTypes] = useState<Array<any>>([]);

  //inspection date definition
  const [inspectionTypeToSet, setInspectionTypeToSet] = useState<any | null>(null);
  const [dueDate, setDueDate] = useState<Date | null>(null);
  const [reminderDate, setReminderDate] = useState<Date | null>(null);

  //selection
  const [editInspection, setEditInspection] = useState<any | null>(null);
  const onCloseEdit = () => {
    setEditInspection(null);
  };
  const [deleteInspection, setDeleteInspection] = useState<any | null>(null);
  const onCloseWarning = () => {
    setDeleteInspection(null);
  };
  const onDeleteInspection = async () => {
    if (deleteInspection) {
      setLoading(true);
      const [, err] = await httpClient.delete(`/management/inspections/` + deleteInspection.id);
      if (err !== null) {
        setError("Service Termin Löschen fehlgeschlagen");
        setLoading(false);
        return;
      }
      dispatch(loadAssets());
      setLoading(false);
    }
  };

  useEffect(() => {
    if (asset) {
      const datesByTypeID = (asset.inspection_dates || []).reduce((map, iDate) => {
        if (!map[iDate.inspection_type_id]) {
          map[iDate.inspection_type_id] = [];
        }
        map[iDate.inspection_type_id].push(iDate);
        return map;
      }, {});
      //retrieve the inspection types for this asset
      const loadInspectionTypes = async () => {
        const [{data}, err] = await httpClient.get(
          "/management/inspections/types/findAllForAsset/" + asset.id
        );
        if (err !== null) {
          setError("Service Termine Laden fehlgeschlagen");
          return;
        }
        setInspectionTypes(
          data.reduce((obj: any, it: any) => {
            const category = it.category || "Allgemeine Service Termine";
            if (!obj[category]) {
              obj[category] = [];
            }
            it.dates = datesByTypeID[it.id] || [];
            obj[category].push(it);
            return obj;
          }, {})
        );
        const typesWithDate = Object.keys(datesByTypeID).reduce((arr, key) => {
          if (datesByTypeID[key].find((date: any) => !date.done)) {
            arr.push(key);
          }
          return arr;
        }, new Array<string>());
        const unset = data.filter((it: any) => !typesWithDate.includes(it.id));
        setUnsetInspectionTypes(unset);
        if (unset && unset.length) {
          setInspectionTypeToSet(unset[0]);
        }
      };
      loadInspectionTypes();
    }
  }, [asset]);

  const addInspectionDate = async () => {
    // console.log("add inspection", asset, unsetInspectionTypes, inspectionTypeToSet, inspectionDate);
    if (asset && unsetInspectionTypes && unsetInspectionTypes.length && inspectionTypeToSet && dueDate) {
      setLoading(true);
      const inspectionDateData = {
        asset_id: asset.id,
        inspection_type_id: inspectionTypeToSet.id,
        due_date: dayjs(dueDate).format(),
        reminder_date: reminderDate ? dayjs(reminderDate).format() : undefined,
      };
      const [, err] = await httpClient.post(`/management/inspections/`, JSON.stringify(inspectionDateData), {
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (err !== null) {
        setError("Service Termin Hinzufügen fehlgeschlagen");
        setLoading(false);
        return;
      }
      dispatch(loadAssets());
      setInspectionTypeToSet(null);
      setDueDate(null);
      setReminderDate(null);
      setLoading(false);
    }
  };

  // console.log("UnsetInspectionTypes:", unsetInspectionTypes);
  return (
    <InspectionContainer>
      <Loading inprogress={loading} zIndex={5001} />
      {error && <ErrorMessage text={t(error)} error />}
      {unsetInspectionTypes &&
      unsetInspectionTypes.length &&
      hasPermission("management:CreateInspections") ? (
        <InspectionHeader>
          <InspectionHeaderTitle>{t("Service Termin hinzufügen") + ":"}</InspectionHeaderTitle>
          <div style={{display: "flex"}}>
            <Autocomplete
              style={{flexGrow: 1}}
              value={inspectionTypeToSet}
              onChange={(_: any, newValue: any | null) => {
                if (newValue) {
                  setInspectionTypeToSet(newValue);
                }
              }}
              autoHighlight
              options={unsetInspectionTypes}
              getOptionLabel={(it: any) => it.name}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Termin-Art"
                  variant="outlined"
                  inputProps={{
                    ...params.inputProps,
                    autoComplete: "new-password",
                  }}
                />
              )}
              placeholder={t("Termin-Art wählen...")}
              disabled={loading}
              clearText={t("Leeren")}
              closeText={t("Schließen")}
              loadingText={t("Lädt...")}
              noOptionsText={t("Keine Optionen")}
              openText={t("Öffnen")}
            />
            <DatePicker>
              <PickerUtil utils={DayjsUtils} locale={resolveUserDateLocale()}>
                <KeyboardDatePicker
                  disableToolbar
                  name="due_date"
                  label={t("Fälligkeit")}
                  variant="inline"
                  format={resolveUserDayFormat()}
                  margin="none"
                  value={dueDate}
                  minDate={dayjs()
                    .startOf("day")
                    .toDate()}
                  onChange={(date: any) => {
                    setDueDate(date);
                  }}
                  TextFieldComponent={TextField as any}
                  invalidDateMessage={t("Ungültiges Datum")}
                  keyboardIcon={<i className="icon-ico_cal-week" />}
                  PopoverProps={{
                    className: "dkv-datetimepicker",
                  }}
                  inputVariant="outlined"
                  disabled={loading}
                  required
                />
                <KeyboardDatePicker
                  disableToolbar
                  name="reminder_date"
                  label={t("Errinnerung")}
                  variant="inline"
                  format={resolveUserDayFormat()}
                  margin="none"
                  value={reminderDate}
                  minDate={dayjs()
                    .startOf("day")
                    .toDate()}
                  maxDate={dueDate}
                  onChange={(date: any) => {
                    setReminderDate(date);
                  }}
                  TextFieldComponent={TextField as any}
                  invalidDateMessage={t("Ungültiges Datum")}
                  keyboardIcon={<i className="icon-ico_cal-week" />}
                  PopoverProps={{
                    className: "dkv-datetimepicker",
                  }}
                  inputVariant="outlined"
                  disabled={loading}
                />
              </PickerUtil>
            </DatePicker>
            <RoundButton disabled={!inspectionTypeToSet || !dueDate || loading} onClick={addInspectionDate}>
              <RoundButtonIcon>
                <span className="icon-ico_add" />
              </RoundButtonIcon>
              {t("Hinzufügen")}
            </RoundButton>
          </div>
        </InspectionHeader>
      ) : (
        ""
      )}
      <InspectionList>
        {Object.keys(inspectionTypes).length ? (
          Object.keys(inspectionTypes)
            .filter((key) =>
              inspectionTypes[key].find(
                (it: any) => it.dates && it.dates.find((d: any) => !d.done && !d.deleted)
              )
            )
            .map((key) => (
              <InspectionCategory key={key}>
                <InspectionCategoryTitle>{key}</InspectionCategoryTitle>
                <InspectionDateContainer>
                  <Table>
                    <tbody>
                      {inspectionTypes[key]
                        .filter((it: any) => it.dates && it.dates.find((d: any) => !d.done && !d.deleted))
                        .map((it: any) => {
                          const datesForType =
                            it.dates && it.dates.length
                              ? it.dates
                                  .filter((d: any) => !d.done)
                                  .sort((d1: any, d2: any) =>
                                    dayjs(d1.due_date).isBefore(dayjs(d2.due_date)) ? -1 : 1
                                  )
                              : [];
                          return (
                            <tr key={it.id}>
                              <TableLabel>{t(it.name)}:</TableLabel>
                              <TableValue>
                                {datesForType.length
                                  ? dayjs(datesForType[0].due_date).format(resolveUserDayFormat())
                                  : ""}
                              </TableValue>
                              {datesForType.length && datesForType[0].due_date ? (
                                <TableActions>
                                  <Tooltip
                                    title={
                                      !datesForType.length ||
                                      !datesForType[0].reminder_date ||
                                      datesForType[0].reminder_date === "0001-01-01T00:00:00Z"
                                        ? `${t("Keine Erinnerung gesetzt")}`
                                        : `${t("Erinnerung")}: ${dayjs(datesForType[0].reminder_date).format(
                                            resolveUserDayFormat()
                                          )}`
                                    }
                                  >
                                    <TableIcon
                                      className="icon-ico_clock"
                                      style={
                                        !datesForType.length ||
                                        !datesForType[0].reminder_date ||
                                        datesForType[0].reminder_date === "0001-01-01T00:00:00Z"
                                          ? {color: "var(--dkv-grey_40)"}
                                          : undefined
                                      }
                                    />
                                  </Tooltip>
                                  {hasPermission("management:EditInspections") && (
                                    <TableIcon
                                      className="icon-ico_edit"
                                      onClick={() => setEditInspection(datesForType[0])}
                                    />
                                  )}

                                  {hasPermission("management:DeleteInspections") && (
                                    <TableIcon
                                      className="icon-ico_delete"
                                      onClick={() => setDeleteInspection(datesForType[0])}
                                    />
                                  )}
                                </TableActions>
                              ) : (
                                <TableActions />
                              )}
                            </tr>
                          );
                        })}
                    </tbody>
                  </Table>
                </InspectionDateContainer>
              </InspectionCategory>
            ))
        ) : (
          <NoInspectionMessage>
            <div style={{marginBottom: "0.5em"}}>
              {t(
                "Für dieses Fahrzeug stehen leider keine Service Termine zur Verfügung," +
                  " bitte überprüfen Sie ob die Fahrzeugart und das Zulassungsland korrekt" +
                  " gesetzt sind."
              )}
            </div>
            <div>
              {t("Falls das Problem weiterhin besteht, kontaktieren Sie bitte unseren Kundensupport.")}
            </div>
          </NoInspectionMessage>
        )}
      </InspectionList>
      {editInspection && (
        <Window onClose={onCloseEdit} headline={t("Service Termin ändern")}>
          {(props) => <InspectionDateForm {...props} inspectionDate={editInspection} />}
        </Window>
      )}
      {deleteInspection && (
        <Window onClose={onCloseWarning} headline={t("Sind Sie sicher?")}>
          {(props) => <DeleteWarning {...props} onAccept={onDeleteInspection} />}
        </Window>
      )}
    </InspectionContainer>
  );
};

export default InspectionTab;
