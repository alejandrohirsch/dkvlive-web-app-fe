import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {Button, Message} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowActionButtons,
  WindowTabs,
  WindowTabsContainer,
} from "app/components/Window";
import {Tab, TextField} from "dkv-live-frontend-ui";

// -----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 260px 260px 260px;
  grid-gap: 24px;
`;

// -----------------------------------------------------------------------------------------------------------
interface RegisterData {
  serialNumber: string;
}

// -----------------------------------------------------------------------------------------------------------
const RegisterForm: React.FC<WindowContentProps> = ({forceCloseWindow, setLoading, headline}) => {
  const {t} = useTranslation();
  // const dispatch = useDispatch();

  // // form
  const [error, setError] = useState("");

  const form = useFormik<RegisterData>({
    initialValues: {serialNumber: ""},
    onSubmit: (values) => {
      console.log(`values:`, values); // nocheckin

      setLoading(true, true, t("wird registriert"));
      setError("");

      const submit = async () => {
        //   const formData = new FormData();
        //   formData.append("label", values.label);
        //   formData.append("reference_language", values.referenceLanguage.value);
        //   if (editMode) {
        //     formData.append("id", values.id || "");
        //   }
        //   let err;
        //   if (editMode) {
        //     [, err] = await httpClient.put(`/i18n/namespaces/${values.id}`, formData);
        //   } else {
        //     [, err] = await httpClient.post("/i18n/namespaces", formData);
        //   }
        //   if (err !== null) {
        //     setError(editMode ? "Änderung fehlgeschlagen" : "Erstellen fehlgeschlagen");
        //     setLoading(false);
        //     form.setSubmitting(false);
        //     return;
        //   }
        //   dispatch(loadNamespaces());
        // forceCloseWindow();
        console.log(`forceCloseWindow:`, forceCloseWindow); // nocheckin
      };

      submit();
    },
  });

  // render
  return (
    <WindowContentContainer full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowTabsContainer>
        <WindowTabs value={0} variant="scrollable">
          <Tab label={t("Ortung und Fahrer")} />
          <Tab label={t("Lenk- u. Ruhezeiten")} />
          <Tab label={t("Fahrten")} />
        </WindowTabs>
      </WindowTabsContainer>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {error && <Message text={t(error)} error />}

          <FormGrid>
            <TextField
              name="serialNumber"
              type="text"
              label={t("Seriennummer")}
              value={form.values.serialNumber}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="serialNumber"
              type="text"
              label={t("Seriennummer")}
              value={form.values.serialNumber}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="serialNumber"
              type="text"
              label={t("Seriennummer")}
              value={form.values.serialNumber}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="serialNumber"
              type="text"
              label={t("Seriennummer")}
              value={form.values.serialNumber}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="serialNumber"
              type="text"
              label={t("Seriennummer")}
              value={form.values.serialNumber}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="serialNumber"
              type="text"
              label={t("Seriennummer")}
              value={form.values.serialNumber}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="serialNumber"
              type="text"
              label={t("Seriennummer")}
              value={form.values.serialNumber}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="serialNumber"
              type="text"
              label={t("Seriennummer")}
              value={form.values.serialNumber}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
            />
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !form.values.serialNumber}>
              {t("Registrieren")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default RegisterForm;
