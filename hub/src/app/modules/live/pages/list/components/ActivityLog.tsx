import React, {useState, useEffect, useCallback} from "react";
import Window, {WindowContentContainer, WindowHeadline} from "app/components/Window";
import httpClient from "services/http";
import dayjs from "dayjs";
import {FixedSizeList as List, ListChildComponentProps} from "react-window";
import styled, {css} from "styled-components";
import AutoSizer from "react-virtualized-auto-sizer";
import {Message, TextField, Button, Select, Checkbox} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import {useFormik} from "formik";
import {KeyboardDateTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DayjsUtils from "@date-io/dayjs";
import {MenuItem, IconButton} from "@material-ui/core";
import ListItemText from "@material-ui/core/ListItemText";
import {resolveUserDateFormat, resolveUserDateLocale} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
interface ListWrapperProps {
  maxListHeight?: string;
}

const ListWrapper = styled.div<ListWrapperProps>`
  min-width: 800px;
  height: 580px;

  ${(props) =>
    props.maxListHeight &&
    css`
      max-height: ${props.maxListHeight};
    `}
`;

const EntryContainer = styled.div`
  padding: 0 24px;

  & > div {
    padding: 8px 0 10px 0;
    border-bottom: 1px solid #efefef;
  }
`;

const EntryContainerHead = styled.div`
  display: flex;
`;

const EntryContainerTags = styled.div`
  margin-top: 2px;
  display: flex;
  opacity: 0.7;
`;

const EntryContainerBody = styled.div`
  margin-top: 8px;
  font-size: 1.1428571428571428rem;
  display: flex;
  align-items: center;
  overflow: hidden;

  & > span {
    width: 100%;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`;

const MessageButton = styled(IconButton)`
  font-size: 16px !important;
  padding: 0 !important;
  justify-self: flex-end;
  flex-shrink: 0;
  margin-left: 4px;
`;

const EntryTimestamp = styled.span`
  font-weight: bold;
`;

interface EntryLevelProps {
  level: number;
}

const EntryLevel = styled.span<EntryLevelProps>`
  font-weight: bold;
  margin-left: 12px;

  
  ${(props) =>
    props.level === 0 &&
    css`
      color: #2e6b90;
    `}
      
  ${(props) =>
    props.level === 1 &&
    css`
      color: #58be58;
    `}

    ${(props) =>
      props.level === 2 &&
      css`
        color: #fbc02d;
      `}

    ${(props) =>
      props.level === 3 &&
      css`
        color: #e44c4d;
      `}
`;

const EntryService = styled.span`
  margin-left: 12px;
`;

const EntryPublic = styled.span`
  margin-left: 12px;
  font-style: italic;
`;

const EntryDevice = styled.span`
  margin-left: 12px;
`;

const FilterContainer = styled.div`
  margin: 4px 0;

  form {
    display: flex;
    flex-direction: column;
    transform: scale(0.85);

    & > div {
      display: flex;
      & > div {
        margin-right: 4px;
      }
    }
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface ActivityEntryProps {
  item: any;
  style: any;
  setShowMessage: (m: string) => void;
}

const ActivityEntry: React.FC<ActivityEntryProps> = React.memo(({item, style, setShowMessage}) => {
  const {t} = useTranslation();

  const resolveLevel = (level: number) => {
    switch (level) {
      case 0:
        return "DBG";
      case 1:
        return "INF";
      case 2:
        return "WRN";
      case 3:
        return "ERR";
    }

    return "NA";
  };

  return (
    <EntryContainer style={style}>
      <div>
        <EntryContainerHead>
          <EntryTimestamp>{dayjs(item.timestamp).format(resolveUserDateFormat())}</EntryTimestamp>
          <EntryLevel level={item.level}>{resolveLevel(item.level)}</EntryLevel>
          <EntryService>{item.service}</EntryService>
          <EntryPublic> {!item.is_public ? t("nicht öffentlich") : ""} </EntryPublic>

          {item.device && (
            <EntryDevice>
              {t("Gerät")}: {item.device.simiccid};{item.device.imei} (
              {item.device.hardware.key.toUpperCase()}, {item.device.hardware.name})
            </EntryDevice>
          )}
        </EntryContainerHead>
        <EntryContainerTags>
          <span>{item.tags.join(", ")}</span>
        </EntryContainerTags>
        <EntryContainerBody title={item.message}>
          <span>{item.message}</span>
          <MessageButton aria-label={t("Text")} onClick={() => setShowMessage(item.message)}>
            <i className="icon-ico_document" />
          </MessageButton>
        </EntryContainerBody>
      </div>
    </EntryContainer>
  );
});

// -----------------------------------------------------------------------------------------------------------
interface AutoSubmitButtonProps {
  label: string;
  interval: number;
  submit: any;
  isLoading?: boolean;
}

let currentTimeout: any;

const AutoSubmitButton: React.FC<AutoSubmitButtonProps> = React.memo(
  ({label, interval, submit, isLoading}) => {
    const [delta, setDelta] = useState(interval);

    useEffect(() => {
      const t = () => {
        setDelta((d) => {
          let newD = d - 1;
          if (newD === -1) {
            newD = interval;
          }

          return newD;
        });

        if (!isLoading) {
          currentTimeout = setTimeout(t, 1000);
        } else {
          clearTimeout(currentTimeout);
          currentTimeout = null;
        }
      };

      if (!isLoading) {
        setDelta(interval);
        currentTimeout = setTimeout(t, 1000);
      } else {
        clearTimeout(currentTimeout);
        currentTimeout = null;
      }

      return () => {
        if (currentTimeout) {
          clearTimeout(currentTimeout);
        }
      };
    }, [interval, isLoading]);

    useEffect(() => {
      if (delta === 0) {
        submit();
        setDelta(interval);
      }
    }, [delta, submit, interval]);

    // render
    return (
      <Button autoWidth type="button" onClick={submit}>
        {label} ({delta}s)
      </Button>
    );
  }
);

// -----------------------------------------------------------------------------------------------------------
interface ActivityLogProps {
  assetID: string;
  setLoading: (loading: boolean) => void;
  headline?: string;
  maxListHeight?: string;
}

const ActivityLog: React.FC<ActivityLogProps> = React.memo(
  ({headline, assetID, setLoading, maxListHeight}) => {
    const {t} = useTranslation();

    // filter
    const filterForm = useFormik<any>({
      initialValues: {
        level: ["0", "1", "2", "3"],
        from: dayjs()
          .subtract(2, "day")
          .toDate(),
        to: new Date(),
        tag: "",
      },
      onSubmit: (values) => {
        const load = async () => {
          setLoading(true);

          const [{data}, err] = await httpClient.get(
            `/management/assets/log/${assetID}?from=${values.from.toISOString()}&to=${values.to.toISOString()}&tag=${
              values.tag
            }&level=${values.level.join(",")}`
          );
          if (err !== null) {
            setError(
              err.response?.data?.message ? err.response?.data?.message : "Daten konnten nicht geladen werden"
            );
            setData([]);
            filterForm.setSubmitting(false);
            setLoading(false);
            console.error(err);
            return;
          }

          filterForm.setSubmitting(false);
          setLoading(false);
          setError("");
          setData(data);
        };

        load();
      },
    });

    const formSubmit = filterForm.submitForm;
    const submit = useCallback(() => {
      const now = new Date();

      if (filterForm.values.to.getTime) {
        const toVal = filterForm.values.to.getTime();

        if (!isNaN(toVal)) {
          const delta = now.getTime() - toVal;
          if (delta < 120000) {
            filterForm.setFieldValue("to", new Date());
          }
        }
      }

      formSubmit();
    }, [filterForm, formSubmit]);

    // message
    const [showMessage, setShowMessage] = useState("");

    // load
    const [data, setData] = useState<any>([]);
    const [error, setError] = useState("");

    useEffect(() => {
      filterForm.submitForm();

      const submit = () => {
        filterForm.submitForm();
      };

      window.addEventListener("dkv-assetlog-reload", submit);
      return () => {
        window.removeEventListener("dkv-assetlog-reload", submit);
      };

      // eslint-disable-next-line
    }, []);

    const ActivityListItem = ({index, style}: ListChildComponentProps) => {
      const item = data[index];
      return <ActivityEntry item={item} style={style} setShowMessage={setShowMessage} />;
    };

    // render
    const dateFormat = resolveUserDateFormat();

    return (
      <WindowContentContainer minWidth="850px" full>
        {headline && (
          <WindowHeadline padding>
            <h1>{headline}</h1>
          </WindowHeadline>
        )}

        <FilterContainer>
          <form onSubmit={filterForm.handleSubmit}>
            <div>
              <MuiPickersUtilsProvider utils={DayjsUtils} locale={resolveUserDateLocale()}>
                <KeyboardDateTimePicker
                  disableToolbar
                  name="from"
                  label={t("Von")}
                  variant="inline"
                  format={dateFormat}
                  margin="none"
                  value={filterForm.values.from}
                  onChange={(d: any) => filterForm.setFieldValue("from", d)}
                  TextFieldComponent={TextField as any}
                  invalidDateMessage={t("Ungültiges Datum")}
                  keyboardIcon={<i className="icon-ico_cal-week" />}
                  PopoverProps={{
                    className: "dkv-datetimepicker",
                  }}
                  inputVariant="outlined"
                />
                <KeyboardDateTimePicker
                  disableToolbar
                  name="to"
                  label={t("Bis")}
                  variant="inline"
                  format={dateFormat}
                  margin="none"
                  value={filterForm.values.to}
                  onChange={(d: any) => filterForm.setFieldValue("to", d)}
                  TextFieldComponent={TextField as any}
                  invalidDateMessage={t("Ungültiges Datum")}
                  keyboardIcon={<i className="icon-ico_cal-week" />}
                  PopoverProps={{
                    className: "dkv-datetimepicker",
                  }}
                  inputVariant="outlined"
                />
              </MuiPickersUtilsProvider>
            </div>
            <div style={{marginTop: 12}}>
              <Select
                name="level"
                value={filterForm.values.level}
                onChange={filterForm.handleChange}
                label={t("Level")}
                style={{minWidth: 200}}
                multiple
                renderValue={(selected: any) => {
                  const txt: string[] = [];
                  selected.forEach((s: string) => {
                    switch (s) {
                      case "0":
                        txt.push("DBG");
                        break;
                      case "1":
                        txt.push("INF");
                        break;
                      case "2":
                        txt.push("WRN");
                        break;
                      case "3":
                        txt.push("ERR");
                        break;
                    }
                  });

                  return txt.join(", ");
                }}
              >
                <MenuItem value="0" className="dkv-select-checkbox-menuitem">
                  <Checkbox checked={filterForm.values.level.indexOf("0") > -1} />
                  <ListItemText primary={"Debug"} />
                </MenuItem>
                <MenuItem value="1" className="dkv-select-checkbox-menuitem">
                  <Checkbox checked={filterForm.values.level.indexOf("1") > -1} />
                  <ListItemText primary={"Info"} />
                </MenuItem>
                <MenuItem value="2" className="dkv-select-checkbox-menuitem">
                  <Checkbox checked={filterForm.values.level.indexOf("2") > -1} />
                  <ListItemText primary={"Warning"} />
                </MenuItem>
                <MenuItem value="3" className="dkv-select-checkbox-menuitem">
                  <Checkbox checked={filterForm.values.level.indexOf("3") > -1} />
                  <ListItemText primary={"Error"} />
                </MenuItem>
              </Select>

              <TextField
                name="tag"
                value={filterForm.values.tag}
                label={t("Tag Filter")}
                onChange={filterForm.handleChange}
                variant="outlined"
              />
            </div>
            <div style={{marginTop: 8, justifyContent: "flex-end"}}>
              <AutoSubmitButton
                label={t("Abfrage")}
                interval={30}
                submit={submit}
                isLoading={filterForm.isSubmitting}
              />
            </div>

            <button type="submit" hidden />
          </form>
        </FilterContainer>

        {error && <Message style={{margin: 24}} text={t(error)} error />}

        <ListWrapper maxListHeight={maxListHeight}>
          <AutoSizer>
            {({height, width}) => (
              <List height={height} width={width} itemCount={data.length} itemSize={85}>
                {ActivityListItem}
              </List>
            )}
          </AutoSizer>
        </ListWrapper>

        {showMessage && (
          <Window onClose={() => setShowMessage("")}>
            <div style={{minWidth: 600, minHeight: 650, padding: 12, display: "flex"}}>
              <textarea
                defaultValue={showMessage}
                style={{width: "100%", minHeight: 400, marginRight: 32}}
                readOnly
              />
            </div>
          </Window>
        )}
      </WindowContentContainer>
    );
  }
);

export default ActivityLog;
