import React, {useEffect, useState} from "react";
import httpClient from "services/http";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components";
import {Asset} from "app/modules/live/state/assets/reducer";
import {TextField, Loading} from "dkv-live-frontend-ui";
// import {Close as CloseIcon} from "styled-icons/material/Close";
import {loadAssets} from "app/modules/live/state/assets/actions";
import {useDispatch} from "react-redux";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Window from "app/components/Window";
import AssignWarning from "./AssignWarning";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------

const SplitTab = styled.div`
  display: flex;
  flex-grow: 1;
`;

const SectionContainer = styled.div`
  flex-basis: 50%;
  padding: 10px;
`;

const SectionTitle = styled.div`
  position: relative;
  color: var(--dkv-grey_70);
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 20px;
`;

const TitleIcon = styled.div`
  font-size: 24px;
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
`;

const TitleText = styled.div`
  margin-left: 30px;
`;

const ListEntryContainer = styled.div`
  display: flex;
  border-radius: 2px;
  border: solid 1px #98bace;
  background-color: #e9f3f9;
  margin-bottom: 5px;
  justify-content: space-between;
  padding: 10px;
`;

const ListEntryInfo = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: column;
`;

// const ListEntryRemoveIconContainer = styled.div`
//   display: flex;
//   margin-left: 10px;
// `;

// const ListEntryRemoveRemoveIcon = styled(CloseIcon)`
//   margin: auto;
// `;

const ListEntryTitle = styled.div`
  display: flex;
  font-weight: bold;
  margin: auto 0;
`;

const ListEntryDetail = styled.div`
  margin-top: 0.5em;
  display: flex;
`;

const DeviceDetailFeatures = styled.div`
  display: flex;
`;

const AutocompleteEntry = styled.div`
  display: flex;
  padding: 5px;
  flex-grow: 1;
`;

interface DeviceDetailOnlineStatusProps {
  online: boolean;
}
const DeviceDetailOnlineStatus = styled.div<DeviceDetailOnlineStatusProps>`
  flex-grow: 1;
  text-align: right;
  ${(props) =>
    props.online
      ? css`
          color: var(--dkv-highlight-ok-color);
        `
      : css`
          color: var(--dkv-highlight-error-color);
        `}
`;

// -----------------------------------------------------------------------------------------------------------

interface DevicesTabProps {
  asset?: Asset;
}

const DevicesTab: React.FC<DevicesTabProps> = ({asset}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // loading
  const [loading, setLoading] = useState<boolean>(false);

  // devices
  const [deviceToConnect, setDeviceToConnect] = useState<any | null>(null);
  const [devices, setDevices] = useState(new Array<any>());
  const [assetDevices, setAssetDevices] = useState(new Array<any>());
  const [assetDeviceIDs, setAssetDeviceIDs] = useState(new Array<string>());
  // hardware
  const [hardwareMap, setHardwareMap] = useState<Map<string, string> | null>(null);

  useEffect(() => {
    //retrieve the hardware definitions
    const loadHardware = async () => {
      const [{data}, err] = await httpClient.get("/management/hardware");
      if (err !== null) {
        // setError("Geräte-Laden fehlgeschlagen");
        return;
      }
      const hardwareMap = data.reduce((map: Map<string, string>, entry: any) => {
        map.set(entry.id, entry.name);
        return map;
      }, new Map<string, string>());
      setHardwareMap(hardwareMap);
    };
    loadHardware();

    //retrieve the devices for this division
    const loadDevices = async () => {
      const [{data}, err] = await httpClient.get("/management/devices");
      if (err !== null) {
        // setError("Geräte-Laden fehlgeschlagen");
        return;
      }
      setDevices(data);
    };
    loadDevices();
  }, []);

  useEffect(() => {
    if (asset) {
      //retrieve devices from asset information
      const loadAssetDevices = async () => {
        setLoading(true);
        const [{data}, err] = await httpClient.get("/management/devices/findAllForAsset/" + asset.id);
        if (err !== null) {
          // setError("Geräte-Laden fehlgeschlagen");
          setLoading(false);
          return;
        }

        setLoading(false);
        setAssetDevices(data);
        setAssetDeviceIDs(data.map((d: {id: string}) => d.id));
      };
      loadAssetDevices();
    }
  }, [asset]);

  const connectDevice = async () => {
    if (asset && deviceToConnect) {
      setLoading(true);
      const [res, err] = await httpClient.put(
        "/management/devices/" + deviceToConnect.id + "/deviceassignment/" + asset.id
      );
      if (err !== null) {
        // setError("Geräte-Laden fehlgeschlagen");
        setLoading(false);
        return;
      }
      console.info("Successfully assigned device to asset", res);
      dispatch(loadAssets());
      setLoading(false);
      setDeviceToConnect(null);
    }
  };

  const onCloseWarning = () => {
    setDeviceToConnect(null);
  };

  const renderDevice = (device: any) => {
    return (
      <ListEntryInfo>
        <ListEntryTitle>
          {(hardwareMap && hardwareMap.has(device.hardware_id)
            ? hardwareMap.get(device.hardware_id)
            : t("Unbekanntes Gerät")) +
            " " +
            device.simiccid +
            "-" +
            device.imei}
        </ListEntryTitle>
        <ListEntryDetail>
          <DeviceDetailFeatures>
            {device.data_category_support.gnss && (
              <div className="icon-ico_route-active" style={{fontSize: "24px"}} />
            )}
            {device.data_category_support.canbus && (
              <div className="icon-ico_tacho" style={{fontSize: "24px"}} />
            )}
            {device.data_category_support.rdl && (
              <div className="icon-ico_tachograph" style={{fontSize: "24px"}} />
            )}
            {device.data_category_support.ebs && (
              <div className="icon-ico_break" style={{fontSize: "24px"}} />
            )}
            {device.data_category_support.thermo && (
              <div className="icon-ico_cooling" style={{fontSize: "24px"}} />
            )}
          </DeviceDetailFeatures>
          <DeviceDetailOnlineStatus online={device.live_status.online}>
            {t(device.live_status.online ? "Online" : "Offline")}
          </DeviceDetailOnlineStatus>
        </ListEntryDetail>
      </ListEntryInfo>
    );
  };

  return (
    <SplitTab>
      <Loading inprogress={loading} zIndex={5001} />
      <SectionContainer>
        <SectionTitle>
          <TitleIcon className="icon-ico_tachograph" />
          <TitleText>{t("Verbundene Geräte") + ":"}</TitleText>
        </SectionTitle>
        <div>
          {loading
            ? t("Geräte werden geladen...")
            : assetDevices && assetDevices.length && hardwareMap && hardwareMap.size
            ? assetDevices.map((device) => {
                return (
                  <ListEntryContainer key={device.id}>
                    {renderDevice(device)}
                    {/* <ListEntryRemoveIconContainer>
                          <ListEntryRemoveRemoveIcon size={24} />
                        </ListEntryRemoveIconContainer> */}
                  </ListEntryContainer>
                );
              })
            : t("Keine Geräte verbunden!")}
        </div>
      </SectionContainer>
      {hasPermission("management:AssignDevices") && (
        <SectionContainer
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <SectionTitle>{t("Gerät verbinden") + ":"}</SectionTitle>
          <Autocomplete
            value={deviceToConnect}
            onChange={(_: any, newValue: any | null) => {
              setDeviceToConnect(newValue);
            }}
            autoHighlight
            options={
              assetDeviceIDs && assetDeviceIDs.length
                ? devices.filter((d: {id: string}) => !assetDeviceIDs.includes(d.id))
                : devices
            }
            getOptionLabel={(device: any) =>
              (hardwareMap && hardwareMap.has(device.hardware_id)
                ? hardwareMap.get(device.hardware_id)
                : t("Unbekanntes Gerät")) +
              " " +
              device.simiccid +
              "-" +
              device.imei
            }
            renderOption={(device) => (
              <AutocompleteEntry key={device.id}>{renderDevice(device)}</AutocompleteEntry>
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Gerät"
                variant="outlined"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password",
                }}
              />
            )}
            placeholder="DKV.live Box 12345-123456789"
            disabled={loading}
            clearText={t("Leeren")}
            closeText={t("Schließen")}
            loadingText={t("Lädt...")}
            noOptionsText={t("Keine Optionen")}
            openText={t("Öffnen")}
          />
        </SectionContainer>
      )}

      {deviceToConnect && (
        <Window onClose={onCloseWarning} headline={t("Sind Sie sicher?")}>
          {(props) => <AssignWarning {...props} onAccept={connectDevice} />}
        </Window>
      )}
    </SplitTab>
  );
};

export default DevicesTab;
