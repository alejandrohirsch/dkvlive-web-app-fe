import React, {useEffect, useState, useCallback, useRef} from "react";
import {useTranslation} from "react-i18next";
import Page from "app/components/Page";
import {useDispatch, useSelector} from "react-redux";
import {assetsListSelector} from "../../state/assets/selectors";
import {loadAssetsIfNeeded} from "../../state/assets/actions";
import {Button, TextField} from "dkv-live-frontend-ui";
import {Tooltip} from "@material-ui/core";
import DataTable, {
  TablePageContent,
  TableStickyHeader,
  TableStickyFooter,
  TableContainer,
  useColumns,
  bodyField,
  TableFooterActions,
  ToolbarButton,
  ToolbarButtonIcon,
  TableFooterRightActions,
  TableSelectionCountInfo,
} from "app/components/DataTable";
import {Asset} from "../../state/assets/reducer";
import AssetStatus from "../map/components/AssetStatus";
import dayjs from "dayjs";
import {ErrorCircle as ErrorIcon} from "@styled-icons/boxicons-solid/ErrorCircle";
import {InfoCircle as InfoIcon} from "@styled-icons/boxicons-solid/InfoCircle";
import styled from "styled-components";
import Window, {
  WindowContentProps,
  WindowContentContainer,
  WindowHeadline,
  WindowContent,
} from "app/components/Window";
import AssetDetails from "./components/AssetDetails";
import ActivityLog from "./components/ActivityLog";
import {
  resolveUserDayFormat,
  hasPermission,
  resolveUserDateLocale,
  resolveUserDateFormat,
} from "app/modules/login/state/login/selectors";
import httpClient from "services/http";
import {Growl} from "primereact/growl";
import {useFormik} from "formik";
import {MuiPickersUtilsProvider, KeyboardDateTimePicker} from "@material-ui/pickers";
import DayjsUtils from "@date-io/dayjs";

// -----------------------------------------------------------------------------------------------------------
const cmdRDLAuth = async (assetID: string) => {
  const [{data}, err] = await httpClient.post(`/ingestion/rdl/auth/trigger/asset/${assetID}`);
  return [data, err];
};

const cmdRDLDriver = async (assetID: string, number: string) => {
  const [{data}, err] = await httpClient.post(`/ingestion/rdl/download/driver/asset/${assetID}/${number}`);
  return [data, err];
};

const cmdRDLMassStorage = async (assetID: string, from?: Date, to?: Date) => {
  const params = [];
  if (from) {
    const fromEncoded = encodeURIComponent(from.toISOString());
    params.push(`from=${fromEncoded}`);
  }

  if (to) {
    const toEncoded = encodeURIComponent(to.toISOString());
    params.push(`to=${toEncoded}`);
  }

  const [{data}, err] = await httpClient.post(
    `/ingestion/rdl/download/mass/asset/${assetID}?${params.join("&")}`
  );
  return [data, err];
};

// -----------------------------------------------------------------------------------------------------------
// const TableContainer = styled.div`
//   height: 100%;
//   overflow: hidden;
// `;

const FeatureIconsContainer = styled.div`
  width: 100%;
  display: flex;
`;

const AssetTagList = styled.ul`
  display: flex;
  flex-flow: row;

  li {
    border-radius: 4px;
    background-color: #2e6b90;
    color: #ffffff;
    padding: 2px 5px;
    margin-left: 4px;
    font-size: 12px;
    white-space: nowrap;
  }
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 12px;
`;

// -----------------------------------------------------------------------------------------------------------
const statusTemplate = (asset: Asset) => {
  return (
    <div style={{display: "flex"}}>
      <AssetStatus size={24} asset={asset} />
      <div style={{margin: "auto", paddingLeft: 4}}>{(asset.calc.speed ?? 0) + "km/h"}</div>
    </div>
  );
};

const featuresTemplate = (asset: Asset, t: (k: any) => string) => {
  // const featureCountPerDevice = Object.values(asset.main_device_by_category).reduce((map, value) => {
  //   if (emptyId !== value) {
  //     map[value] = (map[value] ?? 0) + 1;
  //   }
  //   return map;
  // }, {});
  return (
    <FeatureIconsContainer>
      {/* <div style={{marginRight: "5px"}}>{Object.keys(featureCountPerDevice).length}</div> */}
      {asset.data_category_support.gnss && (
        <Tooltip title={t("Ortung verfügbar") as string}>
          <div className="icon-ico_route-active" style={{fontSize: "24px"}} />
        </Tooltip>
      )}
      {asset.data_category_support.canbus && (
        <Tooltip title={t("CAN-Bus Informationen verfügbar") as string}>
          <div className="icon-ico_tacho" style={{fontSize: "24px"}} />
        </Tooltip>
      )}
      {asset.data_category_support.rdl && (
        <Tooltip title={t("Remote-Download-fähig") as string}>
          <div className="icon-ico_tachograph" style={{fontSize: "24px"}} />
        </Tooltip>
      )}
      {asset.data_category_support.ebs && (
        <Tooltip title={t("Elektronisches Bremssystem") as string}>
          <div className="icon-ico_break" style={{fontSize: "24px"}} />
        </Tooltip>
      )}
      {asset.data_category_support.thermo && (
        <Tooltip title={t("Temperaturdaten verfügbar") as string}>
          <div className="icon-ico_cooling" style={{fontSize: "24px"}} />
        </Tooltip>
      )}
    </FeatureIconsContainer>
  );
};

const nextServiceTemplate = (asset: Asset, inspectionTypeMap: any, t: (k: any) => string) => {
  const dates =
    asset.inspection_dates && asset.inspection_dates.length
      ? asset.inspection_dates
          .filter((d: any) => !d.done && inspectionTypeMap[d.inspection_type_id])
          .sort((d1: any, d2: any) => (dayjs(d1.due_date).isBefore(dayjs(d2.due_date)) ? -1 : 1))
      : [];
  return (
    <div style={{display: "flex"}}>
      <div style={{margin: "auto", marginRight: "5px"}}>
        {dates && dates.length ? dayjs(dates[0].due_date).format(resolveUserDayFormat()) : ""}
      </div>
      {Boolean(dates && dates.length) && (
        <Tooltip title={t(inspectionTypeMap[dates[0].inspection_type_id].name) as string}>
          <div
            className="icon-Ico_info_outline"
            style={{fontSize: "24px", margin: "auto", marginLeft: "5px"}}
          />
        </Tooltip>
      )}
    </div>
  );
};

const servicecardTemplate = (asset: Asset) => {
  return (
    <div style={{display: "flex"}}>
      <div style={{margin: "auto", marginRight: "5px"}}>{asset.service_cards?.length ?? 0}</div>
      <div className="icon-ico_creditcard" style={{fontSize: "24px", margin: "auto", marginLeft: "5px"}} />
    </div>
  );
};

const generateTagColor = (name: string) => {
  const colorPalette = [
    "#b3e7e8",
    "#b6a5e0",
    "#a8c2e6",
    "#ffa797",
    "#dcd6e8",
    "#b5e5a3",
    "#e4d2ca",
    "#ccdbe4",
    "#e3f297",
    "#97cee6",
    "#a5a5d3",
    "#f1cea1",
  ];
  const hash = name.split("").reduce(function(a, b) {
    a = (a << 5) - a + b.charCodeAt(0);
    return a & a;
  }, 0);
  return colorPalette[hash % colorPalette.length];
};

const tagTemplate = (asset: Asset) => {
  return (
    <div style={{position: "relative"}}>
      <AssetTagList>
        {asset.tags
          ? asset.tags.map((tag) => (
              <li
                key={tag.id}
                style={{
                  backgroundColor: generateTagColor(tag.name),
                  color: "#363636",
                }}
              >
                {tag.name}
              </li>
            ))
          : null}
      </AssetTagList>
    </div>
  );
};

// -----------------------------------------------------------------------------------------------------------
const List: React.FC = () => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // load assets
  const assets = useSelector(assetsListSelector);

  useEffect(() => {
    dispatch(loadAssetsIfNeeded());
  }, [dispatch]);

  // load types
  const [inspectionTypeMap, setInspectionTypeMap] = useState({});
  const [assetTypeMap, setAssetTypeMap] = useState({});
  useEffect(() => {
    //retrieve the inspection typea
    const loadInspectionTypes = async () => {
      const [{data}, err] = await httpClient.get("/management/inspections/types");
      if (err !== null) {
        return;
      }
      setInspectionTypeMap(
        data.reduce((obj: any, it: any) => {
          obj[it.id] = it;
          return obj;
        }, {})
      );
    };
    loadInspectionTypes();

    //retrieve the asset types
    const loadAssetTypes = async () => {
      const [{data}, err] = await httpClient.get("/management/assets/types");
      if (err !== null) {
        return;
      }
      setAssetTypeMap(
        data.reduce((obj: any, it: any) => {
          obj[it.id] = it;
          return obj;
        }, {})
      );
    };
    loadAssetTypes();
  }, []);

  // columns
  const [columns, setColumns] = useState<Array<any>>([]);
  const visibleColumns = useColumns(columns);
  useEffect(() => {
    const listColumns = [
      {field: "name", header: "Name", width: 150, sortable: true},
      {field: "license_plate", header: "Kennzeichen", width: 120, sortable: true},
      {field: "vin", header: "Fahrgestellnummer", width: 160, sortable: true},
      {field: "tags", header: "Tags", body: tagTemplate, width: 125, sortable: true},
      {
        field: "_status",
        header: "Status",
        width: 120,
        sortable: true,
        bodyClassName: "full-height",
        body: statusTemplate,
      },
      {
        field: "gnssts",
        header: "Ortung",
        width: 180,
        body: bodyField((a: Asset) => (
          <Tooltip title={a.gnss._ts ?? ""}>
            <span>
              {a.gnss._ts?.startsWith(dayjs().format(resolveUserDayFormat()))
                ? a.gnss._ts?.substring(11)
                : a.gnss._ts}
            </span>
          </Tooltip>
        )),
      },
      {
        field: "asset_type_id",
        header: "Art",
        body: (a: Asset) => (
          <Tooltip
            title={
              assetTypeMap && assetTypeMap[a.asset_type_id] ? `${t(assetTypeMap[a.asset_type_id].name)}` : ""
            }
          >
            <div
              className={assetTypeMap[a.asset_type_id]?.icon_class_name ?? undefined}
              style={{fontSize: "24px", textAlign: "center"}}
            />
          </Tooltip>
        ),
        width: 75,
        sortable: true,
      },
      {
        field: "service_cards",
        header: "Tankkarten",
        body: servicecardTemplate,
        width: 90,
        sortable: true,
      },
      {
        field: "devices",
        header: "Geräte",
        width: 120,
        body: (asset: Asset) => featuresTemplate(asset, t),
        sortable: true,
      },
      {
        field: "nextservice",
        header: "Nächster Service",
        body: (asset: Asset) => nextServiceTemplate(asset, inspectionTypeMap, t),
        width: 150,
        sortable: true,
      },
    ];
    setColumns(listColumns);
  }, [inspectionTypeMap, assetTypeMap, t]);

  // const tmpAssets: Asset[] = [];
  // for (let i = 0; i < 50; i++) {
  //   const ass = {...assets[0]};
  //   ass.id = i.toString();

  //   tmpAssets.push(ass);
  // }

  // actions
  // - register
  // const [showRegisterWindow, setShowRegisterWindow] = useState(false);
  // const onCloseRegisterWindow = useCallback(() => setShowRegisterWindow(false), [setShowRegisterWindow]);
  // const openRegisterWindow = () => setShowRegisterWindow(true);
  // - details
  const [showDetailWindow, setShowDetailWindow] = useState<Asset | null>(null);
  const onCloseDetailWindow = useCallback(() => setShowDetailWindow(null), [setShowDetailWindow]);
  const openDetailWindow = useCallback((e: any) => setShowDetailWindow(e.data as Asset), [
    setShowDetailWindow,
  ]);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  const typeCount = {incomplete: 0, new: 0, default: 0};
  const typedAssets = assets
    .map((asset) => {
      const typedAsset = {
        ...asset,
        _type:
          !asset.license_plate || !asset.license_plate.length || !asset.vin || !asset.vin.length
            ? "incomplete"
            : asset.created_at === asset.modified_at
            ? "new"
            : "default",
      };
      typeCount[typedAsset._type]++;
      return typedAsset;
    })
    .sort((assetA, assetB) => {
      const calcValue = (assetType?: string) =>
        assetType === "incomplete" ? 3 : assetType === "new" ? 2 : 1;
      return calcValue(assetB._type) - calcValue(assetA._type);
    });

  // actions
  // - register
  // const [showRegisterWindow, setShowRegisterWindow] = useState(false);
  // const onCloseRegisterWindow = useCallback(() => setShowRegisterWindow(false), [setShowRegisterWindow]);
  // const openRegisterWindow = () => setShowRegisterWindow(true);

  const [showActivityWindow, setShowActivityWindow] = useState<any>(null);
  const onCloseActivityWindow = useCallback(() => setShowActivityWindow(null), [setShowActivityWindow]);
  const openActivityWindow = () => setShowActivityWindow(selected && selected.length ? selected[0] : null);

  // commands
  const growlRef = useRef<any>();

  const doCommand = (cmd: string, label: string, params?: any) => {
    const runCommand = async () => {
      const asset = selected && selected.length ? selected[0] : null;
      if (!asset) {
        return;
      }

      // setLoading(true);

      let err: any = "Befehl nicht gefunden";
      switch (cmd) {
        case "rdlauth":
          [, err] = await cmdRDLAuth(asset.id);
          break;
        case "rdlmass":
          [, err] = await cmdRDLMassStorage(asset.id);
          break;
        case "rdlmassrange":
          [, err] = await cmdRDLMassStorage(asset.id, params.from, params.to);
          break;
        case "rdlcard1":
          [, err] = await cmdRDLDriver(asset.id, "1");
          break;
        case "rdlcard2":
          [, err] = await cmdRDLDriver(asset.id, "2");
          break;
      }

      if (err !== null) {
        growlRef.current.show({
          life: 5000,
          closable: false,
          severity: "error",
          summary: t(label),
          detail: err.response?.data?.message
            ? err.response?.data?.message
            : typeof err === "string"
            ? err
            : t("Befehl konnte nicht ausgeführt werden"),
        });

        // setLoading(false);
        return;
      }

      setTimeout(() => {
        growlRef.current.show({
          life: 5000,
          closable: false,
          severity: "success",
          summary: t(label),
          detail: t("Befehl erfolgreich ausgelöst"),
        });

        // setLoading(false);
        window.dispatchEvent(new Event("dkv-assetlog-reload"));
      }, 1000);
    };

    runCommand();
  };

  const [showMassStorageRange, setShowMassStorageRange] = useState<any>(false);
  const openMassStorageRange = () => {
    setShowMassStorageRange(true);
  };

  const onCloseMassStorageRange = () => {
    setShowMassStorageRange(false);
  };

  // render
  return (
    <Page id="list">
      <TablePageContent>
        <TableStickyHeader>
          <div>
            <h1>
              {assets.length}{" "}
              {t(assets.length === 1 ? "Fahrzeug mit DKV-Live Box" : "Fahrzeuge mit DKV-Live Box")}
            </h1>
            {Boolean(typeCount.incomplete) && (
              <h2 style={{color: "var(--dkv-highlight-warning-color)"}}>
                {typeCount.incomplete +
                  " " +
                  t(
                    typeCount.incomplete === 1
                      ? "Fahrzeug mit unvollständigen Informationen"
                      : "Fahrzeuge mit unvollständigen Informationen"
                  )}
              </h2>
            )}
            {Boolean(typeCount.new) && (
              <h2 style={{color: "var(--dkv-highlight-ok-color)"}}>
                {typeCount.new + " " + t(typeCount.new === 1 ? "neues Fahrzeug" : "neue Fahrzeuge")}
              </h2>
            )}
          </div>

          {/* @todo commingsoon */}
          {/* <SearchTextField
            name="search"
            type="text"
            required
            label={t("Suche")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          /> */}

          {/* <ToolbarButton onClick={openRegisterWindow}>
            <ToolbarButtonIcon>
              <span className="icon-ico_fleet" />
            </ToolbarButtonIcon>
            {t("Registrieren")}
          </ToolbarButton> */}
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={typedAssets}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            onRowDoubleClick={openDetailWindow}
            rowGroupMode={typeCount.incomplete + typeCount.new ? "subheader" : undefined}
            groupField="_type"
            rowGroupHeaderTemplate={(data: any) => {
              return (
                <div
                  style={{
                    fontWeight: "bold",
                    color:
                      data._type === "incomplete"
                        ? "var(--dkv-highlight-warning-color)"
                        : data._type === "new"
                        ? "var(--dkv-highlight-ok-color)"
                        : "var(--dkv-dkv_blue)",
                  }}
                >
                  {data._type === "incomplete" || data._type === "new" ? (
                    <ErrorIcon size={24} style={{marginRight: "1em"}} />
                  ) : (
                    <InfoIcon size={24} style={{marginRight: "1em"}} />
                  )}
                  {t(data._type === "incomplete" ? "Unvollständig" : data._type === "new" ? "Neu" : "Aktiv")}
                </div>
              );
            }}
            rowClassName={(rowData: any) => {
              return {
                "dkv-row-new-asset": rowData._type === "new",
                "dkv-row-missing-information": rowData._type === "incomplete",
              };
            }}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Fahrzeug ausgewählt" : "Fahrzeuge ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>

              <TableFooterActions>
                {selected.length === 1 && selected[0].data_category_support.rdl ? (
                  <>
                    {hasPermission("rdl:TriggerAuth") && (
                      <ToolbarButton onClick={() => doCommand("rdlauth", "RDL Auth")}>
                        <ToolbarButtonIcon>
                          <span className="icon-ico_tools" />
                        </ToolbarButtonIcon>
                        {t("RDL Auth")}
                      </ToolbarButton>
                    )}

                    {hasPermission("rdl:Download") && (
                      <>
                        <ToolbarButton onClick={() => doCommand("rdlmass", "RDL Massenspeicher")}>
                          <ToolbarButtonIcon>
                            <span className="icon-ico_tools" />
                          </ToolbarButtonIcon>
                          {t("RDL Massenspeicher")}
                        </ToolbarButton>

                        <ToolbarButton onClick={openMassStorageRange}>
                          <ToolbarButtonIcon>
                            <span className="icon-ico_tools" />
                          </ToolbarButtonIcon>
                          {t("RDL Massenspeicher Zeitraum")}
                        </ToolbarButton>

                        <ToolbarButton onClick={() => doCommand("rdlcard1", "RDL Fahrer 1")}>
                          <ToolbarButtonIcon>
                            <span className="icon-ico_tools" />
                          </ToolbarButtonIcon>
                          {t("RDL Fahrer 1")}
                        </ToolbarButton>

                        <ToolbarButton onClick={() => doCommand("rdlcard2", "RDL Fahrer 2")}>
                          <ToolbarButtonIcon>
                            <span className="icon-ico_tools" />
                          </ToolbarButtonIcon>
                          {t("RDL Fahrer 2")}
                        </ToolbarButton>
                      </>
                    )}
                  </>
                ) : null}

                {hasPermission("management:ListAssetsLog") && (
                  <ToolbarButton onClick={openActivityWindow} style={{marginLeft: 32}}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_activity" />
                    </ToolbarButtonIcon>
                    {t("Aktivität")}
                  </ToolbarButton>
                )}
              </TableFooterActions>
              <TableFooterRightActions />
            </>
          )}

          {/* <TableFooterRightActions>
            <Button autoWidth secondary ariaLabel={t("Gesamte Liste exportieren")}>
              {t("Gesamte Liste exportieren")}
            </Button>

            {selected.length > 0 && (
              <Button autoWidth ariaLabel={t("Auswahl als Liste exportieren")}>
                {t("Auswahl als Liste exportieren")}
              </Button>
            )}
          </TableFooterRightActions> */}
        </TableStickyFooter>
      </TablePageContent>

      {/* {showRegisterWindow && (
        <Window onClose={onCloseRegisterWindow} headline={t("Fahrzeug registrieren")}>
          {(props) => <RegisterForm {...props} />}
        </Window>
      )} */}

      <Growl ref={(el) => (growlRef.current = el)} baseZIndex={20000} />

      {showActivityWindow && (
        <Window
          onClose={onCloseActivityWindow}
          headline={`${t("Aktivität")} - ${showActivityWindow.name} / ${showActivityWindow.license_plate}`}
        >
          {(props) => <ActivityLog {...props} assetID={showActivityWindow.id} />}
        </Window>
      )}

      {showDetailWindow && (
        <Window onClose={onCloseDetailWindow} headline={t("Fahrzeug-Details")}>
          {(props) => <AssetDetails {...props} assetID={showDetailWindow.id} />}
        </Window>
      )}

      {showMassStorageRange && (
        <Window onClose={onCloseMassStorageRange} headline={t("Massenspeicher Zeitraum")}>
          {(props) => (
            <MassStorageRangePicker
              {...props}
              onSubmit={(data) => {
                setShowMassStorageRange(false);
                doCommand("rdlmassrange", "RDL Massenspeicher Zeitraum", data);
              }}
            />
          )}
        </Window>
      )}
    </Page>
  );
};

export default List;

// -----------------------------------------------------------------------------------------------------------
interface MassStorageRangePickerProps extends WindowContentProps {
  onSubmit: (data: any) => void;
}

const MassStorageRangePicker: React.FC<MassStorageRangePickerProps> = ({headline, onSubmit}) => {
  const {t} = useTranslation();

  const form = useFormik<any>({
    initialValues: {
      from: null,
      to: null,
    },
    onSubmit: (values) => {
      const from = values.from.toDate ? values.from.toDate() : values.from;
      const to = values.to.toDate ? values.to.toDate() : values.to;

      onSubmit({from, to});
    },
  });

  // render
  return (
    <WindowContentContainer full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <form onSubmit={form.handleSubmit}>
          <FormGrid>
            <MuiPickersUtilsProvider utils={DayjsUtils} locale={resolveUserDateLocale()}>
              <KeyboardDateTimePicker
                disableToolbar
                name="from"
                label={t("Von")}
                variant="inline"
                format={resolveUserDateFormat()}
                margin="none"
                value={form.values.from}
                onChange={(d: any) => form.setFieldValue("from", d)}
                TextFieldComponent={TextField as any}
                invalidDateMessage={t("Ungültiges Datum")}
                keyboardIcon={<i className="icon-ico_cal-week" />}
                PopoverProps={{
                  className: "dkv-datetimepicker",
                }}
                inputVariant="outlined"
                required
              />
              <KeyboardDateTimePicker
                disableToolbar
                name="to"
                label={t("Bis")}
                variant="inline"
                format={resolveUserDateFormat()}
                margin="none"
                value={form.values.to}
                onChange={(d: any) => form.setFieldValue("to", d)}
                TextFieldComponent={TextField as any}
                invalidDateMessage={t("Ungültiges Datum")}
                keyboardIcon={<i className="icon-ico_cal-week" />}
                PopoverProps={{
                  className: "dkv-datetimepicker",
                }}
                inputVariant="outlined"
                required
              />
            </MuiPickersUtilsProvider>
          </FormGrid>

          <div style={{marginTop: 12, justifyContent: "flex-end"}}>
            <Button type="submit">{t("Senden")}</Button>
          </div>
        </form>
      </WindowContent>
    </WindowContentContainer>
  );
};
