import React from "react";
import Page from "app/components/Page";
import {useRouteMatch, Switch, Route} from "react-router-dom";
import Selection from "./Selection";
import Status from "./Status";
import MassStorage from "./MassStorage";
import DriverCard from "./DriverCard";
import store from "app/state/store";
import {RDLModule} from "./state/module";

//-----------------------------------------------------------------------------------------------------------
store.addModule(RDLModule);

// -----------------------------------------------------------------------------------------------------------
const RemoteDownload: React.FC = () => {
  // route
  const {path} = useRouteMatch();

  // render
  return (
    <Page id="remote-download">
      <Switch>
        <Route exact path={`${path}`}>
          <Selection path={path} />
        </Route>
        <Route path={`${path}/status`}>
          <Status backPath={path} />
        </Route>
        <Route path={`${path}/massstorage`}>
          <MassStorage backPath={path} />
        </Route>
        <Route path={`${path}/drivercard`}>
          <DriverCard backPath={path} />
        </Route>
      </Switch>
    </Page>
  );
};

export default RemoteDownload;
