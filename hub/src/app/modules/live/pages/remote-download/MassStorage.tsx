import React, {useState, useEffect} from "react";
import {useTranslation} from "react-i18next";
import Page from "app/components/Page";
import {Button, Loading} from "dkv-live-frontend-ui";
import {Tooltip} from "@material-ui/core";
import DataTable, {
  TablePageContent,
  TableStickyHeader,
  TableStickyFooter,
  TableContainer,
  useColumns,
  TableSelectionCountInfo,
  TableFooterRightActions,
  bodyField,
} from "app/components/DataTable";
import {resolveUserDateFormat, hasPermission} from "app/modules/login/state/login/selectors";
import dayjs from "dayjs";
import {massStorageListSelector, massStorageListStateSelector} from "./state/massstorage/selectors";
import {loadMassStorageIfNeeded} from "./state/massstorage/actions";
import {useSelector, useDispatch} from "react-redux";
import styled, {css} from "styled-components";
import Window from "app/components/Window";
import DownloadRangePicker from "./components/DownloadRangePicker";
import MassStorageChart from "./components/MassStorageChart";
import fileDownload from "js-file-download";
import httpClient from "services/http";

// -----------------------------------------------------------------------------------------------------------
interface StautsFieldProps {
  ok?: boolean;
  error?: boolean;
}

const StautsField = styled.span<StautsFieldProps>`
  display: flex;
  align-items: center;
  font-weight: 500;

  i {
    font-size: 24px;
    margin-right: 4px;
  }

  ${(props) =>
    props.ok &&
    css`
      color: var(--dkv-highlight-ok-color);
    `}

  ${(props) =>
    props.error &&
    css`
      color: var(--dkv-carminepink);
    `}
`;

// -----------------------------------------------------------------------------------------------------------
const lockColumn = (t: any) => (rowData: any) => {
  const d = rowData.asset["lock_date"] ?? "";
  const lockDate = d && d !== "0001-01-01T00:00:00Z" ? dayjs(d).format(resolveUserDateFormat()) : "";

  if (!rowData.asset["lock_company"]) {
    return (
      <StautsField error>
        <i className="icon-ico_warning_full" />
        {t("Keine Sperre")}
      </StautsField>
    );
  }

  if (!rowData.asset["lock_valid"]) {
    return (
      <Tooltip title={`${t("Ungültige Sperre")}`} aria-label={`${t("Ungültige Sperre")}`} enterDelay={20}>
        <StautsField error>{rowData.asset["lock_company"]}</StautsField>
      </Tooltip>
    );
  }

  return (
    <Tooltip
      title={`${lockDate}; ${rowData.asset["lock_card"]}`}
      aria-label={`${lockDate}; ${rowData.asset["lock_card"]}`}
      enterDelay={20}
    >
      <span>{rowData.asset["lock_company"]}</span>
    </Tooltip>
  );
};

const dateColumn = (rowData: any, column: any) => {
  const d = dayjs(rowData[column.field]);

  return d.format(resolveUserDateFormat());
};

const statusColumn = (t: any) => (rowData: any) => {
  const s = parseInt(rowData.asset.download_status);

  switch (s) {
    case 0:
      return (
        <StautsField ok>
          <i className="icon-ico_check_status" />
          {t("Erfolgreich")}
        </StautsField>
      );
    case 1:
      return (
        <StautsField error>
          <i className="icon-ico_warning_full" />
          {t("Fehlgeschlagen")}
        </StautsField>
      );
  }

  return (
    <StautsField>
      <i className="icon-ico_minus" />
      {t("Unbekannt")}
    </StautsField>
  );
};

const lockDateField = (rowData: any) => {
  const d = rowData.asset["lock_expire"];
  if (!d || d === "0001-01-01T00:00:00Z") {
    return "";
  }

  const date = dayjs(d);

  const diff = date.diff(dayjs(), "w");
  let style = undefined;
  if (diff <= 3) {
    style = {color: "var(--dkv-highlight-warning-color)"};
  }

  return <span style={style}>{date.format(resolveUserDateFormat())}</span>;
};

const completeField = (t: any) => (rowData: any) => {
  switch (rowData.complete) {
    case 0:
      return t("Keine Daten");
    case 1:
      return t("Ja");
    case 2:
      return t("Nein");
  }

  return "";
};

// -----------------------------------------------------------------------------------------------------------
const massStorageColumns = [
  {field: "license_plate", header: "Kennzeichen", width: 150, body: bodyField((d) => d.asset.licence_plate)},
  {field: "vin", header: "FIN", width: 250, body: bodyField((d) => d.asset.vin)},
  {field: "rdl_status", header: "Remote Download Status", width: 250, bodyWithTranslation: statusColumn},
  {field: "complete", header: "Vollständigkeit", width: 100, bodyWithTranslation: completeField},
  {field: "last_download", header: "Letzter Download", width: 150, body: dateColumn},
  {field: "_lock", header: "Sperre Firma", width: 250, bodyWithTranslation: lockColumn},
  {field: "_lock_expire", header: "Sperre Ablaufdatum", width: 150, body: lockDateField},
];

// -----------------------------------------------------------------------------------------------------------
interface MassStorageProps {
  backPath: string;
}

const MassStorage: React.FC<MassStorageProps> = ({backPath}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // load policies
  const listState = useSelector(massStorageListStateSelector);
  const entries = useSelector(massStorageListSelector);

  useEffect(() => {
    dispatch(loadMassStorageIfNeeded());
  }, [dispatch]);

  // columns
  const [columns] = useState(massStorageColumns);
  const visibleColumns = useColumns(columns, t, false, true);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  // expanded
  const [expandedRows, setExpandedRows] = useState(null);

  const rowExpansionTemplate = (data: any) => {
    return <MassStorageChart data={data} />;
  };

  // download
  const [showDownloadRange, setShowDownloadRange] = useState<any>(false);
  const openDownloadRange = () => {
    setShowDownloadRange(true);
  };

  const onCloseDownloadRange = () => {
    setShowDownloadRange(false);
  };

  const download = async (from: string, to: string) => {
    console.log("download", selected, from, to);

    for (const assetData of selected) {
      const [{data}, err] = await httpClient.get(`/dtco/rdlmassfile/${assetData.asset.id}/${from}/${to}`, {
        responseType: "blob",
      });
      if (err !== null) {
        console.error(err);
        return err.response?.data?.message
          ? err.response?.data?.message
          : typeof err === "string"
          ? err
          : t("Datei konnte nicht heruntergeladen werden");
      }

      try {
        fileDownload(
          data,
          `${(assetData.asset.license_plate_normalized ?? assetData.asset.id ?? "").replace(
            /-/g,
            ""
          )}-${from.replace(/-/g, "")}-${to.replace(/-/g, "")}.zip`
        );
      } catch (err) {
        console.error(err);
        return t("Datei konnte nicht heruntergeladen werden");
      }
    }

    onCloseDownloadRange();

    return null;
  };

  // render
  return (
    <Page id="rdl-status">
      <Loading inprogress={listState.loading} />

      <TablePageContent>
        <TableStickyHeader backPath={backPath}>
          <h1>{t("Massenspeicher")}</h1>

          {/* <SearchTextField
            name="search"
            type="text"
            required
            label={t("Suche")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          /> */}
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={entries}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            expandedRows={expandedRows}
            onRowToggle={(e: any) => setExpandedRows(e.data)}
            rowExpansionTemplate={rowExpansionTemplate}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Eintrag ausgewählt" : "Einträge ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>
            </>
          )}

          <TableFooterRightActions>
            {selected.length > 0 && hasPermission("DownloadRDLMass") && (
              <Button
                autoWidth
                secondary
                ariaLabel={t("DDD Datei herunterladen")}
                onClick={openDownloadRange}
              >
                {t("DDD Datei herunterladen")}
              </Button>
            )}
          </TableFooterRightActions>
        </TableStickyFooter>
      </TablePageContent>

      {showDownloadRange && (
        <Window onClose={onCloseDownloadRange} headline={t("DDD Download")}>
          {(props) => <DownloadRangePicker {...props} download={download} />}
        </Window>
      )}
    </Page>
  );
};

export default MassStorage;
