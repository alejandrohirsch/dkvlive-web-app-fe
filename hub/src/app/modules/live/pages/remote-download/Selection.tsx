import React from "react";
import styled from "styled-components";
import Page from "app/components/Page";
import Headline from "app/components/Headline";
import PageContent from "app/components/PageContent";
import {useTranslation} from "react-i18next";
import {Link} from "react-router-dom";

import MassIcon from "./icons/dashboard-massenspeicher.svg";
import DriverIcon from "./icons/dashboard-fahrerkarte.svg";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const Box = styled.section`
  background-color: var(--dkv-background-primary-color);
  min-width: 0;
  min-height: 0;
  overflow: auto;
  flex-shrink: 0;

  a {
    padding: 26px 32px 48px 32px;
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 100%;
    width: 100%;
    color: #8c8c8c;

    h1 {
      color: #004b78;
      font-size: 1.4285714285714286rem;
      font-weight: 500;
      line-height: 1.4;
      margin-top: 20px;
    }

    p {
      margin-top: 20px;
      text-align: center;
      font-size: 1.1428571428571428rem;
      padding: 0 30px;
      color: #8c8c8c;
      font-weight: 500;
      line-height: 1.5;
    }
  }

  img {
    width: 120px;
  }

  &:hover {
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.3);
    transition: box-shadow 0.1s linear;
  }
`;

const BoxContainer = styled.div`
  padding: 8px 64px 42px 64px;

  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  grid-gap: 24px;

  @media (max-width: 1440px) {
    grid-template-columns: 1fr 1fr;
    padding: 8px 24px 48px 24px;
    height: auto;
  }

  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface SelectionProps {
  path: string;
}

const Selection: React.FC<SelectionProps> = ({path}) => {
  const {t} = useTranslation();

  // render
  return (
    <Page id="remote-download">
      <Headline>{t("Tachograph")}</Headline>
      <PageContent>
        <BoxContainer>
          {/* <Box>
            <Link to={`${path}/status`}>
              <img src={StatusIcon} alt={t("Status")} />

              <h1>{t("Status")}</h1>

              <p>
                {t(
                  "Sehen Sie sich den aktuellen Status der Remote-Download aller Digitalen Tachographen an."
                )}
              </p>
            </Link>
          </Box> */}
          {hasPermission("dtco:ListRDLMass") && (
            <Box>
              <Link to={`${path}/massstorage`}>
                <img src={MassIcon} alt={t("Massenspeicher")} />

                <h1>{t("Massenspeicher")}</h1>

                <p>
                  {t("Übersicht über die bisher heruntergeladenen Massenspeicher-Daten (Fahrzeugbezogen).")}
                </p>
              </Link>
            </Box>
          )}

          {hasPermission("ListRDLDriver") && (
            <Box>
              <Link to={`${path}/drivercard`}>
                <img src={DriverIcon} alt={t("Massenspeicher")} />

                <h1>{t("Fahrerkarten")}</h1>

                <p>{t("Übersicht über die bisher heruntergeladenen Fahrer-Daten (Fahrerbezogen).")}</p>
              </Link>
            </Box>
          )}
        </BoxContainer>
      </PageContent>
    </Page>
  );
};

export default Selection;
