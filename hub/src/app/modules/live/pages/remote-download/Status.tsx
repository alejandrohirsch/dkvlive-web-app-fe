import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import Page from "app/components/Page";
import {useDispatch} from "react-redux";
import {loadAssetsIfNeeded} from "../../state/assets/actions";
import {Button} from "dkv-live-frontend-ui";
import DataTable, {
  TablePageContent,
  TableStickyHeader,
  TableStickyFooter,
  TableContainer,
  useColumns,
  TableSelectionCountInfo,
  // bodyField,
} from "app/components/DataTable";
import styled, {css} from "styled-components";

import {Pause as PauseIcon} from "@styled-icons/boxicons-regular/Pause";
import {Stop as StopIcon} from "@styled-icons/boxicons-regular/Stop";
import {LocationArrow as LocationArrowIcon} from "@styled-icons/fa-solid/LocationArrow";
import {Check as CheckIcon} from "@styled-icons/fa-solid/Check";
import {Minus as MinusIcon} from "@styled-icons/fa-solid/Minus";

// -----------------------------------------------------------------------------------------------------------

const TagList = styled.ul`
  display: flex;
  flex-flow: row;

  li {
    border-radius: 4px;
    background-color: #2e6b90;
    color: #ffffff;
    padding: 2px 5px;
    margin-left: 4px;
    font-size: 12px;
    white-space: nowrap;
  }
`;

interface StatusContainerProps {
  status: string;
}

const StatusContainer = styled.div<StatusContainerProps>`
  margin-top: 3px;

  border-radius: 50%;
  background-color: var(--dkv-highlight-error-color);
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
  flex-shrink: 0;
  height: 24px;
  width: 24px;

  ${(props) => {
    if (props.status === "online") {
      return css`
        background-color: var(--dkv-highlight-ok-color);
      `;
    }

    if (props.status === "standby") {
      return css`
        background-color: var(--dkv-highlight-warning-color);
      `;
    }

    return null;
  }}
`;

const IconContainer = styled.div`
  margin-top: 3px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
  flex-shrink: 0;
  height: 24px;
  width: 24px;
`;

// -----------------------------------------------------------------------------------------------------------
const generatedEntries = [
  {
    vehicleNumber: "001",
    licensePlate: "KUF973XY",
    tags: ["FST 1", "FST 2"],
    status: "online",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 08:04:53",
    lastAuthenticationTryDate: "21.04.2020 06:44:16",
    version: "DirectRDL",
    track: 210.4217265695479,
  },
  {
    vehicleNumber: "002",
    licensePlate: "KUF200XY",
    tags: ["FST 3"],
    status: "online",
    online: false,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 09:03:29",
    lastAuthenticationTryDate: "21.04.2020 08:58:27",
    version: "DirectRDL",
    track: 318.05153571316623,
  },
  {
    vehicleNumber: "003",
    licensePlate: "KUF974XY",
    tags: ["FST 2", "FST 3"],
    status: "offline",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 07:51:27",
    lastAuthenticationTryDate: "21.04.2020 07:42:59",
    version: "DirectRDL",
    track: 175.8455137195324,
  },
  {
    vehicleNumber: "004",
    licensePlate: "KUF756XY",
    tags: ["DSX"],
    status: "online",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 04:38:41",
    lastAuthenticationTryDate: "21.04.2020 07:55:53",
    version: "DirectRDL",
    track: 149.59078538478934,
  },
  {
    vehicleNumber: "005",
    licensePlate: "KUF844XY",
    tags: ["FST 2"],
    status: "online",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 05:17:06",
    lastAuthenticationTryDate: "21.04.2020 05:54:52",
    version: "DirectRDL",
    track: 206.99970893081627,
  },
  {
    vehicleNumber: "006",
    licensePlate: "KUF865XY",
    tags: ["DSX", "FST 3"],
    status: "online",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 08:30:55",
    lastAuthenticationTryDate: "21.04.2020 06:10:24",
    version: "DirectRDL",
    track: 326.972425615204,
  },
  {
    vehicleNumber: "007",
    licensePlate: "KUF582XY",
    tags: ["FST 2", "FST 3"],
    status: "online",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 05:14:19",
    lastAuthenticationTryDate: "21.04.2020 06:53:32",
    version: "DirectRDL",
    track: 249.0232383854891,
  },
  {
    vehicleNumber: "008",
    licensePlate: "KUF185XY",
    tags: ["FST 1"],
    status: "standby",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 07:31:33",
    lastAuthenticationTryDate: "21.04.2020 04:17:11",
    version: "DirectRDL",
    track: 329.6294217235845,
  },
  {
    vehicleNumber: "009",
    licensePlate: "KUF788XY",
    tags: ["DSX", "FST 3"],
    status: "online",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 04:56:39",
    lastAuthenticationTryDate: "21.04.2020 08:05:29",
    version: "DirectRDL",
    track: 273.8248009801828,
  },
  {
    vehicleNumber: "010",
    licensePlate: "KUF098XY",
    tags: ["FST 2"],
    status: "online",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 08:08:01",
    lastAuthenticationTryDate: "21.04.2020 09:01:33",
    version: "DirectRDL",
    track: 209.14874852416688,
  },
  {
    vehicleNumber: "011",
    licensePlate: "KUF271XY",
    tags: ["FST 2", "FST 3"],
    status: "offline",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 05:12:32",
    lastAuthenticationTryDate: "21.04.2020 08:53:39",
    version: "DirectRDL",
    track: 202.2582275716286,
  },
  {
    vehicleNumber: "012",
    licensePlate: "KUF670XY",
    tags: ["FST 1"],
    status: "online",
    online: false,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: false,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 05:58:48",
    lastAuthenticationTryDate: "21.04.2020 04:46:43",
    version: "DirectRDL",
    track: 215.99864211086242,
  },
  {
    vehicleNumber: "013",
    licensePlate: "KUF073XY",
    tags: ["DSX", "FST 3"],
    status: "online",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 08:17:04",
    lastAuthenticationTryDate: "21.04.2020 08:38:31",
    version: "DirectRDL",
    track: 194.49527245087666,
  },
  {
    vehicleNumber: "014",
    licensePlate: "KUF878XY",
    tags: ["FST 3"],
    status: "offline",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 05:57:37",
    lastAuthenticationTryDate: "21.04.2020 05:34:15",
    version: "DirectRDL",
    track: 306.86817000230803,
  },
  {
    vehicleNumber: "015",
    licensePlate: "KUF966XY",
    tags: ["FST 2"],
    status: "online",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 08:48:55",
    lastAuthenticationTryDate: "21.04.2020 04:20:51",
    version: "DirectRDL",
    track: 137.8314216042054,
  },
  {
    vehicleNumber: "016",
    licensePlate: "KUF025XY",
    tags: ["DSX"],
    status: "online",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 07:24:40",
    lastAuthenticationTryDate: "21.04.2020 05:57:51",
    version: "DirectRDL",
    track: 319.1968766044738,
  },
  {
    vehicleNumber: "017",
    licensePlate: "KUF127XY",
    tags: ["DSX"],
    status: "online",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 08:40:02",
    lastAuthenticationTryDate: "21.04.2020 08:56:29",
    version: "DirectRDL",
    track: 165.42175350623438,
  },
  {
    vehicleNumber: "018",
    licensePlate: "KUF771XY",
    tags: ["FST 2"],
    status: "online",
    online: false,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: false,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 08:42:31",
    lastAuthenticationTryDate: "21.04.2020 06:49:49",
    version: "DirectRDL",
    track: 324.2616716266639,
  },
  {
    vehicleNumber: "019",
    licensePlate: "KUF452XY",
    tags: ["FST 1", "FST 2"],
    status: "standby",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 06:19:55",
    lastAuthenticationTryDate: "21.04.2020 07:48:06",
    version: "DirectRDL",
    track: 36.254961459308106,
  },
  {
    vehicleNumber: "020",
    licensePlate: "KUF556XY",
    tags: ["FST 1"],
    status: "online",
    online: true,
    date: "21.04.2020 14:09:52",
    firmware: "15APR20",
    fmsDate: "21.04.2020 14:09:52",
    dtcoDate: "21.04.2020 14:09:52",
    ignition: true,
    lastDownloadDate: "21.04.2020 14:09:52",
    lastAuthenticationDate: "21.04.2020 06:58:58",
    lastAuthenticationTryDate: "21.04.2020 06:51:44",
    version: "DirectRDL",
    track: 174.42973001459904,
  },
];
const statusColumns = [
  // {field: "vehicleNumber", header: "Fahrzeug", width: 120, sortable: true},
  {field: "licensePlate", header: "Kennzeichen", width: 200, sortable: true},
  {
    field: "tags",
    header: "Tags",
    body: (r: any) => {
      return (
        <TagList>
          {r.tags.map((tag: string) => (
            <li
              key={r.licensePlate + tag}
              style={{
                backgroundColor: tag === "FST 1" ? "#f18400" : tag === "FST 3" ? "#F39A2E" : undefined,
              }}
            >
              {tag}
            </li>
          ))}
        </TagList>
      );
    },
    width: 150,
    sortable: true,
  },
  {
    field: "status",
    header: "Status",
    body: (r: any) => {
      switch (r.status) {
        case "offline":
          return (
            <StatusContainer status={r.status}>
              <StopIcon size={24} />
            </StatusContainer>
          );
        case "online":
          return (
            <StatusContainer status={r.status}>
              <LocationArrowIcon size={12} style={{transform: `rotate(${-45 + r.track!}deg)`}} />
            </StatusContainer>
          );
        case "standby":
          return (
            <StatusContainer status={r.status}>
              <PauseIcon size={24} />
            </StatusContainer>
          );
        default:
          return "";
      }
    },
    width: 100,
    sortable: true,
  },
  {
    field: "online",
    header: "Online",
    body: (r: any) =>
      r.online ? (
        <IconContainer style={{backgroundColor: "var(--dkv-highlight-ok-color)"}}>
          <CheckIcon size={12} />
        </IconContainer>
      ) : (
        <IconContainer style={{backgroundColor: "var(--dkv-highlight-error-color)"}}>
          <MinusIcon size={12} />
        </IconContainer>
      ),
    width: 100,
    sortable: true,
  },
  {
    field: "date",
    header: "Datum",
    width: 200,
    sortable: true,
  },
  {
    field: "firmware",
    header: "Firmware",
    width: 150,
    sortable: true,
  },
  // {
  //   field: "fmsDate",
  //   header: "FMS Datum",
  //   width: 150,
  //   sortable: true,
  // },
  // {
  //   field: "dtcoDate",
  //   header: "DTCO Datum",
  //   width: 150,
  //   sortable: true,
  // },
  {
    field: "ignition",
    header: "Zündung",
    body: (r: any) =>
      r.ignition ? (
        <IconContainer style={{backgroundColor: "var(--dkv-highlight-ok-color)"}}>
          <CheckIcon size={12} />
        </IconContainer>
      ) : (
        <IconContainer style={{backgroundColor: "var(--dkv-highlight-error-color)"}}>
          <MinusIcon size={12} />
        </IconContainer>
      ),
    width: 150,
    sortable: true,
  },
  {
    field: "lastDownloadDate",
    header: "Letzter RDL",
    width: 150,
    sortable: true,
  },
  // {
  //   field: "lastAuthenticationDate",
  //   header: "Letzte Authentifizierung",
  //   width: 150,
  //   sortable: true,
  // },
  // {
  //   field: "lastAuthenticationTryDate",
  //   header: "Letzter Authentifizierungsversuch",
  //   width: 150,
  //   sortable: true,
  // },
  {
    field: "version",
    header: "Version",
    width: 150,
    sortable: true,
  },
];

// -----------------------------------------------------------------------------------------------------------
interface StatusProps {
  backPath: string;
}

const Status: React.FC<StatusProps> = ({backPath}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // @todo: load assets from backend
  // const assets = generateEntries();
  const assets = generatedEntries;

  useEffect(() => {
    dispatch(loadAssetsIfNeeded());
  }, [dispatch]);

  // columns
  const [columns] = useState(statusColumns);
  const visibleColumns = useColumns(columns);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  // render
  return (
    <Page id="rdl-status">
      <TablePageContent>
        <TableStickyHeader backPath={backPath}>
          <h1>
            {assets.length}
            {" Remote-Download " + (assets.length === 1 ? "Eintrag" : "Einträge")}
          </h1>

          {/* @todo commingsoon */}
          {/* <SearchTextField
            name="search"
            type="text"
            required
            label={t("Suche")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          /> */}
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={assets}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Eintrag ausgewählt" : "Einträge ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>
            </>
          )}

          {/* <TableFooterRightActions>
            <Button autoWidth secondary ariaLabel={t("Gesamte Liste exportieren")}>
              {t("Gesamte Liste exportieren")}
            </Button>

            {selected.length > 0 && (
              <Button autoWidth ariaLabel={t("Auswahl als Liste exportieren")}>
                {t("Auswahl als Liste exportieren")}
              </Button>
            )}
          </TableFooterRightActions> */}
        </TableStickyFooter>
      </TablePageContent>
    </Page>
  );
};

export default Status;
