import {RDLState} from "../reducer";
import {MassStorageLoadedAction, LoadingMassStorageFailedAction} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface MassStorage {
  last_download: string;
  complete: number;
  division: string;

  asset: {
    id: string;
    download_status: number;
    license_plate: string;
    vin: string;
    lock_company: string;
    lock_date: string;
    lock_card: string;
    lock_expire: string;
    lock_valid: boolean;
  };
}

export interface MassStorageData {
  list: {
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: string]: MassStorage};
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingMassStorage(state: RDLState) {
  const data = state.massStorage.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingMassStorageFailed(state: RDLState, action: LoadingMassStorageFailedAction) {
  const data = state.massStorage;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleMassStorageLoaded(state: RDLState, action: MassStorageLoadedAction) {
  const data = state.massStorage;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.forEach((t) => (items[t.asset.id] = t));
  data.items = items;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}
