import {Drivercard} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_DRIVERCARD = "rdl/loadingDrivercard";

export interface LoadingDrivercardAction {
  type: typeof LOADING_DRIVERCARD;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_DRIVERCARD_FAILED = "rdl/loadingDrivercardFailed";

export interface LoadingDrivercardFailedAction {
  type: typeof LOADING_DRIVERCARD_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const DRIVERCARD_LOADED = "rdl/drivercardLoaded";

export interface DrivercardLoadedAction {
  type: typeof DRIVERCARD_LOADED;
  payload: Drivercard[];
}

// -----------------------------------------------------------------------------------------------------------
export type DrivercardActions =
  | LoadingDrivercardAction
  | LoadingDrivercardFailedAction
  | DrivercardLoadedAction;
