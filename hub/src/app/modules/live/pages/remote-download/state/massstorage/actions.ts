import {RDLThunkResult} from "../module";
import {MassStorage} from "./reducer";
import httpClient from "services/http";
import {
  LoadingMassStorageAction,
  LOADING_MASSSTORAGE,
  MassStorageLoadedAction,
  MASSSTORAGE_LOADED,
  LoadingMassStorageFailedAction,
  LOADING_MASSSTORAGE_FAILED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingMassStorageAction(): LoadingMassStorageAction {
  return {
    type: LOADING_MASSSTORAGE,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingMassStorageFailedAction(
  errorText?: string,
  unsetItems = true
): LoadingMassStorageFailedAction {
  return {
    type: LOADING_MASSSTORAGE_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function massStorageLoadedAction(items: MassStorage[]): MassStorageLoadedAction {
  return {
    type: MASSSTORAGE_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadMassStorage(): RDLThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingMassStorageAction());

    const [{data}, err] = await httpClient.get("/dtco/rdl/assets");
    if (err !== null) {
      // @todo: log
      dispatch(loadingMassStorageFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(massStorageLoadedAction(data));
  };
}

export function loadMassStorageIfNeeded(): RDLThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().rdl.massStorage;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadMassStorage());
  };
}
