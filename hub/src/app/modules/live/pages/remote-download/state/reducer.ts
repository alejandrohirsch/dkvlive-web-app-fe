import {createReducer} from "@reduxjs/toolkit";
import {
  MassStorageData,
  handleLoadingMassStorage,
  handleLoadingMassStorageFailed,
  handleMassStorageLoaded,
} from "./massstorage/reducer";
import {MASSSTORAGE_LOADED, LOADING_MASSSTORAGE_FAILED, LOADING_MASSSTORAGE} from "./massstorage/types";
import {
  DrivercardData,
  handleLoadingDrivercard,
  handleLoadingDrivercardFailed,
  handleDrivercardLoaded,
} from "./drivercard/reducer";
import {DRIVERCARD_LOADED, LOADING_DRIVERCARD_FAILED, LOADING_DRIVERCARD} from "./drivercard/types";

// -----------------------------------------------------------------------------------------------------------
export interface RDLState {
  massStorage: MassStorageData;
  drivercard: DrivercardData;
}

const initialState: RDLState = {
  massStorage: {
    list: {loading: true},
  },
  drivercard: {
    list: {loading: true},
  },
};

export const reducer = createReducer(initialState, {
  // mass storage
  [LOADING_MASSSTORAGE]: handleLoadingMassStorage,
  [LOADING_MASSSTORAGE_FAILED]: handleLoadingMassStorageFailed,
  [MASSSTORAGE_LOADED]: handleMassStorageLoaded,

  // drivercard
  [LOADING_DRIVERCARD]: handleLoadingDrivercard,
  [LOADING_DRIVERCARD_FAILED]: handleLoadingDrivercardFailed,
  [DRIVERCARD_LOADED]: handleDrivercardLoaded,
});
