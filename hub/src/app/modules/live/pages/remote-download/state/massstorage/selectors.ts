import {RDLModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const massStorageItemsSelector = (state: RDLModuleState) => state.rdl.massStorage.items;

// list
export const massStorageListStateSelector = (state: RDLModuleState) => state.rdl.massStorage.list;

export const massStorageListSelector = createSelector(massStorageItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// item
export const massStorageSelector = (id: string) =>
  createSelector(massStorageItemsSelector, (items) => (items ? items[id] : undefined));
