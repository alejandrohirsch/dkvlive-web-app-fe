import {IModule} from "redux-dynamic-modules";
import {RDLActionTypes} from "./actions";
import {ThunkAction} from "redux-thunk";
import {RDLState, reducer} from "./reducer";

export interface RDLModuleState {
  rdl: RDLState;
}

export type RDLThunkResult<R> = ThunkAction<R, RDLModuleState, void, RDLActionTypes>;

export const RDLModule: IModule<RDLModuleState> = {
  id: "rdl",
  reducerMap: {
    rdl: reducer,
  },
};
