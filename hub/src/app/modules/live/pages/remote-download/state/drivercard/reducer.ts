import {RDLState} from "../reducer";
import {DrivercardLoadedAction, LoadingDrivercardFailedAction} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface Drivercard {
  driver_id: string;
  driver_name: string;
  driver_card: string;
  download_status: number;
  last_download: string;
  card_expire_date: string;
}

export interface DrivercardData {
  list: {
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: string]: Drivercard};
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingDrivercard(state: RDLState) {
  const data = state.drivercard.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingDrivercardFailed(state: RDLState, action: LoadingDrivercardFailedAction) {
  const data = state.drivercard;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleDrivercardLoaded(state: RDLState, action: DrivercardLoadedAction) {
  const data = state.drivercard;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.forEach((t) => (items[t.driver_id] = t));
  data.items = items;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}
