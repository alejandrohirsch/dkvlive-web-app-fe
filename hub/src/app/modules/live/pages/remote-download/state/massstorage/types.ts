import {MassStorage} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_MASSSTORAGE = "rdl/loadingMassStorage";

export interface LoadingMassStorageAction {
  type: typeof LOADING_MASSSTORAGE;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_MASSSTORAGE_FAILED = "rdl/loadingMassStorageFailed";

export interface LoadingMassStorageFailedAction {
  type: typeof LOADING_MASSSTORAGE_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const MASSSTORAGE_LOADED = "rdl/massStorageLoaded";

export interface MassStorageLoadedAction {
  type: typeof MASSSTORAGE_LOADED;
  payload: MassStorage[];
}

// -----------------------------------------------------------------------------------------------------------
export type MassStorageActions =
  | LoadingMassStorageAction
  | LoadingMassStorageFailedAction
  | MassStorageLoadedAction;
