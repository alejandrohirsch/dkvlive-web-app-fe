import {RDLModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const drivercardItemsSelector = (state: RDLModuleState) => state.rdl.drivercard.items;

// list
export const drivercardListStateSelector = (state: RDLModuleState) => state.rdl.drivercard.list;

export const drivercardListSelector = createSelector(drivercardItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// item
export const drivercardSelector = (id: string) =>
  createSelector(drivercardItemsSelector, (items) => (items ? items[id] : undefined));
