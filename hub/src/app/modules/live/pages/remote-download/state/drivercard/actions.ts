import {RDLThunkResult} from "../module";
import {Drivercard} from "./reducer";
import httpClient from "services/http";
import {
  LoadingDrivercardAction,
  LOADING_DRIVERCARD,
  DrivercardLoadedAction,
  DRIVERCARD_LOADED,
  LoadingDrivercardFailedAction,
  LOADING_DRIVERCARD_FAILED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingDrivercardAction(): LoadingDrivercardAction {
  return {
    type: LOADING_DRIVERCARD,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingDrivercardFailedAction(errorText?: string, unsetItems = true): LoadingDrivercardFailedAction {
  return {
    type: LOADING_DRIVERCARD_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function drivercardLoadedAction(items: Drivercard[]): DrivercardLoadedAction {
  return {
    type: DRIVERCARD_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadDrivercard(): RDLThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingDrivercardAction());

    const [{data}, err] = await httpClient.get("/dtco/ddddrivers");

    if (err !== null) {
      // @todo: log
      dispatch(loadingDrivercardFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(drivercardLoadedAction(data));
  };
}

export function loadDrivercardIfNeeded(): RDLThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().rdl.drivercard;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadDrivercard());
  };
}
