import {MassStorageActions} from "./massstorage/types";
import {DrivercardActions} from "./drivercard/types";

export type RDLActionTypes = MassStorageActions | DrivercardActions;
