import React, {useState} from "react";
import {
  WindowContentProps,
  WindowContentContainer,
  WindowHeadline,
  WindowContent,
} from "app/components/Window";
import {useTranslation} from "react-i18next";
import {useFormik} from "formik";
import {MuiPickersUtilsProvider, KeyboardDatePicker} from "@material-ui/pickers";
import {Button, TextField, Message} from "dkv-live-frontend-ui";
import DayjsUtils from "@date-io/dayjs";
import {resolveUserDateLocale, resolveUserDayFormat} from "app/modules/login/state/login/selectors";
import styled from "styled-components";
import dayjs from "dayjs";

// -----------------------------------------------------------------------------------------------------------
const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 12px;
`;

// -----------------------------------------------------------------------------------------------------------
interface DownloadRangePickerProps extends WindowContentProps {
  download: (from: string, to: string) => Promise<string | null>;
}

const DownloadRangePicker: React.FC<DownloadRangePickerProps> = ({headline, setLoading, download}) => {
  const {t} = useTranslation();

  const [error, setError] = useState("");

  const form = useFormik<any>({
    initialValues: {
      from: null,
      to: null,
    },
    onSubmit: (values) => {
      const from = values.from.toDate ? values.from.toDate() : values.from;
      const to = values.to.toDate ? values.to.toDate() : values.to;

      const doDownload = async () => {
        setLoading(true);

        const err = await download(dayjs(from).format("YYYY-MM-DD"), dayjs(to).format("YYYY-MM-DD"));
        if (err !== null) {
          setError(err);
          setLoading(false);
        }
      };

      doDownload();
    },
  });

  // render
  return (
    <WindowContentContainer full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        {error && <Message style={{marginBottom: 24}} text={t(error)} error />}

        <form onSubmit={form.handleSubmit}>
          <FormGrid>
            <MuiPickersUtilsProvider utils={DayjsUtils} locale={resolveUserDateLocale()}>
              <KeyboardDatePicker
                disableToolbar
                name="from"
                label={t("Von")}
                variant="inline"
                format={resolveUserDayFormat()}
                margin="none"
                value={form.values.from}
                onChange={(d: any) => form.setFieldValue("from", d)}
                TextFieldComponent={TextField as any}
                invalidDateMessage={t("Ungültiges Datum")}
                keyboardIcon={<i className="icon-ico_cal-week" />}
                PopoverProps={{
                  className: "dkv-datetimepicker",
                }}
                inputVariant="outlined"
                required
              />

              <KeyboardDatePicker
                disableToolbar
                name="to"
                label={t("Bis")}
                variant="inline"
                format={resolveUserDayFormat()}
                margin="none"
                value={form.values.to}
                onChange={(d: any) => form.setFieldValue("to", d)}
                TextFieldComponent={TextField as any}
                invalidDateMessage={t("Ungültiges Datum")}
                keyboardIcon={<i className="icon-ico_cal-week" />}
                PopoverProps={{
                  className: "dkv-datetimepicker",
                }}
                inputVariant="outlined"
                required
              />
            </MuiPickersUtilsProvider>
          </FormGrid>

          <div style={{marginTop: 12, justifyContent: "flex-end"}}>
            <Button type="submit">{t("Download")}</Button>
          </div>
        </form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default DownloadRangePicker;
