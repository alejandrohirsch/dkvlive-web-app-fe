import React, {useState, useEffect} from "react";
import {Drivercard} from "../state/drivercard/reducer";
import styled, {css} from "styled-components/macro";
import {resolveUserDateLocale} from "app/modules/login/state/login/selectors";
import {IconButton} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import httpClient from "services/http";
import {Message, Loading} from "dkv-live-frontend-ui";
import dayjs from "dayjs";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  padding-bottom: 24px;
  position: relative;
`;

const InfoContainer = styled.div`
  flex-shrink: 0;
  font-weight: bold;
  margin-right: 8px;
  min-width: 125px;
`;

const ChartContainerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const ChartContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
`;

const ChartLegend = styled.div`
  width: 100%;
  padding: 24px 56px 0 56px;
  display: flex;
  justify-content: flex-end;

  ul {
    display: flex;
    flex-direction: row;

    li {
      display: flex;
      align-items: center;
      margin-left: 16px;
      font-size: 12px;

      i {
        font-size: 18px;
      }

      span {
        margin-left: 4px;
      }
    }
  }
`;

const ChartNav = styled.div`
  flex-shrink: 0;
  width: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 12px;

  i {
    font-size: 24px;
    color: #666666;
  }
`;

const Chart = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
`;

interface DayProps {
  first?: boolean;
  last?: boolean;
  hasData?: boolean;
  notAvailable?: boolean;
  userDownloaded?: boolean;
}

const Day = styled.div<DayProps>`
  text-align: center;
  position: relative;
  padding-bottom: 4px;
  overflow: hidden;
  display: flex;
  flex-direction: column;

  &:after {
    content: "";
    width: 100%;
    height: 6px;
    background-color: #dbdbdb;
    position: absolute;
    bottom: 0;
    left: 0;

    ${(props) =>
      props.first &&
      css`
        border-radius: 3px 0 0 3px;
      `}

    ${(props) =>
      props.last &&
      css`
        border-radius: 0 3px 3px 0;
      `}

    ${(props) =>
      props.hasData &&
      css`
        background-color: #58be5b;
      `}
      
    ${(props) =>
      props.notAvailable &&
      css`
        background-color: #fbc02d;
      `}

    ${(props) =>
      props.userDownloaded &&
      css`
        background-color: #266426;
      `}
  }
`;

const DayLabel = styled.span`
  color: #c1c1c1;
`;

const DayDate = styled.span`
  margin-top: 4px;
  margin-bottom: 4px;
`;

interface DayIconProps {
  ok?: boolean;
  error?: boolean;
}

const DayIcon = styled.div<DayIconProps>`
  font-size: 24px;

  ${(props) =>
    props.ok &&
    css`
      color: #58be5b;
    `}

  ${(props) =>
    props.error &&
    css`
      color: #e44c4d;
    `}
`;

const LineLegendCircle = styled.div<DayProps>`
  width: 12px;
  height: 12px;
  border-radius: 50%;
  background-color: #dbdbdb;
  display: inline-block;

  ${(props) =>
    props.hasData &&
    css`
      background-color: #58be5b;
    `}

  ${(props) =>
    props.notAvailable &&
    css`
      background-color: #fbc02d;
    `}

  ${(props) =>
    props.userDownloaded &&
    css`
      background-color: #266426;
    `}
`;

// -----------------------------------------------------------------------------------------------------------
const resolveMonthInfo = (month: number, year: number) => {
  return {
    days: new Date(year, month + 1, 0).getDate(),
    day: new Date(year, month, 1).getDay(),
  };
};

const today = new Date();
const defaultReqData = {
  month: today.getMonth(),
  year: today.getFullYear(),
  date: today.getDate(),
};

// -----------------------------------------------------------------------------------------------------------
interface DrivercardChartProps {
  data: Drivercard;
}

const DrivercardChart: React.FC<DrivercardChartProps> = React.memo(({data: driverCard}) => {
  const {t} = useTranslation();

  const [reqData, setReqData] = useState(defaultReqData);
  const [data, setData] = useState<any>([]);

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    const dateLocale = resolveUserDateLocale();
    const startDate = dayjs(new Date(reqData.year, reqData.month, 1)).format("YYYY-MM-01");

    setLoading(true);

    const load = async () => {
      const [{data}, err] = await httpClient.get(
        `/dtco/ddddriver/timeline/${driverCard.driver_id}/${startDate}`
      );
      if (err !== null) {
        console.error(err);
        setError(
          err.response?.data?.message
            ? err.response?.data?.message
            : typeof err === "string"
            ? err
            : t("Datei konnte nicht heruntergeladen werden")
        );
        return;
      }

      const info = resolveMonthInfo(reqData.month, reqData.year);

      let day = info.day - 1;
      const graphData =
        data && data.TimeLine
          ? data.TimeLine.map((dayData: any) => {
              day++;
              if (day > 6) {
                day = 0;
              }

              return {
                day: dateLocale && dateLocale.weekdaysMin ? dateLocale.weekdaysMin[day] : day,
                date: dayData.Day,
                error: dayData.Available === 0,
                download: dayData.Download === 1,
                downloadError: dayData.Download === 2,
                data: dayData.Available === 2,
                userDownloaded: dayData.Available === 3,
                notAvailable: dayData.Available === 1,
              };
            })
          : [];

      setError("");
      setLoading(false);
      setData(graphData);
    };

    load();
  }, [reqData, t, driverCard]);

  const nav = (dir: number) => {
    setReqData((rData) => {
      let month = rData.month + dir;
      let year = rData.year;
      if (month === -1) {
        month = 11;
        year = year - 1;
      }

      if (month === 12) {
        month = 0;
        year = year + 1;
      }

      return {
        month: month,
        year: year,
        date: 0,
      };
    });
  };

  // render
  const dateLocale = resolveUserDateLocale();

  if (error) {
    return (
      <Container>
        <Message style={{marginBottom: 24}} text={t(error)} error />
      </Container>
    );
  }

  return (
    <Container>
      <Loading inprogress={loading} />

      <InfoContainer>
        <span>
          {dateLocale && dateLocale.months ? dateLocale.months[reqData.month] : reqData.month + 1}{" "}
          {reqData.year}
        </span>
      </InfoContainer>
      <ChartContainerWrapper>
        <ChartContainer>
          <ChartNav>
            <IconButton
              aria-label={t("Zurück")}
              onClick={() => nav(-1)}
              onMouseDown={(e) => e.preventDefault()}
            >
              <span className="icon-ico_chevron-left" />
            </IconButton>
          </ChartNav>
          <Chart>
            {data.map((d: any) => (
              <Day
                key={`${reqData.year}-${reqData.month}-${d.date}`}
                style={{width: `calc(100% / ${data.length})`}}
                first={d.date === 1}
                last={d.date === data.length}
                hasData={d.data}
                notAvailable={d.notAvailable}
                userDownloaded={d.userDownloaded}
              >
                <DayLabel>{d.day}</DayLabel>
                <DayDate>{d.date}</DayDate>

                {d.download && (
                  <DayIcon ok>
                    <i className="icon-ico_download" />
                  </DayIcon>
                )}

                {d.downloadError && (
                  <DayIcon error>
                    <i className="icon-ico_warning_full" />
                  </DayIcon>
                )}
              </Day>
            ))}
          </Chart>
          <ChartNav>
            <IconButton aria-label={t("Vor")} onClick={() => nav(1)} onMouseDown={(e) => e.preventDefault()}>
              <span className="icon-ico_chevron-right" />
            </IconButton>
          </ChartNav>
        </ChartContainer>
        <ChartLegend>
          <ul>
            <li>
              <i className="icon-ico_download" style={{color: "#58be5b"}} />{" "}
              <span>{t("Download stattgefunden")}</span>
            </li>
            <li>
              <i className="icon-ico_warning_full" style={{color: "#e44c4d"}} />{" "}
              <span>{t("Remote Download fehlgeschlagen")}</span>
            </li>
            <li>
              <LineLegendCircle userDownloaded /> <span>{t("Bereits heruntergeladen")}</span>
            </li>
            <li>
              <LineLegendCircle hasData /> <span>{t("Daten vorhanden")}</span>
            </li>
            <li>
              <LineLegendCircle notAvailable /> <span>{t("Daten nicht verfügbar")}</span>
            </li>
          </ul>
        </ChartLegend>
      </ChartContainerWrapper>
    </Container>
  );
});

export default DrivercardChart;
