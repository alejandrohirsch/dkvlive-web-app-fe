import React, {useState, useEffect} from "react";
import {useTranslation} from "react-i18next";
import Page from "app/components/Page";
import {Button, Loading} from "dkv-live-frontend-ui";
import DataTable, {
  TablePageContent,
  TableStickyHeader,
  TableStickyFooter,
  TableContainer,
  useColumns,
  TableSelectionCountInfo,
  TableFooterRightActions,
} from "app/components/DataTable";
import {resolveUserDateFormat} from "app/modules/login/state/login/selectors";
import dayjs from "dayjs";
import {drivercardListSelector, drivercardListStateSelector} from "./state/drivercard/selectors";
import {loadDrivercardIfNeeded} from "./state/drivercard/actions";
import {useSelector, useDispatch} from "react-redux";
import styled, {css} from "styled-components";
import Window from "app/components/Window";
import DownloadRangePicker from "./components/DownloadRangePicker";
import DrivercardChart from "./components/DrivercardChart";
import {Drivercard as DrivercardData} from "./state/drivercard/reducer";
import httpClient from "services/http";
import fileDownload from "js-file-download";

// -----------------------------------------------------------------------------------------------------------
interface StautsFieldProps {
  ok?: boolean;
  error?: boolean;
}

const StautsField = styled.span<StautsFieldProps>`
  display: flex;
  align-items: center;
  font-weight: 500;

  i {
    font-size: 24px;
    margin-right: 4px;
  }

  ${(props) =>
    props.ok &&
    css`
      color: var(--dkv-highlight-ok-color);
    `}

  ${(props) =>
    props.error &&
    css`
      color: var(--dkv-carminepink);
    `}
`;

// -----------------------------------------------------------------------------------------------------------
const dateColumn = (rowData: any, column: any) => {
  const d = rowData[column.field];
  if (!d || d === "0001-01-01T00:00:00Z") {
    return "";
  }

  return dayjs(d).format(resolveUserDateFormat());
};

const statusColumn = (t: any) => (rowData: any) => {
  const s = parseInt(rowData["rdl_status"]);

  switch (s) {
    case 1:
      return (
        <StautsField ok>
          <i className="icon-ico_check_status" />
          {t("Erfolgreich")}
        </StautsField>
      );
    case 2:
      return (
        <StautsField error>
          <i className="icon-ico_warning_full" />
          {t("Fehlgeschlagen")}
        </StautsField>
      );
  }

  return (
    <StautsField>
      <i className="icon-ico_minus" />
      {t("Unbekannt")}
    </StautsField>
  );
};

const dateField = (rowData: any, column: any) => {
  const d = rowData[column.field];
  if (!d || d === "0001-01-01T00:00:00Z") {
    return "";
  }

  const date = dayjs(d);

  const diff = date.diff(dayjs(), "w");
  let style = undefined;
  if (diff <= 3) {
    style = {color: "var(--dkv-highlight-warning-color)"};
  }

  return <span style={style}>{date.format(resolveUserDateFormat())}</span>;
};

// -----------------------------------------------------------------------------------------------------------
const drivercardColumns = [
  {field: "driver_name", header: "Name", width: 150},
  {field: "driver_card", header: "Karte", width: 150},
  {field: "download_status", header: "Remote Download Status", width: 250, bodyWithTranslation: statusColumn},
  {field: "last_download", header: "Letzter Download", width: 150, body: dateColumn},
  {field: "card_expire_date", header: "Karte Ablaufdatum", width: 150, body: dateField},
];

// -----------------------------------------------------------------------------------------------------------
interface DrivercardProps {
  backPath: string;
}

const Drivercard: React.FC<DrivercardProps> = ({backPath}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // load policies
  const listState = useSelector(drivercardListStateSelector);
  const entries = useSelector(drivercardListSelector);

  useEffect(() => {
    dispatch(loadDrivercardIfNeeded());
  }, [dispatch]);

  // columns
  const [columns] = useState(drivercardColumns);
  const visibleColumns = useColumns(columns, t, false, true);

  // selection
  const [selected, setSelected] = useState<DrivercardData[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  // expanded
  const [expandedRows, setExpandedRows] = useState(null);

  const rowExpansionTemplate = (data: any) => {
    return <DrivercardChart data={data} />;
  };

  // download
  const [showDownloadRange, setShowDownloadRange] = useState<any>(false);
  const openDownloadRange = () => {
    setShowDownloadRange(true);
  };

  const onCloseDownloadRange = () => {
    setShowDownloadRange(false);
  };

  const download = async (from: string, to: string) => {
    const ids = [];
    for (const driverData of selected) {
      ids.push(driverData.driver_id);
    }

    const [{data}, err] = await httpClient.get(`/dtco/dddfile/${ids.join(",")}/${from}/${to}`, {
      responseType: "blob",
    });
    if (err !== null) {
      console.error(err);
      return err.response?.data?.message
        ? err.response?.data?.message
        : typeof err === "string"
        ? err
        : t("Datei konnte nicht heruntergeladen werden");
    }

    try {
      fileDownload(data, `drivers-${from.replace(/-/g, "")}-${to.replace(/-/g, "")}.zip`);
    } catch (err) {
      console.error(err);
      return t("Datei konnte nicht heruntergeladen werden");
    }

    onCloseDownloadRange();

    return null;
  };

  // render
  return (
    <Page id="rdl-status">
      <Loading inprogress={listState.loading} />
      <TablePageContent>
        <TableStickyHeader backPath={backPath}>
          <h1>{t("Fahrerkarten")}</h1>

          {/* <SearchTextField
            name="search"
            type="text"
            required
            label={t("Suche")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          /> */}
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={entries}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            expandedRows={expandedRows}
            onRowToggle={(e: any) => setExpandedRows(e.data)}
            rowExpansionTemplate={rowExpansionTemplate}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Eintrag ausgewählt" : "Einträge ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>
            </>
          )}

          <TableFooterRightActions>
            {selected.length > 0 && (
              <Button
                autoWidth
                secondary
                ariaLabel={t("DDD Datei herunterladen")}
                onClick={openDownloadRange}
              >
                {t("DDD Datei herunterladen")}
              </Button>
            )}
          </TableFooterRightActions>
        </TableStickyFooter>
      </TablePageContent>
      {showDownloadRange && (
        <Window onClose={onCloseDownloadRange} headline={t("DDD Download")}>
          {(props) => <DownloadRangePicker {...props} download={download} />}
        </Window>
      )}
    </Page>
  );
};

export default Drivercard;
