import React from "react";
import {useTranslation} from "react-i18next";
import AlarmList, {AlarmHeader, AlarmHeaderBadge} from "./AlarmList";

// -----------------------------------------------------------------------------------------------------------
const Infos: React.FC = () => {
  const {t} = useTranslation();

  const data: any[] = [
    {id: "123", type: "Betankung stattgefunden", license_plate: "KU124CD", time: "8"},
    {id: "124", type: "Geburtstag", license_plate: "Dieter Fink"},
  ];

  return (
    <>
      <AlarmHeader type="info">
        <h1>{t("Ereignisse")}</h1>
        <h2>{t("Keine Handlung erforderlich")}</h2>
        <AlarmHeaderBadge type="info">{data.length}</AlarmHeaderBadge>
      </AlarmHeader>
      <AlarmList items={data} />
    </>
  );
};

export default Infos;
