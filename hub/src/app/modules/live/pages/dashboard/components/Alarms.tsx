import React from "react";
import {useTranslation} from "react-i18next";
import AlarmList, {AlarmHeader, AlarmHeaderBadge} from "./AlarmList";

// -----------------------------------------------------------------------------------------------------------
const Alarms: React.FC = () => {
  const {t} = useTranslation();

  const data: any[] = [
    {id: "123", type: "Fahrt ohne Fahrerkarte", license_plate: "KU124CD", time: "10"},
    {id: "124", type: "Geschwindigkeit Überschreitung", license_plate: "KU124CD", time: "12"},
    {id: "125", type: "Lenkzeit Überschreitung", license_plate: "KU125EF", time: "22"},
    {id: "126", type: "Lenkzeit Überschreitung", license_plate: "KU124CD", time: "30"},
    {id: "127", type: "Geschwindigkeit Überschreitung", license_plate: "KU126GH", time: "41"},
    {id: "128", type: "Geschwindigkeit Überschreitung", license_plate: "KU127IJ", time: "46"},
  ];

  return (
    <>
      <AlarmHeader type="alarm">
        <h1>{t("Alarme")}</h1>
        <h2>{t("Sofortige Handlung notwendig")}</h2>
        <AlarmHeaderBadge type="alarm">{data.length}</AlarmHeaderBadge>
      </AlarmHeader>
      <AlarmList items={data} />
    </>
  );
};

export default Alarms;
