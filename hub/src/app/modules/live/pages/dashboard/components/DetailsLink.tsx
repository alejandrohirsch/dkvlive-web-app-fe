import React from "react";
import styled from "styled-components/macro";

// -----------------------------------------------------------------------------------------------------------
export const Container = styled.button`
  color: var(--dkv-link-primary-color);
  line-height: 1.25;
  font-size: 1em;
  font-weight: 500;
  transition: color linear 0.1s;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover,
  &:focus {
    color: var(--dkv-link-primary-hover-color);
  }

  span {
    font-size: 12px;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface DetailsLinkProps {
  label: string;
  className?: string;
  onClick?: () => void;
}

const DetailsLink: React.FC<DetailsLinkProps> = ({label, className, onClick}) => {
  return (
    <Container className={className} onClick={onClick}>
      <span className="icon-ico_chevron-right" /> {label}
    </Container>
  );
};

export default DetailsLink;
