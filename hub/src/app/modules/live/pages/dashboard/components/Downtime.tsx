import React from "react";
import {useTranslation} from "react-i18next";
import AutoSizer from "react-virtualized-auto-sizer";
import {BarChart, Bar as RechartsBar, XAxis, YAxis, ReferenceLine as RechartsReferenceLine} from "recharts";
import DetailsLink from "./DetailsLink";
import styled from "styled-components";

// -----------------------------------------------------------------------------------------------------------
const DetailsLinkAbsolute = styled(DetailsLink)`
  position: absolute;
  right: 32px;
  bottom: 26px;
`;

// -----------------------------------------------------------------------------------------------------------
const Bar = RechartsBar as any;
const ReferenceLine = RechartsReferenceLine as any;

const BarLabel = (props: any) => {
  const {y, value, barWidth} = props;

  return (
    <text
      type="category"
      orientation="left"
      x={barWidth - 60}
      y={y + 8}
      stroke="none"
      fill="rgb(102, 102, 102)"
      className="recharts-text recharts-cartesian-axis-tick-value"
      textAnchor="left"
    >
      <tspan>
        {value} {props.t("Std")}
      </tspan>
    </text>
  );
};

const YAxisTick = (props: any): SVGElement => {
  const {width, height, x, y, payload} = props;

  return ((
    <text
      orientation="left"
      width={width}
      height={height}
      type="number"
      x={x - 16}
      y={y}
      stroke="none"
      fill={"rgb(102, 102, 102)"}
      className="recharts-text recharts-cartesian-axis-tick-value recharts-cartesian-axis-tick-value-x"
      textAnchor="end"
    >
      <tspan x={x - 16} dy="0.355em">
        {payload.value}
      </tspan>
    </text>
  ) as unknown) as SVGElement;
};

const ReferenceLabel = (props: any) => {
  const {
    viewBox: {x, y, height},
    offset,
    value,
  } = props;

  return (
    <text
      x={x}
      y={y + height + 12}
      offset={offset}
      className="recharts-text recharts-label recharts-reference-label"
      textAnchor="end"
      fill="#A8A8A8"
    >
      <tspan x={x} dy="0.355em">
        {props.t("Durchschnitt = {{.hours}} Std/Woche", {hours: value})}
      </tspan>
    </text>
  );
};

// -----------------------------------------------------------------------------------------------------------
const Downtime: React.FC = () => {
  const {t} = useTranslation();

  const data = [
    {name: "KU124CD", amount: 48},
    {name: "KU125EF", amount: 44},
    {name: "KU126GH", amount: 36},
    {name: "n/a", amount: 36},
    {name: "KU127IJ", amount: 35},
  ];

  const average = 34;

  return (
    <>
      <h1>{t("Längste Standzeiten / Woche")}</h1>
      <h2>{t("Flottendurchschnitt = {{.hours}} Std/Woche", {hours: average})}</h2>

      <AutoSizer>
        {({width}) => (
          <BarChart
            width={width}
            height={data.length * 48}
            data={data}
            layout="vertical"
            margin={{top: 20, right: 80, left: 50, bottom: 66}}
          >
            <XAxis type="number" hide />
            <YAxis type="category" dataKey="name" axisLine={false} tickLine={false} tick={YAxisTick} />
            <Bar dataKey="amount" fill="#5B7C93" barSize={5} label={<BarLabel barWidth={width} t={t} />} />
            <ReferenceLine
              x={34}
              stroke="#A8A8A8"
              strokeDasharray="5 5"
              label={<ReferenceLabel value={average} t={t} />}
            />
          </BarChart>
        )}
      </AutoSizer>

      <DetailsLinkAbsolute label={t("Details")} />
    </>
  );
};

export default Downtime;
