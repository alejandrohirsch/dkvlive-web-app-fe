import React, {useRef, useEffect} from "react";
import {useTranslation} from "react-i18next";
import AutoSizer from "react-virtualized-auto-sizer";
import {BarChart, Bar as RechartsBar, XAxis, YAxis} from "recharts";
import styled from "styled-components";
import {resolveUserLanguage} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const InfoText = styled.h2`
  position: absolute;
  left: 32px;
  bottom: 26px;
`;

// -----------------------------------------------------------------------------------------------------------
const Bar = RechartsBar as any;

const BarLabel = (props: any) => {
  const {y, value, barWidth, index} = props;

  return (
    <text
      type="category"
      orientation="left"
      x={barWidth - 220}
      y={y + 8}
      stroke="none"
      fill="rgb(102, 102, 102)"
      className="recharts-text recharts-cartesian-axis-tick-value"
      textAnchor="left"
    >
      <tspan>
        {props.numberFormat.format(value.toFixed(1))} l{" "}
        <tspan fill="#909090" dx={5}>
          {props.data[index].driver}
        </tspan>
      </tspan>
    </text>
  );
};

const YAxisTick = (props: any): SVGElement => {
  const {width, height, x, y, payload} = props;

  return ((
    <text
      orientation="left"
      width={width}
      height={height}
      type="number"
      x={x - 16}
      y={y}
      stroke="none"
      fill={"rgb(102, 102, 102)"}
      className="recharts-text recharts-cartesian-axis-tick-value recharts-cartesian-axis-tick-value-x"
      textAnchor="end"
    >
      <tspan x={x - 16} dy="0.355em">
        {payload.value}
      </tspan>
    </text>
  ) as unknown) as SVGElement;
};

// -----------------------------------------------------------------------------------------------------------
const TopDriver: React.FC = () => {
  const {t} = useTranslation();

  // nf
  const numberFormatRef = useRef<Intl.NumberFormat>();
  useEffect(() => {
    numberFormatRef.current = Intl.NumberFormat(resolveUserLanguage(), {minimumFractionDigits: 1});
  }, []);

  // data
  const data = [
    {name: "1", amount: 27, driver: "Alexander Pfaff"},
    {name: "2", amount: 27.3, driver: "Frank Hertzog "},
    {name: "3", amount: 27.6, driver: "Michael Ostermann"},
    {name: "4", amount: 28.0, driver: "Jens Abt"},
    {name: "5", amount: 28.2, driver: "Stefan Wulf"},
  ];

  const average = "30,0";

  // render
  return (
    <>
      <h1>{t("Top 5 Fahrer")}</h1>
      <h2>Ökologisches Fahrverhalten ihrer Fahrer</h2>
      <h2>{t("Durchschnittsverbrauch {{.average}} l", {average: average})}</h2>

      <AutoSizer>
        {({width}) => (
          <BarChart
            width={width}
            height={data.length * 48}
            data={data}
            layout="vertical"
            margin={{top: 20, right: 230, left: -20, bottom: 66}}
          >
            <XAxis type="number" hide />
            <YAxis type="category" dataKey="name" axisLine={false} tickLine={false} tick={YAxisTick} />
            <Bar
              dataKey="amount"
              fill="#5B7C93"
              barSize={5}
              label={<BarLabel barWidth={width} t={t} data={data} numberFormat={numberFormatRef.current} />}
            />
          </BarChart>
        )}
      </AutoSizer>

      <InfoText>Verbrauchsdurchschnitt Liter pro 100km</InfoText>
    </>
  );
};

export default TopDriver;
