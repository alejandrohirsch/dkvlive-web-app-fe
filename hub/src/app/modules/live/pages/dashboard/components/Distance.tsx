import React, {useEffect, useRef} from "react";
import {useTranslation} from "react-i18next";
import AutoSizer from "react-virtualized-auto-sizer";
import {BarChart, Bar as RechartsBar, XAxis, YAxis} from "recharts";
import {resolveUserLanguage} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const Bar = RechartsBar as any;

const Bars = (props: any) => {
  const {x, y, width, name} = props;
  const nameData = name.split(";");

  let height = 5;
  let dy = height / 2;
  let fill = "#5B7C93";
  if (nameData[1]) {
    height = 8;
    dy = 0;
    fill = "#004B78";
  }

  return (
    <svg>
      <rect x={x} y={y + dy} width={width} height={height} stroke="none" fill={fill} />
    </svg>
  );
};

const BarLabel = (props: any) => {
  const {y, value, barWidth, name} = props;
  const nameData = name.split(";");

  return (
    <text
      type="category"
      orientation="left"
      x={barWidth - 60}
      y={y + 8}
      stroke="none"
      fill="rgb(102, 102, 102)"
      className="recharts-text recharts-cartesian-axis-tick-value"
      textAnchor="left"
      style={{fontWeight: nameData[1] ? "bold" : "normal"}}
    >
      <tspan>{props.numberFormat.format(value)}</tspan>
    </text>
  );
};

const YAxisTick = (props: any): SVGElement => {
  const {width, height, x, y, payload} = props;

  const value = payload.value.split(";");

  return ((
    <text
      orientation="left"
      width={width}
      height={height}
      type="number"
      x={x - 16}
      y={y}
      stroke="none"
      fill={"rgb(102, 102, 102)"}
      className="recharts-text recharts-cartesian-axis-tick-value recharts-cartesian-axis-tick-value-x"
      textAnchor="end"
      style={{fontWeight: value[1] ? "bold" : "normal"}}
    >
      <tspan x={x - 16} dy="0.355em">
        {value[0]}
      </tspan>
    </text>
  ) as unknown) as SVGElement;
};

// -----------------------------------------------------------------------------------------------------------
const Distance: React.FC = () => {
  const {t} = useTranslation();

  // nf
  const numberFormatRef = useRef<Intl.NumberFormat>();
  useEffect(() => {
    numberFormatRef.current = Intl.NumberFormat(resolveUserLanguage(), {minimumFractionDigits: 0});
  }, []);

  // data
  const data = [
    {name: "Heute;today", amount: 3023},
    {name: "Gestern", amount: 4891},
    {name: "So, 01.03", amount: 1120},
    {name: "Sa, 29.02", amount: 2944},
    {name: "Fr, 28.02", amount: 5818},
    {name: "Do, 27.02", amount: 5444},
  ];

  // render
  return (
    <>
      <h1>{t("Gefahrene Kilometer")}</h1>
      <h2>{t("Gesamtkilometer Ihrer Flotte")}</h2>

      <AutoSizer>
        {({width}) => (
          <BarChart
            width={width}
            height={data.length * 48}
            data={data}
            layout="vertical"
            margin={{top: 20, right: 80, left: 28, bottom: 66}}
          >
            <XAxis type="number" hide />
            <YAxis type="category" dataKey="name" axisLine={false} tickLine={false} tick={YAxisTick} />
            <Bar
              dataKey="amount"
              fill="#5B7C93"
              barSize={8}
              label={<BarLabel barWidth={width} numberFormat={numberFormatRef.current} />}
              shape={<Bars />}
            />
          </BarChart>
        )}
      </AutoSizer>
    </>
  );
};

export default Distance;
