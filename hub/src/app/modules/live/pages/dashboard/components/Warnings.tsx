import React from "react";
import {useTranslation} from "react-i18next";
import AlarmList, {AlarmHeader, AlarmHeaderBadge} from "./AlarmList";

// -----------------------------------------------------------------------------------------------------------
const Warnings: React.FC = () => {
  const {t} = useTranslation();

  const data: any[] = [
    {id: "123", type: "Serviceintervall Überschreitung", license_plate: "KU124CD", time: "8"},
    {id: "124", type: "Signal verloren", license_plate: "KU125EF", time: "12"},
    {id: "125", type: "Auffällige Betankung", license_plate: "KU127IJ", time: "18"},
  ];

  return (
    <>
      <AlarmHeader type="warning">
        <h1>{t("Hinweise")}</h1>
        <h2>{t("Handlung erforderlich")}</h2>
        <AlarmHeaderBadge type="warning">{data.length}</AlarmHeaderBadge>
      </AlarmHeader>
      <AlarmList items={data} />
    </>
  );
};

export default Warnings;
