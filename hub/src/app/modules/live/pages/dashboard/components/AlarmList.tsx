import React, {useCallback, MouseEventHandler, useState} from "react";
import styled, {css} from "styled-components";
import DetailsLink from "./DetailsLink";
import {useTranslation} from "react-i18next";
import {IconButton} from "@material-ui/core";
import {useHistory} from "react-router-dom";

// -----------------------------------------------------------------------------------------------------------
const List = styled.ul`
  height: 100%;
  overflow: auto;
`;

const ItemContent = styled.div`
  width: 100%;
  height: 100%;
  border-bottom: 1px solid #f7f7f7;
  padding: 20px 0;
  position: relative;
  display: flex;
  flex-direction: column;
`;

const Item = styled.li`
  padding: 0 32px;

  &:first-of-type {
    ${ItemContent} {
      border-top: 1px solid #f7f7f7;
    }
  }
`;

const LicensePlate = styled.span`
  font-weight: bold;
  margin-top: 6px;
`;

const DetailsLinkAbsolute = styled(DetailsLink)`
  position: absolute;
  right: 32px;
  bottom: 20px;
`;

const Time = styled.span`
  position: absolute;
  top: 10px;
  right: 0;
  font-size: 0.8571428571428571em;
  color: #666666;
`;

const CloseButton = styled(IconButton)`
  position: absolute !important;
  bottom: 14px;
  right: 0;
  padding: 0 !important;
  width: 24px;
  height: 24px;

  span {
    font-size: 18px;
  }
`;

interface AlarmHeaderProps {
  type?: "alarm" | "warning" | "info";
}

export const AlarmHeaderBadge = styled.div<AlarmHeaderProps>`
  width: 24px;
  height: 16px;
  border-radius: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
  font-size: 0.7142857142857143em;
  position: absolute;
  right: 48px;
  top: 50%;

  ${(props) => {
    if (!props.type) {
      return undefined;
    }

    let color = "";
    switch (props.type) {
      case "alarm":
        color = "--dkv-highlight-error-color";
        break;
      case "warning":
        color = "--dkv-highlight-warning-color";
        break;
      case "info":
        color = "--dkv-highlight-info-color";
        break;
    }

    return css`
      background-color: var(${color});
    `;
  }}
`;

export const AlarmHeader = styled.div<AlarmHeaderProps>`
  padding: 12px 0 12px 32px;
  border-left: 3px solid;
  position: relative;

  ${(props) => {
    if (!props.type) {
      return undefined;
    }

    let color = "";
    switch (props.type) {
      case "alarm":
        color = "--dkv-highlight-error-color";
        break;
      case "warning":
        color = "--dkv-highlight-warning-color";
        break;
      case "info":
        color = "--dkv-highlight-info-color";
        break;
    }

    return css`
      border-color: var(${color});

      h1 {
        color: var(${color});
      }
    `;
  }}
`;

// -----------------------------------------------------------------------------------------------------------
interface AlarmListProps {
  items: any[];
}

const AlarmList: React.FC<AlarmListProps> = ({items}) => {
  const {t} = useTranslation();
  const history = useHistory();

  const [alarms, setAlarms] = useState(items);

  const confirm: MouseEventHandler<HTMLButtonElement> = useCallback(
    (e) => {
      const data = (e.currentTarget as HTMLButtonElement).dataset;
      const id = data?.id;

      if (!id || !alarms) {
        return;
      }

      const index = alarms.findIndex((item) => item.id === id);
      alarms.splice(index, 1);
      setAlarms((alarms) => [...alarms]);
    },
    [alarms, setAlarms]
  );

  const handleMouseDown = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  return (
    <List>
      {alarms.map((item: any) => (
        <Item key={item.id}>
          <ItemContent>
            <span>{item.type}</span>
            <LicensePlate>{item.license_plate}</LicensePlate>

            <DetailsLinkAbsolute onClick={() => history.push("/alerts")} label={t("Details")} />
            <Time>{item.time ? t("Von {{.time}} min.", {time: item.time}) : t("Heute")}</Time>

            <CloseButton
              aria-label={t("Alarm bestätigen")}
              onClick={confirm}
              onMouseDown={handleMouseDown}
              data-id={item.id}
            >
              <span className="icon-ico_clear" />
            </CloseButton>
          </ItemContent>
        </Item>
      ))}
    </List>
  );
};

export default AlarmList;
