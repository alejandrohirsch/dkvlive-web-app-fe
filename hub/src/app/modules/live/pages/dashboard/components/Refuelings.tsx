import React from "react";
import styled from "styled-components/macro";
import {useTranslation} from "react-i18next";
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip as RechartsTooltip} from "recharts";
import AutoSizer from "react-virtualized-auto-sizer";
import DetailsLink from "./DetailsLink";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  position: relative;
`;

const BarContainer = styled.div`
  height: 100%;
  position: relative;
`;

const TooltipContainer = styled.div`
  padding: 8px;
  border: solid 0.5px #707070;
  background-color: #fff;
`;

const TooltipLabel = styled.p`
  font-weight: bold;
  font-size: 1.14286em;
  color: var(--dkv-text-primary-color);
  line-height: 1.25;
`;

const TooltipData = styled.div`
  margin-top: 4px;
  font-size: 1em;

  span {
    font-weight: bold;
  }
`;

const DetailsLinkAbsolute = styled(DetailsLink)`
  position: absolute;
  right: 0;
  bottom: 0;
`;

const VerifiedInfo = styled.div`
  position: absolute;
  top: 4px;
  right: 0;
  font-size: 0.8571428571428571em;
  color: #909090;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
`;

const VerifiedInfoIcon = styled.div`
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: #f18400;
  margin-right: 5px;
`;

// -----------------------------------------------------------------------------------------------------------
const Tooltip = RechartsTooltip as any;

const Bars = (props: any) => {
  const {fill, x, y, width, height, dataKey} = props;

  const count = props[dataKey];

  const margin = 2;
  let barHeight = height / count;

  if (props.unverified && dataKey === "unverified" && count > 1) {
    barHeight = barHeight - margin / count;
  }

  const rects = [];
  for (let i = 0; i < count; i++) {
    rects.push(
      <rect
        key={y + i * barHeight}
        x={x + width - width / 2 - 2}
        y={y + i * barHeight + i * (margin / count)}
        width={8}
        height={barHeight - margin}
        stroke="none"
        fill={fill}
      />
    );
  }

  return <svg>{rects}</svg>;
};

const XAxisTick = (props: any) => {
  const {width, height, x, y, payload} = props;

  const value = payload.value.split(";");

  return (
    <text
      orientation="bottom"
      width={width}
      height={height}
      type="category"
      x={x}
      y={y}
      stroke="none"
      fill="#909090"
      className="recharts-text recharts-cartesian-axis-tick-value recharts-cartesian-axis-tick-value-x"
      textAnchor="middle"
    >
      <tspan x={x} dy="0.71em">
        {value[0]}
      </tspan>
      <tspan x={x} dy="1.5em">
        {value[1]}
      </tspan>
    </text>
  );
};

const YAxisTick = (props: any) => {
  const {width, height, x, y, payload} = props;

  if (payload.value === 0) {
    return null;
  }

  return (
    <text
      orientation="left"
      width={width}
      height={height}
      type="number"
      x={x}
      y={y}
      stroke="none"
      fill="#909090"
      className="recharts-text recharts-cartesian-axis-tick-value recharts-cartesian-axis-tick-value-x"
      textAnchor="end"
    >
      <tspan x={x} dy="0.355em">
        {payload.value}
      </tspan>
    </text>
  );
};

const TooltipContent = (props: any) => {
  const {t} = useTranslation();

  if (!props.active) {
    return null;
  }

  const label = props.label.split(";");

  const data = {verified: 0, unverified: 0};
  props.payload.forEach((p: any) => {
    if (p.dataKey === "verified") {
      data.verified += p.value;
    } else if (p.dataKey === "unverified") {
      data.unverified += p.value;
    }
  });

  return (
    <TooltipContainer>
      <TooltipLabel>
        {label[1]} {label[0]}
      </TooltipLabel>

      <TooltipData>
        <label>{t("verifiziert")}: </label>
        <span>{data.verified}</span>
      </TooltipData>

      <TooltipData>
        <label>{t("nicht verifiziert")}: </label>
        <span>{data.unverified}</span>
      </TooltipData>
    </TooltipContainer>
  );
};

// -----------------------------------------------------------------------------------------------------------
const Refuelings: React.FC = React.memo(() => {
  const {t} = useTranslation();

  const data = [
    {name: "24.02;Mo", verified: 8, unverified: 2},
    {name: "25.02;Di", verified: 6, unverified: 4},
    {name: "26.02;Mi", verified: 6, unverified: 1},
    {name: "27.02;Do", verified: 5, unverified: 4},
    {name: "28.02;Fr", verified: 8, unverified: 2},
    {name: "29.02;Sa", verified: 3, unverified: 3},
    {name: "01.03;So", verified: 2, unverified: 0},
  ];

  return (
    <Container>
      <h1>{t("Betankungen")}</h1>
      <h2>{t("Anzahl der einzelnen Betankungen (DKV-verifiziert und nicht verifiziert)")}</h2>

      <BarContainer>
        <AutoSizer>
          {({height, width}) => (
            <BarChart
              height={height}
              width={width}
              data={data}
              margin={{top: 20, right: 12, left: -35, bottom: 42}}
            >
              <CartesianGrid strokeDasharray="5 5" vertical={false} />
              <XAxis dataKey="name" axisLine={false} tickLine={false} tick={<XAxisTick />} interval={0} />
              <YAxis
                domain={[0, "dataMax"]}
                axisLine={false}
                tickLine={false}
                tickCount={3}
                tick={<YAxisTick />}
              />
              <Tooltip
                cursor={{fill: "#efefef"}}
                animationDuration={100}
                animationEasing="linear"
                content={<TooltipContent />}
              />
              <Bar
                dataKey="verified"
                stackId="fuelings"
                fill="#f18400"
                shape={<Bars dataKey="verified" />}
                isAnimationActive={false}
              />
              <Bar
                dataKey="unverified"
                stackId="fuelings"
                fill="#C1C1C1"
                shape={<Bars dataKey="unverified" />}
                isAnimationActive={false}
              />
            </BarChart>
          )}
        </AutoSizer>

        <DetailsLinkAbsolute label={t("Details")} />
      </BarContainer>

      <VerifiedInfo>
        <VerifiedInfoIcon />
        {t("DKV-Verifiziert")}
      </VerifiedInfo>
    </Container>
  );
});

export default Refuelings;
