import React from "react";
import styled, {css} from "styled-components/macro";
import {useTranslation} from "react-i18next";
import {LocationArrow as LocationArrowIcon} from "@styled-icons/fa-solid/LocationArrow";

// -----------------------------------------------------------------------------------------------------------
const StatusList = styled.ul`
  margin: 18px 16px;
`;

const StatusListItem = styled.li`
  padding: 12px 0;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
`;

interface StatusListItemStatusProps {
  error?: boolean;
  warning?: boolean;
}

const StatusListItemStatus = styled.div<StatusListItemStatusProps>`
  width: 16px;
  height: 16px;
  border-radius: 50%;
  background-color: #58be58;
  margin-right: 12px;
  color: #fff;
  display: flex;
  justify-content: center;
  align-items: center;

  ${(props) =>
    props.warning &&
    css`
      background-color: #fbc02d;
    `}

  ${(props) =>
    props.error &&
    css`
      background-color: #e44c4d;
    `}
`;

const StatusListItemValue = styled.span`
  font-size: 1.4285714285714286em;
  font-weight: bold;
  margin-right: 5px;
`;

// -----------------------------------------------------------------------------------------------------------
const Status: React.FC = () => {
  const {t} = useTranslation();

  return (
    <>
      <h1>{t("Fahrzeugstatus")}</h1>
      <StatusList>
        <StatusListItem>
          <StatusListItemStatus>
            <LocationArrowIcon size="8" />
          </StatusListItemStatus>
          <StatusListItemValue>11</StatusListItemValue>
          <span>{t("Fahrzeuge verbunden")}</span>
        </StatusListItem>
        <StatusListItem>
          <StatusListItemStatus error>!</StatusListItemStatus>
          <StatusListItemValue>6</StatusListItemValue>
          <span>{t("Fahrzeuge mit Problem")}</span>
        </StatusListItem>
        <StatusListItem>
          <StatusListItemStatus warning>!</StatusListItemStatus>
          <StatusListItemValue>1</StatusListItemValue>
          <span>{t("Fahrzeug Serviceintervall Überschreitung")}</span>
        </StatusListItem>
      </StatusList>
      <h1>{t("Systemstatus")}</h1>
      <StatusList>
        <StatusListItem>
          <StatusListItemStatus />
          <span>{t("Fahrzeug-Hardware arbeitet normal")}</span>
        </StatusListItem>
      </StatusList>
    </>
  );
};

export default Status;
