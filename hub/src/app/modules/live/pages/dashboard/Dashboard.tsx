import React from "react";
import styled, {css} from "styled-components/macro";
import {useTranslation} from "react-i18next";
import Headline from "app/components/Headline";
import PageContent from "app/components/PageContent";
import Page from "app/components/Page";
import Refuelings from "./components/Refuelings";
import Status from "./components/Status";
import Alarms from "./components/Alarms";
import Warnings from "./components/Warnings";
import Infos from "./components/Infos";
import Distance from "./components/Distance";
import Downtime from "./components/Downtime";
import TopDriver from "./components/TopDriver";
import ComingSoon from "app/components/ComingSoon";

// -----------------------------------------------------------------------------------------------------------
interface BoxProps {
  alarm?: boolean;
  scroll?: boolean;
}

const Box = styled.section<BoxProps>`
  background-color: var(--dkv-background-primary-color);
  padding: 26px 32px;
  min-width: 0;
  min-height: 350px;
  overflow: auto;
  flex-shrink: 0;

  h1 {
    color: var(--dkv-text-primary-color);
    font-size: 1.4286em;
    font-weight: 500;
  }

  h2 {
    color: var(--dkv-text-caption-color);
    font-size: 0.8571em;
    margin-top: 4px;
  }

  ${(props) =>
    props.alarm &&
    css`
      padding: 0 32px 26px 0;
    `}

  ${(props) =>
    props.scroll &&
    css`
      overflow: hidden;
      padding: 0;
      display: flex;
      flex-direction: column;
    `}
`;

const BoxContainer = styled.div`
  padding: 8px 64px 24px 64px;

  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  grid-gap: 16px;
  height: 100%;

  @media (max-width: 1440px) {
    grid-template-columns: 1fr 1fr;
    padding: 8px 24px 48px 24px;
    height: auto;
  }

  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`;

// -----------------------------------------------------------------------------------------------------------
const Dashboard: React.FC = () => {
  const {t} = useTranslation();

  return (
    <Page id="dashboard">
      <ComingSoon /> {/* @todo commingsoon */}
      <Headline>{t("Dashboard")}</Headline>
      <PageContent>
        <BoxContainer>
          <Box scroll>
            <Alarms />
          </Box>

          <Box scroll>
            <Warnings />
          </Box>

          <Box scroll>
            <Infos />
          </Box>

          <Box>
            <Status />
          </Box>

          <Box>
            <Distance />
          </Box>

          <Box>
            <Downtime />
          </Box>

          <Box>
            <TopDriver />
          </Box>

          <Box>
            <Refuelings />
          </Box>
        </BoxContainer>
      </PageContent>
    </Page>
  );
};

export default Dashboard;
