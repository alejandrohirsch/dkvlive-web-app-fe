import React, {useEffect, useState, useCallback} from "react";
import {useTranslation} from "react-i18next";
import Page from "app/components/Page";
import {useDispatch, useSelector} from "react-redux";
import {Button} from "dkv-live-frontend-ui";
import DataTable, {
  TablePageContent,
  TableStickyHeader,
  TableStickyFooter,
  TableContainer,
  useColumns,
  bodyField,
  ToolbarButton,
  ToolbarButtonIcon,
  TableFooterActions,
  TableSelectionCountInfo,
  TableFooterRightActions,
} from "app/components/DataTable";
import store from "app/state/store";
import {DriversModule} from "./state/module";
import {driversListSelector} from "./state/selectors";
import {loadDriversIfNeeded} from "./state/actions";
import {Driver} from "./state/reducer";
import Window from "app/components/Window";
import DriverDetails from "./components/DriverDetails";
import {useParams} from "react-router-dom";
import {resolveCountryName} from "services/countries";

// -----------------------------------------------------------------------------------------------------------
store.addModule(DriversModule);

// -----------------------------------------------------------------------------------------------------------
const driversColumns = [
  {
    field: "card.card_number",
    header: "Fahrerkarte",
    width: 250,
    sortable: true,
    filterable: false,
    body: bodyField((d: Driver) => d.card.card_number),
  },
  {
    field: "card.issuing_member_state_alpha",
    header: "Austellungsland Fahrerkarte",
    width: 250,
    sortable: true,
    filterable: false,
    bodyWithTranslation: (t: any) => (d: Driver) => t(resolveCountryName(d.card.issuing_member_state_alpha)),
  },
  {field: "first_name", header: "Vorname", width: 200, sortable: true, filterable: false},
  {field: "last_name", header: "Nachname", width: 200, sortable: true, filterable: false},
  {
    field: "_currentOrganization.phone_number",
    header: "Telefonnummer",
    width: 150,
    sortable: true,
    filterable: false,
  },
  // {
  //   field: "drivingTimeDay",
  //   header: "Tageslenkzeit",
  //   body: bodyField((r: any) => formatTimeInHours(r.drivingTimeDay), "h"),
  //   width: 150,
  //   sortable: true,
  // },
  // {
  //   field: "drivingTimeDayRemaining",
  //   header: "Tagesrestlenkzeit",
  //   body: bodyField((r: any) => formatTimeInHours(r.drivingTimeDayRemaining), "h"),
  //   width: 150,
  //   sortable: true,
  // },
  // {
  //   field: "drivingTimeSincePause",
  //   header: "Lenkzeit seit Pause",
  //   body: bodyField((r: any) => formatTimeInHours(r.drivingTimeSincePause), "h"),
  //   width: 150,
  //   sortable: true,
  // },
  // {
  //   field: "drivingTimeRemainingTilPause",
  //   header: "Lenkzeit bis Pause",
  //   body: bodyField((r: any) => formatTimeInHours(r.drivingTimeRemainingTilPause), "h"),
  //   width: 150,
  //   sortable: true,
  // },
];

// -----------------------------------------------------------------------------------------------------------
const Drivers: React.FC = () => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // route
  const {id: queryDriverID} = useParams();

  // load drivers
  const driversList = useSelector(driversListSelector);

  useEffect(() => {
    dispatch(loadDriversIfNeeded());
  }, [dispatch]);

  // actions
  // - details
  const [showDetailWindow, setShowDetailWindow] = useState<Driver | null>(null);
  const onCloseDetailWindow = useCallback(() => setShowDetailWindow(null), [setShowDetailWindow]);
  const openDetailWindow = useCallback((e: any) => setShowDetailWindow(e.data as Driver), [
    setShowDetailWindow,
  ]);

  useEffect(() => {
    if (!showDetailWindow) {
      return;
    }

    const driver = driversList.find((d) => d.id === showDetailWindow.id);

    if (driver) {
      setShowDetailWindow(driver);
    }
  }, [driversList, showDetailWindow]);

  // open window from url
  useEffect(() => {
    if (!queryDriverID) {
      return;
    }

    const driver = driversList.find((d) => d.card.driver_id_full === queryDriverID);

    if (driver) {
      setShowDetailWindow(driver);
    }
  }, [driversList, queryDriverID]);

  // columns
  const [columns] = useState(driversColumns);
  const visibleColumns = useColumns(columns, t);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  const openDetailsWindowForSelected = useCallback(
    () => selected.length && setShowDetailWindow(selected[0]),
    [selected, setShowDetailWindow]
  );

  // render
  return (
    <Page id="drivers">
      <TablePageContent>
        <TableStickyHeader>
          <h1>{driversList?.length} Fahrer</h1>

          {/* @todo commingsoon */}
          {/* <SearchTextField
            name="search"
            type="text"
            required
            label={t("Suche")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          /> */}
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={driversList}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            onRowDoubleClick={openDetailWindow}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo count={selected.length} label={t("Fahrer ausgewählt")} />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>
              <TableFooterActions>
                <ToolbarButton onClick={openDetailsWindowForSelected}>
                  <ToolbarButtonIcon>
                    <span className="icon-ico_fleet" />
                  </ToolbarButtonIcon>
                  {t("Details")}
                </ToolbarButton>
              </TableFooterActions>
              <TableFooterRightActions />
            </>
          )}

          {/* <TableFooterRightActions>
            <Button autoWidth secondary ariaLabel={t("Gesamte Liste exportieren")}>
              {t("Gesamte Liste exportieren")}
            </Button>

            {selected.length > 0 && (
              <Button autoWidth ariaLabel={t("Auswahl als Liste exportieren")}>
                {t("Auswahl als Liste exportieren")}
              </Button>
            )}
          </TableFooterRightActions> */}
        </TableStickyFooter>
      </TablePageContent>

      {showDetailWindow && (
        <Window onClose={onCloseDetailWindow} headline={t("Fahrer")}>
          {(props) => <DriverDetails {...props} driver={showDetailWindow} />}
        </Window>
      )}
    </Page>
  );
};

export default Drivers;
