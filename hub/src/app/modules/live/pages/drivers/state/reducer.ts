import {createReducer} from "@reduxjs/toolkit";
import {
  LoadingDriversFailedAction,
  DriversLoadedAction,
  LOADING_DRIVERS,
  LOADING_DRIVERS_FAILED,
  DRIVERS_LOADED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface Driver {
  id: string;
  first_name: string | null;
  last_name: string | null;
  date_of_birth: string | null;
  current_organization_id: string;
  current_division_id: string;
  created_at: string;
  modified_at: string;
  card: Card;
  organizations: Organization[];
  organization_history: ActiveOrganization[];

  _currentOrganization?: Organization;
}

interface Card {
  driver_id_full: string;
  issuing_member_state: string;
  issuing_member_state_alpha: string;
  driver_id: string;
  card_number: string;
}

interface Organization {
  organization_id: string;
  division_id: string[];
  phone_number: string | null;
  driving_license_number: string | null;
  address: Address;
}

interface Address {
  street: string | null;
  house_number: string | null;
  address_addition: string | null;
  zip: string | null;
  place: string | null;
  country_code: string | null;
}

interface ActiveOrganization {
  organization_id: string;
  start_at: string;
}

// -----------------------------------------------------------------------------------------------------------
export interface DriversState {
  loading: boolean;
  loaded?: boolean;
  error?: boolean;
  errorText?: string;
  items?: {[id: string]: Driver};
}

const initialState: DriversState = {
  loading: true,
};

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingDrivers(state: DriversState) {
  state.loading = true;
  state.loaded = false;

  if (state.error) {
    delete state.error;
    delete state.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingDriversFailed(state: DriversState, action: LoadingDriversFailedAction) {
  state.loading = false;
  state.error = true;
  state.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleDriversLoaded(state: DriversState, action: DriversLoadedAction) {
  state.loading = false;
  state.loaded = true;

  const items = {};
  action.payload.forEach((a) => {
    a._currentOrganization = a.organizations
      ? a.organizations.find((o) => o.organization_id === a.current_organization_id)
      : undefined;

    items[a.id] = a;
  });

  state.items = items;

  if (state.error) {
    delete state.error;
    delete state.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export const driversReducer = createReducer(initialState, {
  [LOADING_DRIVERS]: handleLoadingDrivers,
  [LOADING_DRIVERS_FAILED]: handleLoadingDriversFailed,
  [DRIVERS_LOADED]: handleDriversLoaded,
});
