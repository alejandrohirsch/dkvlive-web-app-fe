import {Driver} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_DRIVERS = "map/loadingDrivers";

export interface LoadingDriversAction {
  type: typeof LOADING_DRIVERS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_DRIVERS_FAILED = "map/loadingDriversFailed";

export interface LoadingDriversFailedAction {
  type: typeof LOADING_DRIVERS_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const DRIVERS_LOADED = "map/driversLoaded";

export interface DriversLoadedAction {
  type: typeof DRIVERS_LOADED;
  payload: Driver[];
}

// -----------------------------------------------------------------------------------------------------------
export type DriversActions = LoadingDriversAction | LoadingDriversFailedAction | DriversLoadedAction;
