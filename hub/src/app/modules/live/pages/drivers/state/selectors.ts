import {DriversModuleState} from "./module";
import {createSelector} from "@reduxjs/toolkit";

export const driversItemsSelector = (state: DriversModuleState) => state.drivers.items;

// list
export const driversListSelector = createSelector(driversItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// driver
export const driverSelector = (id: string) =>
  createSelector(driversItemsSelector, (items) => (items ? items[id] : undefined));
