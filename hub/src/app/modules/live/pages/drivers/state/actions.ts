import {DriversThunkResult} from "./module";
import {Driver} from "./reducer";
import httpClient from "services/http";
import {
  LoadingDriversAction,
  LOADING_DRIVERS,
  DriversLoadedAction,
  DRIVERS_LOADED,
  LoadingDriversFailedAction,
  LOADING_DRIVERS_FAILED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingDriversAction(): LoadingDriversAction {
  return {
    type: LOADING_DRIVERS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingDriversFailedAction(errorText?: string): LoadingDriversFailedAction {
  return {
    type: LOADING_DRIVERS_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function driversLoadedAction(items: Driver[]): DriversLoadedAction {
  return {
    type: DRIVERS_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadDrivers(): DriversThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingDriversAction());

    const [{data}, err] = await httpClient.get("/management/drivers");
    if (err !== null) {
      // @todo: log
      dispatch(loadingDriversFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(driversLoadedAction(data));
  };
}

export function loadDriversIfNeeded(): DriversThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().drivers;
    const isLoaded = data.items !== undefined && data.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadDrivers());
  };
}
