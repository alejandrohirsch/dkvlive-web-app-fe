import {IModule} from "redux-dynamic-modules";
import {DriversActions} from "./types";
import {ThunkAction} from "redux-thunk";
import {DriversState, driversReducer} from "./reducer";

export interface DriversModuleState {
  drivers: DriversState;
}

export type DriversThunkResult<R> = ThunkAction<R, DriversModuleState, void, DriversActions>;

export const DriversModule: IModule<DriversModuleState> = {
  id: "drivers",
  reducerMap: {
    drivers: driversReducer,
  },
};
