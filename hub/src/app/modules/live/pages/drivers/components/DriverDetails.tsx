import React, {useState} from "react";
import {Driver} from "../state/reducer";
import Window, {
  WindowContentContainer,
  WindowHeadline,
  WindowTabsContainer,
  WindowTabs,
  WindowContent,
  WindowContentProps,
  WindowActions,
} from "app/components/Window";
import {Tab} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components";
import {ToolbarButton, ToolbarButtonIcon} from "app/components/DataTable";
import {clone} from "lodash";
import DriverForm from "./DriverForm";
import {resolveCountryName} from "services/countries";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
export const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

export const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

export const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

// -----------------------------------------------------------------------------------------------------------
interface DriverData {
  driving_license_number: string;
  phone_number: string;
  zip: string;
  place: string;
  country_code: string;
  house_number: string;
  street: string;
  address_addition: string;
}

interface DriverDetailsProps extends WindowContentProps {
  driver: Driver;
}

const DriverDetails: React.FC<DriverDetailsProps> = React.memo(({driver}) => {
  const {t} = useTranslation();

  // tab
  const [tab, setTab] = useState(0);

  const handleTabChange = (_: React.ChangeEvent<unknown>, newTab: number) => {
    setTab(newTab);
  };

  // eidt
  const [showEditWindow, setShowEditWindow] = useState<Driver | null>(null);
  const onCloseEditWindow = () => setShowEditWindow(null);
  const openEditWindow = () => setShowEditWindow(clone(driver));

  // render
  return (
    <WindowContentContainer minWidth="700px" full>
      <WindowHeadline padding>
        <h1>
          {driver.last_name} {driver.first_name}
        </h1>
      </WindowHeadline>

      <WindowTabsContainer>
        <WindowTabs value={tab} onChange={handleTabChange} variant="scrollable">
          <Tab label={t("Übersicht")} />
          <Tab label={t("Adresse")} />
        </WindowTabs>
      </WindowTabsContainer>

      <WindowContent padding>
        {tab === 0 && (
          <Table>
            <tbody>
              <tr>
                <TableLabel>{t("Fahrerkarte")}:</TableLabel>
                <TableValue>{driver.card.card_number}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Vorname")}:</TableLabel>
                <TableValue>{driver.first_name}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Nachname")}:</TableLabel>
                <TableValue>{driver.last_name}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Geburtstag")}:</TableLabel>
                <TableValue>{driver.date_of_birth}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Telefonnummer")}:</TableLabel>
                <TableValue>{driver._currentOrganization?.phone_number}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Führerscheinnummer")}:</TableLabel>
                <TableValue>{driver._currentOrganization?.driving_license_number}</TableValue>
              </tr>
            </tbody>
          </Table>
        )}

        {tab === 1 && (
          <Table>
            <tbody>
              <tr>
                <TableLabel>{t("Straße")}:</TableLabel>
                <TableValue>
                  {driver._currentOrganization?.address.house_number}{" "}
                  {driver._currentOrganization?.address.street}
                </TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Zusatz")}:</TableLabel>
                <TableValue>{driver._currentOrganization?.address.address_addition}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("PLZ")}:</TableLabel>
                <TableValue>{driver._currentOrganization?.address.zip}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Ort")}:</TableLabel>
                <TableValue>{driver._currentOrganization?.address.place}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Land")}:</TableLabel>
                <TableValue>
                  {t(resolveCountryName(driver._currentOrganization?.address.country_code ?? ""))}
                </TableValue>
              </tr>
              <tr>
                <TableLabel>&nbsp;</TableLabel>
                <TableValue>&nbsp;</TableValue>
              </tr>
            </tbody>
          </Table>
        )}

        <WindowActions>
          {hasPermission("management:EditDrivers") && (
            <ToolbarButton type="button" onClick={openEditWindow}>
              <ToolbarButtonIcon>
                <span className="icon-ico_edit" />
              </ToolbarButtonIcon>
              {t("Bearbeiten")}
            </ToolbarButton>
          )}
        </WindowActions>
      </WindowContent>

      {showEditWindow && (
        <Window onClose={onCloseEditWindow} headline={t("Fahrer")}>
          {(props) => <DriverForm {...props} driver={showEditWindow} />}
        </Window>
      )}
    </WindowContentContainer>
  );
});

export default DriverDetails;
