import React, {useState} from "react";
import {Driver} from "../state/reducer";
import {
  WindowContentContainer,
  WindowHeadline,
  WindowContent,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import {TextField, Message, Button} from "dkv-live-frontend-ui";
import Autocomplete from "@material-ui/lab/Autocomplete";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components";
import {useFormik} from "formik";
import httpClient from "services/http";
import {loadDrivers} from "../state/actions";
import {useDispatch} from "react-redux";
import {countryOptions, countries} from "services/countries";

// -----------------------------------------------------------------------------------------------------------
export const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

export const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

export const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

// -----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const ErrorMessage = styled(Message)`
  margin-bottom: 24px;
`;

// -----------------------------------------------------------------------------------------------------------
interface DriverData {
  driving_license_number: string;
  phone_number: string;
  zip: string;
  place: string;
  country_code: string;
  house_number: string;
  street: string;
  address_addition: string;
}

interface DriverFormProps extends WindowContentProps {
  driver: Driver;
}

const DriverForm: React.FC<DriverFormProps> = React.memo(({driver, setLoading, forceCloseWindow}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // form
  const [error, setError] = useState("");

  const initialValues = {
    driving_license_number: driver._currentOrganization?.driving_license_number ?? "",
    phone_number: driver._currentOrganization?.phone_number ?? "",
    zip: driver._currentOrganization?.address.zip ?? "",
    place: driver._currentOrganization?.address.place ?? "",
    country_code: driver._currentOrganization?.address.country_code ?? "",
    house_number: driver._currentOrganization?.address.house_number ?? "",
    street: driver._currentOrganization?.address.street ?? "",
    address_addition: driver._currentOrganization?.address.address_addition ?? "",
  };

  const form = useFormik<DriverData>({
    initialValues: initialValues,
    onSubmit: (values) => {
      const organizationData = {
        phone_number: values.phone_number,
        driving_license_number: values.driving_license_number,
        address: {
          street: values.street,
          house_number: values.house_number,
          address_addition: values.address_addition,
          zip: values.zip,
          place: values.place,
          country_code: values.country_code,
        },
      };

      setLoading(true, true, t("wird gespeichert"));
      setError("");

      const submit = async () => {
        const [, err] = await httpClient.put(
          `/management/drivers/${driver.id}/organization`,
          JSON.stringify(organizationData),
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        if (err !== null) {
          setError("Änderung fehlgeschlagen");
          setLoading(false);
          form.setSubmitting(false);
          return;
        }

        dispatch(loadDrivers());
        setLoading(false);
        form.setSubmitting(false);
        forceCloseWindow();
      };

      submit();
    },
  });

  // render
  return (
    <WindowContentContainer minWidth="700px" full>
      <WindowHeadline padding>
        <h1>
          {driver.last_name} {driver.first_name}
        </h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {error && <ErrorMessage text={t(error)} error />}
          <Table>
            <tbody>
              <tr>
                <TableValue>
                  <TextField
                    label={t("Fahrerkarte")}
                    type="text"
                    value={driver.card.card_number}
                    disabled
                    variant="outlined"
                  />
                </TableValue>
              </tr>
              <tr>
                <TableValue>
                  <TextField
                    label={t("Vorname")}
                    type="text"
                    value={driver.first_name}
                    disabled
                    variant="outlined"
                  />
                </TableValue>
              </tr>
              <tr>
                <TableValue>
                  <TextField
                    label={t("Nachname")}
                    type="text"
                    value={driver.last_name}
                    disabled
                    variant="outlined"
                  />
                </TableValue>
              </tr>
              <tr>
                <TableValue>
                  <TextField
                    label={t("Geburtstag")}
                    type="text"
                    value={driver.date_of_birth}
                    disabled
                    variant="outlined"
                  />
                </TableValue>
              </tr>
              <tr>
                <TableValue>
                  <TextField
                    name="phone_number"
                    type="text"
                    label={t("Telefonnummer")}
                    value={form.values.phone_number}
                    onChange={form.handleChange}
                    disabled={form.isSubmitting}
                    variant="outlined"
                  />
                </TableValue>
              </tr>
              <tr>
                <TableValue>
                  <TextField
                    name="driving_license_number"
                    type="text"
                    label={t("Führerscheinnummer")}
                    value={form.values.driving_license_number}
                    onChange={form.handleChange}
                    disabled={form.isSubmitting}
                    variant="outlined"
                  />
                </TableValue>
              </tr>
              <tr>
                <td style={{marginTop: 12}}>&nbsp;</td>
              </tr>
              <tr>
                <TableValue>
                  <TextField
                    name="street"
                    type="text"
                    label={t("Straße")}
                    value={form.values.street}
                    onChange={form.handleChange}
                    disabled={form.isSubmitting}
                    variant="outlined"
                    style={{width: "74%"}}
                  />
                  <TextField
                    name="house_number"
                    type="text"
                    label={t("Hausnummer")}
                    value={form.values.house_number}
                    onChange={form.handleChange}
                    disabled={form.isSubmitting}
                    variant="outlined"
                    style={{width: "25%", marginLeft: 5}}
                  />
                </TableValue>
              </tr>
              <tr>
                <TableValue>
                  <TextField
                    name="address_addition"
                    type="text"
                    label={t("Adresszusatz")}
                    value={form.values.address_addition}
                    onChange={form.handleChange}
                    disabled={form.isSubmitting}
                    variant="outlined"
                  />
                </TableValue>
              </tr>
              <tr>
                <TableValue>
                  <TextField
                    name="zip"
                    type="text"
                    label={t("PLZ")}
                    value={form.values.zip}
                    onChange={form.handleChange}
                    disabled={form.isSubmitting}
                    variant="outlined"
                  />
                </TableValue>
              </tr>
              <tr>
                <TableValue>
                  <TextField
                    name="place"
                    type="text"
                    label={t("Ort")}
                    value={form.values.place}
                    onChange={form.handleChange}
                    disabled={form.isSubmitting}
                    variant="outlined"
                  />
                </TableValue>
              </tr>
              <tr>
                <TableValue>
                  <Autocomplete
                    value={{
                      value: form.values.country_code,
                      label: countries[form.values.country_code] ?? "",
                    }}
                    onChange={(_: any, newValue: {label: string; value: string} | null) => {
                      form.setFieldValue("country_code", newValue ? newValue.value : "");
                    }}
                    options={countryOptions}
                    autoHighlight
                    getOptionLabel={(option) => t(option.label)}
                    renderOption={(option) => (
                      <>
                        {t(option.label)} ({option.value})
                      </>
                    )}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Land"
                        variant="outlined"
                        inputProps={{
                          ...params.inputProps,
                          autoComplete: "new-password",
                        }}
                      />
                    )}
                    clearText={t("Leeren")}
                    closeText={t("Schließen")}
                    loadingText={t("Lädt...")}
                    noOptionsText={t("Keine Optionen")}
                    openText={t("Öffnen")}
                  />
                </TableValue>
              </tr>
            </tbody>
          </Table>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
});

export default DriverForm;
