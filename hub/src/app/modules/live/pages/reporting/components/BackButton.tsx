import React from "react";
import styled from "styled-components/macro";
import {Link} from "react-router-dom";

// -----------------------------------------------------------------------------------------------------------

export const StickyHeader = styled.div`
  flex-shrink: 0;
  background-color: #ffffff;
  margin-top: 33px;
  margin-bottom: -33px;
  padding: 28px;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  justify-content: flex-start;
  height: 30px;
`;

export const Container = styled(Link)`
  color: var(--dkv-link-primary-color);
  line-height: 1.25;
  font-size: 1.2em;
  font-weight: 500;
  transition: color linear 0.1s;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  flex-grow: 0;
  justify-content: flex-end;
  align-items: center;

  &:hover,
  &:focus {
    color: var(--dkv-link-primary-hover-color);
  }
`;

// -----------------------------------------------------------------------------------------------------------
const BackButton: React.FC<{backPath: string}> = ({backPath}) => {
  return (
    <StickyHeader>
      <Container to={`${backPath}`}>
        <span className={"icon-ico_back"} />
        {"Zurück"}
      </Container>
    </StickyHeader>
  );
};

export default BackButton;
