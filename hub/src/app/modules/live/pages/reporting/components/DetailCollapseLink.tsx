import React from "react";
import styled from "styled-components/macro";

// -----------------------------------------------------------------------------------------------------------
export const Container = styled.button`
  color: var(--dkv-link-primary-color);
  line-height: 1.25;
  font-size: 1em;
  font-weight: 500;
  transition: color linear 0.1s;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  flex-grow: 0;
  justify-content: flex-end;
  align-items: center;

  &:hover,
  &:focus {
    color: var(--dkv-link-primary-hover-color);
  }

  span {
    font-size: 12px;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface DetailCollapseLinkProps {
  labelCollapsed: string;
  labelDecollapsed: string;
  collapsed: boolean;
  onClick: (collapsed: any) => void;
  className?: string;
}

const DetailCollapseLink: React.FC<DetailCollapseLinkProps> = ({
  labelCollapsed,
  labelDecollapsed,
  collapsed,
  onClick,
  className,
}) => {
  return (
    <Container className={className} onClick={onClick}>
      {collapsed ? labelCollapsed : labelDecollapsed}
      <span className={collapsed ? "icon-ico_chevron-up" : "icon-ico_chevron-down"} />
    </Container>
  );
};

export default DetailCollapseLink;
