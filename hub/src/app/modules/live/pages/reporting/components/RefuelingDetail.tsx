import React from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components";
import BarChart from "app/components/BarChart";
// -----------------------------------------------------------------------------------------------------------
const StatsContainer = styled.div`
  background-color: #ffffff;
  margin: 0;
  display: flex;
  padding: 0 28px;
`;

const StatsComponent = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: flex-start;
  padding-right: 28px;
  font-size: 16px;
  h1 {
    color: #2e6b90;
    font-size: 24px;
  }
`;

const LegendItem = styled.div`
  font-size: 0.8571428571428571em;
  color: #909090;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  line-height: 2em;
`;

const LegendItemIcon = styled.div`
  width: 10px;
  height: 10px;
  border-radius: 50%;
  margin-right: 5px;
`;

// -----------------------------------------------------------------------------------------------------------
const RefuelingDetail: React.FC = () => {
  const {t} = useTranslation();
  const refuelingStats = [
    {name: "24.02;Mo", verified: 8, unverified: 2},
    {name: "25.02;Di", verified: 6, unverified: 4},
    {name: "26.02;Mi", verified: 6, unverified: 1},
    {name: "27.02;Do", verified: 5, unverified: 4},
    {name: "28.02;Fr", verified: 8, unverified: 2},
    {name: "29.02;Sa", verified: 3, unverified: 3},
    {name: "heute", verified: 3, unverified: 3},
  ];
  const keys = [
    {
      key: "verified",
      label: t("DKV-Betankungen"),
      color: "var(--dkv-highlight-primary-color)",
    },
    {
      key: "unverified",
      label: t("restliche Betankungen"),
      color: "var(--dkv-nav-primary-color)",
    },
  ];

  const recordToday = refuelingStats.find((s) => s.name === "heute");
  const statsToday = recordToday
    ? {
        unverified: recordToday.unverified,
        verified: recordToday.verified,
        totals: recordToday.unverified + recordToday.verified,
        verified_percent: (recordToday.verified / (recordToday.unverified + recordToday.verified)) * 100,
      }
    : {unverified: 0, verified: 0, totals: 0, verified_percent: 0};

  return (
    <StatsContainer>
      {/* totals */}
      <StatsComponent style={{width: "200px"}}>
        <h1>
          {statsToday.totals} {t("Mal")}
        </h1>
        {t("wurde heute insgesamt getankt.")}
      </StatsComponent>
      <StatsComponent style={{width: "200px"}}>
        <h1>{statsToday.verified_percent}%</h1>
        {t("davon")}
        <br />
        {t("DKV verifiziert.")}
      </StatsComponent>
      {/* graphs */}
      <StatsComponent>
        <span className="icon-ico_chevron-left" />
      </StatsComponent>
      <StatsComponent>
        <BarChart name="refuelings" width={300} height={210} data={refuelingStats} keys={keys} />
      </StatsComponent>
      <StatsComponent>
        <BarChart name="refuelings" width={300} height={210} data={refuelingStats} keys={keys} />
      </StatsComponent>
      <StatsComponent>
        <span className="icon-ico_chevron-right" />
      </StatsComponent>
      <StatsComponent>
        {keys.map((k, i) => (
          <LegendItem key={i}>
            <LegendItemIcon style={{backgroundColor: k.color}} />
            {t(k.label)}
          </LegendItem>
        ))}
      </StatsComponent>
    </StatsContainer>
  );
};

export default RefuelingDetail;
