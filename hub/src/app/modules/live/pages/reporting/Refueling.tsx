import React, {useState} from "react";
import DataTable, {
  TablePageContent,
  TableStickyHeader,
  useColumns,
  TableStickyFooter,
  TableContainer,
  bodyField,
  TableSelectionCountInfo,
} from "app/components/DataTable";
import {useTranslation} from "react-i18next";
import Page from "app/components/Page";
import {Button, TextField} from "dkv-live-frontend-ui";
import {Popover, InputAdornment, FormControlLabel} from "@material-ui/core";
import {CheckCircle as CheckCircleIcon} from "@styled-icons/boxicons-solid/CheckCircle";
import {InfoCircle as InfoCircleIcon} from "@styled-icons/boxicons-solid/InfoCircle";
import {LineChart as LineChartIcon} from "@styled-icons/boxicons-regular/LineChart";
import RefuelingDetail from "./components/RefuelingDetail";
import styled from "styled-components";
import DetailCollapseLink from "./components/DetailCollapseLink";
import Switch from "@material-ui/core/Switch";
import FuelGraph from "./assets/fuelgraph_example.jpg";

// -----------------------------------------------------------------------------------------------------------

const FilterGroup = styled.div`
  min-width: 50%;
  max-width: 75%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
  padding-bottom: 15px;
  .MuiTextField-root {
    width: 200px;
    margin-left: 1em !important;
  }
`;

const StickyMainHeader = styled(TableStickyHeader)`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-top: 70px;
`;

const StickyHeaderAddOn = styled.div`
  flex-shrink: 0;
  background-color: #ffffff;
  margin-top: -23px;
  margin-bottom: 24px;
  padding: 15px 28px 15px 28px;
`;

const FuelChartPopover = styled(Popover)`
  display: flex;
  flex-direction: column;
  font-size: 1.2em;

  h1 {
    font-size: 1.3em;
    margin-bottom: 0.5em;
    padding: 0.5em;
  }

  .MuiTextField-root {
    width: 200px;
    margin-left: 1em !important;
  }
`;
// -----------------------------------------------------------------------------------------------------------
const RefuelingFilters: React.FC<any> = ({onDkvSwitch}) => {
  const {t} = useTranslation();

  return (
    <FilterGroup>
      <div>
        <TextField
          type="date"
          InputProps={{
            startAdornment: <InputAdornment position="start">{t("Von")}</InputAdornment>,
          }}
        />
        <TextField
          type="date"
          InputProps={{
            startAdornment: <InputAdornment position="start">{t("Bis")}</InputAdornment>,
          }}
        />
      </div>
      <FormControlLabel
        control={<Switch onChange={onDkvSwitch} color="default" />}
        label={t("Nur DKV-Betankungen anzeigen")}
        style={{margin: "0 2em"}}
      />

      {/* @todo commingsoon */}
      {/* <SearchTextField
        name="search"
        type="text"
        required
        label={t("Suche")}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <SearchIcon className="icon-ico_search" />
            </InputAdornment>
          ),
        }}
      /> */}
    </FilterGroup>
  );
};

const LicensePlateCell = React.memo((props: any) => {
  const {t} = useTranslation();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleOpen = (e: any) => setAnchorEl(e.currentTarget);
  const handleClose = () => setAnchorEl(null);

  return (
    <>
      <div style={{display: "flex", justifyContent: "space-between"}}>
        <span>{props.rowData.license_plate}</span>
        <LineChartIcon onClick={handleOpen} size={20} style={{cursor: "pointer"}} />
      </div>
      <FuelChartPopover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "center",
          horizontal: "right",
        }}
      >
        <div>
          <h1>
            {t("Treibstoffdiagramm")} {t("Fahrzeug")} {props.rowData.license_plate}
          </h1>

          <div>
            <TextField
              type="date"
              InputProps={{
                startAdornment: <InputAdornment position="start">{t("Von")}</InputAdornment>,
              }}
            />
            <TextField
              type="date"
              InputProps={{
                startAdornment: <InputAdornment position="start">{t("Bis")}</InputAdornment>,
              }}
            />
          </div>
          <img src={FuelGraph} alt="Fuel graph" />
        </div>
      </FuelChartPopover>
    </>
  );
});
// -----------------------------------------------------------------------------------------------------------
interface RefuelingProps {
  backPath: string;
}

const refuelingColumns = [
  {
    field: "license_plate",
    header: "Kennzeichen",
    width: 130,
    body: (a: any) => <LicensePlateCell rowData={a} />,
  },
  {
    field: "driver",
    header: "Fahrer",
    width: 200,
  },
  {
    field: "service_station",
    header: "Servicestation",
    width: 250,
  },
  {
    field: "dkv_verified",
    header: "DKV-Verifizierung",
    width: 170,
    body: bodyField((a) => {
      switch (a.dkv_verified) {
        case 1:
          return <CheckCircleIcon size={24} style={{color: "var(--dkv-highlight-ok-color)"}} />;
        case 2:
          return <InfoCircleIcon size={24} style={{color: "var(--dkv-highlight-warning-color)"}} />;
        default:
          return "";
      }
    }),
  },
  {
    field: "card_no",
    header: "Tankkartennummer",
    width: 170,
  },
  {
    field: "litres_burnt",
    header: "Verbrannt (l)",
    width: 110,
    body: (a: any) => <span className="highlight">{a.litres_burnt ? a.litres_burnt + " l" : ""}</span>,
  },
  {
    field: "litres_refuel",
    header: "Getankt (l)",
    width: 110,
    body: (a: any) => <span className="highlight">{a.litres_refuel ? a.litres_refuel + " l" : ""}</span>,
  },
  {
    field: "litre_price",
    header: "Preis (€/l)",
    width: 110,
    body: bodyField((a) => (a.litre_price ? a.litre_price + " €/l" : "")),
  },
  {
    field: "total",
    header: "Gesamtpreis (€)",
    width: 140,
    body: (a: any) => <span className="highlight">{a.total ? a.total + " €" : ""}</span>,
  },
  {
    field: "time",
    header: "Zeit",
    width: 160,
  },
];

const Refueling: React.FC<RefuelingProps> = ({backPath}) => {
  const {t} = useTranslation();

  // dummy data
  const verified = {
    license_plate: "HH 453 TE",
    driver: "Luca Zweig",
    service_station: "Shell, D-22452, n. Freiburg",
    dkv_verified: 1,
    card_no: 70430443015634,
    litres_refuel: 203,
    litre_price: 1.14,
    total: 190.3,
  };
  const warning = {
    license_plate: "HH 453 TE",
    driver: "Luca Zweig",
    service_station: "Shell, D-22452, n. Freiburg",
    dkv_verified: 2,
    card_no: 70430443015634,
    litres_burnt: 3,
    litres_refuel: 40,
    litre_price: 1.14,
    total: 190.3,
  };
  const unverified = {
    license_plate: "HH 453 TE",
    driver: "Luca Zweig",
    dkv_verified: 0,
  };

  const refuelings = [
    {
      ...verified,
      time: "vor 45min",
    },
    {
      ...unverified,
      time: "vor 45min",
    },
    {
      ...unverified,
      time: "vor 47 min",
    },
    {
      ...warning,
      time: "vor 49 min",
    },
    {
      ...unverified,
      time: "vor 1 Std",
    },
    {
      ...verified,
      time: "vor 2 Std",
    },
    {
      ...unverified,
      time: "vor 2 Std",
    },
    {
      ...unverified,
      time: "vor 2 Std",
    },
    {
      ...verified,
      time: "vor 2 Std",
    },
    {
      ...verified,
      time: "vor 2 Std",
    },
    {
      ...unverified,
      time: "vor 2 Std",
    },
    {
      ...verified,
      time: "vor 2 Std",
    },
  ];

  // columns
  const [columns] = useState(refuelingColumns);
  const visibleColumns = useColumns(columns);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  // detail
  const [showDetail, setShowDetail] = useState(true);
  const [dkvOnly, setDkvOnly] = useState(false);

  const rowClassName = (rowData: any) => {
    return {warning: rowData.dkv_verified === 2};
  };

  // render
  return (
    <Page id="refueling">
      <TablePageContent>
        <>
          <StickyMainHeader backPath={backPath}>
            <h1>
              {refuelings.length} {t(refuelings.length === 1 ? "Betankung" : "Betankungen")}
            </h1>
            <DetailCollapseLink
              labelCollapsed={t("Detail ausblenden")}
              labelDecollapsed={t("Detail einblenden")}
              collapsed={showDetail}
              onClick={() => setShowDetail(!showDetail)}
            />
          </StickyMainHeader>
          {showDetail && (
            <StickyHeaderAddOn>
              <RefuelingDetail />
            </StickyHeaderAddOn>
          )}
          <StickyHeaderAddOn>
            <RefuelingFilters onDkvSwitch={() => setDkvOnly(!dkvOnly)} />
          </StickyHeaderAddOn>
          <TableContainer>
            <DataTable
              items={!dkvOnly ? refuelings : refuelings.filter((r) => r.dkv_verified === 1)}
              columns={visibleColumns}
              selected={selected}
              onSelectionChange={onSelectionChange}
              rowClassName={rowClassName}
            />
          </TableContainer>
        </>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Betankung ausgewählt" : "Betankungen ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>
            </>
          )}

          {/* <TableFooterRightActions>
            <Button autoWidth secondary ariaLabel={t("Gesamte Liste exportieren")}>
              {t("Gesamte Liste exportieren")}
            </Button>

            {selected.length > 0 && (
              <Button autoWidth ariaLabel={t("Auswahl als Liste exportieren")}>
                {t("Auswahl als Liste exportieren")}
              </Button>
            )}
          </TableFooterRightActions> */}
        </TableStickyFooter>
      </TablePageContent>
    </Page>
  );
};

export default Refueling;
