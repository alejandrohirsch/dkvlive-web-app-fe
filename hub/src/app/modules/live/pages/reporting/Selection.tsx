import React from "react";
import styled from "styled-components/macro";
import Page from "app/components/Page";
import Headline from "app/components/Headline";
import PageContent from "app/components/PageContent";
import {useTranslation} from "react-i18next";
import {Link} from "react-router-dom";

import LogbookIcon from "./icons/dashboard-fahrtenbuch.svg";
import DriverIcon from "./icons/dashboard-fahrer.svg";
import StandingtimeIcon from "./icons/dashboard-standzeit.svg";
import FuelIcon from "./icons/dashboard-treibstoff.svg";
import MileageIcon from "./icons/dashboard-verbrauch.svg";
import TrailerDataIcon from "./icons/dashboard-trailerdaten.svg";
import DrivingstyleIcon from "./icons/dashboard-fahrspur.svg";
import ComingSoon from "app/components/ComingSoon";

// -----------------------------------------------------------------------------------------------------------
const Box = styled.section`
  background-color: var(--dkv-background-primary-color);
  min-width: 0;
  min-height: 0;
  overflow: auto;
  flex-shrink: 0;

  a {
    padding: 26px 32px 48px 32px;
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 100%;
    width: 100%;
    color: #8c8c8c;

    h1 {
      color: #004b78;
      font-size: 1.4285714285714286rem;
      font-weight: 500;
      line-height: 1.4;
      margin-top: 20px;
    }

    p {
      margin-top: 20px;
      text-align: center;
      font-size: 1.1428571428571428rem;
      padding: 0 30px;
      color: #8c8c8c;
      font-weight: 500;
      line-height: 1.5;
    }
  }

  img {
    width: 120px;
  }

  &:hover {
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.3);
    transition: box-shadow 0.1s linear;
  }
`;

const BoxContainer = styled.div`
  padding: 8px 64px 42px 64px;

  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  grid-gap: 24px;

  @media (max-width: 1440px) {
    grid-template-columns: 1fr 1fr;
    padding: 8px 24px 48px 24px;
    height: auto;
  }

  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface SelectionProps {
  path: string;
}

const Selection: React.FC<SelectionProps> = ({path}) => {
  const {t} = useTranslation();

  // render
  return (
    <Page id="reporting">
      <ComingSoon /> {/* @todo commingsoon */}
      <Headline>{t("Auswertungen")}</Headline>
      <PageContent>
        <BoxContainer>
          <Box>
            <Link to={path}>
              <img src={LogbookIcon} alt={t("Fahrtenbuch")} />

              <h1>{t("Fahrtenbuch")}</h1>

              <p>
                {t(
                  "Behalten Sie alle gewünschten Fahrzeuge immer im Blick - in einer übersichtlichen Darstellung."
                )}
              </p>
            </Link>
          </Box>
          <Box>
            <Link to={path}>
              <img src={DriverIcon} alt={t("Wirtschaftlichkeit Fahrer")} />

              <h1>{t("Wirtschaftlichkeit Fahrer")}</h1>

              <p>
                {t(
                  "Sehen Sie hier, welche Fahrer besonders wirtschaftlich fahren und welche nicht." +
                    " z.B. Informationen wie Standzeiten bei laufendem Motor, effiziente Geschwindigkeit oder Tempomatnutzung."
                )}
              </p>
            </Link>
          </Box>
          <Box>
            <Link to={path}>
              <img src={StandingtimeIcon} alt={t("Standzeiten")} />

              <h1>{t("Standzeiten")}</h1>

              <p>
                {t(
                  "Sehen Sie detailliert für jedes einzelne Ihrer Fahrzeuge die Standzeiten an Tagen, Wochen und Monaten."
                )}
              </p>
            </Link>
          </Box>
          <Box>
            <Link to={`${path}/refueling`}>
              <img src={FuelIcon} alt={t("Treibstoff")} />

              <h1>{t("Treibstoff")}</h1>

              <p>
                {t(
                  "Sehen Sie alle Tankvorgänge auf einen Blick und vergleichen Sie diese In bestimmten Zeiträumen."
                )}
              </p>
            </Link>
          </Box>
          <Box>
            <Link to={path}>
              <img src={MileageIcon} alt={t("Verbrauch")} />

              <h1>{t("Verbrauch")}</h1>

              <p>{t("Sehen Sie die Verbräuche einzelner Fahrzeuge und die der gesamten Flotte.")}</p>
            </Link>
          </Box>
          <Box>
            <Link to={path}>
              <img src={TrailerDataIcon} alt={t("Trailerdaten")} />

              <h1>{t("Trailerdaten")}</h1>

              <p>
                {t(
                  "Erhalten Sie alle wichtigen Informationen wie beispielsweise Temparaturdaten der an Ihre Fahrzeuge angekoppelten Trailer."
                )}
              </p>
            </Link>
          </Box>
          <Box>
            <Link to={path}>
              <img src={DrivingstyleIcon} alt={t("Fahrspuranalyse")} />

              <h1>{t("Fahrspuranalyse")}</h1>

              <p>
                {t(
                  "Nutzen Sie die Anzeige der Fahrspuren Ihrer einzelnen Fahrzeuge für eine detaillierte Analyse der Steckenplanung und des Fahrverhaltens."
                )}
              </p>
            </Link>
          </Box>
        </BoxContainer>
      </PageContent>
    </Page>
  );
};

export default Selection;
