import React from "react";
import Page from "app/components/Page";
import {useRouteMatch, Switch, Route} from "react-router-dom";
import Selection from "./Selection";
import Refueling from "./Refueling";

// -----------------------------------------------------------------------------------------------------------
const Reporting: React.FC = () => {
  // route
  const {path} = useRouteMatch();

  // render
  return (
    <Page id="reporting">
      <Switch>
        <Route exact path={`${path}`}>
          <Selection path={path} />
        </Route>
        <Route path={`${path}/refueling`}>
          <Refueling backPath={path} />
        </Route>
      </Switch>
    </Page>
  );
};

export default Reporting;
