import React from "react";
import Page from "app/components/Page";
import {useRouteMatch, Switch, Route} from "react-router-dom";
import Selection from "./Selection";
import Users from "./Users";
import Policies from "./Policies";
// import System from "./System";
import Alarms from "./alarms/Alarms";
import Organization from "./Organization";

// -----------------------------------------------------------------------------------------------------------
const Management: React.FC = () => {
  // route
  const {path} = useRouteMatch();

  // render
  return (
    <Page id="management">
      <Switch>
        <Route exact path={`${path}`}>
          <Selection path={path} />
        </Route>
        <Route path={`${path}/organization`}>
          <Organization backPath={path} />
        </Route>
        <Route path={`${path}/users`}>
          <Users backPath={path} />
        </Route>
        <Route path={`${path}/policies`}>
          <Policies backPath={path} />
        </Route>
        <Route path={`${path}/policies/new`}>
          <Policies backPath={path} openCreateDialog={true} />
        </Route>
        <Route path={`${path}/alarms`}>
          <Alarms backPath={path} />
        </Route>
      </Switch>
    </Page>
  );
};

export default Management;
