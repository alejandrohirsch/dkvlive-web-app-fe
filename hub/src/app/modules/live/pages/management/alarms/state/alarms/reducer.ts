import {AlarmState} from "../reducer";
import {AlarmsLoadedAction, LoadingAlarmsFailedAction} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface Alarm {
  id: string;
  organization_id: string;
  division_id: string;
  user_id: string;
  name: string;
  type: number;
  condition: number;
  active: boolean;
  next_alert_time: number;
  remind_alert_time: number;
  alert_count: number;
  triggers: Trigger[];
  assets: string[];
  tags: string[];
  notifications: Notification[];
  created_at: string;
  modified_at: string;
}

export interface Trigger {
  id: number;
  params: string[];
}

export interface Notification {
  type: number;
  recipients: string[];
}

export interface AlarmData {
  list: {
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: number]: Alarm};
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingAlarms(state: AlarmState) {
  const data = state.alarms.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingAlarmsFailed(state: AlarmState, action: LoadingAlarmsFailedAction) {
  const data = state.alarms;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleAlarmsLoaded(state: AlarmState, action: AlarmsLoadedAction) {
  const data = state.alarms;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.forEach((a) => (items[a.id] = a));
  data.items = items;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}
