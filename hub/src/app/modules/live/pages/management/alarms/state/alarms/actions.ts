import {AlarmThunkResult} from "../module";
import {Alarm} from "./reducer";
import httpClient from "services/http";
import {
  LoadingAlarmsAction,
  LOADING_ALARMS,
  AlarmsLoadedAction,
  ALARMS_LOADED,
  LoadingAlarmsFailedAction,
  LOADING_ALARMS_FAILED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingAlarmsAction(): LoadingAlarmsAction {
  return {
    type: LOADING_ALARMS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingAlarmsFailedAction(errorText?: string, unsetItems = true): LoadingAlarmsFailedAction {
  return {
    type: LOADING_ALARMS_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function alarmsLoadedAction(items: Alarm[]): AlarmsLoadedAction {
  return {
    type: ALARMS_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadAlarms(): AlarmThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingAlarmsAction());

    const [{data}, err] = await httpClient.get("/alarm/alarms");
    if (err !== null) {
      // @todo: log
      dispatch(loadingAlarmsFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(alarmsLoadedAction(data));
  };
}

export function loadAlarmsIfNeeded(): AlarmThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().alarm.alarms;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadAlarms());
  };
}

export function deleteAlarm(id: string): AlarmThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingAlarmsAction());

    const [, err] = await httpClient.delete(`/alarm/alarms/${id}`);
    if (err !== null) {
      // @todo: log
      dispatch(loadingAlarmsFailedAction("Löschen fehlgeschlagen"));
      return;
    }

    dispatch(loadAlarms());
  };
}
