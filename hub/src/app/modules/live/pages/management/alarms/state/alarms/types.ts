import {Alarm} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ALARMS = "alarm/loadingAlarms";

export interface LoadingAlarmsAction {
  type: typeof LOADING_ALARMS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ALARMS_FAILED = "alarm/loadingAlarmsFailed";

export interface LoadingAlarmsFailedAction {
  type: typeof LOADING_ALARMS_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const ALARMS_LOADED = "alarm/alarmsLoaded";

export interface AlarmsLoadedAction {
  type: typeof ALARMS_LOADED;
  payload: Alarm[];
}

// -----------------------------------------------------------------------------------------------------------
export type AlarmActions = LoadingAlarmsAction | LoadingAlarmsFailedAction | AlarmsLoadedAction;
