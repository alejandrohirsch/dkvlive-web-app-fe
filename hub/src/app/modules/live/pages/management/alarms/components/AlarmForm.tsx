import React, {useCallback, useEffect, useState} from "react";
import Window, {
  WindowActionButtons,
  WindowContent,
  WindowContentContainer,
  WindowContentProps,
  WindowHeadline,
} from "../../../../../../components/Window";
import {useTranslation} from "react-i18next";
import {
  Checkbox,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  FormControlLabel,
  InputAdornment,
  MenuItem,
  Step,
  StepLabel,
  Stepper,
} from "@material-ui/core";
import styled from "styled-components";
import {FieldArray, FormikProvider, useFormik} from "formik";
import httpClient from "../../../../../../../services/http";
import {Button, Message, Select, TextField} from "dkv-live-frontend-ui";
import TriggerWindow from "./TriggerWindow";
import {useDispatch, useSelector} from "react-redux";
import {triggersListSelector} from "../state/triggers/selectors";
import {loadTriggersIfNeeded} from "../state/triggers/actions";
import {Field, Trigger} from "../state/triggers/reducer";
import {loadAssetsIfNeeded} from "../../../../state/assets/actions";
import {assetsListSelector} from "../../../../state/assets/selectors";
import {loadAlarms} from "../state/alarms/actions";
import {Autocomplete} from "@material-ui/lab";

// -----------------------------------------------------------------------------------------------------------
export interface AlarmEditData {
  id?: string;
  name: string;
  type: number;
  next_alert_time: number;
  remind_alert_time: number;
  condition: number;
  triggers: any[];
  tags: any[];
  assets: any[];
  active: boolean;
}

// -----------------------------------------------------------------------------------------------------------
const CustomStepper = styled(Stepper)`
  > .MuiStep-root > .MuiStepLabel-root {
    > .MuiStepLabel-iconContainer {
      > .MuiStepIcon-root {
        color: var(--dkv-stepper-inactive-color);
        > .MuiStepIcon-text {
          font-family: inherit !important;
        }
      }
      > .MuiStepIcon-root.MuiStepIcon-active {
        color: var(--dkv-stepper-active-color);
      }
      > .MuiStepIcon-root.MuiStepIcon-completed {
        color: var(--dkv-stepper-completed-color);
      }
    }
    > .MuiStepLabel-labelContainer {
      > .MuiStepLabel-label {
        font-family: inherit !important;
        color: var(--dkv-stepper-inactive-color);
      }
      > .MuiStepLabel-label.MuiStepLabel-active {
        color: var(--dkv-stepper-active-color);
      }
      > .MuiStepLabel-label.MuiStepLabel-completed {
        color: var(--dkv-stepper-completed-color);
      }
    }
  }
`;

const CustomWindowActionButtons = styled(WindowActionButtons)`
  margin-top: 16px;
  justify-content: space-between;

  button {
    min-width: auto;
  }
`;

const StepContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 22px 22px 22px;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
  padding: 0 38px;
`;

const HelpText = styled.div`
  font-weight: bold;
`;

const AddLink = styled.button`
  color: var(--dkv-link-primary-color);
  line-height: 1.25;
  font-size: 1.1426em;
  font-weight: 500;
  transition: color linear 0.1s;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  justify-content: left;
  align-items: center;

  &:hover,
  &:focus {
    color: var(--dkv-link-primary-hover-color);
  }
`;

const CardHeader = styled.div`
  margin-bottom: 16px;
`;

const TriggerList = styled.div`
  max-height: 350px;
  overflow-y: scroll;
`;

const TriggerItem = styled.div`
  border: 1px solid var(--dkv-grey_40);
  padding: 16px;
  margin-bottom: 16px;
`;

const DeleteAction = styled.div`
  margin-top: 16px;
  font-size: 14px;
  display: inline-flex;
  align-items: center;
  justify-content: flex-start;
  cursor: pointer;
  &:hover {
    color: var(--dkv-dkv_blue);
  }

  span {
    margin-right: 4px;
    font-size: 20px;
  }
`;

const FieldList = styled.div`
  margin-top: 16px;
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
`;

const TagList = styled.div`
  display: grid;
  padding: 0 38px;
`;

const AssetList = styled.div``;

const TagItem = styled(ExpansionPanel)`
  &.MuiPaper-elevation1 {
    box-shadow: none;
  }

  &.Mui-expanded {
    margin: 0 !important;
  }

  &:before {
    background-color: #ffffff !important;
  }

  .MuiIconButton-root {
    padding: 8px;
  }

  .MuiExpansionPanelSummary-root {
    padding: 0;
    min-height: 24px;

    &.Mui-expanded {
      min-height: 24px;
    }
  }

  .MuiExpansionPanelSummary-content {
    margin: 0;

    &.Mui-expanded {
      margin: 0;
    }
  }

  .MuiExpansionPanelDetails-root {
    padding: 0px 24px;
  }
`;

const Notification = styled.div`
  border: 1px solid var(--dkv-grey_40);
  padding: 16px;
  width: 100%;

  .MuiInputBase-root {
    max-width: 400px;
    height: auto;
  }
`;

// -----------------------------------------------------------------------------------------------------------
const steps = ["Meldung", "Regeln", "Zuordnung", "Benachrichtigung"];

const nextAlertTimeList = [
  {
    value: 5,
    text: "nach 5 Minuten",
  },
  {
    value: 10,
    text: "nach 10 Minuten",
  },
  {
    value: 30,
    text: "nach 30 Minuten",
  },
];

const remindAlertTimeList = [
  {
    value: 0,
    text: "deaktiviert",
  },
  {
    value: 60,
    text: "nach 1 Stunde",
  },
  {
    value: 480,
    text: "nach 8 Stunden",
  },
  {
    value: 1440,
    text: "nach 24 Stunden",
  },
  {
    value: 2880,
    text: "nach 2 Tagen",
  },
  {
    value: 7200,
    text: "nach 5 Tagen",
  },
  {
    value: 11520,
    text: "nach 8 Tagen",
  },
  {
    value: 20160,
    text: "nach 2 Wochen",
  },
];

const userPosition = [
  "Fuhrparkleiter",
  "Verwaltung",
  "Disponent",
  "Geschäftsführung",
  "Christopher Seger",
  "Sandro Walker",
  "Rupprecht Eisner",
];

interface AlarmFormProps extends WindowContentProps {
  editData?: AlarmEditData;
}

const AlarmForm: React.FC<AlarmFormProps> = ({forceCloseWindow, setLoading, headline, editData}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // load triggers
  const triggers = useSelector(triggersListSelector);

  useEffect(() => {
    dispatch(loadTriggersIfNeeded());
  }, [dispatch]);

  // load assets
  const assets = useSelector(assetsListSelector);

  useEffect(() => {
    dispatch(loadAssetsIfNeeded());
  }, [dispatch]);

  // form
  const editMode = editData !== undefined;
  const [error, setError] = useState("");
  const [initialValues, setInitialValues] = useState(
    editData ?? {
      name: "",
      type: 0,
      next_alert_time: 5,
      remind_alert_time: 0,
      condition: 0,
      triggers: [],
      tags: [],
      assets: [],
      active: true,
    }
  );

  const form = useFormik<AlarmEditData>({
    enableReinitialize: true,
    initialValues: initialValues,
    onSubmit: (values) => {
      setLoading(true, true, t(editMode ? "wird geändert" : "wird erstellt"));
      setError("");

      const submit = async () => {
        let err;
        const config = {
          headers: {
            "Content-Type": "application/json",
          },
        };

        const triggers: any[] = [];
        values.triggers.forEach((t, i) => {
          if (t !== undefined) {
            triggers.push({
              id: i,
              params: t,
            });
          }
        });

        const submitValues = Object.assign({}, values);
        submitValues.triggers = triggers;

        if (editMode) {
          [, err] = await httpClient.put(`/alarm/alarms/${values.id}`, JSON.stringify(submitValues), config);
        } else {
          [, err] = await httpClient.post("/alarm/alarms", JSON.stringify(submitValues), config);
        }

        if (err !== null) {
          setError(editMode ? "Änderung fehlgeschlagen" : "Erstellen fehlgeschlagen");
          setLoading(false);
          form.setSubmitting(false);
          return;
        }

        dispatch(loadAlarms());
        forceCloseWindow();
      };

      submit();
    },
  });

  // triggers
  const [selectedTriggers, setSelectedTriggers] = useState<any[]>([]);
  const handleTriggerSelected = useCallback(
    (id: number, params: any[]) => {
      const included = selectedTriggers.some((trigger) => {
        return trigger.id === id;
      });

      if (!included && triggers.length > 0) {
        const trigger = triggers[id - 1];
        if (!editData && triggers[id - 1].fields) {
          triggers[id - 1].fields.forEach((_, i) => {
            params[i] = "";
          });
          initialValues.triggers[id] = params;
        }
        setInitialValues({
          ...initialValues,
          triggers: initialValues.triggers,
        });
        setSelectedTriggers((selectedTriggers) => [...selectedTriggers, trigger]);
      }
    },
    [selectedTriggers, triggers, editData, initialValues]
  );

  const useMountEffect = (fun: any) => useEffect(fun, []);

  useMountEffect(() => {
    if (!editData) {
      return;
    }

    editData.triggers.forEach((t, i) => {
      if (t !== undefined) {
        handleTriggerSelected(i, t);
        console.log("TEST");
      }
    });
  });

  const handleTriggerDelete = (id: number) => {
    const index = selectedTriggers.findIndex((trigger) => trigger.id === id);
    selectedTriggers.splice(index, 1);
    setSelectedTriggers((selectedTriggers) => [...selectedTriggers]);
    console.log(selectedTriggers);

    // TODO: remove field from initialValues and values
  };

  const [showTriggerWindow, setShowTriggerWindow] = useState(false);
  const onCloseTriggerWindow = useCallback(() => setShowTriggerWindow(false), [setShowTriggerWindow]);
  const openTriggerWindow = () => setShowTriggerWindow(true);

  // tags
  const tagList: any[] = [];

  assets.forEach((a) => {
    a.tags.forEach((t) => {
      const exist = tagList.some((i) => {
        return i.name === t.name;
      });

      if (!exist) {
        tagList.push(t);
      }
    });
  });

  // steps
  const [activeStep, setActiveStep] = useState(0);
  const handleNext = () => {
    const nextStep = activeStep + 1;
    if (activeStep === steps.length - 1) {
      return;
    }
    setActiveStep(nextStep);
  };

  const handleBack = () => {
    const nextStep = activeStep - 1;
    setActiveStep(nextStep);
  };

  const handleOpenTriggerWindow = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    openTriggerWindow();
  };

  const renderSelectField = (index: number, id: number, value: string, field: Field) => {
    return (
      <Select
        name={`triggers[${id}][${index.toString()}]`}
        value={value}
        key={field.label}
        label={t(field.label)}
        onChange={form.handleChange}
      >
        {field.values &&
          field.values.map((item) => {
            return (
              <MenuItem key={item.value} value={item.value}>
                {t(item.name)}
              </MenuItem>
            );
          })}
      </Select>
    );
  };

  const renderNumberField = (index: number, id: number, value: string, field: Field) => {
    return (
      <TextField
        name={`triggers[${id}][${index.toString()}]`}
        value={value}
        key={field.label}
        type="text"
        label={t(field.label)}
        onChange={form.handleChange}
        disabled={form.isSubmitting}
        variant="outlined"
        InputProps={{
          endAdornment: <InputAdornment position="end">{t(field.values[0].name)}</InputAdornment>,
        }}
      />
    );
  };

  const renderTriggerItem = (trigger: Trigger) => {
    return (
      <TriggerItem key={trigger.id}>
        <div>
          <b>{trigger.label}</b>
        </div>
        <div>{trigger.description}</div>

        <FieldList>
          {trigger.fields &&
            trigger.fields.map((field, index) => {
              let value = "";
              if (typeof form.values.triggers[trigger.id] !== "undefined") {
                value = form.values.triggers[trigger.id][index];
              }

              switch (field.type) {
                case "select":
                  return renderSelectField(index, trigger.id, value, field);
                case "number":
                  return renderNumberField(index, trigger.id, value, field);
                default:
                  return "";
              }
            })}
        </FieldList>

        <DeleteAction onClick={() => handleTriggerDelete(trigger.id)}>
          <span className="icon-ico_delete" />
          {t("Regel löschen")}
        </DeleteAction>
      </TriggerItem>
    );
  };

  const renderStep = (stepIndex: number) => {
    switch (stepIndex) {
      case 0:
        return (
          <FormGrid>
            <HelpText></HelpText>
            <TextField
              name="name"
              type="text"
              label={t("Bezeichnung")}
              value={form.values.name}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />

            <Select name="type" value={form.values.type} onChange={form.handleChange} label={t("Vorgang")}>
              <MenuItem value={0} style={{color: "var(--dkv-highlight-error-color)"}}>
                {t("Alarm")}
              </MenuItem>
              <MenuItem value={1} style={{color: "var(--dkv-highlight-warning-color)"}}>
                {t("Hinweis")}
              </MenuItem>
              <MenuItem value={2} style={{color: "var(--dkv-highlight-info-color)"}}>
                {t("Information")}
              </MenuItem>
            </Select>

            <Select
              name="next_alert_time"
              value={form.values.next_alert_time}
              onChange={form.handleChange}
              label={t("Erneute Alarmierung frühestens")}
            >
              {nextAlertTimeList.map((i) => {
                return (
                  <MenuItem key={i.value} value={i.value}>
                    {t(i.text)}
                  </MenuItem>
                );
              })}
            </Select>

            <Select
              name="remind_alert_time"
              value={form.values.remind_alert_time}
              onChange={form.handleChange}
              label={t("Erinnerung bei durchgehend aktiver Meldung nach")}
            >
              {remindAlertTimeList.map((i) => {
                return (
                  <MenuItem key={i.value} value={i.value}>
                    {t(i.text)}
                  </MenuItem>
                );
              })}
            </Select>
          </FormGrid>
        );
      case 1:
        return (
          <FormGrid>
            <HelpText></HelpText>
            <Select
              name="condition"
              value={form.values.condition}
              onChange={form.handleChange}
              label={t("Regel-Verkettung")}
            >
              <MenuItem value={0}>{t("UND (Alarm wenn ALLE Events zutreffen)")}</MenuItem>
              <MenuItem value={1}>{t("ODER (Alarm wenn mind. ein Event zutrifft)")}</MenuItem>
            </Select>

            <TriggerList>
              {selectedTriggers.map((trigger) => {
                return renderTriggerItem(trigger);
              })}
            </TriggerList>

            <AddLink onClick={handleOpenTriggerWindow}>
              <span className="icon-ico_chevron-right" style={{fontSize: 14}} />
              {selectedTriggers.length > 0 ? t("Weitere Regel hinzufügen") : t("Regel hinzufügen")}
            </AddLink>
          </FormGrid>
        );
      case 2:
        return (
          <TagList>
            <HelpText></HelpText>
            <FormikProvider value={form}>
              <FieldArray
                name="tags"
                render={(arrayHelpers) => (
                  <div>
                    {tagList &&
                      tagList.map((tag) => {
                        return (
                          <TagItem key={tag.id}>
                            <ExpansionPanelSummary
                              expandIcon={<span className="icon-ico_chevron-down" />}
                              aria-label="Expand"
                              aria-controls="additional-actions1-content"
                              id="additional-actions1-header"
                            >
                              <FormControlLabel
                                aria-label="Acknowledge"
                                onClick={(event) => event.stopPropagation()}
                                onFocus={(event) => event.stopPropagation()}
                                control={
                                  <Checkbox
                                    name="tags"
                                    value={tag.id}
                                    checked={form.values.tags.includes(tag.id)}
                                    color="default"
                                    onChange={(e) => {
                                      if (e.target.checked) {
                                        arrayHelpers.push(tag.id);
                                      } else {
                                        const idx = form.values.tags.indexOf(tag.id);
                                        arrayHelpers.remove(idx);
                                      }
                                    }}
                                  />
                                }
                                label={tag.name}
                              />
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                              <FormikProvider value={form}>
                                <FieldArray
                                  name="assets"
                                  render={(arrayHelpers) => (
                                    <div>
                                      <AssetList>
                                        {assets &&
                                          assets.map((asset) => {
                                            if (!asset.tags.some((t) => t.name === tag.name)) {
                                              return "";
                                            }
                                            return (
                                              <FormControlLabel
                                                key={asset.id}
                                                control={
                                                  <Checkbox
                                                    name="assets"
                                                    value={asset.id}
                                                    checked={form.values.assets.includes(asset.id)}
                                                    color="default"
                                                    onChange={(e) => {
                                                      if (e.target.checked) {
                                                        arrayHelpers.push(asset.id);
                                                      } else {
                                                        const idx = form.values.assets.indexOf(asset.id);
                                                        arrayHelpers.remove(idx);
                                                      }
                                                    }}
                                                  />
                                                }
                                                label={asset.license_plate_normalized}
                                              />
                                            );
                                          })}
                                      </AssetList>
                                    </div>
                                  )}
                                />
                              </FormikProvider>
                            </ExpansionPanelDetails>
                          </TagItem>
                        );
                      })}
                  </div>
                )}
              />
            </FormikProvider>
          </TagList>
        );
      case 3:
        return (
          <FormGrid>
            <HelpText></HelpText>
            <Notification>
              <CardHeader>
                <div>
                  <b>{t("Meldung im Portal")}</b>
                </div>
                <div>
                  {t(
                    "Der Alarm wird maximal eine Stunde gespeichert. Wenn der Benutzer in dieser Zeit nicht online ist bekommt er ihn nicht zu sehen."
                  )}
                </div>
              </CardHeader>

              <Autocomplete
                multiple
                size="small"
                id="tags-standard"
                options={userPosition}
                renderInput={(params) => (
                  <TextField {...params} variant="outlined" label="Benutzer oder Position" />
                )}
              />
            </Notification>
            <Notification>
              <CardHeader>
                <div>
                  <b>{t("Meldung per E-Mail")}</b>
                </div>
                <div>{t('Mehrere Empfänger durch Komma (",") trennen.')}</div>
              </CardHeader>
              <TextField multiline variant="outlined" rows={5} />
            </Notification>
          </FormGrid>
        );
      default:
        return null;
    }
  };

  // render
  return (
    <WindowContentContainer fixedWidth="600px" full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form>
          {error && <Message text={t(error)} error />}

          <CustomStepper activeStep={activeStep} alternativeLabel>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{t(label)}</StepLabel>
              </Step>
            ))}
          </CustomStepper>

          <StepContainer>
            <div>{renderStep(activeStep)}</div>
          </StepContainer>

          <CustomWindowActionButtons>
            {activeStep === 0 ? (
              <Button type="button" secondary onClick={forceCloseWindow}>
                {t("Abbrechen")}
              </Button>
            ) : (
              <Button type="button" secondary onClick={handleBack}>
                {t("Zurück")}
              </Button>
            )}

            {activeStep === 3 ? (
              <Button
                type="button"
                onClick={() => form.submitForm()}
                disabled={form.isSubmitting || !form.values.name}
              >
                {t(editData ? "Ändern" : "Erstellen")}
              </Button>
            ) : (
              <Button type="button" onClick={handleNext} disabled={form.isSubmitting || !form.values.name}>
                {t("Weiter")}
              </Button>
            )}
          </CustomWindowActionButtons>
        </Form>
      </WindowContent>

      {showTriggerWindow && (
        <Window onClose={onCloseTriggerWindow} headline={t("Neue Regel hinzufügen")}>
          {(props) => <TriggerWindow onTriggerSelected={handleTriggerSelected} {...props} />}
        </Window>
      )}
    </WindowContentContainer>
  );
};

export default AlarmForm;
