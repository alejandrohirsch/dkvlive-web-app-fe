import {IModule} from "redux-dynamic-modules";
import {AlarmActionTypes} from "./actions";
import {ThunkAction} from "redux-thunk";
import {AlarmState, reducer} from "./reducer";

export interface AlarmModuleState {
  alarm: AlarmState;
}

export type AlarmThunkResult<R> = ThunkAction<R, AlarmModuleState, void, AlarmActionTypes>;

export const AlarmModule: IModule<AlarmModuleState> = {
  id: "alarm",
  reducerMap: {
    alarm: reducer,
  },
};
