import {AlarmThunkResult} from "../module";
import {Trigger} from "./reducer";
import httpClient from "services/http";
import {
  LoadingTriggersAction,
  LOADING_TRIGGERS,
  TriggersLoadedAction,
  TRIGGERS_LOADED,
  LoadingTriggersFailedAction,
  LOADING_TRIGGERS_FAILED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingTriggersAction(): LoadingTriggersAction {
  return {
    type: LOADING_TRIGGERS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingTriggersFailedAction(errorText?: string, unsetItems = true): LoadingTriggersFailedAction {
  return {
    type: LOADING_TRIGGERS_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function triggersLoadedAction(items: Trigger[]): TriggersLoadedAction {
  return {
    type: TRIGGERS_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadTriggers(): AlarmThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingTriggersAction());

    const [{data}, err] = await httpClient.get("/alarm/triggers");
    if (err !== null) {
      // @todo: log
      dispatch(loadingTriggersFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(triggersLoadedAction(data));
  };
}

export function loadTriggersIfNeeded(): AlarmThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().alarm.triggers;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadTriggers());
  };
}
