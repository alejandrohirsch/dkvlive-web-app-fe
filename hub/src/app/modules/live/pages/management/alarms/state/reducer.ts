import {createReducer} from "@reduxjs/toolkit";
import {TRIGGERS_LOADED, LOADING_TRIGGERS, LOADING_TRIGGERS_FAILED} from "./triggers/types";
import {
  handleLoadingTriggers,
  handleLoadingTriggersFailed,
  handleTriggersLoaded,
  TriggerData,
} from "./triggers/reducer";
import {
  AlarmData,
  handleAlarmsLoaded,
  handleLoadingAlarms,
  handleLoadingAlarmsFailed,
} from "./alarms/reducer";
import {ALARMS_LOADED, LOADING_ALARMS, LOADING_ALARMS_FAILED} from "./alarms/types";

// -----------------------------------------------------------------------------------------------------------
export interface AlarmState {
  triggers: TriggerData;
  alarms: AlarmData;
}

const initialState: AlarmState = {
  triggers: {
    list: {loading: true},
  },
  alarms: {
    list: {loading: true},
  },
};

export const reducer = createReducer(initialState, {
  // triggers
  [LOADING_TRIGGERS]: handleLoadingTriggers,
  [LOADING_TRIGGERS_FAILED]: handleLoadingTriggersFailed,
  [TRIGGERS_LOADED]: handleTriggersLoaded,
  // alarms
  [LOADING_ALARMS]: handleLoadingAlarms,
  [LOADING_ALARMS_FAILED]: handleLoadingAlarmsFailed,
  [ALARMS_LOADED]: handleAlarmsLoaded,
});
