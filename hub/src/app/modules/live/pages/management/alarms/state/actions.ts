import {TriggerActions} from "./triggers/types";
import {AlarmActions} from "./alarms/types";

export type AlarmActionTypes = TriggerActions | AlarmActions;
