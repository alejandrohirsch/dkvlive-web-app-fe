import {AlarmState} from "../reducer";
import {TriggersLoadedAction, LoadingTriggersFailedAction} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface Trigger {
  id: number;
  name: string;
  label: string;
  description: string;
  fields: Field[];
}

export interface Field {
  label: string;
  type: string;
  values: Value[];
}

export interface Value {
  value: string;
  name: string;
}

export interface TriggerData {
  list: {
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: number]: Trigger};
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingTriggers(state: AlarmState) {
  const data = state.triggers.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingTriggersFailed(state: AlarmState, action: LoadingTriggersFailedAction) {
  const data = state.triggers;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleTriggersLoaded(state: AlarmState, action: TriggersLoadedAction) {
  const data = state.triggers;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.forEach((t) => (items[t.id] = t));
  data.items = items;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}
