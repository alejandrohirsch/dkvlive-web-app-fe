import {AlarmModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const triggersItemsSelector = (state: AlarmModuleState) => state.alarm.triggers.items;

// list
export const triggersListStateSelector = (state: AlarmModuleState) => state.alarm.triggers.list;

export const triggersListSelector = createSelector(triggersItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// item
export const triggerSelector = (id: string) =>
  createSelector(triggersItemsSelector, (items) => (items ? items[id] : undefined));
