import {Trigger} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_TRIGGERS = "alarm/loadingTriggers";

export interface LoadingTriggersAction {
  type: typeof LOADING_TRIGGERS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_TRIGGERS_FAILED = "alarm/loadingTriggersFailed";

export interface LoadingTriggersFailedAction {
  type: typeof LOADING_TRIGGERS_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const TRIGGERS_LOADED = "alarm/triggersLoaded";

export interface TriggersLoadedAction {
  type: typeof TRIGGERS_LOADED;
  payload: Trigger[];
}

// -----------------------------------------------------------------------------------------------------------
export type TriggerActions = LoadingTriggersAction | LoadingTriggersFailedAction | TriggersLoadedAction;
