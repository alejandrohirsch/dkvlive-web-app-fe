import React, {MouseEventHandler, useCallback, useEffect, useState} from "react";
import Page from "../../../../../components/Page";

import {
  TableContainer,
  TablePageContent,
  TableStickyFooter,
  TableStickyHeader,
  ToolbarButton,
  ToolbarButtonIcon,
} from "../../../../../components/DataTable";
import {useTranslation} from "react-i18next";
import {FormControlLabel, MenuItem, Select} from "@material-ui/core";
import styled, {css} from "styled-components";
import Switch from "@material-ui/core/Switch";
import Window from "../../../../../components/Window";
import AlarmForm, {AlarmEditData} from "./components/AlarmForm";
import {useDispatch, useSelector} from "react-redux";
import {alarmsListSelector} from "./state/alarms/selectors";
import {deleteAlarm, loadAlarmsIfNeeded} from "./state/alarms/actions";
import dayjs from "dayjs";
import ConfirmWindow from "../../../../../components/ConfirmWindow";
import {resolveUserDateFormat, hasPermission} from "app/modules/login/state/login/selectors";
import ComingSoon from "app/components/ComingSoon";

// -----------------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------------
const TableHeader = styled(TableStickyHeader)`
  display: block;

  h1 {
    display: inline;
  }
`;

const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

const TableLabel = styled.td`
  padding-bottom: 4px;
  color: var(--dkv-grey_50);
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

const TableValue = styled.td<TableValueProps>`
  padding-bottom: 0px;
  color: var(--dkv-grey_90);

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

const AlarmFilter = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
`;

const CardContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(450px, 1fr));
  grid-template-rows: auto;
  grid-gap: 20px;
  overflow: scroll;
`;

const Card = styled.div`
  border: 1px solid var(--dkv-grey_40);
  background-color: #fff;
  padding: 20px;
  height: 190px;
`;

const AlarmAction = styled.div`
  font-size: 14px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  cursor: pointer;
  &:hover {
    color: var(--dkv-dkv_blue);
  }

  span {
    margin-left: 8px;
    font-size: 20px;
  }
`;

// -----------------------------------------------------------------------------------------------------------

interface AlarmsProps {
  backPath: string;
}

// -----------------------------------------------------------------------------------------------------------
const Alarms: React.FC<AlarmsProps> = ({backPath}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // load alarms
  const alarms = useSelector(alarmsListSelector);

  useEffect(() => {
    dispatch(loadAlarmsIfNeeded());
  }, [dispatch]);

  // alarm type filter
  const [type, setType] = React.useState("custom");
  const handleTypeChange = (event: React.ChangeEvent<{value: unknown}>) => {
    setType(event.target.value as string);
  };

  // actions
  // - alarm form
  const [showNewWindow, setShowNewWindow] = useState(false);
  const onCloseNewWindow = useCallback(() => setShowNewWindow(false), [setShowNewWindow]);
  const openNewWindow = () => {
    setEditData(undefined);
    setShowNewWindow(true);
  };

  // - delete alarm
  const [deleteID, setDeleteID] = useState("");

  const showDeleteConfirm: MouseEventHandler<HTMLDivElement> = useCallback(
    (e) => {
      setDeleteID((e.currentTarget as any).dataset.id as string);
    },
    [setDeleteID]
  );

  const onDeleteConfirm = useCallback(
    (confirmed: boolean) => {
      if (!confirmed) {
        setDeleteID("");
        return;
      }

      dispatch(deleteAlarm(deleteID));
      setDeleteID("");
    },
    [deleteID, setDeleteID, dispatch]
  );

  // - edit alarm
  const [editData, setEditData] = useState<AlarmEditData | undefined>();

  const showEditWindow: MouseEventHandler<HTMLDivElement> = useCallback(
    (e) => {
      if (!alarms) {
        return;
      }

      const id = (e.currentTarget as any).dataset.id as string;
      const alarm = alarms.find((a) => a.id === id);
      if (!alarm) {
        return;
      }

      const triggers: any[] = [];
      alarm.triggers.forEach((t) => {
        triggers[t.id] = t.params;
      });

      setEditData({
        id: alarm.id,
        name: alarm.name,
        type: alarm.type,
        next_alert_time: alarm.next_alert_time,
        remind_alert_time: alarm.remind_alert_time,
        condition: alarm.condition,
        triggers: triggers,
        tags: alarm.tags,
        assets: alarm.assets,
        active: alarm.active,
      });
      setShowNewWindow(true);
    },
    [setEditData, alarms]
  );

  // render
  return (
    <Page id="alarms">
      <ComingSoon /> {/* @todo commingsoon */}
      <TablePageContent>
        <TableHeader backPath={backPath}>
          <h1>{t("Meldungen")}</h1>

          <AlarmFilter>
            <Select onChange={handleTypeChange} value={type} style={{minWidth: 250}}>
              <MenuItem value="predefined">{t("Vordefinierte Meldungen")}</MenuItem>
              <MenuItem value="custom">{t("Eigene Meldungen")}</MenuItem>
            </Select>

            {/* @todo commingsoon */}
            {/* <SearchTextField
              name="search"
              type="text"
              required
              label={t("Suche")}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <SearchIcon className="icon-ico_search" />
                  </InputAdornment>
                ),
              }}
            /> */}

            {hasPermission("alarm:CreateAlarm") && (
              <ToolbarButton general onClick={openNewWindow}>
                <ToolbarButtonIcon>
                  <span className="icon-ico_notifications" />
                </ToolbarButtonIcon>
                {t("Eigene Meldung")}
              </ToolbarButton>
            )}
          </AlarmFilter>
        </TableHeader>

        <TableContainer>
          {type === "predefined" && <div></div>}

          {type === "custom" && (
            <CardContainer>
              {alarms &&
                alarms.map((alarm) => {
                  return (
                    <Card key={alarm.id}>
                      <Table>
                        <tbody>
                          <tr>
                            <TableLabel>{t("Bezeichnung")}:</TableLabel>
                            <TableValue>{alarm.name}</TableValue>
                          </tr>
                          <tr>
                            <TableLabel>{t("Erstellt am")}:</TableLabel>
                            <TableValue style={{width: "100%"}}>
                              {dayjs(alarm.created_at).format(resolveUserDateFormat())}
                            </TableValue>
                            <TableValue>
                              <FormControlLabel
                                control={<Switch size="small" checked={alarm.active} color="default" />}
                                label={t("Aktiv")}
                                labelPlacement="start"
                                style={{color: "var(--dkv-dkv_blue)"}}
                              />
                            </TableValue>
                          </tr>
                          <tr>
                            <TableLabel>{t("Benutzer")}:</TableLabel>
                            <TableValue>Nicole Theiss</TableValue>
                          </tr>
                          <tr>
                            <TableLabel>{t("Fahrzeuge")}:</TableLabel>
                            <TableValue>{alarm.assets && alarm.assets.length}</TableValue>
                          </tr>
                          <tr>
                            <TableLabel>{t("Ausgelöst")}:</TableLabel>
                            <TableValue>5</TableValue>

                            {hasPermission("alarm:DeleteAlarm") && (
                              <TableValue>
                                <AlarmAction data-id={alarm.id} onClick={showDeleteConfirm}>
                                  {t("Löschen")}
                                  <span className="icon-ico_delete" />
                                </AlarmAction>
                              </TableValue>
                            )}
                            {/*
                            <TableValue>
                              <AlarmAction>
                                {t("Aktivität")}
                                <span className="icon-ico_clock" />
                              </AlarmAction>
                            </TableValue>
                            */}
                          </tr>
                          <tr>
                            <TableValue colSpan={2}>
                              <Select style={{minWidth: 150}} value={alarm.type}>
                                <MenuItem value={0} style={{color: "var(--dkv-highlight-error-color)"}}>
                                  {t("Alarm")}
                                </MenuItem>
                                <MenuItem value={1} style={{color: "var(--dkv-highlight-warning-color)"}}>
                                  {t("Hinweis")}
                                </MenuItem>
                                <MenuItem value={2} style={{color: "var(--dkv-highlight-info-color)"}}>
                                  {t("Information")}
                                </MenuItem>
                              </Select>
                            </TableValue>
                            <TableValue>
                              {hasPermission("i18n:EditAlarm") && (
                                <AlarmAction data-id={alarm.id} onClick={showEditWindow}>
                                  {t("Bearbeiten")}
                                  <span className="icon-ico_edit" />
                                </AlarmAction>
                              )}
                            </TableValue>
                          </tr>
                        </tbody>
                      </Table>
                    </Card>
                  );
                })}
            </CardContainer>
          )}
        </TableContainer>

        <TableStickyFooter>
          <div style={{marginRight: "20px"}}>
            <b>{alarms.length}</b>
            <br /> {t("Alarme insgesamt")}
          </div>
        </TableStickyFooter>
      </TablePageContent>
      {showNewWindow && (
        <Window
          onClose={onCloseNewWindow}
          headline={t(editData ? "Eigene Meldung ändern" : "Eigene Meldung erstellen")}
        >
          {(props) => <AlarmForm editData={editData} {...props} />}
        </Window>
      )}
      {deleteID && <ConfirmWindow headline={t("Alarm wirklich löschen?")} onConfirm={onDeleteConfirm} />}
    </Page>
  );
};

export default Alarms;
