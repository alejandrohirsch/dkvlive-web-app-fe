import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {
  WindowActionButtons,
  WindowContent,
  WindowContentContainer,
  WindowContentProps,
  WindowHeadline,
} from "../../../../../../components/Window";
import {Button, TextField} from "dkv-live-frontend-ui";
import styled from "styled-components/macro";
import store from "../../../../../../state/store";
import {AlarmModule} from "../state/module";
import {useDispatch, useSelector} from "react-redux";
import {triggersListSelector} from "../state/triggers/selectors";
import {loadTriggersIfNeeded} from "../state/triggers/actions";
import Fuse from "fuse.js";
import {InputAdornment} from "@material-ui/core";

// -----------------------------------------------------------------------------------------------------------
store.addModule(AlarmModule);

// -----------------------------------------------------------------------------------------------------------
const SearchTextField = styled(TextField)`
  width: 100%;

  .MuiInputLabel-root {
    color: #c1c1c1;
  }
`;

const SearchIcon = styled.span`
  font-family: "DKV" !important;
  font-size: 20px;
`;

const TriggerList = styled.ul`
  margin-top: 24px;
  height: 500px;
  overflow-y: scroll;
`;

const TriggerItem = styled.li`
  padding: 4px 0;
  cursor: pointer;
  outline: 0;

  &:hover {
    background-color: var(--dkv-background-primary-hover-color);
  }

  &.selected {
    background-color: var(--dkv-background-primary-hover-color);
  }
`;

const CustomWindowActionButtons = styled(WindowActionButtons)`
  margin-top: 24px;
`;

// -----------------------------------------------------------------------------------------------------------
interface TriggerWindowProps extends WindowContentProps {
  onTriggerSelected: (id: number, params: any[]) => void;
}

const TriggerWindow: React.FC<TriggerWindowProps> = ({forceCloseWindow, headline, onTriggerSelected}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // load triggers
  const triggers = useSelector(triggersListSelector);

  useEffect(() => {
    dispatch(loadTriggersIfNeeded());
  }, [dispatch]);

  // trigger selection
  const [triggerID, setTriggerID] = React.useState(-1);
  const handleTriggerChange = (id: number) => {
    setTriggerID(id);
  };

  const handleTriggerSave = () => {
    onTriggerSelected(triggerID, []);
    forceCloseWindow();
  };

  // filter
  const [filterValue, setFilterValue] = useState("");

  const handleFilterChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFilterValue(event.target.value);
  };
  const [filteredTriggers, setFilteredTriggers] = useState<any[]>([]);

  useEffect(() => {
    if (filterValue === "") {
      setFilteredTriggers([]);
      return undefined;
    }

    const options = {
      includeScore: true,
      threshold: 0.3,
      keys: ["label", "description"],
    };

    const fuse = new Fuse(triggers, options);
    const result = fuse.search(filterValue);
    setFilteredTriggers(result || []);
    return;
  }, [triggers, filterValue]);

  const renderTriggerItem = (id: number, label: string, description: string) => {
    return (
      <TriggerItem
        key={id}
        className={triggerID === id ? "selected" : ""}
        tabIndex={0}
        onClick={() => handleTriggerChange(id)}
      >
        <div>
          <b>{t(label)}</b>
        </div>
        <div>{t(description)}</div>
      </TriggerItem>
    );
  };

  return (
    <WindowContentContainer minWidth="600px" full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <SearchTextField
          name="search"
          type="text"
          label={t("Suche nach Regeln")}
          onChange={handleFilterChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <SearchIcon className="icon-ico_search" />
              </InputAdornment>
            ),
          }}
        />

        {filteredTriggers.length > 0 && (
          <TriggerList>
            {filteredTriggers.map((trigger) => {
              return renderTriggerItem(trigger.item.id, trigger.item.label, trigger.item.description);
            })}
          </TriggerList>
        )}

        {triggers && filteredTriggers.length === 0 && (
          <TriggerList>
            {triggers.map((trigger) => {
              return renderTriggerItem(trigger.id, trigger.label, trigger.description);
            })}
          </TriggerList>
        )}

        <CustomWindowActionButtons>
          <Button type="button" secondary onClick={forceCloseWindow}>
            {t("Abbrechen")}
          </Button>

          <Button type="button" onClick={handleTriggerSave} disabled={triggerID === -1}>
            {t("Hinzufügen")}
          </Button>
        </CustomWindowActionButtons>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default TriggerWindow;
