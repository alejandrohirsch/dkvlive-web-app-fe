import {AlarmModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const alarmsItemsSelector = (state: AlarmModuleState) => state.alarm.alarms.items;

// list
export const alarmsListStateSelector = (state: AlarmModuleState) => state.alarm.alarms.list;

export const alarmsListSelector = createSelector(alarmsItemsSelector, (items) =>
  items ? Object.values(items) : []
);
