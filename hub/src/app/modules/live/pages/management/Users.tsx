import React, {useState, useCallback, useEffect} from "react";
import Page from "app/components/Page";
import {useTranslation} from "react-i18next";
import DataTable, {
  TablePageContent,
  useColumns,
  TableStickyHeader,
  TableStickyFooter,
  // TableFooterRightActions,
  TableContainer,
  bodyField,
  ToolbarButton,
  ToolbarButtonIcon,
  TableFooterActions,
  TableSelectionCountInfo,
  TableFooterRightActions,
} from "app/components/DataTable";
import {Button, Loading} from "dkv-live-frontend-ui";
import Window from "app/components/Window";
import UserDetails from "./components/UserDetails";
import store from "app/state/store";
import {ManagementModule} from "./state/module";
import {useSelector, useDispatch} from "react-redux";
import {loadUsers, loadUsersIfNeeded, deleteUser} from "./state/users/actions";
import {usersListSelector, usersListStateSelector} from "./state/users/selectors";
import {User, emptyUser} from "./state/users/reducer";
import ConfirmWindow from "app/components/ConfirmWindow";
import UserForm from "./components/UserForm";
import dayjs from "dayjs";
import {hasPermission} from "app/modules/login/state/login/selectors";
import UserActivityLog from "./components/UserActivityLog";
import TagList from "dkv-live-frontend-ui/lib/taglist";
import {availableLanguages} from "app/modules/internal/pages/i18n/I18n";
import PolicyForm from "./components/PolicyForm";
import {emptyPolicy, Policy} from "./state/policies/reducer";

//-----------------------------------------------------------------------------------------------------------
store.addModule(ManagementModule);

//-----------------------------------------------------------------------------------------------------------
const userColumns = [
  {
    field: "email",
    header: "E-Mail-Adresse",
    width: 200,
  },
  {
    field: "display_name",
    header: "Name",
    width: 200,
  },
  {
    field: "language",
    header: "Sprache",
    body: bodyField((a) => availableLanguages.find((l: any) => l.value === a.language)?.label),
    width: 100,
  },
  {
    field: "position_tags",
    header: "Position",
    width: 250,
    body: bodyField((a) => <TagList tags={a.position_tags} />),
  },
  {
    field: "divisions_with_names",
    header: "Divisions",
    width: 250,
    body: bodyField((a) => <TagList tags={a.divisions_with_names} />),
  },
  {
    field: "created_at",
    header: "Angelegt",
    body: (a: User) =>
      a.created_at && a.created_at !== "0001-01-01T00:00:00Z"
        ? dayjs(a.created_at).format("DD.MM.YYYY HH:mm:ss")
        : "",
    width: 150,
  },
  {
    field: "last_login_at",
    header: "Letzter Login",
    body: (a: User) =>
      a.last_login_at && a.last_login_at !== "0001-01-01T00:00:00Z"
        ? dayjs(a.last_login_at).format("DD.MM.YYYY HH:mm:ss")
        : "",
    width: 150,
  },
];

//-----------------------------------------------------------------------------------------------------------
interface UsersProps {
  backPath: string;
}

const Users: React.FC<UsersProps> = ({backPath}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // load users
  const usersList = useSelector(usersListSelector);
  useEffect(() => {
    dispatch(loadUsersIfNeeded());
  }, [dispatch]);
  const usersListState = useSelector(usersListStateSelector);

  // actions
  // - detail
  const [activeUserWindow, setActiveUserWindow] = useState<User | null>(null);
  const onCloseUserWindow = useCallback(() => {
    setActiveUserWindow(null);
    setEdit(false);
    setTab(0);
  }, [setActiveUserWindow]);
  const openUserWindow = useCallback((e: any) => setActiveUserWindow(e.data as User), [setActiveUserWindow]);

  // - edit
  const [edit, setEdit] = useState<boolean>(false);
  const [tab, setTab] = useState<number>(0);

  // - create
  const openUserCreateWindow = () => {
    setEdit(true);
    setActiveUserWindow(Object.assign({...emptyUser}));
  };

  // - delete
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
  const onDeleteConfirm = async (confirmed: boolean) => {
    if (!confirmed) {
      setShowDeleteConfirmation(false);
      return;
    }

    const callback = (success: boolean) => {
      if (success) {
        setShowDeleteConfirmation(false);
        dispatch(loadUsers());
        setSelected([]);
      }
    };
    const idToBeDeleted = selected[0].id || "";
    if (idToBeDeleted) {
      await dispatch(deleteUser(idToBeDeleted, callback));
    }
  };

  // - activity log
  const [showActivityWindow, setShowActivityWindow] = useState<boolean>(false);

  // - policy
  const [newPolicy, setNewPolicy] = useState<Policy | null>(null);
  const onCreateNewPolicy = () => setNewPolicy(Object.assign({...emptyPolicy}));

  useEffect(() => {
    if (!activeUserWindow) {
      return;
    }

    const user = usersList.find((u) => u.id === activeUserWindow.id);

    if (user) {
      setActiveUserWindow(user);
    }
  }, [usersList, activeUserWindow]);

  // columns
  const [columns] = useState(userColumns);
  const visibleColumns = useColumns(columns, t);

  // selection
  const [selected, setSelected] = useState<User[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const deselect = () => setSelected([]);
  const openUserWindowForSelected = useCallback(
    (edit?: boolean) => {
      edit && setEdit(edit);
      selected.length && setActiveUserWindow(selected[0]);
    },
    [selected, setActiveUserWindow]
  );

  // render
  return (
    <Page id="userlist">
      <Loading inprogress={usersListState.loading} />

      <TablePageContent>
        <TableStickyHeader backPath={backPath}>
          <h1>{t("Benutzer")}</h1>

          {/* @todo commingsoon */}
          {/* <SearchTextField
            name="search"
            type="text"
            required
            label={t("Suche")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          /> */}

          {hasPermission("management:CreateUsers") && (
            <ToolbarButton
              general
              style={{
                marginLeft: "auto",
              }}
              onClick={openUserCreateWindow}
            >
              <ToolbarButtonIcon>
                <span className="icon-ico_add" />
              </ToolbarButtonIcon>
              {t("Hinzufügen")}
            </ToolbarButton>
          )}
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={usersList}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            onRowDoubleClick={openUserWindow}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo count={selected.length} label={t("Benutzer ausgewählt")} />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={deselect}>
                {t("Abbrechen")}
              </Button>
              <TableFooterActions>
                {hasPermission("management:EditUsers") && (
                  <ToolbarButton onClick={() => openUserWindowForSelected(true)}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_edit" />
                    </ToolbarButtonIcon>
                    {t("Bearbeiten")}
                  </ToolbarButton>
                )}
                {hasPermission("management:DeleteUsers") && (
                  <ToolbarButton onClick={() => setShowDeleteConfirmation(true)}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_delete" />
                    </ToolbarButtonIcon>
                    {t("Löschen")}
                  </ToolbarButton>
                )}
              </TableFooterActions>
              <TableFooterRightActions />
            </>
          )}

          {/* <TableFooterRightActions>
            <Button autoWidth secondary ariaLabel={t("Gesamte Liste exportieren")}>
              {t("Gesamte Liste exportieren")}
            </Button>

            {selected.length > 0 && (
              <Button autoWidth ariaLabel={t("Auswahl als Liste exportieren")}>
                {t("Auswahl als Liste exportieren")}
              </Button>
            )}
          </TableFooterRightActions> */}
        </TableStickyFooter>
      </TablePageContent>

      {activeUserWindow &&
        (edit ? (
          <Window
            onClose={() => setEdit(false)}
            headline={activeUserWindow.id ? t("Benutzer bearbeiten: ") : t("Benutzer erstellen")}
          >
            {(props) => (
              <UserForm
                {...props}
                user={activeUserWindow}
                tab={tab}
                setTab={setTab}
                onCreateNewPolicy={onCreateNewPolicy}
              />
            )}
          </Window>
        ) : (
          activeUserWindow.id && (
            <Window onClose={onCloseUserWindow} headline={t("Detailansicht Benutzer")}>
              {(props) => (
                <UserDetails
                  {...props}
                  user={activeUserWindow}
                  setEdit={setEdit}
                  showActivity={setShowActivityWindow}
                  tab={tab}
                  setTab={setTab}
                />
              )}
            </Window>
          )
        ))}

      {showActivityWindow && activeUserWindow && (
        <Window onClose={() => setShowActivityWindow(false)} headline={t("Aktivitäten Benutzer")}>
          {(props) => <UserActivityLog {...props} userID={activeUserWindow.id || ""} />}
        </Window>
      )}

      {showDeleteConfirmation && selected.length && (
        <ConfirmWindow
          headline={
            // (selected.length > 1
            //   ? selected.length + " " + t("ausgewählte Benutzer")
            // : t("Ausgewählten Benutzer"))
            // + " " + t("wirklich löschen?")
            t("Ausgewählten Benutzer") + " " + selected[0].display_name + " " + t("wirklich löschen?")
          }
          onConfirm={onDeleteConfirm}
        />
      )}

      {newPolicy && (
        <Window onClose={() => setNewPolicy(null)} headline={t("Policy erstellen")}>
          {(props) => <PolicyForm {...props} policy={newPolicy} />}
        </Window>
      )}
    </Page>
  );
};

export default Users;
