import React, {useState, useEffect} from "react";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components/macro";
import {Message, Button, AutocompleteInput, Tab, Loading} from "dkv-live-frontend-ui";
import {
  WindowTabPanel,
  WindowActionButtons,
  WindowTabsContainer,
  WindowContentContainer,
  WindowHeadline,
  WindowContentProps,
  WindowTabs,
  WindowContent,
} from "app/components/Window";
import {TextField} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {useDispatch, useSelector} from "react-redux";
import {loadUsers, updateUser, createUser} from "../state/users/actions";
import {usersItemStateSelector} from "../state/users/selectors";
import UserPolicies from "./UserPolicies";
import {User, Tag} from "../state/users/reducer";
import {availableLanguages} from "app/modules/internal/pages/i18n/I18n";
import httpClient from "services/http";
import TagInput from "dkv-live-frontend-ui/lib/taginput";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  padding-top: 1em;
  display: grid;
  grid-gap: 24px;
  width: 99%;
`;

interface SubContainerProps {
  width?: string;
}

const SubContainer = styled.div<SubContainerProps>`
  display: inline-flex;
  flex-direction: column;
  margin: 10px;
  overflow: auto;
  font-size: 1.1428571428571428rem;

  ${(props) =>
    props.width
      ? css`
          width: calc(${props.width});
        `
      : css`
          max-width: 98%;
        `};

  h1 {
    display: inline-flex;
    margin-top: 24px;
    margin-bottom: 10px;
    font-size: inherit;
    font-weight: bold;
  }
`;

interface PolicyFieldProps {
  allowsHoveredPermission?: boolean;
  deniesHoveredPermission?: boolean;
}

// -----------------------------------------------------------------------------------------------------------
interface UserFormProps extends WindowContentProps {
  user: User;
  tab: number;
  setTab: (n: number) => void;
  onCreateNewPolicy?: () => void;
}

const UserForm: React.FC<UserFormProps> = ({
  user,
  headline,
  forceCloseWindow,
  tab,
  setTab,
  onCreateNewPolicy,
}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // tabs
  const handleChange = (_: React.ChangeEvent<unknown>, newTabValue: number) => {
    setTab(newTabValue);
  };

  const itemState = useSelector(usersItemStateSelector);

  // form
  const form = useFormik<User>({
    initialValues: user,
    onSubmit: (userData) => {
      form.setSubmitting(true);
      const submit = async () => {
        const callback = (success: boolean) => {
          form.setSubmitting(false);
          if (success) {
            dispatch(loadUsers());
            forceCloseWindow();
          }
        };
        dispatch(userData.id ? updateUser(userData.id, userData, callback) : createUser(userData, callback));
      };
      submit();
    },
  });

  // tags
  const [positionTagList, setPositionTagList] = useState<{name: string; id?: string}[]>([]);
  const [divisionTagList, setDivisionTagList] = useState<{name: string; id?: string}[]>([]);
  const [positionTagsError, setPositionTagsError] = useState("");
  const [divisionTagsError, setDivisionTagsError] = useState("");

  const tags = {
    position: {
      field: "position_tags",
      tags: positionTagList,
      set: setPositionTagList,
      error: positionTagsError,
      setError: setPositionTagsError,
    },
    division: {
      field: "divisions_with_names",
      tags: divisionTagList,
      set: setDivisionTagList,
      error: divisionTagsError,
      setError: setDivisionTagsError,
    },
  };

  useEffect(() => {
    httpClient
      .get("/management/tags/1", {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then(([{data}, err]) =>
        setPositionTagList(
          err
            ? [] && setPositionTagsError(err.message)
            : data.reduce((d: any, tag: Tag) => ({...d, [tag.name]: tag}), {})
        )
      );

    httpClient
      .get("/management/divisions", {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then(([{data}, err]) =>
        setDivisionTagList(
          err
            ? [] && setDivisionTagsError(err.message)
            : data.reduce((d: any, t: any) => ({...d, [t.name]: {name: t.name, id: t.id}}), {})
        )
      );
  }, [user, form.values.organization_id]);

  const handleToggleTag = (tagKeys: string[], type: string) => {
    if (tags[type]) {
      form.setFieldValue(
        tags[type].field,
        tagKeys.map((k) => {
          if (!tags[type].tags[k]) {
            const newTag = {name: k};
            tags[type].set({...tags[type].tags, [k]: newTag});
            return newTag;
          }
          return tags[type].tags[k];
        })
      );
    }
  };

  const handleTogglePolicy = (target: any) => {
    form.setFieldValue(
      "policies",
      form.values.policies.find((p) => p === target)
        ? form.values.policies.filter((p) => p !== target)
        : form.values.policies.concat(target)
    );
  };

  // render
  return (
    <WindowContentContainer full fixedWidth="1100px" fixedHeight="800px">
      <WindowHeadline padding>
        <h1>
          {headline} {user.display_name}
        </h1>
      </WindowHeadline>

      <Loading inprogress={itemState.loading} />

      <WindowTabsContainer>
        <WindowTabs value={tab} variant="scrollable" onChange={handleChange}>
          <Tab label={t("Übersicht")} value={0} />
          <Tab label={t("Policies")} value={1} />
        </WindowTabs>
      </WindowTabsContainer>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {itemState.error && <Message text={t(itemState.errorText || "Unbekannter Fehler")} error />}
          <WindowTabPanel current={tab} index={0}>
            <SubContainer width="50%">
              <FormGrid>
                <TextField
                  name="email"
                  type="text"
                  label={t("E-Mail-Adresse")}
                  value={form.values.email}
                  onChange={form.handleChange}
                  data-autofocus
                  disabled={form.isSubmitting}
                  variant="outlined"
                  required
                />
                <TextField
                  name="last_name"
                  type="text"
                  label={t("Nachname")}
                  value={form.values.last_name}
                  onChange={form.handleChange}
                  data-autofocus
                  disabled={form.isSubmitting}
                  variant="outlined"
                  required
                />
                <TextField
                  name="first_name"
                  type="text"
                  label={t("Vorname")}
                  value={form.values.first_name}
                  onChange={form.handleChange}
                  data-autofocus
                  disabled={form.isSubmitting}
                  variant="outlined"
                  required
                />

                <AutocompleteInput
                  value={
                    form.values.language
                      ? availableLanguages.find((l: any) => l.value === form.values.language)
                      : null
                  }
                  onChange={(_: any, newValue: any) => {
                    form.setFieldValue("language", newValue ? newValue.value : "");
                  }}
                  options={availableLanguages}
                  autoHighlight
                  getOptionLabel={(option: any) => t(option.label)}
                  renderInput={(params: any) => (
                    <TextField
                      {...params}
                      label={t("Sprache")}
                      variant="outlined"
                      required
                      inputProps={{
                        ...params.inputProps,
                      }}
                    />
                  )}
                />
                <TextField
                  name="phone"
                  type="text"
                  label={t("Telefonnummer")}
                  value={form.values.phone}
                  onChange={form.handleChange}
                  data-autofocus
                  disabled={form.isSubmitting}
                  variant="outlined"
                />
                <TextField
                  name="note"
                  type="text"
                  label={t("Bemerkung")}
                  value={form.values.note}
                  onChange={form.handleChange}
                  data-autofocus
                  disabled={form.isSubmitting}
                  variant="outlined"
                />
              </FormGrid>
            </SubContainer>
            <SubContainer width="50%">
              <FormGrid>
                <TagInput
                  preventDefaultOnAdd
                  value={form.values.position_tags.map((p) => p.name)}
                  onChange={(tags) => handleToggleTag(tags, "position")}
                  options={positionTagsError ? [positionTagsError] : Object.keys(positionTagList)}
                  label={t("Positionen")}
                  required
                />
                <TagInput
                  preventDefaultOnAdd
                  value={form.values.divisions_with_names.map((p) => p.name)}
                  onChange={(tags) => handleToggleTag(tags, "division")}
                  options={divisionTagsError ? [divisionTagsError] : Object.keys(divisionTagList)}
                  label={t("Divisions")}
                  required
                />
              </FormGrid>
            </SubContainer>
          </WindowTabPanel>

          {hasPermission("management:EditPolicies") && (
            <WindowTabPanel current={tab} index={1}>
              <UserPolicies
                user={form.values}
                onToggle={handleTogglePolicy}
                onCreateNewPolicy={onCreateNewPolicy}
              />
            </WindowTabPanel>
          )}

          <WindowActionButtons style={{position: "absolute", height: "40px", bottom: "40px", width: "90%"}}>
            <Button
              type="button"
              secondary
              onClick={() => {
                forceCloseWindow();
                !form.values.id && setTab(0);
              }}
            >
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !(user as User)}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default UserForm;
