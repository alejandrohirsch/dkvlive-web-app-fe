import React from "react";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components/macro";
import {
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowTabs,
  WindowTabsContainer,
  WindowTabPanel,
  WindowActions,
} from "app/components/Window";
import {Tab, Loading} from "dkv-live-frontend-ui";
import {User} from "../state/users/reducer";
import {ToolbarButton, ToolbarButtonIcon} from "app/components/DataTable";
import UserPolicies from "./UserPolicies";
import {Table} from "@material-ui/core";
import {TableLabel, TableValue} from "../../map/components/details/Overview";
import dayjs from "dayjs";
import {hasPermission} from "app/modules/login/state/login/selectors";
import TagList from "dkv-live-frontend-ui/lib/taglist";
import {usersItemStateSelector} from "../state/users/selectors";
import {availableLanguages} from "app/modules/internal/pages/i18n/I18n";
import {useSelector} from "react-redux";

// -----------------------------------------------------------------------------------------------------------

interface SubContainerProps {
  width?: string;
}

const SubContainer = styled.div<SubContainerProps>`
  display: inline-flex;
  justify-content: center;
  flex-direction: column;
  margin: 10px;
  max-height: 550px;
  flex-grow: 1;
  font-size: 1.1428571428571428rem;
  overflow: auto;

  ${(props) =>
    props.width
      ? css`
          width: calc(${props.width});
        `
      : css`
          max-width: 98%;
        `};

  h1 {
    display: inline-flex;
    margin-bottom: 10px;
    font-size: inherit;
    font-weight: bold;
  }

  ul {
    margin-bottom: 20px;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface UserDetailsWindowProps extends WindowContentProps {
  user: User;
  setEdit: (_: any) => void;
  tab: number;
  setTab: (n: number) => void;
  showActivity: (_: any) => void;
}

const UserDetails: React.FC<UserDetailsWindowProps> = ({
  user,
  setEdit,
  headline,
  tab,
  setTab,
  showActivity,
}) => {
  const {t} = useTranslation();

  // tabs
  const handleChange = (_: React.ChangeEvent<unknown>, newTabValue: number) => {
    setTab(newTabValue);
  };

  const itemState = useSelector(usersItemStateSelector);

  // render
  return (
    <WindowContentContainer full fixedWidth="800px" fixedHeight="700px">
      <WindowHeadline padding>
        <h1>
          {headline} {user.display_name}
        </h1>
      </WindowHeadline>

      <Loading inprogress={itemState.loading} />

      <WindowTabsContainer>
        <WindowTabs value={tab} variant="scrollable" onChange={handleChange}>
          <Tab label={t("Übersicht")} value={0} />
          {hasPermission("management:ListPolicies") && <Tab label={t("Policies")} value={1} />}
        </WindowTabs>
      </WindowTabsContainer>

      <WindowContent padding>
        <WindowTabPanel current={tab} index={0}>
          <SubContainer width="50%">
            <Table>
              <tbody>
                <tr>
                  <TableLabel>{t("E-Mail-Adresse")}:</TableLabel>
                  <TableValue>{user.email}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Nachname")}:</TableLabel>
                  <TableValue>{user.last_name}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Vorname")}:</TableLabel>
                  <TableValue>{user.first_name}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Sprache")}:</TableLabel>
                  <TableValue>
                    {t(availableLanguages.find((l: any) => l.value === user.language)?.label || "")}
                  </TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Telefonnummer")}:</TableLabel>
                  <TableValue>{user.phone}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Bemerkung")}:</TableLabel>
                  <TableValue>{user.note}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Angelegt")}:</TableLabel>
                  <TableValue>
                    {" "}
                    {user.created_at &&
                      user.created_at !== "0001-01-01T00:00:00Z" &&
                      dayjs(user.created_at).format("DD.MM.YYYY HH:mm:ss")}
                  </TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Letzter Login")}:</TableLabel>
                  <TableValue>
                    {user.last_login_at &&
                      user.last_login_at !== "0001-01-01T00:00:00Z" &&
                      dayjs(user.last_login_at).format("DD.MM.YYYY HH:mm:ss")}
                  </TableValue>
                </tr>
              </tbody>
            </Table>
          </SubContainer>
          <SubContainer width="50%">
            <h1>{t("Position")}</h1>
            <TagList tags={user.position_tags} />
            <h1>{t("Divisions")}</h1>
            <TagList tags={user.divisions_with_names} />
          </SubContainer>
        </WindowTabPanel>
        <WindowTabPanel current={tab} index={1}>
          <UserPolicies user={user} />
        </WindowTabPanel>
        <WindowActions style={{position: "absolute", width: "90%", height: "40px", bottom: "40px"}}>
          {/* @todo commingsoon */ hasPermission("superadmin:root") && (
            <ToolbarButton type="button" onClick={showActivity}>
              <ToolbarButtonIcon>
                <span className="icon-ico_activity" />
              </ToolbarButtonIcon>
              {t("Aktivität")}
            </ToolbarButton>
          )}

          {hasPermission("management:EditUsers") && (
            <ToolbarButton type="button" onClick={setEdit}>
              <ToolbarButtonIcon>
                <span className="icon-ico_edit" />
              </ToolbarButtonIcon>
              {t("Bearbeiten")}
            </ToolbarButton>
          )}
        </WindowActions>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default UserDetails;
