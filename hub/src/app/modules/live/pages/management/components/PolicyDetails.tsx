import React from "react";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components/macro";
import {
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowActions,
  WindowPanel,
} from "app/components/Window";
import {Policy} from "../state/policies/reducer";
import {TableLabel, TableValue} from "../../map/components/details/Overview";
import PolicyPermissions from "./PolicyPermissions";
import {hasPermission} from "app/modules/login/state/login/selectors";
import {ToolbarButton, ToolbarButtonIcon} from "app/components/DataTable";
import {Table} from "@material-ui/core";
import {Loading} from "dkv-live-frontend-ui";
import {useSelector} from "react-redux";
import {policiesItemStateSelector} from "../state/policies/selectors";

// -----------------------------------------------------------------------------------------------------------

interface SubContainerProps {
  width?: string;
}

const SubContainer = styled.div<SubContainerProps>`
  display: flex;
  flex-direction: column;
  margin: 10px;
  flex-grow: 1;
  font-size: 1.1428571428571428rem;
  align-items: space-between;
  max-height: 550px;
  overflow: auto;
  ${(props) =>
    props.width
      ? css`
          width: calc(${props.width} - 20px);
        `
      : css`
          width: 98%;
        `};

  h1 {
    display: inline-flex;
    margin-top: 24px;
    margin-bottom: 10px;
    font-size: inherit;
    font-weight: bold;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface PolicyDetailsWindowProps extends WindowContentProps {
  policy: Policy;
  setEdit: (_: any) => void;
  showActivity: (_: any) => void;
}

const PolicyDetails: React.FC<PolicyDetailsWindowProps> = ({policy, setEdit, headline, showActivity}) => {
  const {t} = useTranslation();
  const itemState = useSelector(policiesItemStateSelector);

  // render
  return (
    <WindowContentContainer full minWidth="800px" minHeight="760px">
      <WindowHeadline padding>
        <h1>
          {headline} {policy.name}
        </h1>
      </WindowHeadline>

      <Loading inprogress={itemState.loading} />

      <WindowContent padding>
        <WindowPanel>
          <SubContainer>
            <Table>
              <tbody>
                <tr>
                  <TableLabel>{t("Bezeichnung")}:</TableLabel>
                  <TableValue>{policy.name}</TableValue>
                </tr>
              </tbody>
              <tbody>
                <tr>
                  <TableLabel>{t("Bemerkung")}:</TableLabel>
                  <TableValue>{policy.note}</TableValue>
                </tr>
              </tbody>
            </Table>
            <PolicyPermissions policy={policy} />
          </SubContainer>
        </WindowPanel>

        <WindowActions>
          {/* @todo commingsoon */ hasPermission("superadmin:root") && (
            <ToolbarButton type="button" onClick={showActivity}>
              <ToolbarButtonIcon>
                <span className="icon-ico_activity" />
              </ToolbarButtonIcon>
              {t("Aktivität")}
            </ToolbarButton>
          )}

          {policy.organization_id && hasPermission("management:EditPolicies") && (
            <ToolbarButton type="button" onClick={setEdit}>
              <ToolbarButtonIcon>
                <span className="icon-ico_edit" />
              </ToolbarButtonIcon>
              {t("Bearbeiten")}
            </ToolbarButton>
          )}
        </WindowActions>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default PolicyDetails;
