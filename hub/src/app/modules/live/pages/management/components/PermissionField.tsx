import styled, {css} from "styled-components/macro";

// -----------------------------------------------------------------------------------------------------------
export interface PermissionFieldProps {
  key: string;
  allowed?: boolean;
  denied?: boolean;
  allowedPreview?: boolean;
  deniedPreview?: boolean;
  hoverColor?: string;
}

export const PermissionField = styled.div<PermissionFieldProps>`
  position: relative;
  height: 30px;
  margin: 5px;
  padding-left: 15px;
  padding-right: 15px;
  border-radius: 5px;
  border: 1px solid;
  display: grid;
  grid-template-columns: 10% 1fr 25px;
  align-items: center;
  cursor: default;
  font-variant: normal;

  .icon-ico_unlock,
  .icon-ico_lock {
    opacity: 20%;
  }

  ${(props) =>
    props.allowed &&
    css`
      border-color: #58be58;
      background-color: #58be58;
      color: #ffffff;
      .icon-ico_unlock {
        opacity: 100%;
      }
    `};

  ${(props) =>
    props.allowedPreview &&
    css`
      font-weight: bold;
      border-color: "#58be58";
      background-color: ${props.allowed ? "#58be58" : "#def2de"};
      color: ${props.denied ? "#dbdbdb" : props.allowed ? "#ffffff" : "#58be58"};
      opacity: 100%;
      .icon-ico_unlock {
        opacity: 100%;
      }
    `};

  ${(props) =>
    props.denied &&
    css`
      border-color: #e44c4d;
      background-color: #e44c4d;
      color: #ffffff;
      .icon-ico_lock {
        opacity: 100%;
      }
    `};

  ${(props) =>
    props.denied &&
    props.allowedPreview &&
    css`
      border-color: #dbdbdb;
      background-color: #ffffff;
      color: #dbdbdb;
    `};

  ${(props) =>
    props.deniedPreview &&
    css`
      font-weight: bold;
      border-color: #e44c4d;
      background-color: ${props.denied ? "#e44c4d" : "#fadbdb"};
      color: ${props.denied ? "#ffffff" : "#e44c4d"};
      opacity: 100%;
    `};

  ${(props) =>
    props.hoverColor &&
    css`
      :hover {
        cursor: pointer;
        background-color: ${props.hoverColor};
        color: #ffffff;
        .icon-ico_unlock {
          color: #ffffff;
        }
        .icon-ico_lock {
          color: #ffffff;
        }
      }
    `};
`;
