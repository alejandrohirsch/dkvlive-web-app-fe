import React, {useState, useEffect} from "react";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components/macro";
import {Button, Message, Loading} from "dkv-live-frontend-ui";
import {
  WindowTabPanel,
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import {TextField} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {Policy} from "../state/policies/reducer";
import {useDispatch, useSelector} from "react-redux";
import {loadPolicies, updatePolicy, createPolicy, loadPoliciesIfNeeded} from "../state/policies/actions";
import {policiesItemStateSelector, policiesListSelector} from "../state/policies/selectors";
import PolicyPermissions from "./PolicyPermissions";
import {InputLabel, FormControl, MenuItem} from "@material-ui/core";
import {Select} from "dkv-live-frontend-ui";

// -----------------------------------------------------------------------------------------------------------

const Form = styled.form`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const FormGrid = styled.div`
  padding-top: 1em;
  display: grid;
  grid-gap: 24px;
  width: 99%;
`;

interface SubContainerProps {
  width?: string;
}

const SubContainer = styled.div<SubContainerProps>`
  display: inline-flex;
  flex-direction: column;
  margin: 10px;
  flex-grow: 1;
  max-height: 550px;
  overflow: auto;
  font-size: 1.1428571428571428rem;

  ${(props) =>
    props.width
      ? css`
          width: calc(${props.width});
        `
      : css`
          width: 98%;
        `};

  h1 {
    display: inline-flex;
    margin-top: 24px;
    margin-bottom: 10px;
    font-size: inherit;
    font-weight: bold;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface PolicyDetailsWindowProps extends WindowContentProps {
  policy: Policy;
  setEdit?: (edit: boolean) => void;
}

const PolicyForm: React.FC<PolicyDetailsWindowProps> = ({forceCloseWindow, headline, policy}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const itemState = useSelector(policiesItemStateSelector);

  // form
  const form = useFormik<Policy>({
    initialValues: policy,
    onSubmit: (policyData) => {
      form.setSubmitting(true);
      const submit = async () => {
        const callback = (success: boolean) => {
          form.setSubmitting(false);
          if (success) {
            dispatch(loadPolicies());
            forceCloseWindow();
          }
        };
        dispatch(
          policyData.id
            ? updatePolicy(policyData.id, policyData, callback)
            : createPolicy(policyData, callback)
        );
      };
      submit();
    },
  });

  const [allowHovered, setAllowHovered] = useState(false);

  const handleToggle = (targetKey: string) => {
    if (targetKey.length) {
      let allow = form.values.permissions["allow"];
      let deny = form.values.permissions["deny"];
      if (allowHovered) {
        if (allow.includes(targetKey)) {
          allow = allow.filter((v: string) => v !== targetKey);
        } else {
          deny = deny.filter((v: string) => v !== targetKey);
          allow = allow.concat(targetKey);
        }
      } else {
        if (deny.includes(targetKey)) {
          deny = deny.filter((v: string) => v !== targetKey);
        } else {
          allow = allow.filter((v: string) => v !== targetKey);
          deny = deny.concat(targetKey);
        }
      }
      form.setFieldValue("permissions", {allow, deny});
    }
  };

  // load policies
  const policiesList = useSelector(policiesListSelector);
  useEffect(() => {
    dispatch(loadPoliciesIfNeeded());
  }, [dispatch]);

  const handleTemplateChange = (event: any) => {
    const id = event.target.value;
    if (id) {
      const template = policiesList.find((p) => p.id === id);
      if (template && template.permissions) {
        form.setFieldValue("permissions", template.permissions);
      }
    }
  };

  // render
  return (
    <WindowContentContainer full minWidth="1100px" fixedHeight="760px">
      <WindowHeadline padding>
        <h1>
          {headline} {policy.id && t(policy.name)}
        </h1>
      </WindowHeadline>

      <Loading inprogress={itemState.loading} />

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {itemState.error && <Message text={t(itemState.errorText || "Unbekannter Fehler")} error />}
          <WindowTabPanel current={0} index={0}>
            <SubContainer width="50%" style={{justifyContent: "space-between"}}>
              <FormGrid>
                <TextField
                  name="name"
                  type="text"
                  label={t("Bezeichnung der Policy")}
                  value={form.values.name}
                  onChange={form.handleChange}
                  data-autofocus
                  disabled={form.isSubmitting}
                  variant="outlined"
                  required
                />
                <TextField
                  name="note"
                  type="text"
                  multiline
                  label={t("Anmerkungen")}
                  value={form.values.note}
                  onChange={form.handleChange}
                  data-autofocus
                  disabled={form.isSubmitting}
                  variant="outlined"
                />
              </FormGrid>
              <FormControl variant="outlined">
                <InputLabel>{t("Berechtigungen aus bestehender Policy übernehmen...")}</InputLabel>
                <Select value="" id="permissions" onChange={handleTemplateChange}>
                  {policiesList.map((p) => (
                    <MenuItem key={p.id} value={p.id}>
                      {p.name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </SubContainer>
            <SubContainer width="50%">
              <PolicyPermissions
                policy={form.values}
                onToggle={handleToggle}
                allowHovered={allowHovered}
                setAllowHovered={setAllowHovered}
              />
            </SubContainer>
          </WindowTabPanel>
          <WindowActionButtons style={{position: "absolute", height: "40px", bottom: "40px", right: "40px"}}>
            <Button secondary type="button" onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !(policy as Policy)}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default PolicyForm;
