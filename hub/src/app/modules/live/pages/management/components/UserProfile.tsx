import React, {useState, useEffect} from "react";
import {
  WindowContentContainer,
  WindowHeadline,
  WindowContent,
  WindowActionButtons,
  WindowContentProps,
} from "app/components/Window";
import {Message, Button, TextField, AutocompleteInput} from "dkv-live-frontend-ui";
import styled from "styled-components";
import {useTranslation} from "react-i18next";
import {useFormik} from "formik";
import httpClient from "services/http";
import {getLoggedInUser} from "app/modules/login/state/login/selectors";
import {availableLanguages} from "app/modules/internal/pages/i18n/I18n";
import {InputAdornment, IconButton} from "@material-ui/core";

// ----------------------------------------------------------------------------------------------------------
interface ProfileData {
  id: string;
  email: string;
  last_name: string;
  first_name: string;
  language: string;
  password: string;
  password_again: string;
  showPassword: boolean;
  showPasswordAgain: boolean;
}

// ----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
`;

const PWVisibilityIcon = styled.span`
  font-family: "DKV" !important;
  font-size: 20px;

  &:before {
    font-family: "DKV" !important;
  }
`;

const PWVisibilityOffIcon = styled.span`
  font-family: "DKV" !important;
  font-size: 20px;

  &:before {
    font-family: "DKV" !important;
  }
`;

// -----------------------------------------------------------------------------------------------------------
const UserProfile: React.FC<WindowContentProps> = ({forceCloseWindow, headline, setLoading}) => {
  const {t} = useTranslation();

  // form
  const [error, setError] = useState("");

  const form = useFormik<ProfileData>({
    initialValues: {
      id: "",
      email: "",
      last_name: "",
      first_name: "",
      language: "",
      password: "",
      password_again: "",
      showPassword: false,
      showPasswordAgain: false,
    },
    onSubmit: (values) => {
      if (values.password !== values.password_again) {
        setError("Passwörter stimmen nicht überein");
        form.setSubmitting(false);
        return;
      }

      setLoading(true, true, t("wird geändert"));
      setError("");

      const submit = async () => {
        const [, err] = await httpClient.put(
          `/management/userprofile`,
          JSON.stringify({
            id: values.id,
            email: values.email,
            last_name: values.last_name,
            first_name: values.first_name,
            language: values.language,
            password: values.password,
          }),
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );

        if (err !== null) {
          setError(
            err.response?.data?.message
              ? err.response?.data?.message
              : typeof err === "string"
              ? err
              : t("User konnte nicht geändert werden")
          );
          setLoading(false);
          form.setSubmitting(false);
          return;
        }

        window.dispatchEvent(
          new CustomEvent("dkv-growl", {
            detail: {
              life: 5000,
              closable: false,
              severity: "success",
              summary: t("Profil geändert"),
            },
          })
        );
        forceCloseWindow();
      };

      submit();
    },
  });

  // load
  const user = getLoggedInUser();
  const userID = user ? user.id : null;

  useEffect(() => {
    if (!userID) {
      return;
    }

    setLoading(true);

    const load = async () => {
      const [{data}, err] = await httpClient.get(`/management/userprofile`);
      if (err !== null) {
        setLoading(false);
        setError(t("Laden fehlgeschlagen"));
        return;
      }

      setLoading(false);
      form.setValues({
        id: data.id,
        email: data.email,
        last_name: data.last_name,
        first_name: data.first_name,
        language: data.language,
        password: "",
        password_again: "",
        showPassword: false,
        showPasswordAgain: false,
      });
    };

    load();

    // eslint-disable-next-line
  }, []);

  const toggleShowPassword = (form: any) => {
    form.setFieldValue("showPassword", !form.values.showPassword);
  };

  const toggleShowPasswordAgain = (form: any) => {
    form.setFieldValue("showPasswordAgain", !form.values.showPasswordAgain);
  };

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  // render
  return (
    <WindowContentContainer full minWidth="666px">
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {error && <Message style={{marginBottom: 24}} text={t(error)} error />}

          <FormGrid>
            <TextField
              name="email"
              type="text"
              label={t("E-Mail-Adresse")}
              value={form.values.email}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
              autoFocus
            />
            <TextField
              name="last_name"
              type="text"
              label={t("Nachname")}
              value={form.values.last_name}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="first_name"
              type="text"
              label={t("Vorname")}
              value={form.values.first_name}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <AutocompleteInput
              value={
                form.values.language
                  ? availableLanguages.find((l: any) => l.value === form.values.language)
                  : null
              }
              onChange={(_: any, newValue: any) => {
                form.setFieldValue("language", newValue ? newValue.value : "");
              }}
              options={availableLanguages}
              autoHighlight
              getOptionLabel={(option: any) => t(option.label)}
              renderInput={(params: any) => (
                <TextField
                  {...params}
                  label={t("Sprache")}
                  variant="outlined"
                  required
                  inputProps={{
                    ...params.inputProps,
                  }}
                />
              )}
            />

            <TextField
              name="password"
              type={form.values.showPassword ? "text" : "password"}
              label={t("Neues Passwort")}
              value={form.values.password}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              inputProps={{minLength: 8}}
              variant="outlined"
              style={{marginTop: 24}}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label={t("Passwort umschalten")}
                      onClick={() => toggleShowPassword(form)}
                      onMouseDown={handleMouseDownPassword}
                    >
                      {form.values.showPassword ? (
                        <PWVisibilityIcon className="icon-ico_watching" />
                      ) : (
                        <PWVisibilityOffIcon className="icon-ico_doc_send" />
                      )}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />

            <TextField
              name="password_again"
              type={form.values.showPasswordAgain ? "text" : "password"}
              label={t("Neues Passwort wiederholen")}
              value={form.values.password_again}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              inputProps={{minLength: 8}}
              variant="outlined"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label={t("Passwort umschalten")}
                      onClick={() => toggleShowPasswordAgain(form)}
                      onMouseDown={handleMouseDownPassword}
                    >
                      {form.values.showPasswordAgain ? (
                        <PWVisibilityIcon className="icon-ico_watching" />
                      ) : (
                        <PWVisibilityOffIcon className="icon-ico_doc_send" />
                      )}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !form.values.id}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default UserProfile;
