import React, {useEffect} from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {Message, Button} from "dkv-live-frontend-ui";
import {
  WindowActionButtons,
  WindowContentContainer,
  WindowHeadline,
  WindowContentProps,
  WindowContent,
} from "app/components/Window";
import {TextField} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {useDispatch, useSelector} from "react-redux";
import {Organization} from "../state/organization/reducer";
import {organizationStateSelector} from "../state/organization/selectors";
import {updateOrganization, loadOrganization} from "../state/organization/actions";

// -----------------------------------------------------------------------------------------------------------

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  padding-top: 1em;
  display: grid;
  grid-gap: 24px;
  width: 99%;
`;

// -----------------------------------------------------------------------------------------------------------
interface OrganizationFormProps extends WindowContentProps {
  organization: Organization;
}

const OrganizationForm: React.FC<OrganizationFormProps> = ({organization, headline, forceCloseWindow}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const itemState = useSelector(organizationStateSelector);
  useEffect(() => {
    dispatch(loadOrganization());
  }, [dispatch]);

  // form
  const form = useFormik<Organization>({
    initialValues: organization,
    onSubmit: (organizationData) => {
      form.setSubmitting(true);
      const submit = async () => {
        const callback = (success: boolean) => {
          form.setSubmitting(false);
          if (success) {
            forceCloseWindow();
          }
        };
        dispatch(updateOrganization(organizationData.id, organizationData, callback));
        form.setSubmitting(false);
        if (!itemState.error) {
          forceCloseWindow();
        }
      };
      submit();
    },
  });

  // render
  return (
    <WindowContentContainer full minWidth="500px" minHeight="300px">
      <WindowHeadline padding>
        <h1>
          {headline} {organization.name || ""}
        </h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {itemState.error && <Message text={t(itemState.errorText || "Unbekannter Fehler")} error />}

          <FormGrid>
            <TextField
              name="name"
              type="text"
              label={t("Name der Organisation")}
              value={form.values["name"]}
              onChange={form.handleChange}
              data-autofocus
              disabled
              variant="outlined"
            />
            <TextField
              name="customer_number"
              type="text"
              label={t("Kundennummer")}
              value={form.values.customer_number}
              onChange={form.handleChange}
              data-autofocus
              disabled
              variant="outlined"
            />
            <TextField
              name="notification_email"
              type="text"
              label={t("E-Mail")}
              value={form.values.notification_email}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
            />
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !(organization as Organization)}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default OrganizationForm;
