import React from "react";
import styled from "styled-components";
import Page from "app/components/Page";
import Headline from "app/components/Headline";
import PageContent from "app/components/PageContent";
import {useTranslation} from "react-i18next";
import {Link} from "react-router-dom";
import OrganizationIcon from "./icons/dashboard-organisationsstruktur.svg";
import UserIcon from "./icons/dashboard-benutzer.svg";
import PolicyIcon from "./icons/dashboard-policies.svg";
import AlarmIcon from "./icons/dashboard-meldungen.svg";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const Box = styled.section`
  background-color: var(--dkv-background-primary-color);
  min-width: 0;
  min-height: 0;
  overflow: auto;
  flex-shrink: 0;

  a {
    padding: 26px 32px 48px 32px;
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 100%;
    width: 100%;
    color: #8c8c8c;

    h1 {
      color: #004b78;
      font-size: 1.4285714285714286rem;
      font-weight: 500;
      line-height: 1.4;
      margin-top: 20px;
    }

    p {
      margin-top: 20px;
      text-align: center;
      font-size: 1.1428571428571428rem;
      padding: 0 30px;
      color: #8c8c8c;
      font-weight: 500;
      line-height: 1.5;
    }
  }

  img {
    width: 120px;
  }

  &:hover {
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.3);
    transition: box-shadow 0.1s linear;
  }
`;

const BoxContainer = styled.div`
  padding: 8px 64px 42px 64px;

  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  grid-gap: 24px;

  @media (max-width: 1440px) {
    grid-template-columns: 1fr 1fr;
    padding: 8px 24px 48px 24px;
    height: auto;
  }

  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface SelectionProps {
  path: string;
}

const Selection: React.FC<SelectionProps> = ({path}) => {
  const {t} = useTranslation();

  // render
  return (
    <Page id="management">
      <Headline>{t("Verwaltung")}</Headline>
      <PageContent>
        <BoxContainer>
          {hasPermission("organizations:Visible") && (
            <Box>
              <Link to={`${path}/organization`}>
                <img src={OrganizationIcon} alt={t("Organisationsstruktur")} />
                <h1>{t("Organisationsstruktur")}</h1>
                <p>
                  {t("Bearbeiten Sie die Einstellungen rund um Ihre Organisation und deren Zweigstellen.")}
                </p>
              </Link>
            </Box>
          )}
          {hasPermission("users:Visible") && (
            <Box>
              <Link to={`${path}/users`}>
                <img src={UserIcon} alt={t("Benutzer")} />

                <h1>{t("Benutzer")}</h1>

                <p>{t("Sehen und verwalten Sie hier alle angelegten Benutzer und deren Zugehörigkeiten.")}</p>
              </Link>
            </Box>
          )}
          {hasPermission("policies:Visible") && (
            <Box>
              <Link to={`${path}/policies`}>
                <img src={PolicyIcon} alt={t("Policies")} />

                <h1>{t("Policies")}</h1>

                <p>
                  {t(
                    "Verwalten und überprüfen Sie hier die verschiedenen Policies und damit verbundenen Berechtigungen."
                  )}
                </p>
              </Link>
            </Box>
          )}
          {hasPermission("alarms:Visible") && (
            <Box>
              <Link to={`${path}/alarms`}>
                <img src={AlarmIcon} alt={t("Meldungen")} />

                <h1>{t("Meldungen")}</h1>

                <p>{t("Verwalten Sie hier die Meldungen für Ihr DKV.live Portal.")}</p>
              </Link>
            </Box>
          )}
        </BoxContainer>
      </PageContent>
    </Page>
  );
};

export default Selection;
