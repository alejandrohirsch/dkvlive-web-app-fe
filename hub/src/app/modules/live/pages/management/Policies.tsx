import React, {useState, useCallback, useEffect} from "react";
import Page from "app/components/Page";
import {useTranslation} from "react-i18next";
import DataTable, {
  TablePageContent,
  useColumns,
  TableStickyHeader,
  TableStickyFooter,
  // TableFooterRightActions,
  TableContainer,
  // bodyField,
  ToolbarButtonIcon,
  ToolbarButton,
  TableFooterActions,
  TableSelectionCountInfo,
  TableFooterRightActions,
} from "app/components/DataTable";
import {Button, Loading} from "dkv-live-frontend-ui";
// import {Circle as CircleIcon} from "styled-icons/boxicons-solid/Circle";
// import {TagListWrapper} from "./components/TagListWrapper";
import PolicyDetails from "./components/PolicyDetails";
import Window from "app/components/Window";
import {ManagementModule} from "./state/module";
import store from "app/state/store";
import {useDispatch, useSelector} from "react-redux";
import {policiesListSelector, policiesListStateSelector} from "./state/policies/selectors";
import {loadPoliciesIfNeeded, deletePolicy, loadPolicies} from "./state/policies/actions";
import {Policy, emptyPolicy} from "./state/policies/reducer";
import ConfirmWindow from "app/components/ConfirmWindow";
import {hasPermission} from "app/modules/login/state/login/selectors";
import PolicyForm from "./components/PolicyForm";
import dayjs from "dayjs";
import PolicyActivityLog from "./components/PolicyActivityLog";

//-----------------------------------------------------------------------------------------------------------
store.addModule(ManagementModule);

// -----------------------------------------------------------------------------------------------------------

const policyColumns = [
  // {
  //   field: "active",
  //   header: "Aktiv",
  //   width: 30,
  //   body: bodyField((a) =>
  //     a.active ? (
  //       <CircleIcon size={24} style={{color: "var(--dkv-highlight-ok-color)"}} />
  //     ) : (
  //       <CircleIcon size={24} style={{color: "var(--dkv-highlight-error-color)"}} />
  //     )
  //   ),
  // },
  {
    field: "name",
    header: "Bezeichnung",
    width: 200,
  },
  // {
  //   field: "level",
  //   header: "Zugriffslevel",
  //   width: 100,
  //   body: bodyField((a) => {
  //     switch (a.level) {
  //       case 1:
  //         return "Admin";
  //       default:
  //         return "User";
  //     }
  //   }),
  // },
  // {
  //   field: "user_count",
  //   header: "Useranzahl",
  //   width: 100,
  // },
  // {
  //   field: "user_divisions",
  //   header: "Divisions",
  //   width: 300,
  //   body: bodyField((a) => <TagListWrapper tagList={a.user_divisions} displayAllAsSelected />),
  // },
  {
    field: "created_at",
    header: "Angelegt",
    body: (a: Policy) =>
      a.created_at && a.created_at !== "0001-01-01T00:00:00Z"
        ? dayjs(a.created_at).format("DD.MM.YYYY HH:mm:ss")
        : "",
    width: 140,
  },
  {
    field: "modified_at",
    header: "Modified",
    body: (a: Policy) =>
      a.modified_at && a.modified_at !== "0001-01-01T00:00:00Z"
        ? dayjs(a.modified_at).format("DD.MM.YYYY HH:mm:ss")
        : "",
    width: 140,
  },
];

//-----------------------------------------------------------------------------------------------------------
interface PoliciesProps {
  backPath: string;
  openCreateDialog?: boolean;
}

const Policies: React.FC<PoliciesProps> = ({backPath, openCreateDialog}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // load policies
  const policiesList = useSelector(policiesListSelector);
  useEffect(() => {
    dispatch(loadPoliciesIfNeeded());
  }, [dispatch]);
  const policiesListState = useSelector(policiesListStateSelector);

  // actions
  // - detail
  const [activePolicyWindow, setActivePolicyWindow] = useState<Policy | null>(null);
  const onClosePolicyWindow = useCallback(() => {
    setActivePolicyWindow(null);
    setEdit(false);
  }, [setActivePolicyWindow]);
  const openPolicyWindow = useCallback((e: any) => setActivePolicyWindow(e.data as Policy), [
    setActivePolicyWindow,
  ]);

  // - edit
  const [edit, setEdit] = useState<boolean>(false);

  // - create
  const openPolicyCreateWindow = () => {
    setEdit(true);
    setActivePolicyWindow(Object.assign({...emptyPolicy}));
  };
  useEffect(() => {
    if (openCreateDialog) {
      setActivePolicyWindow(Object.assign({...emptyPolicy}));
    }
  }, [openCreateDialog]);

  // - delete
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
  const onDeleteConfirm = async (confirmed: boolean) => {
    if (!confirmed) {
      setShowDeleteConfirmation(false);
      return;
    }

    const callback = (success: boolean) => {
      if (success) {
        setShowDeleteConfirmation(false);
        dispatch(loadPolicies());
        setSelected([]);
      }
    };

    const idToBeDeleted = selected[0].id || "";
    if (idToBeDeleted) {
      await dispatch(deletePolicy(idToBeDeleted, callback));
    }
  };

  // - activity log
  const [showActivityWindow, setShowActivityWindow] = useState<boolean>(false);

  useEffect(() => {
    if (!activePolicyWindow) {
      return;
    }

    const policy = policiesList.find((p) => p.id === activePolicyWindow.id);

    if (policy) {
      setActivePolicyWindow(policy);
    }
  }, [policiesList, activePolicyWindow]);

  // columns
  const [columns] = useState(policyColumns);
  const visibleColumns = useColumns(columns);

  // selection
  const [selected, setSelected] = useState<Policy[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const deselect = () => setSelected([]);
  const openPolicyWindowForSelected = useCallback(
    (edit?: boolean) => {
      edit && setEdit(edit);
      selected.length && setActivePolicyWindow(selected[0]);
    },
    [selected, setActivePolicyWindow]
  );

  // render
  return (
    <Page id="policies">
      <Loading inprogress={policiesListState.loading} />

      <TablePageContent>
        <TableStickyHeader backPath={backPath}>
          <h1>{t("Policies")}</h1>

          {/* @todo commingsoon */}
          {/* <SearchTextField
            name="search"
            type="text"
            required
            label={t("Suche")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          /> */}

          {hasPermission("management:CreatePolicies") && (
            <ToolbarButton
              general
              style={{
                marginLeft: "auto",
              }}
              onClick={openPolicyCreateWindow}
            >
              <ToolbarButtonIcon>
                <span className="icon-ico_add" />
              </ToolbarButtonIcon>
              {t("Hinzufügen")}
            </ToolbarButton>
          )}
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={policiesList}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            onRowDoubleClick={openPolicyWindow}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Policy ausgewählt" : "Policies ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={deselect}>
                {t("Abbrechen")}
              </Button>
              <TableFooterActions>
                {hasPermission("management:EditPolicies") && (
                  <ToolbarButton onClick={() => openPolicyWindowForSelected(true)}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_edit" />
                    </ToolbarButtonIcon>
                    {t("Bearbeiten")}
                  </ToolbarButton>
                )}
                {hasPermission("management:DeletePolicies") && (
                  <ToolbarButton onClick={() => setShowDeleteConfirmation(true)}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_delete" />
                    </ToolbarButtonIcon>
                    {t("Löschen")}
                  </ToolbarButton>
                )}
              </TableFooterActions>
              <TableFooterRightActions />
            </>
          )}

          {/* <TableFooterRightActions>
            <Button autoWidth secondary ariaLabel={t("Gesamte Liste exportieren")}>
              {t("Gesamte Liste exportieren")}
            </Button>

            {selected.length > 0 && (
              <Button autoWidth ariaLabel={t("Auswahl als Liste exportieren")}>
                {t("Auswahl als Liste exportieren")}
              </Button>
            )}
          </TableFooterRightActions> */}
        </TableStickyFooter>
      </TablePageContent>

      {activePolicyWindow &&
        (edit || openCreateDialog ? (
          <Window
            onClose={() => setEdit(false)}
            headline={activePolicyWindow.id ? t("Policy bearbeiten - ") : t("Policy erstellen")}
          >
            {(props) => <PolicyForm {...props} policy={activePolicyWindow} />}
          </Window>
        ) : (
          activePolicyWindow.id && (
            <Window onClose={onClosePolicyWindow} headline={t("Detailansicht Policy")}>
              {(props) => (
                <PolicyDetails
                  {...props}
                  showActivity={setShowActivityWindow}
                  policy={activePolicyWindow}
                  setEdit={setEdit}
                />
              )}
            </Window>
          )
        ))}

      {showActivityWindow && activePolicyWindow && (
        <Window onClose={() => setShowActivityWindow(false)} headline={t("Aktivitäten Policy")}>
          {(props) => <PolicyActivityLog {...props} policyID={activePolicyWindow.id || ""} />}
        </Window>
      )}

      {showDeleteConfirmation && selected.length && (
        <ConfirmWindow
          headline={
            // (selected.length > 1
            //   ? selected.length + " " + t("ausgewählte Policies")
            //   : t("Ausgewählte Policy")) +
            // " " + t("wirklich löschen?")
            t("Ausgewählte Policy") + " " + selected[0].name + " " + t("wirklich löschen?")
          }
          onConfirm={onDeleteConfirm}
        />
      )}
    </Page>
  );
};

export default Policies;
