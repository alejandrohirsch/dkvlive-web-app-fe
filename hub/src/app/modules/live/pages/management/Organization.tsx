import React, {useState, useCallback, useEffect} from "react";
import Page from "app/components/Page";
import {useTranslation} from "react-i18next";
import DataTable, {
  TablePageContent,
  useColumns,
  TableStickyHeader,
  TableStickyFooter,
  TableContainer,
  ToolbarButton,
  ToolbarButtonIcon,
  TableFooterActions,
  bodyField,
  TableSelectionCountInfo,
  TableFooterRightActions,
} from "app/components/DataTable";
import {Button, Loading} from "dkv-live-frontend-ui";
import Window from "app/components/Window";
import store from "app/state/store";
import {useDispatch, useSelector} from "react-redux";
import {
  divisionsListSelector,
  divisionsListStateSelector,
  divisionsItemSelector,
} from "./state/divisions/selectors";
import {loadDivisions, deleteDivision, loadDivisionsIfNeeded} from "./state/divisions/actions";
import {Division, emptyDivision} from "./state/divisions/reducer";
import DivisionDetails from "./components/DivisionDetails";
import DivisionForm from "./components/DivisionForm";
import ConfirmWindow from "app/components/ConfirmWindow";
import dayjs from "dayjs";
import {hasPermission} from "app/modules/login/state/login/selectors";
import {availableLanguages} from "app/modules/internal/pages/i18n/I18n";
import DivisionActivityLog from "./components/DivisionsActivityLog";
import {ManagementModule} from "./state/module";
import OrganizationForm from "./components/OrganizationForm";
import {organizationDataSelector} from "./state/organization/selectors";
import {loadOrganization} from "./state/organization/actions";

//-----------------------------------------------------------------------------------------------------------
store.addModule(ManagementModule);

//-----------------------------------------------------------------------------------------------------------
const divisionColumns = [
  {
    field: "name",
    header: "Name",
    width: 200,
  },
  {
    field: "language_code",
    header: "Sprache",
    body: bodyField((a) => availableLanguages.find((l: any) => l.value === a.language_code)?.label),
    width: 140,
  },
  {
    field: "created_at",
    header: "Angelegt",
    width: 140,
    body: (a: Division) =>
      a.created_at && a.created_at !== "0001-01-01T00:00:00Z"
        ? dayjs(a.created_at).format("DD.MM.YYYY HH:mm:ss")
        : "",
  },
  {
    field: "modified_at",
    header: "Geändert",
    width: 140,
    body: (a: Division) =>
      a.modified_at && a.modified_at !== "0001-01-01T00:00:00Z"
        ? dayjs(a.modified_at).format("DD.MM.YYYY HH:mm:ss")
        : "",
  },
];
// -----------------------------------------------------------------------------------------------------------
interface OrganizationProps {
  backPath: string;
}

const Organization: React.FC<OrganizationProps> = ({backPath}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // columns
  const [columns] = useState(divisionColumns);
  const visibleColumns = useColumns(columns, t);

  // selection
  const [selected, setSelected] = useState<Division[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const deselect = () => setSelected([]);

  // load divisions
  const divisionsList = useSelector(divisionsListSelector);
  useEffect(() => {
    dispatch(loadDivisionsIfNeeded());
  }, [dispatch]);
  const divisionsItem = useSelector(divisionsItemSelector);
  const divisionsListState = useSelector(divisionsListStateSelector);

  // load organization
  const organization = useSelector(organizationDataSelector);
  useEffect(() => {
    dispatch(loadOrganization());
  }, [dispatch]);

  // actions
  // - detail
  const [activeDivisionWindow, setActiveDivisionWindow] = useState<Division | null>(null);
  const onCloseDivisionWindow = useCallback(() => {
    const id = activeDivisionWindow?.id || "";
    setActiveDivisionWindow(null);
    setEditDivision(false);

    if (id) {
      const updatedItem = divisionsItem[id];
      const updatedSelection = selected.filter((s) => s.id === id);
      updatedSelection.push(updatedItem);
      setSelected(updatedSelection);
    }
  }, [activeDivisionWindow, divisionsItem, selected]);
  const openDivisionWindow = useCallback((e: any) => setActiveDivisionWindow(e.data as Division), [
    setActiveDivisionWindow,
  ]);

  // - edit organization
  const [showEditOrganizationWindow, setShowEditOrganizationWindow] = useState<boolean>(false);

  // - edit division
  const [editDivision, setEditDivision] = useState<boolean>(false);

  // - create
  const openDivisionCreateWindow = () => {
    setEditDivision(true);
    setActiveDivisionWindow(Object.assign({...emptyDivision}));
  };

  // - delete
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
  const onDeleteConfirm = async (confirmed: boolean) => {
    if (!confirmed || !selected[0].id) {
      setShowDeleteConfirmation(false);
      return;
    }

    const callback = (success: boolean) => {
      if (success) {
        dispatch(loadDivisions());
        deselect();
        setShowDeleteConfirmation(false);
      }
    };

    dispatch(deleteDivision(selected[0].id, callback));
  };

  // - activity log
  const [showActivityWindow, setShowActivityWindow] = useState<boolean>(false);

  const openDivisionWindowForSelected = useCallback(
    (edit?: boolean) => {
      edit && setEditDivision(edit);
      selected.length && setActiveDivisionWindow(selected[0]);
    },
    [selected, setActiveDivisionWindow]
  );

  // useEffect(() => {
  //   if (!activeDivisionWindow) {
  //     return;
  //   }

  //   const division = divisionsItem[activeDivisionWindow?.id || ""];

  //   if (division) {
  //     setActiveDivisionWindow(division);
  //   }
  // }, [activeDivisionWindow, divisionsItem]);

  // render
  return (
    <Page id="organization">
      <Loading inprogress={divisionsListState.loading} />
      <TablePageContent>
        <TableStickyHeader backPath={backPath}>
          <h1>{t("Organisationsstruktur")}</h1>

          {/* @todo commingsoon */}
          {/* <SearchTextField
            name="search"
            type="text"
            required
            label={t("Suche")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          /> */}

          <ToolbarButton
            general
            style={{
              marginLeft: "auto",
            }}
            onClick={() => setShowEditOrganizationWindow(true)}
          >
            <ToolbarButtonIcon>
              <span className="icon-ico_edit" />
            </ToolbarButtonIcon>
            {t("Organisation bearbeiten")}
          </ToolbarButton>
          <ToolbarButton general onClick={openDivisionCreateWindow}>
            <ToolbarButtonIcon>
              <span className="icon-ico_add" />
            </ToolbarButtonIcon>
            {t("Neue Division erstellen")}
          </ToolbarButton>
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={divisionsList}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            onRowDoubleClick={openDivisionWindow}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Division ausgewählt" : "Divisions ausgewählt")}
              />

              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={deselect}>
                {t("Abbrechen")}
              </Button>

              <TableFooterActions>
                {hasPermission("management:UpdateDivisions") && (
                  <ToolbarButton onClick={() => openDivisionWindowForSelected(true)}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_edit" />
                    </ToolbarButtonIcon>

                    {t("Bearbeiten")}
                  </ToolbarButton>
                )}
                {divisionsList.length > 1 && hasPermission("management:DeleteDivisions") && (
                  <ToolbarButton onClick={() => setShowDeleteConfirmation(true)}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_delete" />
                    </ToolbarButtonIcon>

                    {t("Löschen")}
                  </ToolbarButton>
                )}
              </TableFooterActions>
              {/* <TableFooterRightActions>
            <Button autoWidth secondary ariaLabel={t("Gesamte Liste exportieren")}>
              {t("Gesamte Liste exportieren")}
            </Button>

            {selected.length > 0 && (
              <Button autoWidth ariaLabel={t("Auswahl als Liste exportieren")}>
                {t("Auswahl als Liste exportieren")}
              </Button>
            )}
          </TableFooterRightActions> */}
              <TableFooterRightActions />
            </>
          )}
        </TableStickyFooter>
      </TablePageContent>

      {activeDivisionWindow &&
        (editDivision ? (
          <Window
            onClose={() => setEditDivision(false)}
            headline={activeDivisionWindow.id ? t("Division bearbeiten: ") : t("Division erstellen")}
          >
            {(props) => <DivisionForm {...props} division={activeDivisionWindow} />}
          </Window>
        ) : (
          activeDivisionWindow.id && (
            <Window onClose={onCloseDivisionWindow} headline={t("Detailansicht Division")}>
              {(props) => (
                <DivisionDetails
                  {...props}
                  division={activeDivisionWindow}
                  setEdit={setEditDivision}
                  showActivity={setShowActivityWindow}
                />
              )}
            </Window>
          )
        ))}

      {showDeleteConfirmation && selected.length && (
        <ConfirmWindow
          headline={
            // (selected.length > 1
            //   ? selected.length + " " + t("ausgewählte Divisions")
            //   : t("Ausgewählte Division")) +
            // " " +
            // t("wirklich löschen?")
            t("Ausgewählte Division") + " " + selected[0].name + " " + t("wirklich löschen?")
          }
          onConfirm={onDeleteConfirm}
        />
      )}

      {showEditOrganizationWindow && organization && (
        <Window onClose={() => setShowEditOrganizationWindow(false)} headline={t("Organisation bearbeiten")}>
          {(props) => <OrganizationForm {...props} organization={organization} />}
        </Window>
      )}

      {showActivityWindow && activeDivisionWindow && (
        <Window onClose={() => setShowActivityWindow(false)} headline={t("Aktivitäten Division")}>
          {(props) => <DivisionActivityLog {...props} divisionID={activeDivisionWindow.id || ""} />}
        </Window>
      )}
    </Page>
  );
};

export default Organization;
