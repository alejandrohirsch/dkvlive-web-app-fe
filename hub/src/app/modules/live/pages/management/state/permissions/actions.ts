import {ManagementThunkResult} from "../module";
import {Permission} from "./reducer";
import httpClient from "services/http";
import {
  LoadingPermissionsAction,
  LOADING_PERMISSIONS,
  PermissionsLoadedAction,
  PERMISSIONS_LOADED,
  LoadingPermissionsFailedAction,
  LOADING_PERMISSIONS_FAILED,
} from "./types";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
function loadingPermissionsAction(): LoadingPermissionsAction {
  return {
    type: LOADING_PERMISSIONS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingPermissionsFailedAction(
  errorText?: string,
  unsetItems = true
): LoadingPermissionsFailedAction {
  return {
    type: LOADING_PERMISSIONS_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function permissionsLoadedAction(items: Permission[]): PermissionsLoadedAction {
  return {
    type: PERMISSIONS_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadPermissions(): ManagementThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingPermissionsAction());

    // @todo: only get permissions for current user
    const [{data}, err] = await httpClient.get("/management/permissions/findAll", {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        loadingPermissionsFailedAction("Laden fehlgeschlagen" + (": " + err.response?.data.message || ""))
      );
      return;
    }

    dispatch(permissionsLoadedAction(data.filter((p: Permission) => hasPermission(p.key))));
  };
}

export function loadPermissionsIfNeeded(): ManagementThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().management.permissions;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadPermissions());
  };
}
