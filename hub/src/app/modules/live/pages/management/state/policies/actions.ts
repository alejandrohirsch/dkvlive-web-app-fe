import {ManagementThunkResult} from "../module";
import {Policy} from "./reducer";
import httpClient from "services/http";
import {
  LoadingPoliciesAction,
  LOADING_POLICIES,
  PoliciesLoadedAction,
  POLICIES_LOADED,
  LoadingPoliciesFailedAction,
  LOADING_POLICIES_FAILED,
  ProcessingPolicyActionAction,
  PROCESSING_POLICY_ACTION,
  ProcessingPolicyActionFailedAction,
  PROCESSING_POLICY_ACTION_FAILED,
  PolicyActionProcessedAction,
  POLICY_ACTION_PROCESSED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingPoliciesAction(): LoadingPoliciesAction {
  return {
    type: LOADING_POLICIES,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingPoliciesFailedAction(errorText?: string, unsetItems = true): LoadingPoliciesFailedAction {
  return {
    type: LOADING_POLICIES_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function policiesLoadedAction(items: Policy[]): PoliciesLoadedAction {
  return {
    type: POLICIES_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
function processingPolicyActionAction(): ProcessingPolicyActionAction {
  return {
    type: PROCESSING_POLICY_ACTION,
  };
}

// -----------------------------------------------------------------------------------------------------------
function processingPolicyActionFailedAction(errorText?: string): ProcessingPolicyActionFailedAction {
  return {
    type: PROCESSING_POLICY_ACTION_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function policyActionProcessedAction(policy: Policy): PolicyActionProcessedAction {
  return {
    type: POLICY_ACTION_PROCESSED,
    payload: policy,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadPolicies(): ManagementThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingPoliciesAction());

    // @todo: only get policies for current user
    const [{data}, err] = await httpClient.get("/management/policies", {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        loadingPoliciesFailedAction("Laden fehlgeschlagen" + (": " + err.response?.data.message || ""))
      );
      return;
    }

    const policies: Array<Policy> = [];

    data.forEach((d: any) => {
      const permissions = {
        allow: [],
        deny: [],
      };

      if (d.permission_configs) {
        d.permission_configs.forEach((p: {action: string; key: string}) => permissions[p.action].push(p.key));
      }
      policies.push({...d, permissions: permissions});
    });

    dispatch(policiesLoadedAction(policies));
  };
}

export function loadPoliciesIfNeeded(): ManagementThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().management.policies;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadPolicies());
  };
}

export function updatePolicy(
  id: string,
  policy: Policy,
  callback?: (success: boolean) => void
): ManagementThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingPolicyActionAction());

    const permission_configs = policy.permissions.allow
      .map((p: string) => {
        return {key: p, action: "allow"};
      })
      .concat(
        policy.permissions.deny.map((p: string) => {
          return {key: p, action: "deny"};
        })
      );

    const [{response}, err] = await httpClient.put(
      "/management/policies/" + id,
      JSON.stringify({...policy, permission_configs: permission_configs}),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    if (err !== null) {
      // @todo: log
      dispatch(
        processingPolicyActionFailedAction(
          "Speichern fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);
      return;
    }

    dispatch(policyActionProcessedAction(response));
    callback && callback(true);
  };
}

export function createPolicy(
  policy: Policy,
  callback?: (success: boolean) => void
): ManagementThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingPolicyActionAction());

    const permission_configs = policy.permissions.allow
      .map((p: string) => {
        return {key: p, action: "allow"};
      })
      .concat(
        policy.permissions.deny.map((p: string) => {
          return {key: p, action: "deny"};
        })
      );

    const [{response}, err] = await httpClient.post(
      "/management/policies",
      JSON.stringify({...policy, permission_configs: permission_configs}),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    if (err !== null) {
      // @todo: log
      dispatch(
        processingPolicyActionFailedAction(
          "Erstellen fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);
      return;
    }

    dispatch(policyActionProcessedAction(response));
    callback && callback(true);
  };
}

export function deletePolicy(
  id: string,
  callback?: (success: boolean) => void
): ManagementThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingPolicyActionAction());
    const [{response}, err] = await httpClient.delete("/management/policies/" + id);

    if (err !== null) {
      // @todo: log
      dispatch(
        processingPolicyActionFailedAction(
          "Löschen fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);
      return;
    }

    dispatch(policyActionProcessedAction(response));
    callback && callback(true);
  };
}
