import {ManagementState} from "../reducer";
import {
  LoadingOrganizationFailedAction,
  OrganizationLoadedAction,
  ProcessingOrganizationActionFailedAction,
} from "./types";
import {Division} from "../divisions/reducer";

// -----------------------------------------------------------------------------------------------------------
export interface Organization {
  id: string;
  name: string;
  divisions: Division[];
  shipper?: string;
  carrier?: string;
  suspension?: any;
  createdByOrder?: boolean;
  customer_number?: string;
  notification_email?: string;
  created_at?: string;
  modified_at?: string;
}

export interface OrganizationData {
  data?: Organization;
  state: {
    id?: string;
    loading: boolean;
    error?: boolean;
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingOrganization(state: ManagementState) {
  const data = state.organization.state;

  data.loading = true;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingOrganizationFailed(
  state: ManagementState,
  action: LoadingOrganizationFailedAction
) {
  const data = state.organization.state;

  data.id = "";
  data.loading = false;
  data.error = true;
  data.errorText = action.payload;
}

// -----------------------------------------------------------------------------------------------------------
export function handleOrganizationLoaded(state: ManagementState, action: OrganizationLoadedAction) {
  const data = state.organization.state;

  data.loading = false;
  if (action.payload.id) {
    data.id = action.payload.id;
    state.organization.data = action.payload;
  }

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleProcessingOrganizationAction(state: ManagementState) {
  const data = state.organization.state;

  data.loading = true;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleProcessingOrganizationActionFailed(
  state: ManagementState,
  action: ProcessingOrganizationActionFailedAction
) {
  const data = state.organization.state;

  data.loading = false;
  data.error = true;
  data.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleOrganizationActionProcessed(state: ManagementState) {
  const data = state.organization.state;

  data.loading = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}
