import {Organization} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ORGANIZATION = "management/loadingOrganization";

export interface LoadingOrganizationAction {
  type: typeof LOADING_ORGANIZATION;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ORGANIZATION_FAILED = "management/loadingOrganizationFailed";

export interface LoadingOrganizationFailedAction {
  type: typeof LOADING_ORGANIZATION_FAILED;
  payload: string;
}

// -----------------------------------------------------------------------------------------------------------
export const ORGANIZATION_LOADED = "management/organizationLoaded";

export interface OrganizationLoadedAction {
  type: typeof ORGANIZATION_LOADED;
  payload: Organization;
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_ORGANIZATION_ACTION = "management/processingOrganizationAction";

export interface ProcessingOrganizationActionAction {
  type: typeof PROCESSING_ORGANIZATION_ACTION;
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_ORGANIZATION_ACTION_FAILED =
  "organimanagementzations/processingOrganizationActionFailed";

export interface ProcessingOrganizationActionFailedAction {
  type: typeof PROCESSING_ORGANIZATION_ACTION_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const ORGANIZATION_ACTION_PROCESSED = "management/OrganizationActionProcessed";

export interface OrganizationActionProcessedAction {
  type: typeof ORGANIZATION_ACTION_PROCESSED;
  payload: {organization?: Organization};
}
// -----------------------------------------------------------------------------------------------------------

export type OrganizationActions =
  | LoadingOrganizationAction
  | LoadingOrganizationFailedAction
  | OrganizationLoadedAction
  | ProcessingOrganizationActionAction
  | ProcessingOrganizationActionFailedAction
  | OrganizationActionProcessedAction;
