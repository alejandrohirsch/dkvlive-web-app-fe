import {ManagementModuleState} from "../module";

export const organizationDataSelector = (state: ManagementModuleState) => state.management.organization.data;

export const organizationStateSelector = (state: ManagementModuleState) =>
  state.management.organization.state;
