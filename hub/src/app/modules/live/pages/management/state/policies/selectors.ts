import {ManagementModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const policiesItemsSelector = (state: ManagementModuleState) => state.management.policies.items;

// list
export const policiesListStateSelector = (state: ManagementModuleState) => state.management.policies.list;

export const policiesListSelector = createSelector(policiesItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// item
export const policiesItemStateSelector = (state: ManagementModuleState) => state.management.policies.item;
export const policiesSelector = (id: string) =>
  createSelector(policiesItemsSelector, (items) => (items ? items[id] : undefined));
