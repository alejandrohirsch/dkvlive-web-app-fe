import {
  LoadingDivisionsAction,
  LOADING_DIVISIONS,
  LoadingDivisionsFailedAction,
  LOADING_DIVISIONS_FAILED,
  DivisionsLoadedAction,
  DIVISIONS_LOADED,
  ProcessingDivisionActionFailedAction,
  ProcessingDivisionActionAction,
  DivisionActionProcessedAction,
  DIVISION_ACTION_PROCESSED,
  PROCESSING_DIVISION_ACTION_FAILED,
  PROCESSING_DIVISION_ACTION,
} from "../divisions/types";
import {Division} from "./reducer";
import {ManagementThunkResult} from "../module";
import httpClient from "services/http";

// -----------------------------------------------------------------------------------------------------------
function loadingDivisionsAction(): LoadingDivisionsAction {
  return {
    type: LOADING_DIVISIONS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingDivisionsFailedAction(errorText?: string, unsetItems = true): LoadingDivisionsFailedAction {
  return {
    type: LOADING_DIVISIONS_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function divisionsLoadedAction(items: Division[]): DivisionsLoadedAction {
  return {
    type: DIVISIONS_LOADED,
    payload: items,
  };
}
// -----------------------------------------------------------------------------------------------------------
function processingDivisionActionAction(): ProcessingDivisionActionAction {
  return {
    type: PROCESSING_DIVISION_ACTION,
  };
}

// -----------------------------------------------------------------------------------------------------------
function processingDivisionActionFailedAction(errorText?: string): ProcessingDivisionActionFailedAction {
  return {
    type: PROCESSING_DIVISION_ACTION_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function divisionActionProcessedAction(division?: Record<string, unknown>): DivisionActionProcessedAction {
  return {
    type: DIVISION_ACTION_PROCESSED,
    payload: {division},
  };
}
// -----------------------------------------------------------------------------------------------------------

export function loadDivisions(): ManagementThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingDivisionsAction());
    const [{data}, err] = await httpClient.get("/management/divisions", {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        loadingDivisionsFailedAction("Laden fehlgeschlagen" + (": " + err.response?.data.message || ""))
      );
      return;
    }

    dispatch(divisionsLoadedAction(data));
  };
}

export function loadDivisionsIfNeeded(): ManagementThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().management.divisions;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadDivisions());
  };
}

export function updateDivision(
  id: string,
  division: Division,
  callback?: (success: boolean) => void
): ManagementThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingDivisionActionAction());
    const [{response}, err] = await httpClient.put("/management/divisions/" + id, JSON.stringify(division), {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        processingDivisionActionFailedAction(
          "Speichern fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);

      return;
    }

    dispatch(divisionActionProcessedAction(response));
    callback && callback(true);
  };
}

export function createDivision(
  division: Division,
  callback?: (success: boolean) => void
): ManagementThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingDivisionActionAction());
    const [{response}, err] = await httpClient.post("/management/divisions", JSON.stringify(division), {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        processingDivisionActionFailedAction(
          "Erstellen fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);

      return;
    }

    dispatch(divisionActionProcessedAction(response));
    callback && callback(true);
  };
}

export function deleteDivision(
  id: string,
  callback?: (success: boolean) => void
): ManagementThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingDivisionActionAction());
    const [{response}, err] = await httpClient.delete("/management/divisions/" + id, {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        processingDivisionActionFailedAction(
          "Löschen fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);
      return;
    }

    dispatch(divisionActionProcessedAction(response));
    callback && callback(true);
  };
}
