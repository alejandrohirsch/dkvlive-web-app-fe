import {ManagementModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const permissionsItemsSelector = (state: ManagementModuleState) => state.management.permissions.items;

// list
export const permissionsListStateSelector = (state: ManagementModuleState) =>
  state.management.permissions.list;

export const permissionsListSelector = createSelector(permissionsItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// item
export const permissionSelector = (id: string) =>
  createSelector(permissionsItemsSelector, (items) => (items ? items[id] : undefined));
