import {createSelector} from "@reduxjs/toolkit";
import {ManagementModuleState} from "../module";

export const divisionsItemsSelector = (state: ManagementModuleState) => state.management.divisions.items;

// list
export const divisionsListStateSelector = (state: ManagementModuleState) => state.management.divisions.list;

export const divisionsListSelector = createSelector(divisionsItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// item
export const divisionsItemStateSelector = (state: ManagementModuleState) => state.management.divisions.item;

export const divisionsItemSelector = (id: string) =>
  createSelector(divisionsItemsSelector, (items) => (items ? items[id] : {}));
