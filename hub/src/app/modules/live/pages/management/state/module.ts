import {IModule} from "redux-dynamic-modules";
import {ManagementActionTypes} from "./actions";
import {ThunkAction} from "redux-thunk";
import {ManagementState, reducer} from "./reducer";

export interface ManagementModuleState {
  management: ManagementState;
}

export type ManagementThunkResult<R> = ThunkAction<R, ManagementModuleState, void, ManagementActionTypes>;

export const ManagementModule: IModule<ManagementModuleState> = {
  id: "management",
  reducerMap: {
    management: reducer,
  },
};
