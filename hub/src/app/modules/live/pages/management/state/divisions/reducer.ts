import {ManagementState} from "../reducer";
import {
  DivisionsLoadedAction,
  LoadingDivisionsFailedAction,
  ProcessingDivisionActionFailedAction,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface Division {
  id?: string;
  name: string;
  organization_id?: string;
  suspension?: Suspension;
  language_code: string;
  created_at?: string;
  modified_at?: string;
}

export const emptyDivision: Division = {
  name: "",
  language_code: "de",
};

export interface Suspension {
  state: boolean;
  since?: string;
  cause?: string;
  by?: string;
}

export interface DivisionsData {
  list: {
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  item: {
    loading: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: string]: Division};
}
// -----------------------------------------------------------------------------------------------------------
export function handleLoadingDivisions(state: ManagementState) {
  const data = state.divisions.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingDivisionsFailed(state: ManagementState, action: LoadingDivisionsFailedAction) {
  const data = state.divisions;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleDivisionsLoaded(state: ManagementState, action: DivisionsLoadedAction) {
  const data = state.divisions;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.forEach((n) => n.id && (items[n.id] = n));
  data.items = items;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleProcessingDivisionAction(state: ManagementState) {
  const data = state.divisions.item;

  data.loading = true;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleProcessingDivisionActionFailed(
  state: ManagementState,
  action: ProcessingDivisionActionFailedAction
) {
  const data = state.divisions.item;

  data.loading = false;
  data.error = true;
  data.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleDivisionActionProcessed(state: ManagementState) {
  const data = state.divisions.item;

  data.loading = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}
