import httpClient from "services/http";
import {
  LoadingOrganizationAction,
  LOADING_ORGANIZATION,
  LOADING_ORGANIZATION_FAILED,
  LoadingOrganizationFailedAction,
  OrganizationLoadedAction,
  ORGANIZATION_LOADED,
  ProcessingOrganizationActionAction,
  ProcessingOrganizationActionFailedAction,
  ORGANIZATION_ACTION_PROCESSED,
  OrganizationActionProcessedAction,
  PROCESSING_ORGANIZATION_ACTION_FAILED,
  PROCESSING_ORGANIZATION_ACTION,
} from "./types";
import {Organization} from "./reducer";
import {ManagementThunkResult} from "../module";

// -----------------------------------------------------------------------------------------------------------
function loadingOrganizationAction(): LoadingOrganizationAction {
  return {
    type: LOADING_ORGANIZATION,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingOrganizationFailedAction(errorText?: string): LoadingOrganizationFailedAction {
  return {
    type: LOADING_ORGANIZATION_FAILED,
    payload: errorText ?? "",
  };
}

// -----------------------------------------------------------------------------------------------------------
function organizationLoadedAction(item: Organization): OrganizationLoadedAction {
  return {
    type: ORGANIZATION_LOADED,
    payload: item,
  };
}

// -----------------------------------------------------------------------------------------------------------
function processingOrganizationActionAction(): ProcessingOrganizationActionAction {
  return {
    type: PROCESSING_ORGANIZATION_ACTION,
  };
}

// -----------------------------------------------------------------------------------------------------------
function processingOrganizationActionFailedAction(
  errorText?: string
): ProcessingOrganizationActionFailedAction {
  return {
    type: PROCESSING_ORGANIZATION_ACTION_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function organizationActionProcessedAction(organization?: Organization): OrganizationActionProcessedAction {
  return {
    type: ORGANIZATION_ACTION_PROCESSED,
    payload: {organization},
  };
}
// -----------------------------------------------------------------------------------------------------------
export function loadOrganization(): ManagementThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingOrganizationAction());

    const [{data}, err] = await httpClient.get("/management/organizations", {
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (err !== null) {
      // @todo: log
      dispatch(
        loadingOrganizationFailedAction("Laden fehlgeschlagen" + (": " + err.response?.data.message || ""))
      );
      return;
    }

    dispatch(organizationLoadedAction(data[0]));
  };
}

export function updateOrganization(
  id: string,
  organization: Organization,
  callback?: (success: boolean) => void
): ManagementThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingOrganizationActionAction());
    const [{response}, err] = await httpClient.put(
      "/management/organizations/" + id,
      JSON.stringify(organization),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    if (err !== null) {
      // @todo: log
      dispatch(
        processingOrganizationActionFailedAction(
          "Speichern fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);
      return;
    }

    dispatch(organizationActionProcessedAction(response));
    callback && callback(true);
  };
}
