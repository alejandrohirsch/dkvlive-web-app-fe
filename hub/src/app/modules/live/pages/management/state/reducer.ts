import {createReducer} from "@reduxjs/toolkit";
import {PERMISSIONS_LOADED, LOADING_PERMISSIONS, LOADING_PERMISSIONS_FAILED} from "./permissions/types";
import {
  handleLoadingPermissions,
  handleLoadingPermissionsFailed,
  handlePermissionsLoaded,
  PermissionData,
} from "./permissions/reducer";
import {
  UserData,
  handleUsersLoaded,
  handleLoadingUsers,
  handleLoadingUsersFailed,
  handleProcessingUserAction,
  handleProcessingUserActionFailed,
  handleUserActionProcessed,
} from "./users/reducer";
import {
  LOADING_USERS,
  LOADING_USERS_FAILED,
  USERS_LOADED,
  PROCESSING_USER_ACTION,
  PROCESSING_USER_ACTION_FAILED,
  USER_ACTION_PROCESSED,
} from "./users/types";
import {
  LOADING_POLICIES,
  LOADING_POLICIES_FAILED,
  POLICIES_LOADED,
  PROCESSING_POLICY_ACTION_FAILED,
  POLICY_ACTION_PROCESSED,
  PROCESSING_POLICY_ACTION,
} from "./policies/types";
import {
  handleLoadingPolicies,
  handleLoadingPoliciesFailed,
  handlePoliciesLoaded,
  PolicyData,
  handleProcessingPolicyAction,
  handleProcessingPolicyActionFailed,
  handlePolicyActionProcessed,
} from "./policies/reducer";
import {
  LOADING_DIVISIONS,
  LOADING_DIVISIONS_FAILED,
  DIVISIONS_LOADED,
  DIVISION_ACTION_PROCESSED,
  PROCESSING_DIVISION_ACTION,
  PROCESSING_DIVISION_ACTION_FAILED,
} from "./divisions/types";
import {
  DivisionsData,
  handleLoadingDivisions,
  handleLoadingDivisionsFailed,
  handleDivisionsLoaded,
  handleProcessingDivisionAction,
  handleProcessingDivisionActionFailed,
  handleDivisionActionProcessed,
} from "./divisions/reducer";
import {
  LOADING_ORGANIZATION,
  LOADING_ORGANIZATION_FAILED,
  ORGANIZATION_LOADED,
  PROCESSING_ORGANIZATION_ACTION,
  ORGANIZATION_ACTION_PROCESSED,
  PROCESSING_ORGANIZATION_ACTION_FAILED,
} from "./organization/types";
import {
  handleLoadingOrganization,
  handleOrganizationLoaded,
  handleLoadingOrganizationFailed,
  handleProcessingOrganizationAction,
  handleProcessingOrganizationActionFailed,
  handleOrganizationActionProcessed,
  OrganizationData,
} from "./organization/reducer";

// -----------------------------------------------------------------------------------------------------------
export interface ManagementState {
  permissions: PermissionData;
  policies: PolicyData;
  users: UserData;
  organization: OrganizationData;
  divisions: DivisionsData;
}

const initialState: ManagementState = {
  permissions: {
    list: {loading: true},
  },
  policies: {
    list: {loading: true},
    item: {loading: false},
  },
  users: {
    list: {loading: true},
    item: {loading: false},
  },
  organization: {
    state: {loading: true},
  },
  divisions: {
    list: {loading: true},
    item: {loading: false},
  },
};

export const reducer = createReducer(initialState, {
  // permissions
  [LOADING_PERMISSIONS]: handleLoadingPermissions,
  [LOADING_PERMISSIONS_FAILED]: handleLoadingPermissionsFailed,
  [PERMISSIONS_LOADED]: handlePermissionsLoaded,
  // policies
  [LOADING_POLICIES]: handleLoadingPolicies,
  [LOADING_POLICIES_FAILED]: handleLoadingPoliciesFailed,
  [POLICIES_LOADED]: handlePoliciesLoaded,
  [PROCESSING_POLICY_ACTION]: handleProcessingPolicyAction,
  [PROCESSING_POLICY_ACTION_FAILED]: handleProcessingPolicyActionFailed,
  [POLICY_ACTION_PROCESSED]: handlePolicyActionProcessed,
  // users
  [LOADING_USERS]: handleLoadingUsers,
  [LOADING_USERS_FAILED]: handleLoadingUsersFailed,
  [USERS_LOADED]: handleUsersLoaded,
  [PROCESSING_USER_ACTION]: handleProcessingUserAction,
  [PROCESSING_USER_ACTION_FAILED]: handleProcessingUserActionFailed,
  [USER_ACTION_PROCESSED]: handleUserActionProcessed,
  // organization
  [LOADING_ORGANIZATION]: handleLoadingOrganization,
  [LOADING_ORGANIZATION_FAILED]: handleLoadingOrganizationFailed,
  [ORGANIZATION_LOADED]: handleOrganizationLoaded,
  [PROCESSING_ORGANIZATION_ACTION]: handleProcessingOrganizationAction,
  [PROCESSING_ORGANIZATION_ACTION_FAILED]: handleProcessingOrganizationActionFailed,
  [ORGANIZATION_ACTION_PROCESSED]: handleOrganizationActionProcessed,
  // divisions
  [LOADING_DIVISIONS]: handleLoadingDivisions,
  [LOADING_DIVISIONS_FAILED]: handleLoadingDivisionsFailed,
  [DIVISIONS_LOADED]: handleDivisionsLoaded,
  [PROCESSING_DIVISION_ACTION]: handleProcessingDivisionAction,
  [PROCESSING_DIVISION_ACTION_FAILED]: handleProcessingDivisionActionFailed,
  [DIVISION_ACTION_PROCESSED]: handleDivisionActionProcessed,
});
