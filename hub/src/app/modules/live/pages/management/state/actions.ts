import {PermissionActions} from "./permissions/types";
import {PolicyActions} from "./policies/types";
import {UserActions} from "./users/types";
import {DivisionsActions} from "./divisions/types";
import {OrganizationActions} from "./organization/types";

export type ManagementActionTypes =
  | PermissionActions
  | PolicyActions
  | UserActions
  | OrganizationActions
  | DivisionsActions;
