import {ManagementState} from "../reducer";
import {PermissionsLoadedAction, LoadingPermissionsFailedAction} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface Permission {
  id: string;
  name: string;
  key: string;
  created_at: string;
  modified_at: string;
  description?: string;
}

export interface PermissionData {
  list: {
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: string]: Permission};
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingPermissions(state: ManagementState) {
  const data = state.permissions.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingPermissionsFailed(
  state: ManagementState,
  action: LoadingPermissionsFailedAction
) {
  const data = state.permissions;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handlePermissionsLoaded(state: ManagementState, action: PermissionsLoadedAction) {
  const data = state.permissions;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.forEach((t) => (items[t.id] = t));
  data.items = items;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}
