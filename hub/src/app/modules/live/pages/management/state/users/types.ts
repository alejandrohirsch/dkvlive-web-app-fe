import {User} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_USERS = "management/loadingUsers";

export interface LoadingUsersAction {
  type: typeof LOADING_USERS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_USERS_FAILED = "management/loadingUsersFailed";

export interface LoadingUsersFailedAction {
  type: typeof LOADING_USERS_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const USERS_LOADED = "management/usersLoaded";

export interface UsersLoadedAction {
  type: typeof USERS_LOADED;
  payload: User[];
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_USER_ACTION = "management/processingUserAction";

export interface ProcessingUserActionAction {
  type: typeof PROCESSING_USER_ACTION;
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_USER_ACTION_FAILED = "management/processingUserActionFailed";

export interface ProcessingUserActionFailedAction {
  type: typeof PROCESSING_USER_ACTION_FAILED;
  payload: {errorText?: string};
}

// -----------------------------------------------------------------------------------------------------------
export const USER_ACTION_PROCESSED = "management/userActionProcessed";

export interface UserActionProcessedAction {
  type: typeof USER_ACTION_PROCESSED;
  payload: User;
}

// -----------------------------------------------------------------------------------------------------------
export type UserActions =
  | LoadingUsersAction
  | LoadingUsersFailedAction
  | UsersLoadedAction
  | ProcessingUserActionAction
  | ProcessingUserActionFailedAction
  | UserActionProcessedAction;
