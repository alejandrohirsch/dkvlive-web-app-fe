import {ManagementModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const usersItemsSelector = (state: ManagementModuleState) => state.management.users.items;

// list
export const usersListStateSelector = (state: ManagementModuleState) => state.management.users.list;

export const usersListSelector = createSelector(usersItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// item
export const usersItemStateSelector = (state: ManagementModuleState) => state.management.users.item;
