import {ManagementState} from "../reducer";
import {PoliciesLoadedAction, LoadingPoliciesFailedAction, ProcessingPolicyActionFailedAction} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface Policy {
  id?: string;
  organization_id?: string;
  active?: boolean;
  name: string;
  // level: number;
  // user_count: number;
  // user_divisions: string[];
  permissions: {
    allow: string[];
    deny: string[];
  };
  created_at?: string;
  modified_at?: string;
  note?: string;
}

export const emptyPolicy: Policy = {
  name: "",
  permissions: {
    allow: [],
    deny: [],
  },
};

// export interface Permission {
//   id: string;
//   name: string;
// }

export interface PolicyData {
  list: {
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  item: {
    loading: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: string]: Policy};
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingPolicies(state: ManagementState) {
  const data = state.policies.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingPoliciesFailed(state: ManagementState, action: LoadingPoliciesFailedAction) {
  const data = state.policies;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handlePoliciesLoaded(state: ManagementState, action: PoliciesLoadedAction) {
  const data = state.policies;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.forEach((t) => {
    if (t.id) {
      items[t.id] = Object.assign({...emptyPolicy}, t);
    }
  });
  data.items = items;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleProcessingPolicyAction(state: ManagementState) {
  const data = state.policies;

  data.item.loading = true;

  if (data.item.error) {
    delete data.item.error;
    delete data.item.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------

export function handleProcessingPolicyActionFailed(
  state: ManagementState,
  action: ProcessingPolicyActionFailedAction
) {
  const data = state.policies;

  data.item.loading = false;
  data.item.error = true;
  data.item.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------

export function handlePolicyActionProcessed(state: ManagementState) {
  const data = state.policies;

  data.item.loading = false;

  if (data.item.error) {
    delete data.item.error;
    delete data.item.errorText;
  }
}
