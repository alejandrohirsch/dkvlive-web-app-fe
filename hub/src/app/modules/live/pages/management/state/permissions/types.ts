import {Permission} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_PERMISSIONS = "management/loadingPermissions";

export interface LoadingPermissionsAction {
  type: typeof LOADING_PERMISSIONS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_PERMISSIONS_FAILED = "management/loadingPermissionsFailed";

export interface LoadingPermissionsFailedAction {
  type: typeof LOADING_PERMISSIONS_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const PERMISSIONS_LOADED = "management/permissionsLoaded";

export interface PermissionsLoadedAction {
  type: typeof PERMISSIONS_LOADED;
  payload: Permission[];
}

// -----------------------------------------------------------------------------------------------------------
export type PermissionActions =
  | LoadingPermissionsAction
  | LoadingPermissionsFailedAction
  | PermissionsLoadedAction;
