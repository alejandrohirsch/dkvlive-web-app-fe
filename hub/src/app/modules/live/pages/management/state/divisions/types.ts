import {Division} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_DIVISIONS = "management/loadingDivisions";

export interface LoadingDivisionsAction {
  type: typeof LOADING_DIVISIONS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_DIVISIONS_FAILED = "management/loadingDivisionsFailed";

export interface LoadingDivisionsFailedAction {
  type: typeof LOADING_DIVISIONS_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const DIVISIONS_LOADED = "management/divisionsLoaded";

export interface DivisionsLoadedAction {
  type: typeof DIVISIONS_LOADED;
  payload: Division[];
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_DIVISION_ACTION = "management/processingDivisionAction";

export interface ProcessingDivisionActionAction {
  type: typeof PROCESSING_DIVISION_ACTION;
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_DIVISION_ACTION_FAILED = "management/processingDivisionActionFailed";

export interface ProcessingDivisionActionFailedAction {
  type: typeof PROCESSING_DIVISION_ACTION_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const DIVISION_ACTION_PROCESSED = "management/divisionActionProcessed";

export interface DivisionActionProcessedAction {
  type: typeof DIVISION_ACTION_PROCESSED;
  payload: {division?: Record<string, unknown>};
}
// -----------------------------------------------------------------------------------------------------------
export type DivisionsActions =
  | LoadingDivisionsAction
  | LoadingDivisionsFailedAction
  | DivisionsLoadedAction
  | ProcessingDivisionActionAction
  | ProcessingDivisionActionFailedAction
  | DivisionActionProcessedAction;
