import {MapState} from "../reducer";
import {
  GeofencesLoadedAction,
  LoadingGeofencesFailedAction,
  LoadingGeofenceFailedAction,
  GeofenceLoadedAction,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface Geofence {
  id: string;
  name: string;
  description: string;
  polygon: Polygon;
  active: boolean;
  public: boolean;
  created_at: any;
  modified_at: any;
}

export interface Polygon {
  type: string;
  coordinates: number[][][];
}

export interface GeofencesData {
  list: {
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  item: {
    loading: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: string]: Geofence};
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingGeofences(state: MapState) {
  const data = state.geofences.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingGeofencesFailed(state: MapState, action: LoadingGeofencesFailedAction) {
  const data = state.geofences;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleGeofencesLoaded(state: MapState, action: GeofencesLoadedAction) {
  const data = state.geofences;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.forEach((n) => (items[n.id] = n));
  data.items = items;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingGeofence(state: MapState) {
  const data = state.geofences.item;

  data.loading = true;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingGeofenceFailed(state: MapState, action: LoadingGeofenceFailedAction) {
  const data = state.geofences.item;

  data.loading = false;
  data.error = true;
  data.errorText = action.payload;
}

// -----------------------------------------------------------------------------------------------------------
export function handleGeofenceLoaded(state: MapState, action: GeofenceLoadedAction) {
  const data = state.geofences;

  data.item.loading = false;

  data.items = {...data.items, [action.payload.id]: action.payload};

  if (data.item.error) {
    delete data.item.error;
    delete data.item.errorText;
  }
}
