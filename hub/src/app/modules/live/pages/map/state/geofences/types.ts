import {Geofence} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_GEOFENCES = "map/loadingGeofences";

export interface LoadingGeofencesAction {
  type: typeof LOADING_GEOFENCES;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_GEOFENCES_FAILED = "map/loadingGeofencesFailed";

export interface LoadingGeofencesFailedAction {
  type: typeof LOADING_GEOFENCES_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const GEOFENCES_LOADED = "map/geofencesLoaded";

export interface GeofencesLoadedAction {
  type: typeof GEOFENCES_LOADED;
  payload: Geofence[];
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_GEOFENCE = "map/loadingGeofence";

export interface LoadingGeofenceAction {
  type: typeof LOADING_GEOFENCE;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_GEOFENCE_FAILED = "map/loadingGeofenceFailed";

export interface LoadingGeofenceFailedAction {
  type: typeof LOADING_GEOFENCE_FAILED;
  payload: string;
}

// -----------------------------------------------------------------------------------------------------------
export const GEOFENCE_LOADED = "map/namespaceLoaded";

export interface GeofenceLoadedAction {
  type: typeof GEOFENCE_LOADED;
  payload: Geofence;
}

// -----------------------------------------------------------------------------------------------------------
export type GeofencesActions =
  | LoadingGeofencesAction
  | LoadingGeofencesFailedAction
  | GeofencesLoadedAction
  | LoadingGeofenceAction
  | LoadingGeofenceFailedAction
  | GeofenceLoadedAction;
