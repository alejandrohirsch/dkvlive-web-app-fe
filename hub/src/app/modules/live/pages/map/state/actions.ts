import {OverlayActions} from "./overlay/types";
import {DetailsActions} from "./details/types";
import {FuelStationsActions} from "./fuelstations/types";
import {GeofencesActions} from "./geofences/types";

export type MapActionTypes = OverlayActions | DetailsActions | FuelStationsActions | GeofencesActions;
