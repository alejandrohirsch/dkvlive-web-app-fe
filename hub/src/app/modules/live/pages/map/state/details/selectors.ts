import {MapModuleState} from "../module";

export const detailsSelector = (state: MapModuleState) => state.map.details;
