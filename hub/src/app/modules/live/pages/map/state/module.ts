import {IModule} from "redux-dynamic-modules";
import {MapActionTypes} from "./actions";
import {ThunkAction} from "redux-thunk";
import {MapState, reducer} from "./reducer";

export interface MapModuleState {
  map: MapState;
}

export type MapThunkResult<R> = ThunkAction<R, MapModuleState, void, MapActionTypes>;

export const MapModule: IModule<MapModuleState> = {
  id: "map",
  reducerMap: {
    map: reducer,
  },
};
