import {OverlayType} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const SHOW_OVERLAY = "map/showOverlay";

export interface ShowOverlayAction {
  type: typeof SHOW_OVERLAY;
  payload: {
    type: OverlayType;
    assetID: string;
    data?: any;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const HIDE_OVERLAY = "map/hideOverlay";

export interface HideOverlayAction {
  type: typeof HIDE_OVERLAY;
}

// -----------------------------------------------------------------------------------------------------------
export type OverlayActions = ShowOverlayAction | HideOverlayAction;
