import {
  ShowDetailsAction,
  SHOW_DETAILS,
  HideDetailsAction,
  HIDE_DETAILS,
  CHANGE_DETAILS_TAB,
  ChangeDetailsTabAction,
  HIDE_DETAILS_OVERLAY,
  HideDetailsOverlayAction,
  SHOW_DETAILS_OVERLAY,
  ShowDetailsOverlayAction,
} from "./types";
import {TabType} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export function showDetails(assetID: string, tab?: TabType): ShowDetailsAction {
  return {
    type: SHOW_DETAILS,
    payload: {assetID, tab},
  };
}

// -----------------------------------------------------------------------------------------------------------
export function hideDetails(): HideDetailsAction {
  return {
    type: HIDE_DETAILS,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function hideDetailsOverlay(): HideDetailsOverlayAction {
  return {
    type: HIDE_DETAILS_OVERLAY,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function showDetailsOverlay(): ShowDetailsOverlayAction {
  return {
    type: SHOW_DETAILS_OVERLAY,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function changeDetailsTab(tab: TabType): ChangeDetailsTabAction {
  return {
    type: CHANGE_DETAILS_TAB,
    payload: {tab},
  };
}
