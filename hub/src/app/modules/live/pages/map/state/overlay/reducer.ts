import {MapState} from "../reducer";
import {ShowOverlayAction} from "./types";

// -----------------------------------------------------------------------------------------------------------
export type OverlayType = string;

export const OVERLAY_ROUTE: OverlayType = "route";
export const OVERLAY_GEOFENCE: OverlayType = "geofence";
export const OVERLAY_GEOFENCE_EDIT: OverlayType = "geofence_edit";
export const OVERLAY_FUELSTATION: OverlayType = "fuelstation";

// -----------------------------------------------------------------------------------------------------------
export interface OverlayData {
  visible: boolean;
  type?: OverlayType;
  assetID?: string;
  data?: any;
}

// -----------------------------------------------------------------------------------------------------------
export function handleShowOverlay(state: MapState, action: ShowOverlayAction) {
  const data = state.overlay;

  data.visible = true;
  data.type = action.payload.type;
  data.assetID = action.payload.assetID;
  data.data = action.payload.data;
}

// -----------------------------------------------------------------------------------------------------------
export function handleHideOverlay(state: MapState) {
  const data = state.overlay;
  data.visible = false;
}
