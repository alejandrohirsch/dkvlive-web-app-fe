import {ShowOverlayAction, SHOW_OVERLAY, HideOverlayAction, HIDE_OVERLAY} from "./types";
import {OverlayType} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export function showOverlay(type: OverlayType, assetID: string, data?: any): ShowOverlayAction {
  return {
    type: SHOW_OVERLAY,
    payload: {type, assetID, data},
  };
}

// -----------------------------------------------------------------------------------------------------------
export function hideOverlay(): HideOverlayAction {
  return {
    type: HIDE_OVERLAY,
  };
}
