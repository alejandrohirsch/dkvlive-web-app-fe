import {createReducer} from "@reduxjs/toolkit";
import {SHOW_OVERLAY, HIDE_OVERLAY} from "./overlay/types";
import {OverlayData, handleShowOverlay, handleHideOverlay} from "./overlay/reducer";
import {
  DetailsData,
  handleShowDetails,
  handleHideDetails,
  handleChangeDetailsTab,
  handleHideDetailsOverlay,
  handleShowDetailsOverlay,
} from "./details/reducer";
import {
  SHOW_DETAILS,
  HIDE_DETAILS,
  CHANGE_DETAILS_TAB,
  HIDE_DETAILS_OVERLAY,
  SHOW_DETAILS_OVERLAY,
} from "./details/types";
import {
  FuelStationData,
  handleFuelStationsLoaded,
  handleLoadingFuelStations,
  handleLoadingFuelStationsFailed,
} from "./fuelstations/reducer";
import {FUELSTATIONS_LOADED, LOADING_FUELSTATIONS, LOADING_FUELSTATIONS_FAILED} from "./fuelstations/types";
import {GEOFENCES_LOADED, LOADING_GEOFENCES, LOADING_GEOFENCES_FAILED} from "./geofences/types";
import {
  GeofencesData,
  handleGeofencesLoaded,
  handleLoadingGeofences,
  handleLoadingGeofencesFailed,
} from "./geofences/reducer";

// -----------------------------------------------------------------------------------------------------------
export interface MapState {
  overlay: OverlayData;
  details: DetailsData;
  fuelStations: FuelStationData;
  geofences: GeofencesData;
}

const initialState: MapState = {
  overlay: {
    visible: false,
  },
  details: {
    dataOverlayVisible: true,
  },
  fuelStations: {
    loading: false,
  },
  geofences: {
    list: {loading: true},
    item: {loading: true},
  },
};

export const reducer = createReducer(initialState, {
  [SHOW_OVERLAY]: handleShowOverlay,
  [HIDE_OVERLAY]: handleHideOverlay,
  [SHOW_DETAILS]: handleShowDetails,
  [HIDE_DETAILS]: handleHideDetails,
  [CHANGE_DETAILS_TAB]: handleChangeDetailsTab,
  [HIDE_DETAILS_OVERLAY]: handleHideDetailsOverlay,
  [SHOW_DETAILS_OVERLAY]: handleShowDetailsOverlay,
  [LOADING_FUELSTATIONS]: handleLoadingFuelStations,
  [LOADING_FUELSTATIONS_FAILED]: handleLoadingFuelStationsFailed,
  [FUELSTATIONS_LOADED]: handleFuelStationsLoaded,
  [LOADING_GEOFENCES]: handleLoadingGeofences,
  [LOADING_GEOFENCES_FAILED]: handleLoadingGeofencesFailed,
  [GEOFENCES_LOADED]: handleGeofencesLoaded,
});
