import {TabType} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const SHOW_DETAILS = "map/showDetails";

export interface ShowDetailsAction {
  type: typeof SHOW_DETAILS;
  payload: {
    assetID: string;
    tab?: TabType;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const HIDE_DETAILS = "map/hideDetails";

export interface HideDetailsAction {
  type: typeof HIDE_DETAILS;
}

// -----------------------------------------------------------------------------------------------------------
export const HIDE_DETAILS_OVERLAY = "map/hideDetailsOverlay";

export interface HideDetailsOverlayAction {
  type: typeof HIDE_DETAILS_OVERLAY;
}

// -----------------------------------------------------------------------------------------------------------
export const SHOW_DETAILS_OVERLAY = "map/showDetailsOverlay";

export interface ShowDetailsOverlayAction {
  type: typeof SHOW_DETAILS_OVERLAY;
}

// -----------------------------------------------------------------------------------------------------------
export const CHANGE_DETAILS_TAB = "map/changeDetailsTab";

export interface ChangeDetailsTabAction {
  type: typeof CHANGE_DETAILS_TAB;
  payload: {
    tab: TabType;
  };
}

// -----------------------------------------------------------------------------------------------------------
export type DetailsActions = ShowDetailsAction | HideDetailsAction | ChangeDetailsTabAction;
