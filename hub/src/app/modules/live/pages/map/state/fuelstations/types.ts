import {FuelStation} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_FUELSTATIONS = "map/loadingFuelStations";

export interface LoadingFuelStationsAction {
  type: typeof LOADING_FUELSTATIONS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_FUELSTATIONS_FAILED = "map/loadingFuelStationsFailed";

export interface LoadingFuelStationsFailedAction {
  type: typeof LOADING_FUELSTATIONS_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const FUELSTATIONS_LOADED = "map/fuelStationsLoaded";

export interface FuelStationsLoadedAction {
  type: typeof FUELSTATIONS_LOADED;
  payload: FuelStation[];
}

// -----------------------------------------------------------------------------------------------------------
export type FuelStationsActions =
  | LoadingFuelStationsAction
  | LoadingFuelStationsFailedAction
  | FuelStationsLoadedAction;
