import {MapState} from "../reducer";
import {ShowDetailsAction, ChangeDetailsTabAction} from "./types";

// -----------------------------------------------------------------------------------------------------------
export type TabType = string;

export const TAB_OVERVIEW: TabType = "overview";
export const TAB_DRIVINGTIME: TabType = "drivingtime";
export const TAB_ASSETDATA: TabType = "assetdata";

// -----------------------------------------------------------------------------------------------------------
export interface DetailsData {
  dataOverlayVisible: boolean;
  assetID?: string;
  tab?: string;
}

// -----------------------------------------------------------------------------------------------------------
export function handleShowDetails(state: MapState, action: ShowDetailsAction) {
  const data = state.details;

  data.assetID = action.payload.assetID;

  if (!data.tab) {
    data.tab = action.payload.tab ?? "overview";
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleHideDetails(state: MapState) {
  state.details = {
    dataOverlayVisible: state.details.dataOverlayVisible,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function handleChangeDetailsTab(state: MapState, action: ChangeDetailsTabAction) {
  const data = state.details;

  data.tab = action.payload.tab;
}

// -----------------------------------------------------------------------------------------------------------
export function handleHideDetailsOverlay(state: MapState) {
  state.details = {
    ...state.details,
    dataOverlayVisible: false,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function handleShowDetailsOverlay(state: MapState) {
  state.details = {
    ...state.details,
    dataOverlayVisible: true,
  };
}
