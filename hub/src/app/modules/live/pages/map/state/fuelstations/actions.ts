import {MapThunkResult} from "../module";
import {FuelStation} from "./reducer";
import httpClient from "services/http";
import {
  FUELSTATIONS_LOADED,
  FuelStationsLoadedAction,
  LOADING_FUELSTATIONS,
  LOADING_FUELSTATIONS_FAILED,
  LoadingFuelStationsAction,
  LoadingFuelStationsFailedAction,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingFuelStationsAction(): LoadingFuelStationsAction {
  return {
    type: LOADING_FUELSTATIONS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingFuelStationsFailedAction(errorText?: string): LoadingFuelStationsFailedAction {
  return {
    type: LOADING_FUELSTATIONS_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function fuelStationsLoadedAction(items: FuelStation[]): FuelStationsLoadedAction {
  return {
    type: FUELSTATIONS_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadFuelStations(
  latitude: number | null | undefined,
  longitude: number | null | undefined,
  minDistance: number,
  maxDistance: number
): MapThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingFuelStationsAction());

    // @todo
    const [{data}, err] = await httpClient.get(
      `/poi/pois/findDKVStations?lat=${latitude}&lon=${longitude}&min=${minDistance}&max=${maxDistance}`
    );
    if (err !== null) {
      // @todo: log
      dispatch(loadingFuelStationsFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(fuelStationsLoadedAction(data));
  };
}
