import {MapThunkResult} from "../module";
import {Geofence} from "./reducer";
import httpClient from "services/http";
import {
  LoadingGeofencesAction,
  LOADING_GEOFENCES,
  GeofencesLoadedAction,
  GEOFENCES_LOADED,
  LoadingGeofencesFailedAction,
  LOADING_GEOFENCES_FAILED,
  LoadingGeofenceAction,
  LOADING_GEOFENCE,
  LoadingGeofenceFailedAction,
  LOADING_GEOFENCE_FAILED,
  GeofenceLoadedAction,
  GEOFENCE_LOADED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingGeofencesAction(): LoadingGeofencesAction {
  return {
    type: LOADING_GEOFENCES,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingGeofencesFailedAction(errorText?: string, unsetItems = true): LoadingGeofencesFailedAction {
  return {
    type: LOADING_GEOFENCES_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function geofencesLoadedAction(items: Geofence[]): GeofencesLoadedAction {
  return {
    type: GEOFENCES_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingGeofenceAction(): LoadingGeofenceAction {
  return {
    type: LOADING_GEOFENCE,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingGeofenceFailedAction(errorText?: string): LoadingGeofenceFailedAction {
  return {
    type: LOADING_GEOFENCE_FAILED,
    payload: errorText ?? "",
  };
}

// -----------------------------------------------------------------------------------------------------------
function geofenceLoadedAction(item: Geofence): GeofenceLoadedAction {
  return {
    type: GEOFENCE_LOADED,
    payload: item,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadGeofences(): MapThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingGeofencesAction());

    const [{data}, err] = await httpClient.get("/management/geofences");
    if (err !== null) {
      // @todo: log
      dispatch(loadingGeofencesFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(geofencesLoadedAction(data));
  };
}

export function loadGeofencesIfNeeded(): MapThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().map.geofences;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadGeofences());
  };
}

export function deleteGeofence(id: string): MapThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingGeofenceAction());

    const [, err] = await httpClient.delete(`/management/geofences/${id}`);
    if (err !== null) {
      // @todo: log
      dispatch(loadingGeofenceFailedAction("Löschen fehlgeschlagen"));
      return;
    }

    dispatch(loadGeofences());
  };
}

export function loadGeofence(id: string): MapThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingGeofenceAction());

    const [{data}, err] = await httpClient.get(`/management/geofences/${id}`);
    if (err !== null) {
      // @todo: log
      dispatch(loadingGeofenceFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(geofenceLoadedAction(data));
  };
}
