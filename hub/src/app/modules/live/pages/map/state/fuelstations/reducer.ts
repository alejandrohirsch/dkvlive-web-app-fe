import {MapState} from "../reducer";
import {FuelStationsLoadedAction, LoadingFuelStationsFailedAction} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface FuelStation {
  id: string;
  organization: string;
  divisions: string[];
  source: string;
  source_ref: string;
  category: string;
  brand: string;
  names: string[];
  location: Location;
  address: Address;
  contact: Contact;
  features: string[];
  fuel_types: string[];
  route: Route;
  price: number;
  created_at: string;
  modified_at: string;

  _status?: string;
}

export interface Location {
  type: string;
  coordinates: number[];
}

export interface Address {
  street: string;
  zip: string;
  city: string;
  country: string;
}

export interface Contact {
  email: string;
  phone: string;
  fax: string;
}

export interface Route {
  shape: number[];
  distance: number;
  time: number;
}

export interface Maneuver {
  position: {
    latitude: 47.56664;
    longitude: 12.15097;
  };
}

export interface FuelStationData {
  loading: boolean;
  loaded?: boolean;
  error?: boolean;
  errorText?: string;
  items?: {[id: string]: FuelStation};
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingFuelStations(state: MapState) {
  const data = state.fuelStations;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingFuelStationsFailed(state: MapState, action: LoadingFuelStationsFailedAction) {
  const data = state.fuelStations;

  data.loading = false;
  data.error = true;
  data.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleFuelStationsLoaded(state: MapState, action: FuelStationsLoadedAction) {
  const data = state.fuelStations;

  data.loading = false;
  data.loaded = true;

  const items = {};
  if (action.payload != null) {
    action.payload.forEach((a) => {
      items[a.id] = a;
    });
  }

  data.items = items;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}
