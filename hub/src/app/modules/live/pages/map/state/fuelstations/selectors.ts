import {MapModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const fuelStationsItemsSelector = (state: MapModuleState) => state.map.fuelStations.items;

// list
export const fuelStationsListSelector = createSelector(fuelStationsItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// fuelstation
export const fuelStationsSelector = (id: string) =>
  createSelector(fuelStationsItemsSelector, (items) => (items ? items[id] : undefined));

// loading
export const fuelStationsLoadingSelector = (state: MapModuleState) => state.map.fuelStations.loading;
