import {MapModuleState} from "../module";

export const overlaySelector = (state: MapModuleState) => state.map.overlay;
