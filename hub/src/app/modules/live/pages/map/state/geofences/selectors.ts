import {MapModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const geofencesItemsSelector = (state: MapModuleState) => state.map.geofences.items;

// list
export const geofencesListStateSelector = (state: MapModuleState) => state.map.geofences.list;

export const geofencesListSelector = createSelector(geofencesItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// item
export const geofencesItemStateSelector = (state: MapModuleState) => state.map.geofences.item;

export const geofenceSelector = (id: string) =>
  createSelector(geofencesItemsSelector, (items) => (items ? items[id] : undefined));
