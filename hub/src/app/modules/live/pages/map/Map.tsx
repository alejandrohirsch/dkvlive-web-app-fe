import React, {useEffect, useRef, useState} from "react";
import styled from "styled-components/macro";
import {loadScript, loadScripts} from "dkv-live-frontend-ui";
import config from "config";
import Sidebar from "./components/Sidebar";
import MapAssets from "./components/MapAssets";
import MapGlobalSearch from "./components/MapGlobalSearch";
import store from "app/state/store";
import {MapModule} from "./state/module";
import {loadAssetsIfNeeded} from "../../state/assets/actions";
import {useDispatch} from "react-redux";
import MapOverlay from "./components/MapOverlay";
import MapSettings from "./components/MapSettings";
import AssetDetails from "./components/AssetDetails";
import MapContextMenu from "./components/MapContextMenu";
import MapDataOverlay from "./components/MapDataOverlay";
import MapTracemode from "./components/MapTracemode";
import DrivingtimeDetails from "./components/DrivingtimeDetails";

// -----------------------------------------------------------------------------------------------------------
store.addModule(MapModule);

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  width: 100%;
  height: 100%;
  background-color: var(--dkv-background-secondary-color);
  position: relative;
  display: flex;
  flex-flow: row nowrap;
`;

const MapContainer = styled.div`
  width: 100%;
  position: relative;
`;

// -----------------------------------------------------------------------------------------------------------
const mapLibraryBaseURL = "https://js.api.here.com/v3/3.1"; // Download new css file if version is updated.

let resizeHandler: (() => void) | undefined;
let currentZoom = 10;
let currentCenter = {lat: 47.565775, lng: 12.151248};

// -----------------------------------------------------------------------------------------------------------
export interface HereMap {
  map: any;
  ui: any;
  platform: any;
  defaultLayers: any;
  behavior: any;
}

const Map: React.FC = () => {
  // load assets
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadAssetsIfNeeded());

    // @todo: remove
    // setTimeout(() => {
    //   const data = [
    //     {id: "1", position: {lat: 47.566369, lng: 12.151753}, track: 150, license_plate: "HH 6633 18"},
    //     {id: "3", position: {lat: 47.566169, lng: 14.151253}, track: 168, license_plate: "HH 6633 19"},
    //   ];

    //   dispatch(assetsUpdateAction(data));
    // }, 10000);
  }, [dispatch]);

  // build map
  const [isBuild, setIsBuild] = useState(false);

  const mapContainerRef = useRef<HTMLDivElement>(null);
  const mapRef = useRef<HereMap>();

  useEffect(() => {
    const build = async () => {
      // https://developer.here.com/documentation/maps/api_reference
      // https://github.com/DefinitelyTyped/DefinitelyTyped/blob/e7bafe48b853ac5d3975a25f1024e5833a0873e6/types/heremaps/index.d.ts
      // https://developer.here.com/tutorials/javascript-api/
      // https://tcs.ext.here.com/examples/v3.1
      // https://developer.here.com/documentation/examples/maps-js/clustering/marker-clustering

      if ((window as any).H === undefined) {
        await loadScript(`${mapLibraryBaseURL}/mapsjs-core.js`);
        await loadScripts([
          `${mapLibraryBaseURL}/mapsjs-service.js`,
          `${mapLibraryBaseURL}/mapsjs-ui.js`,
          `${mapLibraryBaseURL}/mapsjs-mapevents.js`,
          `${mapLibraryBaseURL}/mapsjs-clustering.js`,
        ]);
      }

      const platform = new H.service.Platform({
        apikey: config.HERE_API_KEY,
        useCIT: true,
        useHTTPS: true,
      });

      const defaultLayers = platform.createDefaultLayers();

      const map = new H.Map(mapContainerRef.current, defaultLayers.vector.normal.map, {
        zoom: currentZoom,
        center: currentCenter,
        pixelRatio: window.devicePixelRatio || 1,
      });

      map.__centerOnAsset = false;

      // enable map events (zoom, ...)
      const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

      map.addEventListener("mapviewchangeend", () => {
        currentCenter = map.getCenter();
        currentZoom = map.getZoom();
      });

      // enable default ui
      const ui = new H.ui.UI(map, {
        scalebar: {
          alignment: H.ui.LayoutAlignment.BOTTOM_RIGHT,
        },
        zoom: {
          slider: true,
          alignment: H.ui.LayoutAlignment.RIGHT_BOTTOM,
        },
      });

      // style
      // const provider = map.getBaseLayer().getProvider();
      // const style = new H.map.Style(`${publicURL}/assets/map/base.yaml?v=${config.VERSION}`);
      // provider.setStyle(style);

      // resize
      resizeHandler = () => {
        setTimeout(() => map.getViewPort().resize());
      };
      window.addEventListener("resize", resizeHandler);
      window.addEventListener("dkv-resize", resizeHandler);

      // draws
      /*
      {
        const lineString = new H.geo.LineString();
        lineString.pushPoint({lat: 47.56734538244875, lng: 12.150460285572654});
        lineString.pushPoint({lat: 47.56773967674009, lng: 12.150423761724173});
        lineString.pushPoint({lat: 47.56794668005482, lng: 12.150076785163588});
        lineString.pushPoint({lat: 47.56787028606964, lng: 12.149353612963637});
        lineString.pushPoint({lat: 47.5674562780182, lng: 12.149123512718198});
        lineString.pushPoint({lat: 47.567155627263105, lng: 12.149360917733334});
        lineString.pushPoint({lat: 47.56704719541979, lng: 12.14990877546057});
        const polygon = new H.map.Polygon(lineString, {
          style: {
            fillColor: "rgba(0, 75, 120, .25)",
            strokeColor: "rgb(0, 75, 120)",
            lineWidth: 3,
          },
        });

        let polyInfoBubble: any;

        polygon.addEventListener("pointerenter", (ev: any) => {
          map.getViewPort().element.style.cursor = "pointer";

          if (polyInfoBubble) {
            ui.removeBubble(polyInfoBubble);
            polyInfoBubble.dispose();
            polyInfoBubble = null;
          }

          const point = map.screenToGeo(ev.pointers[0].viewportX, ev.pointers[0].viewportY);
          polyInfoBubble = new H.ui.InfoBubble(point, {content: "Endach - Kreisverkehr Überlastung"});
          polyInfoBubble.addClass("dkv-map-asset-fencetooltipbubble");

          ui.addBubble(polyInfoBubble);
        });

        polygon.addEventListener("pointermove", (ev: any) => {
          const point = map.screenToGeo(ev.pointers[0].viewportX, ev.pointers[0].viewportY);
          polyInfoBubble.setPosition(point);
        });

        polygon.addEventListener("pointerleave", () => {
          map.getViewPort().element.style.cursor = "auto";

          if (polyInfoBubble) {
            ui.removeBubble(polyInfoBubble);
            polyInfoBubble.dispose();
            polyInfoBubble = null;
          }
        });

        polygon.addEventListener("tap", () => {
          dispatch(showOverlay(OVERLAY_GEOFENCE_EDIT, ""));
        });

        map.addObject(polygon);
      }
       */

      mapRef.current = {map, ui, defaultLayers, platform, behavior};

      setIsBuild(true);
    };

    build();

    return () => {
      if (resizeHandler) {
        window.removeEventListener("resize", resizeHandler);
        window.removeEventListener("dkv-resize", resizeHandler);
        resizeHandler = undefined;
      }
    };
  }, [dispatch]);

  // render
  return (
    <Container>
      <Sidebar />

      <MapContainer ref={mapContainerRef}>
        {isBuild && (
          <>
            <AssetDetails hereMap={mapRef.current as HereMap} />
            <MapDataOverlay />
            <MapOverlay hereMap={mapRef.current as HereMap} />
            <MapSettings hereMap={mapRef.current as HereMap} />
          </>
        )}
      </MapContainer>

      {isBuild && (
        <>
          <MapAssets hereMap={mapRef.current as HereMap} />
          <MapGlobalSearch hereMap={mapRef.current as HereMap} />
          <MapContextMenu hereMap={mapRef.current as HereMap} />
          <MapTracemode hereMap={mapRef.current as HereMap} />
        </>
      )}

      <DrivingtimeDetails />
    </Container>
  );
};

export default Map;
