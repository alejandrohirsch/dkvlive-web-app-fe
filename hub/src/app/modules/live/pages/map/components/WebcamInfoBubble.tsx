import React from "react";
import styled from "styled-components";
import {IconButton} from "@material-ui/core";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.section`
  white-space: nowrap;
  padding: 0;
  width: 430px;
  height: 450px;
`;

const CloseButton = styled(IconButton)`
  position: absolute !important;
  bottom: 14px;
  right: 10px;
  top: 10px;
  padding: 0 !important;
  width: 24px;
  height: 24px;

  span {
    color: #363636 !important;
  }
`;

const Frame = styled.iframe`
  width: 100%;
  height: 100%;
`;

// -----------------------------------------------------------------------------------------------------------
interface AssetInfoBubbleProps {
  cam: any;
  close: () => void;
}

const AssetInfoBubble: React.FC<AssetInfoBubbleProps> = React.memo(({cam, close}) => {
  // render
  return (
    <Container>
      <Frame src={cam.url} frameBorder="0" title={cam.name} />

      <CloseButton onClick={close} title={"Schließen"}>
        <span className="icon-ico_clear" style={{fontSize: 24}} />
      </CloseButton>
    </Container>
  );
});

export default AssetInfoBubble;
