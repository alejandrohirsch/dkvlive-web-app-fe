import React, {useCallback, useEffect, useRef} from "react";
import styled, {css} from "styled-components";
import {FuelStation} from "../state/fuelstations/reducer";
import {useSelector} from "react-redux";
import {fuelStationsListSelector} from "../state/fuelstations/selectors";
import {Collapse, FormControl, MenuItem, Select} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import ServiceStationIcon from "../../../../../../assets/poi/dkv-service-small.svg";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  height: 100%;
`;

const PriceDetails = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 15px 0 0;
`;

const PriceText = styled.div`
  font-weight: bold;
`;

interface PriceLevelProps {
  hidden: boolean;
}

const PriceLevel = styled.div<PriceLevelProps>`
  ${(props) =>
    props.hidden &&
    css`
      display: none;
    `}
`;

interface PriceIconProps {
  color: string;
}

const PriceIcon = styled.div<PriceIconProps>`
  display: inline-block;
  height: 15px;
  width: 15px;
  border-radius: 50%;
  text-align: center;
  line-height: 15px;
  font-size: 13px;
  color: white;
  margin-right: 3px;
  background-color: ${(props) => props.color};
`;

const List = styled.div`
  max-height: 400px;
  overflow: auto;
`;

interface FuelStationItemContainerProps {
  expanded?: boolean;
}

const FuelStationItemCard = styled.div`
  border-top: 1px solid var(--dkv-background-secondary-color);
  padding: 15px 0;
  position: relative;
  cursor: pointer;
`;

const FuelStationItemContainer = styled.div<FuelStationItemContainerProps>`
  padding: 0 30px;

  ${(props) =>
    props.expanded &&
    css`
      background: var(--dkv-background-primary-hover-color);

      & + & {
        & ${FuelStationItemCard} {
          border-top: none;
        }
      }
    `}
`;

const FuelStationItemHeader = styled.div`
  font-weight: bold;
  width: 90%;
`;

const FuelStationItemBody = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 5px;

  & ${PriceLevel} {
    margin-right: 50px;
  }
`;

const FuelStationItemCollapseButton = styled.span`
  display: inline-block;
  position: absolute;
  right: -8px;
  top: 50%;
  transform: translateY(-50%);
`;

const FuelStationItemDetails = styled(Collapse)`
  padding-bottom: 10px;
`;

const AddressDetails = styled.div``;

const AddressItem = styled.div`
  margin: 5px 0;
`;

const RouteDetails = styled.div``;

const ServiceDetails = styled.div`
  margin: 15px 0 0;
`;

const ServiceList = styled.ul`
  & li {
    margin: 5px 0;
  }
`;

const StationFormControl = styled(FormControl)`
  width: 100%;

  .MuiInputBase-root {
    font-size: 11px;
    border-radius: 20px;
    color: var(--dkv-text-primary-color);
    font-family: "FrutigerNextLTW05", sans-serif;

    .Mui-focused {
      outline: 0;
    }
  }

  &:focus {
    outline: 0 !important;
  }

  &:first-child {
    margin-right: 5px;
  }
`;

const StationSelect = styled(Select)`
  .MuiSelect-root {
    padding: 7px;
  }

  .MuiOutlinedInput-notchedOutline {
    border: 0;
  }

  .MuiSelect-outlined {
    &.MuiSelect-outlined {
      padding-right: 20px;
      border-radius: 20px;
      border: 1px solid var(--dkv-text-primary-color);
    }
  }

  .MuiSelect-select {
    &:focus {
      background-color: initial;
    }
  }

  .Mui-focused {
    border: none;
  }
`;

const SelectMenuItem = styled(MenuItem)``;

const Filter = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 30px;
  margin: 10px 0 15px 0;
`;

// -----------------------------------------------------------------------------------------------------------

interface FuelStationItemProps {
  fuelStation: FuelStation;
  expanded: boolean;
  onClick: (id: string) => void;
  onMouseOver: (id: string) => void;
  priceLevel: any;
}

const FuelStationItem: React.FC<FuelStationItemProps> = ({
  fuelStation,
  expanded,
  onClick,
  onMouseOver,
  priceLevel,
}) => {
  const {t} = useTranslation();

  const handleClick = () => {
    onClick(fuelStation.id);
  };

  const handleMouseOver = () => {
    onMouseOver(fuelStation.id);
  };

  const priceLevelIconRender = (priceLevel: any, hidden: boolean) => {
    switch (priceLevel) {
      case 1:
        return (
          <PriceLevel hidden={hidden}>
            <PriceIcon color="green">€</PriceIcon>
          </PriceLevel>
        );
      case 2:
        return (
          <PriceLevel hidden={hidden}>
            <PriceIcon color="orange">€</PriceIcon>
            <PriceIcon color="orange">€</PriceIcon>
          </PriceLevel>
        );
      case 3:
        return (
          <PriceLevel hidden={hidden}>
            <PriceIcon color="red">€</PriceIcon>
            <PriceIcon color="red">€</PriceIcon>
            <PriceIcon color="red">€</PriceIcon>
          </PriceLevel>
        );
      default:
        return;
    }
  };

  const priceLevelTextRender = (priceLevel: any) => {
    switch (priceLevel) {
      case 1:
        return t("Preisniveau sehr gut");
      case 2:
        return t("Preisniveau gut");
      case 3:
        return t("Preisniveau befriedigend");
      default:
        return;
    }
  };

  return (
    <FuelStationItemContainer expanded={expanded}>
      <FuelStationItemCard onClick={handleClick} onMouseOver={handleMouseOver}>
        <FuelStationItemHeader>
          {fuelStation.category === "FUEL" ? (
            <span className="icon-ico_fuel" />
          ) : (
            <img style={{width: 14, height: 14}} src={ServiceStationIcon} alt={""} />
          )}{" "}
          {fuelStation.names[0]}, {fuelStation.address.street}
        </FuelStationItemHeader>

        <FuelStationItemBody>
          {fuelStation.route && fuelStation.route.distance !== 0 && fuelStation.route.time !== 0 && (
            <RouteDetails>
              In {Math.round(fuelStation.route.distance / 1000)} km /{" "}
              {Math.round(fuelStation.route.time / 60)} Min.
            </RouteDetails>
          )}
          {fuelStation.category === "FUEL" && priceLevelIconRender(priceLevel, expanded)}
        </FuelStationItemBody>

        <FuelStationItemCollapseButton>
          {!expanded ? (
            <span className={`icon-ico_chevron-down`} style={{fontSize: 25, color: "#353535"}} />
          ) : (
            <span className={`icon-ico_chevron-up`} style={{fontSize: 25, color: "#353535"}} />
          )}
        </FuelStationItemCollapseButton>
      </FuelStationItemCard>

      <FuelStationItemDetails in={expanded} timeout="auto" unmountOnExit>
        <AddressDetails>
          <AddressItem>{fuelStation.address.street}</AddressItem>
          <AddressItem>
            {fuelStation.address.country} - {fuelStation.address.zip} {fuelStation.address.city}
          </AddressItem>
          {fuelStation.contact.phone && <AddressItem>Tel.: {fuelStation.contact.phone}</AddressItem>}
          <AddressItem>DKV-Nummer: {fuelStation.source_ref}</AddressItem>
        </AddressDetails>

        {fuelStation.category === "FUEL" && (
          <PriceDetails>
            <PriceText>{priceLevelTextRender(priceLevel)}</PriceText>
            {priceLevelIconRender(priceLevel, !expanded)}
          </PriceDetails>
        )}

        {fuelStation.features?.length > 0 && (
          <ServiceDetails>
            <b>{t("Services")}</b>
            <ServiceList>
              {fuelStation.features &&
                fuelStation.features.map((item) => {
                  return (
                    <li key={item}>
                      <span
                        className="icon-ico_check"
                        style={{marginRight: 5, verticalAlign: "middle", fontSize: 15, color: "#363636"}}
                      />
                      <span style={{verticalAlign: "middle"}}>{t(item)}</span>
                    </li>
                  );
                })}
            </ServiceList>
          </ServiceDetails>
        )}
      </FuelStationItemDetails>
    </FuelStationItemContainer>
  );
};

// -----------------------------------------------------------------------------------------------------------
function calcPriceLevel(stations: FuelStation[]) {
  const level = new Map();

  const min = Math.min(
    ...stations
      .filter((station) => station.price !== undefined)
      .map(function(o) {
        return o.price;
      })
  );

  const max = Math.max(
    ...stations
      .filter((station) => station.price !== undefined)
      .map(function(o) {
        return o.price;
      })
  );

  stations.forEach((station) => {
    if (station.price === min) {
      level.set(station.id, 1);
    } else if (station.price === max) {
      level.set(station.id, 3);
    } else {
      level.set(station.id, 2);
    }
  });

  return level;
}

interface FuelStationListProps {
  setActiveStation: (id: string) => void;
  activeStation: string;
  map: any;
  setSelected: (selected: boolean) => void;
  selected: boolean;
  setHoverStation: (id: string) => void;
}

const FuelStationList: React.FC<FuelStationListProps> = ({
  setActiveStation,
  activeStation,
  map,
  setSelected,
  selected,
  setHoverStation,
}) => {
  const {t} = useTranslation();
  let fuelStations = useSelector(fuelStationsListSelector);
  const [sort, setSort] = React.useState("price");
  const [serviceFilter, setServiceFilter] = React.useState([]);
  const polylineRef = useRef(null);
  const hoverRef = useRef("");

  const availableServices: string[] = [];
  if (fuelStations.length > 0) {
    fuelStations.forEach((station) => {
      if (station.features) {
        station.features.forEach((feature) => {
          if (availableServices.indexOf(feature) === -1) {
            availableServices.push(feature);
          }
        });
      }
    });
  }

  const sortStations = (sort: string) => {
    if (sort === "price") {
      const noPrice = fuelStations.filter((station) => typeof station.price === "undefined");
      const hasPrice = fuelStations.filter((station) => typeof station.price !== "undefined");
      hasPrice.sort((a: FuelStation, b: FuelStation) => a.price - b.price);
      fuelStations = hasPrice.concat(noPrice);
    } else {
      fuelStations.sort((a: FuelStation, b: FuelStation) => a.route.distance - b.route.distance);
    }
  };

  const priceLevel = calcPriceLevel(fuelStations);
  sortStations(sort);

  const handleSortChange = (event: React.ChangeEvent<{value: unknown}>) => {
    setSort(event.target.value as string);
    sortStations(event.target.value as string);
  };

  const handleServiceFilterChange = useCallback(
    (event: React.ChangeEvent<{value: unknown}>) => {
      setServiceFilter(event.target.value as []);
    },
    [setServiceFilter]
  );

  const removeRoute = useCallback(() => {
    if (polylineRef.current) {
      map.removeObject(polylineRef.current);
      polylineRef.current = null;
    }
  }, [polylineRef, map]);

  const showRoute = useCallback(
    (id: string) => {
      removeRoute();

      const lineString = new H.geo.LineString();
      const fuelStation = fuelStations.find((station) => station.id === id);

      if (fuelStation) {
        const shape = fuelStation.route.shape;
        if (shape) {
          for (let i = 0; i < shape.length; i += 2) {
            lineString.pushLatLngAlt(shape[i], shape[i + 1], 0);
          }
        }
      }

      try {
        const polyline = new H.map.Polyline(lineString, {
          style: {
            lineWidth: 5,
            strokeColor: "#004b78",
          },
        });

        map.addObject(polyline);
        polylineRef.current = polyline;
        if (!selected) {
          map.getViewPort().setPadding(100, 750, 325, 250);
          map.getViewModel().setLookAtData({
            bounds: polyline.getBoundingBox(),
          });
          setSelected(true);
        }
      } catch (err) {
        //@todo: show growl
        console.error(err);
      }
    },
    [map, fuelStations, removeRoute, setSelected, selected]
  );

  useEffect(() => {
    if (activeStation !== "") {
      showRoute(activeStation);
    } else {
      removeRoute();
    }

    return () => {
      removeRoute();
    };
  }, [activeStation, showRoute, map, removeRoute]);

  const handleClick = useCallback(
    (id: string) => {
      setSelected(false);
      showRoute(id);
      if (activeStation === id) {
        setActiveStation("");
      } else {
        setActiveStation(id);
      }
    },
    [setSelected, showRoute, activeStation, setActiveStation]
  );

  const handleMouseOver = useCallback(
    (id: string) => {
      if (hoverRef.current !== id) {
        setHoverStation(id);
        hoverRef.current = id;
      }
    },
    [setHoverStation, hoverRef]
  );

  const handleMouseLeave = useCallback(() => {
    setHoverStation("");
  }, [setHoverStation]);

  // render
  return (
    <Container>
      {fuelStations.length > 0 && (
        <Filter>
          <StationFormControl variant="outlined">
            <StationSelect
              IconComponent={(props) => (
                <span
                  {...props}
                  className={`icon-ico_chevron-down ${props.className}`}
                  style={{fontSize: 15, color: "#353535", lineHeight: 1.6}}
                />
              )}
              onChange={handleSortChange}
              value={sort}
            >
              <SelectMenuItem value="price">{t("Nach Preis sortieren")}</SelectMenuItem>
              <SelectMenuItem value="distance">{t("Nach Distanz sortieren")}</SelectMenuItem>
            </StationSelect>
          </StationFormControl>

          <StationFormControl variant="outlined">
            <StationSelect
              IconComponent={(props) => (
                <span
                  {...props}
                  className={`icon-ico_chevron-down ${props.className}`}
                  style={{fontSize: 15, color: "#353535", lineHeight: 1.6}}
                />
              )}
              onChange={handleServiceFilterChange}
              value={serviceFilter}
              renderValue={(selected) => {
                if ((selected as string[]).length === 0) {
                  return <span>{t("Service auswählen")}</span>;
                }

                return (selected as string[]).join(", ");
              }}
              displayEmpty
              multiple
            >
              <SelectMenuItem value="" disabled>
                {t("Service auswählen")}
              </SelectMenuItem>
              {availableServices.map((item) => (
                <SelectMenuItem key={item} value={item}>
                  {t(item)}
                </SelectMenuItem>
              ))}
            </StationSelect>
          </StationFormControl>
        </Filter>
      )}

      <List onMouseLeave={handleMouseLeave}>
        {fuelStations
          .filter((item) => {
            if (serviceFilter.length > 0) {
              let count = 0;
              item.features?.forEach((feature) => {
                serviceFilter.forEach((service) => {
                  if (feature.indexOf(service) === 0) {
                    count++;
                  }
                });
              });
              return count === serviceFilter.length;
            }
            return true;
          })
          .map((item) => (
            <FuelStationItem
              key={item.id}
              fuelStation={item}
              expanded={activeStation === item.id}
              onClick={handleClick}
              onMouseOver={handleMouseOver}
              priceLevel={priceLevel.get(item.id)}
            />
          ))}
      </List>
    </Container>
  );
};

export default FuelStationList;
