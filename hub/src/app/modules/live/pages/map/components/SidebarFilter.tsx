import React, {useState, useRef, useEffect} from "react";
import styled, {css} from "styled-components";
import {useSelector, useDispatch} from "react-redux";
import {assetTagsSelector, mapFilterSelector} from "app/modules/live/state/assets/selectors";
import {toggleMapFilterTagAction, setMapFilterSearch} from "app/modules/live/state/assets/actions";
import {useTranslation} from "react-i18next";
import {TextField, debounce} from "dkv-live-frontend-ui";

// -----------------------------------------------------------------------------------------------------------
const FilterContainer = styled.section`
  padding: 12px;
`;

interface FilterContainerHeadlineProps {
  margin?: boolean;
}

const FilterContainerHeadline = styled.h2<FilterContainerHeadlineProps>`
  font-size: 0.8571428571428571rem;
  color: #363636;
  border-bottom: 1px solid #979797;
  padding-bottom: 4px;
  text-transform: uppercase;
  margin-bottom: 12px;

  ${(props) =>
    props.margin &&
    css`
      margin-top: 24px;
    `}
`;

const FilterHeadline = styled.h1`
  color: #363636;
  font-size: 1.1428571428571428rem;
  font-weight: bold;
  margin-bottom: 16px;
`;

const AssetTagList = styled.ul`
  display: flex;
  flex-flow: row;
`;

interface AssetTagItemProps {
  inactive?: boolean;
}

const AssetTagItem = styled.li<AssetTagItemProps>`
  button {
    user-select: none;
    border: 0;
    outline: 0;
    cursor: pointer;
    overflow: hidden;
    font: inherit;

    border-radius: 4px;
    background-color: #2e6b90;
    color: #ffffff;
    padding: 4px 10px;
    margin-right: 4px;
    white-space: nowrap;

    &:hover,
    &:focus {
      background-color: #003e63;
    }

    ${(props) =>
      props.inactive &&
      css`
        background-color: #d4d4d4;

        &:hover,
        &:focus {
          background-color: #797979;
        }
      `}
  }
`;

// -----------------------------------------------------------------------------------------------------------
const SidebarFilter: React.FC = React.memo(() => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const mapFilter = useSelector(mapFilterSelector);

  // search
  const [search, setSearch] = useState(mapFilter.search ?? "");
  const setSearchFilter = useRef<any>(null);

  useEffect(() => {
    const set = (str: string) => {
      dispatch(setMapFilterSearch(str));
    };

    setSearchFilter.current = debounce(set, 350);
  }, [dispatch]);

  const onSearchChange = (e: any) => {
    const v = e.target.value;

    setSearch(v);
    setSearchFilter.current(v);
  };

  // tags
  const tags = useSelector(assetTagsSelector);
  const toggleMapFilterTag = (e: any) => {
    const tag = e.currentTarget.dataset.tag;

    dispatch(toggleMapFilterTagAction(tag));
  };

  // render
  return (
    <FilterContainer>
      <FilterHeadline>{t("Filter")}</FilterHeadline>

      <FilterContainerHeadline>{t("Suche")}</FilterContainerHeadline>
      <TextField
        name="search"
        type="text"
        value={search}
        onChange={onSearchChange}
        variant="outlined"
        placeholder={t("Kennzeichen / Name Suche")}
        autoFocus
      />

      <FilterContainerHeadline margin>{t("Tags")}</FilterContainerHeadline>
      <AssetTagList>
        {Object.keys(tags)
          .sort()
          .map((tag: any) => (
            <AssetTagItem key={tag} inactive={mapFilter.inactiveTags[tag]}>
              <button type="button" data-tag={tag} onClick={toggleMapFilterTag}>
                {tag}
              </button>
            </AssetTagItem>
          ))}
      </AssetTagList>
    </FilterContainer>
  );
});

export default SidebarFilter;
