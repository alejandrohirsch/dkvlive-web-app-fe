import React, {useState, useCallback, useEffect} from "react";
import {WindowContentProps, WindowContentContainer} from "app/components/Window";
import DataTable, {
  useColumns,
  TablePageContent,
  TableStickyHeader,
  ToolbarButton,
  ToolbarButtonIcon,
  TableContainer,
  bodyField,
} from "app/components/DataTable";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {useDispatch, useSelector} from "react-redux";
import {showOverlay} from "../state/overlay/actions";
import {OVERLAY_GEOFENCE, OVERLAY_GEOFENCE_EDIT} from "../state/overlay/reducer";
import {geofencesListSelector, geofencesListStateSelector} from "../state/geofences/selectors";
import {loadGeofencesIfNeeded} from "../state/geofences/actions";
import {Loading} from "dkv-live-frontend-ui";
import {Geofence} from "../state/geofences/reducer";
import ComingSoon from "app/components/ComingSoon";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const Container = styled(WindowContentContainer)`
  background-color: #ededed;
  padding: 0;
  width: 90vw;
  height: 88vh;
`;

const PageContent = styled(TablePageContent)`
  padding: 0 24px 24px 24px;
`;

/*
const GeoButtonIcon = styled.div`
  width: 36px;
  height: 36px;
  border-radius: 50%;
  background-color: #efefef;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 6px;
  transition: color linear 0.1s, background-color linear 0.1s;

  span {
    color: #666666;
    font-size: 16px;
    transition: color linear 0.1s;
  }
`;
 */

// -----------------------------------------------------------------------------------------------------------
const listColumns = [
  {field: "id", header: "", width: 0, hidden: true, sortable: false, filterable: false},
  {field: "name", header: "Geozone", width: 250, sortable: true, filterable: false},
  {field: "description", header: "Beschreibung", width: 250, sortable: true, filterable: false},
  {
    field: "type",
    header: "Meldung",
    width: 150,
    sortable: true,
    filterable: false,
    body: bodyField(() => "Alarm"),
  },
  {
    field: "vehicle",
    header: "Fahrzeuge",
    width: 150,
    sortable: true,
    filterable: false,
    body: bodyField(() => "Alle"),
  },
  {
    field: "public",
    header: "Öffentlich",
    width: 150,
    sortable: true,
    filterable: false,
    body: bodyField(() => "Ja"),
  },
  {
    field: "status",
    header: "Status",
    width: 100,
    sortable: true,
    filterable: false,
    body: bodyField(() => "Aktiv"),
  },
  {field: "_actions", header: "", width: 60, sortable: false, filterable: false},
];

// -----------------------------------------------------------------------------------------------------------
const GeofencesList: React.FC<WindowContentProps> = ({closeWindow}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // load geofences
  const geofencesListState = useSelector(geofencesListStateSelector);
  const geofences = useSelector(geofencesListSelector);

  useEffect(() => {
    dispatch(loadGeofencesIfNeeded());
  }, [dispatch]);

  // columns
  const [columns] = useState(listColumns);
  const visibleColumns = useColumns(columns);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);

  // actions
  const showNew = useCallback(() => {
    dispatch(showOverlay(OVERLAY_GEOFENCE, ""));
    closeWindow();
  }, [dispatch, closeWindow]);

  const showEdit = useCallback(
    (e: any) => {
      if (!hasPermission("management:EditGeofences")) {
        return;
      }

      const geofence = e.data as Geofence;
      if (geofence) {
        dispatch(showOverlay(OVERLAY_GEOFENCE_EDIT, "", geofence));
        closeWindow();
      }
    },
    [dispatch, closeWindow]
  );

  // render
  return (
    <Container>
      <ComingSoon onClick={closeWindow} /> {/* @todo commingsoon */}
      <PageContent>
        <Loading inprogress={geofencesListState.loading} />

        <TableStickyHeader>
          <h1>
            {geofences.length}{" "}
            {t(geofences.length === 1 ? "Geozone wurden angelegt" : "Geozonen wurden angelegt")}
          </h1>

          {/* @todo commingsoon */}
          {/* <SearchTextField
            name="search"
            type="text"
            required
            label={t("Suche")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          /> */}

          {hasPermission("management:CreateGeofences") && (
            <ToolbarButton onClick={showNew}>
              <ToolbarButtonIcon>
                <span>+</span>
              </ToolbarButtonIcon>
              {t("Neue Zone")}
            </ToolbarButton>
          )}
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={geofences}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            onRowDoubleClick={showEdit}
          />
        </TableContainer>

        {/* <TableStickyFooter>
          <TableFooterRightActions>
            <Button autoWidth secondary ariaLabel={t("Gesamte Liste exportieren")}>
              {t("Gesamte Liste exportieren")}
            </Button>
          </TableFooterRightActions>
        </TableStickyFooter> */}
      </PageContent>
    </Container>
  );
};

export default GeofencesList;
