import React from "react";
import styled from "styled-components";
import {Asset} from "../../../state/assets/reducer";
import AssetStatus from "./AssetStatus";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  margin-top: 4px;
  display: flex;
  flex-direction: row;
`;

const AssetItemDetailsList = styled.ul`
  font-size: 1.1428571428571428em;
  line-height: 1.45;
  color: #666666;
  margin-left: 8px;
  width: 100%;
`;

const GeoAddressLabel = styled.span`
  white-space: nowrap;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  display: inline-block;
  margin-top: 3px;
`;

// const DriverName = styled.span`
//   margin-left: 8px;
// `;

// -----------------------------------------------------------------------------------------------------------
interface AssetItemDetailsProps {
  asset: Asset;

  className?: string;
}

const AssetItemDetails: React.FC<AssetItemDetailsProps> = ({asset, className}) => {
  return (
    <Container className={className}>
      <AssetStatus asset={asset} />
      <AssetItemDetailsList>
        <li>
          <span>{asset.calc.speed} km/h</span>
          {/* <DriverName>{asset.driver1?.display_name}</DriverName> @todo: PR activate again */}
        </li>
        <li>
          <GeoAddressLabel title={asset.gnss.address?.label ?? ""}>
            {asset.gnss.address?.label}
          </GeoAddressLabel>
        </li>
      </AssetItemDetailsList>
    </Container>
  );
};

export default AssetItemDetails;
