import React, {useCallback, useState, useEffect, useRef} from "react";
import ReactDOM from "react-dom";
import {HereMap} from "../Map";
import styled, {css} from "styled-components/macro";
import {Satellite as SatelliteIcon} from "@styled-icons/material/Satellite";
import config from "config";
import {useTranslation} from "react-i18next";
import httpClient from "services/http";
import WebcamInfoBubble from "./WebcamInfoBubble";
import Window from "app/components/Window";
import GeofencesList from "./GeofencesList";
import {IconButton} from "@material-ui/core";
import Tooltip from "@material-ui/core/Tooltip";
import ComingSoon from "app/components/ComingSoon";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  position: absolute;
  top: 16px;
  right: 16px;
  z-index: 2;
`;

interface ListProps {
  open?: boolean;
  openWidth?: number;
}

const List = styled.div<ListProps>`
  display: flex;
  flex-flow: row-reverse nowrap;
  height: 64px;
  width: 64px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: var(--dkv-background-primary-color);
  transition: width linear 0.1s;
  overflow: hidden;
  align-items: center;

  ${(props) =>
    props.open &&
    css`
      width: ${props.openWidth}px;
    `}
`;

const ToggleButtonContainer = styled.div`
  width: 64px;
  height: 64px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-shrink: 0;
`;

const ToggleButton = styled(IconButton)`
  span {
    font-size: 24px;
    color: #666666;
  }
`;

const ToolbarButtonIcon = styled.div`
  width: 48px;
  height: 48px;
  border-radius: 50%;
  background-color: #efefef;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: color linear 0.1s, background-color linear 0.1s;

  span,
  svg {
    color: #666666;
    font-size: 24px;
    transition: color linear 0.1s;
  }
`;

interface ToolbarButtonProps {
  active?: boolean;
}

const ToolbarButton = styled.button<ToolbarButtonProps>`
  user-select: none;
  border: 0;
  outline: 0;
  cursor: pointer;
  overflow: visible;
  font: inherit;
  background-color: transparent;
  display: flex;
  flex-direction: column;
  align-items: center;
  font-size: 12px;
  color: #666666;
  margin-left: 8px;
  transition: color linear 0.1s;
  flex-shrink: 0;
  width: 50px;
  white-space: nowrap;

  ${(props) =>
    props.active &&
    css`
      color: #004b78;
      ${ToolbarButtonIcon} {
        background-color: #004b78;

        span,
        svg {
          color: #fff;
        }
      }
    `}

  &:hover {
    color: #2e6b90;

    ${ToolbarButtonIcon} {
      background-color: #2e6b90;

      span,
      svg {
        color: #fff;
      }
    }
  }
`;

// -----------------------------------------------------------------------------------------------------------
let webcamIcon: any;
function createWebcamIcon() {
  webcamIcon = new H.map.Icon(
    `<svg viewBox="0 0 24 24" height="24" width="24" role="img" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M12 2c-4.963 0-9 4.038-9 9 0 3.328 1.82 6.232 4.513 7.79l-2.067 1.378A1 1 0 006 22h12a1 1 0 00.555-1.832l-2.067-1.378C19.18 17.232 21 14.328 21 11c0-4.962-4.037-9-9-9zm0 16c-3.859 0-7-3.141-7-7 0-3.86 3.141-7 7-7s7 3.14 7 7c0 3.859-3.141 7-7 7z"></path><path d="M12 6c-2.757 0-5 2.243-5 5s2.243 5 5 5 5-2.243 5-5-2.243-5-5-5zm0 8c-1.654 0-3-1.346-3-3s1.346-3 3-3 3 1.346 3 3-1.346 3-3 3z"></path></svg>`
  );
}

function createWebcamMarker(cam: any, minZoom: number) {
  if (!webcamIcon) {
    createWebcamIcon();
  }

  const marker = new H.map.Marker({lat: cam.lat, lng: cam.lng}, {icon: webcamIcon, min: minZoom});
  marker.setData({cam: cam});

  return marker;
}

const onWebcamMarkerClick = (ui: any, map: any) => (e: MouseEvent) => {
  e.preventDefault();
  e.stopPropagation();

  const targetMarker = e.target as any;
  const {cam, bounds} = targetMarker.getData();

  if (cam === undefined) {
    // cluster
    map.getViewModel().setLookAtData({
      bounds: bounds,
    });
    return;
  }

  if (targetMarker._bubble !== undefined) {
    ui.removeBubble(targetMarker._bubble);
    targetMarker._bubble = undefined;
  }

  const content = document.createElement("div");
  ReactDOM.render(
    <WebcamInfoBubble
      cam={cam}
      close={() => {
        ui.removeBubble(targetMarker._bubble);
        targetMarker._bubble = undefined;
      }}
    />,
    content
  );

  targetMarker._bubble = new H.ui.InfoBubble(targetMarker.getGeometry(), {
    content: content,
  });
  ui.addBubble(targetMarker._bubble);

  const screenPos = map.geoToScreen(targetMarker.getGeometry());
  screenPos.y -= 200;
  map.setCenter(map.screenToGeo(screenPos.x, screenPos.y));
};

const webcamsMarkerTheme = (map: any) => ({
  getClusterPresentation: function(cluster: any) {
    let count = 0;
    cluster.forEachDataPoint(() => count++);

    const clusterMarker = new H.map.DomMarker(cluster.getPosition(), {
      icon: new H.map.DomIcon(`
      <div class="dkv-map-icon-webcamcluster">
        <svg viewBox="0 0 24 24" height="24" width="24" role="img" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M12 2c-4.963 0-9 4.038-9 9 0 3.328 1.82 6.232 4.513 7.79l-2.067 1.378A1 1 0 006 22h12a1 1 0 00.555-1.832l-2.067-1.378C19.18 17.232 21 14.328 21 11c0-4.962-4.037-9-9-9zm0 16c-3.859 0-7-3.141-7-7 0-3.86 3.141-7 7-7s7 3.14 7 7c0 3.859-3.141 7-7 7z"></path><path d="M12 6c-2.757 0-5 2.243-5 5s2.243 5 5 5 5-2.243 5-5-2.243-5-5-5zm0 8c-1.654 0-3-1.346-3-3s1.346-3 3-3 3 1.346 3 3-1.346 3-3 3z"></path></svg>
        ${count}
      </div>
      `),
      min: cluster.getMinZoom(),
      max: cluster.getMaxZoom(),
    });

    clusterMarker.addEventListener("pointerenter", () => {
      map.getViewPort().element.style.cursor = "pointer";
    });

    clusterMarker.addEventListener("pointerleave", () => {
      map.getViewPort().element.style.cursor = "auto";
    });

    clusterMarker.setData({bounds: cluster.getBoundingBox()});

    return clusterMarker;
  },
  getNoisePresentation: function(noisePoint: any) {
    const data = noisePoint.getData();

    const marker = createWebcamMarker(data, noisePoint.getMinZoom());

    marker.addEventListener("pointerenter", () => {
      map.getViewPort().element.style.cursor = "pointer";
    });

    marker.addEventListener("pointerleave", () => {
      map.getViewPort().element.style.cursor = "auto";
    });

    return marker;
  },
});

// -----------------------------------------------------------------------------------------------------------
const loadWebcams = async (): Promise<any[] | undefined> => {
  const [{data}, err] = await httpClient.get(`/assets/map/webcams.json`, {baseURL: "/"});
  if (err !== null) {
    console.error(err);
    return;
  }

  const latlngCheck = {};
  const cams = [];
  for (const cam of data) {
    const tmpLat = `${cam.lat}${cam.lng}`;

    if (latlngCheck[tmpLat]) {
      continue;
    }

    latlngCheck[tmpLat] = true;
    cams.push(cam);
  }

  return cams;
};

// -----------------------------------------------------------------------------------------------------------
interface MapSettingsProps {
  hereMap: HereMap;
}

const MapSettings: React.FC<MapSettingsProps> = ({hereMap: {map, defaultLayers, ui}}) => {
  const {t} = useTranslation();

  // @todo commingsoon
  const [commingSoon, setCommingSoon] = useState(false);
  const hideCommingSoon = () => setCommingSoon(false);

  // list
  const [open, setOpen] = useState(false);
  const toggleOpen = useCallback(() => setOpen((o) => !o), [setOpen]);

  // settings
  const [settings, setSettings] = useState({
    traffic: false,
    truckAttributes: false,
    satellite: false,
    centerOnAsset: false,
    webcams: false,
    traceMode: false,
  });

  // - traffic
  const toggleTraffic = useCallback(() => {
    setSettings((s) => {
      const traffic = !s.traffic;

      if (traffic) {
        map.addLayer(defaultLayers.vector.normal.traffic);
      } else {
        map.removeLayer(defaultLayers.vector.normal.traffic);
      }

      return {...s, traffic: traffic};
    });
  }, [setSettings, map, defaultLayers.vector.normal.traffic]);

  // - truck attributes
  useEffect(() => {
    const provider = defaultLayers.vector.normal.truck.getProvider();
    const style = new H.map.Style(`${publicURL}/assets/map/truck.yaml?v=${config.VERSION}`);
    provider.setStyle(style);
  }, [defaultLayers.vector.normal.truck]);

  const toggleTruckAttributes = useCallback(() => {
    setSettings((s) => {
      const truckAttributes = !s.truckAttributes;

      if (truckAttributes) {
        if (s.satellite) {
          map.addLayer(truckOverlayLayerRef.current);
        } else {
          map.setBaseLayer(defaultLayers.vector.normal.truck);
        }
      } else {
        if (s.satellite) {
          map.removeLayer(truckOverlayLayerRef.current);
        } else {
          map.setBaseLayer(defaultLayers.vector.normal.map);
        }
      }

      return {...s, truckAttributes: truckAttributes};
    });
  }, [setSettings, map, defaultLayers.vector.normal.truck, defaultLayers.vector.normal.map]);

  // - satellite
  const truckOverlayLayerRef = useRef(null);
  useEffect(() => {
    const truckOverlayProvider = new H.map.provider.ImageTileProvider({
      min: 8,
      max: 20,
      getURL: function(col: number, row: number, level: number) {
        const server = 1 + ((row + col) % 4);
        return `https://${server}.base.maps.ls.hereapi.com/maptile/2.1/truckonlytile/newest/normal.day/${level}/${col}/${row}/512/png8?style=fleet&apiKey=${config.HERE_API_KEY}`;
      },
    });

    truckOverlayLayerRef.current = new H.map.layer.TileLayer(truckOverlayProvider);
  }, []);

  const toggleSatellite = useCallback(() => {
    setSettings((s) => {
      const satellite = !s.satellite;

      if (satellite) {
        map.setBaseLayer(defaultLayers.raster.satellite.xbase);
        map.addLayer(defaultLayers.raster.satellite.labels);

        if (s.truckAttributes) {
          map.addLayer(truckOverlayLayerRef.current);
        }
      } else {
        map.removeLayer(defaultLayers.raster.satellite.labels);

        if (s.truckAttributes) {
          map.removeLayer(truckOverlayLayerRef.current);
          map.setBaseLayer(defaultLayers.vector.normal.truck);
        } else {
          map.setBaseLayer(defaultLayers.vector.normal.map);
        }
      }

      return {...s, satellite: satellite};
    });
  }, [
    setSettings,
    map,
    defaultLayers.raster.satellite.labels,
    defaultLayers.raster.satellite.xbase,
    defaultLayers.vector.normal.truck,
    defaultLayers.vector.normal.map,
  ]);

  // - center
  const toggleCenter = useCallback(() => {
    setSettings((s) => {
      const centerOnAsset = !s.centerOnAsset;

      if (centerOnAsset) {
        map.__centerOnAsset = true;
      } else {
        map.__centerOnAsset = false;
      }

      return {...s, centerOnAsset: centerOnAsset};
    });
  }, [setSettings, map]);

  // - tracemode
  const toggleTraceMode = useCallback(() => {
    setCommingSoon(true); // @todo commingsoon

    // setSettings((s) => {
    //   const traceMode = !s.traceMode;

    //   if (traceMode) {
    //     window.dispatchEvent(new Event("dkv-map-tracmode-active"));
    //   } else {
    //     window.dispatchEvent(new Event("dkv-map-tracmode-inactive"));
    //   }

    //   return {...s, traceMode: traceMode};
    // });
  }, [setCommingSoon]);

  // - webcam
  const [webcams, setWebcams] = useState<any[]>();
  const webcamMarkerGroup = useRef<any>();

  const toggleWebcams = useCallback(() => {
    const showWebcams = async () => {
      let clusteringLayer = webcamMarkerGroup.current;

      let cams = webcams;
      if (!cams) {
        cams = await loadWebcams();

        const dataPoints = cams!.map(function(item) {
          return new H.clustering.DataPoint(item.lat, item.lng, null, item);
        });

        const clusteredDataProvider = new H.clustering.Provider(dataPoints, {
          clusteringOptions: {
            eps: 32,
            minWeight: 2,
          },
          theme: webcamsMarkerTheme(map),
        });

        if (!clusteringLayer) {
          clusteringLayer = new H.map.layer.ObjectLayer(clusteredDataProvider);
          webcamMarkerGroup.current = clusteringLayer;
        }

        clusteredDataProvider.addEventListener("tap", onWebcamMarkerClick(ui, map));

        setWebcams(cams);
      }

      setSettings((s) => {
        const webcams = !s.webcams;

        try {
          if (webcams) {
            map.addLayer(webcamMarkerGroup.current);
          } else {
            map.removeLayer(webcamMarkerGroup.current);
          }
        } catch (e) {
          console.error(e);
        }

        return {...s, webcams: webcams};
      });
    };

    showWebcams();
  }, [setSettings, map, webcams, ui]);

  // - geozones
  const [showGeozones, setShowGeozones] = useState(false);
  const onShowGeozones = () => setShowGeozones(true);
  const onHideGeozones = () => setShowGeozones(false);

  // render
  let itemCount = 7;

  if (!hasPermission("management:ListGeofences")) {
    itemCount = itemCount - 1;
  }

  return (
    <Container>
      <List open={open} openWidth={itemCount * 67.13}>
        {/* <Item>
          <ItemButton onClick={toggleOpen} title={t("Einstellungen") as string}>
            <span className="icon-ico_optional-menu" style={{fontSize: 24}} />
          </ItemButton>
        </Item> */}
        <ToggleButtonContainer>
          <ToggleButton onClick={toggleOpen} title={t("Einstellungen") as string}>
            <span className="icon-ico_optional-menu" />
          </ToggleButton>
        </ToggleButtonContainer>

        {hasPermission("management:ListGeofences") && (
          <Tooltip title={t("Geo Zonen") as string} enterDelay={0}>
            <ToolbarButton onClick={onShowGeozones}>
              <ToolbarButtonIcon>
                <span className="icon-ico_geozone" />
              </ToolbarButtonIcon>
            </ToolbarButton>
          </Tooltip>
        )}

        <Tooltip title={t("Spurmodus") as string} enterDelay={0}>
          <ToolbarButton onClick={toggleTraceMode} active={settings.traceMode}>
            <ToolbarButtonIcon>
              <span className="icon-ico_trail" />
            </ToolbarButtonIcon>
          </ToolbarButton>
        </Tooltip>

        <Tooltip title={t("Webcams") as string} enterDelay={0}>
          <ToolbarButton onClick={toggleWebcams} active={settings.webcams}>
            <ToolbarButtonIcon>
              <span className="icon-ico_webcam" />
            </ToolbarButtonIcon>
          </ToolbarButton>
        </Tooltip>

        <Tooltip title={t("Verkehr") as string} enterDelay={0}>
          <ToolbarButton onClick={toggleTraffic} active={settings.traffic}>
            <ToolbarButtonIcon>
              <span className="icon-ico_tunnel" />
            </ToolbarButtonIcon>
          </ToolbarButton>
        </Tooltip>

        <Tooltip title={t("LKW Attribute") as string} enterDelay={0}>
          <ToolbarButton onClick={toggleTruckAttributes} active={settings.truckAttributes}>
            <ToolbarButtonIcon>
              <span className="icon-ico_fleet" />
            </ToolbarButtonIcon>
          </ToolbarButton>
        </Tooltip>

        <Tooltip title={t("Satellitenbild") as string} enterDelay={0}>
          <ToolbarButton onClick={toggleSatellite} active={settings.satellite}>
            <ToolbarButtonIcon>
              <SatelliteIcon size="24px" />
            </ToolbarButtonIcon>
          </ToolbarButton>
        </Tooltip>

        <Tooltip title={t("Karte zentrieren") as string} enterDelay={0}>
          <ToolbarButton onClick={toggleCenter} active={settings.centerOnAsset}>
            <ToolbarButtonIcon>
              <span className="icon-ico_position" />
            </ToolbarButtonIcon>
          </ToolbarButton>
        </Tooltip>
      </List>
      {showGeozones && (
        <Window onClose={onHideGeozones} headline={t("Geo Zonen")} tableWindow>
          {(props) => <GeofencesList {...props} />}
        </Window>
      )}
      {commingSoon && <ComingSoon fixed onClick={hideCommingSoon} />} {/* @todo commingsoon */}
    </Container>
  );
};

export default MapSettings;
