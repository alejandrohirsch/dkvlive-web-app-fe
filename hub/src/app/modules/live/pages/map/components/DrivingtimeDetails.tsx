import React, {useState, useEffect} from "react";
import styled, {css} from "styled-components/macro";
import DataTable, {
  TablePageContent,
  TableContainer,
  TableStickyHeader,
  useColumns,
  TableStickyHeaderDetails,
} from "app/components/DataTable";
import {TextField, Loading} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import {Tooltip} from "@material-ui/core";
import DrivingtimeDayChart from "./DrivingtimeDayChart";
import {detailsSelector} from "../state/details/selectors";
import {useSelector} from "react-redux";
import httpClient from "services/http";
import dayjs from "dayjs";
import {formatMinutes} from "services/date";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import {InputAdornment} from "@material-ui/core";
import DayjsUtils from "@date-io/dayjs";
import {
  resolveUserDayFormat,
  resolveUserDateLocale,
  resolveUserDateFormat,
  resolveUserLanguage,
} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: #ededed;
  z-index: 5000;
`;

const DetailSpan = styled.span`
  font-size: 1rem;
  margin-top: 3px;
  display: inline-block;
`;

const Shifts = styled.div`
  & > div:last-of-type {
    margin-top: 10px;
  }

  & > div:first-of-type {
    margin-top: 0;
  }
`;

const DetailsChildrenContainer = styled.div`
  display: flex;
  flex-flow: row nowrap;
  width: 100%;
`;

const CurrentDayDetails = styled.div`
  min-width: 800px;
  margin-left: auto;

  h2 {
    font-size: 1.7142857142857142rem;
    color: var(--dkv-dkv_blue);
    margin-bottom: 12px;
    font-weight: 500;

    span {
      font-size: 1.1428571428571428rem;
    }
  }
`;

interface CurrentDayTimesProps {
  activity: string;
}

const CurrentDayTimes = styled.div<CurrentDayTimesProps>`
  color: #989898;
  margin-top: 12px;
  padding-left: 32px;
  position: relative;
  margin-bottom: 12px;

  span {
    color: #323232;
  }

  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 18px;
    height: 18px;
    background-color: var(--dkv-grey_30);
    border-radius: 50%;

    ${(props) =>
      props.activity === "Work" &&
      css`
        background-color: var(--dkv-dkv_blue);
      `}

      
    ${(props) =>
      props.activity === "Driving" &&
      css`
        background-color: #58be58;
      `}

      
    ${(props) =>
      props.activity === "Available" &&
      css`
        background-color: #77a7c5;
      `}
  }
`;

const CurrentDayTimesActivities = styled.div`
  font-size: 1.1428571428571428rem;
`;

const CurrentDayTimesBreaks = styled.div`
  margin-top: 4px;
  font-size: 1rem;
`;

const CurrentDayLegend = styled.ul`
  margin-top: 32px;
  margin-right: 20px;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`;

interface CurrentDayTimesProps {
  activity: string;
}

const CurrentDayLegendEntry = styled.li<CurrentDayTimesProps>`
  margin-left: 32px;
  position: relative;
  padding-left: 15px;
  font-size: 1.1428571428571428rem;

  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    bottom: 2px;
    margin: auto;
    width: 8px;
    height: 8px;
    background-color: var(--dkv-grey_30);
    border-radius: 50%;

    ${(props) =>
      props.activity === "Work" &&
      css`
        background-color: var(--dkv-dkv_blue);
      `}

      
    ${(props) =>
      props.activity === "Driving" &&
      css`
        background-color: #58be58;
      `}

      
    ${(props) =>
      props.activity === "Available" &&
      css`
        background-color: #77a7c5;
      `}
  }
`;

const TableWrapper = styled.div`
  height: 100%;
  overflow: hidden;
  background-color: #fff;
  display: flex;
  flex-direction: column;

  ${TableContainer} {
    border-top: 1px solid #efefef;
  }
`;

// -----------------------------------------------------------------------------------------------------------
const DoubleWeekChartContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  margin-right: 24px;
  grid-gap: 36px;
`;

interface WeekShiftsChartContainerProps {
  reversed?: boolean;
}

const WeekShiftsChartContainer = styled.div<WeekShiftsChartContainerProps>`
  height: 185px;
  width: 100%;
  display: flex;
  flex-flow: row nowrap;
  position: relative;
  padding-left: 20px;
  align-items: center;

  ${(props) =>
    props.reversed &&
    css`
      flex-direction: row-reverse;
      padding-left: 0;
      padding-right: 20px;
    `}
`;

const WeekShiftsChartYAxis = styled.div<WeekShiftsChartContainerProps>`
  position: relative;
  height: 155px;
  width: 12px;
  flex-shrink: 0;

  div {
    display: block;
    position: absolute;
    left: 0;
    font-size: 12px;
    line-height: 1.67;
    height: 1px;
    width: 8px;
    background-color: #c1c1c1;

    span {
      position: absolute;
      color: #c1c1c1;
      top: -9px;
      left: -18px;
      width: 12px;
      text-align: right;

      ${(props) =>
        props.reversed &&
        css`
          left: 12px;
        `}
    }
  }
`;

const WeekShiftsChartShifts = styled.div`
  display: flex;
  flex-flow: row nowrap;
  height: 100%;
  width: 100%;
`;

const WeekShiftsChartShift = styled.div`
  width: 28px;
  align-items: center;
  height: 185px;
  position: relative;
  display: flex;
  flex-flow: row nowrap;
`;

const WeekShiftsChartShiftDetails = styled.div`
  position: relative;
  height: 155px;
  display: flex;
  justify-content: center;
  width: 28px;
`;

const WeekShiftsChartShiftIndicator = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  margin: auto;
  width: 1px;
  height: calc(100% - 15px);
  background-color: #dbdbdb;

  div {
    position: absolute;
    bottom: -8px;
    width: 6px;
    height: 6px;
    border-radius: 50%;
    background-color: #dbdbdb;
    transform: translateX(-50%);
    left: 1px;
  }
`;

interface WeekShiftsChartShiftDetailEntryProps {
  activity: string;
}

const WeekShiftsChartShiftDetailEntry = styled.div<WeekShiftsChartShiftDetailEntryProps>`
  position: absolute;
  height: 8px;
  width: 8px;
  transform: translateY(-4px) ;
  background-color: #dbdbdb;

  ${(props) =>
    props.activity === "Work" &&
    css`
      background-color: var(--dkv-dkv_blue);
    `}

    
  ${(props) =>
    props.activity === "Driving" &&
    css`
      background-color: #58be58;
    `}

    
  ${(props) =>
    props.activity === "Available" &&
    css`
      background-color: #77a7c5;
    `}

`;

interface WeekShiftsChartShiftDetailProps {
  minutes: number;
  activity: string;
  title: string;
}

const WeekShiftsChartShiftDetail: React.FC<WeekShiftsChartShiftDetailProps> = ({minutes, activity}) => {
  if (!minutes) {
    return null;
  }

  const top = 100 - (minutes / 900) * 100;
  const formattedMinutes = formatMinutes(minutes);

  return (
    <Tooltip
      title={`${formattedMinutes}h`}
      aria-label={`${formattedMinutes}h`}
      classes={{popper: "dkv-dtco-tooltip"}}
      enterDelay={20}
    >
      <WeekShiftsChartShiftDetailEntry activity={activity} style={{top: `${top}%`}} />
    </Tooltip>
  );
};

interface WeekShiftsChartProps {
  shifts: any[];
  reversed?: boolean;
}

const WeekShiftsChart: React.FC<WeekShiftsChartProps> = React.memo(({shifts, reversed}) => {
  const {t} = useTranslation();

  const yAxis = [1, 3, 5, 7, 9, 11, 13, 15];

  // render
  return (
    <WeekShiftsChartContainer reversed={reversed}>
      <WeekShiftsChartYAxis reversed={reversed}>
        {yAxis.map((h) => (
          <div key={h} style={{top: `${100 - (h / 15) * 100}%`}}>
            <span>{h}</span>
          </div>
        ))}
      </WeekShiftsChartYAxis>

      <WeekShiftsChartShifts>
        {(shifts ?? []).map((s: any) => (
          <WeekShiftsChartShift key={s.start}>
            <WeekShiftsChartShiftIndicator>
              <Tooltip
                title={`${dayjs(s.start, {utc: true}).format(resolveUserDateFormat(false))} - ${dayjs(s.end, {
                  utc: true,
                }).format(resolveUserDateFormat(false))}`}
                aria-label={`${dayjs(s.start, {utc: true}).format(resolveUserDateFormat(false))} - ${dayjs(
                  s.end,
                  {
                    utc: true,
                  }
                ).format(resolveUserDateFormat(false))}`}
                enterDelay={20}
                classes={{popper: "dkv-dtco-tooltip"}}
              >
                <div />
              </Tooltip>
            </WeekShiftsChartShiftIndicator>

            <WeekShiftsChartShiftDetails>
              <WeekShiftsChartShiftDetail
                minutes={s.drive}
                activity="Driving"
                title={t("Lenken") as string}
              />
              <WeekShiftsChartShiftDetail minutes={s.work} activity="Work" title={t("Arbeit") as string} />
              <WeekShiftsChartShiftDetail
                minutes={s.avail}
                activity="Available"
                title={t("Bereitschaft") as string}
              />
              <WeekShiftsChartShiftDetail
                minutes={s.duration}
                activity="Break"
                title={t("Schicht") as string}
              />
            </WeekShiftsChartShiftDetails>
          </WeekShiftsChartShift>
        ))}
      </WeekShiftsChartShifts>
    </WeekShiftsChartContainer>
  );
});

// -----------------------------------------------------------------------------------------------------------
const DetailsValues = styled.ul`
  flex-shrink: 0;
  margin-left: 24px;
`;

interface DetailsValuesEntryProps {
  activity: string;
}

const DetailsValuesEntry = styled.li<DetailsValuesEntryProps>`
  position: relative;
  font-size: 1rem;
  color: #989898;
  padding: 2px 0 2px 16px;
  margin-top: 5px;

  h3 {
    font-size: 1rem;
    font-weight: bold;
  }

  div {
    margin-top: 2px;
  }

  span {
    display: inline-flex;
    align-items: center;
    margin-right: 12px;

    i {
      font-size: 1.25rem;
      margin-bottom: 1px;
    }

    i:last-of-type {
      margin-right: 2px;
    }
  }

  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 6px;
    background-color: #dbdbdb;
  }

  ${(props) =>
    props.activity === "Work" &&
    css`
      color: var(--dkv-dkv_blue);

      &:before {
        background-color: var(--dkv-dkv_blue);
      }
    `}

  ${(props) =>
    props.activity === "Driving" &&
    css`
      color: #58be58;

      &:before {
        background-color: #58be58;
      }
    `}

  ${(props) =>
    props.activity === "Available" &&
    css`
      color: #77a7c5;

      &:before {
        background-color: #77a7c5;
      }
    `};
`;

const DetailsValuesHeadline = styled.p`
  margin: 16px 0;
  color: #666;
  font-weight: 500;
  text-align: center;
`;

const ShiftsHeadline = styled.p`
  margin: 16px 0 8px 0;
  color: #666;
  font-weight: 500;
  text-align: center;
`;

const DatePickerFilter = styled.div`
  margin: 2px 0px;

  .MuiTextField-root {
    width: 200px;
    transform: scale(0.85);
  }
`;

// -----------------------------------------------------------------------------------------------------------
const dateColumn = (rowData: any) => {
  const d = dayjs(rowData.day, {utc: true});

  return (
    <>
      <span>{d.format(resolveUserDayFormat())}</span>
      <br />
      <DetailSpan>{d.format("dddd")}</DetailSpan>
    </>
  );
};

const shiftsColumn = (t: any) => (rowData: any) => {
  return (
    <Shifts>
      {(rowData.shifts ?? []).map((shift: any) => (
        <div key={`${rowData.start}-${shift.end}`}>
          <span>
            {shift.duration > 13 * 60 ? "15" : "13"} {t("Stunden")}
          </span>
          <br />
          <DetailSpan>
            {dayjs(shift.start, {utc: true}).format("HH:mm")} {t("bis")}{" "}
            {dayjs(shift.end, {utc: true}).format("HH:mm")} (UTC)
          </DetailSpan>
        </div>
      ))}
    </Shifts>
  );
};

const timesColumn = (t: any) => (rowData: any) => {
  return (
    <Shifts>
      {(rowData.shifts ?? []).map((shift: any) => (
        <div key={`${rowData.start}-${shift.end}`}>
          <span>
            {t("Lenken")}: {formatMinutes(shift.drive)}, {t("Arbeit")}: {formatMinutes(shift.work)},{" "}
            {t("Bereitschaft")}: {formatMinutes(shift.avail)}
          </span>
          <br />
          <DetailSpan>
            {t("Ruhezeit vor Schichtbeginn")}: {formatMinutes(shift.rest)}, {t("Pausenzeit")}:{" "}
            {formatMinutes(shift.break)}
          </DetailSpan>
        </div>
      ))}
    </Shifts>
  );
};

const dayChartColumn = (rowData: any) => {
  return <DrivingtimeDayChart rowData={rowData} />;
};

const listColumns = [
  {field: "date", header: "Datum", width: 150, className: "dkv-dt-column-alignedtop", body: dateColumn},
  {field: "shifts", header: "Schichten", width: 250, bodyWithTranslation: shiftsColumn},
  {field: "times", header: "Zeiten", width: 380, bodyWithTranslation: timesColumn},
  {field: "day", header: "Tagesübersicht", width: 750, body: dayChartColumn},
];

// -----------------------------------------------------------------------------------------------------------
const formatCurrentDay = (ts: string) => {
  const date = dayjs(ts).locale(resolveUserLanguage());

  return {day: date.format(`dddd ${resolveUserDayFormat()}`), clock: date.format("HH:mm")};
};

// -----------------------------------------------------------------------------------------------------------
const DrivingtimeDetails: React.FC = () => {
  const {t} = useTranslation();

  // visible
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    const onEvent = () => {
      setVisible(true);
    };

    window.addEventListener("dkv-show-drivingtime", onEvent);

    return () => {
      window.removeEventListener("dkv-show-drivingtime", onEvent);
    };
  }, []);

  const hide = (e: any) => {
    e.preventDefault();
    e.stopPropagation();
    setVisible(false);
  };

  // date filter
  const [selectedFromDate, setSelectedFromDate] = React.useState(
    dayjs()
      .subtract(7, "day")
      .toDate()
  );
  const handleFromDateChange = (date: any) => {
    setSelectedFromDate(date);
  };

  const [selectedToDate, setSelectedToDate] = React.useState(new Date());
  const handleToDateChange = (date: any) => {
    setSelectedToDate(date);
  };

  // data
  const [loading, setLoading] = useState(false);
  const {assetID} = useSelector(detailsSelector);

  const [drivingTimeData, setDrivingTimeData] = useState<any>(null);
  useEffect(() => {
    if (!visible) {
      return;
    }

    const load = async () => {
      setLoading(true);

      const from = dayjs(selectedFromDate).format("YYYY-MM-DD");
      const to = dayjs(selectedToDate).format("YYYY-MM-DD");

      const [{data}, err] = await httpClient.get(
        `/dtco/activities/0D_DF000037966010/${from}/${to}` // @todo url richtig stellen
      );
      if (err !== null) {
        setDrivingTimeData(null);
        setLoading(false);
        return;
      }

      data._currentDay =
        data.drive_summaries.day && data.drive_summaries.day.start
          ? formatCurrentDay(data.drive_summaries.day.start)
          : null;

      setDrivingTimeData(data);
      setLoading(false);
    };

    load();
  }, [assetID, selectedToDate, selectedFromDate, visible]);

  // columns
  const [columns] = useState(listColumns);
  const visibleColumns = useColumns(columns, t, true);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  // const unselect = () => setSelected([]);

  // render
  if (!visible) {
    return null;
  }

  let lastActivity = "Break";
  if (drivingTimeData && drivingTimeData.last_day && drivingTimeData.last_day.activities) {
    lastActivity = drivingTimeData.last_day.activities[drivingTimeData.last_day.activities.length - 1].type;
  }

  const dayFormat = resolveUserDayFormat();

  return (
    <Container>
      <TablePageContent>
        <Loading inprogress={loading} />

        <TableStickyHeader onClick={hide} details>
          <h1>{t("Lenk- und Ruhezeiten")}</h1>
        </TableStickyHeader>
        <TableStickyHeaderDetails ChildrenContainer={DetailsChildrenContainer}>
          <div>
            <ShiftsHeadline>{t("Schichten")}</ShiftsHeadline>

            <DoubleWeekChartContainer>
              <WeekShiftsChart shifts={drivingTimeData ? drivingTimeData.shifts_dbl_week : []} />
            </DoubleWeekChartContainer>
          </div>

          {drivingTimeData && (
            <DetailsValues>
              <DetailsValuesHeadline>{t("Zeiten in Woche und Doppelwoche")}:</DetailsValuesHeadline>

              <DetailsValuesEntry activity="Break">
                <h3>{t("Ruhezeiten (inkl. Pausenzeit)")}:</h3>
                <div>
                  <span>
                    <Tooltip
                      title={t("Doppelwoche") as string}
                      aria-label={t("Doppelwoche")}
                      classes={{popper: "dkv-dtco-tooltip"}}
                      enterDelay={20}
                    >
                      <span style={{marginRight: 2}}>
                        <i className="icon-ico_cal-week" />+<i className="icon-ico_cal-week" />
                      </span>
                    </Tooltip>
                    {formatMinutes(drivingTimeData.drive_summaries.dbl_week.break)}h
                  </span>
                  <span>
                    <Tooltip
                      title={t("Woche") as string}
                      aria-label={t("Woche")}
                      classes={{popper: "dkv-dtco-tooltip"}}
                      enterDelay={20}
                    >
                      <span style={{marginRight: 2}}>
                        <i className="icon-ico_cal-week" />
                      </span>
                    </Tooltip>
                    {formatMinutes(drivingTimeData.drive_summaries.week.break)}h
                  </span>
                </div>
              </DetailsValuesEntry>
              <DetailsValuesEntry activity="Driving">
                <h3>{t("Lenkzeiten")}:</h3>
                <div>
                  <span>
                    <Tooltip
                      title={t("Doppelwoche") as string}
                      aria-label={t("Doppelwoche")}
                      classes={{popper: "dkv-dtco-tooltip"}}
                      enterDelay={20}
                    >
                      <span style={{marginRight: 2}}>
                        <i className="icon-ico_cal-week" />+<i className="icon-ico_cal-week" />{" "}
                      </span>
                    </Tooltip>
                    {formatMinutes(drivingTimeData.drive_summaries.dbl_week.drive)}h
                  </span>
                  <span>
                    <Tooltip
                      title={t("Woche") as string}
                      aria-label={t("Woche")}
                      classes={{popper: "dkv-dtco-tooltip"}}
                      enterDelay={20}
                    >
                      <span style={{marginRight: 2}}>
                        <i className="icon-ico_cal-week" />
                      </span>
                    </Tooltip>
                    {formatMinutes(drivingTimeData.drive_summaries.week.drive)}h
                  </span>
                </div>
              </DetailsValuesEntry>
              <DetailsValuesEntry activity="Work">
                <h3>{t("Arbeitszeiten")}:</h3>
                <div>
                  <span>
                    <Tooltip
                      title={t("Doppelwoche") as string}
                      aria-label={t("Doppelwoche")}
                      classes={{popper: "dkv-dtco-tooltip"}}
                      enterDelay={20}
                    >
                      <span style={{marginRight: 2}}>
                        <i className="icon-ico_cal-week" />+<i className="icon-ico_cal-week" />{" "}
                      </span>
                    </Tooltip>
                    {formatMinutes(drivingTimeData.drive_summaries.dbl_week.work)}h
                  </span>
                  <span>
                    <Tooltip
                      title={t("Woche") as string}
                      aria-label={t("Woche")}
                      classes={{popper: "dkv-dtco-tooltip"}}
                      enterDelay={20}
                    >
                      <span style={{marginRight: 2}}>
                        <i className="icon-ico_cal-week" />
                      </span>
                    </Tooltip>
                    {formatMinutes(drivingTimeData.drive_summaries.week.work)}h
                  </span>
                </div>
              </DetailsValuesEntry>
              <DetailsValuesEntry activity="Available">
                <h3>{t("Bereitschaften")}:</h3>
                <div>
                  <span>
                    <Tooltip
                      title={t("Doppelwoche") as string}
                      aria-label={t("Doppelwoche")}
                      classes={{popper: "dkv-dtco-tooltip"}}
                      enterDelay={20}
                    >
                      <span style={{marginRight: 2}}>
                        <i className="icon-ico_cal-week" />+<i className="icon-ico_cal-week" />{" "}
                      </span>
                    </Tooltip>
                    {formatMinutes(drivingTimeData.drive_summaries.dbl_week.avail)}h
                  </span>
                  <span>
                    <Tooltip
                      title={t("Woche") as string}
                      aria-label={t("Woche")}
                      classes={{popper: "dkv-dtco-tooltip"}}
                      enterDelay={20}
                    >
                      <span style={{marginRight: 2}}>
                        <i className="icon-ico_cal-week" />
                      </span>
                    </Tooltip>
                    {formatMinutes(drivingTimeData.drive_summaries.week.avail)}h
                  </span>
                </div>
              </DetailsValuesEntry>
            </DetailsValues>
          )}

          {drivingTimeData && drivingTimeData.drive_summaries.day && drivingTimeData._currentDay && (
            <CurrentDayDetails>
              <h2>
                {drivingTimeData._currentDay.day} <span>{drivingTimeData._currentDay.clock} (UTC)</span>
              </h2>
              <CurrentDayTimes activity={lastActivity}>
                <CurrentDayTimesActivities>
                  {t("Lenken")}: <span>{formatMinutes(drivingTimeData.drive_summaries.day.drive)}</span>,{" "}
                  {t("Arbeit")}: <span>{formatMinutes(drivingTimeData.drive_summaries.day.work)}</span>,{" "}
                  {t("Bereitschaft")}: <span>{formatMinutes(drivingTimeData.drive_summaries.day.avail)}</span>
                </CurrentDayTimesActivities>
                <CurrentDayTimesBreaks>
                  {t("Ruhezeit vor Schichtbeginn")}:{" "}
                  <span>{formatMinutes(drivingTimeData.last_shift.rest)}</span>, {t("Pausenzeit")}:{" "}
                  <span>{formatMinutes(drivingTimeData.drive_summaries.day.break)}</span>
                </CurrentDayTimesBreaks>
              </CurrentDayTimes>

              <DrivingtimeDayChart rowData={drivingTimeData} today />

              <CurrentDayLegend>
                <CurrentDayLegendEntry activity="Driving">{t("Lenkzeiten")}</CurrentDayLegendEntry>
                <CurrentDayLegendEntry activity="Work">{t("Arbeitszeiten")}</CurrentDayLegendEntry>
                <CurrentDayLegendEntry activity="Available">{t("Bereitschaften")}</CurrentDayLegendEntry>
                <CurrentDayLegendEntry activity="Break">{t("Ruhe- und Pausenzeiten")}</CurrentDayLegendEntry>
              </CurrentDayLegend>
            </CurrentDayDetails>
          )}
        </TableStickyHeaderDetails>

        <TableWrapper>
          <DatePickerFilter>
            <MuiPickersUtilsProvider utils={DayjsUtils} locale={resolveUserDateLocale()}>
              <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format={dayFormat}
                margin="none"
                value={selectedFromDate}
                onChange={handleFromDateChange}
                TextFieldComponent={TextField as any}
                InputProps={{
                  startAdornment: <InputAdornment position="start">{t("Von")}</InputAdornment>,
                }}
                invalidDateMessage={t("Ungültiges Datum")}
                keyboardIcon={<i className="icon-ico_cal-week" />}
                PopoverProps={{
                  className: "dkv-datetimepicker",
                }}
              />
              <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format={dayFormat}
                margin="none"
                value={selectedToDate}
                onChange={handleToDateChange}
                TextFieldComponent={TextField as any}
                InputProps={{
                  startAdornment: <InputAdornment position="start">{t("Bis")}</InputAdornment>,
                }}
                invalidDateMessage={t("Ungültiges Datum")}
                keyboardIcon={<i className="icon-ico_cal-week" />}
                PopoverProps={{
                  className: "dkv-datetimepicker",
                }}
              />
            </MuiPickersUtilsProvider>
          </DatePickerFilter>

          <TableContainer>
            <DataTable
              items={drivingTimeData ? drivingTimeData.days ?? [] : []}
              columns={visibleColumns}
              selected={selected}
              onSelectionChange={onSelectionChange}
              noSelection
            />
          </TableContainer>
        </TableWrapper>

        <div style={{height: 10}} />
      </TablePageContent>
    </Container>
  );
};

export default DrivingtimeDetails;
