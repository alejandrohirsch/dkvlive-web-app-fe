import React, {useCallback, useState, useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import {MapOverlayContainer, MapOverlayCloseButton} from "./MapOverlay";
import {detailsSelector} from "../state/details/selectors";
import {hideDetailsOverlay} from "../state/details/actions";
import {assetSelector} from "app/modules/live/state/assets/selectors";
import {useTranslation} from "react-i18next";
import AssetDataCharts from "./details/AssetDataCharts";
import styled from "styled-components";
import {AreaDataTable, TableLabel, TableValue} from "./details/Overview";
import {overlaySelector} from "../state/overlay/selectors";
import {AssetLiveDetailsLink} from "./AssetList";
import Chip from "@material-ui/core/Chip";
import httpClient from "services/http";
import {formatMinutes} from "services/date";
import dayjs from "dayjs";
import ComingSoon from "app/components/ComingSoon";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const DataContainer = styled.div`
  padding: 0 24px 12px 24px;
  font-size: 1.1428571428571428rem;
  overflow: auto;
  overflow-x: hidden;
`;

const DataTable = styled(AreaDataTable)`
  margin: 0 0 16px 0;
`;

const DetailsLink = styled(AssetLiveDetailsLink)`
  position: relative;
  font-size: 1.1428571428571428rem;
  margin-top: 8px;
`;

const SubHeadline = styled.p`
  margin-top: 10px;
  margin-bottom: 8px;
  font-weight: bold;
`;

const OverlayHeadline = styled.p`
  margin-top: 24px;
  margin-bottom: 8px;
  font-weight: bold;
  margin-left: 24px;
  font-size: 1.1428571428571428rem;
  color: #004b78;

  span {
    color: #666;
    font-weight: normal;
  }
`;

const AlarmsChip = styled<any>(Chip)`
  font-family: inherit !important;
  font-size: 1rem !important;
  margin-bottom: 8px;
  height: 29px !important;
  margin-right: 8px;

  .MuiChip-icon {
    font-size: 20px !important;
  }

  .MuiChip-label {
    font-family: inherit !important;
  }

  &.alarms {
    background-color: var(--dkv-highlight-error-color);
    color: #fff;

    .MuiChip-icon {
      color: #fff !important;
    }
  }
`;

const Overlay = styled(MapOverlayContainer)`
  display: flex;
`;

const Content = styled.section`
  display: flex;
  flex-direction: column;
  overflow: hidden;
`;

const PhoneLink = styled.a`
  margin-left: 5px;
`;

const DriverName = styled.span`
  display: inline-block;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 164px;
`;

const DriverContainer = styled.div`
  display: flex;
  align-items: center;
`;

// -----------------------------------------------------------------------------------------------------------
const MapDataOverlay: React.FC = () => {
  const dispatch = useDispatch();
  const {t} = useTranslation();

  // overlay
  const overlay = useSelector(overlaySelector);

  // data
  const {assetID, dataOverlayVisible} = useSelector(detailsSelector);
  const asset = useSelector(assetSelector(assetID || ""));

  // close
  const closeOverlay = useCallback(() => {
    dispatch(hideDetailsOverlay());
  }, [dispatch]);

  // drivingtime
  const openDrivingTime = useCallback(() => {
    window.dispatchEvent(new Event("dkv-show-drivingtime"));
  }, []);

  const [drivingTimeData, setDrivingTimeData] = useState<any>(null);
  const driverID = asset?.driver1?.card.driver_id_full;
  useEffect(() => {
    const load = async () => {
      if (!driverID) {
        return;
      }

      // @todo comingsoon
      if (hasPermission("superadmin:root")) {
        const now = dayjs().format("YYYY-MM-DD");

        const [{data}, err] = await httpClient.get(
          `/dtco/activities/${driverID}/${now}/${now}?disable=nolist,noshifts`
        );
        if (err !== null) {
          setDrivingTimeData(null);
          return;
        }

        setDrivingTimeData(data);
      } else {
        setDrivingTimeData(null);
      }
    };

    load();
  }, [assetID, driverID]);

  // render
  if (asset === undefined || !dataOverlayVisible || overlay.visible) {
    return null;
  }

  return (
    <Overlay style={{width: 300}}>
      <ComingSoon small onClick={closeOverlay} /> {/* @todo commingsoon */}
      <MapOverlayCloseButton onClick={closeOverlay} title={"Ausblenden"}>
        <span className="icon-ico_clear" style={{fontSize: 24, color: "#363636"}} />
      </MapOverlayCloseButton>
      <Content>
        <OverlayHeadline>
          <span>{t("Infos zu")}</span>
          <br />
          {asset._displayName}
        </OverlayHeadline>

        <DataContainer>
          <AssetDataCharts asset={asset} />

          {asset.driver1?.display_name && (
            <>
              <SubHeadline>{t("Fahrer")}</SubHeadline>
              <DataTable>
                <tbody>
                  <tr>
                    <TableLabel>{t("Fahrer")}:</TableLabel>
                    <TableValue noWrap>
                      <DriverContainer>
                        <DriverName title={asset.driver1?.display_name}>
                          {asset.driver1?.display_name}
                        </DriverName>
                        {asset.driver1?.phone_number && (
                          <PhoneLink
                            href={`tel:${asset.driver1?.phone_number}`}
                            title={t("Anrufen") as string}
                            aria-label={t("Anrufen")}
                          >
                            <span className="icon-ico_phone" />
                          </PhoneLink>
                        )}
                      </DriverContainer>
                    </TableValue>
                  </tr>
                  <tr hidden={!asset.driver2}>
                    <TableLabel>{t("Beifahrer")}:</TableLabel>
                    <TableValue noWrap>
                      <DriverContainer>
                        <DriverName title={`${asset.driver2?.last_name} ${asset.driver2?.first_name}`}>
                          {asset.driver2?.last_name} {asset.driver2?.first_name}
                        </DriverName>
                        <PhoneLink
                          href={`tel:${asset.driver2?.phone_number}`}
                          title={t("Anrufen") as string}
                          aria-label={t("Anrufen")}
                          hidden={!asset.driver2?.phone_number}
                        >
                          <span className="icon-ico_phone" />
                        </PhoneLink>
                      </DriverContainer>
                    </TableValue>
                  </tr>
                </tbody>
              </DataTable>
            </>
          )}

          {drivingTimeData && (
            <>
              <SubHeadline>{t("Lenkzeiten")}</SubHeadline>
              <DataTable>
                <tbody>
                  <tr>
                    <TableLabel>{t("Tageslenkzeit")}:</TableLabel>
                    <TableValue>{formatMinutes(drivingTimeData.drive_summaries.day.drive)} </TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Tagesrestzeit")}:</TableLabel>
                    <TableValue>{formatMinutes(drivingTimeData.drive_summaries.day.drive_left)} </TableValue>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <div style={{marginTop: 4}} />
                    </td>
                  </tr>
                  <tr>
                    <TableLabel>{t("Lenkzeit seit Pause")}:</TableLabel>
                    <TableValue>{formatMinutes(drivingTimeData.drive_summaries.drive.drive)} </TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Restlenkzeit bis Pause")}:</TableLabel>
                    <TableValue>{formatMinutes(drivingTimeData.drive_summaries.drive.drive_left)}</TableValue>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <DetailsLink onClick={openDrivingTime}>
                        <span className="icon-ico_chevron-right" style={{fontSize: 14}} />{" "}
                        {t("Details zu Lenk- u. Ruhezeiten")}
                      </DetailsLink>
                    </td>
                  </tr>
                </tbody>
              </DataTable>
            </>
          )}

          <SubHeadline>{t("Meldungen")}</SubHeadline>
          <AlarmsChip icon={<span className="icon-ico_alert" />} label="2" className="alarms" />
          <AlarmsChip icon={<span className="icon-ico_warning" />} label="0" />
          <AlarmsChip icon={<span className="icon-ico_warning24" />} label="0" />
        </DataContainer>
      </Content>
    </Overlay>
  );
};

export default MapDataOverlay;
