import React, {useCallback} from "react";
import {HereMap} from "../Map";
import styled from "styled-components/macro";
import {useSelector, useDispatch} from "react-redux";
import {overlaySelector} from "../state/overlay/selectors";
import {hideOverlay} from "../state/overlay/actions";
import {
  OVERLAY_ROUTE,
  OVERLAY_GEOFENCE,
  OVERLAY_GEOFENCE_EDIT,
  OVERLAY_FUELSTATION,
} from "../state/overlay/reducer";
import RouteOverlay from "./overlays/RouteOverlay";
import GeofenceOverlay from "./overlays/GeofenceOverlay";
import GeofenceEditOverlay from "./overlays/GeofenceEditOverlay";
import {IconButton} from "@material-ui/core";
import FuelStationOverlay from "./overlays/FuelStationOverlay";

// -----------------------------------------------------------------------------------------------------------
export const MapOverlayContainer = styled.div`
  position: absolute;
  top: 92px;
  right: 16px;
  background-color: var(--dkv-background-primary-color);
  width: 430px;
  color: var(--dkv-text-primary-color);
  line-height: 1.25;
  max-height: calc(100% - 357px);
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  z-index: 2;
  overflow: auto;
`;

export const MapOverlayCloseButton = styled(IconButton)`
  position: absolute !important;
  bottom: 14px;
  right: 10px;
  top: 10px;
  padding: 0 !important;
  width: 30px;
  height: 30px;

  svg {
    fill: #666666 !important;
  }
`;

export const Headline = styled.h1`
  font-size: 1.7142857142857142rem;
  font-weight: normal;
  padding: 24px 24px 12px 24px;
  color: #004b78;
`;

// -----------------------------------------------------------------------------------------------------------
interface MapOverlayProps {
  hereMap: HereMap;
}

const MapOverlay: React.FC<MapOverlayProps> = ({hereMap}) => {
  const dispatch = useDispatch();

  // overlay data
  const overlay = useSelector(overlaySelector);

  // close
  const closeOverlay = useCallback(() => {
    dispatch(hideOverlay());
  }, [dispatch]);

  // render
  if (!overlay.visible) {
    return null;
  }

  const renderOverlay = () => {
    switch (overlay.type) {
      case OVERLAY_ROUTE:
        return <RouteOverlay hereMap={hereMap} data={overlay} />;
      case OVERLAY_GEOFENCE:
        return <GeofenceOverlay hereMap={hereMap} data={overlay} />;
      case OVERLAY_GEOFENCE_EDIT:
        return <GeofenceEditOverlay hereMap={hereMap} data={overlay} />;
      case OVERLAY_FUELSTATION:
        return <FuelStationOverlay hereMap={hereMap} data={overlay} />;
    }

    return null;
  };

  return (
    <MapOverlayContainer>
      <MapOverlayCloseButton onClick={closeOverlay} title={"Ausblenden"}>
        <span className="icon-ico_clear" style={{fontSize: 24, color: "#363636"}} />
      </MapOverlayCloseButton>
      {renderOverlay()}
    </MapOverlayContainer>
  );
};

export default MapOverlay;
