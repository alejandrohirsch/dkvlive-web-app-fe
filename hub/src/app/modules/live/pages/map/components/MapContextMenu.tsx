import React, {useEffect} from "react";
import {HereMap} from "../Map";
import {useTranslation} from "react-i18next";
import {useSelector, useDispatch} from "react-redux";
import {overlaySelector} from "../state/overlay/selectors";
import {OVERLAY_ROUTE} from "../state/overlay/reducer";
import {resolveUserLanguage} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
function triggerRouteTargetEvent(platform: any, clickCoords: any) {
  const reverseGeocodingParameters = {
    prox: clickCoords.lat + "," + clickCoords.lng + ",200",
    mode: "retrieveAddresses",
    maxresults: 1,
    language: resolveUserLanguage(),
  };

  platform.getGeocodingService().reverseGeocode(reverseGeocodingParameters, function(result: any) {
    try {
      const location = result.Response.View[0].Result[0].Location;
      const label = location.Address.Label;

      window.dispatchEvent(
        new CustomEvent("dkv-route-target", {detail: {locationId: location.LocationId, label}})
      );
    } catch (e) {
      // @todo growl
      console.error(e);
    }
  });
}

// -----------------------------------------------------------------------------------------------------------
interface MapContextMenuProps {
  hereMap: HereMap;
}

const MapContextMenu: React.FC<MapContextMenuProps> = ({hereMap: {map, platform}}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // overlay
  const overlay = useSelector(overlaySelector);

  // contextmenu
  useEffect(() => {
    const onContextMenu = (e: any) => {
      const clickCoords = map.screenToGeo(e.viewportX, e.viewportY);

      if (overlay.visible && overlay.type === OVERLAY_ROUTE) {
        e.items.push(
          new H.util.ContextItem({
            label: t("Route hierher"),
            callback: () => triggerRouteTargetEvent(platform, clickCoords),
          })
        );
      }

      // @todo commingsoon
      // if (hasPermission("management:CreateGeofences")) {
      //   e.items.push(
      //     new H.util.ContextItem({
      //       label: `${t("Neue Geozone anlegen")}`,
      //       callback: () => {
      //         dispatch(showOverlay(OVERLAY_GEOFENCE, ""));
      //       },
      //     })
      //   );
      // }
    };

    map.addEventListener("contextmenu", onContextMenu);

    return () => map.removeEventListener("contextmenu", onContextMenu);
  }, [map, t, overlay, platform, dispatch]);

  return null;
};

export default MapContextMenu;
