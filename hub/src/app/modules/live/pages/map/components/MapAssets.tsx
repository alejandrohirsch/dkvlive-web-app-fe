import React, {useEffect, useRef} from "react";
import ReactDOM from "react-dom";
import {HereMap} from "../Map";
import {useSelector, useDispatch} from "react-redux";
import {assetSelector, mapAssetsListSelector, assetsItemsSelector} from "../../../state/assets/selectors";
import {Asset} from "../../../state/assets/reducer";
import AssetInfoBubble from "./AssetInfoBubble";
import {showDetails, hideDetails} from "../state/details/actions";
import {detailsSelector} from "../state/details/selectors";
import {hideOverlay} from "../state/overlay/actions";
import {hasPermission} from "app/modules/login/state/login/selectors";
import {Dispatch} from "redux";

// -----------------------------------------------------------------------------------------------------------
let mapInitialyZoomed = false;
let markerBubble:
  | {
      bubble: any;
      marker: any;
    }
  | undefined;

// -----------------------------------------------------------------------------------------------------------
function createAssetMarkerDiv(asset: Asset) {
  const div = document.createElement("div");
  div.style.width = "116px";
  div.style.height = "28px";
  div.style.cursor = "pointer";
  div.style["border-radius"] = "20px";
  div.style["font-size"] = "14px";
  div.style["line-height"] = "1.25";
  div.style.color = "var(--dkv-text-primary-color)";
  div.style["font-weight"] = "bold";
  div.style["padding-left"] = "10px";
  div.style["display"] = "flex";
  div.style["align-items"] = "center";
  div.style.margin = "-14px 0 0 -104px";
  div.style["will-change"] = "transform";
  div.style["background-color"] = "#fff";
  div.style["color"] = "#363636";
  div.style["border"] = "2px solid transparent";
  div.style["box-shadow"] = "0 2px 5px 0 rgba(128, 128, 128, 0.5)";
  div.style["overflow"] = "hidden";

  // label
  const span = document.createElement("span");
  span.classList.add("dkv-asset-label");
  span.innerText = asset.name ?? asset.license_plate;
  div.appendChild(span);

  // icon
  const iconDiv = document.createElement("div");
  iconDiv.classList.add("dkv-asset-icondiv");
  iconDiv.classList.add("dkv-asset-icondiv--offline");

  iconDiv.innerHTML = `
    <div class="dkv-asset-icondiv-online">
      <svg viewBox="0 0 512 512" height="12" width="12" aria-hidden="true" focusable="false" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill="currentColor" d="M444.52 3.52L28.74 195.42c-47.97 22.39-31.98 92.75 19.19 92.75h175.91v175.91c0 51.17 70.36 67.17 92.75 19.19l191.9-415.78c15.99-38.39-25.59-79.97-63.97-63.97z"></path>
      </svg>
    </div>
    <div class="dkv-asset-icondiv-standby">
      <svg viewBox="0 0 24 24" height="24" width="24" aria-hidden="true" focusable="false" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path d="M8 7h3v10H8zm5 0h3v10h-3z"></path>
      </svg>
    </div>
    <div class="dkv-asset-icondiv-offline">
      <svg viewBox="0 0 24 24" height="24" width="24" aria-hidden="true" focusable="false" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path d="M7 7h10v10H7z"></path>
      </svg>
    </div>
  `;

  div.appendChild(iconDiv);

  return div;
}

function openMarkerBubble(
  marker: any,
  map: any,
  ui: any,
  dispatch: any,
  zoom?: boolean,
  dontCenter?: boolean
) {
  const {asset: targetAsset, bubbleDiv} = marker.getData();

  // open bubble
  if (markerBubble !== undefined) {
    ui.removeBubble(markerBubble.bubble);
  }

  renderAssetInfoBubble(bubbleDiv, targetAsset as Asset, dispatch, ui, map, marker);

  const bubble = new H.ui.InfoBubble(marker.getGeometry(), {content: bubbleDiv});
  bubble.addClass("dkv-map-asset-infobubble");
  ui.addBubble(bubble);

  markerBubble = {
    bubble,
    marker: marker,
  };

  if (!dontCenter) {
    map.setCenter(marker.getGeometry());
  }

  // zoom
  if (zoom) {
    if (map.getZoom() < 17) {
      map.setZoom(17);
    }
  }
}

interface AssetMarkerIconElements {
  container: HTMLDivElement;
  label: HTMLSpanElement;
  icon: HTMLDivElement;
  track: SVGElement;
}

export function createAssetMarker(asset: Asset, map: any, ui: any, dispatch?: Dispatch<any>) {
  const div = createAssetMarkerDiv(asset);

  const markerIcon = new H.map.DomIcon(div, {
    onAttach: function(el: any, _: any, marker: any) {
      const data = marker.getData();

      const elements: AssetMarkerIconElements = {
        container: el,
        label: el.querySelector(".dkv-asset-label") as HTMLSpanElement,
        icon: el.querySelector(".dkv-asset-icondiv") as HTMLDivElement,
        track: el.querySelector(".dkv-asset-icondiv-online svg") as SVGElement,
      };

      updateAssetMarkerIcon(elements, data.asset, data.asset.gnss.track, map, marker);

      marker.setData({...data, _elements: elements});
    },
    onDetach: function(_el: any, _: any, marker: any) {
      const data = marker.getData();
      marker.setData({...data, _elements: undefined});
    },
  });

  const marker = new H.map.DomMarker(
    {lat: asset.gnss.latitude, lng: asset.gnss.longitude},
    {icon: markerIcon, volatility: true}
  );
  marker.setData({
    bubbleDiv: document.createElement("div"),
    asset: asset,
  });

  if (dispatch && typeof dispatch === "function") {
    marker.addEventListener("tap", (e: MouseEvent) => {
      e.preventDefault();
      e.stopPropagation();

      const targetMarker = e.target as any;
      const {asset} = targetMarker.getData();

      map.__centerAsset = asset ? asset.id : null;
      openMarkerBubble(targetMarker, map, ui, dispatch, false);
      dispatch(showDetails(asset.id));
      dispatch(hideOverlay());
    });
  }

  return marker;
}

function ease(ts: any, marker: any, endCoord: any, durationMs: any, onStep: any, startCoord?: any) {
  if (!startCoord) {
    startCoord = marker.getGeometry();
  }

  const stepCount = durationMs / 16;

  const valueIncrementLat = (endCoord.lat - startCoord.lat) / stepCount;
  const valueIncrementLng = (endCoord.lng - startCoord.lng) / stepCount;
  const sinValueIncrement = Math.PI / stepCount;
  let currentValueLat = startCoord.lat;
  let currentValueLng = startCoord.lng;
  let currentSinValue = 0;

  function step() {
    if (!assetsAttached) {
      return;
    }

    if (!marker._isActiveWalking) {
      marker.setGeometry(endCoord);
      return;
    }

    const {asset} = marker.getData();
    if (ts !== asset.gnss.ts) {
      return;
    }

    currentSinValue += sinValueIncrement;
    currentValueLat += valueIncrementLat;
    currentValueLng += valueIncrementLng;

    if (currentSinValue < Math.PI) {
      onStep({lat: currentValueLat, lng: currentValueLng});
      window.requestAnimationFrame(step);
    } else {
      onStep(endCoord);
    }
  }

  window.requestAnimationFrame(step);
}

// let targetMarker: any;

// function createMarkerIcon() {
//   const div = document.createElement("div");
//   div.classList.add("dkv-map-location-icon");
//   div.classList.add("icon-ico_route-active");

//   return new H.map.DomIcon(div);
// }

// function showMarker(map: any, data: any) {
//   hideMarker(map);

//   const pos = {lat: data.latitude, lng: data.longitude};
//   targetMarker = new H.map.DomMarker(pos, {icon: createMarkerIcon()});
//   map.addObject(targetMarker);
// }

// function hideMarker(map: any) {
//   if (!targetMarker) {
//     return;
//   }

//   map.removeObjects([targetMarker]);
//   targetMarker = null;
// }

const updateMarkerGeometry = (marker: any, map: any, asset: Asset, coords: any) => {
  marker.setGeometry(coords);

  if (map.__centerAsset !== asset.id) {
    marker._isActiveWalking = false;
    marker.__currentTS = undefined;
    return;
  }

  // update info bubble
  if (markerBubble && markerBubble.marker === marker) {
    markerBubble.bubble.setPosition(coords);
  }

  // cam
  if (map.__centerOnAsset && !controlPressed) {
    map.setCenter(coords);
  }
};

export function updateAssetMarker(marker: any, asset: Asset, map: any, ui: any, dispatch?: any) {
  const data = marker.getData();

  // update marker
  let track;

  const newPoint = {lat: asset.gnss.latitude, lng: asset.gnss.longitude};
  if (map.__centerAsset === asset.id && asset.gnss.ts) {
    if (map.__interpolateMarker) {
      // @todo: add user config to activate
      marker._isActiveWalking = true;

      if (marker.__currentTS === undefined) {
        marker.__currentTS = asset.gnss.ts;
      }

      if (marker.__currentTS !== asset.gnss.ts) {
        const delta = new Date(asset.gnss.ts!).getTime() - new Date(marker.__currentTS).getTime();
        marker.__currentTS = asset.gnss.ts;

        const distance = marker.getGeometry().distance(new H.geo.Point(newPoint.lat, newPoint.lng));
        if (distance > 1000) {
          marker._isActiveWalking = false;
          marker.__currentTS = undefined;
          updateMarkerGeometry(marker, map, asset, newPoint);
        } else {
          ease(asset.gnss.ts, marker, newPoint, delta + (delta < 5000 ? 1000 : 2000), (coords: any) => {
            updateMarkerGeometry(marker, map, asset, coords);
          });
        }
      }

      track = asset._prevTrack;
    } else {
      marker._isActiveWalking = false;
      marker.__currentTS = undefined;
      updateMarkerGeometry(marker, map, asset, newPoint);

      track = asset._currentTrack;
    }
  } else {
    marker._isActiveWalking = false;
    marker.__currentTS = undefined;
    marker.setGeometry(newPoint);

    track = asset._currentTrack;
  }

  marker.setData({
    ...data,
    asset: asset,
  });

  if (data._elements) {
    updateAssetMarkerIcon(data._elements, asset, track, map, marker);
  }

  // update info bubble
  if (dispatch && markerBubble && markerBubble.marker === marker) {
    renderAssetInfoBubble(data.bubbleDiv, asset, dispatch, ui, map, marker);
  }
}

function renderAssetInfoBubble(
  div: HTMLDivElement,
  asset: Asset,
  dispatch: any,
  ui: any,
  map: any,
  marker: any
) {
  ReactDOM.render(
    <AssetInfoBubble
      asset={asset}
      dispatch={dispatch}
      ui={ui}
      close={removeAssetInfoBubble}
      map={map}
      marker={marker}
    />,
    div
  );
}

function updateAssetMarkerIcon(
  elements: AssetMarkerIconElements,
  asset: Asset,
  track: number | undefined,
  map: any,
  marker: any
) {
  // status
  switch (asset._status) {
    case "online":
      elements.icon.classList.add("dkv-asset-icondiv--online");
      elements.icon.classList.remove("dkv-asset-icondiv--standby");
      elements.icon.classList.remove("dkv-asset-icondiv--offline");
      break;
    case "standby":
      elements.icon.classList.remove("dkv-asset-icondiv--online");
      elements.icon.classList.add("dkv-asset-icondiv--standby");
      elements.icon.classList.remove("dkv-asset-icondiv--offline");
      break;
    default:
      elements.icon.classList.remove("dkv-asset-icondiv--online");
      elements.icon.classList.remove("dkv-asset-icondiv--standby");
      elements.icon.classList.add("dkv-asset-icondiv--offline");
  }

  // track
  elements.track.style.transform = `rotate(${-45 + (track ?? 45)}deg)`;

  // active
  if (map.__centerAsset === asset.id) {
    if (marker.getZIndex() !== 1) {
      marker.setZIndex(1);
    }

    elements.container.classList.add("dkv-asset-icon--active");
  }
}

function removeAssetInfoBubble(ui: any) {
  if (markerBubble === undefined) {
    return;
  }

  ui.removeBubble(markerBubble.bubble);
  markerBubble = undefined;
}

// -----------------------------------------------------------------------------------------------------------
let currentActiveID: string;
let controlPressed: boolean;
let assetsAttached: boolean;

// -----------------------------------------------------------------------------------------------------------
interface MapAssetsProps {
  hereMap: HereMap;
}

const MapAssets: React.FC<MapAssetsProps> = ({hereMap: {map, ui}}) => {
  const dispatch = useDispatch();

  const assets = useSelector(assetsItemsSelector);
  const mapAssets = useSelector(mapAssetsListSelector);

  const assetMarkerGroup = useRef(new H.map.Group());
  const assetMarkers = useRef({});

  // add marker group to map
  useEffect(() => {
    map.addObject(assetMarkerGroup.current);

    map.addEventListener("tap", () => {
      removeAssetInfoBubble(ui);
    });

    assetsAttached = true;
    return () => {
      assetsAttached = false;
    };
  }, [map, ui]);

  // create / update markers
  useEffect(() => {
    const group = assetMarkerGroup.current;
    const markers = assetMarkers.current;

    const assetsFound = {};

    for (const mapAsset of mapAssets) {
      const asset = assets[mapAsset];

      let marker = markers[asset.id];
      if (marker) {
        updateAssetMarker(marker, asset, map, ui, dispatch);
      } else {
        marker = createAssetMarker(asset, map, ui, dispatch);
        group.addObject(marker);
        markers[asset.id] = marker;
      }

      assetsFound[asset.id] = true;
    }

    for (const aid in markers) {
      if (!assetsFound[aid]) {
        const m = markers[aid];
        group.removeObject(m);

        if (markerBubble && m === markerBubble.marker) {
          removeAssetInfoBubble(ui);
        }

        if (map.__centerAsset === aid) {
          map.__centerAsset = null;
          dispatch(hideOverlay());
          dispatch(hideDetails());
        }

        delete markers[aid];
      }
    }

    if (!mapInitialyZoomed && mapAssets && mapAssets.length) {
      map.getViewModel().setLookAtData({
        bounds: group.getBoundingBox(),
      });

      map.__assetGroup = group;

      setTimeout(() => {
        if (map.getZoom() > 19) {
          map.setZoom(18);
        } else {
          map.setZoom(map.getZoom() - 0.5);
        }
      });

      mapInitialyZoomed = true;
    }
  }, [map, assets, mapAssets, ui, dispatch]);

  // global event listener
  useEffect(() => {
    const onShowAsset = (e: CustomEventInit<string>) => {
      const marker = assetMarkers.current[e.detail ?? "-1"];
      if (!marker) {
        return;
      }

      map.__centerAsset = e.detail ?? undefined;
      dispatch(hideOverlay());
      openMarkerBubble(marker, map, ui, dispatch);
    };

    window.addEventListener("dkv-map-show-asset", onShowAsset);

    // add interpolate toggle für superadmin
    const toggleInterpolation = () => {
      map.__interpolateMarker = !map.__interpolateMarker;
    };

    if (hasPermission("superadmin:InterpolateAsset")) {
      window.addEventListener("dkv-map-interpolate-asset", toggleInterpolation);
    }

    return () => {
      window.removeEventListener("dkv-map-show-asset", onShowAsset);

      if (hasPermission("superadmin:InterpolateAsset")) {
        window.removeEventListener("dkv-map-interpolate-asset", toggleInterpolation);
      }
    };
  }, [map, ui, dispatch]);

  // active asset
  const {assetID} = useSelector(detailsSelector);
  const asset = useSelector(assetSelector(assetID || ""));

  useEffect(() => {
    if (!asset) {
      return;
    }

    const markers = assetMarkers.current;

    // remove from current active
    if (currentActiveID && currentActiveID !== asset.id) {
      const currentMarker = markers[currentActiveID];
      if (currentMarker) {
        currentMarker.setZIndex(0);
        const {_elements} = currentMarker.getData();
        if (_elements) {
          _elements.container.classList.remove("dkv-asset-icon--active");
        }
      }
    }

    // set active
    let center = false;
    let init = false;
    if (!map.__centerAsset) {
      map.__centerAsset = asset.id;
      center = map.__centerOnAsset || false;
      init = true;
    }

    currentActiveID = asset.id;
    const marker = markers[asset.id];
    if (marker) {
      marker.setZIndex(1);
      const {_elements} = marker.getData();
      if (_elements) {
        _elements.container.classList.add("dkv-asset-icon--active");
      }

      if (init) {
        openMarkerBubble(marker, map, ui, dispatch, false, true);
      }

      if (center) {
        map.setCenter(marker.getGeometry());
      }
    }
  }, [asset, map, ui, dispatch]);

  // ctrl key state
  useEffect(() => {
    const onKey = (e: any) => {
      controlPressed = e.ctrlKey;
    };

    document.addEventListener("keydown", onKey);
    document.addEventListener("keyup", onKey);

    return () => {
      controlPressed = false;
      document.removeEventListener("keydown", onKey);
      document.removeEventListener("keyup", onKey);
    };
  }, []);

  return null;
};

export default MapAssets;
