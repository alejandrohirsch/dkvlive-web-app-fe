import React, {useCallback} from "react";
import {Asset} from "../../../../state/assets/reducer";
import styled, {css} from "styled-components/macro";
import {useTranslation} from "react-i18next";
import PedalStatus from "./PedalStatus";
import {formatNumber} from "dkv-live-frontend-ui";
import {useDispatch} from "react-redux";
import {showDetailsOverlay} from "../../state/details/actions";
import {IconButton, Tooltip} from "@material-ui/core";
import {resolveUserLanguage} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  padding: 16px 40px 16px 40px;
  overflow: auto;
  flex-flow: row wrap;
  position: relative;
`;

interface AreaProps {
  areaWidth?: string;
}

export const Area = styled.div<AreaProps>`
  line-height: 1.5;
  font-size: 1.1428571428571428rem;
  margin-right: 16px;
  max-width: 500px;
  flex-shrink: 0;

  ${(props) =>
    props.areaWidth &&
    css`
      width: ${props.areaWidth};
    `}
`;

export const AreaLabel = styled.p`
  font-weight: bold;
  color: var(--dkv-text-black-color);
  margin-bottom: 8px;
`;

export const AreaDataTable = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  width: 100%;
`;

export const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 4px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

export const TableValue = styled.td<TableValueProps>`
  padding-bottom: 4px;
  color: var(--dkv-grey_90);
  padding-left: 16px;

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

interface TableDetailLabelProps {
  first?: boolean;
}

interface AxleWeightLabelProps {
  first?: boolean;
}

export const AxleWeightLabel = styled.span<AxleWeightLabelProps>`
  color: #8c8c8c;
  margin-left: 16px;

  ${(props) =>
    props.first &&
    css`
      margin-left: 0;
    `}
`;

export const TableDetailLabel = styled.span<TableDetailLabelProps>`
  color: #8c8c8c;
  margin-left: 16px;

  ${(props) =>
    props.first &&
    css`
      margin-left: 0;
    `}
`;

export const Icon = styled.span`
  font-size: 1.4285714285714286rem;
  flex-shrink: 0;
  margin-right: 8px;
`;

const StickButton = styled(IconButton)`
  position: absolute !important;
  padding: 0 !important;
  right: 8px;
  top: 8px;
  display: flex;
  align-items: center;
  width: 64px;
  height: 64px;
  margin: auto !important;
  color: #004b78;
  font-weight: bold;

  .MuiIconButton-label {
    display: flex;
    flex-direction: column;
  }

  span {
    font-size: 12px;
    color: #004b78;
  }

  span:first-of-type {
    font-size: 20px;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface OverviewProps {
  asset: Asset;
  dataOverlayVisible: boolean;
}

const Overview: React.FC<OverviewProps> = ({asset, dataOverlayVisible}) => {
  const {t} = useTranslation();

  const locale = resolveUserLanguage();

  // open overlay
  const dispatch = useDispatch();

  const openOverlay = useCallback(() => {
    dispatch(showDetailsOverlay());
  }, [dispatch]);

  const handleMouseDown = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  // render
  return (
    <Container>
      <Area areaWidth="400px">
        <AreaDataTable>
          <tbody>
            <tr>
              <TableLabel>{t("Letzte Ortung")}:</TableLabel>
              <TableValue>{asset.gnss._ts}</TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Zone")}:</TableLabel>
              <TableValue>
                {asset.gnss.address?.label}{" "}
                <Tooltip title={`${asset.gnss.latitude?.toFixed(6)},${asset.gnss.longitude?.toFixed(6)}`}>
                  <i className="icon-ico_info" />
                </Tooltip>
              </TableValue>
            </tr>
            <tr>
              <TableLabel>
                <span>{t("Geschwindigkeit")}:</span>
              </TableLabel>
              <TableValue>{asset.calc.speed} km/h</TableValue>
            </tr>
          </tbody>
        </AreaDataTable>
      </Area>

      <Area areaWidth="296px">
        <AreaDataTable>
          <tbody>
            {asset.canbus.fuel_level ? (
              <tr>
                <TableLabel>
                  <span>{t("Tankinhalt")}:</span>
                </TableLabel>
                <TableValue>{formatNumber(locale, asset.canbus.fuel_level)}%</TableValue>
              </tr>
            ) : null}

            {asset.canbus.fuel_used_hr ? (
              <tr>
                <TableLabel>
                  <span>{t("Verbrauch")}:</span>
                </TableLabel>
                <TableValue>{formatNumber(locale, asset.canbus.fuel_used_hr)} l</TableValue>
              </tr>
            ) : null}

            {asset.canbus.vehicle_distance ? (
              <tr>
                <TableLabel>
                  <span>{t("KM-Stand")}:</span>
                </TableLabel>
                <TableValue>{formatNumber(locale, asset.canbus.vehicle_distance)} km</TableValue>
              </tr>
            ) : null}

            <tr>
              <TableLabel>{t("Höhe")}:</TableLabel>
              <TableValue>{asset.gnss.altitude} m</TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Richtung")}:</TableLabel>
              <TableValue>{asset.gnss.track}°</TableValue>
            </tr>
          </tbody>
        </AreaDataTable>
      </Area>

      <Area areaWidth="256px">
        <AreaDataTable>
          <tbody>
            {asset.canbus.engine_speed ? (
              <tr>
                <TableLabel>{t("Motordrehzahl")}:</TableLabel>
                <TableValue>
                  {formatNumber(locale, asset.canbus.engine_speed)} {t("RPM")}
                </TableValue>
              </tr>
            ) : null}

            {asset.canbus.engine_temp ? (
              <tr>
                <TableLabel>{t("Motortemperatur")}:</TableLabel>
                <TableValue>{asset.canbus.engine_temp}°</TableValue>
              </tr>
            ) : null}

            {asset.canbus.engine_hours ? (
              <tr>
                <TableLabel>{t("Motorstunden")}:</TableLabel>
                <TableValue>{formatNumber(locale, asset.canbus.engine_hours)} h</TableValue>
              </tr>
            ) : null}

            {asset.canbus.truck_axle_weight_1 ||
            asset.canbus.truck_axle_weight_2 ||
            asset.canbus.truck_axle_weight_3 ||
            asset.canbus.truck_axle_weight_4 ||
            asset.canbus.truck_axle_weight_5 ? (
              <>
                <tr>
                  <TableLabel colSpan={2} style={{paddingBottom: 2}}>
                    {t("Achslast")} (kg):
                  </TableLabel>
                  <TableValue>
                    {formatNumber(locale, asset.canbus.truck_axle_weight_1!)} /{" "}
                    {formatNumber(locale, asset.canbus.truck_axle_weight_1!)}
                    {/* <p>
                      {asset.canbus.truck_axle_weight_1 ? (
                        <>
                          <AxleWeightLabel first>1: </AxleWeightLabel>{" "}
                          {formatNumber(locale, asset.canbus.truck_axle_weight_1)}
                        </>
                      ) : null}

                      {asset.canbus.truck_axle_weight_2 ? (
                        <>
                          <AxleWeightLabel>2: </AxleWeightLabel>{" "}
                          {formatNumber(locale, asset.canbus.truck_axle_weight_2)}
                        </>
                      ) : null}

                      {asset.canbus.truck_axle_weight_3 ? (
                        <>
                          <AxleWeightLabel>3: </AxleWeightLabel>{" "}
                          {formatNumber(locale, asset.canbus.truck_axle_weight_3)}
                        </>
                      ) : null}
                    </p>
                    <p>
                      {asset.canbus.truck_axle_weight_4 ? (
                        <>
                          <AxleWeightLabel first>4: </AxleWeightLabel>{" "}
                          {formatNumber(locale, asset.canbus.truck_axle_weight_4)}
                        </>
                      ) : null}

                      {asset.canbus.truck_axle_weight_5 ? (
                        <>
                          <AxleWeightLabel>5: </AxleWeightLabel>{" "}
                          {formatNumber(locale, asset.canbus.truck_axle_weight_5)}
                        </>
                      ) : null}
                    </p> */}
                  </TableValue>
                </tr>
              </>
            ) : null}
          </tbody>
        </AreaDataTable>
      </Area>

      <Area areaWidth="216px">
        <AreaDataTable>
          <tbody>
            <tr>
              <TableLabel>{t("Kupplung")}:</TableLabel>
              <TableValue>
                <PedalStatus value={asset.canbus.clutch_pedal ? 100 : 0} />
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Bremse")}:</TableLabel>
              <TableValue>
                <PedalStatus value={asset.canbus.break_pedal ? 100 : 0} />
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Gaspedal")}:</TableLabel>
              <TableValue>
                <PedalStatus value={asset.canbus.acc_pedal ?? 0} showLabel />
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Tempomat")}:</TableLabel>
              <TableValue>
                <PedalStatus value={asset.canbus.cruise_control ? 100 : 0} />
              </TableValue>
            </tr>
          </tbody>
        </AreaDataTable>
      </Area>

      {!dataOverlayVisible && (
        <StickButton aria-label={t("Infos")} onClick={openOverlay} onMouseDown={handleMouseDown}>
          <span className="icon-ico_export" />
          <span>{t("Infos")}</span>
        </StickButton>
      )}
    </Container>
  );
};

export default Overview;
