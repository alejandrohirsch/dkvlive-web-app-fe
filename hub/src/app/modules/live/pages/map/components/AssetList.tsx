import React, {useCallback} from "react";
import styled, {css} from "styled-components/macro";
import AutoSizer from "react-virtualized-auto-sizer";
import {FixedSizeList as List, ListChildComponentProps} from "react-window";
import {useSelector, useDispatch} from "react-redux";
import {mapAssetsListSelector, assetsItemsSelector} from "../../../state/assets/selectors";
import {Asset} from "../../../state/assets/reducer";
import {callOnEnter} from "dkv-live-frontend-ui";
import AssetItemDetails from "./AssetItemDetails";
import {showDetails} from "../state/details/actions";
import {detailsSelector} from "../state/details/selectors";

// -----------------------------------------------------------------------------------------------------------
interface ContainerProps {
  filterOpen?: boolean;
}

const Container = styled.div<ContainerProps>`
  height: 100%;
  position: relative;

  &:after {
    content: "";
    transition: background-color ease-in-out 0.2s;
  }

  ${(props) =>
    props.filterOpen &&
    css`
      &:after {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.4);
      }
    `}
`;

interface AssetItemContainerProps {
  selected?: boolean;
}

const AssetItemContainer = styled.div<AssetItemContainerProps>`
  padding: 24px 24px 0 24px;
  cursor: pointer;
  outline: 0;
  border-bottom: 1px solid #dbdbdb;

  ${(props) =>
    props.selected &&
    css`
      background-color: var(--dkv-background-primary-hover-color);
    `}

  &:hover,
  &:focus {
    background-color: var(--dkv-background-primary-hover-color);
  }
`;

const AssetItemData = styled.section`
  height: 100%;
  padding-bottom: 24px;
  position: relative;

  h1 {
    font-size: 1.1428571428571428rem;
    line-height: 1.25;
    font-weight: bold;
    margin-bottom: 8px;

    span {
      font-weight: normal;
      font-size: 0.98em;
      margin-left: 4px;
    }
  }
`;

export const AssetLiveDetailsLink = styled.button`
  position: absolute;
  top: 0;
  right: 0;
  color: var(--dkv-link-primary-color);
  line-height: 1.25;
  font-size: 1.1426em;
  font-weight: 500;
  transition: color linear 0.1s;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover,
  &:focus {
    color: var(--dkv-link-primary-hover-color);
  }
`;

const AssetTagList = styled.ul`
  position: absolute;
  top: 25px;
  right: 16px;
  display: flex;
  flex-flow: row;

  li {
    border-radius: 4px;
    background-color: #2e6b90;
    color: #ffffff;
    padding: 2px 5px;
    margin-left: 4px;
    font-size: 12px;
    white-space: nowrap;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface AssetItemProps {
  asset: Asset;
  style: React.CSSProperties;
}

const AssetItem = React.memo(({asset, style}: AssetItemProps) => {
  const dispatch = useDispatch();

  const {assetID} = useSelector(detailsSelector);

  // callbacks
  const showAssetOnMap: any = useCallback(
    (e: Event) => {
      const dataset = (e.currentTarget as HTMLAnchorElement).dataset;

      if (dataset.id) {
        window.dispatchEvent(new CustomEvent("dkv-map-show-asset", {detail: dataset.id}));
        dispatch(showDetails(dataset.id));
      }
    },
    [dispatch]
  );

  // render
  return (
    <AssetItemContainer
      selected={asset.id === assetID}
      style={style as any}
      onClick={showAssetOnMap}
      data-id={asset.id}
      role="button"
      tabIndex={0}
      onKeyDown={callOnEnter(showAssetOnMap)}
    >
      <AssetItemData>
        <h1>
          {asset.name ? (
            <>
              {asset.name} <span>{asset.license_plate}</span>
            </>
          ) : (
            asset._displayName
          )}
        </h1>
        <AssetItemDetails asset={asset} />
      </AssetItemData>
      <AssetTagList>
        {asset.tags
          ? asset.tags.map((tag) => (
              <li
                key={tag.id}
                style={{
                  backgroundColor: "#F39A2E",
                }}
              >
                {tag.name}
              </li>
            ))
          : null}
      </AssetTagList>
    </AssetItemContainer>
  );
});

// -----------------------------------------------------------------------------------------------------------
interface AssetListProps {
  filterOpen?: boolean;
}

const AssetList: React.FC<AssetListProps> = React.memo(({filterOpen}) => {
  // assets
  const assets = useSelector(assetsItemsSelector);
  const mapAssets = useSelector(mapAssetsListSelector);

  const AssetListItem = ({index, style}: ListChildComponentProps) => {
    const asset = assets[mapAssets[index]];
    return <AssetItem asset={asset} style={style as any} />;
  };

  // render
  return (
    <Container filterOpen={filterOpen}>
      <AutoSizer>
        {({height, width}) => (
          <List height={height} width={width} itemCount={mapAssets.length} itemSize={121}>
            {AssetListItem}
          </List>
        )}
      </AutoSizer>
    </Container>
  );
});

export default AssetList;
