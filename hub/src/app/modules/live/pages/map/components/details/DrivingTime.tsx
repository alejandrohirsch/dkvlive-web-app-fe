import React, {useState, useEffect} from "react";
import {Asset} from "app/modules/live/state/assets/reducer";
import styled, {css} from "styled-components/macro";
import {Area, AreaDataTable, TableLabel, TableValue} from "./Overview";
import {useTranslation} from "react-i18next";
import DrivingtimeDayChart from "../DrivingtimeDayChart";
import httpClient from "services/http";
import {formatDate, formatDateShort, formatMinutes} from "services/date";
import {Message, Loading} from "dkv-live-frontend-ui";
import dayjs from "dayjs";
import ComingSoon from "app/components/ComingSoon";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  padding: 8px 8px 4px 8px;
  overflow: auto;
  flex-flow: row wrap;
  position: relative;
`;

const ChartContainer = styled.div`
  min-width: 500px;
  margin-right: 16px;
`;

const CurrentDayLegend = styled.ul`
  margin-top: 16px;
  margin-right: 32px;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`;

interface CurrentDayTimesProps {
  activity: string;
}

const CurrentDayLegendEntry = styled.li<CurrentDayTimesProps>`
  margin-left: 32px;
  position: relative;
  padding-left: 15px;
  font-size: 1.1428571428571428rem;

  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    bottom: 2px;
    margin: auto;
    width: 8px;
    height: 8px;
    background-color: var(--dkv-grey_30);
    border-radius: 50%;

    ${(props) =>
      props.activity === "Work" &&
      css`
        background-color: var(--dkv-dkv_blue);
      `}

      
    ${(props) =>
      props.activity === "Driving" &&
      css`
        background-color: #58be58;
      `}

      
    ${(props) =>
      props.activity === "Available" &&
      css`
        background-color: #77a7c5;
      `}
  }
`;

const CurrentActivityContainer = styled.div`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  margin: 8px 16px 24px 16px;
`;

const CurrentActivityStatus = styled.div<CurrentDayTimesProps>`
  width: 24px;
  height: 24px;
  border-radius: 50%;
  background-color: var(--dkv-grey_30);
  
  ${(props) =>
    props.activity === "Work" &&
    css`
      background-color: var(--dkv-dkv_blue);
    `}

    
  ${(props) =>
    props.activity === "Driving" &&
    css`
      background-color: #58be58;
    `}

    
  ${(props) =>
    props.activity === "Available" &&
    css`
      background-color: #77a7c5;
    `}
`;

const CurrentActivityStatusLabel = styled.span`
  display: block;
  font-size: 1.1428571428571428rem;
  color: #666;
  font-weight: 500;
  line-height: 1.5;
  margin-left: 12px;
`;

const WeeklyrestChart = styled.div`
  height: 150px;
  width: 240px;
  position: relative;
`;

const WeeklyrestChartContainer = styled.div`
  width: 100%;
  height: 100px;
  display: flex;
  align-items: center;
  position: relative;
`;

const WeeklyrestChartLine = styled.div`
  width: 100%;
  height: 2px;
  background-color: var(--dkv-dkv_blue_20);
`;

const WeeklyrestValueContainer = styled.div`
  display: flex;
  justify-content: space-around;
  width: 100%;
  position: absolute;
  top: 0;
  bottom: 0;
  margin: auto;
  align-items: center;
  font-weight: 500;
`;

const WeeklyrestLabelContainer = styled.div`
  display: flex;
  justify-content: space-around;
  width: 100%;
  position: absolute;
  bottom: -50px;
  align-items: center;
`;

const WeeklyrestValueLabel = styled.div`
  width: 100px;
  position: relative;
`;

interface WeeklyrestValueProps {
  full?: boolean;
}

const WeeklyrestValueLabelLine = styled.div<WeeklyrestValueProps>`
  width: 1px;
  position: absolute;
  left: 0;
  top: -20px;
  right: 0;
  margin: auto;
  background-color: #666666;
  height: 14px;

  &:before {
    content: "";
    width: 3px;
    height: 3px;
    border-radius: 50%;
    background-color: #666666;
    position: absolute;
    left: -1px;
    bottom: -4px;
  }

  ${(props) =>
    props.full &&
    css`
      height: 5px;
      top: -10px;
    `}
`;

const WeeklyrestValue = styled.div<WeeklyrestValueProps>`
  width: 65px;
  height: 65px;
  border-radius: 50%;
  background-color: var(--dkv-dkv_blue_20);
  color: #323232;
  font-size: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  margin-left: 10px;
  margin-right: 10px;
  flex-direction: column;

  ${(props) =>
    props.full &&
    css`
      width: 85px;
      height: 85px;
      margin-left: 0;
      margin-right: 0;
    `}
`;

const WeeklyrestValueLabelContainer = styled.div`
  width: 100%;
  border: solid 2px var(--dkv-dkv_blue_20);
  font-size: 12px;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  padding: 2px;
  align-items: center;

  i {
    color: var(--dkv-dkv_blue_20);
    font-size: 20px;
  }
`;

interface FormatInfoProps {
  block?: boolean;
}

const FormatInfo = styled.span<FormatInfoProps>`
  font-size: 12px;

  ${(props) =>
    props.block &&
    css`
      display: block;
    `}
`;

const CurrentActivityStatusLabelLegend = styled.span`
  color: var(--dkv-grey_50);
`;

// -----------------------------------------------------------------------------------------------------------
interface DrivingTimeProps {
  asset: Asset;
}

const DrivingTime: React.FC<DrivingTimeProps> = (/*{}*/) => {
  const {t} = useTranslation();

  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  const [entry, setEntry] = useState<any>(null);

  useEffect(() => {
    const load = async () => {
      // @todo comingsoon
      if (!hasPermission("superadmin:root")) {
        setEntry(null);
        setLoading(false);
        setError("");
      } else {
        const now = dayjs().format("YYYY-MM-DD");

        const [{data}, err] = await httpClient.get(
          `/dtco/activities/0D_DF000037966010/${now}/${now}` // @todo url richtig stellen
        );
        if (err !== null) {
          setError("Lenkzeiten konnten nicht ermittelt werden.");
          setLoading(false);
          return;
        }

        setLoading(false);
        setEntry(data);
      }
    };

    load();
  }, [setEntry]);

  // render
  if (!entry) {
    return (
      <Container>
        <ComingSoon small smallIcon /> {/* @todo commingsoon */}
        <Loading inprogress={loading} />
        <div style={{width: "100%"}}>
          {error && <Message style={{width: "100%"}} text={t(error)} error />}
        </div>
      </Container>
    );
  }

  let lastActivity = "Break";
  if (entry.last_day && entry.last_day.activities) {
    lastActivity = entry.last_day.activities[entry.last_day.activities.length - 1].type;
  }

  return (
    <Container>
      <Loading inprogress={loading} />
      <ChartContainer>
        <CurrentActivityContainer>
          <CurrentActivityStatus activity={lastActivity}></CurrentActivityStatus>
          <CurrentActivityStatusLabel>
            <CurrentActivityStatusLabelLegend>{t("Tageslenkzeit")}:</CurrentActivityStatusLabelLegend>{" "}
            {formatMinutes(entry.drive_summaries.day.drive)} <FormatInfo>(hh:mm)</FormatInfo>
            <CurrentActivityStatusLabelLegend>
              , {t("Lenkzeit seit Pause")}:
            </CurrentActivityStatusLabelLegend>{" "}
            {formatMinutes(entry.drive_summaries.drive.drive)} <FormatInfo>(hh:mm)</FormatInfo>
          </CurrentActivityStatusLabel>
        </CurrentActivityContainer>

        <DrivingtimeDayChart rowData={entry} today />

        <CurrentDayLegend>
          <CurrentDayLegendEntry activity="Driving">{t("Lenkzeiten")}</CurrentDayLegendEntry>
          <CurrentDayLegendEntry activity="Work">{t("Arbeitszeiten")}</CurrentDayLegendEntry>
          <CurrentDayLegendEntry activity="Available">{t("Bereitschaften")}</CurrentDayLegendEntry>
          <CurrentDayLegendEntry activity="Break">{t("Ruhe- und Pausenzeiten")}</CurrentDayLegendEntry>
        </CurrentDayLegend>
      </ChartContainer>
      <Area areaWidth="320px">
        <AreaDataTable>
          <tbody>
            <tr>
              <TableLabel>{t("Datum")}:</TableLabel>
              <TableValue>{formatDate(entry.starting_times.day, true)} (UTC)</TableValue>
            </tr>
          </tbody>
        </AreaDataTable>

        <AreaDataTable style={{marginTop: 2}}>
          <tbody>
            <tr>
              <TableLabel>{t("Schicht-Restlenkzeit")}:</TableLabel>
              <TableValue>
                {formatMinutes(entry.drive_summaries.shift.drive_left)} <FormatInfo>(hh:mm)</FormatInfo>
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Tages-Restlenkzeit")}:</TableLabel>
              <TableValue>
                {formatMinutes(entry.drive_summaries.day.drive_left)} <FormatInfo>(hh:mm)</FormatInfo>
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Wochen-Restlenkzeit")}:</TableLabel>
              <TableValue>
                {formatMinutes(entry.drive_summaries.week.drive_left)} <FormatInfo>(hh:mm)</FormatInfo>
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Wochen-Schichten")}:</TableLabel>
              <TableValue>
                {entry.drive_summaries.shift_15}x 15h ({entry.drive_summaries.shift_15_left} {t("left")})
                <br />
                {entry.drive_summaries.shift_13}x 13h
              </TableValue>
            </tr>
          </tbody>
        </AreaDataTable>
      </Area>
      <Area areaWidth="300px">
        <AreaDataTable>
          <tbody>
            <tr>
              <TableLabel>{t("Wochenruhezeiten")}:</TableLabel>
            </tr>
            <tr>
              <td>
                <WeeklyrestChart>
                  <WeeklyrestChartContainer>
                    <WeeklyrestChartLine />

                    <WeeklyrestValueContainer>
                      {entry.drive_summaries.dbl_week.weekend_break_duration && (
                        <WeeklyrestValue
                          full={entry.drive_summaries.dbl_week.weekend_break_duration > 45 * 60}
                        >
                          {formatMinutes(entry.drive_summaries.dbl_week.weekend_break_duration)}
                          <FormatInfo block>(hh:mm)</FormatInfo>
                        </WeeklyrestValue>
                      )}

                      {entry.drive_summaries.week.weekend_break_duration && (
                        <WeeklyrestValue full={entry.drive_summaries.week.weekend_break_duration > 45 * 60}>
                          {formatMinutes(entry.drive_summaries.week.weekend_break_duration)}
                          <FormatInfo block>(hh:mm)</FormatInfo>
                        </WeeklyrestValue>
                      )}
                    </WeeklyrestValueContainer>
                    <WeeklyrestLabelContainer>
                      {entry.drive_summaries.dbl_week.weekend_break_duration && (
                        <WeeklyrestValueLabel>
                          <WeeklyrestValueLabelLine
                            full={entry.drive_summaries.dbl_week.weekend_break_duration > 45 * 60}
                          />

                          <WeeklyrestValueLabelContainer>
                            <span>{formatDateShort(entry.drive_summaries.dbl_week.start, true)}</span>
                            <i className="icon-ico_chevron-right" />
                            <span>{formatDateShort(entry.drive_summaries.dbl_week.end, true)}</span>
                          </WeeklyrestValueLabelContainer>
                        </WeeklyrestValueLabel>
                      )}

                      {entry.drive_summaries.week.weekend_break_duration && (
                        <WeeklyrestValueLabel>
                          <WeeklyrestValueLabelLine
                            full={entry.drive_summaries.week.weekend_break_duration > 45 * 60}
                          />
                          <WeeklyrestValueLabelContainer>
                            <span>{formatDateShort(entry.drive_summaries.week.start, true)}</span>
                            <i className="icon-ico_chevron-right" />
                            <span>{formatDateShort(entry.drive_summaries.week.end, true)}</span>
                          </WeeklyrestValueLabelContainer>
                        </WeeklyrestValueLabel>
                      )}
                    </WeeklyrestLabelContainer>
                  </WeeklyrestChartContainer>
                </WeeklyrestChart>
              </td>
            </tr>
          </tbody>
        </AreaDataTable>
      </Area>
    </Container>
  );
};

export default DrivingTime;
