import React from "react";
import styled, {css} from "styled-components/macro";
import {Tooltip} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {formatToHour, formatMinutes} from "services/date";

// -----------------------------------------------------------------------------------------------------------
interface ChartContainerProps {
  today?: boolean;
}

const ChartContainer = styled.div<ChartContainerProps>`
  position: relative;
  width: 100%;
  height: 40px;
  padding: 0 20px;

  ${(props) =>
    props.today &&
    css`
      margin-top: 60px;
    `}
`;

const Timeline = styled.div`
  width: 100%;
  height: 12px;
  background-color: var(--dkv-grey_30);
  border-radius: 5px;
  max-width: 750px;
  position: relative;
`;

interface TimelineEntryContainerProps {
  activity: string;
  first: boolean;
  last: boolean;
  ferry: boolean;
}

const TimelineEntryContainer = styled.div<TimelineEntryContainerProps>`
  height: 12px;
  position: absolute;
  top: 0;

  ${(props) =>
    props.activity === "Work" &&
    css`
      background-color: var(--dkv-dkv_blue);
    `}

    
  ${(props) =>
    props.activity === "Driving" &&
    css`
      background-color: #58be58;
    `}

    
  ${(props) =>
    props.activity === "Available" &&
    css`
      background-color: #77a7c5;
    `}

    
    
  ${(props) =>
    props.first &&
    css`
      border-radius: 5px 0 0 5px;
    `}

    ${(props) =>
      props.last &&
      css`
        border-radius: 0 5px 5px 0;
      `}

    ${(props) =>
      props.ferry &&
      css`
        :after {
          content: "";
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          background: repeating-linear-gradient(
            -45deg,
            rgba(0, 0, 0, 0.1),
            rgba(0, 0, 0, 0.2) 5px,
            rgba(0, 0, 0, 0.1) 5px,
            rgba(0, 0, 0, 0.1) 10px
          );
        }
      `}
`;

// -----------------------------------------------------------------------------------------------------------
interface TimelineEntryProps {
  detail: any;
  first: boolean;
  last: boolean;
  ferry: boolean;
}

const TimelineEntry: React.FC<TimelineEntryProps> = ({detail, first, last, ferry}) => {
  const {t} = useTranslation();

  const startMinutes = detail.time;
  const minutes = detail.duration;

  const left = (startMinutes / 1440) * 100;
  const width = (minutes / 1440) * 100;

  let activity;
  switch (detail.type) {
    case "Work":
      activity = "Arbeit";
      break;
    case "Driving":
      activity = "Lenken";
      break;
    case "Available":
      activity = "Bereitschaft";
      break;
    default:
      activity = "Pause";
  }

  return (
    <Tooltip
      key={detail.time}
      title={`${t(activity)}${ferry ? ` - ${t("Fähre")}` : ""}`}
      aria-label={`${t(activity)}${ferry ? ` - ${t("Fähre")}` : ""}`}
      classes={{popper: "dkv-dtco-tooltip"}}
      enterDelay={20}
    >
      <TimelineEntryContainer
        activity={detail.type}
        style={{left: `${left}%`, width: `${width}%`}}
        first={first}
        last={last}
        ferry={ferry}
      />
    </Tooltip>
  );
};

// -----------------------------------------------------------------------------------------------------------
const TimelineTickContainer = styled.div`
  position: absolute;
  top: 12px;
  display: flex;
  align-items: flex-start;
  flex-flow: column nowrap;

  span {
    font-size: 0.7142857142857143rem;
    color: var(--dkv-grey_40);
    margin-top: 10px;
    transform: translateX(-50%);
  }
`;

const TimelineTickLine = styled.div`
  height: 7px;
  width: 1px;
  background-color: var(--dkv-grey_30);
  position: relative;
  flex-shrink: 0;

  &:after {
    content: "";
    position: absolute;
    bottom: -6px;
    width: 4px;
    height: 4px;
    border-radius: 50%;
    left: -1px;
    background-color: var(--dkv-grey_30);
  }
`;

interface TimelineTickProps {
  tick: number;
}

const TimelineTick: React.FC<TimelineTickProps> = ({tick}) => {
  const left = ((tick * 60) / 1440) * 100;

  return (
    <TimelineTickContainer style={{left: `${left}%`}}>
      <TimelineTickLine />
      <span>{tick < 10 ? `0${tick}` : tick}:00</span>
    </TimelineTickContainer>
  );
};

// -----------------------------------------------------------------------------------------------------------
interface TimelineTodayEntryContainerProps {
  light?: boolean;
}

const TimelineTodayEntryContainer = styled.div<TimelineTodayEntryContainerProps>`
  position: absolute;
  display: flex;
  align-items: flex-start;
  flex-flow: column-reverse nowrap;

  div {
    display: inline-block;
    color: #666666;
    margin-bottom: 6px;
    font-size: 1.1428571428571428rem;
    font-weight: 500;

    span {
      color: #323232;
    }
  }
`;

interface TimelineTodayEntryIndicatorProps {
  height: string;
  light?: boolean;
}

const TimelineTodayEntryIndicator = styled.div<TimelineTodayEntryIndicatorProps>`
  position: relative;
  width: 1px;
  height: ${(props) => props.height};
  background-color: #666666;

  ${(props) =>
    props.light &&
    css`
      background-color: #c1c1c1;
    `}

  &:after {
    content: "";
    position: absolute;
    width: 3px;
    height: 3px;
    background-color: #666666;
    top: -5px;
    border-radius: 50%;
    left: -1px;
  }
`;

interface TimelineTodayEntryProps {
  startMinutes: number;
  label: string;
  height: string;
  value: string;
}

const TimelineTodayEntry: React.FC<TimelineTodayEntryProps> = ({startMinutes, label, height, value}) => {
  const left = (startMinutes / 1440) * 100;

  // render
  return (
    <TimelineTodayEntryContainer style={{left: `${left}%`, top: `calc(-100% - ${height} - 14px)`}}>
      <TimelineTodayEntryIndicator height={height} />
      <div>
        <span>{value}</span> {label}
      </div>
    </TimelineTodayEntryContainer>
  );
};

// -----------------------------------------------------------------------------------------------------------
const resolveStartMinutes = (date: string) => {
  const tmp = formatToHour(date, true).split(":");
  if (tmp.length !== 2) {
    return 0;
  }

  return parseInt(tmp[0]) * 60 + parseInt(tmp[1]);
};

// -----------------------------------------------------------------------------------------------------------
interface DrivingtimeDayChartProps {
  rowData: any;
  today?: boolean;
}

const DrivingtimeDayChart = React.memo<DrivingtimeDayChartProps>(({rowData, today}) => {
  const ticks = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24];

  if (!rowData) {
    return null;
  }

  const tmpActivities = rowData.last_day ? rowData.last_day.activities ?? [] : rowData.activities ?? [];
  const activities = [];
  for (let i = 1; i < tmpActivities.length; i++) {
    const a = tmpActivities[i - 1];
    const d = tmpActivities[i].time - a.time;
    a.duration = d;
    activities.push(a);
  }

  if (tmpActivities.length) {
    activities.push(tmpActivities[tmpActivities.length - 1]);
  }

  return (
    <ChartContainer today={today}>
      <Timeline>
        {activities.map((detail: any, i: number) => (
          <TimelineEntry
            key={detail.time}
            detail={detail}
            first={i === 0}
            last={i === activities.length - 1}
            ferry={Boolean(detail.ferry)}
          />
        ))}

        {ticks.map((tick: number) => (
          <TimelineTick key={tick} tick={tick} />
        ))}

        {today && rowData.drive_summaries.day.start && (
          <>
            <TimelineTodayEntry
              startMinutes={resolveStartMinutes(rowData.drive_summaries.day.start)}
              label="(UTC) Schichtstart"
              value={formatToHour(rowData.drive_summaries.day.start, true)}
              height="4px"
            />
            {rowData.drive_summaries.drive.start && (
              <TimelineTodayEntry
                startMinutes={rowData.drive_summaries.drive.drive_left_time}
                label="Restlenkzeit bis Pause"
                value={`${formatMinutes(rowData.drive_summaries.drive.drive_left)}h`}
                height="28px"
              />
            )}
          </>
        )}
      </Timeline>
    </ChartContainer>
  );
});

export default DrivingtimeDayChart;
