import React from "react";
import styled from "styled-components";
import {useFormik} from "formik";
import {useTranslation} from "react-i18next";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.form`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;

  input {
    border: 0;
    outline: 0;
    background-color: transparent;
    font-size: 1.2142857142857142rem;
    color: #dbdbdb;
    font-family: inherit;
    width: 100%;
    font-weight: normal;
  }
`;

const SearchButton = styled.button`
  flex-shrink: 0;
  margin-left: 4px;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  background-color: transparent;
  cursor: pointer;
`;

// -----------------------------------------------------------------------------------------------------------
const Search = () => {
  const {t} = useTranslation();

  // search
  const searchForm = useFormik({
    initialValues: {value: ""},
    onSubmit: (values) => {
      console.log(values);
    },
  });

  return (
    <Container onSubmit={searchForm.handleSubmit}>
      <input
        name="value"
        type="value"
        required={true}
        value={searchForm.values.value}
        onChange={searchForm.handleChange}
        placeholder={t("Kennzeichen, Auftrasnummer...")}
      />
      <SearchButton type="submit">
        <span className="icon-ico_search" style={{fontSize: 35}} />
      </SearchButton>
    </Container>
  );
};

export default Search;
