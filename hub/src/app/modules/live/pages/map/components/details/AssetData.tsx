import React, {useCallback} from "react";
import {Asset} from "../../../../state/assets/reducer";
import styled from "styled-components";
import {useTranslation} from "react-i18next";
import {formatNumber} from "dkv-live-frontend-ui";
import {Area as OverviewArea, AreaDataTable, TableLabel, TableValue, AxleWeightLabel} from "./Overview";
import PedalStatus from "./PedalStatus";
import AssetDataCharts from "./AssetDataCharts";
import {IconButton} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {showDetailsOverlay} from "../../state/details/actions";
import {resolveUserLanguage} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  padding: 24px 88px 24px 40px;
  overflow: auto;
  flex-direction: row;
  position: relative;
`;

const StickButton = styled(IconButton)`
  position: absolute !important;
  padding: 0 !important;
  right: 8px;
  top: 8px;
  display: flex;
  align-items: center;
  width: 64px;
  height: 64px;
  margin: auto !important;
  color: #004b78;
  font-weight: bold;

  .MuiIconButton-label {
    display: flex;
    flex-direction: column;
  }

  span {
    font-size: 12px;
    color: #004b78;
  }

  span:first-of-type {
    font-size: 20px;
  }
`;

const Area = styled(OverviewArea)`
  margin-right: 40px;
`;

// -----------------------------------------------------------------------------------------------------------
interface OverviewProps {
  asset: Asset;
}

const Overview: React.FC<OverviewProps> = ({asset}) => {
  const {t} = useTranslation();

  const locale = resolveUserLanguage();

  // open overlay
  const dispatch = useDispatch();

  const openOverlay = useCallback(() => {
    dispatch(showDetailsOverlay());
  }, [dispatch]);

  const handleMouseDown = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  // render
  return (
    <Container>
      <Area areaWidth="296px">
        <AreaDataTable>
          <tbody>
            <tr>
              <TableLabel>{t("Zeitstempel")}:</TableLabel>
              <TableValue>{asset.canbus._ts}</TableValue>
            </tr>
            <tr>
              <TableLabel>
                <span>{t("Geschwindigkeit")}:</span>
              </TableLabel>
              <TableValue>{asset.calc.speed} km/h</TableValue>
            </tr>
            <tr>
              <TableLabel>
                <span>{t("Tankinhalt")}:</span>
              </TableLabel>
              <TableValue>{formatNumber(locale, asset.canbus.fuel_level ?? 0)}%</TableValue>
            </tr>
            <tr>
              <TableLabel>
                <span>{t("Verbrauch")}:</span>
              </TableLabel>
              <TableValue>{formatNumber(locale, asset.canbus.fuel_used_hr ?? 0)} l</TableValue>
            </tr>
            <tr>
              <TableLabel>
                <span>{t("KM-Stand")}:</span>
              </TableLabel>
              <TableValue>{formatNumber(locale, asset.canbus.vehicle_distance ?? 0)} km</TableValue>
            </tr>
          </tbody>
        </AreaDataTable>
      </Area>

      <Area areaWidth="240px">
        <AreaDataTable>
          <tbody>
            <tr>
              <TableLabel>{t("Motordrehzahl")}:</TableLabel>
              <TableValue>
                {formatNumber(locale, asset.canbus.engine_speed ?? 0)} {t("RPM")}
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Motortemperatur")}:</TableLabel>
              <TableValue>{asset.canbus.engine_temp}°</TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Motorstunden")}:</TableLabel>
              <TableValue>{formatNumber(locale, asset.canbus.engine_hours ?? 0)} h</TableValue>
            </tr>

            {(asset.canbus.truck_axle_weight_1 ||
              asset.canbus.truck_axle_weight_2 ||
              asset.canbus.truck_axle_weight_3 ||
              asset.canbus.truck_axle_weight_4 ||
              asset.canbus.truck_axle_weight_5) && (
              <>
                <tr>
                  <TableLabel colSpan={2} style={{paddingBottom: 2}}>
                    {t("Achslast")} (kg):
                  </TableLabel>
                </tr>
                <tr>
                  <TableValue colSpan={2} style={{padding: 0}}>
                    <p>
                      <AxleWeightLabel first>1: </AxleWeightLabel> {asset.canbus.truck_axle_weight_1}
                      <AxleWeightLabel>2: </AxleWeightLabel> {asset.canbus.truck_axle_weight_2}
                      <AxleWeightLabel>3: </AxleWeightLabel> {asset.canbus.truck_axle_weight_3}
                    </p>
                    <p>
                      <AxleWeightLabel first>4: </AxleWeightLabel> {asset.canbus.truck_axle_weight_4}
                      <AxleWeightLabel>5: </AxleWeightLabel> {asset.canbus.truck_axle_weight_5}
                    </p>
                  </TableValue>
                </tr>
              </>
            )}
          </tbody>
        </AreaDataTable>
      </Area>

      <Area areaWidth="216px">
        <AreaDataTable>
          <tbody>
            <tr>
              <TableLabel>{t("Kupplung")}:</TableLabel>
              <TableValue>
                <PedalStatus value={asset.canbus.clutch_pedal ? 100 : 0} />
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Bremse")}:</TableLabel>
              <TableValue>
                <PedalStatus value={asset.canbus.break_pedal ? 100 : 0} />
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Gaspedal")}:</TableLabel>
              <TableValue>
                <PedalStatus value={asset.canbus.acc_pedal ?? 0} showLabel />
              </TableValue>
            </tr>
            <tr>
              <TableLabel>{t("Tempomat")}:</TableLabel>
              <TableValue>
                <PedalStatus value={asset.canbus.cruise_control ? 100 : 0} />
              </TableValue>
            </tr>
          </tbody>
        </AreaDataTable>
      </Area>
      <AssetDataCharts asset={asset} />

      <StickButton aria-label={t("Anheften")} onClick={openOverlay} onMouseDown={handleMouseDown}>
        <span className="icon-ico_export" />
        <span>{t("Anheften")}</span>
      </StickButton>
    </Container>
  );
};

export default Overview;
