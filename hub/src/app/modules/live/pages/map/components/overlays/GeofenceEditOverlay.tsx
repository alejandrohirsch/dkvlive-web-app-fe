import React, {useCallback, useEffect, useState} from "react";
import {HereMap} from "../../Map";
import {Loading} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import {FormControlLabel} from "@material-ui/core";
import styled from "styled-components";
import Switch from "@material-ui/core/Switch";
import {AreaDataTable, TableLabel, TableValue} from "../details/Overview";
import {showOverlay} from "../../state/overlay/actions";
import {OVERLAY_GEOFENCE, OVERLAY_GEOFENCE_EDIT, OverlayData} from "../../state/overlay/reducer";
import {useDispatch} from "react-redux";

// -----------------------------------------------------------------------------------------------------------
const HeadlineEdit = styled.h1`
  font-size: 1.7142857142857142rem;
  font-weight: normal;
  padding: 24px 24px 0px 24px;
  color: #004b78;
`;
// background-color: #efefef;

const ZoneSpan = styled.span`
  color: #666666
  font-size: 16px;
  margin: 11px 0 0 8px;
`;

const DataTable = styled(AreaDataTable)`
  margin: 0 0 12px 0;
`;

const ActiveZone = styled.div`
  display: flex;
`;

const EditContainer = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 16px;
  margin: 0 22px 16px 22px;
`;

const DetailsContainer = styled.div`
  font-size: 16px;
  margin: 0 0 0 8px;
`;

const HorizontalLine = styled.div`
  height: 1px;
  background-color: #dbdbdb;
  margin: 22px 0;
`;

const StepHeader = styled.div`
  font-weight: bold;
  margin-bottom: 12px;
`;

const GeoButtonIcon = styled.div`
  width: 26px;
  height: 26px;
  border-radius: 50%;
  background-color: #efefef;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 12px;
  transition: color linear 0.1s, background-color linear 0.1s;

  span {
    color: #666666;
    font-size: 16px;
    transition: color linear 0.1s;
  }
`;

const GeoButton = styled.button`
  user-select: none;
  border: 0;
  outline: 0;
  cursor: pointer;
  overflow: hidden;
  font: inherit;
  background-color: transparent;
  display: flex;
  flex-direction: row;
  align-items: center;
  font-size: 15px;
  margin-top: 12px;
  color: #666666;
  transition: color linear 0.1s;

  &:hover,
  &:focus {
    color: #2e6b90;

    ${GeoButtonIcon} {
      background-color: #2e6b90;

      span {
        color: #fff;
      }
    }
  }
`;

const ActivateFormControlLabel = styled(FormControlLabel)`
  > .MuiTypography-body1 {
    font-family: inherit !important;
    color: var(--dkv-nav-primary-color);
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface GeofenceOverlayProps {
  hereMap: HereMap;
  data: OverlayData;
}

const GeofenceEditOverlay: React.FC<GeofenceOverlayProps> = ({hereMap: {map}, data}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const [loading] = useState(false);

  // set active
  const [active, setActive] = React.useState(true);
  const handleActiveChange = (_: React.ChangeEvent<unknown>, checked: boolean) => {
    setActive(checked);
  };

  // edit
  const showEdit = useCallback(() => {
    if (!data.data) {
      return;
    }

    const editData = {
      id: data.data.id,
      name: data.data.name,
      description: data.data.description,
    };
    dispatch(showOverlay(OVERLAY_GEOFENCE, "", editData));
  }, [dispatch, data]);

  useEffect(() => {
    if (!data.data) {
      return;
    }

    const lineString = new H.geo.LineString();
    const coords = data.data.polygon.coordinates[0][0];
    for (let i = 0; i < coords.length - 1; i = i + 2) {
      lineString.pushPoint({lat: coords[i], lng: coords[i + 1]});
    }

    const polygon = new H.map.Polygon(lineString, {
      style: {
        fillColor: "rgba(0, 75, 120, .25)",
        strokeColor: "rgb(0, 75, 120)",
        lineWidth: 3,
      },
    });

    let polyInfoBubble: any;

    const ui = new H.ui.UI(map, {});
    polygon.addEventListener("pointerenter", (ev: any) => {
      if (!data.data) {
        return;
      }
      map.getViewPort().element.style.cursor = "pointer";

      if (polyInfoBubble) {
        ui.removeBubble(polyInfoBubble);
        polyInfoBubble.dispose();
        polyInfoBubble = null;
      }

      const point = map.screenToGeo(ev.pointers[0].viewportX, ev.pointers[0].viewportY);
      polyInfoBubble = new H.ui.InfoBubble(point, {
        content: `${data.data.name} - ${data.data.description}`,
      });
      polyInfoBubble.addClass("dkv-map-asset-fencetooltipbubble");

      ui.addBubble(polyInfoBubble);
    });

    polygon.addEventListener("pointermove", (ev: any) => {
      const point = map.screenToGeo(ev.pointers[0].viewportX, ev.pointers[0].viewportY);
      if (polyInfoBubble) {
        polyInfoBubble.setPosition(point);
      }
    });

    polygon.addEventListener("pointerleave", () => {
      map.getViewPort().element.style.cursor = "auto";

      if (polyInfoBubble) {
        ui.removeBubble(polyInfoBubble);
        polyInfoBubble.dispose();
        polyInfoBubble = null;
      }
    });

    polygon.addEventListener("tap", () => {
      dispatch(showOverlay(OVERLAY_GEOFENCE_EDIT, "", data.data));
    });

    map.addObject(polygon);
    map.getViewPort().setPadding(100, 750, 325, 250);
    map.getViewModel().setLookAtData({
      bounds: polygon.getBoundingBox(),
    });
  }, [data.data, dispatch, map, setActive]);

  // render
  return (
    <section>
      <Loading inprogress={loading} />

      {data.data && (
        <>
          <HeadlineEdit>{data.data.name}</HeadlineEdit>
          <EditContainer>
            <ActiveZone>
              <ZoneSpan>{t("Geozone aktiv seit 21.06.2020")}</ZoneSpan>
              <div style={{flexGrow: 1}} />
              <ActivateFormControlLabel
                control={<Switch onChange={handleActiveChange} value={active} color="default" />}
                label={t("Aktiv")}
                labelPlacement={"start"}
              />
            </ActiveZone>
            <div style={{display: "flex"}}>
              <GeoButton onClick={showEdit}>
                <GeoButtonIcon>
                  <span className="icon-ico_edit" />
                </GeoButtonIcon>
                <span>{t("Aktuelle Geozone bearbeiten")}</span>
              </GeoButton>
            </div>
            <HorizontalLine />
            <DetailsContainer>
              <StepHeader>{t("Details")}</StepHeader>
              <DataTable>
                <tbody>
                  <tr>
                    <TableLabel>{t("Beschreibung")}:</TableLabel>
                    <TableValue>{data.data.description}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Meldung")}:</TableLabel>
                    <TableValue>Alarm</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Zeitplan")}:</TableLabel>
                    <TableValue>Mo Di Mi Do / 08:00 - 16:00</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Fahrzeuge")}:</TableLabel>
                    <TableValue>Alle</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Öffentlich")}:</TableLabel>
                    <TableValue>Ja</TableValue>
                  </tr>
                </tbody>
              </DataTable>
            </DetailsContainer>
          </EditContainer>
        </>
      )}
    </section>
  );
};

export default GeofenceEditOverlay;
