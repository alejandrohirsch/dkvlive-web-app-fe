import React from "react";
import styled, {css} from "styled-components";
import {Asset} from "../../../state/assets/reducer";
import {Pause as PauseIcon} from "@styled-icons/boxicons-regular/Pause";
import {Stop as StopIcon} from "@styled-icons/boxicons-regular/Stop";
import {LocationArrow as LocationArrowIcon} from "@styled-icons/fa-solid/LocationArrow";

// -----------------------------------------------------------------------------------------------------------
interface AssetItemDetailsStatusProps {
  status: string;
  size: number;
}

const AssetItemDetailsStatus = styled.div<AssetItemDetailsStatusProps>`
  margin-top: 3px;

  border-radius: 50%;
  background-color: var(--dkv-highlight-error-color);
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
  flex-shrink: 0;

  ${(props) => css`
    height: ${props.size}px;
    width: ${props.size}px;
  `}

  ${(props) => {
    if (props.status === "online") {
      return css`
        background-color: var(--dkv-highlight-ok-color);
      `;
    }

    if (props.status === "standby") {
      return css`
        background-color: var(--dkv-highlight-warning-color);
      `;
    }

    return null;
  }}
`;

// -----------------------------------------------------------------------------------------------------------
interface AssetStatusProps {
  asset: Asset;
  size?: number;
}

const AssetStatus: React.FC<AssetStatusProps> = ({asset, size = 16}) => {
  return (
    <AssetItemDetailsStatus size={size} status={asset._status ?? "offline"}>
      {asset._status === "offline" && <StopIcon size={size} />}
      {asset._status === "standby" && <PauseIcon size={size} />}
      {asset._status === "online" && (
        <LocationArrowIcon size={size / 2} style={{transform: `rotate(${-45 + asset.gnss.track!}deg)`}} />
      )}
    </AssetItemDetailsStatus>
  );
};

export default AssetStatus;
