import React, {useEffect, useCallback, useState} from "react";
import {HereMap} from "../Map";
import styled, {css} from "styled-components/macro";
import {useSelector, useDispatch} from "react-redux";
import {detailsSelector} from "../state/details/selectors";
import {assetSelector} from "../../../state/assets/selectors";
import {changeDetailsTab} from "../state/details/actions";
import {useTranslation} from "react-i18next";
import {Tabs, Tab} from "dkv-live-frontend-ui";
import {TAB_OVERVIEW, TAB_DRIVINGTIME} from "../state/details/reducer";
import Overview from "./details/Overview";
import DrivingTime from "./details/DrivingTime";
import {overlaySelector} from "../state/overlay/selectors";

// -----------------------------------------------------------------------------------------------------------
interface ContainerProps {
  open: boolean;
}

const Container = styled.div<ContainerProps>`
  position: absolute;
  bottom: 0;
  right: 16px;
  left: 16px;
  color: var(--dkv-text-primary-color);
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  line-height: 1.25;
  height: 250px;
  z-index: 2;
  display: flex;
  flex-direction: column;
  transition: height 0.1s linear;

  ${(props) =>
    !props.open &&
    css`
      height: 0;

      div {
        display: none;
      }

      &:before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: var(--dkv-background-primary-hover-color);
        z-index: 2;
      }
    `}
`;

const Box = styled.div`
  background-color: var(--dkv-background-primary-color);
  width: 100%;
`;

const HeaderContainer = styled.div`
  display: flex;
  flex-flow: row nowrap;

  ${Box} {
    background-color: var(--dkv-background-primary-hover-color);
  }
`;

const InfoBox = styled(Box)`
  white-space: nowrap;
  display: flex;
  align-items: center;
  height: 45px;
  position: relative;
  padding-right: 24px;
`;

const HeadlineContainer = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
  flex-shrink: 0;
  padding: 16px 24px;
`;

const Headline = styled.span`
  font-size: 1.7142857142857142em;
  color: var(--dkv-link-primary-color);
`;

const LicensePlate = styled.span`
  font-size: 1.4285714285714286em;
  line-height: 1.4;
  margin-left: 40px;
  font-weight: 500;
`;

const ContentContainer = styled(Box)`
  border-top: 1px solid #dbdbdb;
  height: 100%;
  display: flex;
  overflow: hidden;
`;

const ToggleButton = styled.button`
  position: absolute;
  top: -23px;
  left: 0;
  right: 0;
  margin: auto;
  background-color: var(--dkv-link-primary-color);
  color: #fff;
  width: 50px;
  height: 22px;
  transition: background-color linear 0.1s;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  cursor: pointer;

  &:hover,
  &:focus {
    background-color: var(--dkv-link-primary-hover-color);
  }

  span {
    font-size: 14px;
  }
`;

const TabsContainer = styled.div`
  overflow-x: auto;
  margin-left: auto;
  align-self: flex-end;
  margin-bottom: 2px;
`;

const DetailsTabs = styled(Tabs)`
  align-items: flex-end;

  .MuiTabs-fixed {
    height: 36px;
  }

  .MuiButtonBase-root {
    height: 36px;
    min-height: 36px;
  }
`;

// -----------------------------------------------------------------------------------------------------------
const tabsIndex = {
  [TAB_OVERVIEW]: 0,
  [TAB_DRIVINGTIME]: 1,
};

const tabsOrder = Object.keys(tabsIndex).reduce((order, key) => {
  order[tabsIndex[key]] = key;
  return order;
}, {});

interface AssetDetailsProps {
  hereMap: HereMap;
}

const AssetDetails: React.FC<AssetDetailsProps> = ({hereMap: {map}}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // overlay data
  const overlay = useSelector(overlaySelector);

  // data
  const {assetID, tab, dataOverlayVisible} = useSelector(detailsSelector);
  const asset = useSelector(assetSelector(assetID || ""));

  console.log(`details:`, tab, asset); // nocheckin

  // modify map padding
  useEffect(() => {
    if (!overlay.visible) {
      if (asset) {
        map.getViewPort().setPadding(0, 0, 225, 0);
      } else {
        map.getViewPort().setPadding(0, 0, 0, 0);
      }
    }
  }, [map, asset, overlay]);

  // open
  const [open, setOpen] = useState(true);

  const toggle = useCallback(() => {
    setOpen((o) => !o);
  }, []);

  // tab
  const switchTab = useCallback(
    (_: any, newTabIndex: number) => {
      dispatch(changeDetailsTab(tabsOrder[newTabIndex] || TAB_OVERVIEW));
    },
    [dispatch]
  );

  // render
  if (asset === undefined) {
    return null;
  }

  return (
    <Container open={open}>
      <HeaderContainer>
        <InfoBox>
          <HeadlineContainer>
            <Headline>{t("Livedaten")}</Headline> <LicensePlate>{asset._displayName}</LicensePlate>
          </HeadlineContainer>

          <TabsContainer>
            <DetailsTabs value={tabsIndex[tab || TAB_OVERVIEW]} onChange={switchTab}>
              <Tab label={t("Übersicht")} />
              <Tab label={t("Lenk- u. Ruhezeiten")} />
              {/* <Tab label={t("Fahrten")} /> */}
              {/* <Tab label={t("Meldungen")} /> */} {/* @todo commingsoon */}
            </DetailsTabs>
          </TabsContainer>
        </InfoBox>
      </HeaderContainer>
      <ContentContainer>
        {tab === TAB_OVERVIEW && <Overview asset={asset} dataOverlayVisible={dataOverlayVisible} />}
        {tab === TAB_DRIVINGTIME && <DrivingTime asset={asset} />}
      </ContentContainer>

      <ToggleButton aria-label={t("Schließen")} onClick={toggle}>
        {open && <span className="icon-ico_chevron-down" />}
        {!open && <span className="icon-ico_chevron-up" />}
      </ToggleButton>
    </Container>
  );
};

export default AssetDetails;
