import React, {useState, useCallback} from "react";
import {Asset} from "../../../../state/assets/reducer";
import SpeedChart from "./charts/SpeedChart";
import FuelChart from "./charts/FuelChart";
import HeightChart from "./charts/HeightChart";
import {Tabs as UITabs, Tab as UITab} from "dkv-live-frontend-ui";
import styled from "styled-components";
import {useTranslation} from "react-i18next";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  min-width: 385px;
`;

const TabsContainer = styled.div``;

const Tabs = styled(UITabs)`
  height: 32px !important;
  min-height: 32px !important;
`;

const Tab = styled(UITab)`
  font-size: 12px !important;
  min-height: 32px !important;
  padding: 2px 4px !important;
`;

// -----------------------------------------------------------------------------------------------------------
interface AssetDataChartsProps {
  asset: Asset;
}

const AssetDataCharts: React.FC<AssetDataChartsProps> = ({asset}) => {
  const {t} = useTranslation();

  // tab
  const [tab, setTab] = useState(0);

  const switchTab = useCallback(
    (_: any, newTabIndex: number) => {
      setTab(newTabIndex);
    },
    [setTab]
  );

  // render
  return (
    <Container>
      <TabsContainer>
        <Tabs value={tab} onChange={switchTab} variant="scrollable">
          <Tab label={t("Geschwindigkeit")} />
          <Tab label={t("Tankinhalt")} />
          <Tab label={t("Höhe")} />
        </Tabs>
      </TabsContainer>

      {tab === 0 && <SpeedChart asset={asset} />}
      {tab === 1 && <FuelChart asset={asset} />}
      {tab === 2 && <HeightChart asset={asset} />}
    </Container>
  );
};

export default AssetDataCharts;
