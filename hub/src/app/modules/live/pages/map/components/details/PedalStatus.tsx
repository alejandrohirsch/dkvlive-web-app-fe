import React from "react";
import styled from "styled-components";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  width: 100px;
  background-color: #efefef;
  border-radius: 5px;
  height: 7px;
  position: relative;
`;

const PedalValue = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  border-radius: 5px;
  height: 7px;
  background-color: #2e6b90;
`;

const PedalLabel = styled.span`
  position: absolute;
  left: 0;
  right: 0;
  text-align: center;
  top: -22px;
  color: var(--dkv-link-primary-color);
  font-size: 1rem;
  font-weight: 500;
`;

// -----------------------------------------------------------------------------------------------------------
interface PedalStatusProps {
  value: number;
  showLabel?: boolean;
}

const PedalStatus: React.FC<PedalStatusProps> = ({value, showLabel}) => {
  return (
    <Container>
      {showLabel && <PedalLabel>{value.toFixed(0)}%</PedalLabel>}

      <PedalValue style={{width: `${value}%`}} />
    </Container>
  );
};

export default PedalStatus;
