import React, {useCallback, useState, useRef} from "react";
import styled, {css} from "styled-components";
import {useTranslation} from "react-i18next";
import {IconButton, Popover} from "@material-ui/core";
import AssetList from "./AssetList";
import SidebarFilter from "./SidebarFilter";
import {useSelector} from "react-redux";
import {mapFilterActiveSelector} from "app/modules/live/state/assets/selectors";

// -----------------------------------------------------------------------------------------------------------
interface ContainerProps {
  open: boolean;
}

const Container = styled.div<ContainerProps>`
  display: flex;
  flex-direction: column;
  flex-shrink: 0;
  height: 100%;
  background-color: var(--dkv-background-primary-color);
  z-index: 3000;
  border-top: 2px solid var(--dkv-background-secondary-color);
  border-left: 2px solid var(--dkv-background-secondary-color);
  width: 402px;
  position: relative;
  transition: width 0.1s linear;

  ${(props) =>
    !props.open &&
    css`
      width: 0px;

      div {
        display: none;
      }

      &:before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: white;
        z-index: 2;
      }
    `}
`;

const FilterButton = styled(IconButton)`
  flex-shrink: 0 !important;
  margin-left: 8px !important;
  cursor: pointer !important;

  span {
    font-size: 28px;
    color: #666666;
  }
`;

interface SelectionContainerProps {
  filterActive?: boolean;
}

const SelectionContainer = styled.div<SelectionContainerProps>`
  flex-shrink: 0;
  height: 72px;
  border-bottom: 2px solid var(--dkv-background-secondary-color);
  padding: 0 20px;
  display: flex;
  align-items: center;

  ${(props) =>
    props.filterActive &&
    css`
      ${FilterButton} {
        span {
          color: var(--dkv-highlight-primary-color);
        }
      }
    `}
`;

const ToggleButton = styled.button<ContainerProps>`
  position: absolute;
  top: 128px;
  left: 401px;
  background-color: var(--dkv-link-primary-color);
  color: #fff;
  width: 22px;
  height: 50px;
  transition: background-color linear 0.1s;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  cursor: pointer;

  &:hover,
  &:focus {
    background-color: var(--dkv-link-primary-hover-color);
  }

  span {
    font-size: 14px;
  }

  ${(props) =>
    !props.open &&
    css`
      left: 1px;
    `}
`;

const SelectButton = styled.button`
  width: 100%;
  transition: background-color linear 0.1s;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  display: flex;
  align-items: center;
  background: none;

  span:first-of-type {
    width: 100%;
    display: inline-block;
    color: #323232;
    font-size: 1.4285714285714286rem;
    text-align: left;
    font-weight: 500;
  }

  span:last-of-type {
    font-size: 24px;
    color: #666666;
  }
`;

// -----------------------------------------------------------------------------------------------------------
const Sidebar = React.memo(() => {
  const {t} = useTranslation();

  // open
  const [open, setOpen] = useState(true);

  const toggle = useCallback(() => {
    setOpen((o) => !o);

    setTimeout(() => {
      window.dispatchEvent(new Event("dkv-resize"));
    }, 300);
  }, []);

  // filter popover
  const filterActive = useSelector(mapFilterActiveSelector);

  const searchContainerRef = useRef<HTMLDivElement>(null);
  const [filterOpen, setFilterOpen] = useState(false);

  const toggleFilter = () => setFilterOpen((o) => !o);

  // render
  return (
    <Container open={open}>
      <SelectionContainer ref={searchContainerRef} filterActive={filterActive}>
        <SelectButton disabled>
          <span>{t("Fahrzeuge")}</span>
          {/* <span className="icon-ico_trend_down" /> */}
        </SelectButton>
        <FilterButton onClick={toggleFilter}>
          <span className="icon-ico_abstract-filter" />
        </FilterButton>

        <Popover
          anchorEl={searchContainerRef.current}
          open={filterOpen}
          onClose={toggleFilter}
          classes={{
            paper: "dkv-filter-popover",
          }}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "left",
          }}
        >
          <SidebarFilter />
        </Popover>
      </SelectionContainer>

      <AssetList filterOpen={filterOpen} />

      <ToggleButton open={open} aria-label={t("sidebar schließen")} onClick={toggle}>
        {open && <span className="icon-ico_chevron-left" />}
        {!open && <span className="icon-ico_chevron-right" />}
      </ToggleButton>
    </Container>
  );
});

export default Sidebar;
