import React, {useCallback, useEffect, useRef} from "react";
import styled from "styled-components";
import {
  ChevronLeft as ChevronLeftIcon,
  ChevronRight as ChevronRightIcon,
} from "@styled-icons/boxicons-regular";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex-shrink: 0;
  margin: 16px 0;
  padding: 0 30px 0;
  width: 100%;
`;

const Slider = styled.div`
  width: 100%;
  height: 7px;
  background-color: #ccc;
  border-radius: 5px;
  position: relative;
  margin: 20px 0;
`;

const Bar = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  border-radius: 5px;
  height: 7px;
  background-color: var(--dkv-highlight-primary-color);
`;

const ThumbMin = styled.div`
  position: absolute;
  top: -7px;
  cursor: pointer;
  border-radius: 20px;
  width: 20px;
  height: 20px;
  background-color: #fff;
  border: 1px solid var(--dkv-text-primary-color);
  z-index: 8;
`;

const ThumbMax = styled.div`
  width: 95px;
  padding: 3px;
  border-radius: 20px;
  border: 1px solid var(--dkv-text-primary-color);
  position: absolute;
  top: -9px;
  cursor: pointer;
  text-align: center;
  user-select: none;
  background-color: #fff;
  z-index: 9;
  line-height: 1;
`;

const RangeContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 0 0 16px;
`;

const RangeLabelMin = styled.div`
  color: var(--dkv-text-caption-color);
  position: absolute;
  text-align: center;
`;

const RangeLabelMax = styled.div`
  color: var(--dkv-text-caption-color);
  position: absolute;
  right: 30px;
`;

const ValueLabel = styled.span`
  line-height: 1;
`;

// -----------------------------------------------------------------------------------------------------------
function getValue(value: number, max: number, end: number) {
  return Math.round((max * value) / end);
}

// -----------------------------------------------------------------------------------------------------------

interface FuelStationSliderProps {
  min: number;
  max: number;
  step: number;
  onChange: (minDistance: number, maxDistance: number) => void;
}

const FuelStationSlider: React.FC<FuelStationSliderProps> = ({min, max, step, onChange}) => {
  const sliderRef = useRef<HTMLDivElement>(null);
  const thumbMinRef = useRef<HTMLDivElement>(null);
  const thumbMaxRef = useRef<HTMLDivElement>(null);
  const valueRef = useRef<HTMLSpanElement>(null);
  const barRef = useRef<HTMLDivElement>(null);
  const labelMinRef = useRef<HTMLDivElement>(null);
  const minX = useRef(0);
  const maxX = useRef(0);
  const minDistance = useRef(0);
  const maxDistance = useRef(0);
  let minDiff = 0;
  let maxDiff = 0;

  const updateBar = (minX: number, maxX: number, offset: number) => {
    if (barRef.current !== null) {
      barRef.current.style.left = `${minX > 0 ? minX + offset : 0}px`;
      barRef.current.style.width = `${maxX - minX + offset}px`;
    }
  };

  const updateThumb = (thumb: any, value: number) => {
    if (thumb.current !== null) {
      thumb.current.style.left = `calc(${value}%)`;
    }
  };

  const updateLabelMin = () => {
    if (labelMinRef.current !== null) {
      const minLabelWidth = labelMinRef.current.offsetWidth / 2;
      const startPosition = 30;
      labelMinRef.current.style.left = `${
        minX.current >= minLabelWidth ? minX.current + minLabelWidth : startPosition
      }px`;
      labelMinRef.current.textContent = minDistance.current.toString() + " km";
    }
  };

  const setMinSlider = (value: number) => {
    if (sliderRef.current === null || thumbMinRef.current === null || thumbMaxRef.current === null) {
      return;
    }

    const x = value - minDiff - sliderRef.current.getBoundingClientRect().left;
    if (x >= maxX.current - 20) {
      return;
    }
    minX.current = x;
    const start = 0;
    const end = sliderRef.current.offsetWidth;

    if (minX.current < start) {
      minX.current = 0;
    }

    const thumbMinWidth = thumbMinRef.current.offsetWidth;
    const thumbMaxWidth = thumbMaxRef.current.offsetWidth;
    if (minX.current + thumbMinWidth > end) {
      minX.current = end - thumbMinWidth;
    }

    let distance = getValue(minX.current, max, end - thumbMinWidth - thumbMaxWidth);
    if (distance >= max) {
      distance = 99;
    }
    const percentage = getValue(minX.current, 100, end);

    minDistance.current = distance;

    if (minDistance.current % step === 0) {
      updateBar(minX.current, maxX.current, thumbMinRef.current.offsetWidth / 2);
      updateLabelMin();
      updateThumb(thumbMinRef, percentage);
    }
  };

  const setMaxSlider = (value: number) => {
    if (
      sliderRef.current === null ||
      thumbMaxRef.current === null ||
      valueRef.current === null ||
      thumbMinRef.current === null
    ) {
      return;
    }

    const x = value - maxDiff - sliderRef.current.getBoundingClientRect().left;
    if (x - 20 <= minX.current) {
      return;
    }
    maxX.current = x;
    const end = sliderRef.current.offsetWidth;
    const start = 0;

    if (maxX.current < start) {
      maxX.current = 0;
    }

    const thumbWidth = thumbMaxRef.current.offsetWidth;
    if (maxX.current + thumbWidth > end) {
      maxX.current = end - thumbWidth;
    }

    let distance = getValue(maxX.current, max, end - thumbWidth);
    if (distance <= 10) {
      distance = 10;
    }
    const percentage = getValue(maxX.current, 100, end);

    maxDistance.current = distance;

    if (maxDistance.current % step === 0) {
      valueRef.current.textContent = maxDistance.current.toString();
      updateBar(minX.current, maxX.current, thumbMinRef.current.offsetWidth / 2);
      updateThumb(thumbMaxRef, percentage + 1);
    }
  };

  const handleThumbMinMouseMove = (e: MouseEvent) => {
    setMinSlider(e.clientX);
  };

  const handleThumbMinMouseUp = () => {
    document.removeEventListener("mouseup", handleThumbMinMouseUp);
    document.removeEventListener("mousemove", handleThumbMinMouseMove);

    onChange(minDistance.current, maxDistance.current);
  };

  const handleThumbMinMouseDown = (e: React.MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
    document.addEventListener("mousemove", handleThumbMinMouseMove);
    document.addEventListener("mouseup", handleThumbMinMouseUp);

    const thumbPos = thumbMinRef.current ? thumbMinRef.current.getBoundingClientRect().left : 0;
    minDiff = e.clientX - thumbPos;
  };

  const handleThumbMaxMouseUp = () => {
    document.removeEventListener("mouseup", handleThumbMaxMouseUp);
    document.removeEventListener("mousemove", handleThumbMaxMouseMove);

    onChange(minDistance.current, maxDistance.current);
  };

  const handleThumbMaxMouseDown = (e: React.MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
    document.addEventListener("mousemove", handleThumbMaxMouseMove);
    document.addEventListener("mouseup", handleThumbMaxMouseUp);

    const thumbPos = thumbMaxRef.current ? thumbMaxRef.current.getBoundingClientRect().left : 0;
    maxDiff = e.clientX - thumbPos;
  };

  const handleThumbMaxMouseMove = (e: MouseEvent) => {
    setMaxSlider(e.clientX);
  };

  const setMaxDefaultValue = useCallback((value: number) => {
    if (
      sliderRef.current === null ||
      thumbMaxRef.current === null ||
      thumbMinRef.current === null ||
      valueRef.current === null
    ) {
      return;
    }
    valueRef.current.textContent = value.toString();
    maxX.current = sliderRef.current.offsetWidth - value - thumbMaxRef.current.offsetWidth * 2;
    maxDistance.current = value;
    updateBar(minX.current, maxX.current, thumbMinRef.current.offsetWidth / 2);
    updateThumb(
      thumbMaxRef,
      value - getValue(thumbMaxRef.current.offsetWidth / 2, 100, sliderRef.current.offsetWidth)
    );
  }, []);

  useEffect(() => {
    setMaxDefaultValue(50);
  }, [setMaxDefaultValue]);

  // render
  return (
    <Container>
      <RangeContainer>
        <RangeLabelMin ref={labelMinRef}>{min} km</RangeLabelMin>
        <RangeLabelMax>{max} km</RangeLabelMax>
      </RangeContainer>
      <Slider ref={sliderRef}>
        <ThumbMin ref={thumbMinRef} onMouseDown={handleThumbMinMouseDown} onMouseUp={handleThumbMinMouseUp} />
        <ThumbMax ref={thumbMaxRef} onMouseDown={handleThumbMaxMouseDown} onMouseUp={handleThumbMaxMouseUp}>
          <ChevronLeftIcon size="18" />
          <ValueLabel ref={valueRef}>0</ValueLabel> km
          <ChevronRightIcon size="18" />
        </ThumbMax>
        <Bar ref={barRef} />
      </Slider>
    </Container>
  );
};

export default FuelStationSlider;
