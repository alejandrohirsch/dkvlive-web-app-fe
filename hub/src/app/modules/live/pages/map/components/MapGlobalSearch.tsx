import React, {useEffect, useRef} from "react";
import {HereMap} from "../Map";
import FuelStationMarkerIcon from "../../../../../../assets/poi/dkv-fuel-selected.svg";
import ServiceStationMarkerIcon from "../../../../../../assets/poi/dkv-service-selected.svg";

function createMarkerPlaceIcon() {
  const div = document.createElement("div");
  div.classList.add("dkv-map-location-icon");
  div.classList.add("icon-ico_route-active");

  return new H.map.DomIcon(div);
}

function createMarkerStationIcon(icon: string) {
  const div = document.createElement("img");
  div.classList.add("dkv-map-poi-icon");
  div.src = icon;

  return new H.map.DomIcon(div);
}

function createMarkerPlace(data: any) {
  const pos = {lat: data.latitude, lng: data.longitude};
  const marker = new H.map.DomMarker(pos, {icon: createMarkerPlaceIcon()});

  marker.setData({
    data: data,
  });

  return marker;
}

function createMarkerStation(data: any) {
  const pos = {lat: data.latitude, lng: data.longitude};
  const icon = data.category === "FUEL" ? FuelStationMarkerIcon : ServiceStationMarkerIcon;
  const marker = new H.map.DomMarker(pos, {icon: createMarkerStationIcon(icon)});

  marker.setData({
    data: data,
  });

  marker.addEventListener("tap", (e: MouseEvent) => {
    e.preventDefault();
    e.stopPropagation();
  });

  return marker;
}

// -----------------------------------------------------------------------------------------------------------
interface MapGlobalSearchProps {
  hereMap: HereMap;
}

const MapGlobalSearch: React.FC<MapGlobalSearchProps> = ({hereMap: {map, ui}}) => {
  const markerRef = useRef(null);

  // global event listener
  useEffect(() => {
    const removeMarker = () => {
      if (markerRef.current !== null) {
        map.removeObject(markerRef.current);
        markerRef.current = null;
      }
    };

    const onShowPlace = (e: CustomEventInit<any>) => {
      removeMarker();
      const data = e.detail?.place;
      const marker = createMarkerPlace({
        latitude: data.displayPosition.latitude,
        longitude: data.displayPosition.longitude,
      });
      markerRef.current = marker;
      map.addObject(marker);
      map.setCenter(marker.getGeometry());
      map.setZoom(17);
    };

    const onShowStation = (e: CustomEventInit<any>) => {
      removeMarker();
      const data = e.detail?.station;
      const marker = createMarkerStation({
        category: data.category,
        latitude: data.location?.coordinates[1],
        longitude: data.location?.coordinates[0],
      });
      markerRef.current = marker;
      map.addObject(marker);
      map.setCenter(marker.getGeometry());
      map.setZoom(17);
    };

    window.addEventListener("dkv-map-show-place", onShowPlace);
    window.addEventListener("dkv-map-show-station", onShowStation);

    return () => {
      removeMarker();
      window.removeEventListener("dkv-map-show-place", onShowPlace);
      window.removeEventListener("dkv-map-show-station", onShowStation);
    };
  }, [map, ui]);

  return null;
};

export default MapGlobalSearch;
