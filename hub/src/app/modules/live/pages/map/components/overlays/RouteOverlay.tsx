import React, {useState, useEffect, useRef, useCallback} from "react";
import {HereMap} from "../../Map";
import {OverlayData} from "../../state/overlay/reducer";
import {useSelector} from "react-redux";
import {assetSelector} from "../../../../state/assets/selectors";
import {Headline} from "../MapOverlay";
import {useTranslation} from "react-i18next";
import styled from "styled-components";
import {debounce, TextField, Loading, formatSeconds, formatNumber, Message} from "dkv-live-frontend-ui";
import Autocomplete from "@material-ui/lab/Autocomplete";
import axios from "axios";
import config from "config";
import {resolveUserLanguage} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const FormContainer = styled.div`
  display: flex;
  flex-flow: row nowrap;
  margin-top: 8px;
  padding: 0 24px 16px 24px;

  .MuiFormControl-root {
    margin: 8px 0;
  }
`;

const Form = styled.div`
  width: 100%;
`;

const FormIcons = styled.div`
  flex-shrink: 0;
  display: flex;
  flex-direction: column;
  width: 20px;
  margin: 14px 12px 14px 0;
  align-items: center;
  justify-content: space-between;
`;

const FromIcon = styled.div`
  position: relative !important;
  width: 20px !important;
  height: 20px !important;
  margin-top: 16px;
`;

const ToIcon = styled.div`
  width: 24px;
  height: 24px;
  font-size: 24px;
  color: #ee8500;
`;

const RouteInformationContainer = styled.div`
  background-color: #f4faff;
  padding: 25px 30px;

  h2 {
    font-size: 1.1428571428571428em;
    font-weight: bold;
    color: #004b78;
  }
`;

const RouteInformationList = styled.ul`
  margin-top: 16px;

  li {
    margin-bottom: 6px;
  }
`;

const RouteInformationValue = styled.span`
  color: #004b78;
  font-weight: bold;
`;

// -----------------------------------------------------------------------------------------------------------
let targetMarker: any;

function createMarkerIcon() {
  const div = document.createElement("div");
  div.classList.add("dkv-map-location-icon");
  div.classList.add("icon-ico_route-active");

  return new H.map.DomIcon(div);
}

function showMarker(map: any, data: any) {
  hideMarker(map);

  const pos = {lat: data.latitude, lng: data.longitude};
  targetMarker = new H.map.DomMarker(pos, {icon: createMarkerIcon()});
  map.addObject(targetMarker);
}

function hideMarker(map: any) {
  if (!targetMarker) {
    return;
  }

  map.removeObjects([targetMarker]);
  targetMarker = null;
}

// -----------------------------------------------------------------------------------------------------------
interface RouteOverlayProps {
  hereMap: HereMap;
  data: OverlayData;
}

interface Maneuver {
  position: {
    latitude: 47.56664;
    longitude: 12.15097;
  };
  instruction: string;
  travelTime: number;
  length: number;
  id: string;
}

interface RouteData {
  summary: {travelTime: number; distance: number; baseTime: number; trafficTime: number};
  cost: {
    totalCost: string;
    currency: string;
    details: {
      driverCost: string;
      vehicleCost: string;
      tollCost: string;
    };
  };
  maneuvers: Maneuver[];
}

const RouteOverlay: React.FC<RouteOverlayProps> = ({hereMap: {map}, data}) => {
  const {t} = useTranslation();
  const [loading, setLoading] = useState(false);

  const [error, setError] = useState("");

  // asset
  const asset = useSelector(assetSelector(data.assetID ?? ""));

  // route
  const polylineRef = useRef(null);
  const [routeData, setRouteData] = useState<RouteData | null>(null);

  const handleSelection = useCallback(
    (_: any, value: any) => {
      const hideData = () => {
        setRouteData(null);
        setOptionValue("");
        hideMarker(map);

        if (polylineRef.current) {
          map.removeObject(polylineRef.current);
          polylineRef.current = null;
        }
      };

      const calculateRoute = async () => {
        // load target location
        let target;
        try {
          const res = await axios.get(
            `https://geocoder.ls.hereapi.com/6.2/geocode.json?locationid=${value.locationId}&jsonattributes=1&gen=9&apiKey=${config.HERE_API_KEY}`
          );

          const position = res?.data.response?.view[0]?.result[0]?.location?.displayPosition;
          if (!position) {
            setError("Das Reiseziel ist ungültig.");
            setLoading(false);
            return;
          }

          target = position;
        } catch (err) {
          setError("Das Reiseziel ist ungültig.");
          setLoading(false);
          console.error(err);
          return;
        }

        // calculate route
        const calculateRouteParams = {
          waypoint0: `${asset?.gnss.latitude},${asset?.gnss.longitude}`,
          waypoint1: `${target.latitude},${target.longitude}`,
          representation: "overview",
          legAttributes: "-li,mn,sh",
          linkAttributes: "none",
          routeAttributes: "sm",
          metricSystem: "metric",
          language: "de-de",
          currency: "EUR",

          mode: "fastest;car;traffic:enabled", // @todo: switch to truck

          emissionType: "6",
          fuelType: "diesel",

          vehicleNumberAxles: "3",
          tiresCount: "10",
          height: "400cm",
          length: "1000cm",
          width: "255cm",
          limitedWeight: "40000kg",

          tollVehicleType: "3",
          tollPass: "transponder",

          trailersCount: "1",
          trailerType: "2",
          trailerNumberAxles: "2",
          trailerHeight: "400cm",
        };

        let route;
        try {
          const query = Object.keys(calculateRouteParams)
            .map((k) => encodeURIComponent(k) + "=" + encodeURIComponent(calculateRouteParams[k]))
            .join("&");

          const res = await axios.get(
            `https://fleet.ls.hereapi.com/2/calculateroute.json?apiKey=${config.HERE_API_KEY}&${query}`
          );

          route = res?.data.response?.route[0];
        } catch (err) {
          setError("Route konnte nicht berechnet werden. Bitte wählen Sie ein anderes Reiseziel.");
          setLoading(false);
          console.error(err, err.response);
          return;
        }

        if (!route) {
          setError("Route konnte nicht berechnet werden. Bitte wählen Sie ein anderes Reiseziel.");
          hideData();
          setLoading(false);
          return;
        }

        // resolve map data
        const lineString = new H.geo.LineString();
        const maneuvers: Maneuver[] = [];
        route.leg.forEach((leg: any) => {
          const l = leg.shape.length;
          for (let i = 0; i < l; i += 2) {
            lineString.pushLatLngAlt(leg.shape[i], leg.shape[i + 1], 0);
          }

          leg.maneuver.forEach((maneuver: any) => maneuvers.push(maneuver));
        });

        const polyline = new H.map.Polyline(lineString, {
          style: {
            lineWidth: 5,
            strokeColor: "#004b78",
          },
        });

        map.addObject(polyline);
        map.getViewModel().setLookAtData({
          bounds: polyline.getBoundingBox(),
        });
        setTimeout(() => {
          map.setZoom(map.getZoom() - 1);
        });

        showMarker(map, target);
        polylineRef.current = polyline;

        // set route data
        setRouteData({
          summary: route.summary,
          cost: route.cost,
          maneuvers: maneuvers,
        });
        setLoading(false);
      };

      hideData();

      if (value) {
        setOptionValue(value.label);
        setLoading(true);
        setError("");
        calculateRoute();
      }
    },
    [asset, map]
  );

  // remove from map on unmount
  useEffect(() => {
    return () => {
      hideMarker(map);

      if (!polylineRef.current) {
        return;
      }

      map.removeObject(polylineRef.current);
      polylineRef.current = null;
    };
  }, [map]);

  // suggest
  const [inputValue, setInputValue] = useState("");
  const [optionValue, setOptionValue] = useState("");
  const [options, setOptions] = useState<any[]>([]);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
  };

  const fetch = React.useMemo(
    () =>
      debounce(async (value: string, callback: (results?: any[]) => void) => {
        try {
          const res = await axios.get(
            `https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json?apiKey=${config.HERE_API_KEY}&query=${value}`
          );

          const suggestions = res?.data?.suggestions;
          if (!suggestions) {
            callback([]);
            return;
          }

          callback(suggestions);
        } catch (err) {
          //@todo: show growl
          console.error(err);
          callback([]);
        }
      }, 300),
    []
  );

  useEffect(() => {
    let active = true;

    if (inputValue === "") {
      setOptions([]);
      return undefined;
    }

    fetch(inputValue, (results?: any[]) => {
      if (active) {
        setOptions(results || []);
      }
    });

    return () => {
      active = false;
    };
  }, [inputValue, fetch]);

  // route target event
  useEffect(() => {
    const onTarget = (e: any) => {
      setOptions([]);
      handleSelection(null, e.detail);
    };

    window.addEventListener("dkv-route-target", onTarget);

    return () => window.removeEventListener("dkv-route-target", onTarget);
  }, [handleSelection]);

  // render
  const locale = resolveUserLanguage();

  return (
    <section>
      <Loading inprogress={loading} />

      <Headline>{t("Route planen für {{.licensePlate}}", {licensePlate: asset?.license_plate})}</Headline>

      {error && <Message style={{margin: "0 24px 24px 24px"}} text={t(error)} error />}

      <FormContainer>
        <FormIcons>
          <FromIcon className="dkv-asset-icondiv dkv-asset-icondiv--online">
            <svg
              viewBox="0 0 512 512"
              height="10"
              width="10"
              aria-hidden="true"
              focusable="false"
              fill="currentColor"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fill="currentColor"
                d="M444.52 3.52L28.74 195.42c-47.97 22.39-31.98 92.75 19.19 92.75h175.91v175.91c0 51.17 70.36 67.17 92.75 19.19l191.9-415.78c15.99-38.39-25.59-79.97-63.97-63.97z"
              ></path>
            </svg>
          </FromIcon>

          <svg xmlns="http://www.w3.org/2000/svg" width="10" height="32" viewBox="0 0 5 27">
            <g id="points" transform="translate(-680 -764)">
              <circle fill="#b7b7b7" cx="1.5" cy="1.5" r="1.5" transform="translate(681 764)" />
              <circle fill="#b7b7b7" cx="2.5" cy="2.5" r="2.5" transform="translate(680 775)" />
              <circle fill="#b7b7b7" cx="1.5" cy="1.5" r="1.5" transform="translate(681 788)" />
            </g>
          </svg>

          <ToIcon className="icon-ico_route-active" />
        </FormIcons>
        <Form>
          <TextField
            name="from"
            label={t("Startpunkt")}
            defaultValue={t("Aktuelle Fahrzeugposition")}
            required
            disabled
          />

          <Autocomplete
            getOptionLabel={(option) => (typeof option === "string" ? option : option.label)}
            filterOptions={(x) => x}
            options={options}
            autoComplete
            includeInputInList
            renderInput={(params: any) => (
              <TextField {...params} label={t("Reiseziel")} onChange={handleInputChange} />
            )}
            clearText={t("Leeren")}
            closeText={t("Schließen")}
            loadingText={t("Lädt...")}
            noOptionsText={t("Keine Optionen")}
            openText={t("Öffnen")}
            value={optionValue}
            onChange={handleSelection}
          />
        </Form>
      </FormContainer>

      {routeData && (
        <RouteInformationContainer>
          <h2>{t("Routeninformationen")}</h2>

          <RouteInformationList>
            <li>
              <span>{t("Distanz")}: </span>
              <RouteInformationValue>
                {formatNumber(locale, parseFloat((routeData.summary.distance / 1000).toFixed(2)), 2)} km
              </RouteInformationValue>
            </li>
            <li>
              <span>{t("Fahrtzeit")}: </span>
              <RouteInformationValue>{formatSeconds(routeData.summary.travelTime)}</RouteInformationValue>
            </li>
            <li>
              <span>{t("Mautkosten")}: </span>
              <RouteInformationValue>
                {formatNumber(locale, parseFloat(routeData.cost.totalCost), 2)} {routeData.cost.currency}
              </RouteInformationValue>
            </li>
          </RouteInformationList>
        </RouteInformationContainer>
      )}
    </section>
  );
};

export default RouteOverlay;
