import React, {useState, useCallback, useRef, useEffect, MouseEventHandler} from "react";
import {HereMap} from "../Map";
import styled, {css} from "styled-components";
import {useTranslation} from "react-i18next";
import {debounce, Loading} from "dkv-live-frontend-ui";
import axios from "axios";
import config from "config";

// -----------------------------------------------------------------------------------------------------------
interface ContainerProps {
  open?: boolean;
  hasSuggestions?: boolean;
}

const Container = styled.div<ContainerProps>`
  position: absolute;
  top: 24px;
  left: 24px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: var(--dkv-background-primary-color);
  height: 50px;
  z-index: 2;

  --dkv-ui-loading-size: 24px;
  --dkv-ui-loading-border: 1px;
  --dkv-ui-loading-border-radius: 25px;

  ${(props) =>
    props.open &&
    css`
      border-radius: 25px;
    `}

  ${(props) =>
    props.hasSuggestions &&
    css`
      border-radius: 25px 25px 0 0;
    `}
`;

const InputButton = styled.button<ContainerProps>`
  color: inherit;
  line-height: 1.25;
  font-size: 1.1426em;
  font-weight: 500;
  transition: background-color linear 0.1s, color linear 0.1s;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 50px;
  height: 100%;

  &:hover,
  &:focus {
    background-color: var(--dkv-highlight-primary-color);
    color: var(--dkv-highlight-primary-text-color);
  }

  ${(props) =>
    props.open &&
    css`
      display: none;
    `}
`;

const InputField = styled.input<ContainerProps>`
  width: 50px;
  height: 50px;
  border: none;
  outline: none;
  -webkit-appearance: none;
  color: var(--dkv-ui-input-color, #666666);
  line-height: 1.5;
  font-size: 1.14286em;
  font-family: inherit;
  font-weight: 500;
  padding: 0;
  border: solid 1px transparent;
  transition: background-color linear 0.1s, border-color linear 0.1s, width linear 0.1s;
  border-radius: 25px;
  visibility: hidden;

  &:hover {
    border-color: var(--dkv-ui-input-hover-border-color, #666666);
  }

  &:focus {
    border-color: var(--dkv-ui-input-focused-border-color, #666666);
  }

  &:invalid {
    border-color: var(--dkv-ui-input-error-color, #e44c4d);
  }

  ::placeholder {
    color: var(--dkv-ui-input-placeholder-color, #c1c1c1);
    opacity: 1;
  }

  :-ms-input-placeholder {
    color: var(--dkv-ui-input-placeholder-color, #c1c1c1);
    opacity: 1;
  }

  ::-ms-input-placeholder {
    color: var(--dkv-ui-input-placeholder-color, #c1c1c1);
    opacity: 1;
  }

  ${(props) =>
    props.open &&
    css`
      visibility: visible;
      width: 350px;
      padding: 7px 24px 7px 45px;
    `}
`;

const CloseButton = styled.button<ContainerProps>`
  position: absolute;
  top: 13px;
  left: 13px;
  color: #666666;
  line-height: 1.25;
  font-size: 1.1426em;
  font-weight: 500;
  transition: color linear 0.1s;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  display: none;

  &:hover,
  &:focus {
    color: var(--dkv-text-primary-hover-color);
  }

  ${(props) =>
    props.open &&
    css`
      display: block;
    `}
`;

const SuggestionsList = styled.ul`
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: var(--dkv-background-primary-color);
  max-width: 350px;
  overflow: hidden;
`;

const SuggestionButton = styled.button`
  color: inherit;
  transition: background-color linear 0.1s;
  font: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  align-items: center;
  width: 100%;
  padding: 8px 12px 8px 24px;
  text-align: left;

  &:hover,
  &:focus {
    background-color: var(--dkv-ui-button-secondary-hover-background-color, transparent);
    color: var(--dkv-ui-button-secondary-hover-color, #666666);
  }
`;

// -----------------------------------------------------------------------------------------------------------
let mapMarker: any;
// let markerTooltipBubble: any;

// function showMarkerTooltip(ui: any, marker: any) {
//   const data = marker.getData();

//   markerTooltipBubble = new H.ui.InfoBubble(marker.getGeometry(), {content: data.label});
//   markerTooltipBubble.addClass("dkv-map-marker-tooltip");
//   ui.addBubble(markerTooltipBubble);
// }

// function hideMarkerTooltip(ui: any) {
//   if (!markerTooltipBubble) {
//     return;
//   }

//   ui.removeBubble(markerTooltipBubble);
//   markerTooltipBubble = null;
// }

function createMarkerIcon() {
  const div = document.createElement("div");
  div.classList.add("dkv-map-location-icon");
  div.classList.add("icon-ico_route-active");

  return new H.map.DomIcon(div);
}

function showMarker(map: any, data: any /*,ui: any*/) {
  hideMarker(map);

  const pos = {lat: data.position.latitude, lng: data.position.longitude};
  mapMarker = new H.map.DomMarker(pos, {icon: createMarkerIcon()});
  mapMarker.setData(data);

  // mapMarker.addEventListener("pointerenter", (e: MouseEvent) => {
  //   e.preventDefault();
  //   e.stopPropagation();

  //   const targetMarker = e.target as any;
  //   showMarkerTooltip(ui, targetMarker);
  // });

  // mapMarker.addEventListener("pointerleave", (e: MouseEvent) => {
  //   e.preventDefault();
  //   e.stopPropagation();

  //   hideMarkerTooltip(ui);
  // });

  map.addObject(mapMarker);
  map.setCenter(pos);
  map.setZoom(17);
}

function hideMarker(map: any) {
  if (!mapMarker) {
    return;
  }

  map.removeObjects([mapMarker]);
  mapMarker = null;
}

// -----------------------------------------------------------------------------------------------------------
interface MapSearchProps {
  hereMap: HereMap;
}

const MapSearch: React.FC<MapSearchProps> = ({hereMap: {map}}) => {
  const {t} = useTranslation();

  // search
  const [value, setValue] = useState("");
  const [suggestions, setSuggestions] = useState<any[] | null>(null);

  const search = useRef<(...args: any[]) => void>(null);

  useEffect(() => {
    const doSearch = async (v: string) => {
      try {
        const res = await axios.get(
          `https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json?apiKey=${config.HERE_API_KEY}&query=${v}`
        );

        const suggestions = res?.data?.suggestions;
        if (!suggestions) {
          setSuggestions(null);
          return;
        }

        setSuggestions(suggestions);
      } catch (err) {
        //@todo: show growl
        console.error(err);
        setSuggestions(null);
      }
    };

    (search as React.MutableRefObject<(...args: any[]) => void>).current = debounce(doSearch, 300);
  }, [setSuggestions]);

  const onInputChange = useCallback(
    (e) => {
      const v = e.target.value;
      setValue(v);

      if (search.current && v) {
        search.current(v);
      } else {
        setSuggestions(null);
      }
    },
    [setValue, search, setSuggestions]
  );

  // selection
  const [loading, setLoading] = useState(false);

  const showSuggestion: MouseEventHandler<HTMLButtonElement> = useCallback(
    (e) => {
      const load = async () => {
        const data = (e.currentTarget as HTMLButtonElement).dataset;
        const index = data.index || null;
        if (!index || !suggestions) {
          return;
        }

        const suggestion = suggestions[index];
        if (!suggestion) {
          return;
        }

        setLoading(true);
        setValue(suggestion.label);
        setSuggestions(null);

        try {
          const res = await axios.get(
            `https://geocoder.ls.hereapi.com/6.2/geocode.json?locationid=${suggestion.locationId}&jsonattributes=1&gen=9&apiKey=${config.HERE_API_KEY}`
          );

          setLoading(false);

          const position = res?.data.response?.view[0]?.result[0]?.location?.displayPosition;
          if (!position) {
            return;
          }

          showMarker(map, {...suggestion, position});
        } catch (err) {
          //@todo: show growl
          setLoading(false);
          console.error(err);
        }
      };

      load();
    },
    [suggestions, map]
  );

  // open
  const [open, setOpen] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);

  const toggle = useCallback(() => {
    setOpen((o) => {
      const isOpen = !o;

      if (isOpen) {
        setTimeout(() => {
          if (inputRef.current !== null) {
            inputRef.current.focus();
          }
        });
      } else {
        setValue("");
        setSuggestions(null);
        hideMarker(map);
      }

      return isOpen;
    });
  }, [setOpen, setValue, map]);

  // render
  return (
    <Container open={open} hasSuggestions={suggestions !== null}>
      <Loading inprogress={loading} />

      <InputButton open={open} onClick={toggle} title={t("Kartensuche") as string}>
        <span className="icon-ico_search" style={{fontSize: 28}} />
      </InputButton>
      <InputField
        open={open}
        placeholder={t("Kartensuche")}
        value={value}
        onChange={onInputChange}
        ref={inputRef}
      />
      <CloseButton open={open} onClick={toggle} title={"Ausblenden"}>
        <span className="icon-ico_clear" style={{fontSize: 24}} />
      </CloseButton>

      {suggestions !== null && (
        <SuggestionsList>
          {(suggestions as any[]).map((suggestion, i) => (
            <li key={suggestion.locationId}>
              <SuggestionButton onClick={showSuggestion} data-index={i}>
                {suggestion.label}
              </SuggestionButton>
            </li>
          ))}
        </SuggestionsList>
      )}
    </Container>
  );
};

export default MapSearch;
