import React, {useCallback} from "react";
import {Asset} from "../../../state/assets/reducer";
import styled from "styled-components";
import {useTranslation} from "react-i18next";
import {showOverlay} from "../state/overlay/actions";
import {OVERLAY_FUELSTATION, OVERLAY_ROUTE} from "../state/overlay/reducer";
import {showDetails} from "../state/details/actions";
import {IconButton} from "@material-ui/core";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.section`
  white-space: nowrap;
  padding: 16px 6px 0 6px;
  width: 378px;
`;

const Headline = styled.div`
  position: relative;
  margin: 0 32px 0 18px;
  display: flex;
  overflow: hidden;

  span {
    font-weight: normal;
    font-size: 1.1428571428571428rem;
    color: #363636;
    line-height: 1.5;
    flex-shrink: 0;
    padding-right: 32px;
    overflow: hidden;
    text-overflow: ellipsis;
    width: 100%;
  }
`;

const Actions = styled.div`
  display: flex;
  flex-flow: row wrap;
`;

const ActionButton = styled.button`
  color: var(--dkv-text-primary-color);
  line-height: 1.25;
  font-size: 0.857143em;
  font-weight: 500;
  transition: background-color linear 0.1s;
  font-family: inherit;
  border: 0;
  padding: 4px;
  outline: 0;
  background-color: var(--dkv-background-primary-color);
  cursor: pointer;
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  width: 80px;
  margin-bottom: 6px;
  display: flex;
  justify-content: flex-start;

  &:hover,
  &:focus {
    background-color: var(--dkv-background-primary-hover-color);
  }

  &:active {
    background-color: var(--dkv-ui-button-secondary-active-background-color, #666666);
    color: var(--dkv-ui-button-secondary-active-color, #fff);
    border-color: var(--dkv-ui-button-secondary-active-border-color, #666666);
  }

  span {
    margin-top: 8px;
    white-space: normal;
    font-size: 12px;
  }
`;

const CloseButton = styled(IconButton)`
  position: absolute !important;
  bottom: 14px;
  right: 10px;
  top: 10px;
  padding: 0 !important;
  width: 24px;
  height: 24px;

  span {
    color: #363636 !important;
  }
`;
// -----------------------------------------------------------------------------------------------------------
interface AssetInfoBubbleProps {
  asset: Asset;
  dispatch: any;
  ui: any;
  map: any;
  marker: any;
  close: (ui: any) => void;
}

const AssetInfoBubble: React.FC<AssetInfoBubbleProps> = React.memo(
  ({asset, dispatch, ui, map, close, marker}) => {
    const {t} = useTranslation();

    // close
    const closeBubble = useCallback(() => {
      close(ui);
    }, [ui, close]);

    // overlay
    const openRouteOverlay = useCallback(() => {
      dispatch(showOverlay(OVERLAY_ROUTE, asset.id));
      closeBubble();
    }, [dispatch, asset.id, closeBubble]);

    // fuel station overlay
    const openFuelStationOverlay = useCallback(() => {
      dispatch(showOverlay(OVERLAY_FUELSTATION, asset.id));
      closeBubble();
    }, [dispatch, asset.id, closeBubble]);

    // details
    const zoomToMarker = () => {
      map.__centerAsset = asset ? asset.id : null;

      dispatch(showDetails(asset.id));

      map.setCenter(marker.getGeometry());

      if (map.getZoom() < 17) {
        map.setZoom(17);
      }
    };

    const zoomToMap = () => {
      if (!map.__assetGroup) {
        return;
      }

      map.getViewModel().setLookAtData({
        bounds: map.__assetGroup.getBoundingBox(),
      });

      map.setCenter(marker.getGeometry());

      setTimeout(() => {
        if (map.getZoom() > 19) {
          map.setZoom(18);
        }
      });
    };

    return (
      <Container>
        <Headline>
          <span title={asset.gnss.address?.label ?? ""}>{asset.gnss.address?.label}</span>
        </Headline>
        <Actions>
          <ActionButton onClick={openRouteOverlay}>
            <span className="icon-ico_routes" style={{fontSize: 30}} />
            <span>{t("Route planen")}</span>
          </ActionButton>

          {hasPermission("poi:FindDKVStations") && (
            <ActionButton onClick={openFuelStationOverlay}>
              <span className="icon-ico_fuel" style={{fontSize: 30}} />
              <span>{t("Zur Tankstelle navigieren")}</span>
            </ActionButton>
          )}

          <ActionButton onClick={zoomToMarker} style={{marginLeft: "auto"}}>
            <span className="icon-ico_zoom-in" style={{fontSize: 30}} />
            <span>{t("Zoom auf Fahrzeug")}</span>
          </ActionButton>

          <ActionButton onClick={zoomToMap}>
            <span className="icon-ico_zoom-out" style={{fontSize: 30}} />
            <span>{t("Zoom auf Karte")}</span>
          </ActionButton>
        </Actions>

        <CloseButton onClick={closeBubble} title={"Schließen"}>
          <span className="icon-ico_clear" style={{fontSize: 24}} />
        </CloseButton>
      </Container>
    );
  }
);

export default AssetInfoBubble;
