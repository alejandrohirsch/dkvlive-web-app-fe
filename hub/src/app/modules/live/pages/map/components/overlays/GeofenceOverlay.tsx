import React, {useEffect, useState} from "react";
import {HereMap} from "../../Map";
import {Headline} from "../MapOverlay";
import {Button, Loading, TextField} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import {FormControl, FormControlLabel, Radio, RadioGroup, Step, StepLabel, Stepper} from "@material-ui/core";
import styled from "styled-components";
import {useDispatch} from "react-redux";
import {hideOverlay} from "../../state/overlay/actions";
import {useFormik} from "formik";
import httpClient from "../../../../../../../services/http";
import {loadGeofences} from "../../state/geofences/actions";
import {OverlayData} from "../../state/overlay/reducer";

// -----------------------------------------------------------------------------------------------------------
const CustomStepper = styled(Stepper)`
  > .MuiStep-root > .MuiStepLabel-root {
    > .MuiStepLabel-iconContainer {
      > .MuiStepIcon-root {
        color: var(--dkv-stepper-inactive-color);
        > .MuiStepIcon-text {
          font-family: inherit !important;
        }
      }
      > .MuiStepIcon-root.MuiStepIcon-active {
        color: var(--dkv-stepper-active-color);
      }
      > .MuiStepIcon-root.MuiStepIcon-completed {
        color: var(--dkv-stepper-completed-color);
      }
    }
    > .MuiStepLabel-labelContainer {
      > .MuiStepLabel-label {
        font-family: inherit !important;
        color: var(--dkv-stepper-inactive-color);
      }
      > .MuiStepLabel-label.MuiStepLabel-active {
        color: var(--dkv-stepper-active-color);
      }
      > .MuiStepLabel-label.MuiStepLabel-completed {
        color: var(--dkv-stepper-completed-color);
      }
    }
  }
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 16px;
`;

const TextArea = styled(TextField)`
  .MuiOutlinedInput-root {
    height: auto;
  }
`;

const ButtonContainer = styled.div`
  display: flex;
`;

const StepContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 22px 22px 22px;
`;

const HorizontalLine = styled.div`
  height: 1px;
  background-color: #dbdbdb;
  margin: 22px 0;
`;

const StepHeader = styled.div`
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 20px;
`;

export const GeoButtonIcon = styled.div`
  width: 36px;
  height: 36px;
  border-radius: 50%;
  background-color: #efefef;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 6px;
  transition: color linear 0.1s, background-color linear 0.1s;

  span {
    color: #666666;
    font-size: 16px;
    transition: color linear 0.1s;
  }
`;

export const GeoButton = styled.button`
  user-select: none;
  border: 0;
  outline: 0;
  cursor: pointer;
  overflow: hidden;
  font: inherit;
  background-color: transparent;
  display: flex;
  flex-direction: column;
  align-items: center;
  font-size: 12px;
  color: #666666;
  margin-left: 8px;
  transition: color linear 0.1s;

  &:hover,
  &:focus {
    color: #2e6b90;

    ${GeoButtonIcon} {
      background-color: #2e6b90;

      span {
        color: #fff;
      }
    }
  }
`;

const CustomRadio = styled(Radio)`
  &.MuiRadio-colorSecondary.Mui-checked {
    color: var(--dkv-link-primary-color);
  }
  &.MuiRadio-colorSecondary:hover,
  &.MuiRadio-colorSecondary.Mui-checked:hover {
    background-color: rgba(0, 75, 120, 0.04);
  }
`;
const CustomFormControlLabel = styled(FormControlLabel)`
  > .MuiTypography-body1 {
    font-family: inherit !important;
  }
`;

// -----------------------------------------------------------------------------------------------------------
const steps = ["Beschreibung", "Geozone zeichnen", "Meldungen"];

function createGeofenceMarker(startIcon?: boolean) {
  const div = document.createElement("div");
  div.classList.add("dkv-map-geofence");
  if (startIcon) {
    div.classList.add("dkv-map-geofence-start");
  }
  return new H.map.DomIcon(div);
}

// -----------------------------------------------------------------------------------------------------------
export interface GeofenceEditData {
  id?: string;
  name: string;
  description: string;
  coordinates?: number[];
}

interface GeofenceOverlayProps {
  hereMap: HereMap;
  data: OverlayData;
}

interface GeofenceState {
  drawing: boolean;
  geofence: any;
}

const GeofenceOverlay: React.FC<GeofenceOverlayProps> = ({hereMap: {map, behavior}, data}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const [loading] = useState(false);

  const [editMode, setEditMode] = useState(false);

  useEffect(() => {
    if (!data) {
      return;
    }

    setEditMode(data.data !== undefined);
  }, [data, setEditMode]);

  const [activeStep, setActiveStep] = useState(0);
  const [notificationType, setNotificationType] = useState("alarm");
  const [geozone, setGeozone] = useState<GeofenceState>({
    drawing: false,
    geofence: {
      polygon: null,
      markers: [],
    },
  });

  // remove current map objects on unmount
  useEffect(() => {
    return () => {
      if (typeof map._geofenceDrawFunction === "function") {
        map.getViewPort().element.style.cursor = "auto";
        map.removeEventListener("tap", map._geofenceDrawFunction);
      }
      if (map._geofenceObjects && map._geofenceObjects.length) {
        map.removeObjects(map._geofenceObjects);
      }
    };
  }, [map]);

  // draw polygon fn
  const mapCreatePolygon = () => {
    // remove old polygon data
    if (geozone.geofence.polygon) {
      map.removeObject(geozone.geofence.polygon);
      map._geofenceObjects = map._geofenceObjects.filter((x: any) => x !== geozone.geofence.polygon);

      geozone.geofence.markers.forEach((marker: any) => {
        map.removeObject(marker);
        map._geofenceObjects = map._geofenceObjects.filter((x: any) => x !== marker);
      });
    }

    if (map._geofenceDrawFunction) {
      map.removeEventListener("tap", map._geofenceDrawFunction);
    }

    // setup drawing
    const lineString = new H.geo.LineString();
    const geofence: any = {polygon: null, markers: []};
    let polyline: any;

    map._geofenceObjects = [];
    map._geofenceDrawFunction = (e: any) => {
      // add point to line
      const latLng = map.screenToGeo(e.currentPointer.viewportX, e.currentPointer.viewportY);
      lineString.pushPoint(latLng);

      if (lineString.getPointCount() === 1) {
        const startMarker = new H.map.DomMarker(latLng, {icon: createGeofenceMarker(true)});

        // setup start marker click == polygon finished
        startMarker.addEventListener("tap", () => {
          if (lineString.getPointCount() < 3) {
            return;
          }

          lineString.pushPoint(latLng);

          // remove helper objects
          map._geofenceObjects = map._geofenceObjects.filter((x: any) => x !== startMarker);
          map._geofenceObjects = map._geofenceObjects.filter((x: any) => x !== polyline);
          map.removeObject(startMarker);
          map.removeObject(polyline);
          map.removeEventListener("tap", map._geofenceDrawFunction);

          // create polygon
          geofence.polygon = new H.map.Polygon(lineString, {
            style: {
              fillColor: "rgba(0, 75, 120, .25)",
              strokeColor: "rgb(0, 75, 120)",
              lineWidth: 3,
            },
          });

          map.addObject(geofence.polygon);
          map._geofenceObjects.push(geofence.polygon);

          // create corner markers
          for (let i = 0; i < lineString.getPointCount() - 1; i++) {
            const marker = new H.map.DomMarker(lineString.extractPoint(i), {
              icon: createGeofenceMarker(),
            });
            marker.draggable = true;

            marker.addEventListener(
              "dragstart",
              () => {
                map.getViewPort().element.style.cursor = "pointer";
                document.body.style.userSelect = "none";
                behavior.disable();
              },
              false
            );

            marker.addEventListener(
              "drag",
              (ev: any) => {
                const latLng2 = map.screenToGeo(ev.currentPointer.viewportX, ev.currentPointer.viewportY);
                marker.setGeometry(latLng2);
              },
              false
            );

            marker.addEventListener(
              "dragend",
              () => {
                const newLineString = new (window as any).H.geo.LineString();
                geofence.markers.forEach((marker: any) => newLineString.pushPoint(marker.getGeometry()));

                map.removeObject(geofence.polygon);
                map._geofenceObjects = map._geofenceObjects.filter((x: any) => x !== geofence.polygon);

                geofence.polygon = new (window as any).H.map.Polygon(newLineString, {
                  style: {
                    fillColor: "rgba(0, 75, 120, .25)",
                    strokeColor: "rgb(0, 75, 120)",
                    lineWidth: 3,
                  },
                });

                map.addObject(geofence.polygon);
                map._geofenceObjects.push(geofence.polygon);

                setGeozone({drawing: false, geofence: geofence});

                map.getViewPort().element.style.cursor = "auto";
                document.body.style.userSelect = "auto";
                behavior.enable();
              },
              false
            );

            geofence.markers.push(marker);
            map.addObject(marker);
            map._geofenceObjects.push(marker);
          }

          map.getViewPort().element.style.cursor = "auto";
          setGeozone({drawing: false, geofence: geofence});
        });

        map.addObject(startMarker);
        map._geofenceObjects.push(startMarker);
      } else {
        if (!polyline) {
          polyline = new H.map.Polyline(lineString, {
            style: {
              strokeColor: "rgb(0, 75, 120)",
              lineWidth: 3,
            },
          });

          map.addObject(polyline);
          map._geofenceObjects.push(polyline);
        } else {
          polyline.setGeometry(lineString);
        }
      }
    };
    map.addEventListener("tap", map._geofenceDrawFunction);

    map.getViewPort().element.style.cursor = "copy";
    setGeozone({...geozone, drawing: true});
  };

  const toggleMarkers = (nextStep: number) => {
    if (!geozone.geofence.markers || !geozone.geofence.markers.length) {
      return;
    }

    if (nextStep === 1) {
      geozone.geofence.markers.forEach((marker: any) => {
        map.addObject(marker);
        map._geofenceObjects.push(marker);
      });
    } else if (activeStep === 1) {
      geozone.geofence.markers.forEach((marker: any) => {
        map.removeObject(marker);
        map._geofenceObjects = map._geofenceObjects.filter((x: any) => x !== marker);
      });
    }
  };

  // steps
  const handleNext = () => {
    const nextStep = activeStep + 1;
    if (activeStep === steps.length - 1) {
      map._geofenceObjects = null;
      return;
    }
    toggleMarkers(nextStep);
    setActiveStep(nextStep);
  };

  const handleBack = () => {
    const nextStep = activeStep - 1;
    toggleMarkers(nextStep);
    setActiveStep(nextStep);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  // form
  const form = useFormik<GeofenceEditData>({
    initialValues: data?.data ?? {name: "", coordinates: [], description: ""},
    onSubmit: (values) => {
      const submit = async () => {
        const config = {
          headers: {
            "Content-Type": "application/json",
          },
        };

        const submitValues = Object.assign({}, values);
        submitValues.coordinates = geozone.geofence.polygon
          .getGeometry()
          .getExterior()
          .getLatLngAltArray()
          .filter((_: any, i: number) => {
            return (i + 1) % 3 !== 0;
          });

        let err;
        if (editMode) {
          [, err] = await httpClient.put(
            `/management/geofences/${values.id}`,
            JSON.stringify(submitValues),
            config
          );
        } else {
          [, err] = await httpClient.post("/management/geofences", JSON.stringify(submitValues), config);
        }

        if (err !== null) {
          form.setSubmitting(false);
          return;
        }

        dispatch(hideOverlay());
        dispatch(loadGeofences());
      };

      submit();
    },
  });

  // render
  const renderStep = (stepIndex: number) => {
    switch (stepIndex) {
      case 0:
        return (
          <section>
            <StepHeader>{t("1. Name der Geozone")}</StepHeader>
            <FormGrid>
              <TextField
                name="name"
                type="text"
                label={t("Name")}
                value={form.values.name}
                onChange={form.handleChange}
                data-autofocus
                variant="outlined"
              />
              <TextArea
                name="description"
                label={t("Beschreibung")}
                value={form.values.description}
                onChange={form.handleChange}
                multiline
                variant="outlined"
                rows={5}
              />
            </FormGrid>
          </section>
        );
      case 1:
        return (
          <section>
            <div style={{display: "flex", alignItems: "center"}}>
              <StepHeader style={{marginBottom: 0}}>{t("2. Geozone auf Karte zeichnen")}</StepHeader>
              <div style={{flexGrow: 1}} />
              <GeoButton
                onClick={() => {
                  mapCreatePolygon();
                }}
                disabled={geozone.drawing}
              >
                <GeoButtonIcon>
                  <span className="icon-ico_edit" />
                </GeoButtonIcon>
                {geozone.geofence.polygon ? t("Geozone neu zeichnen") : t("Geozone zeichnen")}
              </GeoButton>
            </div>
          </section>
        );
      case 2:
        return (
          <section>
            <StepHeader>{t("3. Meldungen für Geozone")}</StepHeader>
            <FormControl component="fieldset">
              <RadioGroup
                name="notificationType"
                value={notificationType}
                onChange={(e) => {
                  setNotificationType(e.target.value);
                }}
              >
                <CustomFormControlLabel value="alarm" control={<CustomRadio />} label={t("Alarme")} />
                <CustomFormControlLabel value="warn" control={<CustomRadio />} label={t("Hinweise")} />
                <CustomFormControlLabel
                  value="information"
                  control={<CustomRadio />}
                  label={t("Informationen")}
                />
              </RadioGroup>
            </FormControl>
          </section>
        );
      default:
        return null;
    }
  };

  return (
    <form onSubmit={form.handleSubmit}>
      <Loading inprogress={loading} />

      <Headline>{editMode ? t("Geozone bearbeiten") : t("Neue Geozone anlegen")}</Headline>
      <CustomStepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{t(label)}</StepLabel>
          </Step>
        ))}
      </CustomStepper>
      <div>
        {activeStep === steps.length ? (
          <StepContainer>
            <HorizontalLine />
            <div>{t("Geozone konfiguriert")}</div>
            <HorizontalLine />
            <ButtonContainer>
              <Button onClick={handleReset}>{t("Zurücksetzen")}</Button>
            </ButtonContainer>
          </StepContainer>
        ) : (
          <StepContainer>
            <HorizontalLine />
            <div>{renderStep(activeStep)}</div>
            <HorizontalLine />
            <ButtonContainer>
              {activeStep > 0 && (
                <Button onClick={handleBack} secondary autoWidth>
                  {t("Zurück")}
                </Button>
              )}
              <div style={{flexGrow: 1}} />
              <Button
                key={activeStep}
                color="primary"
                onClick={handleNext}
                autoWidth
                disabled={
                  (activeStep === 0 && (!form.values.name || !form.values.name.length)) ||
                  (activeStep === 1 && (!geozone || !geozone.geofence.polygon))
                }
              >
                {activeStep === steps.length - 1 ? t("Zone anlegen") : t("Weiter")}
              </Button>
            </ButtonContainer>
          </StepContainer>
        )}
      </div>
    </form>
  );
};

export default GeofenceOverlay;
