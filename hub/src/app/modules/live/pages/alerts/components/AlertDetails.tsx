import React, {useState} from "react";
import {Alert} from "../state/reducer";
import {
  WindowActions,
  WindowContent,
  WindowContentContainer,
  WindowContentProps,
  WindowHeadline,
  WindowTabs,
  WindowTabsContainer,
} from "../../../../../components/Window";
import {Tab} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components";
import dayjs from "dayjs";
import AlertActions from "./AlertActions";

// -----------------------------------------------------------------------------------------------------------
export const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

export const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

export const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;
const CommentContainer = styled.div`
  color: var(--dkv-text-primary-color);
`;

const CommentHeader = styled.div`
  font-weight: bold;
  font-size: 1.1em;
  margin: 0 0 32px 32px;
`;

const CommentList = styled.ul`
  padding: 0 50px;
`;

const CommentListItem = styled.li`
  margin: 24px 0;
`;

const CommentItemHeader = styled.div`
  margin-bottom: 8px;
`;

// -----------------------------------------------------------------------------------------------------------
interface AlertDetailsProps extends WindowContentProps {
  alert: Alert;
}

const AlertDetails: React.FC<AlertDetailsProps> = React.memo(({alert}) => {
  const {t} = useTranslation();

  // tab
  const [tab, setTab] = useState(0);
  const handleTabChange = (_: React.ChangeEvent<unknown>, newTab: number) => {
    setTab(newTab);
  };

  // render
  return (
    <WindowContentContainer minWidth="700px" full>
      <WindowHeadline padding>
        <h1>{alert.alarm.name}</h1>
      </WindowHeadline>

      <WindowTabsContainer>
        <WindowTabs value={tab} onChange={handleTabChange} variant="scrollable">
          <Tab label={t("Details")} />
          {alert.comments && alert.comments.length > 0 ? <Tab label={t("Kommentare")} /> : ""}
          {alert.activity && alert.activity.length > 0 ? <Tab label={t("Aktivität")} /> : ""}
        </WindowTabs>
      </WindowTabsContainer>

      <WindowContent padding>
        {tab === 0 && (
          <>
            <Table>
              <tbody>
                <tr>
                  <TableLabel>{t("Fahrzeug")}:</TableLabel>
                  <TableValue>{alert.license_plate}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Aktueller Ort")}:</TableLabel>
                  <TableValue>{alert.address}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Datum, Zeit")}:</TableLabel>
                  <TableValue>{dayjs(alert.start_time).format("DD.MM. / HH:mm")}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Von - Bis")}:</TableLabel>
                  <TableValue>
                    {dayjs(alert.start_time).format("HH:mm")} - {dayjs(alert.end_time).format("HH:mm")}
                  </TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Fahrer")}:</TableLabel>
                  <TableValue>
                    {alert.driver.firstname} {alert.driver.lastname}
                  </TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Tel.Nr. Fahrer")}:</TableLabel>
                  <TableValue>{alert.driver.phone}</TableValue>
                </tr>
              </tbody>
            </Table>
            <WindowActions>
              <AlertActions />
              {/*
              <ToolbarButton type="button">
                <ToolbarButtonIcon>
                  <span className="icon-ico_export" />
                </ToolbarButtonIcon>
                {t("Exportieren")}
              </ToolbarButton>
              */}
            </WindowActions>
          </>
        )}

        {tab === 1 && alert.comments && (
          <CommentContainer>
            <CommentHeader>
              {t("Kommentare")} ({alert.comments.length})
            </CommentHeader>
            <CommentList>
              {alert.comments.map((c) => (
                <CommentListItem key={c.id}>
                  <CommentItemHeader>
                    <b>{c.username}</b> {dayjs(c.created_at).format("HH:mm")}
                  </CommentItemHeader>
                  {c.comment}
                </CommentListItem>
              ))}
            </CommentList>
          </CommentContainer>
        )}

        {tab === 2 && <div>Aktivität</div>}
      </WindowContent>
    </WindowContentContainer>
  );
});

export default AlertDetails;
