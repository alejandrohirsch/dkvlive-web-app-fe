import React from "react";
import styled from "styled-components";
import {useTranslation} from "react-i18next";
import dayjs from "dayjs";

// -----------------------------------------------------------------------------------------------------------
const Header = styled.div`
  font-size: 1.7em;
  font-weight: bold;
  border-bottom: 1px solid var(--dkv-grey_30);
  text-align: center;
  padding: 16px;
`;

const HeaderIcon = styled.span`
  vertical-align: middle;
  font-size: 1.3em;
  margin-right: 16px;
`;

const List = styled.ul`
  width: 500px;
`;

const ListItem = styled.li`
  padding: 16px;
  border-bottom: 1px solid var(--dkv-grey_30);
`;

const ListItemHeader = styled.div`
  margin: 0 0 8px 40px;
`;

const ListItemBody = styled.div`
  position: relative;
  margin-left: 40px;
`;

const ListItemIcon = styled.div`
  position: absolute;
  top: -5px;
  left: -30px;
  font-size: 22px;
`;
// -----------------------------------------------------------------------------------------------------------
const activityList = [
  {
    id: 1,
    type: "unread",
    license_plate: "KU434YC",
    username: "Diana Neustadt",
    alarm_name: "Betankung stattgefunden",
    created_at: "2020-06-22T11:29:12.544Z",
  },
  {
    id: 2,
    type: "new_comment",
    license_plate: "KU123AB",
    username: "Thomas Fried",
    alarm_name: "Fahrt ohne Fahrerkarte",
    created_at: "2020-06-19T08:17:12.544Z",
  },
  {
    id: 3,
    type: "delete",
    license_plate: "KU125EF",
    username: "Nicole Theiss",
    alarm_name: "Lenkzeit Überschreitung",
    created_at: "2020-06-17T14:42:12.544Z",
  },
  {
    id: 4,
    type: "done",
    license_plate: "KU124CD",
    username: "Thomas Sommer",
    alarm_name: "Geschwindigkeit Überschreitung",
    created_at: "2020-06-15T10:05:12.544Z",
  },
];

const ActivityList: React.FC = () => {
  const {t} = useTranslation();

  const getIcon = (type: string) => {
    switch (type) {
      case "done":
        return "icon-ico_check";
      case "unread":
        return "icon-ico_unread";
      case "new_comment":
        return "icon-ico_document-new";
      case "delete":
        return "icon-ico_remove";
      default:
        return "";
    }
  };

  const getText = (data: any) => {
    switch (data.type) {
      case "done":
        return t("{{.username}} hat {{.alarm_name}} {{.license_plate}} als bearbeitet markiert.", data);
      case "unread":
        return t("{{.username}} hat {{.alarm_name}} {{.license_plate}} als ungelesen markiert.", data);
      case "new_comment":
        return t("{{.username}} hat bei {{.alarm_name}} {{.license_plate}} einen Kommentar erstellt.", data);
      case "delete":
        return t("{{.username}} hat {{.alarm_name}} {{.license_plate}} gelöscht.", data);
      default:
        return "";
    }
  };

  // render
  return (
    <div>
      <Header>
        <HeaderIcon className="icon-ico_activity" />
        {t("Aktivität")}
      </Header>
      <List>
        {activityList.length > 0 &&
          activityList.map((a) => (
            <ListItem key={a.id}>
              <ListItemHeader>
                <b>{dayjs(a.created_at).format("DD.MM.YY")}</b> {dayjs(a.created_at).format("HH:mm")}
              </ListItemHeader>
              <ListItemBody>
                <ListItemIcon>
                  <span className={getIcon(a.type)} />
                </ListItemIcon>
                {getText(a)}
              </ListItemBody>
            </ListItem>
          ))}
      </List>
    </div>
  );
};

export default ActivityList;
