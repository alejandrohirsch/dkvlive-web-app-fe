import React from "react";

import {useTranslation} from "react-i18next";
import styled from "styled-components";
import {ToolbarButton, ToolbarButtonIcon} from "../../../../../components/DataTable";
import {Alert} from "../state/reducer";
// -----------------------------------------------------------------------------------------------------------

const ActionContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin: auto;
`;

// -----------------------------------------------------------------------------------------------------------
interface AlertActionsProps {
  alert?: Alert;
  closeWindow?: () => void;
}

// -----------------------------------------------------------------------------------------------------------
const AlertActions: React.FC<AlertActionsProps> = () => {
  const {t} = useTranslation();

  // render
  return (
    <ActionContainer>
      <ToolbarButton type="button">
        <ToolbarButtonIcon>
          <span className="icon-ico_route" />
        </ToolbarButtonIcon>
        {t("Zur Live Karte")}
      </ToolbarButton>
      <ToolbarButton type="button">
        <ToolbarButtonIcon>
          <span className="icon-ico_check" />
        </ToolbarButtonIcon>
        {t("Als bearbeitet markieren")}
      </ToolbarButton>
      <ToolbarButton type="button">
        <ToolbarButtonIcon>
          <span className="icon-ico_unread" />
        </ToolbarButtonIcon>
        {t("Als ungelesen markieren")}
      </ToolbarButton>
      <ToolbarButton type="button">
        <ToolbarButtonIcon>
          <span className="icon-ico_document-new" />
        </ToolbarButtonIcon>
        {t("Neue Notiz")}
      </ToolbarButton>
      <ToolbarButton type="button">
        <ToolbarButtonIcon>
          <span className="icon-ico_delete" />
        </ToolbarButtonIcon>
        {t("Löschen")}
      </ToolbarButton>
      {/*
      <ToolbarButton type="button">
          <ToolbarButtonIcon>
          <span className="icon-ico_export" />
          </ToolbarButtonIcon>
          {t("Exportieren")}
      </ToolbarButton>
      */}
    </ActionContainer>
  );
};

export default AlertActions;
