import React, {useCallback, useEffect, useState} from "react";
import Page from "../../../../components/Page";

import DataTable, {
  bodyField,
  newField,
  TableContainer,
  TablePageContent,
  TableStickyFooter,
  TableStickyHeader,
  ToolbarButton,
  ToolbarButtonIcon,
  useColumns,
  TableSelectionCountInfo,
} from "../../../../components/DataTable";
import {Alert} from "./state/reducer";
import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import {loadAlertsIfNeeded} from "./state/actions";
import {FormControlLabel, InputAdornment, MenuItem, Popover, Select} from "@material-ui/core";
import {Button, TextField} from "dkv-live-frontend-ui";
import Window from "../../../../components/Window";
import AlertDetails from "./components/AlertDetails";
import AlertActions from "./components/AlertActions";
import store from "../../../../state/store";
import {AlertsModule} from "./state/module";
import styled, {css} from "styled-components";
import Switch from "@material-ui/core/Switch";
import dayjs from "dayjs";
import DayjsUtils from "@date-io/dayjs";
import ActivityList from "./components/ActivityList";
import {alertsListSelector} from "./state/selectors";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import {resolveUserDayFormat, resolveUserDateLocale} from "app/modules/login/state/login/selectors";
import ComingSoon from "app/components/ComingSoon";

// -----------------------------------------------------------------------------------------------------------
store.addModule(AlertsModule);

// -----------------------------------------------------------------------------------------------------------
const DatePickerFilter = styled.span`
  padding-bottom: 15px;
  margin-left: 30px;
  .MuiTextField-root {
    width: 200px;
    margin-left: 1em !important;
  }
`;

const TableHeader = styled(TableStickyHeader)`
  display: block;

  h1 {
    display: inline;
  }
`;

const ActivityPopover = styled(Popover)`
  .MuiPaper-root {
    border-radius: 0 !important;
  }
`;

interface AlertFilterProps {
  type?: string;
}

const AlertFilter = styled.div<AlertFilterProps>`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;

  .MuiSelect-select.MuiSelect-select {
    font-weight: 500 !important;
    
    ${(props) =>
      props.type === "alarm" &&
      css`
        color: var(--dkv-highlight-error-color) !important;
      `}
      
    ${(props) =>
      props.type === "notice" &&
      css`
        color: var(--dkv-highlight-warning-color) !important;
      `}
      
    ${(props) =>
      props.type === "info" &&
      css`
        color: var(--dkv-highlight-info-color) !important;
      `}
  }
`;

/*
const CommentIcon = styled.div`
  cursor: pointer;
`;
 */

// -----------------------------------------------------------------------------------------------------------
const alertsColumns = [
  {field: "alarm.type", width: 0, hidden: true},
  {field: "license_plate", header: "Fahrzeug", width: 100, sortable: true, filterable: false, body: newField},
  {
    field: "time",
    header: "Datum / Zeit",
    width: 125,
    sortable: true,
    filterable: false,
    body: bodyField((a: Alert) => dayjs(a.start_time).format("DD.MM. / HH:mm")),
  },
  {
    field: "start_time",
    header: "Von - Bis",
    width: 125,
    sortable: true,
    filterable: false,
    body: bodyField(
      (a: Alert) => dayjs(a.start_time).format("HH:mm") + " - " + dayjs(a.end_time).format("HH:mm")
    ),
  },
  {field: "alarm.name", header: "Vorgang", width: 200, sortable: true, filterable: false},
  {field: "address", header: "Ort", width: 300, sortable: true, filterable: false},
  {
    field: "driver_id",
    header: "Fahrer",
    width: 150,
    sortable: true,
    filterable: false,
    body: bodyField((a: Alert) => a.driver.firstname + " " + a.driver.lastname),
  },
  /*{field: "driver.phone", header: "Tel.Nr. Fahrer", width: 150, sortable: true, filterable: false},*/
  {field: "acknowledged_by", header: "Bearbeitet von", width: 150, sortable: true, filterable: false},
  /*
  {
    field: "",
    header: "Kommentare",
    width: 100,
    sortable: true,
    filterable: false,
    body: (a: Alert) => a.comments && <CommentCell rowData={a.comments} />,
  },
   */
];

// -----------------------------------------------------------------------------------------------------------
/*
const CommentCell = React.memo((props: any) => {
  const handleClick = () => {
    console.log("TEST");
  };

  return (
    <CommentIcon onClick={handleClick}>
      <span className="icon-ico_document" /> {props.rowData.length}
    </CommentIcon>
  );
});
 */

// -----------------------------------------------------------------------------------------------------------
const Alerts: React.FC = () => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // load alerts
  const alertsList = useSelector(alertsListSelector);

  useEffect(() => {
    dispatch(loadAlertsIfNeeded());
  }, [dispatch]);

  // actions
  // - details
  const [showDetailWindow, setShowDetailWindow] = useState<Alert | null>(null);
  const onCloseDetailWindow = useCallback(() => setShowDetailWindow(null), [setShowDetailWindow]);
  const openDetailWindow = useCallback((e: any) => setShowDetailWindow(e.data as Alert), [
    setShowDetailWindow,
  ]);

  useEffect(() => {
    if (!showDetailWindow) {
      return;
    }

    const alert = alertsList.find((a) => a.id === showDetailWindow.id);

    if (alert) {
      setShowDetailWindow(alert);
    }
  }, [alertsList, showDetailWindow]);

  // columns
  const [columns] = useState(alertsColumns);
  const visibleColumns = useColumns(columns, t);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  // alert type filter
  const [type, setType] = React.useState("all");
  const handleTypeChange = (event: React.ChangeEvent<{value: unknown}>) => {
    setType(event.target.value as string);
  };

  // activity popover
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleOpen = (e: any) => setAnchorEl(e.currentTarget);
  const handleClose = () => setAnchorEl(null);

  const rowClassName = (rowData: any) => {
    const options: any = {};
    switch (rowData.alarm.type) {
      case 0:
        options.alarm = true;
        break;
      case 1:
        options.notice = true;
        break;
      case 2:
        options.information = true;
        break;
    }
    options["dkv-row-new"] = rowData.new === true;

    return options;
  };

  // date filter
  const [selectedFromDate, setSelectedFromDate] = React.useState(new Date("2020-06-01T00:00:00"));
  const handleFromDateChange = (date: any) => {
    setSelectedFromDate(date);
  };

  const [selectedToDate, setSelectedToDate] = React.useState(new Date("2020-06-30T00:00:00"));
  const handleToDateChange = (date: any) => {
    setSelectedToDate(date);
  };

  // new alert filter
  const [newAlertFilter, setNewAlertFilter] = React.useState(false);
  const handleNewAlertFilterChange = (_: React.ChangeEvent<unknown>, checked: boolean) => {
    setNewAlertFilter(checked);
  };

  // filter alerts list
  const filterAlertList = () => {
    let filterAlertList: any[] = alertsList;
    if (newAlertFilter) {
      filterAlertList = filterAlertList.filter((a) => a.new === true);
    }

    switch (type) {
      case "alarm":
        filterAlertList = filterAlertList.filter((a) => a.alarm.type === 0);
        break;
      case "notice":
        filterAlertList = filterAlertList.filter((a) => a.alarm.type === 1);
        break;
      case "info":
        filterAlertList = filterAlertList.filter((a) => a.alarm.type === 2);
        break;
    }

    return filterAlertList;
  };

  // render
  return (
    <Page id="alerts">
      <ComingSoon /> {/* @todo commingsoon */}
      <TablePageContent>
        <TableHeader>
          <h1>{t("Meldungen")}</h1>

          <DatePickerFilter>
            <MuiPickersUtilsProvider utils={DayjsUtils} locale={resolveUserDateLocale()}>
              <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format={resolveUserDayFormat()}
                margin="none"
                value={selectedFromDate}
                onChange={handleFromDateChange}
                TextFieldComponent={TextField as any}
                InputProps={{
                  startAdornment: <InputAdornment position="start">{t("Von")}</InputAdornment>,
                }}
                invalidDateMessage={t("Ungültiges Datum")}
                keyboardIcon={<i className="icon-ico_cal-week" />}
                PopoverProps={{
                  className: "dkv-datetimepicker",
                }}
              />
              <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format={resolveUserDayFormat()}
                margin="none"
                value={selectedToDate}
                onChange={handleToDateChange}
                TextFieldComponent={TextField as any}
                InputProps={{
                  startAdornment: <InputAdornment position="start">{t("Bis")}</InputAdornment>,
                }}
                invalidDateMessage={t("Ungültiges Datum")}
                keyboardIcon={<i className="icon-ico_cal-week" />}
                PopoverProps={{
                  className: "dkv-datetimepicker",
                }}
              />
            </MuiPickersUtilsProvider>
          </DatePickerFilter>

          <AlertFilter type={type}>
            <Select onChange={handleTypeChange} value={type} style={{minWidth: 250}}>
              <MenuItem value="all">{t("Alle Meldungen")}</MenuItem>
              <MenuItem value="alarm" style={{color: "var(--dkv-highlight-error-color)"}}>
                {t("Alarme")} ({alertsList.filter((a) => a.alarm.type === 0).length})
              </MenuItem>
              <MenuItem value="notice" style={{color: "var(--dkv-highlight-warning-color)"}}>
                {t("Hinweise")} ({alertsList.filter((a) => a.alarm.type === 1).length})
              </MenuItem>
              <MenuItem value="info" style={{color: "var(--dkv-highlight-info-color)"}}>
                {t("Informationen")} ({alertsList.filter((a) => a.alarm.type === 2).length})
              </MenuItem>
            </Select>

            <FormControlLabel
              control={<Switch color="default" />}
              label={t("Nur neue Meldungen anzeigen")}
              labelPlacement="start"
              onChange={handleNewAlertFilterChange}
              style={{margin: "0 2em 0 8em"}}
            />

            {/* @todo commingsoon */}
            {/* <SearchTextField
              name="search"
              type="text"
              required
              label={t("Suche")}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <SearchIcon className="icon-ico_search" />
                  </InputAdornment>
                ),
              }}
            /> */}

            <ToolbarButton general type="button" onClick={handleOpen}>
              <ToolbarButtonIcon>
                <span className="icon-ico_activity" />
              </ToolbarButtonIcon>
              {t("Aktivität")}
            </ToolbarButton>

            <ActivityPopover
              open={open}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "center",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
            >
              <ActivityList />
            </ActivityPopover>
          </AlertFilter>
        </TableHeader>

        <TableContainer>
          <DataTable
            items={filterAlertList()}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            onRowDoubleClick={openDetailWindow}
            rowClassName={rowClassName}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Meldung ausgewählt" : "Meldungen ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>
              <AlertActions />
            </>
          )}

          {/* <TableFooterRightActions>
            <Button autoWidth secondary ariaLabel={t("Gesamte Liste exportieren")}>
              {t("Gesamte Liste exportieren")}
            </Button>

            {selected.length > 0 && (
              <Button autoWidth ariaLabel={t("Auswahl als Liste exportieren")}>
                {t("Auswahl als Liste exportieren")}
              </Button>
            )}
          </TableFooterRightActions> */}
        </TableStickyFooter>
      </TablePageContent>
      {showDetailWindow && (
        <Window onClose={onCloseDetailWindow} headline={t("Fahrer")}>
          {(props) => <AlertDetails {...props} alert={showDetailWindow} />}
        </Window>
      )}
    </Page>
  );
};

export default Alerts;
