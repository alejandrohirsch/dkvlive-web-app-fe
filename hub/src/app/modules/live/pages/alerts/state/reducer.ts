import {createReducer} from "@reduxjs/toolkit";
import {
  LoadingAlertsFailedAction,
  AlertsLoadedAction,
  LOADING_ALERTS,
  LOADING_ALERTS_FAILED,
  ALERTS_LOADED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface Alert {
  id: string;
  alarm: Alarm;
  license_plate: string | null;
  asset_id: string | null;
  driver: Driver;
  address: string | null;
  start_time: string;
  end_time: string;
  comments: Comment[] | null;
  activity: Activity[] | null;
  acknowledged_at: string;
  acknowledged_by: string;
}

export interface Alarm {
  id: string;
  name: string | null;
  type: number | null;
}

export interface Driver {
  id: string;
  firstname: string | null;
  lastname: string | null;
  phone: string | null;
}

export interface Comment {
  id: string;
  username: string;
  comment: string;
  created_at: string;
}

export interface Activity {
  id: string;
  type: string;
  license_plate: string;
  username: string;
  alarm_name: string;
  created_at: string;
}

// -----------------------------------------------------------------------------------------------------------
export interface AlertsState {
  loading: boolean;
  loaded?: boolean;
  error?: boolean;
  errorText?: string;
  items?: {[id: string]: Alert};
}

const initialState: AlertsState = {
  loading: true,
};

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingAlerts(state: AlertsState) {
  state.loading = true;
  state.loaded = false;

  if (state.error) {
    delete state.error;
    delete state.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingAlertsFailed(state: AlertsState, action: LoadingAlertsFailedAction) {
  state.loading = false;
  state.error = true;
  state.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleAlertsLoaded(state: AlertsState, action: AlertsLoadedAction) {
  state.loading = false;
  state.loaded = true;

  const items = {};
  action.payload.forEach((a) => {
    items[a.id] = a;
  });

  state.items = items;

  if (state.error) {
    delete state.error;
    delete state.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export const alertsReducer = createReducer(initialState, {
  [LOADING_ALERTS]: handleLoadingAlerts,
  [LOADING_ALERTS_FAILED]: handleLoadingAlertsFailed,
  [ALERTS_LOADED]: handleAlertsLoaded,
});
