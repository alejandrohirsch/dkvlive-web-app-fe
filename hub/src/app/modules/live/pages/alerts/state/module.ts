import {IModule} from "redux-dynamic-modules";
import {AlertsActions} from "./types";
import {ThunkAction} from "redux-thunk";
import {AlertsState, alertsReducer} from "./reducer";

export interface AlertsModuleState {
  alerts: AlertsState;
}

export type AlertsThunkResult<R> = ThunkAction<R, AlertsModuleState, void, AlertsActions>;

export const AlertsModule: IModule<AlertsModuleState> = {
  id: "alerts",
  reducerMap: {
    alerts: alertsReducer,
  },
};
