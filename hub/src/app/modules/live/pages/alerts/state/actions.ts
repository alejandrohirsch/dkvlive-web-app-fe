import {AlertsThunkResult} from "./module";
import {Alert} from "./reducer";
//import httpClient from "services/http";
import {
  LoadingAlertsAction,
  LOADING_ALERTS,
  AlertsLoadedAction,
  ALERTS_LOADED,
  //LoadingAlertsFailedAction,
  //LOADING_ALERTS_FAILED,
} from "./types";

const dummyAlerts: any[] = [
  {
    id: 1,
    alarm: {
      type: 0,
      name: "Fahrt ohne Fahrerkarte",
    },
    driver: {
      firstname: "Klaus",
      lastname: "Lenmann",
      phone: "01511 997 67 98",
    },
    address: "D-09435, Arnsberg, Möhnestraße 10",
    acknowledged_by: "",
    license_plate: "KU124CD",
    start_time: "2020-06-22T15:08:48.544Z",
    end_time: "2020-06-22T16:22:12.544Z",
    new: true,
  },
  {
    id: 2,
    alarm: {
      type: 1,
      name: "Auffällige Betankung",
    },
    driver: {
      firstname: "Steffen",
      lastname: "Sankt",
      phone: "01515 121 76 46",
    },
    address: "D-22452, Shell, n. Freiburg",
    acknowledged_by: "",
    license_plate: "KU125EF",
    start_time: "2020-06-22T14:43:48.544Z",
    end_time: "2020-06-22T14:54:12.544Z",
    new: true,
  },
  {
    id: 3,
    alarm: {
      type: 2,
      name: "Betankung stattgefunden",
    },
    driver: {
      firstname: "Max",
      lastname: "Zweig",
      phone: "0162 536 99 22",
    },
    address: "D-56357, Rheinland-Pfalz, Joachimstaler Str. 29",
    acknowledged_by: "",
    license_plate: "KU434YC",
    start_time: "2020-06-22T12:08:48.544Z",
    end_time: "2020-06-22T12:39:12.544Z",
    new: true,
  },
  {
    id: 4,
    alarm: {
      type: 0,
      name: "Geschwindigkeit Überschreitung",
    },
    driver: {
      firstname: "Andreas",
      lastname: "Holzman",
      phone: "0174 426 80 40",
    },
    comments: [
      {
        id: 1,
        username: "Diana Neustadt",
        comment: "Fahrer telefonisch nicht erreicht.",
        created_at: "2020-06-20T10:17:50.544Z",
      },
      {
        id: 2,
        username: "Diana Neustadt",
        comment: "Fahrer wurde kontaktiert und über die Überschreitung informiert.",
        created_at: "2020-06-20T12:43:50.544Z",
      },
    ],
    address: "A-1160, Wien, Floridusgasse 75",
    acknowledged_by: "Diana Neustadt",
    license_plate: "KU124CD",
    start_time: "2020-06-20T08:12:34.544Z",
    end_time: "2020-06-20T08:17:50.544Z",
  },
  {
    id: 5,
    alarm: {
      type: 0,
      name: "Lenkzeit Überschreitung",
    },
    driver: {
      firstname: "Swen",
      lastname: "Blau",
      phone: "0170 947 40 44",
    },
    address: "A12 Inntal Autobahn, Rosenheim - Kufstein",
    acknowledged_by: "Nicole Theiss",
    license_plate: "KU123AB",
    start_time: "2020-06-19T14:54:48.544Z",
    end_time: "2020-06-19T15:32:12.544Z",
  },
  {
    id: 6,
    alarm: {
      type: 1,
      name: "Serviceintervall Überschreitung",
    },
    driver: {
      firstname: "Tim",
      lastname: "Eichel",
      phone: "0175 787 93 50",
    },
    address: "D-12555, Berlin, Brandenburgische Straße 55",
    acknowledged_by: "Thomas Fried",
    license_plate: "KU125EF",
    start_time: "2020-06-19T12:08:48.544Z",
    end_time: "2020-06-19T12:39:12.544Z",
  },
  {
    id: 7,
    alarm: {
      type: 1,
      name: "Signal verloren",
    },
    driver: {
      firstname: "Jörg",
      lastname: "Bosch",
      phone: "0173 860 98 19",
    },
    address: "D-80903, München, Rhinstrasse 32",
    acknowledged_by: "Diana Neustadt",
    license_plate: "KU422YC",
    start_time: "2020-06-17T08:43:48.544Z",
    end_time: "2020-06-17T08:45:12.544Z",
  },
  {
    id: 8,
    alarm: {
      type: 2,
      name: "Geburtstag",
    },
    driver: {
      firstname: "Dieter",
      lastname: "Fink",
      phone: "01515 439 81 05",
    },
    address: "-",
    acknowledged_by: "Thomas Fried",
    license_plate: "-",
    start_time: "2020-06-15T10:00:48.544Z",
    end_time: "2020-06-15T10:05:12.544Z",
  },
];

// -----------------------------------------------------------------------------------------------------------
function loadingAlertsAction(): LoadingAlertsAction {
  return {
    type: LOADING_ALERTS,
  };
}

// -----------------------------------------------------------------------------------------------------------
/*
function loadingAlertsFailedAction(errorText?: string): LoadingAlertsFailedAction {
  return {
    type: LOADING_ALERTS_FAILED,
    payload: {errorText},
  };
}
 */

// -----------------------------------------------------------------------------------------------------------
function alertsLoadedAction(items: Alert[]): AlertsLoadedAction {
  return {
    type: ALERTS_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadAlerts(): AlertsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingAlertsAction());

    /*
    const [{data}, err] = await httpClient.get("/alarm/alerts?from=2020-06-01&to=2020-06-04");
    if (err !== null) {
      // @todo: log
      dispatch(loadingAlertsFailedAction("Laden fehlgeschlagen"));
      return;
    }
     */

    dispatch(alertsLoadedAction(dummyAlerts));
  };
}

export function loadAlertsIfNeeded(): AlertsThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().alerts;
    const isLoaded = data.items !== undefined && data.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadAlerts());
  };
}
