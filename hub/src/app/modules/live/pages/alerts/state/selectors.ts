import {AlertsModuleState} from "./module";
import {createSelector} from "@reduxjs/toolkit";

export const alertsItemsSelector = (state: AlertsModuleState) => state.alerts.items;

// list
export const alertsListSelector = createSelector(alertsItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// alert
export const alertSelector = (id: string) =>
  createSelector(alertsItemsSelector, (items) => (items ? items[id] : undefined));
