import {Alert} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ALERTS = "map/loadingAlerts";

export interface LoadingAlertsAction {
  type: typeof LOADING_ALERTS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ALERTS_FAILED = "map/loadingAlertsFailed";

export interface LoadingAlertsFailedAction {
  type: typeof LOADING_ALERTS_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const ALERTS_LOADED = "map/alertsLoaded";

export interface AlertsLoadedAction {
  type: typeof ALERTS_LOADED;
  payload: Alert[];
}

// -----------------------------------------------------------------------------------------------------------
export type AlertsActions = LoadingAlertsAction | LoadingAlertsFailedAction | AlertsLoadedAction;
