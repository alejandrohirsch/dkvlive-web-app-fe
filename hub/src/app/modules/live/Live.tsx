import React, {useEffect, useState, useRef} from "react";
import {Route, Switch, useRouteMatch} from "react-router-dom";
import styled from "styled-components/macro";
import Nav, {NavItem} from "../../components/Nav";
import {Loading} from "dkv-live-frontend-ui";
import {LiveModule} from "./state/module";
import store from "app/state/store";
import Websocket from "./Websocket";
import {hasPermission, hasOnePermission} from "../login/state/login/selectors";

import "./styles/here-mapsjs-ui-3.1.css";
import "./styles/here-map-ui.scss";
import {Growl} from "primereact/growl";

const Dashboard = React.lazy(() => import("./pages/dashboard/Dashboard"));
const Map = React.lazy(() => import("./pages/map/Map"));
const List = React.lazy(() => import("./pages/list/List"));
const Drivers = React.lazy(() => import("./pages/drivers/Drivers"));
const Reporting = React.lazy(() => import("./pages/reporting/Reporting"));
const Alerts = React.lazy(() => import("./pages/alerts/Alerts"));
const RemoteDownload = React.lazy(() => import("./pages/remote-download/RemoteDownload"));
const Management = React.lazy(() => import("./pages/management/Management"));

// -----------------------------------------------------------------------------------------------------------
store.addModule(LiveModule);

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: row;
  overflow: hidden;
`;

// -----------------------------------------------------------------------------------------------------------
const Live = () => {
  // route
  const {path} = useRouteMatch();

  // nav items
  const [navItems, setNavItems] = useState<NavItem[]>([]);

  useEffect(() => {
    const navItems: NavItem[] = [];

    if (hasPermission("dashboard:Visible")) {
      navItems.push({
        to: path,
        exact: true,
        label: "Dashboard",
        icon: "icon-ico_dashboard",
      });
    }

    if (hasPermission("map:Visible")) {
      navItems.push({
        to: `${path}live`,
        label: "Live Karte",
        icon: "icon-ico_route-active",
      });
    }

    if (hasPermission("alarm:ListAlerts")) {
      navItems.push({
        to: `${path}alerts`,
        label: "Meldungen",
        icon: "icon-ico_notifications",
      });
    }

    if (hasOnePermission("reporting:ListLogbook")) {
      navItems.push({
        to: `${path}reporting`,
        label: "Auswertungen",
        icon: "icon-ico_reporting",
      });
    }

    if (hasPermission("list:Visible")) {
      navItems.push({
        to: `${path}list`,
        label: "Fahrzeuge",
        icon: "icon-ico_fleet",
      });
    }

    if (hasPermission("management:ListDrivers")) {
      navItems.push({
        to: `${path}drivers`,
        label: "Fahrer",
        icon: "icon-ico_driver",
      });
    }

    if (hasOnePermission("dtco:ListRDLMass", "dtco:ListRDLDriver")) {
      navItems.push({
        to: `${path}rdl`,
        label: "Tachograph",
        icon: "icon-ico_tacho",
      });
    }

    if (hasOnePermission("management:ListUsers", "management:ListPolicies", "alarm:ListAlarms")) {
      navItems.push({
        to: `${path}management`,
        label: "Verwaltung",
        icon: "icon-ico_settings",
      });
    }

    setNavItems(navItems);
  }, [path]);

  // growl
  const growlRef = useRef<any>();
  useEffect(() => {
    const onGrowl = (e: any) => {
      if (!growlRef.current) {
        return;
      }

      growlRef.current.show(e.detail);
    };

    window.addEventListener("dkv-growl", onGrowl);

    return () => {
      window.removeEventListener("dkv-growl", onGrowl);
    };
  }, []);

  // render
  return (
    <Container>
      <Nav items={navItems} />
      <React.Suspense fallback={<Loading inprogress />}>
        <Switch>
          {hasPermission("dashboard:Visible") && (
            <Route exact path={path}>
              <Dashboard />
            </Route>
          )}

          {hasPermission("map:Visible") && (
            <Route path={`${path}live`}>
              <Map />
            </Route>
          )}

          {hasPermission("alerts:Visible") && (
            <Route path={`${path}alerts`}>
              <Alerts />
            </Route>
          )}

          {hasPermission("list:Visible") && (
            <Route path={`${path}list`}>
              <List />
            </Route>
          )}

          {hasPermission("drivers:Visible") && (
            <Route path={`${path}drivers/:id`}>
              <Drivers />
            </Route>
          )}

          {hasPermission("drivers:Visible") && (
            <Route path={`${path}drivers`}>
              <Drivers />
            </Route>
          )}

          {hasPermission("reporting:Visible") && (
            <Route path={`${path}reporting`}>
              <Reporting />
            </Route>
          )}

          {hasPermission("rdl:Visible") && (
            <Route path={`${path}rdl`}>
              <RemoteDownload />
            </Route>
          )}

          {hasPermission("management:Visible") && (
            <Route path={`${path}management`}>
              <Management />
            </Route>
          )}
        </Switch>
      </React.Suspense>

      <Websocket />

      <Growl ref={(el) => (growlRef.current = el)} baseZIndex={20000} />
    </Container>
  );
};

export default Live;
