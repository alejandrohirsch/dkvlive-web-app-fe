import {AssetTypesThunkResult} from "./module";
import {AssetType} from "./reducer";
import httpClient from "services/http";
import {
  LoadingAssetTypesAction,
  LOADING_ASSETTYPES,
  AssetTypesLoadedAction,
  ASSETTYPES_LOADED,
  LoadingAssetTypesFailedAction,
  LOADING_ASSETTYPES_FAILED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingAssetTypesAction(): LoadingAssetTypesAction {
  return {
    type: LOADING_ASSETTYPES,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingAssetTypesFailedAction(errorText?: string): LoadingAssetTypesFailedAction {
  return {
    type: LOADING_ASSETTYPES_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function assetTypesLoadedAction(items: AssetType[]): AssetTypesLoadedAction {
  return {
    type: ASSETTYPES_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadAssetTypes(): AssetTypesThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingAssetTypesAction());

    const [{data}, err] = await httpClient.get("/management/assets/types");
    if (err !== null) {
      // @todo: log
      dispatch(loadingAssetTypesFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(assetTypesLoadedAction(data));
  };
}

export function loadAssetTypesIfNeeded(): AssetTypesThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().assetTypes;
    const isLoaded = data.items !== undefined && data.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadAssetTypes());
  };
}

export function deleteAssetTypes(ids: string[]): AssetTypesThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingAssetTypesAction());

    for (const id of ids) {
      const [, err] = await httpClient.delete(`/management/assets/types/${id}`);
      if (err !== null) {
        // @todo: log
        dispatch(loadingAssetTypesFailedAction("Löschen fehlgeschlagen"));
        return;
      }
    }

    dispatch(loadAssetTypes());
  };
}
