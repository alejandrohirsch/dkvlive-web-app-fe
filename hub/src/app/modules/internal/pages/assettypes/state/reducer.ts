import {createReducer} from "@reduxjs/toolkit";
import {
  LoadingAssetTypesFailedAction,
  AssetTypesLoadedAction,
  LOADING_ASSETTYPES,
  LOADING_ASSETTYPES_FAILED,
  ASSETTYPES_LOADED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface AssetType {
  id: string;
  name: string;
  icon_class_name: string;
  created_at: string;
  modified_at: string;
}

// -----------------------------------------------------------------------------------------------------------
export interface AssetTypesState {
  loading: boolean;
  loaded?: boolean;
  error?: boolean;
  errorText?: string;
  items?: {[id: string]: AssetType};
}

const initialState: AssetTypesState = {
  loading: true,
};

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingAssetTypes(state: AssetTypesState) {
  state.loading = true;
  state.loaded = false;

  if (state.error) {
    delete state.error;
    delete state.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingAssetTypesFailed(state: AssetTypesState, action: LoadingAssetTypesFailedAction) {
  state.loading = false;
  state.error = true;
  state.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleAssetTypesLoaded(state: AssetTypesState, action: AssetTypesLoadedAction) {
  state.loading = false;
  state.loaded = true;

  const items = {};
  action.payload.forEach((a) => {
    items[a.id] = a;
  });

  state.items = items;

  if (state.error) {
    delete state.error;
    delete state.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export const assetTypesReducer = createReducer(initialState, {
  [LOADING_ASSETTYPES]: handleLoadingAssetTypes,
  [LOADING_ASSETTYPES_FAILED]: handleLoadingAssetTypesFailed,
  [ASSETTYPES_LOADED]: handleAssetTypesLoaded,
});
