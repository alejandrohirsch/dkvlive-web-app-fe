import {AssetType} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ASSETTYPES = "assettypes/loading";

export interface LoadingAssetTypesAction {
  type: typeof LOADING_ASSETTYPES;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ASSETTYPES_FAILED = "assettypes/loadingFailed";

export interface LoadingAssetTypesFailedAction {
  type: typeof LOADING_ASSETTYPES_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const ASSETTYPES_LOADED = "assettypes/loaded";

export interface AssetTypesLoadedAction {
  type: typeof ASSETTYPES_LOADED;
  payload: AssetType[];
}

// -----------------------------------------------------------------------------------------------------------
export type AssetTypesActions =
  | LoadingAssetTypesAction
  | LoadingAssetTypesFailedAction
  | AssetTypesLoadedAction;
