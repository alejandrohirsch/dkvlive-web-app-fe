import {IModule} from "redux-dynamic-modules";
import {AssetTypesActions} from "./types";
import {ThunkAction} from "redux-thunk";
import {AssetTypesState, assetTypesReducer} from "./reducer";

export interface AssetTypesModuleState {
  assetTypes: AssetTypesState;
}

export type AssetTypesThunkResult<R> = ThunkAction<R, AssetTypesModuleState, void, AssetTypesActions>;

export const AssetTypesModule: IModule<AssetTypesModuleState> = {
  id: "assetTypes",
  reducerMap: {
    assetTypes: assetTypesReducer,
  },
};
