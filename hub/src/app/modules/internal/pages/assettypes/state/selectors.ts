import {AssetTypesModuleState} from "./module";
import {createSelector} from "@reduxjs/toolkit";

export const assetTypesItemsSelector = (state: AssetTypesModuleState) => state.assetTypes.items;

// this.state.
export const assetTypesStateSelector = (state: AssetTypesModuleState) => state.assetTypes;

// list
export const assetTypesListSelector = createSelector(assetTypesItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// assetType
export const assetTypeSelector = (id: string) =>
  createSelector(assetTypesItemsSelector, (items) => (items ? items[id] : undefined));
