import React, {useState, useCallback, useEffect, MouseEventHandler} from "react";
import {useTranslation} from "react-i18next";
import Page from "app/components/Page";
import {Button, Loading, Message} from "dkv-live-frontend-ui";
import DataTable, {
  TablePageContent,
  TableStickyHeader,
  TableContainer,
  TableStickyFooter,
  useColumns,
  ToolbarButton,
  ToolbarButtonIcon,
  TableFooterActions,
  TableSelectionCountInfo,
  TableFooterRightActions,
} from "app/components/DataTable";
import Window from "app/components/Window";
import {useSelector, useDispatch} from "react-redux";
import ConfirmWindow from "app/components/ConfirmWindow";
import {AssetTypesModule} from "./state/module";
import store from "app/state/store";
import {loadAssetTypesIfNeeded, loadAssetTypes, deleteAssetTypes} from "./state/actions";
import {assetTypesListSelector, assetTypesStateSelector} from "./state/selectors";
import httpClient from "services/http";
import AssetTypeForm, {AssetTypeCreateData} from "./components/AssetTypeForm";
import {AssetType} from "./state/reducer";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
store.addModule(AssetTypesModule);

// -----------------------------------------------------------------------------------------------------------
const AssetTypes: React.FC = () => {
  const {t} = useTranslation();

  const listColumns = [
    {field: "id", header: "ID", width: 150, sortable: false},
    {field: "name", header: "Name", width: 150, sortable: false},
    {
      field: "icon_class_name",
      header: "Icon",
      body: (at: AssetType) => <div className={at.icon_class_name} style={{fontSize: "24px"}} />,
      width: 150,
      sortable: false,
    },
  ];

  const dispatch = useDispatch();

  // assetTypes
  const assetTypesListState = useSelector(assetTypesStateSelector);
  const assetTypes = useSelector(assetTypesListSelector);

  useEffect(() => {
    dispatch(loadAssetTypesIfNeeded());
  }, [dispatch]);

  const refresh = useCallback(() => dispatch(loadAssetTypes()), [dispatch]);

  // organizations
  const [organizations, setOrganizations] = useState<any>(null);
  const [error, setError] = useState("");

  useEffect(() => {
    const load = async () => {
      const [{data}, err] = await httpClient.get("/management/organizations/findWithDivisions");
      if (err !== null) {
        setError("Laden fehlgeschlagen");
        return;
      }

      setOrganizations(data);
    };

    load();
  }, []);

  // edit
  const [editData, setEditData] = useState<AssetTypeCreateData | undefined>();

  const showEditWndow = useCallback(
    (e: any) => {
      if (!hasPermission("superadmin:EditAssetTypes")) {
        return;
      }

      const org = e.data.organization_id
        ? organizations.find((o: any) => o.id === e.data.organization_id)
        : null;

      const divs: any[] = [];
      if (org && e.data.division_id) {
        e.data.division_id.forEach((dID: string) => {
          const div = org.divisions.find((d: any) => d.id === dID);
          if (div) {
            divs.push(div);
          }
        });
      }

      setEditData({
        id: e.data.id,
        name: e.data.name ?? "",
        icon_class_name: e.data.icon_class_name ?? "",
      });

      setShowNewWindow(true);
    },
    [setEditData, organizations]
  );

  // new window
  const [showNewWindow, setShowNewWindow] = useState(false);

  const onShowNewWindow = useCallback(() => {
    setEditData(undefined);
    setShowNewWindow(true);
  }, [setShowNewWindow]);

  const onCloseNewWindow = useCallback(() => setShowNewWindow(false), [setShowNewWindow]);

  // columns
  const [columns] = useState(listColumns);
  const visibleColumns = useColumns(columns);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  useEffect(() => {
    setSelected((selected) =>
      assetTypes && selected ? selected.map((s) => assetTypes.find((c) => c.id === s.id)) : []
    );
  }, [setSelected, assetTypes]);

  // delete
  const [deleteIDs, setDeleteIDs] = useState<string[]>([]);

  const showDeleteConfirm: MouseEventHandler<HTMLButtonElement> = useCallback(() => {
    setDeleteIDs(Array.from(selected.map((s) => s.id)));
  }, [setDeleteIDs, selected]);

  const onDeleteConfirm = useCallback(
    (confirmed: boolean) => {
      if (!confirmed) {
        setDeleteIDs([]);
        return;
      }

      dispatch(deleteAssetTypes(deleteIDs));
      setDeleteIDs([]);
      setSelected([]);
    },
    [deleteIDs, setDeleteIDs, dispatch]
  );

  // render
  return (
    <Page id="assettypes">
      <Loading inprogress={assetTypesListState.loading} />

      <TablePageContent>
        <TableStickyHeader>
          <h1>{t("Fahrzeugarten")}</h1>

          {hasPermission("superadmin:CreateAssetTypes") && (
            <ToolbarButton
              general
              style={{marginLeft: "auto"}}
              disabled={!organizations}
              onClick={onShowNewWindow}
            >
              <ToolbarButtonIcon>
                <span className="icon-ico_add" />
              </ToolbarButtonIcon>
              {t("Hinzufügen")}
            </ToolbarButton>
          )}

          <ToolbarButton general onClick={refresh}>
            <ToolbarButtonIcon>
              <span className="icon-ico_load" />
            </ToolbarButtonIcon>
            {t("Aktualisieren")}
          </ToolbarButton>
        </TableStickyHeader>

        <TableContainer>
          {error && <Message text={t(error)} error />}
          {!error && (
            <DataTable
              items={assetTypes ?? []}
              columns={visibleColumns}
              selected={selected}
              onSelectionChange={onSelectionChange}
              onRowDoubleClick={showEditWndow}
            />
          )}
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 ? (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Eintrag ausgewählt" : "Einträge ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>
              <TableFooterActions>
                {hasPermission("superadmin:DeleteAssetTypes") && (
                  <ToolbarButton onClick={showDeleteConfirm}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_delete" />
                    </ToolbarButtonIcon>
                    {t("Löschen")}
                  </ToolbarButton>
                )}
              </TableFooterActions>
              <TableFooterRightActions />
            </>
          ) : null}
        </TableStickyFooter>
      </TablePageContent>

      {showNewWindow && (
        <Window onClose={onCloseNewWindow} headline={t("Fahrzeugart")}>
          {(props) => <AssetTypeForm editData={editData} {...props} />}
        </Window>
      )}

      {deleteIDs.length ? (
        <ConfirmWindow headline={t("Fahrzeugarten wirklich löschen?")} onConfirm={onDeleteConfirm} />
      ) : null}
    </Page>
  );
};

export default AssetTypes;
