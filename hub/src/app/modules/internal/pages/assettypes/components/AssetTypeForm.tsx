import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {TextField, Button, Message} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import httpClient from "services/http";
import {useDispatch} from "react-redux";
import {loadAssetTypes} from "../state/actions";

// ----------------------------------------------------------------------------------------------------------
export interface AssetTypeCreateData {
  id?: string;
  name: string;
  icon_class_name: string;
}

// ----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
`;

const IconDisplay = styled.div`
  font-size: 50px;
  margin: auto;
`;

// -----------------------------------------------------------------------------------------------------------
interface AssetTypeFormProps extends WindowContentProps {
  editData?: AssetTypeCreateData;
}

const AssetTypeForm: React.FC<AssetTypeFormProps> = ({forceCloseWindow, setLoading, headline, editData}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const editMode = editData !== undefined;

  // form
  const [error, setError] = useState("");

  const form = useFormik<AssetTypeCreateData>({
    initialValues: editData ?? {
      name: "",
      icon_class_name: "",
    },
    onSubmit: (values) => {
      setLoading(true, true, t(editMode ? "wird geändert" : "wird erstellt"));
      setError("");

      const submit = async () => {
        const assetType = {
          name: values.name,
          icon_class_name: values.icon_class_name,
        } as AssetTypeCreateData;
        if (editMode) {
          assetType.id = values.id;
        }

        let err;
        if (editMode) {
          [, err] = await httpClient.put(`/management/assets/types/${values.id}`, JSON.stringify(assetType), {
            headers: {
              "Content-Type": "application/json",
            },
          });
        } else {
          [, err] = await httpClient.post("/management/assets/types", JSON.stringify(assetType), {
            headers: {
              "Content-Type": "application/json",
            },
          });
        }

        if (err !== null) {
          setError(editMode ? "Änderung fehlgeschlagen" : "Erstellen fehlgeschlagen");
          setLoading(false);
          form.setSubmitting(false);
          return;
        }

        dispatch(loadAssetTypes());
        forceCloseWindow();
      };

      submit();
    },
  });

  // render
  return (
    <WindowContentContainer full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {error && <Message style={{marginBottom: 24}} text={t(error)} error />}

          <FormGrid>
            <TextField
              name="name"
              type="text"
              label={t("Name")}
              value={form.values.name}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
              autoFocus
            />

            <TextField
              name="icon_class_name"
              type="text"
              label={t("Icon Klasse")}
              value={form.values.icon_class_name}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
              autoFocus
            />
            <IconDisplay className={form.values.icon_class_name}></IconDisplay>
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !form.values.name}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default AssetTypeForm;
