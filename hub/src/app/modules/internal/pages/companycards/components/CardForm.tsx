import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {TextField, Button, Message, AutocompleteInput} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import httpClient from "services/http";
import {useDispatch} from "react-redux";
import {MuiPickersUtilsProvider, KeyboardDateTimePicker} from "@material-ui/pickers";
import dayjs from "dayjs";
import DayjsUtils from "@date-io/dayjs";
import {loadCompanycards} from "../state/actions";
import {resolveUserDateFormat, resolveUserDateLocale} from "app/modules/login/state/login/selectors";

// ----------------------------------------------------------------------------------------------------------
export interface CompanycardCreateData {
  id?: string;

  organization_id: string;
  division_id: string[];
  card_id: string;
  card_reader_ip: string;
  card_reader_slot?: number;
  valid_until?: string;
  comment?: string;
  company?: string;

  _organization?: any;
  _divisions?: any[];
  _valid_until?: Date | null;
}

// ----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
`;

// -----------------------------------------------------------------------------------------------------------
interface CardFormProps extends WindowContentProps {
  organizations: any[];
  editData?: CompanycardCreateData;
}

const CardForm: React.FC<CardFormProps> = ({
  forceCloseWindow,
  setLoading,
  headline,
  editData,
  organizations,
}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const editMode = editData !== undefined;

  // form
  const [error, setError] = useState("");

  const form = useFormik<CompanycardCreateData>({
    initialValues: editData ?? {
      card_id: "",
      organization_id: "",
      division_id: [],
      card_reader_ip: "",
      card_reader_slot: 0,
      valid_until: dayjs()
        .add(5, "year")
        .toDate()
        .toISOString(),
      comment: "",
      company: "",

      _organization: null,
      _divisions: [],
      _valid_until: dayjs()
        .add(5, "year")
        .toDate(),
    },
    onSubmit: (values) => {
      setLoading(true, true, t(editMode ? "wird geändert" : "wird erstellt"));
      setError("");

      const submit = async () => {
        const formData = new FormData();

        formData.append("card_id", values.card_id);
        formData.append("organization_id", values.organization_id);
        formData.append("division_id", values.division_id ? values.division_id.join(";") : "");
        formData.append("card_reader_ip", values.card_reader_ip ?? "");
        formData.append(
          "card_reader_slot",
          values.card_reader_slot ? values.card_reader_slot.toString() : ""
        );
        formData.append("valid_until", values.valid_until ?? "");
        formData.append("comment", values.comment ?? "");
        formData.append("company", values.company ?? "");

        if (editMode) {
          formData.append("id", values.id || "");
        }

        let err;
        if (editMode) {
          [, err] = await httpClient.put(`/management/companycards/${values.id}`, formData);
        } else {
          [, err] = await httpClient.post("/management/companycards", formData);
        }

        if (err !== null) {
          setError(editMode ? "Änderung fehlgeschlagen" : "Erstellen fehlgeschlagen");
          setLoading(false);
          form.setSubmitting(false);
          return;
        }

        dispatch(loadCompanycards());
        forceCloseWindow();
      };

      submit();
    },
  });

  const onValidUntilChange = (d: any) => {
    form.setFieldValue("_valid_until", d);

    if (d && !isNaN(d.getTime())) {
      form.setFieldValue("valid_until", d.toISOString());
    } else {
      form.setFieldValue("valid_until", "");
    }
  };

  // render
  return (
    <WindowContentContainer full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {error && <Message style={{marginBottom: 24}} text={t(error)} error />}

          <FormGrid>
            <TextField
              name="card_id"
              type="text"
              label={t("Karten ID")}
              value={form.values.card_id}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
              autoFocus
            />

            <AutocompleteInput
              value={form.values._organization}
              onChange={(_: any, newValue: any) => {
                if (newValue) {
                  form.setFieldValue("organization_id", newValue.id);
                  form.setFieldValue("_organization", newValue);
                } else {
                  form.setFieldValue("organization_id", "");
                  form.setFieldValue("_organization", null);
                }

                form.setFieldValue("division_id", []);
                form.setFieldValue("_divisions", []);
              }}
              options={organizations}
              autoHighlight
              getOptionLabel={(option: any) => t(option.name)}
              renderInput={(params: any) => (
                <TextField
                  {...params}
                  label="Organization"
                  variant="outlined"
                  required
                  inputProps={{
                    ...params.inputProps,
                    autoComplete: "new-password",
                  }}
                />
              )}
              clearText={t("Leeren")}
              closeText={t("Schließen")}
              loadingText={t("Lädt...")}
              noOptionsText={t("Keine Optionen")}
              openText={t("Öffnen")}
            />

            <AutocompleteInput
              value={form.values._divisions}
              onChange={(_: any, newValue: any) => {
                if (newValue) {
                  form.setFieldValue(
                    "division_id",
                    newValue.map((v: any) => v.id)
                  );
                  form.setFieldValue("_divisions", newValue);
                } else {
                  form.setFieldValue("division_id", []);
                  form.setFieldValue("_divisions", []);
                }
              }}
              multiple
              options={
                form.values._organization && form.values._organization.divisions
                  ? form.values._organization.divisions
                  : []
              }
              autoHighlight
              getOptionLabel={(option: any) => t(option.name)}
              renderInput={(params: any) => (
                <TextField
                  {...params}
                  label="Divisions"
                  variant="outlined"
                  inputProps={{
                    ...params.inputProps,
                    autoComplete: "new-password",
                  }}
                />
              )}
              clearText={t("Leeren")}
              closeText={t("Schließen")}
              loadingText={t("Lädt...")}
              noOptionsText={t("Keine Optionen")}
              openText={t("Öffnen")}
              disabled={form.isSubmitting || !form.values._organization}
            />

            <TextField
              name="card_reader_ip"
              type="text"
              label={t("Kartenleser IP")}
              value={form.values.card_reader_ip}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />

            <TextField
              name="card_reader_slot"
              type="number"
              label={t("Kartenleser Slot")}
              value={form.values.card_reader_slot}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />

            <MuiPickersUtilsProvider utils={DayjsUtils} locale={resolveUserDateLocale()}>
              <KeyboardDateTimePicker
                disableToolbar
                variant="inline"
                margin="normal"
                ampm={false}
                label={t("Gültig bis")}
                value={form.values._valid_until}
                onChange={onValidUntilChange}
                format={resolveUserDateFormat(false)}
                inputVariant="outlined"
                TextFieldComponent={TextField as any}
                invalidDateMessage={t("Ungültiges Datum")}
                keyboardIcon={<i className="icon-ico_cal-week" />}
                PopoverProps={{
                  className: "dkv-datetimepicker",
                }}
              />
            </MuiPickersUtilsProvider>

            <TextField
              name="comment"
              type="text"
              label={t("Kommentar")}
              value={form.values.comment}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="company"
              type="text"
              label={t("Firma")}
              value={form.values.company}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
            />
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !form.values.card_id}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default CardForm;
