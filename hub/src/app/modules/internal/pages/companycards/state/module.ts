import {IModule} from "redux-dynamic-modules";
import {CompanycardsActions} from "./types";
import {ThunkAction} from "redux-thunk";
import {CompanycardsState, companycardsReducer} from "./reducer";

export interface CompanycardsModuleState {
  companycards: CompanycardsState;
}

export type CompanycardsThunkResult<R> = ThunkAction<R, CompanycardsModuleState, void, CompanycardsActions>;

export const CompanycardsModule: IModule<CompanycardsModuleState> = {
  id: "companycards",
  reducerMap: {
    companycards: companycardsReducer,
  },
};
