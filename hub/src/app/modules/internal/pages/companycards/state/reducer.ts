import {createReducer} from "@reduxjs/toolkit";
import {
  LoadingCompanycardsFailedAction,
  CompanycardsLoadedAction,
  LOADING_COMPANYCARDS,
  LOADING_COMPANYCARDS_FAILED,
  COMPANYCARDS_LOADED,
} from "./types";
import dayjs from "dayjs";
import {resolveUserDateFormat} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
export interface Companycard {
  id: string;
  organization_id: string;
  division_id: string[];
  card_id: string;
  valid_until: string;
  card_reader_ip: string;
  card_reader_slot: number;
  comment: string;
  company: string;
  created_at: string;
  modified_at: string;
  organization_name: string;
  divisions: string[];

  _valid_until_ts?: string;
}

// -----------------------------------------------------------------------------------------------------------
export interface CompanycardsState {
  loading: boolean;
  loaded?: boolean;
  error?: boolean;
  errorText?: string;
  items?: {[id: string]: Companycard};
}

const initialState: CompanycardsState = {
  loading: true,
};

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingCompanycards(state: CompanycardsState) {
  state.loading = true;
  state.loaded = false;

  if (state.error) {
    delete state.error;
    delete state.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingCompanycardsFailed(
  state: CompanycardsState,
  action: LoadingCompanycardsFailedAction
) {
  state.loading = false;
  state.error = true;
  state.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleCompanycardsLoaded(state: CompanycardsState, action: CompanycardsLoadedAction) {
  state.loading = false;
  state.loaded = true;

  const items = {};
  action.payload.forEach((a) => {
    items[a.id] = a;

    if (a.valid_until) {
      a._valid_until_ts = dayjs(a.valid_until).format(resolveUserDateFormat());
    }
  });

  state.items = items;

  if (state.error) {
    delete state.error;
    delete state.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export const companycardsReducer = createReducer(initialState, {
  [LOADING_COMPANYCARDS]: handleLoadingCompanycards,
  [LOADING_COMPANYCARDS_FAILED]: handleLoadingCompanycardsFailed,
  [COMPANYCARDS_LOADED]: handleCompanycardsLoaded,
});
