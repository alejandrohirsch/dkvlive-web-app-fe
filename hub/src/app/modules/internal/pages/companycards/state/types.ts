import {Companycard} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_COMPANYCARDS = "companycards/loading";

export interface LoadingCompanycardsAction {
  type: typeof LOADING_COMPANYCARDS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_COMPANYCARDS_FAILED = "companycards/loadingFailed";

export interface LoadingCompanycardsFailedAction {
  type: typeof LOADING_COMPANYCARDS_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const COMPANYCARDS_LOADED = "companycards/loaded";

export interface CompanycardsLoadedAction {
  type: typeof COMPANYCARDS_LOADED;
  payload: Companycard[];
}

// -----------------------------------------------------------------------------------------------------------
export type CompanycardsActions =
  | LoadingCompanycardsAction
  | LoadingCompanycardsFailedAction
  | CompanycardsLoadedAction;
