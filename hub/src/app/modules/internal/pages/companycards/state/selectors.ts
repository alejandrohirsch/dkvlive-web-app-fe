import {CompanycardsModuleState} from "./module";
import {createSelector} from "@reduxjs/toolkit";

export const companycardsItemsSelector = (state: CompanycardsModuleState) => state.companycards.items;

// this.state.
export const companycardsStateSelector = (state: CompanycardsModuleState) => state.companycards;

// list
export const companycardsListSelector = createSelector(companycardsItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// companycard
export const companycardSelector = (id: string) =>
  createSelector(companycardsItemsSelector, (items) => (items ? items[id] : undefined));
