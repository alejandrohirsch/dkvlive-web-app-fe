import {CompanycardsThunkResult} from "./module";
import {Companycard} from "./reducer";
import httpClient from "services/http";
import {
  LoadingCompanycardsAction,
  LOADING_COMPANYCARDS,
  CompanycardsLoadedAction,
  COMPANYCARDS_LOADED,
  LoadingCompanycardsFailedAction,
  LOADING_COMPANYCARDS_FAILED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingCompanycardsAction(): LoadingCompanycardsAction {
  return {
    type: LOADING_COMPANYCARDS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingCompanycardsFailedAction(errorText?: string): LoadingCompanycardsFailedAction {
  return {
    type: LOADING_COMPANYCARDS_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function companycardsLoadedAction(items: Companycard[]): CompanycardsLoadedAction {
  return {
    type: COMPANYCARDS_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadCompanycards(): CompanycardsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingCompanycardsAction());

    const [{data}, err] = await httpClient.get("/management/companycards");
    if (err !== null) {
      // @todo: log
      dispatch(loadingCompanycardsFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(companycardsLoadedAction(data));
  };
}

export function loadCompanycardsIfNeeded(): CompanycardsThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().companycards;
    const isLoaded = data.items !== undefined && data.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadCompanycards());
  };
}

export function deleteCompanycards(ids: string[]): CompanycardsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingCompanycardsAction());

    for (const id of ids) {
      const [, err] = await httpClient.delete(`/management/companycards/${id}`);
      if (err !== null) {
        // @todo: log
        dispatch(loadingCompanycardsFailedAction("Löschen fehlgeschlagen"));
        return;
      }
    }

    dispatch(loadCompanycards());
  };
}
