import React, {useState, useCallback, useEffect, MouseEventHandler} from "react";
import {useTranslation} from "react-i18next";
import Page from "app/components/Page";
import {Button, Loading, Message} from "dkv-live-frontend-ui";
import DataTable, {
  TablePageContent,
  TableStickyHeader,
  TableContainer,
  TableStickyFooter,
  useColumns,
  ToolbarButton,
  ToolbarButtonIcon,
  TableFooterActions,
  bodyField,
  TableSelectionCountInfo,
  TableFooterRightActions,
} from "app/components/DataTable";
import Window from "app/components/Window";
import {useSelector, useDispatch} from "react-redux";
import ConfirmWindow from "app/components/ConfirmWindow";
import {CompanycardsModule} from "./state/module";
import store from "app/state/store";
import {loadCompanycardsIfNeeded, loadCompanycards, deleteCompanycards} from "./state/actions";
import {companycardsListSelector, companycardsStateSelector} from "./state/selectors";
import httpClient from "services/http";
import CardForm, {CompanycardCreateData} from "./components/CardForm";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
store.addModule(CompanycardsModule);

// -----------------------------------------------------------------------------------------------------------
const listColumns = [
  {field: "organization_name", header: "Organization", width: 150, sortable: false},
  {field: "card_id", header: "Karten ID", width: 200, sortable: false},
  {field: "card_reader_ip", header: "Kartenleser IP", width: 150, sortable: false},
  {field: "card_reader_slot", header: "Kartenleser Slot", width: 150, sortable: false},
  {
    field: "divisions",
    header: "Divisions",
    width: 150,
    sortable: false,
    body: bodyField((d: any) => d.divisions.join(", ")),
  },
  {field: "_valid_until_ts", header: "Gültig bis", width: 150, sortable: false},
  {field: "comment", header: "Kommentar", width: 250, sortable: false},
  {field: "company", header: "Firma", width: 250, sortable: false},
];

// -----------------------------------------------------------------------------------------------------------
const Companycards: React.FC = () => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // companycards
  const companycardsListState = useSelector(companycardsStateSelector);
  const companycards = useSelector(companycardsListSelector);

  useEffect(() => {
    dispatch(loadCompanycardsIfNeeded());
  }, [dispatch]);

  const refresh = useCallback(() => dispatch(loadCompanycards()), [dispatch]);

  // organizations
  const [organizations, setOrganizations] = useState<any>(null);
  const [error, setError] = useState("");

  useEffect(() => {
    const load = async () => {
      const [{data}, err] = await httpClient.get("/management/organizations/findWithDivisions");
      if (err !== null) {
        setError("Laden fehlgeschlagen");
        return;
      }

      setOrganizations(data);
    };

    load();
  }, []);

  // edit
  const [editData, setEditData] = useState<CompanycardCreateData | undefined>();

  const showEditWndow = useCallback(
    (e: any) => {
      if (!hasPermission("superadmin:EditCompanyCards")) {
        return;
      }

      const org = e.data.organization_id
        ? organizations.find((o: any) => o.id === e.data.organization_id)
        : null;

      const divs: any[] = [];
      if (org && e.data.division_id) {
        e.data.division_id.forEach((dID: string) => {
          const div = org.divisions.find((d: any) => d.id === dID);
          if (div) {
            divs.push(div);
          }
        });
      }

      setEditData({
        id: e.data.id,
        card_id: e.data.card_id ?? "",
        organization_id: e.data.organization_id ?? "",
        division_id: e.data.division_id ? Array.from(e.data.division_id) : [],
        card_reader_ip: e.data.card_reader_ip ?? "",
        card_reader_slot: parseInt(e.data.card_reader_slot) ?? -1,
        valid_until: e.data.valid_until ?? "",
        comment: e.data.comment ?? "",
        company: e.data.company ?? "",
        _organization: org,
        _divisions: divs,
        _valid_until: e.data.valid_until ? new Date(e.data.valid_until) : null,
      });

      setShowNewWindow(true);
    },
    [setEditData, organizations]
  );

  // new window
  const [showNewWindow, setShowNewWindow] = useState(false);

  const onShowNewWindow = useCallback(() => {
    setEditData(undefined);
    setShowNewWindow(true);
  }, [setShowNewWindow]);

  const onCloseNewWindow = useCallback(() => setShowNewWindow(false), [setShowNewWindow]);

  // columns
  const [columns] = useState(listColumns);
  const visibleColumns = useColumns(columns);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  useEffect(() => {
    setSelected((selected) =>
      companycards && selected ? selected.map((s) => companycards.find((c) => c.id === s.id)) : []
    );
  }, [setSelected, companycards]);

  // delete
  const [deleteIDs, setDeleteIDs] = useState<string[]>([]);

  const showDeleteConfirm: MouseEventHandler<HTMLButtonElement> = useCallback(() => {
    setDeleteIDs(Array.from(selected.map((s) => s.id)));
  }, [setDeleteIDs, selected]);

  const onDeleteConfirm = useCallback(
    (confirmed: boolean) => {
      if (!confirmed) {
        setDeleteIDs([]);
        return;
      }

      dispatch(deleteCompanycards(deleteIDs));
      setDeleteIDs([]);
      setSelected([]);
    },
    [deleteIDs, setDeleteIDs, dispatch]
  );

  // render
  return (
    <Page id="translations">
      <Loading inprogress={companycardsListState.loading} />

      <TablePageContent>
        <TableStickyHeader>
          <h1>{t("Unternehmerkarten")}</h1>

          {hasPermission("superadmin:CreateCompanyCards") && (
            <ToolbarButton
              general
              style={{marginLeft: "auto"}}
              disabled={!organizations}
              onClick={onShowNewWindow}
            >
              <ToolbarButtonIcon>
                <span className="icon-ico_add" />
              </ToolbarButtonIcon>
              {t("Hinzufügen")}
            </ToolbarButton>
          )}

          <ToolbarButton general onClick={refresh}>
            <ToolbarButtonIcon>
              <span className="icon-ico_load" />
            </ToolbarButtonIcon>
            {t("Aktualisieren")}
          </ToolbarButton>
        </TableStickyHeader>

        <TableContainer>
          {error && <Message text={t(error)} error />}
          {!error && (
            <DataTable
              items={companycards ?? []}
              columns={visibleColumns}
              selected={selected}
              onSelectionChange={onSelectionChange}
              onRowDoubleClick={showEditWndow}
            />
          )}
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 ? (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Eintrag ausgewählt" : "Einträge ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>
              <TableFooterActions>
                {hasPermission("superadmin:DeleteCompanyCards") && (
                  <ToolbarButton onClick={showDeleteConfirm}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_delete" />
                    </ToolbarButtonIcon>
                    {t("Löschen")}
                  </ToolbarButton>
                )}
              </TableFooterActions>
              <TableFooterRightActions />
            </>
          ) : null}
        </TableStickyFooter>
      </TablePageContent>

      {showNewWindow && (
        <Window onClose={onCloseNewWindow} headline={t("Unternehmerkarte")}>
          {(props) => <CardForm editData={editData} organizations={organizations} {...props} />}
        </Window>
      )}

      {deleteIDs.length ? (
        <ConfirmWindow headline={t("Unternehmerkarten wirklich löschen?")} onConfirm={onDeleteConfirm} />
      ) : null}
    </Page>
  );
};

export default Companycards;
