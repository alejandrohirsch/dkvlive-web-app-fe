import React, {useState, MouseEventHandler} from "react";
import {useSelector, useDispatch} from "react-redux";
import {organizationsListStateSelector, organizationsListSelector} from "./state/organizations/selectors";
import {
  loadOrganizationsIfNeeded,
  loadOrganizations,
  deleteOrganization,
  unsuspendOrganization,
} from "./state/organizations/actions";
import {useEffect, useCallback} from "react";
import {Organization} from "./state/organizations/reducer";
import {Loading} from "dkv-live-frontend-ui";
import Page from "app/components/Page";
import {
  TablePageContent,
  TableStickyHeader,
  ToolbarButtonIcon,
  ToolbarButton,
} from "app/components/DataTable";
import {useTranslation} from "react-i18next";
import {TableContainer} from "@material-ui/core";
import CardGrid from "app/components/CardGrid";
import Window from "app/components/Window";
import ConfirmWindow from "app/components/ConfirmWindow";
import OrganizationsForm from "./components/OrganizationsForm";
import SuspensionForm from "./components/SuspensionForm";
import OrganizationsCreate from "./components/OrganizationsCreate";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
interface OrganizationsListProps {
  path: string;
}

const OrganizationsList: React.FC<OrganizationsListProps> = ({path}) => {
  const dispatch = useDispatch();
  const {t} = useTranslation();

  // organizations
  const organizationsListState = useSelector(organizationsListStateSelector);
  const organizations = useSelector(organizationsListSelector);

  useEffect(() => {
    dispatch(loadOrganizationsIfNeeded());
  }, [dispatch]);

  const refresh = useCallback(() => dispatch(loadOrganizations()), [dispatch]);

  // - edit
  const [activeOrganizationWindow, setActiveOrganizationWindow] = useState<Organization | undefined>();

  const showEditWindow: MouseEventHandler = useCallback(
    (e) => {
      if (!organizations) {
        return;
      }

      const id = (e.currentTarget as HTMLButtonElement).dataset.id as string;
      const organization = organizations.find((o) => o.id === id);
      if (organization) {
        setActiveOrganizationWindow(organization);
      }
    },
    [organizations]
  );

  // - create
  const [showCreateWindow, setShowCreateWindow] = useState(false);

  // - delete
  const [deleteID, setDeleteID] = useState("");
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);

  const showDeleteConfirm: MouseEventHandler = useCallback(
    (e) => {
      setDeleteID((e.currentTarget as HTMLButtonElement).dataset.id as string);
      setShowDeleteConfirmation(true);
    },
    [setDeleteID]
  );

  const onDeleteConfirm = async (confirmed: boolean) => {
    if (!confirmed) {
      setShowDeleteConfirmation(false);
      return;
    }

    const callback = (success: boolean) => {
      if (success) {
        setShowDeleteConfirmation(false);
        dispatch(loadOrganizations());
        setDeleteID("");
      }
    };

    dispatch(deleteOrganization(deleteID, callback));
  };

  // - suspend
  const [suspendOrganizationWindow, setSuspendOrganizationWindow] = useState<Organization | undefined>();

  const showSuspendWindow: MouseEventHandler = useCallback(
    (e) => {
      if (!organizations) {
        return;
      }

      const id = (e.currentTarget as HTMLButtonElement).dataset.id as string;
      const organization = organizations.find((o) => o.id === id);
      if (organization) {
        setSuspendOrganizationWindow(organization);
      }
    },
    [organizations]
  );

  // - unsuspend
  const [unsuspendID, setUnsuspendID] = useState("");
  const [showUnsuspendConfirmation, setShowUnsuspendConfirmation] = useState(false);

  const showUnsuspendConfirm: MouseEventHandler = useCallback(
    (e) => {
      setUnsuspendID((e.currentTarget as HTMLButtonElement).dataset.id as string);
      setShowUnsuspendConfirmation(true);
    },
    [setUnsuspendID]
  );

  const onUnsuspendConfirm = async (confirmed: boolean) => {
    if (!confirmed) {
      setShowUnsuspendConfirmation(false);
      return;
    }

    const callback = (success: boolean) => {
      if (success) {
        setShowUnsuspendConfirmation(false);
        dispatch(loadOrganizations());
        setUnsuspendID("");
      }
    };

    dispatch(unsuspendOrganization(unsuspendID, callback));
  };

  const getOrganizationPath = (item: Organization) => path + "/" + item.id;

  // render
  return (
    <Page id="organizations">
      <Loading inprogress={organizationsListState.loading} />

      <TablePageContent>
        <TableStickyHeader>
          <h1>{t("Organisationen")}</h1>

          {hasPermission("superadmin:CreateOrganizations") && (
            <ToolbarButton
              general
              style={{
                marginLeft: "auto",
              }}
              onClick={() => setShowCreateWindow(true)}
            >
              <ToolbarButtonIcon>
                <span className="icon-ico_add" />
              </ToolbarButtonIcon>
              {t("Hinzufügen")}
            </ToolbarButton>
          )}

          <ToolbarButton general onClick={refresh}>
            <ToolbarButtonIcon>
              <span className="icon-ico_load" />
            </ToolbarButtonIcon>
            {t("Aktualisieren")}
          </ToolbarButton>
        </TableStickyHeader>

        <TableContainer>
          {organizations && (
            <CardGrid
              items={organizations}
              linkTo={getOrganizationPath}
              headerKey="name"
              onEditItem={hasPermission("superadmin:EditOrganizations") ? showEditWindow : undefined}
              onDeleteItem={hasPermission("superadmin:DeleteOrganizations") ? showDeleteConfirm : undefined}
              onSuspendItem={hasPermission("superadmin:SuspendOrganizations") ? showSuspendWindow : undefined}
              onUnsuspendItem={
                hasPermission("superadmin:SuspendOrganizations") ? showUnsuspendConfirm : undefined
              }
              isSuspended={(org: Organization) =>
                org.suspension && !org.suspension.state === false ? true : false
              }
              props={[
                {
                  label: "Kundennummer",
                  key: "customer_number",
                },
                {
                  label: "E-Mail",
                  key: "notification_email",
                },
                {
                  label: "Shipper",
                  key: "shipper",
                },
                {
                  label: "Carrier",
                  key: "carrier",
                },
                {
                  label: "Suspension",
                  getValue: (org: Organization) =>
                    org.suspension && !org.suspension.state === false
                      ? t("ja: " + (org.suspension.cause ? org.suspension.cause : "-"))
                      : t("nein"),
                },
                {
                  label: "Bestellung",
                  getValue: (org: Organization) => (org.createdByOrder ? t("ja") : t("nein")),
                },
              ]}
            />
          )}
        </TableContainer>

        {activeOrganizationWindow && (
          <Window onClose={() => setActiveOrganizationWindow(undefined)} headline={t("Organisation ändern")}>
            {(props) => <OrganizationsForm organization={activeOrganizationWindow} {...props} />}
          </Window>
        )}
        {showCreateWindow && (
          <Window onClose={() => setShowCreateWindow(false)} headline={t("Neue Organisation")}>
            {(props) => <OrganizationsCreate {...props} />}
          </Window>
        )}

        {showDeleteConfirmation && (
          <ConfirmWindow headline={t("Organisation wirklich löschen?")} onConfirm={onDeleteConfirm} />
        )}

        {suspendOrganizationWindow && (
          <Window
            onClose={() => setSuspendOrganizationWindow(undefined)}
            headline={t("Organisation sperren")}
          >
            {(props) => <SuspensionForm organization={suspendOrganizationWindow} {...props} />}
          </Window>
        )}

        {showUnsuspendConfirmation && (
          <ConfirmWindow headline={t("Organisation wirklich entsperren?")} onConfirm={onUnsuspendConfirm} />
        )}
      </TablePageContent>
    </Page>
  );
};

export default OrganizationsList;
