import {OrganizationsState} from "../reducer";
import {
  LoadingOrganizationsFailedAction,
  LoadingOrganizationFailedAction,
  OrganizationsLoadedAction,
  OrganizationLoadedAction,
  ProcessingOrganizationActionFailedAction,
} from "./types";
import {Division} from "../divisions/reducer";

// -----------------------------------------------------------------------------------------------------------
export interface Organization {
  id: string;
  name: string;
  divisions: Division[];
  shipper?: string;
  carrier?: string;
  suspension?: Suspension;
  createdByOrder?: boolean;
  customer_number?: string;
  notification_email?: string;
  created_at?: string;
  modified_at?: string;
}

export interface CreateOrganization {
  organization_name: string;
  division_name: string;
  language_code: string;
  customer_number?: string;
  notification_email?: string;
  created_by_order?: boolean;
}

export const emptyOrganization: CreateOrganization = {
  organization_name: "",
  division_name: "",
  language_code: "",
  customer_number: "",
  notification_email: "",
};

export interface Suspension {
  state: boolean;
  since?: string;
  cause?: string;
  by?: string;
}

export interface OrganizationsData {
  list: {
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  item: {
    id?: string;
    loading: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: string]: Organization};
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingOrganizations(state: OrganizationsState) {
  const data = state.organizations.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingOrganizationsFailed(
  state: OrganizationsState,
  action: LoadingOrganizationsFailedAction
) {
  const data = state.organizations;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleOrganizationsLoaded(state: OrganizationsState, action: OrganizationsLoadedAction) {
  const data = state.organizations;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.forEach((n) => {
    if (n.id) {
      items[n.id] = n;
    }
  });
  data.items = items;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingOrganization(state: OrganizationsState) {
  const data = state.organizations.item;

  data.loading = true;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingOrganizationFailed(
  state: OrganizationsState,
  action: LoadingOrganizationFailedAction
) {
  const data = state.organizations.item;

  data.id = "";
  data.loading = false;
  data.error = true;
  data.errorText = action.payload;
}

// -----------------------------------------------------------------------------------------------------------
export function handleOrganizationLoaded(state: OrganizationsState, action: OrganizationLoadedAction) {
  const data = state.organizations;

  data.item.loading = false;
  if (action.payload.id) {
    data.item.id = action.payload.id;
    data.items = {...data.items, [action.payload.id]: action.payload};
  }

  if (data.item.error) {
    delete data.item.error;
    delete data.item.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleProcessingOrganizationAction(state: OrganizationsState) {
  const data = state.organizations.item;

  data.loading = true;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleProcessingOrganizationActionFailed(
  state: OrganizationsState,
  action: ProcessingOrganizationActionFailedAction
) {
  const data = state.organizations.item;

  data.loading = false;
  data.error = true;
  data.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleOrganizationActionProcessed(state: OrganizationsState) {
  const data = state.organizations.item;

  data.loading = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}
