import {createReducer} from "@reduxjs/toolkit";
import {
  OrganizationsData,
  handleLoadingOrganization,
  handleLoadingOrganizations,
  handleLoadingOrganizationsFailed,
  handleOrganizationLoaded,
  handleLoadingOrganizationFailed,
  handleOrganizationsLoaded,
  handleProcessingOrganizationAction,
  handleOrganizationActionProcessed,
  handleProcessingOrganizationActionFailed,
} from "./organizations/reducer";
import {
  LOADING_ORGANIZATION,
  LOADING_ORGANIZATIONS_FAILED,
  LOADING_ORGANIZATIONS,
  ORGANIZATION_LOADED,
  ORGANIZATIONS_LOADED,
  LOADING_ORGANIZATION_FAILED,
  PROCESSING_ORGANIZATION_ACTION,
  PROCESSING_ORGANIZATION_ACTION_FAILED,
  ORGANIZATION_ACTION_PROCESSED,
} from "./organizations/types";
import {
  DivisionsData,
  handleLoadingDivisions,
  handleLoadingDivisionsFailed,
  handleDivisionsLoaded,
  handleProcessingDivisionAction,
  handleProcessingDivisionActionFailed,
  handleDivisionActionProcessed,
  Resources,
  handleCheckingDivisionResourcesAction,
  handleCheckingDivisionResourcesActionFailed,
  handleDivisionResourcesCheckedAction,
} from "./divisions/reducer";
import {
  LOADING_DIVISIONS,
  LOADING_DIVISIONS_FAILED,
  DIVISIONS_LOADED,
  DIVISION_ACTION_PROCESSED,
  PROCESSING_DIVISION_ACTION,
  PROCESSING_DIVISION_ACTION_FAILED,
  CHECKING_DIVISION_RESOURCES,
  CHECKING_DIVISION_RESOURCES_FAILED,
  DIVISION_RESOURCES_CHECKED,
} from "./divisions/types";
import {
  handleLoadingPermissions,
  handleLoadingPermissionsFailed,
  handlePermissionsLoaded,
  PermissionData,
} from "./permissions/reducer";
import {
  UserData,
  handleUsersLoaded,
  handleLoadingUsers,
  handleLoadingUsersFailed,
  handleProcessingUserAction,
  handleProcessingUserActionFailed,
  handleUserActionProcessed,
} from "./users/reducer";
import {
  LOADING_USERS,
  LOADING_USERS_FAILED,
  USERS_LOADED,
  PROCESSING_USER_ACTION,
  PROCESSING_USER_ACTION_FAILED,
  USER_ACTION_PROCESSED,
} from "./users/types";
import {
  LOADING_POLICIES,
  LOADING_POLICIES_FAILED,
  POLICIES_LOADED,
  PROCESSING_POLICY_ACTION_FAILED,
  POLICY_ACTION_PROCESSED,
  PROCESSING_POLICY_ACTION,
} from "./policies/types";
import {
  handleLoadingPolicies,
  handleLoadingPoliciesFailed,
  handlePoliciesLoaded,
  PolicyData,
  handleProcessingPolicyAction,
  handleProcessingPolicyActionFailed,
  handlePolicyActionProcessed,
} from "./policies/reducer";
import {PERMISSIONS_LOADED, LOADING_PERMISSIONS, LOADING_PERMISSIONS_FAILED} from "./permissions/types";

// -----------------------------------------------------------------------------------------------------------
export interface OrganizationsState {
  organizations: OrganizationsData;
  divisions: DivisionsData;
  users: UserData;
  resources: Resources;
  policies: PolicyData;
  permissions: PermissionData;
}

const initialState: OrganizationsState = {
  organizations: {
    list: {loading: true},
    item: {loading: true},
  },
  divisions: {
    list: {loading: true},
    item: {loading: false},
  },
  users: {
    list: {loading: true},
    item: {loading: false},
  },
  policies: {
    list: {loading: true},
    item: {loading: false},
  },
  permissions: {
    list: {loading: true},
  },
  resources: {loading: false},
};

export const reducer = createReducer(initialState, {
  // organizations
  [LOADING_ORGANIZATIONS]: handleLoadingOrganizations,
  [LOADING_ORGANIZATIONS_FAILED]: handleLoadingOrganizationsFailed,
  [ORGANIZATIONS_LOADED]: handleOrganizationsLoaded,
  [LOADING_ORGANIZATION]: handleLoadingOrganization,
  [LOADING_ORGANIZATION_FAILED]: handleLoadingOrganizationFailed,
  [ORGANIZATION_LOADED]: handleOrganizationLoaded,
  [PROCESSING_ORGANIZATION_ACTION]: handleProcessingOrganizationAction,
  [PROCESSING_ORGANIZATION_ACTION_FAILED]: handleProcessingOrganizationActionFailed,
  [ORGANIZATION_ACTION_PROCESSED]: handleOrganizationActionProcessed,
  // divisions
  [LOADING_DIVISIONS]: handleLoadingDivisions,
  [LOADING_DIVISIONS_FAILED]: handleLoadingDivisionsFailed,
  [DIVISIONS_LOADED]: handleDivisionsLoaded,
  [PROCESSING_DIVISION_ACTION]: handleProcessingDivisionAction,
  [PROCESSING_DIVISION_ACTION_FAILED]: handleProcessingDivisionActionFailed,
  [DIVISION_ACTION_PROCESSED]: handleDivisionActionProcessed,
  [CHECKING_DIVISION_RESOURCES]: handleCheckingDivisionResourcesAction,
  [CHECKING_DIVISION_RESOURCES_FAILED]: handleCheckingDivisionResourcesActionFailed,
  [DIVISION_RESOURCES_CHECKED]: handleDivisionResourcesCheckedAction,
  // permissions
  [LOADING_PERMISSIONS]: handleLoadingPermissions,
  [LOADING_PERMISSIONS_FAILED]: handleLoadingPermissionsFailed,
  [PERMISSIONS_LOADED]: handlePermissionsLoaded,
  // policies
  [LOADING_POLICIES]: handleLoadingPolicies,
  [LOADING_POLICIES_FAILED]: handleLoadingPoliciesFailed,
  [POLICIES_LOADED]: handlePoliciesLoaded,
  [PROCESSING_POLICY_ACTION]: handleProcessingPolicyAction,
  [PROCESSING_POLICY_ACTION_FAILED]: handleProcessingPolicyActionFailed,
  [POLICY_ACTION_PROCESSED]: handlePolicyActionProcessed,
  // users
  [LOADING_USERS]: handleLoadingUsers,
  [LOADING_USERS_FAILED]: handleLoadingUsersFailed,
  [USERS_LOADED]: handleUsersLoaded,
  [PROCESSING_USER_ACTION]: handleProcessingUserAction,
  [PROCESSING_USER_ACTION_FAILED]: handleProcessingUserActionFailed,
  [USER_ACTION_PROCESSED]: handleUserActionProcessed,
});
