import {Policy} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_POLICIES = "organizations/loadingPolicies";

export interface LoadingPoliciesAction {
  type: typeof LOADING_POLICIES;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_POLICIES_FAILED = "organizations/loadingPoliciesFailed";

export interface LoadingPoliciesFailedAction {
  type: typeof LOADING_POLICIES_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const POLICIES_LOADED = "organizations/policiesLoaded";

export interface PoliciesLoadedAction {
  type: typeof POLICIES_LOADED;
  payload: Policy[];
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_POLICY_ACTION = "organizations/processingPolicyAction";

export interface ProcessingPolicyActionAction {
  type: typeof PROCESSING_POLICY_ACTION;
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_POLICY_ACTION_FAILED = "organizations/processingPolicyActionFailed";

export interface ProcessingPolicyActionFailedAction {
  type: typeof PROCESSING_POLICY_ACTION_FAILED;
  payload: {errorText?: string};
}

// -----------------------------------------------------------------------------------------------------------
export const POLICY_ACTION_PROCESSED = "organizations/policyActionProcessed";

export interface PolicyActionProcessedAction {
  type: typeof POLICY_ACTION_PROCESSED;
  payload: Policy;
}

// -----------------------------------------------------------------------------------------------------------
export type PolicyActions =
  | LoadingPoliciesAction
  | LoadingPoliciesFailedAction
  | PoliciesLoadedAction
  | ProcessingPolicyActionAction
  | ProcessingPolicyActionFailedAction
  | PolicyActionProcessedAction;
