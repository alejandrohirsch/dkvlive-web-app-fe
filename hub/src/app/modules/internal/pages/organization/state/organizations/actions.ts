import httpClient from "services/http";
import {
  OrganizationsLoadedAction,
  LoadingOrganizationsAction,
  LoadingOrganizationsFailedAction,
  LOADING_ORGANIZATIONS,
  LOADING_ORGANIZATIONS_FAILED,
  ORGANIZATIONS_LOADED,
  LoadingOrganizationAction,
  LOADING_ORGANIZATION,
  LOADING_ORGANIZATION_FAILED,
  LoadingOrganizationFailedAction,
  OrganizationLoadedAction,
  ORGANIZATION_LOADED,
  ProcessingOrganizationActionAction,
  ProcessingOrganizationActionFailedAction,
  ORGANIZATION_ACTION_PROCESSED,
  OrganizationActionProcessedAction,
  PROCESSING_ORGANIZATION_ACTION_FAILED,
  PROCESSING_ORGANIZATION_ACTION,
} from "./types";
import {Organization, CreateOrganization, Suspension} from "./reducer";
import {OrganizationsThunkResult} from "../modules";

// -----------------------------------------------------------------------------------------------------------
function loadingOrganizationsAction(): LoadingOrganizationsAction {
  return {
    type: LOADING_ORGANIZATIONS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingOrganizationsFailedAction(
  errorText?: string,
  unsetItems = true
): LoadingOrganizationsFailedAction {
  return {
    type: LOADING_ORGANIZATIONS_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function organizationsLoadedAction(items: Organization[]): OrganizationsLoadedAction {
  return {
    type: ORGANIZATIONS_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingOrganizationAction(id: string): LoadingOrganizationAction {
  return {
    type: LOADING_ORGANIZATION,
    payload: id,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingOrganizationFailedAction(errorText?: string): LoadingOrganizationFailedAction {
  return {
    type: LOADING_ORGANIZATION_FAILED,
    payload: errorText ?? "",
  };
}

// -----------------------------------------------------------------------------------------------------------
function organizationLoadedAction(item: Organization): OrganizationLoadedAction {
  return {
    type: ORGANIZATION_LOADED,
    payload: item,
  };
}

// -----------------------------------------------------------------------------------------------------------
function processingOrganizationActionAction(): ProcessingOrganizationActionAction {
  return {
    type: PROCESSING_ORGANIZATION_ACTION,
  };
}

// -----------------------------------------------------------------------------------------------------------
function processingOrganizationActionFailedAction(
  errorText?: string
): ProcessingOrganizationActionFailedAction {
  return {
    type: PROCESSING_ORGANIZATION_ACTION_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function organizationActionProcessedAction(
  organization?: Record<string, unknown>
): OrganizationActionProcessedAction {
  return {
    type: ORGANIZATION_ACTION_PROCESSED,
    payload: {organization},
  };
}
// -----------------------------------------------------------------------------------------------------------
export function loadOrganizations(
  callback?: (success: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingOrganizationsAction());

    const [{data}, err] = await httpClient.get("/management/organizations/findWithDivisions", {
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (err !== null) {
      // @todo: log
      dispatch(
        loadingOrganizationsFailedAction("Laden fehlgeschlagen" + (": " + err.response?.data.message || ""))
      );
      callback && callback(false);
      return;
    }

    dispatch(organizationsLoadedAction(data));
    callback && callback(true);
  };
}

export function loadOrganizationsIfNeeded(): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().organizations.organizations;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadOrganizations());
  };
}

export function loadOrganization(id: string): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingOrganizationAction(id));

    const [{data}, err] = await httpClient.get("/management/organizations", {
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (err !== null) {
      // @todo: log
      dispatch(
        loadingOrganizationFailedAction("Laden fehlgeschlagen" + (": " + err.response?.data.message || ""))
      );
      return;
    }
    const org = data?.find((o: any) => o.id === id);

    dispatch(organizationLoadedAction(org));
  };
}

export function updateOrganization(
  id: string,
  organization: Organization,
  callback?: (success: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingOrganizationActionAction());
    const [{response}, err] = await httpClient.put(
      "/management/organizations/" + id,
      JSON.stringify(organization),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    if (err !== null) {
      // @todo: log
      dispatch(
        processingOrganizationActionFailedAction(
          "Speichern fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);
      return;
    }

    dispatch(organizationActionProcessedAction(response));
    callback && callback(true);
  };
}

export function createOrganization(
  organization: CreateOrganization,
  callback?: (success: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingOrganizationActionAction());
    const [{response}, err] = await httpClient.post(
      "/management/organizations",
      JSON.stringify(organization),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    if (err !== null) {
      // @todo: log
      dispatch(
        processingOrganizationActionFailedAction(
          "Erstellen fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);
      return;
    }

    dispatch(organizationActionProcessedAction(response));
    callback && callback(true);
  };
}

export function deleteOrganization(
  id: string,
  callback?: (success: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingOrganizationActionAction());
    const [{response}, err] = await httpClient.delete("/management/organizations/" + id, {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        processingOrganizationActionFailedAction(
          "Löschen fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);
      return;
    }

    dispatch(organizationActionProcessedAction(response));
    callback && callback(true);
  };
}

export function suspendOrganization(
  id: string,
  suspension: Suspension,
  callback?: (success: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingOrganizationActionAction());
    const [{response}, err] = await httpClient.post(
      "/management/organizations/" + id + "/suspend",
      JSON.stringify(suspension),
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (err !== null) {
      // @todo: log
      dispatch(processingOrganizationActionFailedAction("Sperren fehlgeschlagen"));
      callback && callback(false);
      return;
    }

    dispatch(organizationActionProcessedAction(response));
    callback && callback(true);
  };
}

export function unsuspendOrganization(
  id: string,
  callback?: (success: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingOrganizationActionAction());
    const [{response}, err] = await httpClient.post("/management/organizations/" + id + "/unsuspend", {
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (err !== null) {
      // @todo: log
      dispatch(processingOrganizationActionFailedAction("Entsperren fehlgeschlagen"));
      callback && callback(false);
      return;
    }
    dispatch(organizationActionProcessedAction(response));
    callback && callback(true);
  };
}
