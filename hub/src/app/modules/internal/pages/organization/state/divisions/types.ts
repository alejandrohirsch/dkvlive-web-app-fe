import {Division, ResourcesData} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_DIVISIONS = "organizations/loadingDivisions";

export interface LoadingDivisionsAction {
  type: typeof LOADING_DIVISIONS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_DIVISIONS_FAILED = "organizations/loadingDivisionsFailed";

export interface LoadingDivisionsFailedAction {
  type: typeof LOADING_DIVISIONS_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const DIVISIONS_LOADED = "organizations/divisionsLoaded";

export interface DivisionsLoadedAction {
  type: typeof DIVISIONS_LOADED;
  payload: Division[];
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_DIVISION_ACTION = "organizations/processingDivisionAction";

export interface ProcessingDivisionActionAction {
  type: typeof PROCESSING_DIVISION_ACTION;
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_DIVISION_ACTION_FAILED = "organizations/processingDivisionActionFailed";

export interface ProcessingDivisionActionFailedAction {
  type: typeof PROCESSING_DIVISION_ACTION_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const DIVISION_ACTION_PROCESSED = "organizations/divisionActionProcessed";

export interface DivisionActionProcessedAction {
  type: typeof DIVISION_ACTION_PROCESSED;
  payload: {division?: Record<string, unknown>};
}

export const CHECKING_DIVISION_RESOURCES = "organizations/checkingDivisionResources";

export interface CheckingDivisionResourcesAction {
  type: typeof CHECKING_DIVISION_RESOURCES;
}

// -----------------------------------------------------------------------------------------------------------
export const CHECKING_DIVISION_RESOURCES_FAILED = "organizations/checkingDivisionResourcesFailed";

export interface CheckingDivisionResourcesFailedAction {
  type: typeof CHECKING_DIVISION_RESOURCES_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const DIVISION_RESOURCES_CHECKED = "organizations/divisionResourcesChecked";

export interface DivisionResourcesCheckedAction {
  type: typeof DIVISION_RESOURCES_CHECKED;
  payload: ResourcesData;
}
// -----------------------------------------------------------------------------------------------------------
export type DivisionsActions =
  | LoadingDivisionsAction
  | LoadingDivisionsFailedAction
  | DivisionsLoadedAction
  | ProcessingDivisionActionAction
  | ProcessingDivisionActionFailedAction
  | DivisionActionProcessedAction
  | CheckingDivisionResourcesAction
  | CheckingDivisionResourcesFailedAction
  | DivisionResourcesCheckedAction;
