import {OrganizationsModuleState} from "../modules";
import {createSelector} from "@reduxjs/toolkit";

export const usersItemsSelector = (state: OrganizationsModuleState) => state.organizations.users.items;

// list
export const usersListStateSelector = (state: OrganizationsModuleState) => state.organizations.users.list;

export const usersListSelector = createSelector(usersItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// item
export const usersItemStateSelector = (state: OrganizationsModuleState) => state.organizations.users.item;
