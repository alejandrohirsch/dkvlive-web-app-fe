import {OrganizationsThunkResult} from "../modules";
import {User} from "./reducer";
import httpClient from "services/http";
import {
  LoadingUsersAction,
  LOADING_USERS,
  UsersLoadedAction,
  USERS_LOADED,
  LoadingUsersFailedAction,
  LOADING_USERS_FAILED,
  ProcessingUserActionAction,
  PROCESSING_USER_ACTION,
  ProcessingUserActionFailedAction,
  PROCESSING_USER_ACTION_FAILED,
  UserActionProcessedAction,
  USER_ACTION_PROCESSED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingUsersAction(): LoadingUsersAction {
  return {
    type: LOADING_USERS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingUsersFailedAction(errorText?: string, unsetItems = true): LoadingUsersFailedAction {
  return {
    type: LOADING_USERS_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function usersLoadedAction(items: User[]): UsersLoadedAction {
  return {
    type: USERS_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
function processingUserActionAction(): ProcessingUserActionAction {
  return {
    type: PROCESSING_USER_ACTION,
  };
}

// -----------------------------------------------------------------------------------------------------------
function processingUserActionFailedAction(errorText?: string): ProcessingUserActionFailedAction {
  return {
    type: PROCESSING_USER_ACTION_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function userActionProcessedAction(user: User): UserActionProcessedAction {
  return {
    type: USER_ACTION_PROCESSED,
    payload: user,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadUsers(): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingUsersAction());

    const [{data}, err] = await httpClient.get("/management/users/findAll", {
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (err !== null) {
      // @todo: log
      dispatch(loadingUsersFailedAction("Laden fehlgeschlagen" + (": " + err.response?.data.message || "")));
      return;
    }
    dispatch(usersLoadedAction(data));
  };
}

export function loadUsersIfNeeded(): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().organizations.users;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadUsers());
  };
}

export function updateUser(
  id: string,
  user: User,
  callback?: (success: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingUserActionAction());
    const [{response}, err] = await httpClient.put("/management/users/" + id, JSON.stringify(user), {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        processingUserActionFailedAction(
          "Speichern fehlgeschlagen" + (": " + (err.response ? err.response.data.message : "") || "")
        )
      );
      callback && callback(false);
      return;
    }

    dispatch(userActionProcessedAction(response));
    callback && callback(true);
  };
}

export function createUser(
  user: User,
  callback?: (success: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingUserActionAction());

    const [{response}, err] = await httpClient.post("/management/users", JSON.stringify(user), {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        processingUserActionFailedAction(
          "Erstellen fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);
      return;
    }

    dispatch(userActionProcessedAction(response));
    callback && callback(true);

    // const policies = [];
    // response.policies.forEach((pol) => {
    //   const permissions = {
    //     allow: [],
    //     deny: [],
    //   };

    // })

    //  data.permission_config.forEach((p: {action: string; key: string}) => permissions[p.action].push(p.key));
  };
}

export function deleteUser(
  id: string,
  callback?: (success: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingUserActionAction());
    const [{response}, err] = await httpClient.delete("/management/users/" + id, {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        processingUserActionFailedAction("Löschen fehlgeschlagen" + (": " + err.response?.data.message || ""))
      );
      callback && callback(false);
      return;
    }

    dispatch(userActionProcessedAction(response));
    callback && callback(true);
  };
}
