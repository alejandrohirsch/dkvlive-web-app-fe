import {OrganizationsModuleState} from "../modules";
import {createSelector} from "@reduxjs/toolkit";

export const permissionsItemsSelector = (state: OrganizationsModuleState) =>
  state.organizations.permissions.items;

// list
export const permissionsListStateSelector = (state: OrganizationsModuleState) =>
  state.organizations.permissions.list;

export const permissionsListSelector = createSelector(permissionsItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// item
export const permissionSelector = (id: string) =>
  createSelector(permissionsItemsSelector, (items) => (items ? items[id] : undefined));
