import {Organization} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ORGANIZATIONS = "organizations/loadingOrganizations";

export interface LoadingOrganizationsAction {
  type: typeof LOADING_ORGANIZATIONS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ORGANIZATIONS_FAILED = "organizations/loadingOrganizationsFailed";

export interface LoadingOrganizationsFailedAction {
  type: typeof LOADING_ORGANIZATIONS_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const ORGANIZATIONS_LOADED = "organizations/organizationsLoaded";

export interface OrganizationsLoadedAction {
  type: typeof ORGANIZATIONS_LOADED;
  payload: Organization[];
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ORGANIZATION = "organizations/loadingOrganization";

export interface LoadingOrganizationAction {
  type: typeof LOADING_ORGANIZATION;
  payload: string;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ORGANIZATION_FAILED = "organizations/loadingOrganizationFailed";

export interface LoadingOrganizationFailedAction {
  type: typeof LOADING_ORGANIZATION_FAILED;
  payload: string;
}

// -----------------------------------------------------------------------------------------------------------
export const ORGANIZATION_LOADED = "organizations/organizationLoaded";

export interface OrganizationLoadedAction {
  type: typeof ORGANIZATION_LOADED;
  payload: Organization;
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_ORGANIZATION_ACTION = "organizations/processingOrganizationAction";

export interface ProcessingOrganizationActionAction {
  type: typeof PROCESSING_ORGANIZATION_ACTION;
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_ORGANIZATION_ACTION_FAILED = "organizations/processingOrganizationActionFailed";

export interface ProcessingOrganizationActionFailedAction {
  type: typeof PROCESSING_ORGANIZATION_ACTION_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const ORGANIZATION_ACTION_PROCESSED = "organizations/OrganizationActionProcessed";

export interface OrganizationActionProcessedAction {
  type: typeof ORGANIZATION_ACTION_PROCESSED;
  payload: {organization?: Record<string, unknown>};
}
// -----------------------------------------------------------------------------------------------------------

export type OrganizationsActions =
  | LoadingOrganizationsAction
  | LoadingOrganizationsFailedAction
  | OrganizationsLoadedAction
  | LoadingOrganizationAction
  | LoadingOrganizationFailedAction
  | OrganizationLoadedAction
  | ProcessingOrganizationActionAction
  | ProcessingOrganizationActionFailedAction
  | OrganizationActionProcessedAction;
