import {OrganizationsState} from "../reducer";
import {
  DivisionsLoadedAction,
  LoadingDivisionsFailedAction,
  ProcessingDivisionActionFailedAction,
  CheckingDivisionResourcesFailedAction,
  DivisionResourcesCheckedAction,
} from "./types";
import {Suspension} from "../organizations/reducer";

// -----------------------------------------------------------------------------------------------------------
export interface Division {
  id?: string;
  name: string;
  organization_id: string;
  suspension?: Suspension;
  language_code: string;
  created_at?: string;
  modified_at?: string;
}

export const emptyDivision: Division = {
  name: "",
  organization_id: "",
  language_code: "de",
};

export interface DivisionsData {
  list: {
    organizationID?: string;
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  item: {
    loading: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: string]: Division};
}

export interface Resources {
  loading: boolean;
  loaded?: boolean;
  error?: boolean;
  errorText?: string;
  items?: ResourcesData;
}

export interface ResourcesData {
  assets?: ObjectData;
  devices?: ObjectData;
  drivers?: PersonData;
  tags?: ObjectData;
  users?: PersonData;
  geofences?: ObjectData;
}

export interface ObjectData {
  total_count: number;
  list: ObjectResource[];
}

export interface PersonData {
  total_count: number;
  list: PersonResource[];
}

export interface ObjectResource {
  id: string;
  name?: string;
}

export interface PersonResource {
  id: string;
  first_name?: string;
  last_name?: string;
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingDivisions(state: OrganizationsState) {
  const data = state.divisions.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingDivisionsFailed(
  state: OrganizationsState,
  action: LoadingDivisionsFailedAction
) {
  const data = state.divisions;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleDivisionsLoaded(state: OrganizationsState, action: DivisionsLoadedAction) {
  const data = state.divisions;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.forEach((n) => n.id && (items[n.id] = n));
  data.items = items;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleProcessingDivisionAction(state: OrganizationsState) {
  const data = state.divisions.item;

  data.loading = true;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleProcessingDivisionActionFailed(
  state: OrganizationsState,
  action: ProcessingDivisionActionFailedAction
) {
  const data = state.divisions.item;

  data.loading = false;
  data.error = true;
  data.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleDivisionActionProcessed(state: OrganizationsState) {
  const data = state.divisions.item;

  data.loading = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleCheckingDivisionResourcesAction(state: OrganizationsState) {
  const data = state.resources;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleCheckingDivisionResourcesActionFailed(
  state: OrganizationsState,
  action: CheckingDivisionResourcesFailedAction
) {
  const data = state.resources;

  data.loading = false;
  data.error = true;
  data.errorText = action.payload.errorText;
  data.items = undefined;
}

// -----------------------------------------------------------------------------------------------------------
export function handleDivisionResourcesCheckedAction(
  state: OrganizationsState,
  action: DivisionResourcesCheckedAction
) {
  const data = state.resources;

  data.loading = false;
  data.loaded = true;

  data.items = {};
  if (action.payload.assets) {
    data.items.assets = action.payload.assets;
  }
  if (action.payload.devices) {
    data.items.devices = action.payload.devices;
  }
  if (action.payload.drivers) {
    data.items.drivers = action.payload.drivers;
  }
  if (action.payload.geofences) {
    data.items.geofences = action.payload.geofences;
  }
  if (action.payload.tags) {
    data.items.tags = action.payload.tags;
  }
  if (action.payload.users) {
    data.items.users = action.payload.users;
  }

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}
