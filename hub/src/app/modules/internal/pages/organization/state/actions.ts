import {OrganizationsActions} from "./organizations/types";
import {DivisionsActions} from "./divisions/types";
import {PolicyActions} from "./policies/types";
import {PermissionActions} from "./permissions/types";
import {UserActions} from "./users/types";

export type OrganizationsActionTypes =
  | OrganizationsActions
  | DivisionsActions
  | UserActions
  | PolicyActions
  | PermissionActions;
