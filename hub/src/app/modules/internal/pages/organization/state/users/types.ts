import {User} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_USERS = "organizations/loadingUsers";

export interface LoadingUsersAction {
  type: typeof LOADING_USERS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_USERS_FAILED = "organizations/loadingUsersFailed";

export interface LoadingUsersFailedAction {
  type: typeof LOADING_USERS_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const USERS_LOADED = "organizations/usersLoaded";

export interface UsersLoadedAction {
  type: typeof USERS_LOADED;
  payload: User[];
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_USER_ACTION = "organizations/processingUserAction";

export interface ProcessingUserActionAction {
  type: typeof PROCESSING_USER_ACTION;
}

// -----------------------------------------------------------------------------------------------------------
export const PROCESSING_USER_ACTION_FAILED = "organizations/processingUserActionFailed";

export interface ProcessingUserActionFailedAction {
  type: typeof PROCESSING_USER_ACTION_FAILED;
  payload: {errorText?: string};
}

// -----------------------------------------------------------------------------------------------------------
export const USER_ACTION_PROCESSED = "organizations/userActionProcessed";

export interface UserActionProcessedAction {
  type: typeof USER_ACTION_PROCESSED;
  payload: User;
}

// -----------------------------------------------------------------------------------------------------------
export type UserActions =
  | LoadingUsersAction
  | LoadingUsersFailedAction
  | UsersLoadedAction
  | ProcessingUserActionAction
  | ProcessingUserActionFailedAction
  | UserActionProcessedAction;
