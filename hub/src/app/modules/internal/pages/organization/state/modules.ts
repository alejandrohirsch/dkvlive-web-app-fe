import {OrganizationsState} from "./reducer";
import {OrganizationsActionTypes} from "./actions";
import {ThunkAction} from "redux-thunk";
import {reducer} from "./reducer";
import {IModule} from "redux-dynamic-modules";

// -----------------------------------------------------------------------------------------------------------
export interface OrganizationsModuleState {
  organizations: OrganizationsState;
}

export type OrganizationsThunkResult<R> = ThunkAction<
  R,
  OrganizationsModuleState,
  void,
  OrganizationsActionTypes
>;

export const OrganizationsModule: IModule<OrganizationsModuleState> = {
  id: "organizations",
  reducerMap: {
    organizations: reducer,
  },
};
