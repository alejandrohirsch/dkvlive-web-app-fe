import {Permission} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_PERMISSIONS = "organizations/loadingPermissions";

export interface LoadingPermissionsAction {
  type: typeof LOADING_PERMISSIONS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_PERMISSIONS_FAILED = "organizations/loadingPermissionsFailed";

export interface LoadingPermissionsFailedAction {
  type: typeof LOADING_PERMISSIONS_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const PERMISSIONS_LOADED = "organizations/permissionsLoaded";

export interface PermissionsLoadedAction {
  type: typeof PERMISSIONS_LOADED;
  payload: Permission[];
}

// -----------------------------------------------------------------------------------------------------------
export type PermissionActions =
  | LoadingPermissionsAction
  | LoadingPermissionsFailedAction
  | PermissionsLoadedAction;
