import {OrganizationsModuleState} from "../modules";
import {createSelector} from "@reduxjs/toolkit";

export const divisionsItemsSelector = (state: OrganizationsModuleState) =>
  state.organizations.divisions.items;

// list
export const divisionsListStateSelector = (state: OrganizationsModuleState) =>
  state.organizations.divisions.list;

export const divisionsListSelector = createSelector(divisionsItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// item
export const divisionsItemStateSelector = (state: OrganizationsModuleState) =>
  state.organizations.divisions.item;

export const divisionsItemSelector = (id: string) =>
  createSelector(divisionsItemsSelector, (items) => (items ? items[id] : {}));

// resources
export const resourcesItemsStateSelector = (state: OrganizationsModuleState) =>
  state.organizations.resources.items;
