import {OrganizationsThunkResult} from "../modules";
import {Permission} from "./reducer";
import httpClient from "services/http";
import {
  LoadingPermissionsAction,
  LOADING_PERMISSIONS,
  PermissionsLoadedAction,
  PERMISSIONS_LOADED,
  LoadingPermissionsFailedAction,
  LOADING_PERMISSIONS_FAILED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingPermissionsAction(): LoadingPermissionsAction {
  return {
    type: LOADING_PERMISSIONS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingPermissionsFailedAction(
  errorText?: string,
  unsetItems = true
): LoadingPermissionsFailedAction {
  return {
    type: LOADING_PERMISSIONS_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function permissionsLoadedAction(items: Permission[]): PermissionsLoadedAction {
  return {
    type: PERMISSIONS_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadPermissions(): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingPermissionsAction());

    const [{data}, err] = await httpClient.get("/management/permissions/findAll", {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        loadingPermissionsFailedAction("Laden fehlgeschlagen" + (": " + err.response?.data.message || ""))
      );
      return;
    }

    dispatch(permissionsLoadedAction(data));
  };
}

export function loadPermissionsIfNeeded(): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().organizations.permissions;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadPermissions());
  };
}
