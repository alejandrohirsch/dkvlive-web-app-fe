import {OrganizationsModuleState} from "../modules";
import {createSelector} from "@reduxjs/toolkit";

export const policiesItemsSelector = (state: OrganizationsModuleState) => state.organizations.policies.items;

// list
export const policiesListStateSelector = (state: OrganizationsModuleState) =>
  state.organizations.policies.list;

export const policiesListSelector = createSelector(policiesItemsSelector, (items) =>
  items ? Object.values(items) : []
);

export const policiesForOrganizationSelector = (state: OrganizationsModuleState) =>
  Object.values(state.organizations.policies.items ?? {}).filter(
    (i) => !i.organization_id || i.organization_id === state.organizations.organizations.item.id
  );

// item
export const policiesItemStateSelector = (state: OrganizationsModuleState) =>
  state.organizations.policies.item;
export const policiesSelector = (id: string) =>
  createSelector(policiesItemsSelector, (items) => (items ? items[id] : undefined));
