import {OrganizationsState} from "../reducer";
import {UsersLoadedAction, LoadingUsersFailedAction, ProcessingUserActionFailedAction} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface User {
  id?: string;
  organization_id?: string;
  active?: boolean;
  email: string;
  first_name?: string;
  last_name?: string;
  display_name?: string;
  language: string;
  phone?: string;
  note?: string;
  position_tags: Tag[];
  divisions_with_names: Tag[];
  policies: string[];
  created_at?: string;
  modified_at?: string;
  last_login_at?: string;
  valid_until?: string;
}

export interface Tag {
  id?: string;
  name: string;
}

export const emptyUser: User = {
  email: "",
  first_name: "",
  last_name: "",
  language: "de",
  position_tags: [],
  divisions_with_names: [],
  policies: [],
};

export interface UserData {
  list: {
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  item: {
    loading: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: string]: User};
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingUsers(state: OrganizationsState) {
  const data = state.users.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingUsersFailed(state: OrganizationsState, action: LoadingUsersFailedAction) {
  const data = state.users;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleUsersLoaded(state: OrganizationsState, action: UsersLoadedAction) {
  const data = state.users;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.forEach((a) => {
    if (a.id) {
      items[a.id] = Object.assign({...emptyUser}, a);
    }
  });
  data.items = items;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleProcessingUserAction(state: OrganizationsState) {
  const data = state.users;

  data.item.loading = true;

  if (data.item.error) {
    delete data.item.error;
    delete data.item.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------

export function handleProcessingUserActionFailed(
  state: OrganizationsState,
  action: ProcessingUserActionFailedAction
) {
  const data = state.users;

  data.item.loading = false;
  data.item.error = true;
  data.item.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------

export function handleUserActionProcessed(state: OrganizationsState) {
  const data = state.users;

  data.item.loading = false;

  if (data.item.error) {
    delete data.item.error;
    delete data.item.errorText;
  }
}
