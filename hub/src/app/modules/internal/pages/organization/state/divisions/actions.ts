import {
  LoadingDivisionsAction,
  LOADING_DIVISIONS,
  LoadingDivisionsFailedAction,
  LOADING_DIVISIONS_FAILED,
  DivisionsLoadedAction,
  DIVISIONS_LOADED,
  ProcessingDivisionActionFailedAction,
  ProcessingDivisionActionAction,
  DivisionActionProcessedAction,
  DIVISION_ACTION_PROCESSED,
  PROCESSING_DIVISION_ACTION_FAILED,
  PROCESSING_DIVISION_ACTION,
  CheckingDivisionResourcesAction,
  CHECKING_DIVISION_RESOURCES,
  CheckingDivisionResourcesFailedAction,
  CHECKING_DIVISION_RESOURCES_FAILED,
  DivisionResourcesCheckedAction,
  DIVISION_RESOURCES_CHECKED,
} from "../divisions/types";
import {Division, ResourcesData} from "./reducer";
import {OrganizationsThunkResult} from "../modules";
import httpClient from "services/http";

// -----------------------------------------------------------------------------------------------------------
function loadingDivisionsAction(): LoadingDivisionsAction {
  return {
    type: LOADING_DIVISIONS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingDivisionsFailedAction(errorText?: string, unsetItems = true): LoadingDivisionsFailedAction {
  return {
    type: LOADING_DIVISIONS_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function divisionsLoadedAction(items: Division[]): DivisionsLoadedAction {
  return {
    type: DIVISIONS_LOADED,
    payload: items,
  };
}
// -----------------------------------------------------------------------------------------------------------
function processingDivisionActionAction(): ProcessingDivisionActionAction {
  return {
    type: PROCESSING_DIVISION_ACTION,
  };
}

// -----------------------------------------------------------------------------------------------------------
function processingDivisionActionFailedAction(errorText?: string): ProcessingDivisionActionFailedAction {
  return {
    type: PROCESSING_DIVISION_ACTION_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function divisionActionProcessedAction(division?: Record<string, unknown>): DivisionActionProcessedAction {
  return {
    type: DIVISION_ACTION_PROCESSED,
    payload: {division},
  };
}

// -----------------------------------------------------------------------------------------------------------
function checkingDivisionResourcesAction(): CheckingDivisionResourcesAction {
  return {
    type: CHECKING_DIVISION_RESOURCES,
  };
}

// -----------------------------------------------------------------------------------------------------------
function checkingDivisionResourcesFailedAction(errorText?: string): CheckingDivisionResourcesFailedAction {
  return {
    type: CHECKING_DIVISION_RESOURCES_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function divisionResourcesCheckedAction(items: ResourcesData): DivisionResourcesCheckedAction {
  return {
    type: DIVISION_RESOURCES_CHECKED,
    payload: items,
  };
}
// -----------------------------------------------------------------------------------------------------------
// export function setOrganizationID(organizationID: string): OrganizationsThunkResult<Promise<void>> {
//   return async (dispatch, getState) => {
//     const currentID = getState().organizations.divisions.list.organizationID;
//     if (organizationID !== currentID) {
//       dispatch(setOrganizationIDAction(organizationID));
//     }
//     dispatch(loadDivisionsIfNeeded(organizationID));
//   };
// }

export function loadDivisions(organizationID: string): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingDivisionsAction());
    const [{data}, err] = await httpClient.get("/management/organizations/findWithDivisions", {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        loadingDivisionsFailedAction("Laden fehlgeschlagen" + (": " + err.response?.data.message || ""))
      );
      return;
    }

    dispatch(divisionsLoadedAction(data.find((d: any) => d.id === organizationID).divisions));
  };
}

export function loadDivisionsIfNeeded(organizationID: string): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().organizations.divisions;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadDivisions(organizationID));
  };
}

export function updateDivision(
  id: string,
  division: Division,
  callback?: (success: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingDivisionActionAction());
    const [{response}, err] = await httpClient.put("/management/divisions/" + id, JSON.stringify(division), {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        processingDivisionActionFailedAction(
          "Speichern fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);

      return;
    }

    dispatch(divisionActionProcessedAction(response));
    callback && callback(true);
  };
}

export function createDivision(
  division: Division,
  callback?: (success: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingDivisionActionAction());
    const [{response}, err] = await httpClient.post("/management/divisions", JSON.stringify(division), {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        processingDivisionActionFailedAction(
          "Erstellen fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);

      return;
    }

    dispatch(divisionActionProcessedAction(response));
    callback && callback(true);
  };
}

export function deleteDivision(
  id: string,
  callback?: (success: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(processingDivisionActionAction());
    const [{response}, err] = await httpClient.delete("/management/divisions/" + id, {
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(
        processingDivisionActionFailedAction(
          "Löschen fehlgeschlagen" + (": " + err.response?.data.message || "")
        )
      );
      callback && callback(false);
      return;
    }

    dispatch(divisionActionProcessedAction(response));
    callback && callback(true);
  };
}

export function checkDivisionResources(
  id: string,
  limit: number, // 0 for unlimited results
  callback?: (success: boolean, hasResources?: boolean) => void
): OrganizationsThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(checkingDivisionResourcesAction());
    const [{data}, err] = await httpClient.get("/management/divisions/checkResources/" + id, {
      headers: {
        "Content-Type": "application/json",
      },
      params: {
        limit: limit,
      },
    });

    if (err !== null) {
      // @todo: log
      dispatch(checkingDivisionResourcesFailedAction("Laden von Resourcen fehlgeschlagen"));
      callback && callback(false);
      return;
    }

    const hasResources = Boolean(Object.keys(data).length);
    dispatch(divisionResourcesCheckedAction(data));
    callback && callback(true, hasResources);
  };
}
