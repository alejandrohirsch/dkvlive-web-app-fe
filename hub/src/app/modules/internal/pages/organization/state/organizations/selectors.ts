import {OrganizationsModuleState} from "../modules";
import {createSelector} from "@reduxjs/toolkit";

export const organizationsItemsSelector = (state: OrganizationsModuleState) =>
  state.organizations.organizations.items || [];

// list
export const organizationsListStateSelector = (state: OrganizationsModuleState) =>
  state.organizations.organizations.list;

export const organizationsListSelector = createSelector(organizationsItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// item
export const organizationsItemStateSelector = (state: OrganizationsModuleState) =>
  state.organizations.organizations.item;

export const organizationsItemSelector = (id: string) =>
  createSelector(organizationsItemsSelector, (items) => (items ? items[id] : {}));
