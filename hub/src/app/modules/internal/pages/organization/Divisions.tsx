import React, {useState, useCallback, useEffect} from "react";
import Page from "app/components/Page";
import {useTranslation} from "react-i18next";
import DataTable, {
  TablePageContent,
  useColumns,
  TableStickyHeader,
  TableStickyFooter,
  TableContainer,
  ToolbarButton,
  ToolbarButtonIcon,
  TableFooterActions,
  bodyField,
  TableSelectionCountInfo,
  TableFooterRightActions,
} from "app/components/DataTable";
import {Button, Loading} from "dkv-live-frontend-ui";
import Window, {WindowContentContainer, WindowHeadline, WindowContent} from "app/components/Window";
import store from "app/state/store";
import {OrganizationsModule} from "./state/modules";
import {useDispatch, useSelector} from "react-redux";
import {
  divisionsListSelector,
  divisionsListStateSelector,
  divisionsItemSelector,
  resourcesItemsStateSelector,
} from "./state/divisions/selectors";
import {loadDivisions, deleteDivision, checkDivisionResources} from "./state/divisions/actions";
import {Division, emptyDivision} from "./state/divisions/reducer";
import DivisionDetails from "./components/DivisionDetails";
import DivisionForm from "./components/DivisionForm";
import ConfirmWindow from "app/components/ConfirmWindow";
import {useParams} from "react-router-dom";
import {organizationsItemsSelector} from "./state/organizations/selectors";
import dayjs from "dayjs";
import DivisionActivityLog from "./components/DivisionActivityLog";
import {availableLanguages} from "../i18n/I18n";
import {hasPermission} from "app/modules/login/state/login/selectors";
import styled, {css} from "styled-components";

//-----------------------------------------------------------------------------------------------------------
store.addModule(OrganizationsModule);

//-----------------------------------------------------------------------------------------------------------
const divisionColumns = [
  {
    field: "name",
    header: "Name",
    width: 200,
  },
  {
    field: "language_code",
    header: "Sprache",
    body: bodyField((a) => availableLanguages.find((l: any) => l.value === a.language_code)?.label),
    width: 140,
  },
  {
    field: "created_at",
    header: "Angelegt",
    width: 140,
    body: (a: Division) =>
      a.created_at && a.created_at !== "0001-01-01T00:00:00Z"
        ? dayjs(a.created_at).format("DD.MM.YYYY HH:mm:ss")
        : "",
  },
  {
    field: "modified_at",
    header: "Geändert",
    width: 140,
    body: (a: Division) =>
      a.modified_at && a.modified_at !== "0001-01-01T00:00:00Z"
        ? dayjs(a.modified_at).format("DD.MM.YYYY HH:mm:ss")
        : "",
  },
];

export const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

export const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: left;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

export const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);
  text-align: left;

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

//-----------------------------------------------------------------------------------------------------------
interface DivisionsProps {
  backPath: string;
}

const Divisions: React.FC<DivisionsProps> = ({backPath}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const {id = ""} = useParams();

  // columns
  const [columns] = useState(divisionColumns);
  const visibleColumns = useColumns(columns, t);

  // selection
  const [selected, setSelected] = useState<Division[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const deselect = () => setSelected([]);

  // load divisions
  const organizations = useSelector(organizationsItemsSelector);
  const divisionsList = useSelector(divisionsListSelector);
  useEffect(() => {
    dispatch(loadDivisions(id));
  }, [dispatch, id]);
  const divisionsItem = useSelector(divisionsItemSelector);

  const divisionsListState = useSelector(divisionsListStateSelector);

  const resources = useSelector(resourcesItemsStateSelector);

  // actions
  // - detail
  const [activeDivisionWindow, setActiveDivisionWindow] = useState<Division | null>(null);
  const onCloseDivisionWindow = useCallback(() => {
    const id = activeDivisionWindow?.id || "";
    const updatedSelection = selected.filter((s) => s.id !== id);
    setActiveDivisionWindow(null);
    setEdit(false);
    setSelected(updatedSelection);
  }, [activeDivisionWindow, selected]);
  const openDivisionWindow = useCallback((e: any) => setActiveDivisionWindow(e.data as Division), [
    setActiveDivisionWindow,
  ]);

  // - edit
  const [edit, setEdit] = useState<boolean>(false);

  // - create
  const openDivisionCreateWindow = () => {
    setEdit(true);
    setActiveDivisionWindow(Object.assign({...emptyDivision}));
  };

  // - delete
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
  const onDeleteConfirm = async (confirmed: boolean) => {
    if (!confirmed || !selected[0].id) {
      setShowDeleteConfirmation(false);
      return;
    }

    const callback = (success: boolean) => {
      if (success) {
        dispatch(loadDivisions(id));
        deselect();
        setShowDeleteConfirmation(false);
      }
    };

    dispatch(deleteDivision(selected[0].id, callback));
  };

  // - show resources
  const [showExistingResourcesWindow, setShowExistingResourcesWindow] = useState(false);
  const resourceLimit = 10;
  const checkForResources = () => {
    if (!selected[0].id) {
      setShowExistingResourcesWindow(false);
      return;
    }

    const callback = (success: boolean, hasResources?: boolean) => {
      if (success && hasResources) {
        setShowExistingResourcesWindow(true);
      } else if (success) {
        setShowDeleteConfirmation(true);
        setShowExistingResourcesWindow(false);
      }
    };

    dispatch(checkDivisionResources(selected[0].id, resourceLimit, callback));
  };

  // - activity log
  const [showActivityWindow, setShowActivityWindow] = useState<boolean>(false);

  const openDivisionWindowForSelected = useCallback(
    (edit?: boolean) => {
      edit && setEdit(edit);
      selected.length && setActiveDivisionWindow(selected[0]);
    },
    [selected, setActiveDivisionWindow]
  );

  useEffect(() => {
    if (!activeDivisionWindow) {
      return;
    }

    const division = divisionsItem[activeDivisionWindow?.id || ""];

    if (division) {
      setActiveDivisionWindow(division);
    }
  }, [activeDivisionWindow, divisionsItem]);

  // render
  return (
    <Page id="divisionlist">
      <Loading inprogress={divisionsListState.loading} />
      <TablePageContent>
        <TableStickyHeader backPath={backPath}>
          <h1>
            {t("Divisions")} {organizations && organizations[id] && organizations[id].name}
          </h1>

          {/* @todo commingsoon */}
          {/* <SearchTextField
            name="search"
            type="text"
            required
            label={t("Suche")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          /> */}

          <ToolbarButton
            general
            style={{
              marginLeft: "auto",
            }}
            onClick={openDivisionCreateWindow}
          >
            <ToolbarButtonIcon>
              <span className="icon-ico_add" />
            </ToolbarButtonIcon>
            {t("Neue Division erstellen")}
          </ToolbarButton>
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={divisionsList}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            onRowDoubleClick={openDivisionWindow}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Division ausgewählt" : "Divisions ausgewählt")}
              />

              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={deselect}>
                {t("Abbrechen")}
              </Button>

              <TableFooterActions>
                {hasPermission("management:UpdateDivisions") && (
                  <ToolbarButton onClick={() => openDivisionWindowForSelected(true)}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_edit" />
                    </ToolbarButtonIcon>

                    {t("Bearbeiten")}
                  </ToolbarButton>
                )}
                {divisionsList.length > 1 && hasPermission("management:DeleteDivisions") && (
                  <ToolbarButton onClick={() => checkForResources()}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_delete" />
                    </ToolbarButtonIcon>

                    {t("Löschen")}
                  </ToolbarButton>
                )}
              </TableFooterActions>
              {/* <TableFooterRightActions>
            <Button autoWidth secondary ariaLabel={t("Gesamte Liste exportieren")}>
              {t("Gesamte Liste exportieren")}
            </Button>

            {selected.length > 0 && (
              <Button autoWidth ariaLabel={t("Auswahl als Liste exportieren")}>
                {t("Auswahl als Liste exportieren")}
              </Button>
            )}
          </TableFooterRightActions> */}
              <TableFooterRightActions />
            </>
          )}
        </TableStickyFooter>
      </TablePageContent>

      {activeDivisionWindow &&
        (edit ? (
          <Window
            onClose={onCloseDivisionWindow}
            headline={activeDivisionWindow.id ? t("Division bearbeiten: ") : t("Division erstellen")}
          >
            {(props) => <DivisionForm {...props} id={id} division={activeDivisionWindow} />}
          </Window>
        ) : (
          activeDivisionWindow.id && (
            <Window onClose={onCloseDivisionWindow} headline={t("Detailansicht Division")}>
              {(props) => (
                <DivisionDetails
                  {...props}
                  division={activeDivisionWindow}
                  setEdit={setEdit}
                  showActivity={setShowActivityWindow}
                />
              )}
            </Window>
          )
        ))}

      {showDeleteConfirmation && selected.length && (
        <ConfirmWindow
          headline={
            // (selected.length > 1
            //   ? selected.length + " " + t("ausgewählte Divisions")
            //   : t("Ausgewählte Division")) +
            // " " +
            // t("wirklich löschen?")
            t("Ausgewählte Division") + " " + selected[0].name + " " + t("wirklich löschen?")
          }
          onConfirm={onDeleteConfirm}
        />
      )}

      {showExistingResourcesWindow && (
        <Window onClose={() => setShowExistingResourcesWindow(false)}>
          <WindowContentContainer>
            <WindowHeadline padding>
              <h2>{t("Kann aufgrund vorhandener Resourcen in der Division nicht gelöscht werden")}</h2>
            </WindowHeadline>
            <WindowContent padding>
              <Table>
                <tbody>
                  {resources?.assets && (
                    <tr>
                      <TableLabel>
                        {resources.assets.total_count === 1
                          ? `1 ${t("Fahrzeug")}:`
                          : `${resources.assets.total_count} ${t("Fahrzeuge")}:`}
                      </TableLabel>
                      <TableValue>
                        <ul>
                          {resources.assets.list.map((i: any) => (
                            <li>- {i.name}</li>
                          ))}
                          {resources.assets.total_count > resourceLimit && (
                            <li>
                              {t("... und weitere {{.count}}", {
                                count: resources.assets.total_count - resourceLimit,
                              })}
                            </li>
                          )}
                        </ul>
                      </TableValue>
                    </tr>
                  )}
                  {resources?.devices && (
                    <tr>
                      <TableLabel>
                        {resources.devices.total_count === 1
                          ? `1 ${t("Device")}:`
                          : `${resources.devices.total_count} ${t("Devices")}:`}
                      </TableLabel>
                      <TableValue>
                        <ul>
                          {resources.devices.list.map((i: any) => (
                            <li>- {i.name}</li>
                          ))}
                          {resources.devices.total_count > resourceLimit && (
                            <li>
                              {t("... und weitere {{.count}}", {
                                count: resources.devices.total_count - resourceLimit,
                              })}
                            </li>
                          )}
                        </ul>
                      </TableValue>
                    </tr>
                  )}
                  {resources?.drivers && (
                    <tr>
                      <TableLabel>
                        {resources.drivers.total_count === 1
                          ? `1 ${t("Fahrer")}:`
                          : `${resources.drivers.total_count} ${t("Fahrer")}:`}
                      </TableLabel>
                      <TableValue>
                        <ul>
                          {resources.drivers.list.map((i: any) => (
                            <li>- {i.first_name + " " + i.last_name}</li>
                          ))}
                          {resources.drivers.total_count > resourceLimit && (
                            <li>
                              {t("... und weitere {{.count}}", {
                                count: resources.drivers.total_count - resourceLimit,
                              })}
                            </li>
                          )}
                        </ul>
                      </TableValue>
                    </tr>
                  )}
                  {resources?.users && (
                    <tr>
                      <TableLabel>
                        {resources.users.total_count === 1
                          ? `1 ${t("Benutzer")}:`
                          : `${resources.users.total_count} ${t("Benutzer")}:`}
                      </TableLabel>
                      <TableValue>
                        <ul>
                          {resources.users.list.map((i: any) => (
                            <li>- {i.first_name + " " + i.last_name}</li>
                          ))}
                          {resources.users.total_count > resourceLimit && (
                            <li>
                              {t("... und weitere {{.count}}", {
                                count: resources.users.total_count - resourceLimit,
                              })}
                            </li>
                          )}
                        </ul>
                      </TableValue>
                    </tr>
                  )}
                  {resources?.tags && (
                    <tr>
                      <TableLabel>
                        {resources.tags.total_count === 1
                          ? `1 ${t("Position")}:`
                          : `${resources.tags.total_count} ${t("Positionen")}:`}
                      </TableLabel>
                      <TableValue>
                        <ul>
                          {resources.tags.list.map((i: any) => (
                            <li>- {i.name}</li>
                          ))}
                          {resources.tags.total_count > resourceLimit && (
                            <li>
                              {t("... und weitere {{.count}}", {
                                count: resources.tags.total_count - resourceLimit,
                              })}
                            </li>
                          )}
                        </ul>
                      </TableValue>
                    </tr>
                  )}
                  {resources?.geofences && (
                    <tr>
                      <TableLabel>
                        {" "}
                        {resources.geofences.total_count === 1
                          ? `1 ${t("Geozone")}:`
                          : `${resources.geofences.total_count} ${t("Geozonen")}:`}
                      </TableLabel>
                      <TableValue>
                        <ul>
                          {resources.geofences.list.map((i: any) => (
                            <li>- {i.name}</li>
                          ))}
                          {resources.geofences.total_count > resourceLimit && (
                            <li>
                              {t("... und weitere {{.count}}", {
                                count: resources.geofences.total_count - resourceLimit,
                              })}
                            </li>
                          )}
                        </ul>
                      </TableValue>
                    </tr>
                  )}
                </tbody>
              </Table>
            </WindowContent>
          </WindowContentContainer>
        </Window>
      )}

      {showActivityWindow && activeDivisionWindow && (
        <Window onClose={() => setShowActivityWindow(false)} headline={t("Aktivitäten Division")}>
          {(props) => <DivisionActivityLog {...props} divisionID={activeDivisionWindow.id || ""} />}
        </Window>
      )}
    </Page>
  );
};

export default Divisions;
