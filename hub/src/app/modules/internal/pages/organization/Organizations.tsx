import React from "react";
import store from "app/state/store";
import {useRouteMatch, Switch, Route} from "react-router-dom";
import Page from "app/components/Page";
import {OrganizationsModule} from "./state/modules";
import OrganizationsList from "./OrganizationsList";
import Divisions from "./Divisions";

// -----------------------------------------------------------------------------------------------------------
store.addModule(OrganizationsModule);

// -----------------------------------------------------------------------------------------------------------
const Organizations: React.FC = () => {
  // route
  const {path} = useRouteMatch();

  // render
  return (
    <Page id="organizations">
      <Switch>
        <Route exact path={`${path}`}>
          <OrganizationsList path={path} />
        </Route>
        <Route exact path={`${path}/:id`}>
          <Divisions backPath={path} />
        </Route>
      </Switch>
    </Page>
  );
};

export default Organizations;
