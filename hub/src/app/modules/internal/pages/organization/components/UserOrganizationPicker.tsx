import React, {useState, useEffect} from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {Button, AutocompleteInput, Loading} from "dkv-live-frontend-ui";
import {
  WindowActionButtons,
  WindowContentContainer,
  WindowHeadline,
  WindowContentProps,
  WindowContent,
} from "app/components/Window";
import {TextField} from "dkv-live-frontend-ui";
import {useDispatch, useSelector} from "react-redux";
import {usersItemStateSelector} from "../state/users/selectors";
import {organizationsListSelector} from "../state/organizations/selectors";
import {loadOrganizationsIfNeeded} from "../state/organizations/actions";

// -----------------------------------------------------------------------------------------------------------

interface SubContainerProps {
  width?: string;
}

const SubContainer = styled.div<SubContainerProps>`
  display: inline-flex;
  flex-direction: column;
  margin: 10px;
  overflow: auto;
  font-size: 1.1428571428571428rem;
  padding-top: 1em;
  display: grid;
  width: 99%;

  h1 {
    display: inline-flex;
    margin-top: 24px;
    margin-bottom: 10px;
    font-size: inherit;
    font-weight: bold;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface UserOrganizationPickerProps extends WindowContentProps {
  onConfirm: (uuid: string) => void;
}

const UserOrganizationPicker: React.FC<UserOrganizationPickerProps> = ({
  headline,
  onConfirm,
  forceCloseWindow,
}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const [organizationUUID, setOrganizationUUID] = useState("");
  const handleOrganizationChange = (_: any, newValue: any) => {
    setOrganizationUUID(newValue?.id || "");
  };

  const itemState = useSelector(usersItemStateSelector);

  useEffect(() => {
    dispatch(loadOrganizationsIfNeeded());
  }, [dispatch]);
  const organizations = useSelector(organizationsListSelector);

  // render
  return (
    <WindowContentContainer full minWidth="500px" minHeight="300px">
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <Loading inprogress={itemState.loading} />

      <WindowContent padding>
        <SubContainer>
          <AutocompleteInput
            value={organizations.find((o: any) => o.id === organizationUUID) ?? null}
            onChange={handleOrganizationChange}
            options={organizations}
            autoHighlight
            getOptionLabel={(option: any) => t(option.name)}
            renderInput={(params: any) => (
              <TextField
                {...params}
                label="Organization"
                variant="outlined"
                required
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password",
                }}
              />
            )}
            clearText={t("Leeren")}
            closeText={t("Schließen")}
            loadingText={t("Lädt...")}
            noOptionsText={t("Keine Optionen")}
            openText={t("Öffnen")}
          />
        </SubContainer>

        <WindowActionButtons style={{position: "absolute", height: "40px", bottom: "40px", width: "90%"}}>
          <Button type="button" secondary onClick={forceCloseWindow}>
            {t("Abbrechen")}
          </Button>

          <Button type="button" onClick={() => onConfirm(organizationUUID)} disabled={!organizationUUID}>
            {t("OK")}
          </Button>
        </WindowActionButtons>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default UserOrganizationPicker;
