import React, {useState, useEffect} from "react";
import {useTranslation} from "react-i18next";
import styled, {css} from "styled-components/macro";
import {FormControlLabel, Tooltip, Icon} from "@material-ui/core";
import {Link} from "react-router-dom";
import {Checkbox} from "dkv-live-frontend-ui";
import {PermissionField} from "./PermissionField";
import {User} from "../state/users/reducer";
import {useDispatch, useSelector} from "react-redux";
import {permissionsListSelector} from "../state/permissions/selectors";
import {loadPoliciesIfNeeded} from "../state/policies/actions";
import {loadPermissionsIfNeeded} from "../state/permissions/actions";
import {Policy} from "../state/policies/reducer";
import {Permission} from "../state/permissions/reducer";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------

interface SubContainerProps {
  width?: string;
}

const SubContainer = styled.div<SubContainerProps>`
  display: inline-flex;
  flex-direction: column;
  margin: 10px;
  flex-grow: 1;
  overflow: auto;
  font-size: 1.1428571428571428rem;
  max-height: 450px;

  ${(props) =>
    props.width
      ? css`
          width: calc(${props.width});
          flex-grow: 1;
        `
      : css`
          max-width: 98%;
        `};

  h1 {
    position: relative;
    display: inline-flex;
    align-items: center;
    margin-top: 24px;
    margin-bottom: 10px;
    font-size: inherit;
    font-weight: bold;
  }
`;

interface PolicyFieldProps {
  allowsHoveredPermission?: boolean;
  deniesHoveredPermission?: boolean;
  edit?: boolean;
}

const PolicyField = styled.div<PolicyFieldProps>`
  display: inline-flex;
  align-items: center;
  padding-left: 15px;
  font-size: inherit;
  height: 30px;

  ${(props) =>
    !props.edit &&
    css`
      cursor: default;
      .p-checkbox,
      .MuiCheckbox-root,
      .MuiFormControlLabel-root,
      .MuiFormControlLabel-label {
        cursor: default;
      }
      .MuiIconButton-root:hover {
        background-color: inherit;
      }
      .MuiTouchRipple-root {
        display: none;
      }
    `};

  ${(props) =>
    css`
      background-color: ${props.deniesHoveredPermission
        ? "#fadbdb"
        : props.allowsHoveredPermission && "#def2de"};
      width: 100%;

      border-radius: 4px;

      .MuiCheckbox-root,
      .MuiFormControlLabel-label {
        margin-left: 5px;
        color: ${props.deniesHoveredPermission ? "#e44c4d" : props.allowsHoveredPermission && "#58be58"};

        ${!props.edit && "cursor: default;"}
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    `};

  .MuiFormControlLabel-label:hover {
    font-weight: bold;
  }
`;

const PolicyCreate = styled.div`
  width: fit-content;
  cursor: pointer;
  font-size: 15px;
  padding-left: 4px;
  vertical-align: middle;
  display: inline-flex;
  align-items: center;
  vertical-align: middle;
  height: 30px;
  color: var(--dkv-link-primary-color);

  span {
    height: 100%;
    font-size: 20px;
    width: 24px;
    margin-right: 5px;
  }
`;

const PolicyLink = styled(Link)`
  position: absolute;
  top: 0;
  right: 0;
  span {
    font-size: 20px;
    width: 24px;
    margin-right: 20px;
    margin-top: 24px;
  }
  a:hover {
    color: #2e6b90;
  }
`;

const PermissionGroup = styled.div`
  padding-bottom: 10px;
  font-variant: small-caps;
`;

// -----------------------------------------------------------------------------------------------------------
interface UserPoliciesProps {
  user: User;
  policiesList: Policy[];
  onToggle?: (target: string) => void;
  onCreateNewPolicy?: () => void;
}

const UserPolicies: React.FC<UserPoliciesProps> = ({user, onToggle, policiesList, onCreateNewPolicy}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadPoliciesIfNeeded());
  }, [dispatch]);

  // load permissions
  const permissionsList = useSelector(permissionsListSelector);
  useEffect(() => {
    dispatch(loadPermissionsIfNeeded());
  }, [dispatch]);

  const [showAllowedOnly, toggleShowAllowedOnly] = useState(true);
  const [hoveredPolicy, setHoveredPolicy] = useState<Policy | null>(null);
  const [hoveredPermission, setHoveredPermission] = useState<Permission | null>(null);

  const [evaluatedPermissions, setEvaluatedPermissions] = useState<any[]>([]);
  useEffect(
    () =>
      setEvaluatedPermissions(
        permissionsList.map((perm: Permission) => ({
          ...perm,
          denied: policiesList.some(
            (pol: Policy) =>
              pol && user.policies.includes(pol.id || "") && pol.permissions.deny.includes(perm.key)
          ),
          deniedPreview: hoveredPolicy !== null && hoveredPolicy.permissions.deny.includes(perm.key),
          allowed: policiesList.some(
            (pol: Policy) =>
              pol && user.policies.includes(pol.id || "") && pol.permissions.allow.includes(perm.key)
          ),
          allowedPreview: hoveredPolicy !== null && hoveredPolicy.permissions.allow.includes(perm.key),
        }))
      ),
    [hoveredPolicy, permissionsList, dispatch, user, policiesList]
  );

  // group permissions
  const [groupedPermissionsList, setGroupedPermissionsList] = useState<any>({});
  useEffect(() => {
    if (evaluatedPermissions.length) {
      const groupedList = {};
      const groups = [...new Set(evaluatedPermissions.map((p) => p.key.split(":")[0]))];
      groups.forEach((g) => (groupedList[g] = []));
      evaluatedPermissions.forEach((p) => groupedList[p.key.split(":")[0]].push(p));
      setGroupedPermissionsList(groupedList);
    }
  }, [evaluatedPermissions]);

  // render
  return (
    <>
      <SubContainer width="35%">
        <h1>
          {t("Policies")}{" "}
          <Tooltip
            title={
              <div>
                {t(
                  "Beim Mouseover einer Policy werden rechts alle Berechtigungen farblich unterlegt angezeigt, die durch Auswahl der Policy dazukommen würden. Ausgegraute Berechtigungen würden nicht greifen, da sie durch eine andere Policy übrschrieben werden."
                )}
              </div>
            }
          >
            <Icon className="icon-ico_help" />
          </Tooltip>
          {Boolean(onToggle) && hasPermission("policies:Visible") && (
            <PolicyLink to="/_internal/policies">
              <span className="icon-ico_list" title={t("Zur Policy-Übersicht")} />
            </PolicyLink>
          )}
        </h1>
        {policiesList.map((pol: Policy) => (
          <PolicyField
            allowsHoveredPermission={
              hoveredPermission !== null &&
              pol.permissions.allow.some((perm) => perm === hoveredPermission.key)
            }
            deniesHoveredPermission={
              hoveredPermission !== null &&
              pol.permissions.deny.some((perm) => perm === hoveredPermission.key)
            }
            key={pol.id}
            edit={Boolean(onToggle)}
          >
            <FormControlLabel
              label={t(pol.name)}
              onMouseEnter={() => setHoveredPolicy(pol)}
              onMouseLeave={() => setHoveredPolicy(null)}
              control={
                <Checkbox
                  checked={user.policies && user.policies.includes(pol.id || "")}
                  onChange={onToggle ? (_: any) => onToggle(pol.id || "") : undefined}
                />
              }
            />
          </PolicyField>
        ))}
        {hasPermission("management:CreatePolicies") && Boolean(onCreateNewPolicy) && (
          <PolicyCreate onClick={onCreateNewPolicy}>
            <span className="icon-ico_add" />
            {t("Neue Policy anlegen")}
          </PolicyCreate>
        )}
      </SubContainer>
      <SubContainer width="65%">
        <h1>
          {t("Berechtigungen ")}
          <Tooltip
            title={
              <div>
                {t(
                  "Beim Mouseover einer Berechtigung werden links alle Policies farblich markiert, die einen Einfluss auf diese haben. Die Berechtigungen je nach Policy können nur direkt über die Policy-Verwaltung angepasst werden."
                )}
              </div>
            }
          >
            <Icon className="icon-ico_help" />
          </Tooltip>
        </h1>

        <FormControlLabel
          style={{marginLeft: "10px", marginBottom: "20px"}}
          control={
            <Checkbox
              style={{marginRight: "10px"}}
              checked={showAllowedOnly}
              onChange={(_: any) => toggleShowAllowedOnly(!showAllowedOnly)}
            />
          }
          label={t("zeige nur explizit erteilte Berechtigungen")}
        />
        {Object.keys(groupedPermissionsList).map(
          (key: string) =>
            groupedPermissionsList[key].some(
              (perm: any) => !showAllowedOnly || perm.allowed || perm.allowedPreview || perm.deniedPreview
            ) && (
              <PermissionGroup key={key}>
                {key}
                {groupedPermissionsList[key].map(
                  (perm: any) =>
                    (!showAllowedOnly || perm.allowed || perm.allowedPreview || perm.deniedPreview) && (
                      <PermissionField
                        key={perm.key}
                        denied={perm.denied}
                        allowed={perm.allowed}
                        deniedPreview={perm.deniedPreview}
                        allowedPreview={perm.allowedPreview}
                        onMouseEnter={() => setHoveredPermission(perm)}
                        onMouseLeave={() => setHoveredPermission(null)}
                      >
                        <Icon
                          className={
                            (perm.allowed && !perm.deniedPreview && !perm.denied) || perm.allowedPreview
                              ? "icon-ico_unlock"
                              : ""
                          }
                        />
                        {t(perm.name)}
                        <Icon
                          className={
                            (perm.denied && !perm.allowedPreview) || perm.deniedPreview ? "icon-ico_lock" : ""
                          }
                        />
                      </PermissionField>
                    )
                )}
              </PermissionGroup>
            )
        )}
      </SubContainer>
    </>
  );
};

export default UserPolicies;
