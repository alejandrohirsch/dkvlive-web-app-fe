import React from "react";
import {useTranslation} from "react-i18next";
import {
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowActions,
  WindowPanel,
} from "app/components/Window";
import {ToolbarButton, ToolbarButtonIcon} from "app/components/DataTable";
import {Table} from "@material-ui/core";
import {Division} from "../state/divisions/reducer";
import {TableLabel, TableValue} from "../../../../live/pages/map/components/details/Overview";
import {availableLanguages} from "../../i18n/I18n";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
interface DivisionDetailsWindowProps extends WindowContentProps {
  division: Division;
  setEdit: (_: any) => void;
  showActivity: (_: any) => void;
}

const DivisionDetails: React.FC<DivisionDetailsWindowProps> = ({
  division,
  setEdit,
  headline,
  showActivity,
}) => {
  const {t} = useTranslation();

  // render
  return (
    <WindowContentContainer full minWidth="500px" minHeight="300px">
      <WindowHeadline padding>
        <h1>
          {headline} {division.name || ""}
        </h1>
      </WindowHeadline>

      <WindowContent padding>
        <WindowPanel>
          <Table>
            <tbody>
              <tr>
                <TableLabel>{t("Name")}:</TableLabel>
                <TableValue>{division.name}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Sprache")}:</TableLabel>
                <TableValue>
                  {availableLanguages.find((l: any) => l.value === division.language_code)?.label ?? ""}
                </TableValue>
              </tr>
            </tbody>
          </Table>
        </WindowPanel>

        <WindowActions style={{position: "absolute", width: "90%", height: "40px", bottom: "40px"}}>
          {/* @todo commingsoon */ hasPermission("superadmin:root") && (
            <ToolbarButton type="button" onClick={showActivity}>
              <ToolbarButtonIcon>
                <span className="icon-ico_activity" />
              </ToolbarButtonIcon>
              {t("Aktivität")}
            </ToolbarButton>
          )}

          <ToolbarButton type="button" onClick={setEdit}>
            <ToolbarButtonIcon>
              <span className="icon-ico_edit" />
            </ToolbarButtonIcon>
            {t("Bearbeiten")}
          </ToolbarButton>
        </WindowActions>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default DivisionDetails;
