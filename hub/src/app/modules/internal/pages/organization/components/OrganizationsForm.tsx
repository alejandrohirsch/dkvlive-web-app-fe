import React from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {Message, Button} from "dkv-live-frontend-ui";
import {
  WindowActionButtons,
  WindowContentContainer,
  WindowHeadline,
  WindowContentProps,
  WindowContent,
} from "app/components/Window";
import {TextField} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {useDispatch, useSelector} from "react-redux";
import {Organization} from "../state/organizations/reducer";
import {organizationsItemStateSelector} from "../state/organizations/selectors";
import {loadOrganizations, updateOrganization} from "../state/organizations/actions";

// -----------------------------------------------------------------------------------------------------------

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  padding-top: 1em;
  display: grid;
  grid-gap: 24px;
  width: 99%;
`;

// -----------------------------------------------------------------------------------------------------------
interface OrganizationsFormProps extends WindowContentProps {
  organization: Organization;
}

const OrganizationsForm: React.FC<OrganizationsFormProps> = ({organization, headline, forceCloseWindow}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const itemState = useSelector(organizationsItemStateSelector);

  // form
  const form = useFormik<Organization>({
    initialValues: organization,
    onSubmit: (organizationData) => {
      form.setSubmitting(true);
      const submit = async () => {
        const callback = (success: boolean) => {
          form.setSubmitting(false);
          if (success) {
            dispatch(loadOrganizations());
            forceCloseWindow();
          }
        };
        dispatch(updateOrganization(organizationData.id, organizationData, callback));
        form.setSubmitting(false);
        if (!itemState.error) {
          dispatch(loadOrganizations());
          forceCloseWindow();
        }
      };
      submit();
    },
  });

  // render
  return (
    <WindowContentContainer full minWidth="500px" minHeight="300px">
      <WindowHeadline padding>
        <h1>
          {headline} {organization.name || ""}
        </h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {itemState.error && <Message text={t(itemState.errorText || "Unbekannter Fehler")} error />}

          <FormGrid>
            <TextField
              name="name"
              type="text"
              label={t("Name der Organisation")}
              value={form.values["name"]}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />
            <TextField
              name="customer_number"
              type="text"
              label={t("Kundennummer")}
              value={form.values.customer_number}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
            />
            <TextField
              name="notification_email"
              type="text"
              label={t("E-Mail")}
              value={form.values.notification_email}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
            />
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !(organization as Organization)}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default OrganizationsForm;
