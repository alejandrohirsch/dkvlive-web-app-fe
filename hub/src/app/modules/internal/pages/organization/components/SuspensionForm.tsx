import React from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {Message, Button} from "dkv-live-frontend-ui";
import {
  WindowActionButtons,
  WindowContentContainer,
  WindowHeadline,
  WindowContentProps,
  WindowContent,
} from "app/components/Window";
import {TextField} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {useDispatch, useSelector} from "react-redux";
import {Organization, Suspension} from "../state/organizations/reducer";
import {organizationsItemStateSelector} from "../state/organizations/selectors";
import {loadOrganizations, suspendOrganization} from "../state/organizations/actions";

// -----------------------------------------------------------------------------------------------------------

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  padding-top: 1em;
  display: grid;
  grid-gap: 24px;
  width: 99%;
`;

// -----------------------------------------------------------------------------------------------------------
interface SuspensionFormProps extends WindowContentProps {
  organization: Organization;
}

const SuspensionForm: React.FC<SuspensionFormProps> = ({organization, headline, forceCloseWindow}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const itemState = useSelector(organizationsItemStateSelector);

  // form
  const form = useFormik<any>({
    initialValues: {
      orgID: organization.id,
      cause: "",
    },
    onSubmit: (values) => {
      form.setSubmitting(true);
      const submit = async () => {
        const callback = (success: boolean) => {
          form.setSubmitting(false);
          if (success) {
            dispatch(loadOrganizations());
            forceCloseWindow();
          }
        };
        const suspension: Suspension = {state: true, cause: values.cause};
        dispatch(suspendOrganization(values.orgID, suspension, callback));
        form.setSubmitting(false);
        if (!itemState.error) {
          dispatch(loadOrganizations());
          forceCloseWindow();
        }
      };
      submit();
    },
  });

  // render
  return (
    <WindowContentContainer full minWidth="500px" minHeight="300px">
      <WindowHeadline padding>
        <h1>
          {headline} {organization.name || ""}
        </h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {itemState.error && <Message text={t(itemState.errorText || "Unbekannter Fehler")} error />}

          <FormGrid>
            <TextField
              name="cause"
              type="text"
              label={t("Grund")}
              value={form.values.cause}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !(organization as Organization)}>
              {t("Sperren")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default SuspensionForm;
