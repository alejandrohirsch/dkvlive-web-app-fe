import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {Icon} from "@material-ui/core";
import {PermissionField} from "./PermissionField";
import {Policy} from "../state/policies/reducer";
import {useDispatch, useSelector} from "react-redux";
import {permissionsListSelector} from "../state/permissions/selectors";
import {loadPermissionsIfNeeded} from "../state/permissions/actions";

// -----------------------------------------------------------------------------------------------------------

const PermissionGroup = styled.div`
  padding-bottom: 10px;
  font-variant: small-caps;
`;

const PermissionButton = styled.div`
  position: absolute;
  top: 0;
  width: 50%;
  height: 100%;
`;

const Header = styled.div`
  display: grid;
  grid-template-columns: 40% 1fr;
  justify-content: space-between;
  width: 100%;

  .MuiFormLabel-root {
    font-size: 0.5rm;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface PolicyDetailsWindowProps {
  policy: Policy;
  onToggle?: (target: string) => void;
  allowHovered?: boolean;
  setAllowHovered?: (allow: boolean) => void;
}

const PolicyPermissions: React.FC<PolicyDetailsWindowProps> = ({
  policy,
  onToggle,
  allowHovered,
  setAllowHovered,
}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // load permissions
  const permissionsList = useSelector(permissionsListSelector);
  useEffect(() => {
    dispatch(loadPermissionsIfNeeded());
  }, [dispatch]);

  // group permissions
  const [groupedPermissionsList, setGroupedPermissionsList] = useState<any>({});
  useEffect(() => {
    if (permissionsList.length) {
      const groupedList = {};
      const groups = [...new Set(permissionsList.map((p) => p.key.split(":")[0]))];
      groups.forEach((g) => (groupedList[g] = []));
      permissionsList.forEach((p) => groupedList[p.key.split(":")[0]].push(p));
      setGroupedPermissionsList(groupedList);
    }
  }, [permissionsList]);

  // render
  return (
    <>
      <Header>
        <h1>{t("Berechtigungen")}</h1>
      </Header>

      {Object.keys(groupedPermissionsList).map((key: string) => (
        <PermissionGroup key={key}>
          {key}
          {groupedPermissionsList[key].map((perm: any) => (
            <PermissionField
              key={perm.key}
              denied={policy.permissions["deny"].includes(perm.key)}
              allowed={policy.permissions["allow"].includes(perm.key)}
              onClick={onToggle ? (_: any) => onToggle(perm.key || "") : undefined}
              hoverColor={setAllowHovered ? (allowHovered ? "#58be58" : "#e44c4d") : undefined}
            >
              <PermissionButton onMouseEnter={setAllowHovered ? () => setAllowHovered(true) : undefined} />
              <PermissionButton
                style={{right: 0}}
                onMouseEnter={setAllowHovered ? () => setAllowHovered(false) : undefined}
              />

              <Icon className="icon-ico_unlock" />
              {t(perm.name)}
              <Icon className="icon-ico_lock" />
            </PermissionField>
          ))}
        </PermissionGroup>
      ))}
    </>
  );
};

export default PolicyPermissions;
