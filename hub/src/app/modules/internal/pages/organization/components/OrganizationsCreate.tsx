import React from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {Message, Button, AutocompleteInput} from "dkv-live-frontend-ui";
import {
  WindowActionButtons,
  WindowContentContainer,
  WindowHeadline,
  WindowContent,
} from "app/components/Window";
import {TextField} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {useDispatch, useSelector} from "react-redux";
import {CreateOrganization, emptyOrganization} from "../state/organizations/reducer";
import {organizationsItemStateSelector} from "../state/organizations/selectors";
import {loadOrganizations, createOrganization} from "../state/organizations/actions";
import {availableLanguages} from "../../i18n/I18n";

// -----------------------------------------------------------------------------------------------------------

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  padding-top: 1em;
  display: grid;
  grid-gap: 24px;
  width: 99%;
`;

// -----------------------------------------------------------------------------------------------------------
const OrganizationsCreate: React.FC<any> = ({headline, forceCloseWindow}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const itemState = useSelector(organizationsItemStateSelector);

  // form
  const form = useFormik<CreateOrganization>({
    initialValues: emptyOrganization,
    onSubmit: (organizationData) => {
      form.setSubmitting(true);
      const submit = async () => {
        const callback = (success: boolean) => {
          form.setSubmitting(false);
          if (success) {
            dispatch(loadOrganizations());
            forceCloseWindow();
          }
        };
        dispatch(createOrganization(organizationData, callback));
        form.setSubmitting(false);
      };
      submit();
    },
  });

  // render
  return (
    <WindowContentContainer full minWidth="500px" minHeight="300px">
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {itemState && itemState.error && (
            <Message text={t(itemState.errorText || "Unbekannter Fehler")} error />
          )}

          <FormGrid>
            <TextField
              name="organization_name"
              type="text"
              label={t("Name der Organisation")}
              value={form.values.organization_name}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />
            <TextField
              name="customer_number"
              type="text"
              label={t("Kundennummer")}
              value={form.values.customer_number}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
            />
            <TextField
              name="notification_email"
              type="text"
              label={t("E-Mail")}
              value={form.values.notification_email}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
            />
            <TextField
              name="division_name"
              type="text"
              label={t("Name der Division")}
              value={form.values.division_name}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />
            <AutocompleteInput
              name="language_code"
              value={
                form.values.language_code
                  ? availableLanguages.find((l: any) => l.value === form.values.language_code)
                  : null
              }
              onChange={(_: any, newValue: any) => {
                form.setFieldValue("language_code", newValue ? newValue.value : "");
              }}
              options={availableLanguages}
              autoHighlight
              getOptionLabel={(option: any) => t(option.label)}
              renderInput={(params: any) => (
                <TextField
                  {...params}
                  label={t("Sprache")}
                  variant="outlined"
                  required
                  inputProps={{
                    ...params.inputProps,
                  }}
                />
              )}
            />
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !(form.values as CreateOrganization)}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default OrganizationsCreate;
