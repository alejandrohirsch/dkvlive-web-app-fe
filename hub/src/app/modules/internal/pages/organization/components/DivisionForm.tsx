import React from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {Message, Button, AutocompleteInput} from "dkv-live-frontend-ui";
import {
  WindowActionButtons,
  WindowContentContainer,
  WindowHeadline,
  WindowContentProps,
  WindowContent,
} from "app/components/Window";
import {TextField} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {useDispatch, useSelector} from "react-redux";
import {Division} from "../state/divisions/reducer";
import {divisionsItemStateSelector} from "../state/divisions/selectors";
import {loadDivisions, updateDivision, createDivision} from "../state/divisions/actions";
import {availableLanguages} from "../../i18n/I18n";

// -----------------------------------------------------------------------------------------------------------

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  padding-top: 1em;
  display: grid;
  grid-gap: 24px;
  width: 99%;
`;

// -----------------------------------------------------------------------------------------------------------
interface DivisionFormProps extends WindowContentProps {
  division: Division;
  id: string;
}

const DivisionForm: React.FC<DivisionFormProps> = ({id, division, headline, forceCloseWindow}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const itemState = useSelector(divisionsItemStateSelector);

  // form
  const form = useFormik<Division>({
    initialValues: {...division, organization_id: id},
    onSubmit: (divisionData) => {
      form.setSubmitting(true);
      const submit = async () => {
        const callback = (success: boolean) => {
          form.setSubmitting(false);
          if (success) {
            dispatch(loadDivisions(id));
            forceCloseWindow();
          }
        };
        dispatch(
          divisionData.id
            ? updateDivision(divisionData.id, divisionData, callback)
            : createDivision(divisionData, callback)
        );
      };
      submit();
    },
  });

  // render
  return (
    <WindowContentContainer full minWidth="500px" minHeight="300px">
      <WindowHeadline padding>
        <h1>
          {headline} {division.name || ""}
        </h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {itemState.error && <Message text={t(itemState.errorText || "Unbekannter Fehler")} error />}
          <FormGrid>
            <TextField
              name="name"
              type="text"
              label={t("Name")}
              value={form.values.name}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />
            <AutocompleteInput
              name="language_code"
              onChange={(_: any, newValue: any) => {
                form.setFieldValue("language_code", newValue ? newValue.value : "");
              }}
              value={availableLanguages.find((l: any) => l.value === form.values.language_code) || null}
              options={availableLanguages}
              autoHighlight
              getOptionLabel={(option: any) => t(option.label)}
              renderInput={(params: any) => (
                <TextField
                  {...params}
                  label={t("Sprache")}
                  variant="outlined"
                  required
                  inputProps={{
                    ...params.inputProps,
                  }}
                />
              )}
            />
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !(division as Division)}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default DivisionForm;
