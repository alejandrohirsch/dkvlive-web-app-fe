import React, {useState, useEffect, useCallback} from "react";
import Page from "app/components/Page";
import {useTranslation} from "react-i18next";
import DataTable, {
  TablePageContent,
  useColumns,
  TableStickyHeader,
  SearchIcon,
  SearchTextField,
  TableStickyFooter,
  TableContainer,
  TableSelectionCountInfo,
  TableFooterActions,
  ToolbarButtonIcon,
  ToolbarButton,
  TableFooterRightActions,
} from "app/components/DataTable";
import {InputAdornment} from "@material-ui/core";
import {Button, Loading, callOnEnter} from "dkv-live-frontend-ui";
import store from "app/state/store";
import {DevicesModule} from "./state/module";
import {useDispatch, useSelector} from "react-redux";
import {devicesListSelector, devicesListStateSelector} from "./state/selectors";
import {loadDevicesIfNeeded, loadDevices} from "./state/actions";
import {Device} from "./state/reducer";
import Window from "app/components/Window";
import DeviceDetails from "./components/DeviceDetails";
import dayjs from "dayjs";
import ActivityLog from "app/modules/live/pages/list/components/ActivityLog";
import {resolveUserDateFormat} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
store.addModule(DevicesModule);

const activitesCodes = {
  "0": "Standby",
  "1": "Liveortung passiv",
  "2": "Liveortung aktiv",
  "3": "Firmware-Update",
  "4": "RDL-Authentifizierung",
  "5": "RDL-Download",
};

const resolveActivityName = (id: string) => {
  return activitesCodes[id] ?? "Unbekannt";
};

// -----------------------------------------------------------------------------------------------------------
const devicesColumns = [
  {
    field: "simiccid",
    header: "SIMICCID",
    width: 100,
  },
  {
    field: "imei",
    header: "IMEI",
    width: 100,
  },
  {
    field: "organization_name",
    header: "Organization",
    width: 100,
  },
  {
    field: "license_plate",
    header: "Kennzeichen",
    width: 100,
  },
  {
    field: "asset_name",
    header: "Asset",
    width: 100,
  },
  {
    field: "_activitie",
    header: "Aktive Aktivitäten",
    width: 150,
    bodyWithTranslation: (t: any) => (d: Device) => {
      const activities: string[] = [];
      Object.keys(d.live_status.activities ?? {}).map((id) =>
        activities.push(d.live_status.activities[id] || t(resolveActivityName(id.toString())))
      );
      return activities.join(", ");
    },
  },
  {
    field: "activities_modified_at",
    header: "Letzte Aktivität",
    width: 100,
    body: (d: Device) =>
      d.live_status.error_code !== 0 ? (
        <>
          Error: {d.live_status.error_code} (
          {dayjs(d.live_status.error_code_modified_at).format(resolveUserDateFormat(false))}){" "}
        </>
      ) : d.live_status.activities_modified_at !== "0001-01-01T00:00:00Z" ? (
        dayjs(d.live_status.activities_modified_at).format(resolveUserDateFormat(false))
      ) : (
        ""
      ),
  },
  {
    field: "_online",
    header: "Online",
    width: 100,
    bodyWithTranslation: (t: any) => (d: Device) => t(d.live_status.online ? "Ja" : "Nein"),
  },
  {
    field: "online_modified_at",
    header: "MQTT Login",
    width: 100,
    body: (d: Device) =>
      d.live_status.online_modified_at !== "0001-01-01T00:00:00Z"
        ? dayjs(d.live_status.online_modified_at).format(resolveUserDateFormat(false))
        : "",
  },
  {
    field: "last_checkin_at",
    header: "Letzter Check In",
    width: 100,
    body: (d: Device) =>
      d.last_checkin_at !== "0001-01-01T00:00:00Z"
        ? dayjs(d.last_checkin_at).format(resolveUserDateFormat(false))
        : "",
  },
];

// -----------------------------------------------------------------------------------------------------------
const Devices: React.FC = () => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // load devices
  const [search, setSearch] = useState("");

  const devicesListState = useSelector(devicesListStateSelector);
  const devicesList = useSelector(devicesListSelector);

  useEffect(() => {
    dispatch(loadDevicesIfNeeded(search));

    // eslint-disable-next-line
  }, [dispatch]);

  const refresh = useCallback(() => dispatch(loadDevices(search)), [dispatch, search]);

  // actions
  // - activities
  const [showActivityWindow, setShowActivityWindow] = useState<any>(null);
  const onCloseActivityWindow = useCallback(() => setShowActivityWindow(null), [setShowActivityWindow]);
  const openActivityWindow = () => setShowActivityWindow(selected && selected.length ? selected[0] : null);

  // - detail
  const [activeDevice, setActiveDevice] = useState<Device | null>(null);
  const onCloseDeviceDetail = useCallback(() => setActiveDevice(null), [setActiveDevice]);
  const openDeviceDetail = useCallback((e: any) => setActiveDevice(e.data as Device), [setActiveDevice]);

  useEffect(() => {
    if (!activeDevice) {
      return;
    }

    const device = devicesList.find((d) => d.id === activeDevice.id);

    if (device) {
      setActiveDevice(device);
    }
  }, [devicesList, activeDevice]);

  // columns
  const [columns] = useState(devicesColumns);
  const visibleColumns = useColumns(columns, t);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const deselect = () => setSelected([]);

  const openDetailsWindowForSelected = useCallback(() => selected.length && setActiveDevice(selected[0]), [
    selected,
    setActiveDevice,
  ]);

  // render
  return (
    <Page id="internaldevices">
      <Loading inprogress={devicesListState.list.loading} />

      <TablePageContent>
        <TableStickyHeader>
          <h1>{t("Geräteverwaltung")}</h1>

          <SearchTextField
            name="search"
            type="text"
            required
            label={t("Simiccid Suche")}
            value={search}
            onChange={(e: any) => setSearch(e.target.value)}
            onKeyDown={callOnEnter(refresh)}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          />

          <ToolbarButton general onClick={refresh} style={{marginLeft: "auto"}}>
            <ToolbarButtonIcon>
              <span className="icon-ico_load" />
            </ToolbarButtonIcon>
            {t("Aktualisieren")}
          </ToolbarButton>
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={devicesList}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            onRowDoubleClick={openDeviceDetail}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Eintrag ausgewählt" : "Einträge ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={deselect}>
                {t("Abbrechen")}
              </Button>
              <TableFooterActions style={{display: "flex"}}>
                <ToolbarButton onClick={openActivityWindow}>
                  <ToolbarButtonIcon>
                    <span className="icon-ico_activity" />
                  </ToolbarButtonIcon>
                  {t("Asset Aktivität")}
                </ToolbarButton>
                <ToolbarButton onClick={openDetailsWindowForSelected}>
                  <ToolbarButtonIcon>
                    <span className="icon-ico_billings" />
                  </ToolbarButtonIcon>
                  {t("Details")}
                </ToolbarButton>
              </TableFooterActions>
              <TableFooterRightActions />
            </>
          )}
        </TableStickyFooter>
      </TablePageContent>

      {showActivityWindow && (
        <Window
          onClose={onCloseActivityWindow}
          headline={`${t("Asset Aktivität")} - ${showActivityWindow.license_plate}`}
        >
          {(props) => <ActivityLog {...props} assetID={showActivityWindow.asset_id} />}
        </Window>
      )}

      {activeDevice && (
        <Window onClose={onCloseDeviceDetail} headline={t("Gerät")}>
          {(props) => <DeviceDetails {...props} device={activeDevice} />}
        </Window>
      )}
    </Page>
  );
};

export default Devices;
