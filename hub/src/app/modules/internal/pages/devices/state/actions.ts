import httpClient from "services/http";
import {
  LoadingDevicesAction,
  LOADING_DEVICES,
  LoadingDevicesFailedAction,
  LOADING_DEVICES_FAILED,
  DevicesLoadedAction,
  DEVICES_LOADED,
  LoadingDeviceAction,
  LOADING_DEVICE,
  DeviceLoadedAction,
  DEVICE_LOADED,
  LoadingDeviceFailedAction,
  LOADING_DEVICE_FAILED,
  // CreateDevicesAction,
  // CREATE_DEVICES,
  // CreateDeviceAction,
  // CREATE_DEVICE,
  // UpdateDeviceLogisticsStateAction,
  // UPDATE_DEVICE_LOGISTICS_STATE,
} from "./types";
import {Device} from "./reducer";
import {DevicesThunkResult} from "./module";

// -----------------------------------------------------------------------------------------------------------
function loadingDevicesAction(): LoadingDevicesAction {
  return {
    type: LOADING_DEVICES,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingDevicesFailedAction(errorText?: string): LoadingDevicesFailedAction {
  return {
    type: LOADING_DEVICES_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function devicesLoadedAction(items: Device[]): DevicesLoadedAction {
  return {
    type: DEVICES_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
// function createDevicesAction(items: Device[]): CreateDevicesAction {
//   return {
//     type: CREATE_DEVICES,
//     payload: items,
//   };
// }

// -----------------------------------------------------------------------------------------------------------
function loadingDeviceAction(): LoadingDeviceAction {
  return {
    type: LOADING_DEVICE,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingDeviceFailedAction(errorText?: string): LoadingDeviceFailedAction {
  return {
    type: LOADING_DEVICE_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function deviceLoadedAction(item: Device): DeviceLoadedAction {
  return {
    type: DEVICE_LOADED,
    payload: item,
  };
}

// -----------------------------------------------------------------------------------------------------------
// function createDeviceAction(item: Device): CreateDeviceAction {
//   return {
//     type: CREATE_DEVICE,
//     payload: item,
//   };
// }

// -----------------------------------------------------------------------------------------------------------
// function updateDeviceLogisticsStateAction(imei: string, state: number): UpdateDeviceLogisticsStateAction {
//   return {
//     type: UPDATE_DEVICE_LOGISTICS_STATE,
//     payload: {
//       imei,
//       state,
//     },
//   };
// }

// -----------------------------------------------------------------------------------------------------------
export function loadDevices(search: string): DevicesThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingDevicesAction());

    const [{data}, err] = await httpClient.get(
      `/management/devices/findAll?limit=50&search=${encodeURIComponent(search)}`
    );

    if (err !== null) {
      // @todo: log
      dispatch(loadingDevicesFailedAction("Laden fehlgeschlagen"));
      return;
    }

    // replace with pagination once possible
    dispatch(devicesLoadedAction(data));
  };
}

export function loadDevicesIfNeeded(search: string): DevicesThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().internalDevices;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadDevices(search));
  };
}

export function updateDeviceLogisticsState(imei: string, state: number): DevicesThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingDeviceAction());
    const [{response}, err] = await httpClient.post("/management/devices/setLogisticsState", {imei, state});

    if (err !== null) {
      // @todo: log
      dispatch(loadingDeviceFailedAction("Speichern fehlgeschlagen"));
      return;
    }

    dispatch(deviceLoadedAction(response));
  };
}

export function createDevice(device: Device): DevicesThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingDeviceAction());
    const [{response}, err] = await httpClient.post("/management/devices/create", device);

    if (err !== null) {
      // @todo: log
      dispatch(loadingDeviceFailedAction("Erstellen fehlgeschlagen"));
      return;
    }

    dispatch(deviceLoadedAction(response));
  };
}

export function createDevices(devices: Device[]): DevicesThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingDevicesAction());
    const [{response}, err] = await httpClient.post("/management/devices/createBulk", devices);

    if (err !== null) {
      // @todo: log
      dispatch(loadingDevicesFailedAction("Erstellen fehlgeschlagen"));
      return;
    }

    dispatch(devicesLoadedAction(response));
  };
}
