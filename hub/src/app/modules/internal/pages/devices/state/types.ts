import {Device} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_DEVICES = "map/loadingDevices";

export interface LoadingDevicesAction {
  type: typeof LOADING_DEVICES;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_DEVICES_FAILED = "map/loadingDevicesFailed";

export interface LoadingDevicesFailedAction {
  type: typeof LOADING_DEVICES_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const DEVICES_LOADED = "map/devicesLoaded";

export interface DevicesLoadedAction {
  type: typeof DEVICES_LOADED;
  payload: Device[];
}

// -----------------------------------------------------------------------------------------------------------
export const CREATE_DEVICES = "map/createDevices";

export interface CreateDevicesAction {
  type: typeof CREATE_DEVICES;
  payload: Device[];
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_DEVICE = "map/loadingDevice";

export interface LoadingDeviceAction {
  type: typeof LOADING_DEVICE;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_DEVICE_FAILED = "map/loadingDeviceFailed";

export interface LoadingDeviceFailedAction {
  type: typeof LOADING_DEVICE_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const DEVICE_LOADED = "map/deviceLoaded";

export interface DeviceLoadedAction {
  type: typeof DEVICE_LOADED;
  payload: Device;
}

// -----------------------------------------------------------------------------------------------------------
// export const CREATE_DEVICE = "map/createDevice";

// export interface CreateDeviceAction {
//   type: typeof CREATE_DEVICE;
//   payload: Device;
// }

// -----------------------------------------------------------------------------------------------------------
// export const UPDATE_DEVICE_LOGISTICS_STATE = "map/updateDeviceLogisticsState";

// export interface UpdateDeviceLogisticsStateAction {
//   type: typeof UPDATE_DEVICE_LOGISTICS_STATE;
//   payload: {
//     imei: string;
//     state: number;
//   };
// }

// -----------------------------------------------------------------------------------------------------------
export type DevicesActions =
  | LoadingDevicesAction
  | LoadingDevicesFailedAction
  | DevicesLoadedAction
  | CreateDevicesAction
  | LoadingDeviceAction
  | LoadingDeviceFailedAction
  | DeviceLoadedAction;
// | CreateDeviceAction
// | UpdateDeviceLogisticsStateAction;
