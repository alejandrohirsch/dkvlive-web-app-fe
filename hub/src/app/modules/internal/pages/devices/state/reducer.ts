import {createReducer} from "@reduxjs/toolkit";
import {
  LOADING_DEVICES,
  LoadingDevicesFailedAction,
  LOADING_DEVICES_FAILED,
  DevicesLoadedAction,
  DEVICES_LOADED,
  LoadingDeviceFailedAction,
  DEVICE_LOADED,
  LOADING_DEVICE_FAILED,
  LOADING_DEVICE,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface Device {
  id: string;
  imei: string;
  imsi: string;
  logistics_state: LogisticsState;
  data_category_support: DataCategorySupport;
  created_at: string;
  updated_at: string;
  auto_assign_asset: boolean;
  auto_create_and_assign_asset: boolean;
  activation: Activation;
  configuration: Configuration;
  license_plate: string;
  msisdn: string;
  simiccid: string;
  vin: string;
  organization_id: string;
  organization_name: string;
  division_id: string;
  asset_id: string;
  asset_name: string;
  live_status: LiveStatus;
  rdl: Rdl;
  hardware_id: string;
  hardware_name: string;
  hardware_key: string;
  last_checkin_at: string;
  lpwa_firmware_version: string;
  mcu_firmware_version: string;
  modified_at: string;
}

export interface LogisticsState {
  state: number;
  location: string;
  updated_at: string;
  logistics_reference: string;

  State: number;
  Location: string;
  UpdatedAt: string;
  LogisticsReference: string;
}

export interface DataCategorySupport {
  gnss: boolean;
  fms: boolean;
  ebs: boolean;
  info_dtco: boolean;
  thermo_es: boolean;
  thermo_tk: boolean;
  rdl: boolean;
}

export interface Activation {
  created_at: string;
  username: string;
}

export interface Configuration {
  created_at: string;
  primary_broker_domain: string;
  primary_broker_port: number;
  secondary_broker_domain: string;
  secondary_broker_port: number;
}

export interface LiveStatus {
  online: boolean;
  online_modified_at: string;
  activities: any;
  activities_modified_at: string;
  error_code: number;
  error_code_modified_at: string;
}

export interface Rdl {
  enabled: boolean;
  last_auth_at: string;
  last_auth_try_at: string;
  last_download_at: string;
}

export interface DevicesState {
  list: {
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  item: {
    loading: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: string]: Device};
}

const initialState: DevicesState = {
  list: {
    loading: true,
  },
  item: {
    loading: true,
  },
};

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingDevices(state: DevicesState) {
  state.list.loading = true;
  state.list.loaded = false;

  if (state.list.error) {
    delete state.list.error;
    delete state.list.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingDevicesFailed(state: DevicesState, action: LoadingDevicesFailedAction) {
  state.list.loading = false;
  state.list.error = true;
  state.list.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleDevicesLoaded(state: DevicesState, action: DevicesLoadedAction) {
  state.list.loading = false;
  state.list.loaded = true;

  const items = {};
  action.payload.forEach((a) => (items[a.id] = a));

  state.items = items;

  if (state.list.error) {
    delete state.list.error;
    delete state.list.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingDevice(state: DevicesState) {
  state.item.loading = true;

  if (state.item.error) {
    delete state.item.error;
    delete state.item.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingDeviceFailed(state: DevicesState, action: LoadingDeviceFailedAction) {
  state.item.loading = false;
  state.item.error = true;
  state.item.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleDeviceLoaded(state: DevicesState) {
  state.item.loading = false;

  if (state.item.error) {
    delete state.item.error;
    delete state.item.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
// export function handleCreateDevice(state: DevicesState) {
//   state.item.loading = true;
//   if (state.item.error) {
//     delete state.item.error;
//     delete state.item.errorText;
//   }
// }

// -----------------------------------------------------------------------------------------------------------
// export function handleCreateDevices(state: DevicesState, action: CreateDevicesAction) {}

// -----------------------------------------------------------------------------------------------------------
// export function handleUpdateDeviceLogisticsState(state: DevicesState, action: SetDeviceLogisticStateAction) {}

// -----------------------------------------------------------------------------------------------------------
export const devicesReducer = createReducer(initialState, {
  [LOADING_DEVICES]: handleLoadingDevices,
  [LOADING_DEVICES_FAILED]: handleLoadingDevicesFailed,
  [DEVICES_LOADED]: handleDevicesLoaded,
  [LOADING_DEVICE]: handleLoadingDevice,
  [LOADING_DEVICE_FAILED]: handleLoadingDeviceFailed,
  [DEVICE_LOADED]: handleDeviceLoaded,
});
