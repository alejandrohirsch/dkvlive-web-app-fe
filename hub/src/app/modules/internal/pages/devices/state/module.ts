import {IModule} from "redux-dynamic-modules";
import {ThunkAction} from "redux-thunk";
import {DevicesState, devicesReducer} from "./reducer";
import {DevicesActions} from "./types";

export interface DevicesModuleState {
  internalDevices: DevicesState;
}

export type DevicesThunkResult<R> = ThunkAction<R, DevicesModuleState, void, DevicesActions>;

export const DevicesModule: IModule<DevicesModuleState> = {
  id: "internal_devices",
  reducerMap: {
    internalDevices: devicesReducer,
  },
};
