import {createSelector} from "@reduxjs/toolkit";
import {DevicesModuleState} from "./module";

export const devicesItemsSelector = (state: DevicesModuleState) => state.internalDevices.items;

export const devicesListStateSelector = (state: DevicesModuleState) => state.internalDevices;

// list
export const devicesListSelector = createSelector(devicesItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// device
export const deviceSelector = (id: string) =>
  createSelector(devicesItemsSelector, (items) => (items ? items[id] : undefined));
