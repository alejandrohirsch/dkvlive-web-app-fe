import React, {useState, useEffect, useRef} from "react";
import {Device} from "../state/reducer";
import Window, {
  WindowContentContainer,
  WindowHeadline,
  WindowContentProps,
  WindowContent,
  WindowTabs,
  WindowTabsContainer,
  WindowActions,
} from "app/components/Window";
import {Message, Button, Select} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import {Tab, Menu, MenuItem, Divider} from "@material-ui/core";
import styled, {css} from "styled-components";
import ActivityLog from "app/modules/live/pages/list/components/ActivityLog";
import {ToolbarButton, ToolbarButtonIcon} from "app/components/DataTable";
import httpClient from "services/http";
import {Growl} from "primereact/growl";
import {useFormik} from "formik";
import dayjs from "dayjs";
import DayjsUtils from "@date-io/dayjs";
import {KeyboardDateTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import {TextField} from "dkv-live-frontend-ui";
import {
  resolveUserDateFormat,
  resolveUserDateLocale,
  hasPermission,
} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const activitesCodes = {
  "0": "Standby",
  "1": "Liveortung passiv",
  "2": "Liveortung aktiv",
  "3": "Firmware-Update",
  "4": "RDL-Authentifizierung",
  "5": "RDL-Download",
};

const logisticStates = {
  0: "Neu",
  1: "Auf Lager",
  2: "In Transport",
  3: "Beim Kunden",
  4: "Rückgabe",
};

const resolveActivityName = (id: string) => {
  return activitesCodes[id] ?? "Unbekannt";
};

const cmdRDLAuth = async (deviceID: string) => {
  const [{data}, err] = await httpClient.post(`/ingestion/rdl/auth/trigger/${deviceID}`);
  return [data, err];
};

const cmdRDLDriver = async (deviceID: string, number: string) => {
  const [{data}, err] = await httpClient.post(`/ingestion/rdl/download/driver/${deviceID}/${number}`);
  return [data, err];
};

const cmdRDLMassStorage = async (deviceID: string, from?: Date, to?: Date) => {
  const params = [];
  if (from) {
    const fromEncoded = encodeURIComponent(from.toISOString());
    params.push(`from=${fromEncoded}`);
  }

  if (to) {
    const toEncoded = encodeURIComponent(to.toISOString());
    params.push(`to=${toEncoded}`);
  }

  const [{data}, err] = await httpClient.post(`/ingestion/rdl/download/mass/${deviceID}?${params.join("&")}`);
  return [data, err];
};

const cmdFirmware = async (deviceID: string, firmwareID: string) => {
  const [{data}, err] = await httpClient.post(
    `/devman/firmware/triggerUpdate`,
    JSON.stringify({
      device_id: deviceID,
      firmware_id: firmwareID,
    }),
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  return [data, err];
};

// -----------------------------------------------------------------------------------------------------------
const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

const TableRow = styled.div`
  display: flex;
  justify-content: space-between;
`;

const TableHeadline = styled.div`
  position: relative;
  color: var(--dkv-grey_70);
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 5px;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 12px;
`;

// -----------------------------------------------------------------------------------------------------------
interface DeviceDetailsProps extends WindowContentProps {
  device: Device;
}

const DeviceDetails: React.FC<DeviceDetailsProps> = ({device: listDevice, setLoading}) => {
  const {t} = useTranslation();

  // device
  const [device, setDevice] = useState<Device>();
  const [error, setError] = useState("");

  const load = async () => {
    setError("");
    setLoading(true);

    const [{data}, err] = await httpClient.get(`/management/devices/${listDevice.id}`);
    if (err !== null) {
      setLoading(false);
      setError(
        err.response?.data?.message ? err.response?.data?.message : "Geräte konnte nicht geladen werden"
      );
      return;
    }

    setDevice(data);
    setLoading(false);
  };

  useEffect(() => {
    load();
    // eslint-disable-next-line
  }, []);

  // tabs
  const [tab, setTab] = useState(0);
  const handleTabChange = (_: React.ChangeEvent<unknown>, newTabValue: number) => {
    setTab(newTabValue);
  };

  // cmds
  const growlRef = useRef<any>();

  const [commandsAnchorEl, setCommandsAnchorEl] = React.useState(null);
  const openCommands = (event: any) => {
    setCommandsAnchorEl(event.currentTarget);
  };

  const closeCommands = () => {
    setCommandsAnchorEl(null);
  };

  const doCommand = (cmd: string, label: string, params?: any) => {
    const runCommand = async () => {
      if (!device) {
        return;
      }

      setLoading(true);
      setCommandsAnchorEl(null);

      setTimeout(() => {
        if (document.activeElement && (document.activeElement as any).blur) {
          (document.activeElement as any).blur();
        }
      });

      let err: any = "Befehl nicht gefunden";
      switch (cmd) {
        case "rdlauth":
          [, err] = await cmdRDLAuth(device.id);
          break;
        case "rdlmass":
          [, err] = await cmdRDLMassStorage(device.id);
          break;
        case "rdlmassrange":
          [, err] = await cmdRDLMassStorage(device.id, params.from, params.to);
          break;
        case "rdlcard1":
          [, err] = await cmdRDLDriver(device.id, "1");
          break;
        case "rdlcard2":
          [, err] = await cmdRDLDriver(device.id, "2");
          break;
        case "firmware":
          [, err] = await cmdFirmware(device.id, params.id);
          break;
      }

      if (err !== null) {
        growlRef.current.show({
          life: 5000,
          closable: false,
          severity: "error",
          summary: t(label),
          detail: err.response?.data?.message
            ? err.response?.data?.message
            : typeof err === "string"
            ? err
            : t("Befehl konnte nicht ausgeführt werden"),
        });

        setLoading(false);
        return;
      }

      setTimeout(() => {
        growlRef.current.show({
          life: 5000,
          closable: false,
          severity: "success",
          summary: t(label),
          detail: t("Befehl erfolgreich ausgelöst"),
        });

        setLoading(false);
        window.dispatchEvent(new Event("dkv-assetlog-reload"));
      }, 1000);
    };

    runCommand();
  };

  const [showMassStorageRange, setShowMassStorageRange] = useState<any>(false);
  const openMassStorageRange = () => {
    setCommandsAnchorEl(null);
    setShowMassStorageRange(true);
  };

  const onCloseMassStorageRange = () => {
    setShowMassStorageRange(false);
  };

  const [showFirmwareUpdate, setShowFirmwareUpdate] = useState<any>(-1);

  // render
  const activities: string[] = [];
  let dataCategories: string[] = [];
  if (device) {
    Object.keys(device.live_status.activities ?? {}).map((id) =>
      activities.push(device.live_status.activities[id] || t(resolveActivityName(id.toString())))
    );

    dataCategories = Object.keys(device.data_category_support)
      .filter((k) => device.data_category_support[k])
      .map((k) => k.replace("_", " ").toUpperCase());
  }

  const dateFormat = resolveUserDateFormat(false);

  return (
    <WindowContentContainer minWidth="850px" minHeight="850px" full>
      <WindowHeadline padding>
        <h1>
          {t("Gerät")} - {device ? `${device.simiccid};${device.imei}` : ""}
        </h1>
      </WindowHeadline>
      <WindowTabsContainer>
        <WindowTabs value={tab} onChange={handleTabChange} variant="scrollable">
          <Tab label={t("Übersicht")} />
          {hasPermission("management:ListAssetsLog") && <Tab label={t("Asset Aktivitäten")} />}
        </WindowTabs>
      </WindowTabsContainer>
      <WindowContent>
        {error && <Message style={{margin: 24}} text={t(error)} error />}

        {tab === 0 && device && (
          <>
            <TableRow>
              <Table style={{margin: 24}}>
                <tbody>
                  <tr>
                    <td colSpan={2}>
                      <TableHeadline>{t("Base")}</TableHeadline>
                    </td>
                  </tr>
                  <tr>
                    <TableLabel>{t("Device ID")}:</TableLabel>
                    <TableValue>{device.id}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Hardware")}:</TableLabel>
                    <TableValue>{device.hardware_name}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Erstellt am")}:</TableLabel>
                    <TableValue>
                      {device.created_at !== "0001-01-01T00:00:00Z"
                        ? dayjs(device.created_at).format(dateFormat)
                        : ""}
                    </TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Geändert am")}:</TableLabel>
                    <TableValue>
                      {device.modified_at !== "0001-01-01T00:00:00Z"
                        ? dayjs(device.modified_at).format(dateFormat)
                        : ""}
                    </TableValue>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <div style={{marginTop: 4}} />
                    </td>
                  </tr>
                  <tr>
                    <TableLabel>{t("IMSI")}:</TableLabel>
                    <TableValue>{device.imsi}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("IMEI")}:</TableLabel>
                    <TableValue>{device.imei}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Simiccid")}:</TableLabel>
                    <TableValue>{device.simiccid}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("MSISDN")}:</TableLabel>
                    <TableValue>{device.msisdn}</TableValue>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <div style={{marginTop: 4}} />
                    </td>
                  </tr>
                  <tr>
                    <TableLabel>{t("Organization")}:</TableLabel>
                    <TableValue>{device.organization_name}</TableValue>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <div style={{marginTop: 8}} />
                    </td>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <TableHeadline>{t("Status")}</TableHeadline>
                    </td>
                  </tr>
                  <tr>
                    <TableLabel>{t("Online")}:</TableLabel>
                    <TableValue>{t(device.live_status.online ? "Ja" : "Nein")}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Aktive Aktivitäten")}:</TableLabel>
                    <TableValue>{activities.join(", ")}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Letzte Aktivität")}:</TableLabel>
                    <TableValue>
                      {device.live_status.activities_modified_at !== "0001-01-01T00:00:00Z"
                        ? dayjs(device.live_status.activities_modified_at).format(dateFormat)
                        : ""}
                    </TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Letzter Check In")}:</TableLabel>
                    <TableValue>
                      {device.last_checkin_at !== "0001-01-01T00:00:00Z"
                        ? dayjs(device.last_checkin_at).format(dateFormat)
                        : ""}
                    </TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("MQTT Login")}:</TableLabel>
                    <TableValue>
                      {device.live_status.online_modified_at !== "0001-01-01T00:00:00Z"
                        ? dayjs(device.live_status.online_modified_at).format(dateFormat)
                        : ""}
                    </TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Error")}:</TableLabel>
                    <TableValue>
                      {t(device.live_status.error_code ? "Ja" : "Nein")} (
                      {device.live_status.error_code_modified_at !== "0001-01-01T00:00:00Z"
                        ? dayjs(device.live_status.error_code_modified_at).format(dateFormat)
                        : ""}
                      )
                    </TableValue>
                  </tr>
                </tbody>
              </Table>

              <Table style={{margin: 24}}>
                <tbody>
                  <tr>
                    <td colSpan={2}>
                      <TableHeadline>{t("Asset")}</TableHeadline>
                    </td>
                  </tr>
                  <tr>
                    <TableLabel>{t("Asset ID")}:</TableLabel>
                    <TableValue>{device.asset_id}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Asset Name")}:</TableLabel>
                    <TableValue>{device.asset_name}</TableValue>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <div style={{marginTop: 4}} />
                    </td>
                  </tr>
                  <tr>
                    <TableLabel>{t("Kennzeichen")}:</TableLabel>
                    <TableValue>{device.license_plate}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("FIN")}:</TableLabel>
                    <TableValue>{device.vin}</TableValue>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <div style={{marginTop: 8}} />
                    </td>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <TableHeadline>{t("Logistik State")}</TableHeadline>
                    </td>
                  </tr>
                  <tr>
                    <TableLabel>{t("Ort")}:</TableLabel>
                    <TableValue>{device.logistics_state.Location}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Referenz")}:</TableLabel>
                    <TableValue>{device.logistics_state.LogisticsReference}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Status")}:</TableLabel>
                    <TableValue>{logisticStates[device.logistics_state.State]}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Geändert am")}:</TableLabel>
                    <TableValue>
                      {device.logistics_state.UpdatedAt !== "0001-01-01T00:00:00Z"
                        ? dayjs(device.logistics_state.UpdatedAt).format(dateFormat)
                        : ""}
                    </TableValue>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <div style={{marginTop: 8}} />
                    </td>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <TableHeadline>{t("Versionen")}</TableHeadline>
                    </td>
                  </tr>
                  <tr>
                    <TableLabel>{t("MCU")}:</TableLabel>
                    <TableValue>{device.mcu_firmware_version}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("LPWA")}:</TableLabel>
                    <TableValue>{device.lpwa_firmware_version}</TableValue>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <div style={{marginTop: 8}} />
                    </td>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <TableHeadline>{t("Daten")}</TableHeadline>
                    </td>
                  </tr>
                  <tr>
                    <TableLabel>{t("Funktionen")}:</TableLabel>
                    <TableValue>{dataCategories.join(", ")}</TableValue>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <div style={{marginTop: 8}} />
                    </td>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <TableHeadline>{t("Konfiguration")}</TableHeadline>
                    </td>
                  </tr>
                  <tr>
                    <td colSpan={2}>
                      <div style={{marginTop: 4}} />
                    </td>
                  </tr>
                  <tr>
                    <TableLabel>{t("Primärer Broker")}:</TableLabel>
                    <TableValue>
                      {device.configuration && (
                        <>
                          {device.configuration.primary_broker_domain}:
                          {device.configuration.primary_broker_port}
                        </>
                      )}
                    </TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Sekundärer Broker")}:</TableLabel>
                    <TableValue>
                      {device.configuration && (
                        <>
                          {device.configuration.secondary_broker_domain}:
                          {device.configuration.secondary_broker_port}
                        </>
                      )}
                    </TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Erstellt am")}:</TableLabel>
                    <TableValue>
                      {device.configuration && (
                        <>
                          {device.configuration.created_at !== "0001-01-01T00:00:00Z"
                            ? dayjs(device.configuration.created_at).format(dateFormat)
                            : ""}
                        </>
                      )}
                    </TableValue>
                  </tr>
                </tbody>
              </Table>
            </TableRow>
          </>
        )}

        {tab === 1 && device && (
          <ActivityLog assetID={device.asset_id} setLoading={setLoading} maxListHeight="500px" />
        )}
      </WindowContent>

      <WindowActions autoTop>
        <ToolbarButton type="button" onClick={openCommands}>
          <ToolbarButtonIcon>
            <span className="icon-ico_tools" />
          </ToolbarButtonIcon>
          {t("Befehle")}
        </ToolbarButton>

        {tab !== 1 && (
          <ToolbarButton type="button" onClick={load}>
            <ToolbarButtonIcon>
              <span className="icon-ico_load" />
            </ToolbarButtonIcon>
            {t("Aktualisieren")}
          </ToolbarButton>
        )}
      </WindowActions>

      <Menu
        id="details_commands"
        anchorEl={commandsAnchorEl}
        open={Boolean(commandsAnchorEl)}
        onClose={closeCommands}
      >
        {device && device.data_category_support.rdl && hasPermission("rdl:TriggerAuth") && (
          <MenuItem onClick={() => doCommand("rdlauth", "RDL Auth")}>{t("RDL Auth")}</MenuItem>
        )}

        {device && device.data_category_support.rdl && hasPermission("rdl:Download") && (
          <MenuItem onClick={() => doCommand("rdlmass", "RDL Massenspeicher")}>
            {t("RDL Massenspeicher")}
          </MenuItem>
        )}

        {device && device.data_category_support.rdl && hasPermission("rdl:Download") && (
          <MenuItem onClick={openMassStorageRange}>{t("RDL Massenspeicher Zeitraum")}</MenuItem>
        )}

        {device && device.data_category_support.rdl && hasPermission("rdl:Download") && (
          <MenuItem onClick={() => doCommand("rdlcard1", "RDL Fahrer 1")}>{t("RDL Fahrer 1")}</MenuItem>
        )}

        {device && device.data_category_support.rdl && hasPermission("rdl:Download") && (
          <MenuItem onClick={() => doCommand("rdlcard2", "RDL Fahrer 2")}>{t("RDL Fahrer 2")}</MenuItem>
        )}

        {device && device.data_category_support.rdl && <Divider />}

        {hasPermission("superadmin:TriggerFirmwareUpdate") && (
          <MenuItem
            onClick={() => {
              setCommandsAnchorEl(null);
              setShowFirmwareUpdate(0);
            }}
          >
            {t("MCU Firmware Update")}
          </MenuItem>
        )}

        {hasPermission("superadmin:TriggerFirmwareUpdate") && (
          <MenuItem
            onClick={() => {
              setCommandsAnchorEl(null);
              setShowFirmwareUpdate(1);
            }}
          >
            {t("LPWA Firmware Update")}
          </MenuItem>
        )}
      </Menu>

      <Growl ref={(el) => (growlRef.current = el)} baseZIndex={20000} />

      {showMassStorageRange && (
        <Window onClose={onCloseMassStorageRange} headline={t("Massenspeicher Zeitraum")}>
          {(props) => (
            <MassStorageRangePicker
              {...props}
              onSubmit={(data) => {
                setShowMassStorageRange(false);
                doCommand("rdlmassrange", "RDL Massenspeicher Zeitraum", data);
              }}
            />
          )}
        </Window>
      )}

      {showFirmwareUpdate >= 0 && (
        <Window onClose={() => setShowFirmwareUpdate(-1)} headline={t("Firmware Update")}>
          {(props) => (
            <FirmwarePicker
              {...props}
              target={showFirmwareUpdate}
              hardwareKey={device ? device.hardware_key : "n/a"}
              growl={growlRef.current}
              onSubmit={(data) => {
                setShowFirmwareUpdate(-1);
                doCommand(
                  "firmware",
                  t(`${showFirmwareUpdate === 0 ? "MCU" : "LPWA"} Firmware Update`),
                  data
                );
              }}
            />
          )}
        </Window>
      )}
    </WindowContentContainer>
  );
};

export default DeviceDetails;

// -----------------------------------------------------------------------------------------------------------
interface MassStorageRangePickerProps extends WindowContentProps {
  onSubmit: (data: any) => void;
}

const MassStorageRangePicker: React.FC<MassStorageRangePickerProps> = ({headline, onSubmit}) => {
  const {t} = useTranslation();

  const form = useFormik<any>({
    initialValues: {
      from: null,
      to: null,
    },
    onSubmit: (values) => {
      const from = values.from.toDate ? values.from.toDate() : values.from;
      const to = values.to.toDate ? values.to.toDate() : values.to;

      onSubmit({from, to});
    },
  });

  // render
  return (
    <WindowContentContainer full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <form onSubmit={form.handleSubmit}>
          <FormGrid>
            <MuiPickersUtilsProvider utils={DayjsUtils} locale={resolveUserDateLocale()}>
              <KeyboardDateTimePicker
                disableToolbar
                name="from"
                label={t("Von")}
                variant="inline"
                format={resolveUserDateFormat()}
                margin="none"
                value={form.values.from}
                onChange={(d: any) => form.setFieldValue("from", d)}
                TextFieldComponent={TextField as any}
                invalidDateMessage={t("Ungültiges Datum")}
                keyboardIcon={<i className="icon-ico_cal-week" />}
                PopoverProps={{
                  className: "dkv-datetimepicker",
                }}
                inputVariant="outlined"
                required
              />
              <KeyboardDateTimePicker
                disableToolbar
                name="to"
                label={t("Bis")}
                variant="inline"
                format={resolveUserDateFormat()}
                margin="none"
                value={form.values.to}
                onChange={(d: any) => form.setFieldValue("to", d)}
                TextFieldComponent={TextField as any}
                invalidDateMessage={t("Ungültiges Datum")}
                keyboardIcon={<i className="icon-ico_cal-week" />}
                PopoverProps={{
                  className: "dkv-datetimepicker",
                }}
                inputVariant="outlined"
                required
              />
            </MuiPickersUtilsProvider>
          </FormGrid>

          <div style={{marginTop: 12, justifyContent: "flex-end"}}>
            <Button type="submit">{t("Senden")}</Button>
          </div>
        </form>
      </WindowContent>
    </WindowContentContainer>
  );
};

// -----------------------------------------------------------------------------------------------------------
interface FirmwarePickerProps extends WindowContentProps {
  onSubmit: (data: any) => void;
  target: number;
  hardwareKey: string;
  growl: any;
}

const FirmwarePicker: React.FC<FirmwarePickerProps> = ({
  headline,
  onSubmit,
  target,
  hardwareKey,
  forceCloseWindow,
  growl,
}) => {
  const {t} = useTranslation();

  // hardware
  const [hardware, setHardware] = useState<any>([]);
  useEffect(() => {
    const load = async () => {
      const [{data}, err] = await httpClient.get(`/management/firmware/findByHardware/${hardwareKey}`);
      if (err !== null) {
        growl.show({
          life: 5000,
          closable: false,
          severity: "error",
          summary: t("Firmware Versionen"),
          detail: err.response?.data?.message
            ? err.response?.data?.message
            : typeof err === "string"
            ? err
            : t("Firmware Versionen konnten nicht geladen werden"),
        });

        forceCloseWindow();
        return;
      }

      setHardware(data.filter((d: any) => d.target === target));
    };

    load();
  }, [target, hardwareKey, forceCloseWindow, t, growl]);

  // form
  const form = useFormik<any>({
    initialValues: {
      id: "",
    },
    onSubmit: (values) => {
      if (!values.id) {
        form.setSubmitting(false);
        return;
      }

      onSubmit({id: values.id});
    },
  });

  // render
  return (
    <WindowContentContainer full>
      <WindowHeadline padding>
        <h1>
          {target === 0 ? "MCU" : "LPWA"} {headline}
        </h1>
      </WindowHeadline>

      <WindowContent padding>
        <form onSubmit={form.handleSubmit}>
          <FormGrid>
            <Select
              name="id"
              value={form.values.id}
              onChange={form.handleChange}
              label={t("Version")}
              required
              style={{minWidth: 250}}
            >
              {hardware.map((h: any) => (
                <MenuItem key={h.id} value={h.id}>
                  {h.version}
                </MenuItem>
              ))}
            </Select>
          </FormGrid>

          <div style={{marginTop: 12, justifyContent: "flex-end"}}>
            <Button type="submit">{t("Update")}</Button>
          </div>
        </form>
      </WindowContent>
    </WindowContentContainer>
  );
};
