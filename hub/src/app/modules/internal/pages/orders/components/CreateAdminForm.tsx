import React, {useState} from "react";
import {
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import {useTranslation} from "react-i18next";
import {useFormik} from "formik";
import {Button, TextField, Message} from "dkv-live-frontend-ui";
import styled from "styled-components";
import httpClient from "services/http";
import {useDispatch} from "react-redux";
import {loadOrders} from "../state/actions";
import {Order} from "../state/reducer";

// ----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
  padding: 20px;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
`;

// -----------------------------------------------------------------------------------------------------------
interface CreateAdminFormProps extends WindowContentProps {
  order: Order;
  growl: any;
}

interface CreateAdminFormData {
  order_number: string;
}

const CreateAdminForm: React.FC<CreateAdminFormProps> = ({
  headline,
  forceCloseWindow,
  order,
  growl,
  setLoading,
}: {
  headline?: any;
  forceCloseWindow: any;
  order: Order;
  growl: any;
  setLoading: any;
}) => {
  const {t} = useTranslation();

  const dispatch = useDispatch();

  // form
  const [error, setError] = useState("");

  const form = useFormik<CreateAdminFormData>({
    initialValues: {
      order_number: order?.order_number ?? "",
    },
    onSubmit: (values) => {
      setLoading(true, true, t("wird erstellt"));
      setError("");

      const submit = async () => {
        const body = {
          order_number: values.order_number,
        };

        const [, err] = await httpClient.post(
          "/management/users/createAdminUserFromOrder",
          JSON.stringify(body),
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        if (err !== null) {
          setError(err.response?.data?.message ? err.response?.data?.message : "Erstellen fehlgeschlagen");
          setLoading(false);
          form.setSubmitting(false);
          return;
        }

        growl.show({
          life: 5000,
          closable: false,
          severity: "success",
          summary: "Admin User",
          detail: t("Admin User wurde erfolgreich in P5 angelegt"),
        });

        dispatch(loadOrders());
        forceCloseWindow();
      };

      submit();
    },
  });

  // render
  return (
    <WindowContentContainer full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent>
        <Form onSubmit={form.handleSubmit}>
          {error && <Message style={{marginBottom: 24}} text={t(error)} error />}

          <Message
            style={{marginBottom: 24}}
            text={t("Nach Erstellung erhält der Kunde eine Email und sofortigen Zugang zum Produkt!")}
            warning
          />
          <FormGrid>
            <TextField
              name="order_number"
              type="text"
              label={t("Bestellnummer")}
              value={form.values.order_number}
              disabled
              variant="outlined"
              required
            />
            <TextField
              name="customer_number"
              type="text"
              label={t("Kundennummer")}
              value={order.customer.customer_number}
              disabled
              variant="outlined"
              required
            />
            <TextField
              name="admin_mail"
              type="text"
              label={t("Admin-Email")}
              value={order.admin_user.email}
              disabled
              variant="outlined"
              required
            />
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting}>
              {t("Erstellen")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default CreateAdminForm;
