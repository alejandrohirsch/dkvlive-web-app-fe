import React, {useState} from "react";
import {
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import {useTranslation} from "react-i18next";
import {useFormik} from "formik";
import {Button, TextField, Message, Select, Checkbox} from "dkv-live-frontend-ui";
import styled from "styled-components";
import {MenuItem, IconButton, FormControlLabel} from "@material-ui/core";
import {countryOptions, countries} from "services/countries";
import Autocomplete from "@material-ui/lab/Autocomplete";
import httpClient from "services/http";
import {useDispatch} from "react-redux";
import {loadOrders} from "../state/actions";
import {Order} from "../state/reducer";

// ----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
  margin: 12px 32px;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 12px;
`;

const FormRow = styled.div`
  display: flex;
  flex-direction: row;
`;

const ItemsContainer = styled.div`
  & > div {
    & > * {
      margin-right: 12px;
      margin-top: 12px;
    }
  }
  & > div {
    & > *:last-of-type {
      margin-right: 0;
    }
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface OrderFormProps extends WindowContentProps {
  editData?: any;
}

const OrderForm: React.FC<OrderFormProps> = ({headline, forceCloseWindow, editData, setLoading}) => {
  const {t} = useTranslation();
  const editMode = editData !== undefined;

  const dispatch = useDispatch();

  // form
  const [error, setError] = useState("");

  const form = useFormik<Order>({
    initialValues: editData ?? {
      id: "",
      order_number: "",
      customer: {customer_number: "", name: "", organization_id: ""},
      shipping_recipient: {
        company_name: "",
        contact: {first_name: "", last_name: "", email: "", phone: ""},
        address: {
          street: "",
          house_number: "",
          address_addition: "",
          zip: "",
          place: "",
          country_code: "",
        },
      },
      items: [],
      admin_user: {
        first_name: "",
        last_name: "",
        email: "",
        phone: "",
        language: "",
        time_zone: "",
      },
    },
    onSubmit: (values) => {
      console.log(`values:`, values); // nocheckin

      setLoading(true, true, t(editMode ? "wird geändert" : "wird erstellt"));
      setError("");

      const submit = async () => {
        const order = {
          order_number: values.order_number,
          customer: {
            customer_number: values.customer.customer_number,
            name: values.customer.name,
            organization_id: values.customer.organization_id,
          },
          shipping_recipient: {
            company_name: values.shipping_recipient.company_name,
            contact: {
              first_name: values.shipping_recipient.contact.first_name,
              last_name: values.shipping_recipient.contact.last_name,
              email: values.shipping_recipient.contact.email,
              phone: values.shipping_recipient.contact.phone,
            },
            address: {
              street: values.shipping_recipient.address.street,
              house_number: values.shipping_recipient.address.house_number,
              address_addition: values.shipping_recipient.address.address_addition,
              zip: values.shipping_recipient.address.zip,
              place: values.shipping_recipient.address.place,
              country_code: values.shipping_recipient.address.country_code,
            },
          },
          items: values.items,
          admin_user: {
            first_name: values.admin_user.first_name,
            last_name: values.admin_user.last_name,
            email: values.admin_user.email,
            phone: values.admin_user.phone,
            language: values.admin_user.language,
            time_zone: values.admin_user.time_zone,
          },
        };

        const [{data}, err] = await httpClient.post(
          "/management/orders/createOrUpdate",
          JSON.stringify({orders: [order]}),
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        if (err !== null) {
          setError(
            err.response?.data?.message
              ? err.response?.data?.message
              : editMode
              ? "Änderung fehlgeschlagen"
              : "Erstellen fehlgeschlagen"
          );
          setLoading(false);
          form.setSubmitting(false);
          return;
        }

        if (!data.order_responses || !data.order_responses.length || !data.order_responses[0].success) {
          setError(
            data.order_responses.length && data.order_resonses[0].error_msg
              ? data.order_resonses[0].error_msg
              : editMode
              ? "Änderung fehlgeschlagen"
              : "Erstellen fehlgeschlagen"
          );
          setLoading(false);
          form.setSubmitting(false);
          return;
        }

        dispatch(loadOrders());
        forceCloseWindow();
      };

      submit();
    },
  });

  const addItem = () => {
    form.setFieldValue("items", [
      ...form.values.items,
      {
        _id: Math.random()
          .toString(36)
          .substring(7),
        leo_id: "",
        type: "TRUCK",
        connection_type: "FMS",
        branding: "DKV_DEFAULT",
        already_handed_out: false,
      },
    ]);
  };

  const removeItem = (index: number) => {
    form.values.items.splice(index, 1);
    form.setFieldValue("items", Array.from(form.values.items));
  };

  const handleItemChange = (index: number, field: string, value: any) => {
    const items = form.values.items;
    items[index][field] = value;

    form.setFieldValue("items", [...form.values.items]);
  };

  // render
  return (
    <WindowContentContainer minWidth="1300px" full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent>
        <Form onSubmit={form.handleSubmit}>
          {error && <Message style={{marginBottom: 24}} text={t(error)} error />}

          <FormGrid>
            <h2 style={{gridColumn: "1/span 2"}}>{t("Daten")}</h2>
            <TextField
              name="order_number"
              type="text"
              label={t("Bestellnummer")}
              value={form.values.order_number}
              onChange={form.handleChange}
              disabled={editMode || form.isSubmitting}
              variant="outlined"
              required
              autoFocus
              style={{gridColumn: "1/span 2"}}
            />
            <TextField
              name="customer.customer_number"
              type="text"
              label={t("DKV Kundennummer")}
              value={form.values.customer.customer_number}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />
            <TextField
              name="customer.name"
              type="text"
              label={t("Kunden-Firmenname")}
              value={form.values.customer.name}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />
          </FormGrid>

          <FormGrid style={{marginTop: 32}}>
            <h2>{t("Empfänger")}</h2>
            <TextField
              name="shipping_recipient.company_name"
              type="text"
              label={t("Firmenname")}
              value={form.values.shipping_recipient.company_name}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
              style={{gridColumn: "1/span 2"}}
            />
          </FormGrid>

          <FormGrid style={{marginTop: 24}}>
            <TextField
              name="shipping_recipient.contact.email"
              type="email"
              label={t("E-Mail")}
              value={form.values.shipping_recipient.contact.email}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="shipping_recipient.contact.first_name"
              type="text"
              label={t("Vorname")}
              value={form.values.shipping_recipient.contact.first_name}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />

            <TextField
              name="shipping_recipient.contact.last_name"
              type="text"
              label={t("Nachname")}
              value={form.values.shipping_recipient.contact.last_name}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />

            <TextField
              name="shipping_recipient.contact.phone"
              type="phone"
              label={t("Telefon")}
              value={form.values.shipping_recipient.contact.phone}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />
          </FormGrid>

          <FormGrid style={{marginTop: 24}}>
            <Autocomplete
              value={{
                value: form.values.shipping_recipient.address.country_code,
                label: countries[form.values.shipping_recipient.address.country_code] ?? "",
              }}
              onChange={(_: any, newValue: {label: string; value: string} | null) => {
                form.setFieldValue("shipping_recipient.address.country_code", newValue ? newValue.value : "");
              }}
              options={countryOptions}
              autoHighlight
              getOptionLabel={(option) => t(option.label)}
              renderOption={(option) => (
                <>
                  {t(option.label)} ({option.value})
                </>
              )}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Land"
                  variant="outlined"
                  inputProps={{
                    ...params.inputProps,
                    autoComplete: "new-password",
                  }}
                />
              )}
              clearText={t("Leeren")}
              closeText={t("Schließen")}
              loadingText={t("Lädt...")}
              noOptionsText={t("Keine Optionen")}
              openText={t("Öffnen")}
            />

            <TextField
              name="shipping_recipient.address.zip"
              type="text"
              label={t("PLZ")}
              value={form.values.shipping_recipient.address.zip}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />

            <TextField
              name="shipping_recipient.address.place"
              type="text"
              label={t("Ort")}
              value={form.values.shipping_recipient.address.place}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="shipping_recipient.address.street"
              type="text"
              label={t("Straße")}
              value={form.values.shipping_recipient.address.street}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />

            <TextField
              name="shipping_recipient.address.house_number"
              type="text"
              label={t("Hausnummer")}
              value={form.values.shipping_recipient.address.house_number}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="shipping_recipient.address.address_addition"
              type="text"
              label={t("Adresszusatz")}
              value={form.values.shipping_recipient.address.address_addition}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
            />
          </FormGrid>

          <FormGrid style={{marginTop: 32}}>
            <h2 style={{gridColumn: "1/span 2"}}>{t("Admin User")}</h2>
            <TextField
              name="admin_user.email"
              type="email"
              label={t("E-Mail")}
              value={form.values.admin_user.email}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />

            <TextField
              name="admin_user.first_name"
              type="text"
              label={t("Vorname")}
              value={form.values.admin_user.first_name}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />

            <TextField
              name="admin_user.last_name"
              type="text"
              label={t("Nachname")}
              value={form.values.admin_user.last_name}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />

            <Select
              name="admin_user.language"
              value={form.values.admin_user.language}
              onChange={form.handleChange}
              label={t("Sprache")}
              required
            >
              <MenuItem value="de">DE</MenuItem>
              <MenuItem value="en">EN</MenuItem>
            </Select>

            <TextField
              name="admin_user.phone"
              type="phone"
              label={t("Telefon")}
              value={form.values.admin_user.phone}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
            />

            <TextField
              name="admin_user.time_zone"
              type="text"
              label={t("Zeitzone")}
              value={form.values.admin_user.time_zone}
              onChange={form.handleChange}
              disabled={form.isSubmitting}
              variant="outlined"
            />
          </FormGrid>

          <ItemsContainer style={{marginTop: 32}}>
            <h2>{t("Items")}</h2>

            {form.values.items.map((i: any, k: number) => (
              <FormRow key={i._id}>
                <TextField
                  name="items_leo_id"
                  type="text"
                  label={t("LEO ID")}
                  value={i.leo_id}
                  onChange={(e: any) => handleItemChange(k, "leo_id", e.target.value)}
                  disabled={form.isSubmitting}
                  variant="outlined"
                />

                <Select
                  name="items_type"
                  value={i.type}
                  onChange={(e: any) => handleItemChange(k, "type", e.target.value)}
                  label={t("Type")}
                  required
                  style={{minWidth: 250}}
                >
                  <MenuItem value="TRUCK">TRUCK</MenuItem>
                  <MenuItem value="CAR">CAR</MenuItem>
                  <MenuItem value="BUS">BUS</MenuItem>
                  <MenuItem value="TRAILER">TRAILER</MenuItem>
                </Select>

                <Select
                  name="items_connection_type"
                  value={i.connection_type}
                  onChange={(e: any) => handleItemChange(k, "connection_type", e.target.value)}
                  label={t("Verbindungstyp")}
                  required
                  style={{minWidth: 250}}
                >
                  <MenuItem value="FMS">FMS</MenuItem>
                  <MenuItem value="CANCL">CANCL</MenuItem>
                  <MenuItem value="EBS_WABCO">EBS_WABCO</MenuItem>
                  <MenuItem value="EBS_KNORR">EBS_KNORR</MenuItem>
                  <MenuItem value="UNKNOWN">UNKNOWN</MenuItem>
                  <MenuItem value="N/A">N/A</MenuItem>
                </Select>

                <Select
                  name="items_branding"
                  value={i.branding}
                  onChange={(e: any) => handleItemChange(k, "branding", e.target.value)}
                  label={t("Branding")}
                  required
                  style={{minWidth: 250}}
                >
                  <MenuItem value="DKV_DEFAULT">DKV DEFAULT</MenuItem>
                </Select>

                <div style={{minWidth: 200, paddingLeft: 12, display: "flex"}}>
                  <FormControlLabel
                    label={t("Bereits beim Kunden")}
                    control={
                      <Checkbox
                        style={{marginRight: 8}}
                        onChange={(e: any) => handleItemChange(k, "already_handed_out", e.target.checked)}
                        checked={i.already_handed_out}
                      />
                    }
                  />
                </div>

                <IconButton aria-label={t("Entfernen")} onClick={() => removeItem(k)}>
                  <i className="icon-ico_remove" />
                </IconButton>
              </FormRow>
            ))}

            <IconButton aria-label={t("Hinzufügen")} onClick={addItem}>
              <i className="icon-ico_add" />
            </IconButton>
          </ItemsContainer>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !form.values.order_number}>
              {t(editMode ? "Ändern" : "Bestellen")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default OrderForm;
