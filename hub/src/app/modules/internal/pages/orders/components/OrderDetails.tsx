import React, {useState, useCallback} from "react";
import Window, {
  WindowContentContainer,
  WindowHeadline,
  WindowTabsContainer,
  WindowTabs,
  WindowContent,
  WindowContentProps,
  WindowActions,
} from "app/components/Window";
import {Tab} from "dkv-live-frontend-ui";
import styled, {css} from "styled-components";
import {useTranslation} from "react-i18next";
import {ToolbarButton, ToolbarButtonIcon} from "app/components/DataTable";
import OrderForm from "./OrderForm";
import {hasPermission} from "app/modules/login/state/login/selectors";
import httpClient from "services/http";
import CreateAdminForm from "./CreateAdminForm";
import {useDispatch, useSelector} from "react-redux";
import {loadOrders} from "../state/actions";
import {Order} from "../state/reducer";
import {orderSelector} from "../state/selectors";

// -----------------------------------------------------------------------------------------------------------
export const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

export const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

export const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);
  text-align: right;

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

const OrdersContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 24px;
`;

// -----------------------------------------------------------------------------------------------------------
interface OrderDetailsProps extends WindowContentProps {
  orderID: string;
  growl: any;
}

const OrderDetails: React.FC<OrderDetailsProps> = ({orderID, growl, setLoading}) => {
  const {t} = useTranslation();

  const dispatch = useDispatch();

  // tab
  const [tab, setTab] = useState(0);

  const handleTabChange = (_: React.ChangeEvent<unknown>, newTab: number) => {
    setTab(newTab);
  };

  // order
  const order = useSelector(orderSelector(orderID || ""));

  // edit
  const [showEditWindow, setShowEditWindow] = useState<Order | null>(null);
  const onCloseEditWindow = () => setShowEditWindow(null);
  const openEditWindow = () => setShowEditWindow(order ?? null);

  // create admin user
  const [showCreateAdminUserWindow, setShowCreateAdminUserWindow] = useState<Order | null>(null);
  const onCloseCreateAdminUserWindow = () => setShowCreateAdminUserWindow(null);
  const openCreateAdminUserWindow = () => setShowCreateAdminUserWindow(order ?? null);

  // create shipment
  const createShipment = useCallback(() => {
    const create = async () => {
      if (!order) {
        return;
      }
      setLoading(true);

      const [, err] = await httpClient.post(`/management/shipments/createFromOrder/${order.id}`);
      if (err !== null) {
        growl.show({
          life: 5000,
          closable: false,
          severity: "error",
          summary: "Lieferung",
          detail: err.response?.data?.message
            ? err.response?.data?.message
            : typeof err === "string"
            ? err
            : t("Lieferung konnte nicht erstellt werden"),
        });
        setLoading(false);
        return;
      }

      growl.show({
        life: 5000,
        closable: false,
        severity: "success",
        summary: "Lieferung",
        detail: t("Lieferung erfolgreich angelegt"),
      });

      dispatch(loadOrders());
      setLoading(false);
    };

    create();
  }, [order, setLoading, growl, t, dispatch]);

  // render
  return (
    <WindowContentContainer minWidth="700px" minHeight="500px" full>
      <WindowHeadline padding>
        <h1>
          {t("Bestellung")} - {order ? order.order_number : ""}
        </h1>
      </WindowHeadline>

      <WindowTabsContainer>
        <WindowTabs value={tab} onChange={handleTabChange} variant="scrollable">
          <Tab label={t("Übersicht")} />
          <Tab label={t("Items")} />
          <Tab label={t("Empfänger")} />
          <Tab label={t("Admin User")} />
        </WindowTabs>
      </WindowTabsContainer>

      <WindowContent padding>
        {tab === 0 && (
          <Table>
            <tbody>
              <tr>
                <TableLabel>{t("Bestellnummer")}:</TableLabel>
                <TableValue>{order ? order.order_number : ""}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Kunde")}:</TableLabel>
                <TableValue>
                  {order ? order.customer.name : ""} ({order ? order.customer.customer_number : ""})
                </TableValue>
              </tr>
              <tr>
                <td colSpan={2}>&nbsp;</td>
              </tr>
              <tr>
                <TableLabel>{t("Erstellt am")}:</TableLabel>
                <TableValue>{order ? order._created_at : ""}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Geändert am")}:</TableLabel>
                <TableValue>{order ? order._modified_at : ""}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Lieferung erstellt am")}:</TableLabel>
                <TableValue>{order ? order._shipment_created_at : ""}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Admin-User erstellt am")}:</TableLabel>
                <TableValue>{order ? order._admin_user_created_at : ""}</TableValue>
              </tr>
            </tbody>
          </Table>
        )}

        {tab === 1 && (
          <OrdersContainer>
            {order
              ? order.items.map((i: any) => (
                  <Table key={i.leo_id} style={{marginBottom: 12}}>
                    <tbody>
                      <tr>
                        <TableLabel>{t("LEO ID")}:</TableLabel>
                        <TableValue>{i.leo_id}</TableValue>
                      </tr>
                      <tr>
                        <TableLabel>{t("Type")}:</TableLabel>
                        <TableValue>{i.type}</TableValue>
                      </tr>
                      <tr>
                        <TableLabel>{t("Verbindungstyp")}:</TableLabel>
                        <TableValue>{i.connection_type}</TableValue>
                      </tr>
                      <tr>
                        <TableLabel>{t("Branding")}:</TableLabel>
                        <TableValue>{i.branding}</TableValue>
                      </tr>
                      <tr>
                        <TableLabel>{t("Bereits beim Kunden")}:</TableLabel>
                        <TableValue>{t(i.already_handed_out ? "Ja" : "Nein")}</TableValue>
                      </tr>
                    </tbody>
                  </Table>
                ))
              : ""}
          </OrdersContainer>
        )}

        {tab === 2 && (
          <>
            <Table>
              <tbody>
                <tr>
                  <TableLabel>{t("Firmenname")}:</TableLabel>
                  <TableValue>{order ? order.shipping_recipient.company_name : ""}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Adresse")}:</TableLabel>
                  <TableValue>
                    {order ? order.shipping_recipient.address.country_code : ""}-
                    {order ? order.shipping_recipient.address.zip : ""}{" "}
                    {order ? order.shipping_recipient.address.place : ""}{" "}
                    {order ? order.shipping_recipient.address.street : ""}{" "}
                    {order ? order.shipping_recipient.address.house_number : ""}
                  </TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Adresszusatz")}:</TableLabel>
                  <TableValue>{order ? order.shipping_recipient.address.address_addition : ""}</TableValue>
                </tr>
                <tr>
                  <td colSpan={2}>&nbsp;</td>
                </tr>
                <tr>
                  <TableLabel>{t("E-Mail")}:</TableLabel>
                  <TableValue>{order ? order.shipping_recipient.contact.email : ""}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Name")}:</TableLabel>
                  <TableValue>
                    {order ? order.shipping_recipient.contact.first_name : ""}{" "}
                    {order ? order.shipping_recipient.contact.last_name : ""}
                  </TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Telefon")}:</TableLabel>
                  <TableValue>{order ? order.shipping_recipient.contact.phone : ""}</TableValue>
                </tr>
              </tbody>
            </Table>
          </>
        )}

        {tab === 3 && (
          <>
            <Table>
              <tbody>
                <tr>
                  <TableLabel>{t("E-Mail")}:</TableLabel>
                  <TableValue>{order ? order.admin_user.email : ""}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Name")}:</TableLabel>
                  <TableValue>
                    {order ? order.admin_user.last_name : ""} {order ? order.admin_user.first_name : ""}
                  </TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Sprache")}:</TableLabel>
                  <TableValue>{order ? order.admin_user.language : ""}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Telefon")}:</TableLabel>
                  <TableValue>{order ? order.admin_user.phone : ""}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Zeitzone")}:</TableLabel>
                  <TableValue>{order ? order.admin_user.time_zone : ""}</TableValue>
                </tr>
              </tbody>
            </Table>
          </>
        )}
      </WindowContent>

      <WindowActions autoTop>
        {hasPermission("superadmin:CreateOrUpdateOrders") && (
          <ToolbarButton type="button" onClick={openEditWindow}>
            <ToolbarButtonIcon>
              <span className="icon-ico_edit" />
            </ToolbarButtonIcon>
            {t("Bearbeiten")}
          </ToolbarButton>
        )}

        {hasPermission("superadmin:CreateShipments") && order && !order._shipment_created_at && (
          <ToolbarButton onClick={createShipment}>
            <ToolbarButtonIcon>
              <span className="icon-ico_logout" />
            </ToolbarButtonIcon>
            {t("Lieferung erstellen")}
          </ToolbarButton>
        )}
        {hasPermission("superadmin:CreateAdminUser") && order && !order._admin_user_created_at && (
          <ToolbarButton onClick={openCreateAdminUserWindow}>
            <ToolbarButtonIcon>
              <span className="icon-ico_superuser" />
            </ToolbarButtonIcon>
            {t("Admin-User erstellen")}
          </ToolbarButton>
        )}
      </WindowActions>

      {showEditWindow && (
        <Window onClose={onCloseEditWindow} headline={t("Bestellung")}>
          {(props) => <OrderForm {...props} editData={showEditWindow} />}
        </Window>
      )}
      {showCreateAdminUserWindow && (
        <Window onClose={onCloseCreateAdminUserWindow} headline={t("Admin User erstellen")}>
          {(props) => <CreateAdminForm {...props} order={showCreateAdminUserWindow} growl={growl} />}
        </Window>
      )}
    </WindowContentContainer>
  );
};

export default OrderDetails;
