import React, {useState, useCallback, useEffect, useRef} from "react";
import Page from "app/components/Page";
import {useTranslation} from "react-i18next";
import DataTable, {
  TablePageContent,
  useColumns,
  TableStickyHeader,
  TableStickyFooter,
  TableContainer,
  TableFooterActions,
  ToolbarButton,
  ToolbarButtonIcon,
  bodyField,
  TableSelectionCountInfo,
  TableFooterRightActions,
} from "app/components/DataTable";
import {Button, Loading} from "dkv-live-frontend-ui";
import store from "app/state/store";
import Window from "app/components/Window";
import OrderDetails from "./components/OrderDetails";
import httpClient from "services/http";
import OrderForm from "./components/OrderForm";
import {hasPermission} from "app/modules/login/state/login/selectors";
import {Growl} from "primereact/growl";
import {ordersStateSelector, ordersListSelector} from "./state/selectors";
import {useSelector, useDispatch} from "react-redux";
import {loadOrdersIfNeeded, loadOrders} from "./state/actions";
import {OrdersModule} from "./state/module";
import {Order} from "./state/reducer";

// -----------------------------------------------------------------------------------------------------------
const orderColumns = [
  {
    field: "order_number",
    header: "Bestellnummer",
    body: (o: Order) => (
      <div
        style={{
          color:
            !o._shipment_created_at || !o._admin_user_created_at
              ? "var(--dkv-highlight-warning-color)"
              : "var(--dkv-highlight-ok-color)",
          fontWeight: "bold",
        }}
      >
        {o.order_number}
      </div>
    ),
    width: 250,
  },
  {
    field: "customer.customer_number",
    header: "Kundennummer",
    width: 250,
    body: bodyField((d) => d.customer.customer_number),
  },
  {
    field: "customer.name",
    header: "Kunde",
    width: 250,
    body: bodyField((d) => d.customer.name),
  },
  {
    field: "_shipment_created_at",
    header: "Lieferung erstellt am",
    width: 150,
  },
  {
    field: "_admin_user_created_at",
    header: "Admin-User erstellt am",
    width: 150,
  },
  {
    field: "_created_at",
    header: "Erstellt am",
    width: 150,
  },
  {
    field: "_modified_at",
    header: "Geändert am",
    width: 150,
  },
];

// -----------------------------------------------------------------------------------------------------------

store.addModule(OrdersModule);

// -----------------------------------------------------------------------------------------------------------
const Orders: React.FC = () => {
  const {t} = useTranslation();

  // columns
  const [columns] = useState(orderColumns);
  const visibleColumns = useColumns(columns);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  // detail
  const [showDetailWindow, setShowDetailWindow] = useState<Order | null>(null);
  const onCloseDetailWindow = useCallback(() => setShowDetailWindow(null), [setShowDetailWindow]);
  const openDetailWindow = useCallback((e: any) => setShowDetailWindow(e.data as Order), [
    setShowDetailWindow,
  ]);

  const openDetailsWindowForSelected = useCallback(
    () => selected.length && selected[0] && setShowDetailWindow(selected[0]),
    [selected, setShowDetailWindow]
  );

  // edit
  const [editData, setEditData] = useState<any>();

  // orders
  const growlRef = useRef<any>();

  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();

  // orders
  const orderListState = useSelector(ordersStateSelector);
  const orders = useSelector(ordersListSelector);

  useEffect(() => {
    dispatch(loadOrdersIfNeeded());
  }, [dispatch]);

  const refresh = useCallback(() => dispatch(loadOrders()), [dispatch]);

  // new window
  const [showNewWindow, setShowNewWindow] = useState(false);

  const onShowNewWindow = useCallback(() => {
    setEditData(undefined);
    setShowNewWindow(true);
  }, [setShowNewWindow]);

  const onCloseNewWindow = useCallback(() => {
    setShowNewWindow(false);
  }, []);

  // create shipment
  const createShipment = useCallback(() => {
    if (!selected.length || !selected[0]) {
      return;
    }

    const create = async () => {
      setLoading(true);

      const [, err] = await httpClient.post(`/management/shipments/createFromOrder/${selected[0].id}`);
      if (err !== null) {
        growlRef.current.show({
          life: 5000,
          closable: false,
          severity: "error",
          summary: "Lieferung",
          detail: err.response?.data?.message
            ? err.response?.data?.message
            : typeof err === "string"
            ? err
            : t("Lieferung konnte nicht erstellt werden"),
        });
        setLoading(false);
        return;
      }

      growlRef.current.show({
        life: 5000,
        closable: false,
        severity: "success",
        summary: "Lieferung",
        detail: t("Lieferung erfolgreich angelegt"),
      });
      refresh();
      setLoading(false);
    };

    create();
  }, [refresh, selected, t]);

  // render
  return (
    <Page id="orders">
      <Loading inprogress={loading || orderListState.loading} />

      <TablePageContent>
        <TableStickyHeader>
          <h1>{t("Bestellungen")}</h1>

          {/* <SearchTextField
            name="search"
            type="text"
            required
            label={t("Suche")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          /> */}

          {hasPermission("superadmin:CreateOrUpdateOrders") && (
            <ToolbarButton general style={{marginLeft: "auto"}} onClick={onShowNewWindow}>
              <ToolbarButtonIcon>
                <span className="icon-ico_add" />
              </ToolbarButtonIcon>
              {t("Hinzufügen")}
            </ToolbarButton>
          )}

          <ToolbarButton general onClick={refresh}>
            <ToolbarButtonIcon>
              <span className="icon-ico_load" />
            </ToolbarButtonIcon>
            {t("Aktualisieren")}
          </ToolbarButton>
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={orders}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            onRowDoubleClick={openDetailWindow}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Eintrag ausgewählt" : "Einträge ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>
              <TableFooterActions>
                {selected.length === 1 && (
                  <>
                    <ToolbarButton onClick={openDetailsWindowForSelected}>
                      <ToolbarButtonIcon>
                        <span className="icon-ico_billings" />
                      </ToolbarButtonIcon>
                      {t("Details")}
                    </ToolbarButton>

                    {hasPermission("superadmin:CreateShipments") && (
                      <ToolbarButton onClick={createShipment}>
                        <ToolbarButtonIcon>
                          <span className="icon-ico_logout" />
                        </ToolbarButtonIcon>
                        {t("Lieferung erstellen")}
                      </ToolbarButton>
                    )}
                  </>
                )}
              </TableFooterActions>
              <TableFooterRightActions />
            </>
          )}

          {/* <TableFooterRightActions>
            <Button autoWidth secondary ariaLabel={t("Gesamte Liste exportieren")}>
              {t("Gesamte Liste exportieren")}
            </Button>

            {selected.length > 0 && (
              <Button autoWidth ariaLabel={t("Auswahl als Liste exportieren")}>
                {t("Auswahl als Liste exportieren")}
              </Button>
            )}
          </TableFooterRightActions> */}
        </TableStickyFooter>
      </TablePageContent>

      <Growl ref={(el) => (growlRef.current = el)} baseZIndex={20000} />

      {showDetailWindow && (
        <Window onClose={onCloseDetailWindow} headline={t("Bestellung")}>
          {(props) => <OrderDetails {...props} orderID={showDetailWindow.id} growl={growlRef.current} />}
        </Window>
      )}

      {showNewWindow && (
        <Window onClose={onCloseNewWindow} headline={t("Bestellung")}>
          {(props) => <OrderForm editData={editData} {...props} />}
        </Window>
      )}
    </Page>
  );
};

export default Orders;
