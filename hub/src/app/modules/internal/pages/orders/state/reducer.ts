import {createReducer} from "@reduxjs/toolkit";
import {
  LoadingOrdersFailedAction,
  OrdersLoadedAction,
  LOADING_ORDERS,
  LOADING_ORDERS_FAILED,
  ORDERS_LOADED,
} from "./types";
import dayjs from "dayjs";
import {resolveUserDateFormat} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
export interface Order {
  id: string;
  order_number: string;
  customer: Customer;
  shipping_recipient: ShippingRecipient;
  items: Item[];
  admin_user: AdminUser;
  legacy_user_id: string;
  admin_user_created_at: string;
  shipment_created_at: string;
  created_at: string;
  modified_at: string;

  _admin_user_created_at: string | null;
  _shipment_created_at: string | null;
  _created_at: string | null;
  _modified_at: string | null;
}

export interface Customer {
  customer_number: string;
  name: string;
  organization_id: string;
}

export interface ShippingRecipient {
  company_name: string;
  contact: Contact;
  address: Address;
}

export interface Contact {
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
}

export interface Address {
  street: string;
  house_number: string;
  address_addition: string;
  zip: string;
  place: string;
  country_code: string;
}

export interface Item {
  leo_id: string;
  type: string;
  connection_type: string;
  branding: string;
  already_handed_out: boolean;
}

export interface AdminUser {
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  language: string;
  time_zone: string;
}

// -----------------------------------------------------------------------------------------------------------
export interface OrdersState {
  loading: boolean;
  loaded?: boolean;
  error?: boolean;
  errorText?: string;
  items?: {[id: string]: Order};
}

const initialState: OrdersState = {
  loading: true,
};

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingOrders(state: OrdersState) {
  state.loading = true;
  state.loaded = false;

  if (state.error) {
    delete state.error;
    delete state.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingOrdersFailed(state: OrdersState, action: LoadingOrdersFailedAction) {
  state.loading = false;
  state.error = true;
  state.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleOrdersLoaded(state: OrdersState, action: OrdersLoadedAction) {
  state.loading = false;
  state.loaded = true;

  const items = {};
  action.payload.forEach((a) => {
    a._admin_user_created_at =
      a.admin_user_created_at && a.admin_user_created_at !== "0001-01-01T00:00:00Z"
        ? dayjs(a.admin_user_created_at).format(resolveUserDateFormat())
        : null;
    a._shipment_created_at =
      a.shipment_created_at && a.shipment_created_at !== "0001-01-01T00:00:00Z"
        ? dayjs(a.shipment_created_at).format(resolveUserDateFormat())
        : null;
    a._created_at =
      a.created_at && a.created_at !== "0001-01-01T00:00:00Z"
        ? dayjs(a.created_at).format(resolveUserDateFormat())
        : null;
    a._modified_at =
      a.modified_at && a.modified_at !== "0001-01-01T00:00:00Z"
        ? dayjs(a.modified_at).format(resolveUserDateFormat())
        : null;
    items[a.id] = a;
  });

  state.items = items;

  if (state.error) {
    delete state.error;
    delete state.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export const ordersReducer = createReducer(initialState, {
  [LOADING_ORDERS]: handleLoadingOrders,
  [LOADING_ORDERS_FAILED]: handleLoadingOrdersFailed,
  [ORDERS_LOADED]: handleOrdersLoaded,
});
