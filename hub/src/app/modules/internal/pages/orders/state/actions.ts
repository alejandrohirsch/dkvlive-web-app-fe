import {OrdersThunkResult} from "./module";
import {Order} from "./reducer";
import httpClient from "services/http";
import {
  LoadingOrdersAction,
  LOADING_ORDERS,
  OrdersLoadedAction,
  ORDERS_LOADED,
  LoadingOrdersFailedAction,
  LOADING_ORDERS_FAILED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingOrdersAction(): LoadingOrdersAction {
  return {
    type: LOADING_ORDERS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingOrdersFailedAction(errorText?: string): LoadingOrdersFailedAction {
  return {
    type: LOADING_ORDERS_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function ordersLoadedAction(items: Order[]): OrdersLoadedAction {
  return {
    type: ORDERS_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadOrders(): OrdersThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingOrdersAction());

    const [{data}, err] = await httpClient.get("/management/orders");
    if (err !== null) {
      // @todo: log
      dispatch(loadingOrdersFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(ordersLoadedAction(data));
  };
}

export function loadOrdersIfNeeded(): OrdersThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().orders;
    const isLoaded = data.items !== undefined && data.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadOrders());
  };
}
