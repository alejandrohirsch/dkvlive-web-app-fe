import {OrdersModuleState} from "./module";
import {createSelector} from "@reduxjs/toolkit";

export const ordersItemsSelector = (state: OrdersModuleState) => state.orders.items;

// this.state.
export const ordersStateSelector = (state: OrdersModuleState) => state.orders;

// list
export const ordersListSelector = createSelector(ordersItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// order
export const orderSelector = (id: string) =>
  createSelector(ordersItemsSelector, (items) => (items ? items[id] : undefined));
