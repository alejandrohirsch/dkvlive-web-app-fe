import {IModule} from "redux-dynamic-modules";
import {OrdersActions} from "./types";
import {ThunkAction} from "redux-thunk";
import {OrdersState, ordersReducer} from "./reducer";

export interface OrdersModuleState {
  orders: OrdersState;
}

export type OrdersThunkResult<R> = ThunkAction<R, OrdersModuleState, void, OrdersActions>;

export const OrdersModule: IModule<OrdersModuleState> = {
  id: "orders",
  reducerMap: {
    orders: ordersReducer,
  },
};
