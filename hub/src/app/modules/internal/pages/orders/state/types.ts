import {Order} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ORDERS = "orders/loading";

export interface LoadingOrdersAction {
  type: typeof LOADING_ORDERS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_ORDERS_FAILED = "orders/loadingFailed";

export interface LoadingOrdersFailedAction {
  type: typeof LOADING_ORDERS_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const ORDERS_LOADED = "orders/loaded";

export interface OrdersLoadedAction {
  type: typeof ORDERS_LOADED;
  payload: Order[];
}

// -----------------------------------------------------------------------------------------------------------
export type OrdersActions = LoadingOrdersAction | LoadingOrdersFailedAction | OrdersLoadedAction;
