import React, {useState, useCallback, useEffect, MouseEventHandler} from "react";
import {useTranslation} from "react-i18next";
import Page from "app/components/Page";
import {Button, Loading, Message} from "dkv-live-frontend-ui";
import DataTable, {
  TablePageContent,
  TableStickyHeader,
  TableContainer,
  TableStickyFooter,
  useColumns,
  ToolbarButton,
  ToolbarButtonIcon,
  TableFooterActions,
  TableSelectionCountInfo,
  TableFooterRightActions,
} from "app/components/DataTable";
import Window from "app/components/Window";
import {useSelector, useDispatch} from "react-redux";
import ConfirmWindow from "app/components/ConfirmWindow";
import {InspectionTypesModule} from "./state/module";
import store from "app/state/store";
import {loadInspectionTypesIfNeeded, loadInspectionTypes, deleteInspectionTypes} from "./state/actions";
import {inspectionTypesListSelector, inspectionTypesStateSelector} from "./state/selectors";
import httpClient from "services/http";
import InspectionTypeForm, {InspectionTypeCreateData} from "./components/InspectionTypeForm";
import {InspectionType} from "./state/reducer";
import {countries} from "services/countries";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
store.addModule(InspectionTypesModule);

// -----------------------------------------------------------------------------------------------------------
const InspectionTypes: React.FC = () => {
  const {t} = useTranslation();

  // asset types
  const [assetTypeMap, setAssetTypeMap] = useState<Map<string, string> | null>(new Map<string, string>());
  useEffect(() => {
    //retrieve the asset types
    const loadAssetTypes = async () => {
      const [{data}, err] = await httpClient.get("/management/assets/types");
      if (err !== null) {
        // setError("Geräte-Laden fehlgeschlagen");
        return;
      }
      const assetTypeMap = data.reduce((map: Map<string, string>, at: any) => {
        map.set(at.id, at.name);
        return map;
      }, new Map<string, string>());
      // console.log("loaded asset type map", assetTypeMap);
      setAssetTypeMap(assetTypeMap);
    };
    loadAssetTypes();
  }, []);

  // list columns
  useEffect(() => {
    const listColumns = [
      {field: "name", header: "Name", width: 150, sortable: false},
      {field: "category", header: "Kategorie", width: 150, sortable: false},
      {
        field: "asset_type_id",
        header: "Fahrzeugart",
        body: (it: InspectionType) => {
          // console.log(it, assetTypeMap);
          return assetTypeMap && assetTypeMap.has(it.asset_type_id)
            ? assetTypeMap.get(it.asset_type_id)
            : "?";
        },
        width: 150,
        sortable: false,
      },
      {
        field: "countries",
        header: "Länder",
        body: (it: InspectionType) => it.countries.map((c) => t(countries[c])).join(","),
        width: 150,
        sortable: false,
      },
      // {
      //   field: "validity",
      //   header: "Gültigkeit",
      //   body: (it: InspectionType) =>
      //     it.validity.duration +
      //     " " +
      //     (it.validity.time_unit === "m" ? t("Monat") : t("Tag")) +
      //     (it.validity.duration > 1 ? "e" : ""),
      //   width: 200,
      //   sortable: false,
      // },
    ];
    setColumns(listColumns);
  }, [assetTypeMap, t]);

  const dispatch = useDispatch();

  // inspectionTypes
  const inspectionTypesListState = useSelector(inspectionTypesStateSelector);
  const inspectionTypes = useSelector(inspectionTypesListSelector);

  useEffect(() => {
    dispatch(loadInspectionTypesIfNeeded());
  }, [dispatch]);

  const refresh = useCallback(() => dispatch(loadInspectionTypes()), [dispatch]);

  // organizations
  const [organizations, setOrganizations] = useState<any>(null);
  const [error, setError] = useState("");

  useEffect(() => {
    const load = async () => {
      const [{data}, err] = await httpClient.get("/management/organizations/findWithDivisions");
      if (err !== null) {
        setError("Laden fehlgeschlagen");
        return;
      }

      setOrganizations(data);
    };

    load();
  }, []);

  // edit
  const [editData, setEditData] = useState<InspectionTypeCreateData | undefined>();

  const showEditWndow = useCallback(
    (e: any) => {
      if (!hasPermission("superadmin:EditInspectionTypes")) {
        return;
      }

      const org = e.data.organization_id
        ? organizations.find((o: any) => o.id === e.data.organization_id)
        : null;

      const divs: any[] = [];
      if (org && e.data.division_id) {
        e.data.division_id.forEach((dID: string) => {
          const div = org.divisions.find((d: any) => d.id === dID);
          if (div) {
            divs.push(div);
          }
        });
      }

      setEditData({
        id: e.data.id,
        name: e.data.name ?? "",
        validity: e.data.validity ?? {duration: 0, time_unit: ""},
        category: e.data.category ?? "",
        countries: e.data.countries ? Array.from(e.data.countries) : [],
        asset_type_id: e.data.asset_type_id ?? "",
      });

      setShowNewWindow(true);
    },
    [setEditData, organizations]
  );

  // new window
  const [showNewWindow, setShowNewWindow] = useState(false);

  const onShowNewWindow = useCallback(() => {
    setEditData(undefined);
    setShowNewWindow(true);
  }, [setShowNewWindow]);

  const onCloseNewWindow = useCallback(() => setShowNewWindow(false), [setShowNewWindow]);

  // columns
  const [columns, setColumns] = useState<Array<any>>([]);
  const visibleColumns = useColumns(columns);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  useEffect(() => {
    setSelected((selected) =>
      inspectionTypes && selected ? selected.map((s) => inspectionTypes.find((c) => c.id === s.id)) : []
    );
  }, [setSelected, inspectionTypes]);

  // delete
  const [deleteIDs, setDeleteIDs] = useState<string[]>([]);

  const showDeleteConfirm: MouseEventHandler<HTMLButtonElement> = useCallback(() => {
    setDeleteIDs(Array.from(selected.map((s) => s.id)));
  }, [setDeleteIDs, selected]);

  const onDeleteConfirm = useCallback(
    (confirmed: boolean) => {
      if (!confirmed) {
        setDeleteIDs([]);
        return;
      }

      dispatch(deleteInspectionTypes(deleteIDs));
      setDeleteIDs([]);
      setSelected([]);
    },
    [deleteIDs, setDeleteIDs, dispatch]
  );

  // render
  return (
    <Page id="inspectiontypes">
      <Loading inprogress={inspectionTypesListState.loading} />

      <TablePageContent>
        <TableStickyHeader>
          <h1>{t("Untersuchungsarten")}</h1>

          {hasPermission("superadmin:CreateInspectionTypes") && (
            <ToolbarButton
              general
              style={{marginLeft: "auto"}}
              disabled={!organizations}
              onClick={onShowNewWindow}
            >
              <ToolbarButtonIcon>
                <span className="icon-ico_add" />
              </ToolbarButtonIcon>
              {t("Hinzufügen")}
            </ToolbarButton>
          )}

          <ToolbarButton general onClick={refresh}>
            <ToolbarButtonIcon>
              <span className="icon-ico_load" />
            </ToolbarButtonIcon>
            {t("Aktualisieren")}
          </ToolbarButton>
        </TableStickyHeader>

        <TableContainer>
          {error && <Message text={t(error)} error />}
          {!error && (
            <DataTable
              items={inspectionTypes ?? []}
              columns={visibleColumns}
              selected={selected}
              onSelectionChange={onSelectionChange}
              onRowDoubleClick={showEditWndow}
            />
          )}
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 ? (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Eintrag ausgewählt" : "Einträge ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>
              <TableFooterActions>
                {hasPermission("superadmin:DeleteInspectionTypes") && (
                  <ToolbarButton onClick={showDeleteConfirm}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_delete" />
                    </ToolbarButtonIcon>
                    {t("Löschen")}
                  </ToolbarButton>
                )}
              </TableFooterActions>
              <TableFooterRightActions />
            </>
          ) : null}
        </TableStickyFooter>
      </TablePageContent>

      {showNewWindow && (
        <Window onClose={onCloseNewWindow} headline={t("Untersuchungsarten")}>
          {(props) => <InspectionTypeForm editData={editData} {...props} />}
        </Window>
      )}

      {deleteIDs.length ? (
        <ConfirmWindow headline={t("Untersuchungsarten wirklich löschen?")} onConfirm={onDeleteConfirm} />
      ) : null}
    </Page>
  );
};

export default InspectionTypes;
