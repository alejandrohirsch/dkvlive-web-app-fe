import React, {useState, useEffect} from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {TextField, Button, Message, Select, AutocompleteInput} from "dkv-live-frontend-ui";
import {MenuItem} from "@material-ui/core";
import {useFormik} from "formik";
import {
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import httpClient from "services/http";
import {useDispatch} from "react-redux";
import {loadInspectionTypes} from "../state/actions";
import {countries} from "services/countries";

// ----------------------------------------------------------------------------------------------------------
export interface InspectionTypeCreateData {
  id?: string;
  name: string;
  validity: InspectionTypeValidityCreateData;
  category: string;
  countries: string[];
  asset_type_id: string;
}

export interface InspectionTypeValidityCreateData {
  duration: number;
  time_unit: string;
}

// ----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
`;

// const FormTitle = styled.div`
//   font-size: 14px;
//   font-weight: bold;
// `;

// const FormRow = styled.div`
//   display: flex;
//   > * {
//     flex-grow: 1;
//     flex-basis: 100%;
//   }
// `;

// -----------------------------------------------------------------------------------------------------------
interface InspectionTypeFormProps extends WindowContentProps {
  editData?: InspectionTypeCreateData;
}

const InspectionTypeForm: React.FC<InspectionTypeFormProps> = ({
  forceCloseWindow,
  setLoading,
  headline,
  editData,
}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const editMode = editData !== undefined;

  // asset types
  const [assetTypes, setAssetTypes] = useState<Array<any> | null>(null);
  useEffect(() => {
    //retrieve the asset types
    const loadAssetTypes = async () => {
      const [{data}, err] = await httpClient.get("/management/assets/types");
      if (err !== null) {
        // setError("Geräte-Laden fehlgeschlagen");
        return;
      }
      setAssetTypes(data);
    };
    loadAssetTypes();
  }, []);

  // form
  const [error, setError] = useState("");

  const form = useFormik<InspectionTypeCreateData>({
    initialValues: editData ?? {
      name: "",
      validity: {duration: 0, time_unit: "m"},
      category: "",
      countries: [],
      asset_type_id: "",
    },
    onSubmit: (values) => {
      setLoading(true, true, t(editMode ? "wird geändert" : "wird erstellt"));
      setError("");

      const submit = async () => {
        const inspectionType = {
          name: values.name,
          validity: {
            duration: values.validity.duration,
            time_unit: values.validity.time_unit,
          },
          category: values.category,
          countries: values.countries,
          asset_type_id: values.asset_type_id,
        } as InspectionTypeCreateData;
        if (editMode) {
          inspectionType.id = values.id;
        }

        let err;
        if (editMode) {
          [, err] = await httpClient.put(
            `/management/inspections/types/${values.id}`,
            JSON.stringify(inspectionType),
            {
              headers: {
                "Content-Type": "application/json",
              },
            }
          );
        } else {
          [, err] = await httpClient.post("/management/inspections/types", JSON.stringify(inspectionType), {
            headers: {
              "Content-Type": "application/json",
            },
          });
        }

        if (err !== null) {
          setError(editMode ? "Änderung fehlgeschlagen" : "Erstellen fehlgeschlagen");
          setLoading(false);
          form.setSubmitting(false);
          return;
        }

        dispatch(loadInspectionTypes());
        forceCloseWindow();
      };

      submit();
    },
  });

  // render
  return (
    <WindowContentContainer full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {error && <Message style={{marginBottom: 24}} text={t(error)} error />}

          <FormGrid>
            <TextField
              name="name"
              type="text"
              label={t("Name")}
              value={form.values.name}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
              autoFocus
            />
            <TextField
              name="category"
              type="text"
              label={t("Kategorie")}
              value={form.values.category}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              autoFocus
            />
            {assetTypes ? (
              <Select
                name="asset_type_id"
                value={form.values.asset_type_id}
                onChange={form.handleChange}
                label={t("Fahrzeugart")}
                required
              >
                {assetTypes.map((at) => (
                  <MenuItem key={at.id} value={at.id}>
                    {t(at.name)}
                  </MenuItem>
                ))}
              </Select>
            ) : (
              t("Fahrzeugarten werden geladen...")
            )}

            <AutocompleteInput
              value={form.values.countries}
              onChange={(_: any, newValue: any) => {
                if (newValue) {
                  form.setFieldValue("countries", newValue);
                } else {
                  form.setFieldValue("countries", []);
                }
              }}
              multiple
              options={Object.keys(countries)}
              autoHighlight
              getOptionLabel={(option: any) => t(countries[option])}
              renderInput={(params: any) => (
                <TextField
                  {...params}
                  label="Zulassungsländer"
                  variant="outlined"
                  inputProps={{
                    ...params.inputProps,
                    autoComplete: "new-password",
                  }}
                />
              )}
              clearText={t("Leeren")}
              closeText={t("Schließen")}
              loadingText={t("Lädt...")}
              noOptionsText={t("Keine Optionen")}
              openText={t("Öffnen")}
              disabled={form.isSubmitting}
            />

            {/* <FormTitle>{t("Gültigkeit")}</FormTitle>
            <FormRow>
              <TextField
                name="duration"
                type="number"
                label={t("Dauer")}
                value={form.values.validity.duration}
                onChange={(e) =>
                  form.setFieldValue("validity", {
                    ...form.values.validity,
                    duration: Number.parseInt(e.target.value) || 0,
                  })
                }
                data-autofocus
                disabled={form.isSubmitting}
                variant="outlined"
                required
                autoFocus
              />

              <Select
                name="time_unit"
                value={form.values.validity.time_unit}
                onChange={(e) =>
                  form.setFieldValue("validity", {...form.values.validity, time_unit: e.target.value})
                }
                label={t("Zeitraum")}
                required
              >
                <MenuItem value="d">{t("Tage")}</MenuItem>
                <MenuItem value="m">{t("Monate")}</MenuItem>
              </Select>
            </FormRow> */}
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !form.values.name}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default InspectionTypeForm;
