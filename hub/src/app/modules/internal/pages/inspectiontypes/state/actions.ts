import {InspectionTypesThunkResult} from "./module";
import {InspectionType} from "./reducer";
import httpClient from "services/http";
import {
  LoadingInspectionTypesAction,
  LOADING_INSPECTIONTYPES,
  InspectionTypesLoadedAction,
  INSPECTIONTYPES_LOADED,
  LoadingInspectionTypesFailedAction,
  LOADING_INSPECTIONTYPES_FAILED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingInspectionTypesAction(): LoadingInspectionTypesAction {
  return {
    type: LOADING_INSPECTIONTYPES,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingInspectionTypesFailedAction(errorText?: string): LoadingInspectionTypesFailedAction {
  return {
    type: LOADING_INSPECTIONTYPES_FAILED,
    payload: {errorText},
  };
}

// -----------------------------------------------------------------------------------------------------------
function inspectionTypesLoadedAction(items: InspectionType[]): InspectionTypesLoadedAction {
  return {
    type: INSPECTIONTYPES_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadInspectionTypes(): InspectionTypesThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingInspectionTypesAction());

    const [{data}, err] = await httpClient.get("/management/inspections/types");
    if (err !== null) {
      // @todo: log
      dispatch(loadingInspectionTypesFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(inspectionTypesLoadedAction(data));
  };
}

export function loadInspectionTypesIfNeeded(): InspectionTypesThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().inspectionTypes;
    const isLoaded = data.items !== undefined && data.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadInspectionTypes());
  };
}

export function deleteInspectionTypes(ids: string[]): InspectionTypesThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingInspectionTypesAction());

    for (const id of ids) {
      const [, err] = await httpClient.delete(`/management/inspections/types/${id}`);
      if (err !== null) {
        // @todo: log
        dispatch(loadingInspectionTypesFailedAction("Löschen fehlgeschlagen"));
        return;
      }
    }

    dispatch(loadInspectionTypes());
  };
}
