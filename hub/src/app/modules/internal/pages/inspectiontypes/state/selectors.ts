import {InspectionTypesModuleState} from "./module";
import {createSelector} from "@reduxjs/toolkit";

export const inspectionTypesItemsSelector = (state: InspectionTypesModuleState) =>
  state.inspectionTypes.items;

// this.state.
export const inspectionTypesStateSelector = (state: InspectionTypesModuleState) => state.inspectionTypes;

// list
export const inspectionTypesListSelector = createSelector(inspectionTypesItemsSelector, (items) =>
  items ? Object.values(items) : []
);

// inspectionType
export const inspectionTypeSelector = (id: string) =>
  createSelector(inspectionTypesItemsSelector, (items) => (items ? items[id] : undefined));
