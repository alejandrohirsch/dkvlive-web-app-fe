import {createReducer} from "@reduxjs/toolkit";
import {
  LoadingInspectionTypesFailedAction,
  InspectionTypesLoadedAction,
  LOADING_INSPECTIONTYPES,
  LOADING_INSPECTIONTYPES_FAILED,
  INSPECTIONTYPES_LOADED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface InspectionType {
  id: string;
  name: string;
  asset_type_id: string;
  validity: InspectionValidity;
  category: string;
  countries: string[];
  organization_id: string;
  division_id: string;
  created_at: string;
  modified_at: string;
}

export interface InspectionValidity {
  duration: number;
  time_unit: string;
}

// -----------------------------------------------------------------------------------------------------------
export interface InspectionTypesState {
  loading: boolean;
  loaded?: boolean;
  error?: boolean;
  errorText?: string;
  items?: {[id: string]: InspectionType};
}

const initialState: InspectionTypesState = {
  loading: true,
};

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingInspectionTypes(state: InspectionTypesState) {
  state.loading = true;
  state.loaded = false;

  if (state.error) {
    delete state.error;
    delete state.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingInspectionTypesFailed(
  state: InspectionTypesState,
  action: LoadingInspectionTypesFailedAction
) {
  state.loading = false;
  state.error = true;
  state.errorText = action.payload.errorText;
}

// -----------------------------------------------------------------------------------------------------------
export function handleInspectionTypesLoaded(
  state: InspectionTypesState,
  action: InspectionTypesLoadedAction
) {
  state.loading = false;
  state.loaded = true;

  const items = {};
  action.payload.forEach((a) => {
    items[a.id] = a;
  });

  state.items = items;

  if (state.error) {
    delete state.error;
    delete state.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export const inspectionTypesReducer = createReducer(initialState, {
  [LOADING_INSPECTIONTYPES]: handleLoadingInspectionTypes,
  [LOADING_INSPECTIONTYPES_FAILED]: handleLoadingInspectionTypesFailed,
  [INSPECTIONTYPES_LOADED]: handleInspectionTypesLoaded,
});
