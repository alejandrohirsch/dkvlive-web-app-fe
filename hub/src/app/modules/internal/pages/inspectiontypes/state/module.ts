import {IModule} from "redux-dynamic-modules";
import {InspectionTypesActions} from "./types";
import {ThunkAction} from "redux-thunk";
import {InspectionTypesState, inspectionTypesReducer} from "./reducer";

export interface InspectionTypesModuleState {
  inspectionTypes: InspectionTypesState;
}

export type InspectionTypesThunkResult<R> = ThunkAction<
  R,
  InspectionTypesModuleState,
  void,
  InspectionTypesActions
>;

export const InspectionTypesModule: IModule<InspectionTypesModuleState> = {
  id: "inspectionTypes",
  reducerMap: {
    inspectionTypes: inspectionTypesReducer,
  },
};
