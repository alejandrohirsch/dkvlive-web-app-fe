import {InspectionType} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_INSPECTIONTYPES = "inspectiontypes/loading";

export interface LoadingInspectionTypesAction {
  type: typeof LOADING_INSPECTIONTYPES;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_INSPECTIONTYPES_FAILED = "inspectiontypes/loadingFailed";

export interface LoadingInspectionTypesFailedAction {
  type: typeof LOADING_INSPECTIONTYPES_FAILED;
  payload: {
    errorText?: string;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const INSPECTIONTYPES_LOADED = "inspectiontypes/loaded";

export interface InspectionTypesLoadedAction {
  type: typeof INSPECTIONTYPES_LOADED;
  payload: InspectionType[];
}

// -----------------------------------------------------------------------------------------------------------
export type InspectionTypesActions =
  | LoadingInspectionTypesAction
  | LoadingInspectionTypesFailedAction
  | InspectionTypesLoadedAction;
