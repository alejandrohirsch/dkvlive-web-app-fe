import React, {useState, useEffect, useCallback, useRef} from "react";
import Page from "app/components/Page";
import {useTranslation} from "react-i18next";
import DataTable, {
  TablePageContent,
  useColumns,
  TableStickyHeader,
  TableStickyFooter,
  TableContainer,
  TableFooterActions,
  ToolbarButton,
  ToolbarButtonIcon,
  TableSelectionCountInfo,
  TableFooterRightActions,
  bodyField,
} from "app/components/DataTable";
import {Button, Loading, Message} from "dkv-live-frontend-ui";
import Window from "app/components/Window";
import httpClient from "services/http";
import dayjs from "dayjs";
import {resolveUserDateFormat, hasPermission} from "app/modules/login/state/login/selectors";
import ShipmentDetails from "./components/ShipmentDetails";
import ConfirmWindow from "app/components/ConfirmWindow";
import {Growl} from "primereact/growl";

// -----------------------------------------------------------------------------------------------------------
const listColumns = [
  {
    field: "shipment_number",
    header: "Liefernummer",
    body: (s: any) => (
      <div
        style={{
          color: s._delivered_at
            ? undefined
            : s._shipped_at
            ? "var(--dkv-highlight-ok-color)"
            : "var(--dkv-highlight-warning-color)",
          fontWeight: "bold",
        }}
      >
        {s.shipment_number}
      </div>
    ),
    width: 120,
  },
  {
    field: "order_number",
    header: "Bestellnummer",
    width: 120,
  },
  {
    field: "customer.name",
    header: "Kunde",
    width: 150,
    body: bodyField((d: any) => d.customer.name),
  },
  {
    field: "_created_at",
    header: "Erstellt Am",
    width: 150,
  },
  {
    field: "_confirmed_at",
    header: "Bestätigt Am",
    width: 150,
  },
  {
    field: "_shipped_at",
    header: "Versendet Am",
    width: 150,
  },
  {
    field: "_delivered_at",
    header: "Zugestellt Am",
    width: 150,
  },
];

const Shipments: React.FC = () => {
  const {t} = useTranslation();
  const growlRef = useRef<any>();

  // columns
  const [columns] = useState(listColumns);
  const visibleColumns = useColumns(columns);

  // shipments
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [shipments, setShipments] = useState<any>([]);

  const load = async () => {
    setLoading(true);
    setError("");

    const [{data}, err] = await httpClient.get("/management/shipments");
    if (err !== null) {
      setError("Lieferungen konnten nicht geladen werden.");
      setLoading(false);
      setShipments([]);
      return;
    }

    setShipments(
      data.map((d: any) => {
        d._created_at =
          d.created_at && d.created_at !== "0001-01-01T00:00:00Z"
            ? dayjs(d.created_at).format(resolveUserDateFormat())
            : null;
        d._modified_at =
          d.modified_at && d.modified_at !== "0001-01-01T00:00:00Z"
            ? dayjs(d.modified_at).format(resolveUserDateFormat())
            : null;

        d._confirmed_at =
          d.confirmed_at && d.confirmed_at !== "0001-01-01T00:00:00Z"
            ? dayjs(d.confirmed_at).format(resolveUserDateFormat())
            : null;

        d._shipped_at =
          d.shipped_at && d.shipped_at !== "0001-01-01T00:00:00Z"
            ? dayjs(d.shipped_at).format(resolveUserDateFormat())
            : null;

        d._delivered_at =
          d.delivered_at && d.delivered_at !== "0001-01-01T00:00:00Z"
            ? dayjs(d.delivered_at).format(resolveUserDateFormat())
            : null;

        d._shipping_time =
          d.shipping_info.shipping_time && d.shipping_info.shipping_time !== "0001-01-01T00:00:00Z"
            ? dayjs(d.shipping_info.shipping_time).format(resolveUserDateFormat())
            : null;

        return d;
      })
    );
    setLoading(false);
  };

  useEffect(() => {
    load();
  }, []);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  useEffect(() => {
    setSelected((selected) =>
      shipments
        ? selected.map((s) => shipments.find((t: any) => t.id === s.id)).filter((s: any) => s !== undefined)
        : []
    );
  }, [setSelected, shipments]);

  // detail
  const [showDetailWindow, setShowDetailWindow] = useState<any>(null);
  const onCloseDetailWindow = useCallback(() => setShowDetailWindow(null), [setShowDetailWindow]);
  const openDetailWindow = useCallback((e: any) => setShowDetailWindow(e.data), [setShowDetailWindow]);

  const openDetailsWindowForSelected = useCallback(
    () => selected.length && selected[0] && setShowDetailWindow(selected[0]),
    [selected, setShowDetailWindow]
  );

  // delete
  const [deleteID, setDeleteID] = useState("");

  const showDeleteConfirm = useCallback(() => {
    if (!selected || !selected.length) {
      return;
    }

    setDeleteID(selected[0].id);
  }, [setDeleteID, selected]);

  const onDeleteConfirm = useCallback(
    (confirmed: boolean) => {
      if (!confirmed) {
        setDeleteID("");
        return;
      }

      const deleteShipment = async () => {
        setLoading(true);
        const [, err] = await httpClient.delete(`/management/shipments/deleteByID/${deleteID}`);
        if (err !== null) {
          growlRef.current.show({
            life: 5000,
            closable: false,
            severity: "error",
            summary: "Lieferung",
            detail: err.response?.data?.message
              ? err.response?.data?.message
              : typeof err === "string"
              ? err
              : t("Lieferung konnte nicht gelöscht werden"),
          });
          setLoading(false);
          return;
        }

        setDeleteID("");
        load();
      };

      deleteShipment();
    },
    [deleteID, setDeleteID, t]
  );

  // render
  return (
    <Page id="shipments">
      <Loading inprogress={loading} />

      <TablePageContent>
        <TableStickyHeader>
          <h1>{t("Lieferungen")}</h1>

          {/* <SearchTextField
            name="search"
            type="text"
            required
            label={t("Suche")}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchIcon className="icon-ico_search" />
                </InputAdornment>
              ),
            }}
          /> */}

          <ToolbarButton general onClick={load} style={{marginLeft: "auto"}}>
            <ToolbarButtonIcon>
              <span className="icon-ico_load" />
            </ToolbarButtonIcon>
            {t("Aktualisieren")}
          </ToolbarButton>
        </TableStickyHeader>

        <TableContainer>
          {error && <Message text={t(error)} error />}

          <DataTable
            items={shipments}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            onRowDoubleClick={openDetailWindow}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 && (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Eintrag ausgewählt" : "Einträge ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>

              {selected.length === 1 && (
                <TableFooterActions>
                  <ToolbarButton onClick={openDetailsWindowForSelected}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_billings" />
                    </ToolbarButtonIcon>
                    {t("Details")}
                  </ToolbarButton>

                  {hasPermission("superadmin:DeleteShipments") &&
                  selected[0].confirmed_at === "0001-01-01T00:00:00Z" ? (
                    <ToolbarButton onClick={showDeleteConfirm}>
                      <ToolbarButtonIcon>
                        <span className="icon-ico_delete" />
                      </ToolbarButtonIcon>
                      {t("Löschen")}
                    </ToolbarButton>
                  ) : null}
                </TableFooterActions>
              )}
              <TableFooterRightActions />
            </>
          )}
        </TableStickyFooter>
      </TablePageContent>

      <Growl ref={(el) => (growlRef.current = el)} baseZIndex={20000} />

      {showDetailWindow && (
        <Window onClose={onCloseDetailWindow} headline={t("Lieferung")}>
          {(props) => <ShipmentDetails {...props} shipment={showDetailWindow} />}
        </Window>
      )}

      {deleteID && <ConfirmWindow headline={t("Lieferung wirklich löschen?")} onConfirm={onDeleteConfirm} />}
    </Page>
  );
};

export default Shipments;
