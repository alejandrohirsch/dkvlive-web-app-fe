import React, {useState} from "react";
import {
  WindowContentContainer,
  WindowHeadline,
  WindowTabsContainer,
  WindowTabs,
  WindowContent,
  WindowContentProps,
} from "app/components/Window";
import {Tab} from "dkv-live-frontend-ui";
import styled, {css} from "styled-components";
import {useTranslation} from "react-i18next";

// -----------------------------------------------------------------------------------------------------------
export const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

export const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

export const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);
  text-align: right;

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

const ShipmentsContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 24px;
`;

const DeviceListHeader = styled.div`
  font-size: 18px;
  color: var(--dkv-grey_50);
  margin-bottom: 15px;
`;

const DeviceList = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const DeviceListEntry = styled.div`
  display: flex;
  flex-basis: 50%;
  padding: 5px 0;
`;

const DeviceListEntryIcon = styled.div`
  margin: auto 0;
  margin-right: 5px;
  font-size: 24px;
`;

const DeviceListEntryText = styled.div`
  margin: auto 0;
  color: var(--dkv-grey_90);
  vertical-align: bottom;
`;

// -----------------------------------------------------------------------------------------------------------
interface ShipmentDetailsProps extends WindowContentProps {
  shipment: any;
}

const ShipmentDetails: React.FC<ShipmentDetailsProps> = ({shipment}) => {
  const {t} = useTranslation();

  // tab
  const [tab, setTab] = useState<number>(0);

  const handleTabChange = (_: React.ChangeEvent<unknown>, newTab: number) => {
    setTab(newTab);
  };

  // render
  return (
    <WindowContentContainer minWidth="750px" minHeight="500px" full>
      <WindowHeadline padding>
        <h1>
          {t("Lieferung")} - {shipment.shipment_number}
        </h1>
      </WindowHeadline>

      <WindowTabsContainer>
        <WindowTabs value={tab} onChange={handleTabChange} variant="scrollable">
          <Tab label={t("Übersicht")} />
          <Tab label={t("Items")} />
          <Tab label={t("Empfänger")} />
          {shipment.device_serials && shipment.device_serials.length && <Tab label={t("Versandte Geräte")} />}
        </WindowTabs>
      </WindowTabsContainer>

      <WindowContent padding>
        {tab === 0 && (
          <ShipmentsContainer>
            <Table>
              <tbody>
                <tr>
                  <TableLabel>{t("Liefernummer")}:</TableLabel>
                  <TableValue>{shipment.shipment_number}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Bestellnummer")}:</TableLabel>
                  <TableValue>{shipment.order_number}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Referenz")}:</TableLabel>
                  <TableValue>{shipment.reference}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Kunde")}:</TableLabel>
                  <TableValue>
                    {shipment.customer.name} ({shipment.customer.customer_number})
                  </TableValue>
                </tr>
                <tr>
                  <td colSpan={2}>&nbsp;</td>
                </tr>
                <tr>
                  <TableLabel>{t("Erstellt am")}:</TableLabel>
                  <TableValue>{shipment._created_at}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Geändert am")}:</TableLabel>
                  <TableValue>{shipment._modified_at}</TableValue>
                </tr>
              </tbody>
            </Table>

            <Table style={{marginLeft: 12}}>
              <tbody>
                <tr>
                  <TableLabel>{t("Bestätigt Am")}:</TableLabel>
                  <TableValue>{shipment._confirmed_at}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Versendet Am")}:</TableLabel>
                  <TableValue>{shipment._shipped_at}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Zugestellt Am")}:</TableLabel>
                  <TableValue>{shipment._delivered_at}</TableValue>
                </tr>
                <tr>
                  <td colSpan={2}>&nbsp;</td>
                </tr>
                <tr>
                  <TableLabel>{t("Lieferunternehmen")}:</TableLabel>
                  <TableValue>{shipment.shipping_info.shipper}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Trackingnummer")}:</TableLabel>
                  <TableValue>{shipment.shipping_info.tracking_id}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Versandzeit (Shipper)")}:</TableLabel>
                  <TableValue>{shipment._shipping_time}</TableValue>
                </tr>
                <tr>
                  <td colSpan={2}>&nbsp;</td>
                </tr>
                <tr>
                  <TableLabel>{t("Zustellreferenz")}:</TableLabel>
                  <TableValue>{shipment.delivery_reference}</TableValue>
                </tr>
                <tr>
                  <TableLabel>{t("Zugestellt an")}:</TableLabel>
                  <TableValue>{shipment.receiver}</TableValue>
                </tr>
              </tbody>
            </Table>
          </ShipmentsContainer>
        )}

        {tab === 1 && (
          <ShipmentsContainer>
            {shipment.devices.map((i: any) => (
              <Table key={i.type} style={{marginBottom: 12}}>
                <tbody>
                  <tr>
                    <TableLabel>{t("Artikel")}:</TableLabel>
                    <TableValue>{i.type.toUpperCase()}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Anzahl")}:</TableLabel>
                    <TableValue>{i.quantity}</TableValue>
                  </tr>
                </tbody>
              </Table>
            ))}

            {shipment.accessories.map((i: any) => (
              <Table key={i.article} style={{marginBottom: 12}}>
                <tbody>
                  <tr>
                    <TableLabel>{t("Artikel")}:</TableLabel>
                    <TableValue>{i.article}</TableValue>
                  </tr>
                  <tr>
                    <TableLabel>{t("Anzahl")}:</TableLabel>
                    <TableValue>{i.quantity}</TableValue>
                  </tr>
                </tbody>
              </Table>
            ))}
          </ShipmentsContainer>
        )}

        {tab === 2 && (
          <Table>
            <tbody>
              <tr>
                <TableLabel>{t("Firmenname")}:</TableLabel>
                <TableValue>{shipment.shipping_recipient.company_name}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Adresse")}:</TableLabel>
                <TableValue>
                  {shipment.shipping_recipient.address.country_code}-{shipment.shipping_recipient.address.zip}{" "}
                  {shipment.shipping_recipient.address.place} {shipment.shipping_recipient.address.street}{" "}
                  {shipment.shipping_recipient.address.house_number}
                </TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Adresszusatz")}:</TableLabel>
                <TableValue>{shipment.shipping_recipient.address.address_addition}</TableValue>
              </tr>
              <tr>
                <td colSpan={2}>&nbsp;</td>
              </tr>
              <tr>
                <TableLabel>{t("E-Mail")}:</TableLabel>
                <TableValue>{shipment.shipping_recipient.contact.email}</TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Name")}:</TableLabel>
                <TableValue>
                  {shipment.shipping_recipient.contact.last_name}{" "}
                  {shipment.shipping_recipient.contact.first_name}
                </TableValue>
              </tr>
              <tr>
                <TableLabel>{t("Telefon")}:</TableLabel>
                <TableValue>{shipment.shipping_recipient.contact.phone}</TableValue>
              </tr>
            </tbody>
          </Table>
        )}

        {tab === 3 && (
          <div>
            <DeviceListHeader>{t("Seriennummern")}</DeviceListHeader>
            <DeviceList>
              {shipment.device_serials && shipment.device_serials.length ? (
                shipment.device_serials.map((d: any) => (
                  <DeviceListEntry key={d}>
                    <DeviceListEntryIcon className="icon-ico_tollbox" />
                    <DeviceListEntryText>{d}</DeviceListEntryText>
                  </DeviceListEntry>
                ))
              ) : (
                <DeviceListEntry>{t("Keine Geräte gefunden...")}</DeviceListEntry>
              )}
            </DeviceList>
          </div>
        )}
      </WindowContent>
    </WindowContentContainer>
  );
};

export default ShipmentDetails;
