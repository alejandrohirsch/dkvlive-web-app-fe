import {NamespacesActions} from "./namespaces/types";
import {TranslationsActions} from "./translations/types";

export type I18nActionTypes = NamespacesActions | TranslationsActions;
