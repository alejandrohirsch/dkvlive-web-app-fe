import {Translation} from "./reducer";
import {Namespace} from "../namespaces/reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_TRANSLATIONS = "i18n/loadingTranslations";

export interface LoadingTranslationsAction {
  type: typeof LOADING_TRANSLATIONS;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_TRANSLATIONS_FAILED = "i18n/loadingTranslationsFailed";

export interface LoadingTranslationsFailedAction {
  type: typeof LOADING_TRANSLATIONS_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const TRANSLATIONS_LOADED = "i18n/translationsLoaded";

export interface TranslationsLoadedAction {
  type: typeof TRANSLATIONS_LOADED;
  payload: {items: Translation[]; namespace: Namespace};
}

// -----------------------------------------------------------------------------------------------------------
export type TranslationsActions =
  | LoadingTranslationsAction
  | LoadingTranslationsFailedAction
  | TranslationsLoadedAction;
