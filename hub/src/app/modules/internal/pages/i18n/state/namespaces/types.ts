import {Namespace} from "./reducer";

// -----------------------------------------------------------------------------------------------------------
export const LOADING_NAMESPACES = "i18n/loadingNamespaces";

export interface LoadingNamespacesAction {
  type: typeof LOADING_NAMESPACES;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_NAMESPACES_FAILED = "i18n/loadingNamespacesFailed";

export interface LoadingNamespacesFailedAction {
  type: typeof LOADING_NAMESPACES_FAILED;
  payload: {
    errorText?: string;
    unsetItems?: boolean;
  };
}

// -----------------------------------------------------------------------------------------------------------
export const NAMESPACES_LOADED = "i18n/namespacesLoaded";

export interface NamespacesLoadedAction {
  type: typeof NAMESPACES_LOADED;
  payload: Namespace[];
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_NAMESPACE = "i18n/loadingNamespace";

export interface LoadingNamespaceAction {
  type: typeof LOADING_NAMESPACE;
}

// -----------------------------------------------------------------------------------------------------------
export const LOADING_NAMESPACE_FAILED = "i18n/loadingNamespaceFailed";

export interface LoadingNamespaceFailedAction {
  type: typeof LOADING_NAMESPACE_FAILED;
  payload: string;
}

// -----------------------------------------------------------------------------------------------------------
export const NAMESPACE_LOADED = "i18n/namespaceLoaded";

export interface NamespaceLoadedAction {
  type: typeof NAMESPACE_LOADED;
  payload: Namespace;
}

// -----------------------------------------------------------------------------------------------------------
export type NamespacesActions =
  | LoadingNamespacesAction
  | LoadingNamespacesFailedAction
  | NamespacesLoadedAction
  | LoadingNamespaceAction
  | LoadingNamespaceFailedAction
  | NamespaceLoadedAction;
