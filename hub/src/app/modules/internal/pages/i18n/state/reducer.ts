import {createReducer} from "@reduxjs/toolkit";
import {
  NAMESPACES_LOADED,
  LOADING_NAMESPACES,
  LOADING_NAMESPACES_FAILED,
  LOADING_NAMESPACE,
  LOADING_NAMESPACE_FAILED,
  NAMESPACE_LOADED,
} from "./namespaces/types";
import {
  handleNamespacesLoaded,
  handleLoadingNamespaces,
  NamespacesData,
  handleLoadingNamespacesFailed,
  handleLoadingNamespace,
  handleLoadingNamespaceFailed,
  handleNamespaceLoaded,
} from "./namespaces/reducer";
import {TRANSLATIONS_LOADED, LOADING_TRANSLATIONS, LOADING_TRANSLATIONS_FAILED} from "./translations/types";
import {
  handleTranslationsLoaded,
  handleLoadingTranslations,
  TranslationsData,
  handleLoadingTranslationsFailed,
} from "./translations/reducer";

// -----------------------------------------------------------------------------------------------------------
export interface I18nState {
  namespaces: NamespacesData;
  translations: TranslationsData;
}

const initialState: I18nState = {
  namespaces: {
    list: {loading: true},
    item: {loading: true},
  },
  translations: {
    list: {loading: true},
  },
};

export const reducer = createReducer(initialState, {
  // namespace
  [LOADING_NAMESPACES]: handleLoadingNamespaces,
  [LOADING_NAMESPACES_FAILED]: handleLoadingNamespacesFailed,
  [NAMESPACES_LOADED]: handleNamespacesLoaded,
  [LOADING_NAMESPACE]: handleLoadingNamespace,
  [LOADING_NAMESPACE_FAILED]: handleLoadingNamespaceFailed,
  [NAMESPACE_LOADED]: handleNamespaceLoaded,

  // translations
  [LOADING_TRANSLATIONS]: handleLoadingTranslations,
  [LOADING_TRANSLATIONS_FAILED]: handleLoadingTranslationsFailed,
  [TRANSLATIONS_LOADED]: handleTranslationsLoaded,
});
