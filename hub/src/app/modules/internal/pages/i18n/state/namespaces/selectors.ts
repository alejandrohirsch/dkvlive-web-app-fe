import {I18nModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const namespacesItemsSelector = (state: I18nModuleState) => state.i18n.namespaces.items;

// list
export const namespacesListStateSelector = (state: I18nModuleState) => state.i18n.namespaces.list;

export const namespacesListSelector = createSelector(namespacesItemsSelector, (items) =>
  items ? Object.values(items) : undefined
);

// item
export const namespacesItemStateSelector = (state: I18nModuleState) => state.i18n.namespaces.item;

export const namespaceSelector = (id: string) =>
  createSelector(namespacesItemsSelector, (items) => (items ? items[id] : undefined));
