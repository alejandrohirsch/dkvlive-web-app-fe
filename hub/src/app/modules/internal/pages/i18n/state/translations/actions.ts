import {I18nThunkResult} from "../module";
import {Translation} from "./reducer";
import httpClient from "services/http";
import {
  LoadingTranslationsAction,
  LOADING_TRANSLATIONS,
  TranslationsLoadedAction,
  TRANSLATIONS_LOADED,
  LoadingTranslationsFailedAction,
  LOADING_TRANSLATIONS_FAILED,
} from "./types";
import {Namespace} from "../namespaces/reducer";

// -----------------------------------------------------------------------------------------------------------
function loadingTranslationsAction(): LoadingTranslationsAction {
  return {
    type: LOADING_TRANSLATIONS,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingTranslationsFailedAction(
  errorText?: string,
  unsetItems = true
): LoadingTranslationsFailedAction {
  return {
    type: LOADING_TRANSLATIONS_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function translationsLoadedAction(items: Translation[], namespace: Namespace): TranslationsLoadedAction {
  return {
    type: TRANSLATIONS_LOADED,
    payload: {items, namespace},
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadTranslations(namespaceID: string): I18nThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingTranslationsAction());

    const [{data}, err] = await httpClient.get(`/i18n/translations/findByNamespace/${namespaceID}`);
    if (err !== null) {
      // @todo: log
      dispatch(loadingTranslationsFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(translationsLoadedAction(data.translations, data.namespace));
  };
}

export function deleteTranslations(ids: string[], namespace: string): I18nThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingTranslationsAction());

    for (const id of ids) {
      const [, err] = await httpClient.delete(`/i18n/translations/${id}`);
      if (err !== null) {
        // @todo: log
        dispatch(loadingTranslationsFailedAction("Löschen fehlgeschlagen"));
        return;
      }
    }

    dispatch(loadTranslations(namespace));
  };
}
