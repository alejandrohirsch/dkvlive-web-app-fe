import {I18nThunkResult} from "../module";
import {Namespace} from "./reducer";
import httpClient from "services/http";
import {
  LoadingNamespacesAction,
  LOADING_NAMESPACES,
  NamespacesLoadedAction,
  NAMESPACES_LOADED,
  LoadingNamespacesFailedAction,
  LOADING_NAMESPACES_FAILED,
  LoadingNamespaceAction,
  LOADING_NAMESPACE,
  LoadingNamespaceFailedAction,
  LOADING_NAMESPACE_FAILED,
  NamespaceLoadedAction,
  NAMESPACE_LOADED,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
function loadingNamespacesAction(): LoadingNamespacesAction {
  return {
    type: LOADING_NAMESPACES,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingNamespacesFailedAction(errorText?: string, unsetItems = true): LoadingNamespacesFailedAction {
  return {
    type: LOADING_NAMESPACES_FAILED,
    payload: {errorText, unsetItems},
  };
}

// -----------------------------------------------------------------------------------------------------------
function namespacesLoadedAction(items: Namespace[]): NamespacesLoadedAction {
  return {
    type: NAMESPACES_LOADED,
    payload: items,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingNamespaceAction(): LoadingNamespaceAction {
  return {
    type: LOADING_NAMESPACE,
  };
}

// -----------------------------------------------------------------------------------------------------------
function loadingNamespaceFailedAction(errorText?: string): LoadingNamespaceFailedAction {
  return {
    type: LOADING_NAMESPACE_FAILED,
    payload: errorText ?? "",
  };
}

// -----------------------------------------------------------------------------------------------------------
function namespaceLoadedAction(item: Namespace): NamespaceLoadedAction {
  return {
    type: NAMESPACE_LOADED,
    payload: item,
  };
}

// -----------------------------------------------------------------------------------------------------------
export function loadNamespaces(): I18nThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingNamespacesAction());

    const [{data}, err] = await httpClient.get("/i18n/namespaces");
    if (err !== null) {
      // @todo: log
      dispatch(loadingNamespacesFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(namespacesLoadedAction(data));
  };
}

export function loadNamespacesIfNeeded(): I18nThunkResult<Promise<void>> {
  return async (dispatch, getState) => {
    const data = getState().i18n.namespaces;
    const isLoaded = data.items !== undefined && data.list.loaded;
    if (isLoaded) {
      return;
    }

    dispatch(loadNamespaces());
  };
}

export function deleteNamespace(id: string): I18nThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingNamespacesAction());

    const [, err] = await httpClient.delete(`/i18n/namespaces/${id}`);
    if (err !== null) {
      // @todo: log
      dispatch(loadingNamespacesFailedAction("Löschen fehlgeschlagen"));
      return;
    }

    dispatch(loadNamespaces());
  };
}

export function loadNamespace(id: string): I18nThunkResult<Promise<void>> {
  return async (dispatch) => {
    dispatch(loadingNamespaceAction());

    const [{data}, err] = await httpClient.get(`/i18n/namespaces/${id}`);
    if (err !== null) {
      // @todo: log
      dispatch(loadingNamespaceFailedAction("Laden fehlgeschlagen"));
      return;
    }

    dispatch(namespaceLoadedAction(data));
  };
}
