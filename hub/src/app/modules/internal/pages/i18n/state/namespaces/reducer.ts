import {I18nState} from "../reducer";
import {
  NamespacesLoadedAction,
  LoadingNamespacesFailedAction,
  LoadingNamespaceFailedAction,
  NamespaceLoadedAction,
} from "./types";

// -----------------------------------------------------------------------------------------------------------
export interface Namespace {
  id: string;
  key: string;
  label: string;
  created_at: any;
  modified_at: any;
  reference_language: string;
  language_data?: {
    [language: string]: {
      translated: number;
      untranslated: number;
    };
  };
}

export interface NamespacesData {
  list: {
    loading: boolean;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  item: {
    loading: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: string]: Namespace};
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingNamespaces(state: I18nState) {
  const data = state.namespaces.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingNamespacesFailed(state: I18nState, action: LoadingNamespacesFailedAction) {
  const data = state.namespaces;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleNamespacesLoaded(state: I18nState, action: NamespacesLoadedAction) {
  const data = state.namespaces;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.forEach((n) => (items[n.id] = n));
  data.items = items;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingNamespace(state: I18nState) {
  const data = state.namespaces.item;

  data.loading = true;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingNamespaceFailed(state: I18nState, action: LoadingNamespaceFailedAction) {
  const data = state.namespaces.item;

  data.loading = false;
  data.error = true;
  data.errorText = action.payload;
}

// -----------------------------------------------------------------------------------------------------------
export function handleNamespaceLoaded(state: I18nState, action: NamespaceLoadedAction) {
  const data = state.namespaces;

  data.item.loading = false;

  data.items = {...data.items, [action.payload.id]: action.payload};

  if (data.item.error) {
    delete data.item.error;
    delete data.item.errorText;
  }
}
