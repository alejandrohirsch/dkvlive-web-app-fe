import {I18nState} from "../reducer";
import {TranslationsLoadedAction, LoadingTranslationsFailedAction} from "./types";
import {Namespace} from "../namespaces/reducer";

// -----------------------------------------------------------------------------------------------------------
export interface Translation {
  id: string;
  namespace_id: string;
  key: string;
  created_at: string;
  modified_at: string;
  values?: {[lang: string]: string};
  new?: boolean;
}

export interface TranslationsData {
  list: {
    loading: boolean;
    namespace_id?: string;
    loaded?: boolean;
    error?: boolean;
    errorText?: string;
  };
  items?: {[id: string]: Translation};
  namespace?: Namespace;
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingTranslations(state: I18nState) {
  const data = state.translations.list;

  data.loading = true;
  data.loaded = false;

  if (data.error) {
    delete data.error;
    delete data.errorText;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleLoadingTranslationsFailed(state: I18nState, action: LoadingTranslationsFailedAction) {
  const data = state.translations;

  data.list.loading = false;
  data.list.error = true;
  data.list.errorText = action.payload.errorText;

  if (action.payload.unsetItems) {
    data.items = undefined;
  }
}

// -----------------------------------------------------------------------------------------------------------
export function handleTranslationsLoaded(state: I18nState, action: TranslationsLoadedAction) {
  const data = state.translations;

  data.list.loading = false;
  data.list.loaded = true;

  const items = {};
  action.payload.items.forEach((n) => (items[n.id] = n));
  data.items = items;
  data.namespace = action.payload.namespace;

  if (data.list.error) {
    delete data.list.error;
    delete data.list.errorText;
  }
}
