import {I18nModuleState} from "../module";
import {createSelector} from "@reduxjs/toolkit";

export const translationsItemsSelector = (state: I18nModuleState) => state.i18n.translations.items;

// list
export const translationsListStateSelector = (state: I18nModuleState) => state.i18n.translations.list;

export const translationsListSelector = createSelector(translationsItemsSelector, (items) =>
  items ? Object.values(items) : undefined
);

// namespace
export const translationsNamespaceSelector = (state: I18nModuleState) => state.i18n.translations.namespace;
