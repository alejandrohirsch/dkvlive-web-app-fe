import {IModule} from "redux-dynamic-modules";
import {I18nActionTypes} from "./actions";
import {ThunkAction} from "redux-thunk";
import {I18nState, reducer} from "./reducer";

export interface I18nModuleState {
  i18n: I18nState;
}

export type I18nThunkResult<R> = ThunkAction<R, I18nModuleState, void, I18nActionTypes>;

export const I18nModule: IModule<I18nModuleState> = {
  id: "i18n",
  reducerMap: {
    i18n: reducer,
  },
};
