import React from "react";
import store from "app/state/store";
import {I18nModule} from "./state/module";
import {useRouteMatch, Switch, Route} from "react-router-dom";
import NamespacesList from "./NamespacesList";
import Page from "app/components/Page";
import Translations from "./Translations";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
store.addModule(I18nModule);

// -----------------------------------------------------------------------------------------------------------
export const availableLanguages = [
  {value: "de", label: "Deutsch"},
  {value: "en", label: "English"},
  {value: "es", label: "Spanisch"},
  {value: "cz", label: "Tschechisch"},
  {value: "sk", label: "Slowakisch"},
  {value: "ro", label: "Rumänisch"},
];

// -----------------------------------------------------------------------------------------------------------
const I18n: React.FC = () => {
  // route
  const {path} = useRouteMatch();

  // render
  return (
    <Page id="i18n">
      <Switch>
        {hasPermission("i18n:ListNamespaces") && (
          <Route exact path={`${path}`}>
            <NamespacesList path={path} />
          </Route>
        )}

        {hasPermission("i18n:ListTranslations") && (
          <Route exact path={`${path}/:id/:name`}>
            <Translations backPath={path} />
          </Route>
        )}
      </Switch>
    </Page>
  );
};

export default I18n;
