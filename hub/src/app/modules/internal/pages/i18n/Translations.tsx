import React, {useState, useCallback, useEffect, MouseEventHandler, useRef} from "react";
import {useTranslation} from "react-i18next";
import Page from "app/components/Page";
import {useParams} from "react-router-dom";
import {Button, Loading} from "dkv-live-frontend-ui";
import DataTable, {
  TablePageContent,
  TableStickyHeader,
  TableContainer,
  TableStickyFooter,
  useColumns,
  ToolbarButton,
  ToolbarButtonIcon,
  bodyField,
  TableFooterActions,
  newField,
  TableFooterRightActions,
  TableSelectionCountInfo,
} from "app/components/DataTable";
import Window from "app/components/Window";
import TranslationForm, {TranslationEditData} from "./components/TranslationForm";
import {Translation} from "./state/translations/reducer";
import {
  translationsListStateSelector,
  translationsListSelector,
  translationsNamespaceSelector,
} from "./state/translations/selectors";
import {useSelector, useDispatch} from "react-redux";
import {loadTranslations, deleteTranslations} from "./state/translations/actions";
import {clone} from "lodash";
import ConfirmWindow from "app/components/ConfirmWindow";
import {availableLanguages} from "./I18n";
import httpClient from "services/http";
import fileDownload from "js-file-download";
import {Growl} from "primereact/growl";
import TranslationsImport from "./components/TranslationsImport";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const listColumns = [
  {field: "key", header: "Key", width: 350, sortable: false, body: newField},
  {
    field: "_de",
    header: "Deutsch",
    width: 400,
    sortable: false,
    body: bodyField((t: Translation) => (t.values ? t.values.de : "")),
  },
  {
    field: "_en",
    header: "Englisch",
    width: 400,
    sortable: false,
    body: bodyField((t: Translation) => (t.values ? t.values.en : "")),
  },
  {
    field: "_es",
    header: "Spanisch",
    width: 400,
    sortable: false,
    body: bodyField((t: Translation) => (t.values ? t.values.es : "")),
  },
  {
    field: "_cz",
    header: "Tschechisch",
    width: 400,
    sortable: false,
    body: bodyField((t: Translation) => (t.values ? t.values.cz : "")),
  },
  {
    field: "_sk",
    header: "Slowakisch",
    width: 400,
    sortable: false,
    body: bodyField((t: Translation) => (t.values ? t.values.sk : "")),
  },
  {
    field: "_ro",
    header: "Rumänisch",
    width: 400,
    sortable: false,
    body: bodyField((t: Translation) => (t.values ? t.values.ro : "")),
  },
];

// -----------------------------------------------------------------------------------------------------------
interface TranslationsProps {
  backPath: string;
}

const Translations: React.FC<TranslationsProps> = ({backPath}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const {id = "", name = ""} = useParams();

  // translations
  const namespacesListState = useSelector(translationsListStateSelector);
  const translations = useSelector(translationsListSelector);
  const namespace = useSelector(translationsNamespaceSelector);

  useEffect(() => {
    dispatch(loadTranslations(id));
  }, [dispatch, id]);

  const refresh = useCallback(() => dispatch(loadTranslations(id)), [dispatch, id]);

  // edit
  const [editData, setEditData] = useState<TranslationEditData | undefined>();

  const showEditWndow = useCallback(
    (e: any) => {
      if (!hasPermission("i18n:EditTranslation")) {
        return;
      }

      const defaultValues = {};
      availableLanguages.forEach((l) => (defaultValues[l.value] = ""));

      setEditData({
        id: e.data.id,
        key: e.data.key,
        values: e.data.values ? clone(e.data.values) : defaultValues,
      });
      setShowNewWindow(true);
    },
    [setEditData]
  );

  // new window
  const [showNewWindow, setShowNewWindow] = useState(false);

  const onShowNewWindow = useCallback(() => {
    setEditData(undefined);
    setShowNewWindow(true);
  }, [setShowNewWindow]);

  const onCloseNewWindow = useCallback(() => setShowNewWindow(false), [setShowNewWindow]);

  // columns
  const [columns] = useState(listColumns);
  const visibleColumns = useColumns(columns);

  // selection
  const [selected, setSelected] = useState<any[]>([]);
  const onSelectionChange = (e: any) => setSelected(e.value);
  const unselect = () => setSelected([]);

  useEffect(() => {
    setSelected((selected) =>
      translations ? selected.map((s) => translations.find((t) => s && t.id === s.id)) : []
    );
  }, [setSelected, translations]);

  // delete
  const [deleteIDs, setDeleteIDs] = useState<string[]>([]);

  const showDeleteConfirm: MouseEventHandler<HTMLButtonElement> = useCallback(() => {
    setDeleteIDs(Array.from(selected.map((s) => s.id)));
  }, [setDeleteIDs, selected]);

  const onDeleteConfirm = useCallback(
    (confirmed: boolean) => {
      if (!confirmed) {
        setDeleteIDs([]);
        return;
      }

      dispatch(deleteTranslations(deleteIDs, id));
      setDeleteIDs([]);
    },
    [deleteIDs, setDeleteIDs, dispatch, id]
  );

  // export
  const growlRef = useRef<any>();

  const exportTranslations = () => {
    const exp = async () => {
      const [{data}, err] = await httpClient.get(`/i18n/export/${id}`, {responseType: "blob"});
      if (err !== null) {
        console.error(err);
        growlRef.current.show({
          life: 5000,
          closable: false,
          severity: "error",
          summary: t("Export"),
          detail: err.response?.data?.message
            ? err.response?.data?.message
            : typeof err === "string"
            ? err
            : t("Übersetzungen konnten nicht exportiert werden"),
        });
        return;
      }

      try {
        fileDownload(data, `${namespace ? namespace.key : id}-${new Date().getTime()}.xlsx`);
      } catch (err) {
        console.error(err);
        growlRef.current.show({
          life: 5000,
          closable: false,
          severity: "error",
          summary: t("Export"),
          detail: t("Übersetzungen konnten nicht exportiert werden"),
        });
      }
    };

    exp();
  };

  // import
  const [importWindow, setImportWindow] = useState(false);
  const showImportWindow = () => setImportWindow(true);
  const onCloseImportWindow = () => setImportWindow(false);

  // render
  const rowClassName = (rowData: any) => {
    return {"dkv-row-new": rowData.new === true};
  };

  return (
    <Page id="translations">
      <Loading inprogress={namespacesListState.loading} />

      <TablePageContent>
        <TableStickyHeader backPath={backPath}>
          <h1>{namespace?.label ?? name ?? "-"}</h1>

          {hasPermission("i18n:ExportTranslations") && (
            <ToolbarButton general style={{marginLeft: "auto"}} onClick={exportTranslations}>
              <ToolbarButtonIcon>
                <span className="icon-ico_export" />
              </ToolbarButtonIcon>
              {t("Export")}
            </ToolbarButton>
          )}

          {hasPermission("i18n:ImportTranslations") && (
            <ToolbarButton general onClick={showImportWindow}>
              <ToolbarButtonIcon>
                <span className="icon-ico_document" />
              </ToolbarButtonIcon>
              {t("Import")}
            </ToolbarButton>
          )}

          {hasPermission("i18n:CreateTranslation") && (
            <ToolbarButton general onClick={onShowNewWindow}>
              <ToolbarButtonIcon>
                <span className="icon-ico_add" />
              </ToolbarButtonIcon>
              {t("Hinzufügen")}
            </ToolbarButton>
          )}

          <ToolbarButton general onClick={refresh}>
            <ToolbarButtonIcon>
              <span className="icon-ico_load" />
            </ToolbarButtonIcon>
            {t("Aktualisieren")}
          </ToolbarButton>
        </TableStickyHeader>

        <TableContainer>
          <DataTable
            items={translations ?? []}
            columns={visibleColumns}
            selected={selected}
            onSelectionChange={onSelectionChange}
            onRowDoubleClick={showEditWndow}
            rowClassName={rowClassName}
          />
        </TableContainer>

        <TableStickyFooter>
          {selected.length > 0 ? (
            <>
              <TableSelectionCountInfo
                count={selected.length}
                label={t(selected.length === 1 ? "Eintrag ausgewählt" : "Einträge ausgewählt")}
              />
              <Button autoWidth secondary ariaLabel={t("Abbrechen")} onClick={unselect}>
                {t("Abbrechen")}
              </Button>
              <TableFooterActions>
                {hasPermission("i18n:DeleteTranslation") && (
                  <ToolbarButton onClick={showDeleteConfirm}>
                    <ToolbarButtonIcon>
                      <span className="icon-ico_delete" />
                    </ToolbarButtonIcon>
                    {t("Löschen")}
                  </ToolbarButton>
                )}
              </TableFooterActions>
              <TableFooterRightActions />
            </>
          ) : null}
        </TableStickyFooter>
      </TablePageContent>

      {showNewWindow && (
        <Window onClose={onCloseNewWindow} headline={t("Neue Übersetzung")}>
          {(props) => <TranslationForm editData={editData} namespaceID={id} {...props} />}
        </Window>
      )}

      {deleteIDs.length ? (
        <ConfirmWindow headline={t("Übersetzungen wirklich löschen?")} onConfirm={onDeleteConfirm} />
      ) : null}

      {importWindow && (
        <Window onClose={onCloseImportWindow} headline={t("Import")}>
          {(props) => <TranslationsImport namespaceID={id} growl={growlRef.current} {...props} />}
        </Window>
      )}

      <Growl ref={(el) => (growlRef.current = el)} baseZIndex={20000} />
    </Page>
  );
};

export default Translations;
