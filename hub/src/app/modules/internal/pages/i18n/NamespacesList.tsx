import React, {useEffect, useCallback, useState, MouseEventHandler} from "react";
import styled, {css} from "styled-components/macro";
import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import {loadNamespacesIfNeeded, loadNamespaces, deleteNamespace} from "./state/namespaces/actions";
import {Loading} from "dkv-live-frontend-ui";
import {namespacesListStateSelector, namespacesListSelector} from "./state/namespaces/selectors";
import {Link} from "react-router-dom";
import Page from "app/components/Page";
import Window from "app/components/Window";
import NamespaceForm, {NamespaceEditData} from "./components/NamespaceForm";
import ConfirmWindow from "app/components/ConfirmWindow";
import {availableLanguages} from "./I18n";
import {
  TablePageContent,
  TableStickyHeader,
  TableContainer,
  ToolbarButton,
  ToolbarButtonIcon,
} from "app/components/DataTable";
import {IconButton} from "@material-ui/core";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
const NamespacesContainer = styled.div`
  display: grid;
  padding-bottom: 32px;
  grid-template-columns: repeat(auto-fill, minmax(320px, 1fr));
  grid-gap: 8px;
`;

const NamespaceBox = styled.div`
  min-width: 320px;
  background-color: var(--dkv-background-primary-color);
  min-height: 0;
  overflow: auto;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.1);
  transition: box-shadow 0.1s linear;
  position: relative;

  &:hover {
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.3);
  }

  a {
    padding: 24px;
    display: flex;
    width: 100%;
    height: 100%;
    flex-direction: column;
  }

  h2 {
    color: var(--dkv-link-primary-color);
    font-size: 1.7142857142857142rem;
    font-weight: 500;
    padding: 0 0 24px 0;
  }
`;

const ActionButtons = styled.div`
  position: absolute;
  top: 8px;
  right: 8px;
`;

const ActionButton = styled(IconButton)`
  width: 28px;
  height: 28px;
  padding: 0 !important;

  span {
    font-size: 20px;
    color: #666666;
  }
`;

export const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

export const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

export const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

// -----------------------------------------------------------------------------------------------------------
interface NamespacesListProps {
  path: string;
}

const NamespacesList: React.FC<NamespacesListProps> = ({path}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // namespaces
  const namespacesListState = useSelector(namespacesListStateSelector);
  const namespaces = useSelector(namespacesListSelector);

  useEffect(() => {
    dispatch(loadNamespacesIfNeeded());
  }, [dispatch]);

  const refresh = useCallback(() => dispatch(loadNamespaces()), [dispatch]);

  // edit
  const [editData, setEditData] = useState<NamespaceEditData | undefined>();

  const showEditWndow: MouseEventHandler<HTMLButtonElement> = useCallback(
    (e) => {
      if (!namespaces) {
        return;
      }

      const id = (e.currentTarget as HTMLButtonElement).dataset.id as string;
      const namespace = namespaces.find((n) => n.id === id);
      if (!namespace) {
        return;
      }

      let language = availableLanguages.find((l) => l.value === namespace.reference_language);
      if (!language) {
        language = availableLanguages[0];
      }

      setEditData({
        id: namespace.id,
        key: namespace.key,
        label: namespace.label,
        referenceLanguage: language.value,
      });
      setShowNewWindow(true);
    },
    [setEditData, namespaces]
  );

  // new window
  const [showNewWindow, setShowNewWindow] = useState(false);

  const onShowNewWindow = useCallback(() => {
    setEditData(undefined);
    setShowNewWindow(true);
  }, [setShowNewWindow]);

  const onCloseNewWindow = useCallback(() => setShowNewWindow(false), [setShowNewWindow]);

  // delete
  const [deleteID, setDeleteID] = useState("");

  const showDeleteConfirm: MouseEventHandler<HTMLButtonElement> = useCallback(
    (e) => {
      setDeleteID((e.currentTarget as HTMLButtonElement).dataset.id as string);
    },
    [setDeleteID]
  );

  const onDeleteConfirm = useCallback(
    (confirmed: boolean) => {
      if (!confirmed) {
        setDeleteID("");
        return;
      }

      dispatch(deleteNamespace(deleteID));
      setDeleteID("");
    },
    [deleteID, setDeleteID, dispatch]
  );

  // render
  return (
    <Page id="namespaceslist">
      <Loading inprogress={namespacesListState.loading} />

      <TablePageContent>
        <TableStickyHeader>
          <h1>{t("Namespaces")}</h1>

          {hasPermission("i18n:CreateNamespace") && (
            <ToolbarButton general style={{marginLeft: "auto"}} onClick={onShowNewWindow}>
              <ToolbarButtonIcon>
                <span className="icon-ico_add" />
              </ToolbarButtonIcon>
              {t("Hinzufügen")}
            </ToolbarButton>
          )}

          <ToolbarButton
            general
            style={{marginLeft: !hasPermission("i18n:CreateNamespace") ? "auto" : undefined}}
            onClick={refresh}
          >
            <ToolbarButtonIcon>
              <span className="icon-ico_load" />
            </ToolbarButtonIcon>
            {t("Aktualisieren")}
          </ToolbarButton>
        </TableStickyHeader>

        <TableContainer>
          {namespaces && (
            <NamespacesContainer>
              {namespaces.map((n) => (
                <NamespaceBox key={n.id}>
                  <Link to={`${path}/${n.id}/${n.label}`}>
                    <h2>{n.label}</h2>
                    <Table>
                      <tbody>
                        <tr>
                          <TableLabel>{t("Key")}:</TableLabel>
                          <TableValue>{n.key}</TableValue>
                        </tr>
                      </tbody>
                      <tbody>
                        <tr>
                          <TableLabel>{t("Referenz Sprache")}:</TableLabel>
                          <TableValue>{n.reference_language}</TableValue>
                        </tr>
                      </tbody>
                    </Table>
                  </Link>
                  <ActionButtons>
                    {hasPermission("i18n:EditNamespace") && (
                      <ActionButton onClick={showEditWndow} data-id={n.id}>
                        <span className="icon-ico_edit" />
                      </ActionButton>
                    )}

                    {hasPermission("i18n:DeleteNamespace") && (
                      <ActionButton onClick={showDeleteConfirm} data-id={n.id}>
                        <span className="icon-ico_delete" />
                      </ActionButton>
                    )}
                  </ActionButtons>
                </NamespaceBox>
              ))}
            </NamespacesContainer>
          )}
        </TableContainer>

        {showNewWindow && (
          <Window onClose={onCloseNewWindow} headline={t(editData ? "Namespace ändern" : "Neuer Namespace")}>
            {(props) => <NamespaceForm editData={editData} {...props} />}
          </Window>
        )}

        {deleteID && (
          <ConfirmWindow headline={t("Namespace wirklich löschen?")} onConfirm={onDeleteConfirm} />
        )}
      </TablePageContent>
    </Page>
  );
};

export default NamespacesList;
