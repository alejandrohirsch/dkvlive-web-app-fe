import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {TextField, Button, Select, Message} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import {availableLanguages} from "../I18n";
import httpClient from "services/http";
import {useDispatch} from "react-redux";
import {loadNamespaces} from "../state/namespaces/actions";
import {MenuItem} from "@material-ui/core";

// -----------------------------------------------------------------------------------------------------------
export interface NamespaceEditData {
  id?: string;
  key: string;
  label: string;
  referenceLanguage: string;
}

// ----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
`;

// -----------------------------------------------------------------------------------------------------------
interface NamespaceFormProps extends WindowContentProps {
  editData?: NamespaceEditData;
}

const NamespaceForm: React.FC<NamespaceFormProps> = ({forceCloseWindow, setLoading, headline, editData}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const editMode = editData !== undefined;

  // form
  const [error, setError] = useState("");

  const form = useFormik<NamespaceEditData>({
    initialValues: editData ?? {key: "", label: "", referenceLanguage: availableLanguages[0].value},
    onSubmit: (values) => {
      setLoading(true, true, t(editMode ? "wird geändert" : "wird erstellt"));
      setError("");

      const submit = async () => {
        const formData = new FormData();

        formData.append("key", values.key);
        formData.append("label", values.label);
        formData.append("reference_language", values.referenceLanguage);

        if (editMode) {
          formData.append("id", values.id || "");
        }

        let err;
        if (editMode) {
          [, err] = await httpClient.put(`/i18n/namespaces/${values.id}`, formData);
        } else {
          [, err] = await httpClient.post("/i18n/namespaces", formData);
        }

        if (err !== null) {
          setError(editMode ? "Änderung fehlgeschlagen" : "Erstellen fehlgeschlagen");
          setLoading(false);
          form.setSubmitting(false);
          return;
        }

        dispatch(loadNamespaces());
        forceCloseWindow();
      };

      submit();
    },
  });

  // render
  return (
    <WindowContentContainer full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {error && <Message text={t(error)} error />}

          <FormGrid>
            <TextField
              name="key"
              type="text"
              label={t("Key")}
              value={form.values.key}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />

            <TextField
              name="label"
              type="text"
              label={t("Label")}
              value={form.values.label}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
            />

            <Select
              name="referenceLanguage"
              value={form.values.referenceLanguage}
              onChange={form.handleChange}
              label={t("Referenz Sprache")}
            >
              <MenuItem value="de">DE</MenuItem>
            </Select>
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !form.values.label}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default NamespaceForm;
