import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {TextField, Button, Message} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import {availableLanguages} from "../I18n";
import httpClient from "services/http";
import {loadTranslations} from "../state/translations/actions";
import {useDispatch} from "react-redux";

// -----------------------------------------------------------------------------------------------------------
export interface TranslationEditData {
  id?: string;
  key: string;
  values: {[lang: string]: string};
}

// ----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
`;

// -----------------------------------------------------------------------------------------------------------
interface TranslationFormProps extends WindowContentProps {
  namespaceID: string;
  editData?: TranslationEditData;
}

const TranslationForm: React.FC<TranslationFormProps> = ({
  forceCloseWindow,
  setLoading,
  headline,
  editData,
  namespaceID,
}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const editMode = editData !== undefined;

  const defaultValues = {};
  availableLanguages.forEach((lang) => (defaultValues[lang.value] = ""));

  // form
  const [error, setError] = useState("");

  const form = useFormik<TranslationEditData>({
    initialValues: editData ?? {key: "", values: defaultValues},
    onSubmit: (values) => {
      setLoading(true, true, t(editMode ? "wird geändert" : "wird erstellt"));
      setError("");

      const submit = async () => {
        const formData = new FormData();

        formData.append("key", values.key);
        formData.append("namespace_id", namespaceID);
        formData.append("values", JSON.stringify(values.values));

        if (editMode) {
          formData.append("id", values.id || "");
        }

        let err;
        if (editMode) {
          [, err] = await httpClient.put(`/i18n/translations/${values.id}`, formData);
        } else {
          [, err] = await httpClient.post("/i18n/translations", formData);
        }

        if (err !== null) {
          setError(editMode ? "Änderung fehlgeschlagen" : "Erstellen fehlgeschlagen");
          setLoading(false);
          form.setSubmitting(false);
          return;
        }

        dispatch(loadTranslations(namespaceID));
        forceCloseWindow();
      };

      submit();
    },
  });

  const handleValueChange = (e: any) => {
    form.setFieldValue("values", {...form.values.values, [e.target.name]: e.target.value});
  };

  // render
  return (
    <WindowContentContainer full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {error && <Message style={{marginBottom: 24}} text={t(error)} error />}

          <FormGrid>
            <TextField
              name="key"
              type="text"
              label={t("Key")}
              value={form.values.key}
              onChange={form.handleChange}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
              autoFocus
            />

            {availableLanguages.map((lang) => (
              <TextField
                key={lang.value}
                name={lang.value}
                type="text"
                label={t(lang.label)}
                value={form.values.values[lang.value]}
                onChange={handleValueChange}
                disabled={form.isSubmitting}
                variant="outlined"
              />
            ))}
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !form.values.key}>
              {t("Speichern")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default TranslationForm;
