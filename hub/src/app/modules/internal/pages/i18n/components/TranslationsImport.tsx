import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import styled from "styled-components/macro";
import {TextField, Button, Message} from "dkv-live-frontend-ui";
import {useFormik} from "formik";
import {
  WindowContentContainer,
  WindowContent,
  WindowHeadline,
  WindowContentProps,
  WindowActionButtons,
} from "app/components/Window";
import httpClient from "services/http";
import {loadTranslations} from "../state/translations/actions";
import {useDispatch} from "react-redux";

// ----------------------------------------------------------------------------------------------------------
const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
`;

// -----------------------------------------------------------------------------------------------------------
interface TranslationsImportProps extends WindowContentProps {
  namespaceID: string;
  growl: any;
}

const TranslationsImport: React.FC<TranslationsImportProps> = ({
  forceCloseWindow,
  setLoading,
  headline,
  namespaceID,
  growl,
}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  // form
  const [error, setError] = useState("");

  const form = useFormik<any>({
    initialValues: {file: ""},
    onSubmit: (values) => {
      if (!values.file) {
        form.setSubmitting(false);
        setError("Ungültige Datei");
        return;
      }

      if (values.file.size === 0) {
        form.setSubmitting(false);
        setError("Ungültige Datei");
        return;
      }

      if (values.file.type !== "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
        form.setSubmitting(false);
        setError(
          "Ungültige Datei. Muss eine xlsx Datei (application/vnd.openxmlformats-officedocument.spreadsheetml.sheet) sein."
        );
        return;
      }

      setLoading(true, true, t("wird importieren"));
      setError("");

      const submit = async () => {
        const formData = new FormData();
        formData.append("namespace_id", namespaceID);
        formData.append("file", values.file);

        const [, err] = await httpClient.post(`/i18n/import`, formData);
        if (err !== null) {
          setError(
            err.response?.data?.message
              ? err.response?.data?.message
              : typeof err === "string"
              ? err
              : t("Import fehlgeschlagen")
          );
          setLoading(false);
          form.setSubmitting(false);
          return;
        }

        if (growl) {
          growl.show({
            life: 5000,
            closable: false,
            severity: "success",
            summary: t("Import erfolgreich"),
          });
        }

        dispatch(loadTranslations(namespaceID));
        forceCloseWindow();
      };

      submit();
    },
  });

  // render
  return (
    <WindowContentContainer full>
      <WindowHeadline padding>
        <h1>{headline}</h1>
      </WindowHeadline>

      <WindowContent padding>
        <Form onSubmit={form.handleSubmit}>
          {error && <Message style={{marginBottom: 24}} text={t(error)} error />}

          <FormGrid>
            <TextField
              name="key"
              type="file"
              value={form.values.key}
              onChange={(e: any) => {
                form.setFieldValue("file", (e.currentTarget as any).files[0]);
              }}
              data-autofocus
              disabled={form.isSubmitting}
              variant="outlined"
              required
              autoFocus
              inputProps={{accept: ".xlsx"}}
            />
          </FormGrid>

          <WindowActionButtons>
            <Button type="button" secondary onClick={forceCloseWindow}>
              {t("Abbrechen")}
            </Button>

            <Button type="submit" disabled={form.isSubmitting || !form.values.file}>
              {t("Importieren")}
            </Button>
          </WindowActionButtons>
        </Form>
      </WindowContent>
    </WindowContentContainer>
  );
};

export default TranslationsImport;
