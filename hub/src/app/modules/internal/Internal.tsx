import React, {useState, useEffect} from "react";
import {Route, Switch, useRouteMatch, useHistory} from "react-router-dom";
import styled from "styled-components/macro";
import {Loading} from "dkv-live-frontend-ui";
import Nav, {NavItem} from "app/components/Nav";
import config from "config";
import {hasPermission} from "../login/state/login/selectors";

const I18n = React.lazy(() => import("./pages/i18n/I18n"));
const Organizations = React.lazy(() => import("./pages/organization/Organizations"));
const Users = React.lazy(() => import("./pages/organization/Users"));
const Policies = React.lazy(() => import("./pages/organization/Policies"));
const Devices = React.lazy(() => import("./pages/devices/Devices"));
const Orders = React.lazy(() => import("./pages/orders/Orders"));
const InspectionTypes = React.lazy(() => import("./pages/inspectiontypes/InspectionTypes"));
const AssetTypes = React.lazy(() => import("./pages/assettypes/AssetTypes"));
const Companycards = React.lazy(() => import("./pages/companycards/Companycards"));
const Shipments = React.lazy(() => import("./pages/shipments/Shipments"));

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: row;
  overflow: hidden;
`;
// -----------------------------------------------------------------------------------------------------------
const isDEVOrLocal = process.env.NODE_ENV !== "production" || config.VERSION.includes("master");
// -----------------------------------------------------------------------------------------------------------
const Internal = () => {
  // route
  const {path} = useRouteMatch();
  const history = useHistory();

  // nav items
  const [navItems, setNavItems] = useState<NavItem[]>([]);
  useEffect(() => {
    const navItems: NavItem[] = [];

    if (hasPermission("superadmin:ListOrganizations")) {
      navItems.push({
        to: `${path}/organization`,
        label: "Organisationen",
        icon: "icon-ico_network",
      });
    }

    if (hasPermission("superadmin:ListUsers")) {
      navItems.push({
        to: `${path}/users`,
        label: "Benutzer",
        icon: "icon-ico_profil",
      });
    }

    if (hasPermission("superadmin:ListPolicies")) {
      navItems.push({
        to: `${path}/policies`,
        label: "Policies",
        icon: "icon-ico_lock",
      });
    }

    if (hasPermission("superadmin:ListDevices")) {
      navItems.push({
        to: `${path}/devices`,
        label: "Devices",
        icon: "icon-ico_plug",
      });
    }

    if (hasPermission("superadmin:ListCompanyCards")) {
      navItems.push({
        to: `${path}/companycards`,
        label: "Unternehmerkarten",
        icon: "icon-ico_creditcard",
      });
    }

    if (hasPermission("superadmin:ListOrders")) {
      navItems.push({
        to: `${path}/orders`,
        label: "Bestellungen",
        icon: "icon-ico_billings",
      });
    }

    if (hasPermission("superadmin:ListShipments")) {
      navItems.push({
        to: `${path}/shipments`,
        label: "Lieferungen",
        icon: "icon-ico_logout",
      });
    }

    if (hasPermission("superadmin:ListInspectionTypes")) {
      navItems.push({
        to: `${path}/inspectiontypes`,
        label: "Untersuchungen",
        icon: "icon-ico_truck",
      });
    }

    if (hasPermission("superadmin:ListAssetTypes")) {
      navItems.push({
        to: `${path}/assettypes`,
        label: "Fahrzeugarten",
        icon: "icon-ico_truck",
      });
    }

    if (isDEVOrLocal && hasPermission("i18n:ListNamespaces")) {
      navItems.push({
        to: `${path}/i18n`,
        label: "Internationalisierung",
        icon: "icon-ico_globus",
      });
    }
    setNavItems(navItems);

    // check path
    if (window.location.pathname === "/_internal" && navItems.length > 0) {
      history.replace(navItems[0].to);
    }
  }, [path, history]);
  // render
  return (
    <Container>
      <Nav items={navItems} />
      <React.Suspense fallback={<Loading inprogress />}>
        <Switch>
          {hasPermission("superadmin:ListCompanyCards") && (
            <Route path={`${path}/companycards`}>
              <Companycards />
            </Route>
          )}

          {hasPermission("superadmin:ListOrganizations") && (
            <Route path={`${path}/organization`}>
              <Organizations />
            </Route>
          )}

          {hasPermission("superadmin:ListUsers") && (
            <Route path={`${path}/users`}>
              <Users />
            </Route>
          )}

          {hasPermission("superadmin:ListPolicies") && (
            <Route path={`${path}/policies`}>
              <Policies />
            </Route>
          )}

          {hasPermission("superadmin:ListDevices") && (
            <Route path={`${path}/devices`}>
              <Devices />
            </Route>
          )}

          {hasPermission("superadmin:ListOrders") && (
            <Route path={`${path}/orders`}>
              <Orders />
            </Route>
          )}

          {hasPermission("superadmin:ListShipments") && (
            <Route path={`${path}/shipments`}>
              <Shipments />
            </Route>
          )}

          {hasPermission("superadmin:ListInspections") && (
            <Route path={`${path}/inspectiontypes`}>
              <InspectionTypes />
            </Route>
          )}

          {hasPermission("superadmin:ListAssetTypes") && (
            <Route path={`${path}/assettypes`}>
              <AssetTypes />
            </Route>
          )}

          {isDEVOrLocal && hasPermission("i18n:ListNamespaces") ? (
            <Route path={`${path}/i18n`}>{isDEVOrLocal && <I18n />}</Route>
          ) : null}
        </Switch>
      </React.Suspense>
    </Container>
  );
};
export default Internal;
