import React, {useCallback} from "react";
import Window, {
  WindowContentContainer,
  WindowHeadline,
  WindowContent,
  WindowContentGrid,
  WindowButtons,
} from "./Window";
import {Button} from "dkv-live-frontend-ui";
import {useTranslation} from "react-i18next";
import styled from "styled-components";

// -----------------------------------------------------------------------------------------------------------
const ConfirmText = styled.p`
  font-size: 1.14286em;
  line-height: 1.5;
  color: var(--dkv-confirm-window-text-color, #666666);
`;

// -----------------------------------------------------------------------------------------------------------
interface ConfirmWindowProps {
  headline: string;
  onConfirm: (confirmed: boolean) => void;
  closable?: boolean;
  text?: string;
  yesLabel?: string;
  noLabel?: string;
}

const ConfirmWindow: React.FC<ConfirmWindowProps> = ({
  headline,
  onConfirm,
  text,
  closable = true,
  yesLabel = "Ja",
  noLabel = "Nein",
}) => {
  const {t} = useTranslation();

  const onClose = useCallback(() => {
    onConfirm(false);
  }, [onConfirm]);

  const onConfirmClick = useCallback(() => {
    onConfirm(true);
  }, [onConfirm]);

  return (
    <Window onClose={onClose} headline={headline} closable={closable}>
      <WindowContentContainer>
        <WindowHeadline marginBottom={24}>
          <h1>{headline}</h1>
        </WindowHeadline>

        <WindowContent>
          <WindowContentGrid>
            {text && <ConfirmText>{text}</ConfirmText>}

            <WindowButtons>
              {noLabel && (
                <Button onClick={onClose} secondary>
                  {t(noLabel)}
                </Button>
              )}
              <Button onClick={onConfirmClick} data-autofocus>
                {t(yesLabel)}
              </Button>
            </WindowButtons>
          </WindowContentGrid>
        </WindowContent>
      </WindowContentContainer>
    </Window>
  );
};

export default ConfirmWindow;
