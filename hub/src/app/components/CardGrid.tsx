import React from "react";
import styled, {css} from "styled-components/macro";
import {useTranslation} from "react-i18next";
import {Link} from "react-router-dom";
import {IconButton} from "@material-ui/core";

// -----------------------------------------------------------------------------------------------------------
const CardsContainer = styled.div`
  display: grid;
  padding-bottom: 32px;
  grid-template-columns: repeat(auto-fill, minmax(320px, 1fr));
  grid-gap: 8px;
`;

const Card = styled.div`
  min-width: 320px;
  background-color: var(--dkv-background-primary-color);
  min-height: 0;
  overflow: auto;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.1);
  transition: box-shadow 0.1s linear;
  position: relative;
  padding: 24px;
  display: flex;
  width: 100%;
  height: 100%;
  flex-direction: column;

  &:hover {
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.3);
  }

  h2 {
    color: var(--dkv-link-primary-color);
    font-size: 1.7142857142857142rem;
    font-weight: 500;
    padding: 0 0 24px 0;
  }
`;

const ActionButtons = styled.div`
  position: absolute;
  top: 8px;
  right: 8px;
`;

const ActionButton = styled(IconButton)`
  width: 28px;
  height: 28px;
  padding: 0 !important;

  span {
    font-size: 20px;
    color: #666666;
  }
`;

export const Table = styled.table`
  border: 0;
  border-spacing: 0;
  margin: 0;
  background: none;
  border-collapse: collapse;
  border-spacing: 0;
  background-image: none;
  font-size: 1.1428571428571428rem;
`;

export const TableLabel = styled.td`
  color: var(--dkv-grey_50);
  padding-bottom: 8px;
  display: flex;
  align-items: center;
  min-width: max-content;
`;

interface TableValueProps {
  noWrap?: boolean;
}

export const TableValue = styled.td<TableValueProps>`
  padding-bottom: 8px;
  padding-left: 16px;
  color: var(--dkv-grey_90);

  .MuiTextField-root {
    margin-top: 4px;
  }

  ${(props) =>
    props.noWrap &&
    css`
      white-space: nowrap;
      padding-left: 4px;
    `}
`;

// -----------------------------------------------------------------------------------------------------------
interface CardGridProps {
  items: any[];
  headerKey: string;
  props?: {
    label: string;
    key?: string;
    getValue?: (item: any) => void;
  }[];
  cardClassName?: any;
  linkTo: (item: any) => string;
  onEditItem?: (e: any) => void;
  onDeleteItem?: (e: any) => void;
  onSuspendItem?: (e: any) => void;
  onUnsuspendItem?: (e: any) => void;
  isSuspended?: (item: any) => boolean;
}

export const CardGrid: React.FC<CardGridProps> = React.memo(
  ({
    items,
    headerKey,
    props,
    cardClassName,
    linkTo,
    onEditItem,
    onDeleteItem,
    onSuspendItem,
    onUnsuspendItem,
    isSuspended,
  }) => {
    const {t} = useTranslation();

    const handleItemAction = (e: any, action: string) => {
      e.preventDefault();
      if (action === "edit") {
        onEditItem && onEditItem(e);
      } else if (action === "delete") {
        onDeleteItem && onDeleteItem(e);
      } else if (action === "suspend") {
        onSuspendItem && onSuspendItem(e);
      } else if (action === "unsuspend") {
        onUnsuspendItem && onUnsuspendItem(e);
      }
    };

    // render
    return (
      <CardsContainer>
        {items.map((item) => (
          <Link key={item.id} to={linkTo(item)}>
            <Card className={cardClassName}>
              <h2>{item[headerKey]}</h2>
              {props && (
                <Table>
                  {props.map(
                    (prop: {label: string; getValue?: (item: any) => void; key?: string}) =>
                      ((prop.key && item[prop.key]) || prop.getValue) && (
                        <tbody key={item.id + prop.label}>
                          <tr>
                            <TableLabel>{t(prop.label)}:</TableLabel>
                            <TableValue>
                              {prop.getValue ? prop.getValue(item) : prop.key ? item[prop.key] : ""}
                            </TableValue>
                          </tr>
                        </tbody>
                      )
                  )}
                </Table>
              )}
              <ActionButtons>
                {onEditItem !== undefined && (
                  <ActionButton onClick={(e) => handleItemAction(e, "edit")} data-id={item.id}>
                    <span className="icon-ico_edit" />
                  </ActionButton>
                )}
                {onDeleteItem !== undefined && (
                  <ActionButton onClick={(e) => handleItemAction(e, "delete")} data-id={item.id}>
                    <span className="icon-ico_delete" />
                  </ActionButton>
                )}
                {onSuspendItem !== undefined && isSuspended !== undefined && !isSuspended(item) && (
                  <ActionButton onClick={(e) => handleItemAction(e, "suspend")} data-id={item.id}>
                    <span className="icon-ico_lock" />
                  </ActionButton>
                )}
                {onUnsuspendItem !== undefined && isSuspended !== undefined && isSuspended(item) && (
                  <ActionButton onClick={(e) => handleItemAction(e, "unsuspend")} data-id={item.id}>
                    <span className="icon-ico_unlock" />
                  </ActionButton>
                )}
              </ActionButtons>
            </Card>
          </Link>
        ))}
      </CardsContainer>
    );
  }
);

export default CardGrid;
