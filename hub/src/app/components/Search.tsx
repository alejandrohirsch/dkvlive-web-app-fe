import React, {useCallback, useEffect, useRef, useState, MouseEventHandler} from "react";
import {useTranslation} from "react-i18next";
import {Drawer, IconButton} from "@material-ui/core";
import styled from "styled-components/macro";
import {debounce, TextField} from "dkv-live-frontend-ui";
import axios from "axios";
import config from "../../config";
import useOnclickOutside from "react-cool-onclickoutside";
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {assetsListSelector} from "../modules/live/state/assets/selectors";
import Fuse from "fuse.js";
import {showDetails} from "../modules/live/pages/map/state/details/actions";
import {loadAssetsIfNeeded} from "../modules/live/state/assets/actions";
import httpClient from "../../services/http";
import {hasPermission} from "app/modules/login/state/login/selectors";

const SearchDrawer = styled(Drawer)`
  z-index: 5000 !important;
`;

const Container = styled.div`
  padding: 0 48px 64px 96px;
  height: 100%;
  width: 512px;

  @media (max-width: 512px) {
    width: 350px;
  }
`;

const BackButton = styled(IconButton)`
  top: 60px;
  left: -72px;
  padding: 0 !important;
  width: 48px;
  height: 48px;

  span {
    font-size: 28px;
  }
`;

const Actions = styled.div`
  margin-left: auto;
`;

const ActionButton = styled(IconButton)`
  &.MuiIconButton-root {
    margin-left: auto;
    margin-right: 8px;
    background-color: var(--dkv-background-primary-hover-color);
    &:hover {
      background-color: #2e6b90;
      color: #fff;
    }
  }
  padding: 0 !important;
  width: 24px;
  height: 24px;

  span {
    font-size: 16px;
  }
`;

/*
const Headline = styled.div`
  font-size: 1.3rem;
  margin: 32px 0 0;
  color: var(--dkv-text-caption-color);
`;
 */

const ResultContainer = styled.div`
  margin: 32px 0;
`;

const HeadlineResult = styled.div`
  border-bottom: 1px solid var(--dkv-text-primary-color);
  text-transform: uppercase;
  padding: 0 0 5px;
  margin: 0 0 8px;
  font-size: 0.9rem;
`;

const ResultItem = styled.li`
  display: flex;
  padding: 8px 0;
  cursor: pointer;
  outline: 0;

  &:focus {
    background-color: var(--dkv-background-primary-hover-color);
  }

  &:hover {
    background-color: var(--dkv-background-primary-hover-color);
  }
`;

const ResultIcon = styled.span`
  font-size: 22px;
`;

const ResultText = styled.div`
  margin: 0 0 0 16px;
  line-height: 22px;
`;

// -----------------------------------------------------------------------------------------------------------
const autoCompleteCountryList =
  "AUT, BEL, BGR, HRV, CYP, CZE, DNK, EST, FIN, FRA, DEU, GRC, HUN, IRL, " +
  "ITA, LVA, LTU, LUX, MLT, NLD, POL, PRT, ROU, SVK, SVN, ESP, SWE, GBR";

interface SearchProps {
  open: boolean;
  toggleSearch: () => void;
}

const Search: React.FC<SearchProps> = ({open, toggleSearch}) => {
  const {t} = useTranslation();
  const history = useHistory();
  const dispatch = useDispatch();
  const assets = useSelector(assetsListSelector);

  useEffect(() => {
    dispatch(loadAssetsIfNeeded());
  }, [dispatch]);

  const searchContainerRef = useRef<HTMLDivElement>(null);
  useOnclickOutside(() => toggleSearch(), {refs: [searchContainerRef]});

  const [inputValue, setInputValue] = useState("");

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
  };

  // vehicles
  const [vehicles, setVehicles] = useState<any[]>([]);

  const handleClickVehicle: MouseEventHandler<HTMLLIElement> = useCallback(
    (e) => {
      const dataset = (e.currentTarget as any).dataset;

      history.push("/live");
      toggleSearch();

      setTimeout(() => {
        if (dataset.id) {
          window.dispatchEvent(new CustomEvent("dkv-map-show-asset", {detail: dataset.id}));
          dispatch(showDetails(dataset.id));
        }
      }, 1000);
    },
    [history, toggleSearch, dispatch]
  );

  const handleClickVehicleAction: MouseEventHandler<HTMLButtonElement> = useCallback((e) => {
    e.stopPropagation();
  }, []);

  const renderVehicles = () => {
    return (
      <ResultContainer>
        <HeadlineResult>{t("Fahrzeuge")}</HeadlineResult>
        <ul>
          {vehicles.length > 0 &&
            vehicles.slice(0, 5).map((vehicle) => (
              <ResultItem
                tabIndex={0}
                key={vehicle.item.id}
                data-id={vehicle.item.id}
                onClick={handleClickVehicle}
              >
                <ResultIcon className="icon-ico_truck24" />
                <ResultText>
                  {vehicle.item.name} ({vehicle.item.license_plate})
                </ResultText>

                <Actions>
                  <ActionButton aria-label={t("")} onClick={handleClickVehicleAction}>
                    <span className="icon-ico_info" />
                  </ActionButton>
                  <ActionButton aria-label={t("")} onClick={handleClickVehicleAction}>
                    <span className="icon-ico_optional-menu" />
                  </ActionButton>
                </Actions>
              </ResultItem>
            ))}
        </ul>
      </ResultContainer>
    );
  };

  useEffect(() => {
    if (inputValue === "") {
      setVehicles([]);
      return undefined;
    }

    const options = {
      includeScore: true,
      threshold: 0.3,
      keys: ["license_plate", "name", "tags.name"],
    };

    const fuse = new Fuse(assets, options);
    const result = fuse.search(inputValue);

    setVehicles(result || []);
    return;
  }, [assets, inputValue]);

  // places
  const [places, setPlaces] = useState<any[]>([]);

  const handleClickPlace: MouseEventHandler<HTMLLIElement> = useCallback(
    (e) => {
      const dataset = (e.currentTarget as any).dataset;

      if (dataset.id) {
        history.push("/live");
        toggleSearch();

        setTimeout(() => {
          window.dispatchEvent(
            new CustomEvent("dkv-map-show-place", {
              detail: {id: dataset.id, place: places.find((place) => place.locationId === dataset.id)},
            })
          );
        }, 1000);
      }
    },
    [history, places, toggleSearch]
  );

  const fetchPlaceInformation = React.useMemo(
    () => async (value: any, callback: (results?: any[]) => void) => {
      try {
        const res = await axios.get(
          `https://geocoder.ls.hereapi.com/6.2/geocode.json?locationid=${value.locationId}&jsonattributes=1&gen=9&apiKey=${config.HERE_API_KEY}`
        );

        const location = res?.data.response?.view[0]?.result[0]?.location;
        if (!location) {
          //@todo: show growl
          return;
        }

        callback(location);
      } catch (err) {
        //@todo: show growl
        console.error(err);
        return;
      }
    },
    []
  );

  const fetchPlaces = React.useMemo(
    () =>
      debounce(async (value: string, callback: (results?: any[]) => void) => {
        try {
          const res = await axios.get(
            `https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json?apiKey=${config.HERE_API_KEY}&query=${value}&language=de&country=${autoCompleteCountryList}`
          );

          const suggestions = res?.data?.suggestions;
          if (!suggestions) {
            callback([]);
            return;
          }

          const places: any[] = [];
          for (const suggestion of suggestions) {
            await fetchPlaceInformation(suggestion, (result?: any) => {
              places.push(result);
            });
          }
          callback(places);
        } catch (err) {
          //@todo: show growl
          console.error(err);
          callback([]);
        }
      }, 300),
    [fetchPlaceInformation]
  );

  useEffect(() => {
    let active = true;

    if (inputValue === "") {
      setPlaces([]);
      return undefined;
    }

    fetchPlaces(inputValue, (results?: any[]) => {
      if (active) {
        setPlaces(results || []);
      }
    });

    return () => {
      active = false;
    };
  }, [inputValue, fetchPlaces]);

  const renderPlaces = () => {
    return (
      <ResultContainer>
        <HeadlineResult>{t("Orte")}</HeadlineResult>
        <ul>
          {places.length > 0 &&
            places.map((place) => (
              <ResultItem
                tabIndex={0}
                key={place.locationId}
                data-id={place.locationId}
                onClick={handleClickPlace}
              >
                <ResultIcon className="icon-ico_live" />
                <ResultText>{place.address.label}</ResultText>
              </ResultItem>
            ))}
        </ul>
      </ResultContainer>
    );
  };

  // fuelstations and servicestations
  const [stations, setStations] = useState<any[]>([]);

  const handleClickStation: MouseEventHandler<HTMLLIElement> = useCallback(
    (e) => {
      const dataset = (e.currentTarget as any).dataset;

      if (dataset.id) {
        history.push("/live");
        toggleSearch();

        setTimeout(() => {
          window.dispatchEvent(
            new CustomEvent("dkv-map-show-station", {
              detail: {id: dataset.id, station: stations.find((station) => station.id === dataset.id)},
            })
          );
        }, 1000);
      }
    },
    [history, stations, toggleSearch]
  );

  const fetchStations = React.useMemo(
    () =>
      debounce(async (value: string, callback: (results?: any[]) => void) => {
        if (!hasPermission("poi:FindDKVStations")) {
          callback([]);
          return;
        }

        try {
          const res = await httpClient.get(`/poi/pois/search?q=${value}`);

          const stations = res[0]?.data;
          if (!stations) {
            callback([]);
            return;
          }

          callback(stations);
        } catch (err) {
          //@todo: show growl
          console.error(err);
          callback([]);
        }
      }, 300),
    []
  );

  useEffect(() => {
    let active = true;

    if (inputValue === "") {
      setStations([]);
      return undefined;
    }

    fetchStations(inputValue, (results?: any[]) => {
      if (active) {
        setStations(results || []);
      }
    });

    return () => {
      active = false;
    };
  }, [inputValue, fetchStations]);

  const renderStations = () => {
    return (
      <ResultContainer>
        <HeadlineResult>{t("Tank- und Servicestellen")}</HeadlineResult>
        <ul>
          {stations.length > 0 &&
            stations.map((station) => (
              <ResultItem tabIndex={0} key={station.id} data-id={station.id} onClick={handleClickStation}>
                {station.category === "FUEL" ? (
                  <ResultIcon className="icon-ico_fuel" />
                ) : (
                  <ResultIcon className="icon-ico_maintenance" />
                )}
                <ResultText>
                  {station.names[0]}, {station.address?.street}, {station.address?.country}
                </ResultText>
              </ResultItem>
            ))}
        </ul>
      </ResultContainer>
    );
  };

  // driver
  /*
  const renderDriver = () => {
    return (
      <ResultContainer>
        <HeadlineResult>{t("Fahrer")}</HeadlineResult>
      </ResultContainer>
    );
  };
  */

  return (
    <SearchDrawer anchor={"left"} open={open}>
      <Container ref={searchContainerRef}>
        <BackButton aria-label={t("Suche schließen")} onClick={toggleSearch}>
          <span className="icon-ico_back" />
        </BackButton>
        <TextField label={t("DKV Live durchsuchen")} onChange={handleInputChange} autoFocus={true} />

        {/*<Headline>{t("Letzte Suchergebnisse")}</Headline>*/}

        {vehicles.length > 0 && renderVehicles()}
        {stations.length > 0 && renderStations()}
        {places.length > 0 && renderPlaces()}
        {/*renderDriver()*/}
      </Container>
    </SearchDrawer>
  );
};

export default Search;
