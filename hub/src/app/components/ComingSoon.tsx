import React from "react";
import styled, {css} from "styled-components";
import {useTranslation} from "react-i18next";
import {hasPermission} from "app/modules/login/state/login/selectors";

// -----------------------------------------------------------------------------------------------------------
interface CotainerProps {
  fixed?: boolean;
}

const Container = styled.div<CotainerProps>`
  position: absolute;
  top: 0;
  height: 100%;
  width: 100%;
  left: 0;
  z-index: 100000;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  pointer-events: all;
  background-color: rgba(255, 255, 255, 0.75);

  ${(props) =>
    props.fixed &&
    css`
      position: fixed;
      padding-left: 500px;
    `}
`;

interface LabelProps {
  small?: boolean;
}

const Label = styled.p<LabelProps>`
  font-size: 24px;
  color: #004b78;
  font-weight: bold;
  padding: 12px;
  background: radial-gradient(circle, rgba(255, 255, 255, 0.9) 75%, rgba(255, 255, 255, 0) 100%);
  text-align: center;

  ${(props) =>
    props.small &&
    css`
      font-size: 18px;
    `}
`;

interface IconProps {
  smallIcon?: boolean;
}
const Icon = styled.i<IconProps>`
  font-size: 128px;
  color: #004b78;
  margin-bottom: 32px;

  ${(props) =>
    props.smallIcon &&
    css`
      font-size: 84px;
      margin-bottom: 24px;
    `}
`;

// -----------------------------------------------------------------------------------------------------------
interface ComingSoonProps {
  small?: boolean;
  smallIcon?: boolean;
  fixed?: boolean;
  onClick?: () => void;
}

const ComingSoon: React.FC<ComingSoonProps> = ({small, smallIcon, onClick, fixed}) => {
  const {t} = useTranslation();

  if (hasPermission("superadmin:root")) {
    return null;
  }

  return (
    <Container fixed={fixed} onClick={onClick}>
      <Icon smallIcon={smallIcon} className="icon-ico_tools" />
      <Label small={small}>
        {t("Diese Funktion befindet sich derzeit noch in Entwicklung, wird aber bald verfügbar sein!")}
      </Label>
    </Container>
  );
};

export default ComingSoon;
