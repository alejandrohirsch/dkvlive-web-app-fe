import React from "react";
import styled, {css} from "styled-components/macro";
import {Link} from "react-router-dom";

// -----------------------------------------------------------------------------------------------------------
interface HeadlinleButtonProps {
  firstInLine?: boolean;
}

export const HeadlineContainer = styled.div`
  flex-shrink: 0;
  display: flex;
`;

export const HeadlineLinkButton = styled(({firstInLine, ...rest}) => <Link {...rest} />)`
  color: var(--dkv-page-headline-color);
  transition: color linear 0.1s;
  height: 32px;
  margin-top: 15px;

  ${(props) =>
    props.firstInLine &&
    css`
      margin-top: 0;
      padding: 15px 16px 8px 24px;
    `}

  &:hover {
    color: var(--dkv-text-primary-color);
  }
`;

export const HeadlineActionButton = styled.button<HeadlinleButtonProps>`
  height: 32px;
  margin-left: 4px;
  margin-top: 15px;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  background-color: transparent;
  cursor: pointer;

  color: var(--dkv-page-headline-color);
  transition: color linear 0.1s;

  &:hover {
    color: var(--dkv-text-primary-color);
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface HeadlineProps {
  inLine?: boolean;
}

const Headline = styled.h1<HeadlineProps>`
  flex-shrink: 0;
  font-size: 2.142857142857143rem;
  color: var(--dkv-page-headline-color);
  padding: 36px 64px 24px 64px;
  position: relative;
  font-weight: 500;

  ${(props) =>
    props.inLine &&
    css`
      padding: 20px 64px 8px 0;
    `}

  @media (max-width: 768px) {
    padding: 18px 32px 12px 32px;
  }
`;

export default Headline;
