import React, {MouseEventHandler, useState, useCallback} from "react";
import ReactDOM from "react-dom";
import styled, {css} from "styled-components/macro";
import {Loading, Button} from "dkv-live-frontend-ui";
import {FocusOn} from "react-focus-on";
import {IconButton} from "@material-ui/core";
import {Tabs} from "dkv-live-frontend-ui";

// -----------------------------------------------------------------------------------------------------------
interface WindowcontentContainerProps {
  full?: boolean;
  minWidth?: string;
  minHeight?: string;
  fixedWidth?: string;
  fixedHeight?: string;
}

export const WindowContentContainer = styled.section<WindowcontentContainerProps>`
  display: flex;
  flex-shrink: 0;
  flex-direction: column;
  padding: 27px 56px 44px 32px;

  ${(props) =>
    props.full &&
    css`
      padding: 0;
    `}

    ${(props) =>
      props.minHeight &&
      css`
        min-height: ${props.minHeight};
      `}

  ${(props) =>
    props.minWidth &&
    css`
      max-width: 98%;
      min-width: ${props.minWidth};
    `}

    ${(props) =>
      props.fixedWidth &&
      css`
        max-width: ${props.fixedWidth};
        min-width: ${props.fixedWidth};
      `}

    ${(props) =>
      props.fixedHeight &&
      css`
        max-height: ${props.fixedHeight};
        min-height: ${props.fixedHeight};
      `}
`;

interface WindowContentBoxProps {
  padding?: boolean;
}

export const WindowContent = styled.div<WindowContentBoxProps>`
  ${(props) =>
    props.padding &&
    css`
      padding: 40px 32px 40px 32px;
    `}
`;

interface WindowHeadlineProps {
  padding?: boolean;
  marginBottom?: number;
}

export const WindowHeadline = styled.div<WindowHeadlineProps>`
  position: relative;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;

  ${(props) =>
    props.marginBottom !== undefined &&
    css`
      margin-bottom: ${props.marginBottom}px;
    `}

  ${(props) =>
    props.padding &&
    css`
      padding: 27px 56px 0 32px;
    `}

  h1 {
    font-size: 1.7142857142857142rem;
    font-weight: 500;
    line-height: 1.25;
    color: var(--dkv-window-headline-color, #666666);
    font-weight: bold;
  }

  h2 {
    font-size: 1.2857142857142858rem;
    font-weight: 500;
    line-height: 1.25;
    color: var(--dkv-window-headline-color, #666666);
    margin-left: 18px;
  }
`;

export const WindowContentGrid = styled.div`
  display: grid;
  grid-template-columns: minmax(432px, 1fr);
  grid-row-gap: 24px;
`;

export const WindowButtons = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 26px;

  button {
    width: 100%;
    margin-left: 16px;
    flex-shrink: 1;
  }

  button:first-of-type {
    margin-left: 0;
  }
`;

export const WindowActionButtons = styled(WindowButtons)`
  margin-top: 64px;
  justify-content: flex-end;

  button {
    min-width: 206px;
    max-width: 100%;
    width: auto;
  }
`;

export const WindowActionButton = styled(Button)`
  min-width: 206px;
  max-width: 100%;
  width: auto;
`;

export const WindowTabsContainer = styled.div`
  width: 100%;
  border-bottom: 2px solid #efefef;
  display: flex;
  justify-content: center;
`;

export const WindowTabs = styled(Tabs)`
  height: 42px !important;
  min-height: 42px !important;
`;

// -----------------------------------------------------------------------------------------------------------
interface WindowTabPanelProps {
  children?: React.ReactNode;
  index: any;
  current: any;
}

export const WindowPanel = styled.div`
  display: flex;
  width: 100%;
`;

export const WindowTabPanel: React.FC<WindowTabPanelProps> = ({current, index, children}) => {
  return (
    <WindowPanel id={current} hidden={current !== index}>
      {children}
    </WindowPanel>
  );
};
// -----------------------------------------------------------------------------------------------------------
interface WindowActionsProps {
  right?: boolean;
  autoTop?: boolean;
}

export const WindowActions = styled.div<WindowActionsProps>`
  margin-top: 40px;
  display: flex;
  justify-content: center;

  ${(props) =>
    props.right &&
    css`
      justify-content: flex-end;
    `}

  ${(props) =>
    props.autoTop &&
    css`
      margin-top: auto;
      margin-bottom: 24px;
    `}
`;

// -----------------------------------------------------------------------------------------------------------
interface BackdropProps {
  modal: boolean;
}

const Backdrop = styled.div<BackdropProps>`
  position: fixed;
  top: 0;
  height: 100%;
  width: 100%;
  left: 0;
  z-index: 10000;

  display: flex;
  justify-content: center;
  align-items: center;

  pointer-events: none;

  ${(props) =>
    props.modal &&
    css`
      pointer-events: all;
      background-color: var(--dkv-window-backdrop-color, rgba(50, 50, 50, 0.6));
    `}
`;

const WindowBox = styled.div`
  display: flex;
  position: relative;
  background-color: var(--dkv-window-background-color, #fff);
  box-shadow: 0 0 5px 0 var(--dkv-window-shadow-color, #c1c1c1);
  max-height: calc(100% - 100px);
  max-width: calc(100% - 100px);
  pointer-events: all;

  .dkv-window-focus-on {
    width: 100%;
  }

  .os-host {
    width: 100%;
  }
`;

const CloseButton = styled(IconButton)`
  position: absolute !important;
  bottom: 14px;
  right: 10px;
  top: 10px;
  padding: 0 !important;
  width: 30px;
  height: 30px;
  z-index: 2;

  span {
    font-size: 24px;
    color: #363636;
  }

  &.dkv-table-window {
    top: 36px;
    right: 27px;
  }
`;

const WindowContentBox = styled.div`
  position: relative;
  min-width: 50px;
  width: 100%;
  overflow: auto;
`;

// -----------------------------------------------------------------------------------------------------------
const stopPropagation: MouseEventHandler<HTMLDivElement> = (e) => e.stopPropagation();

// -----------------------------------------------------------------------------------------------------------
export interface WindowContentProps {
  closeWindow: () => void;
  forceCloseWindow: () => void;
  setLoading: (loading: boolean, ariaProps?: boolean, ariaLabel?: string) => void;
  headline?: string;
}

interface WindowProps {
  onClose: () => void;
  children: ((props: WindowContentProps) => React.ReactNode) | React.ReactNode;

  modal?: boolean;
  closable?: boolean;
  headline?: string;
  tableWindow?: boolean;
}

const Window: React.FC<WindowProps> = ({
  onClose,
  headline,
  closable = true,
  modal = true,
  children,
  tableWindow,
}) => {
  // loading
  const [isLoading, setIsLoading] = useState(false);
  const [loadingAriaProps, setLoadingAriaProps] = useState(false);
  const [loadingAriaLabel, setLoadingAriaLabel] = useState<string | undefined>();

  const setLoading = useCallback((loading: boolean, ariaProps?: boolean, ariaLabel?: string) => {
    setIsLoading(loading);
    setLoadingAriaProps(ariaProps ?? false);
    setLoadingAriaLabel(ariaLabel);
  }, []);

  // close
  const closeWindow = useCallback(() => {
    if (isLoading || !closable) {
      return;
    }

    onClose();
  }, [isLoading, closable, onClose]);

  // render
  const component = (
    <Backdrop modal={modal} onClick={closeWindow}>
      <WindowBox onClick={stopPropagation} role="dialog" aria-label={headline}>
        <FocusOn className="dkv-display-flex dkv-window-focus-on" enabled={modal}>
          <Loading inprogress={isLoading} ariaProps={loadingAriaProps} ariaLabel={loadingAriaLabel} />

          <CloseButton
            onClick={closeWindow}
            hidden={isLoading || !closable}
            className={tableWindow ? "dkv-table-window" : undefined}
          >
            <span className="icon-ico_clear" />
          </CloseButton>

          <WindowContentBox>
            {children instanceof Function
              ? children({forceCloseWindow: onClose, closeWindow, setLoading, headline})
              : children}
          </WindowContentBox>
        </FocusOn>
      </WindowBox>
    </Backdrop>
  );

  return ReactDOM.createPortal(component, document.body);
};

export default Window;
