import React, {MouseEventHandler, useCallback, useState, useEffect} from "react";
import AutoSizer from "react-virtualized-auto-sizer";
import {DataTable as PrimeDataTable} from "primereact/datatable";
import styled, {css} from "styled-components/macro";
import {useTranslation} from "react-i18next";
import {IconButton} from "@material-ui/core";
import Popper from "@material-ui/core/Popper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import {TextField} from "dkv-live-frontend-ui";
import {Link} from "react-router-dom";
import {Column} from "primereact/column";
import {resolveUserDateFormat} from "app/modules/login/state/login/selectors";
import dayjs from "dayjs";

// -----------------------------------------------------------------------------------------------------------
const Table = styled(PrimeDataTable)`
  color: #666666;

  .p-datatable-scrollable-wrapper {
    background-color: #ffffff;
  }

  .p-datatable-thead > tr > th,
  .p-datatable-tfoot > tr > td {
    padding: 15px !important;

    &.p-selection-column {
      padding: 12px 15px !important;
    }
  }

  .p-datatable-tbody > tr.p-rowgroup-header {
    background-color: #f8f8f8;
  }

  .p-datatable-tbody > tr.p-datatable-row.warning {
    border-left: 3px solid var(--dkv-highlight-warning-color);

    .highlight {
      color: var(--dkv-highlight-warning-color);
    }
  }

  .p-datatable-tbody > tr.p-datatable-row.dkv-row-new {
    font-weight: bold;
    border-left: 3px solid var(--dkv-highlight-error-color);
  }

  .p-datatable-tbody > tr.p-datatable-row.dkv-row-new-asset {
    border-left: 3px solid var(--dkv-highlight-ok-color);
    background-color: #def2de;
    &:hover {
      background-color: #e6f0f6;
    }
  }

  .p-datatable-tbody > tr.p-datatable-row.dkv-row-missing-information {
    border-left: 3px solid var(--dkv-highlight-warning-color);
    background-color: #fef2d5;
    &:hover {
      background-color: #e6f0f6;
    }
  }

  .p-datatable-tbody > tr.p-datatable-row.alarm {
    border-left: 3px solid var(--dkv-highlight-error-color);
  }

  .p-datatable-tbody > tr.p-datatable-row.notice {
    border-left: 3px solid var(--dkv-highlight-warning-color);
  }

  .p-datatable-tbody > tr.p-datatable-row.information {
    border-left: 3px solid var(--dkv-highlight-info-color);
  }

  .p-datatable-tbody > tr > td {
    padding: 14px 14px 14px 16px !important;
    line-height: 1.25;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    vertical-align: middle;

    &.p-selection-column {
      padding: 12px 14px 8px 16px !important;
      overflow: visible;
      vertical-align: top;
    }

    &.full {
      padding: 0 !important;
    }

    &.full-height {
      padding: 0 14px 0 16px !important;
    }

    &.dkv-dt-column-alignedtop {
      vertical-align: top;
    }

    &.dkv-dt-column-expander {
      width: 3rem;
      padding: 0 !important;
      text-align: center;
    }
  }

  .p-datatable-thead > tr > th {
    &.dkv-dt-column-expander {
      width: 3rem;
      padding: 0 !important;
      text-align: center;
    }
  }

  .p-datatable-tbody > tr:not(.p-datatable-row) {
    border-bottom: 1px solid #efefef;
  }

  .p-datatable-thead > tr > th {
    border-bottom: 1px solid #efefef;
    border-right: 1px solid #efefef;
    font-weight: normal;
    color: #666666;
    background-color: #ffffff;
    text-align: left;
    position: relative;
  }

  .p-column-title {
    & > span {
      pointer-events: none;
      white-space: nowrap;
    }
  }

  .p-selection-column {
    .p-column-title {
      padding-right: 0;
    }
  }

  .p-datatable-thead > tr > th:last-of-type {
    border-right: 0;
  }

  .p-sortable-column-icon {
    position: absolute !important;
    transition: opacity linear 0.1s;
    font-size: 16px;
    top: 2px;
    right: 2px;

    &.pi-sort,
    &.pi-sort-alt {
      opacity: 0;
      font-family: "DKV" !important;

      &:before {
        content: "\\e987";
      }
    }

    &.pi-sort-up,
    &.pi-sort-amount-up-alt {
      font-family: "DKV" !important;

      &:before {
        content: "\\e986";
      }
    }

    &.pi-sort-down,
    &.pi-sort-amount-down {
      font-family: "DKV" !important;

      &:before {
        content: "\\e985";
      }
    }
  }

  .p-row-toggler {
    font-size: 20px;

    .pi-chevron-right {
      font-family: "DKV" !important;

      &:before {
        content: "\\e940";
      }
    }

    .pi-chevron-down {
      font-family: "DKV" !important;

      &:before {
        content: "\\e93d";
      }
    }
  }

  .p-sortable-column:hover {
    .p-sortable-column-icon {
      opacity: 1;
    }
  }

  .p-datatable-row {
    background-color: #ffffff;
    border-bottom: 1px solid #efefef;

    &:hover {
      background-color: #e9f3f9;
    }

    &:focus {
      outline-color: #ccdbe4;
    }
  }

  .p-datatable-tbody {
    font-size: 16px;
  }

  .p-checkbox .p-checkbox-box {
    background-color: #ffffff;
    text-align: center;
    transition: background-color 0.1s, border-color 0.1s, box-shadow 0.1s;
    width: 24px;
    height: 24px;
    border-radius: 2px;
    border: solid 1px #dbdbdb;

    &.p-highlight {
      border-color: #004b78;
      background-color: #ccdbe4;
      color: #004b78;
    }

    &:not(.p-disabled):hover {
      border-color: #666666;
    }
  }

  .p-checkbox .p-checkbox-icon {
    display: block;
    height: 100%;
    font-size: 20px;
  }
`;

const ColumnHeaderFilterButton = styled(IconButton)`
  position: absolute !important;
  padding: 0 !important;
  right: 16px;
  top: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  width: 32px;
  height: 32px;
  margin: auto !important;

  span {
    font-size: 20px;
  }
`;

const FilterOverlay = styled.div`
  background-color: #ffffff;
  padding: 24px 16px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  max-height: 60%;
  overflow: auto;
`;

// -----------------------------------------------------------------------------------------------------------
export const TablePageContent = styled.div`
  height: 100%;
  display: flex;
  flex-flow: column nowrap;
  padding: 0 65px;
  overflow: hidden;

  @media (max-width: 768px) {
    padding: 0 12px;
  }
`;

export const TableStickyFooter = styled.div`
  flex-shrink: 0;
  background-color: #ffffff;
  margin-top: 24px;
  padding: 0 42px;
  display: flex;
  align-items: center;
  height: 96px;
`;

export const TableContainer = styled.div`
  height: 100%;
  overflow: hidden;
`;

interface TableStickyHeaderContainerProps {
  hasBackButton: boolean;
  details?: boolean;
}

const TableStickyHeaderContainer = styled.div<TableStickyHeaderContainerProps>`
  flex-shrink: 0;
  background-color: #ffffff;
  margin-top: 33px;
  margin-bottom: 24px;
  padding: 28px;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;

  ${(props) =>
    props.hasBackButton &&
    css`
      position: relative;
      padding-top: 59px;
    `}

  ${(props) =>
    props.details &&
    css`
      margin-bottom: 0;
      padding-bottom: 8px;
    `}

  h1 {
    font-size: 2.142857142857143em;
    color: #323232;
    font-weight: 500;
  }

  @media (max-width: 768px) {
    margin-top: 12px;
    flex-direction: column;
    align-items: flex-start;
  }
`;

const BackButton = styled(Link)`
  position: absolute;
  top: 28px;
  left: 28px;
  color: var(--dkv-link-primary-color);
  line-height: 1.25;
  font-size: 1.2em;
  font-weight: 500;
  transition: color linear 0.1s;
  font-family: inherit;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  flex-grow: 0;
  justify-content: flex-end;
  align-items: center;

  &:hover,
  &:focus {
    color: var(--dkv-link-primary-hover-color);
  }
`;

const TableStickyHeaderDetailsContainer = styled.div`
  flex-shrink: 0;
  background-color: #ffffff;
  margin-bottom: 24px;
  padding: 8px 24px 24px 24px;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  border-top: 1px solid var(--dkv-grey_20);
  position: relative;
  min-height: 60px;
`;

const TableStickyHeaderDetailsCollapseButton = styled.button`
  color: var(--dkv-link-primary-color);
  line-height: 1.25;
  font-size: 1.1428571428571428rem;
  font-weight: 500;
  transition: color linear 0.1s;
  font-family: inherit;
  border: 0;
  padding: 0;
  outline: 0;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  flex-grow: 0;
  justify-content: flex-end;
  align-items: center;
  position: absolute;
  top: 16px;
  right: 40px;

  &:hover,
  &:focus {
    color: var(--dkv-link-primary-hover-color);
  }

  span {
    margin-left: 4px;
    font-size: 16px;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface NewFlaggedFieldProps {
  new?: boolean;
}

const NewFlaggedField = styled.span<NewFlaggedFieldProps>`
  position: relative;

  ${(props) =>
    props.new &&
    css`
      padding-left: 14px;

      &:before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        bottom: 3px;
        margin: auto;
        display: block;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background-color: var(--dkv-carminepink);
      }
    `}
`;

// -----------------------------------------------------------------------------------------------------------
interface TableStickyHeaderDetailsProps {
  ChildrenContainer?: any;
}

export const TableStickyHeaderDetails: React.FC<TableStickyHeaderDetailsProps> = ({
  children,
  ChildrenContainer = styled.div``,
}) => {
  const {t} = useTranslation();
  const [collapsed, setCollapsed] = useState(true);

  const toggle = () => setCollapsed((c) => !c);

  return (
    <TableStickyHeaderDetailsContainer>
      <TableStickyHeaderDetailsCollapseButton onClick={toggle}>
        {collapsed ? t("Details ausblenden") : t("Details einblenden")}
        <span className={collapsed ? "icon-ico_chevron-up" : "icon-ico_chevron-down"} />
      </TableStickyHeaderDetailsCollapseButton>
      <ChildrenContainer hidden={!collapsed}>{children}</ChildrenContainer>
    </TableStickyHeaderDetailsContainer>
  );
};

export interface TableStickyHeaderProps {
  backPath?: string;
  onClick?: (e: any) => void;
  className?: string;
  details?: boolean;
}

export const TableStickyHeader: React.FC<TableStickyHeaderProps> = ({
  backPath,
  onClick,
  className,
  children,
  details,
}) => {
  const [t] = useTranslation();
  return (
    <TableStickyHeaderContainer
      className={className}
      hasBackButton={Boolean(backPath || onClick)}
      details={details}
    >
      {(backPath || onClick) && (
        <BackButton to={`${backPath}`} onClick={onClick}>
          <span className={"icon-ico_back"} />
          <span style={{marginLeft: "5px"}}>{t("Zurück")}</span>
        </BackButton>
      )}
      {children}
    </TableStickyHeaderContainer>
  );
};

export const SearchTextField = styled(TextField)`
  width: 260px;
  margin-left: auto !important;
  margin-right: 86px !important;

  .MuiInputLabel-root {
    color: #c1c1c1;
  }
`;

export const ToolbarButtonIcon = styled.div`
  width: 36px;
  height: 36px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: color linear 0.1s, background-color linear 0.1s;

  span {
    color: #004b78;
    font-size: 24px;
    transition: color linear 0.1s;
  }
`;

interface ToolbarButtonProps {
  general?: boolean;
}

export const ToolbarButton = styled.button<ToolbarButtonProps>`
  user-select: none;
  border: 0;
  outline: 0;
  cursor: pointer;
  overflow: hidden;
  font: inherit;
  background-color: transparent;
  display: flex;
  flex-direction: column;
  align-items: center;
  font-size: 12px;
  color: #004b78;
  margin-left: 8px;
  transition: color linear 0.1s;
  font-weight: 500;

  &:hover,
  &:focus {
    color: #003e96;

    ${ToolbarButtonIcon} {
      span {
        color: #003e96;
      }
    }
  }

  ${(props) =>
    props.general &&
    css`
      color: #666666;

      ${ToolbarButtonIcon} {
        span {
          color: #666666;
        }
      }
    `}
`;

export const TableFooterInfo = styled.span`
  margin-left: auto;
  font-size: 1.2142857142857142rem;
`;

export const TableFooterActions = styled.span`
  margin-left: auto;
  display: flex;
  justify-content: space-between;
`;

export const TableFooterRightActions = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;

  button {
    margin-left: 48px;
  }
`;

export const SearchIcon = styled.span`
  font-family: "DKV" !important;
  font-size: 20px;
`;

// -----------------------------------------------------------------------------------------------------------
const TableSelectionCountContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 24px;
`;

const TableSelectionCountNumber = styled.span`
  font-size: 16px;
  font-weight: 500;
  color: #666666;
`;

const TableSelectionCountLabel = styled.span`
  font-size: 12px;
  font-weight: 500;
  color: #c1c1c1;
  margin-top: 2px;
`;

interface TableSelectionCountInfoProps {
  count: number;
  label: string;
}

export const TableSelectionCountInfo: React.FC<TableSelectionCountInfoProps> = React.memo(
  ({count, label}) => {
    return (
      <TableSelectionCountContainer>
        <TableSelectionCountNumber>{count}</TableSelectionCountNumber>
        <TableSelectionCountLabel>{label}</TableSelectionCountLabel>
      </TableSelectionCountContainer>
    );
  }
);

// -----------------------------------------------------------------------------------------------------------
export const ColumnHeader = React.memo(({col}: any) => {
  const {t} = useTranslation();

  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const openFilter: MouseEventHandler<HTMLButtonElement> = useCallback((e) => {
    setAnchorEl((e as any).currentTarget.parentElement.parentElement);
  }, []);

  const open = Boolean(anchorEl);

  const handleMouseDown = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  return (
    <>
      <span>{t(col.header)}</span>
      {col.filterable && (
        <ColumnHeaderFilterButton aria-label={t("Filter")} onClick={openFilter} onMouseDown={handleMouseDown}>
          <span className="icon-ico_abstract-filter" />
        </ColumnHeaderFilterButton>
      )}

      <Popper open={open} anchorEl={anchorEl} placement="bottom-start" transition style={{zIndex: 4001}}>
        <ClickAwayListener onClickAway={handleClose}>
          <FilterOverlay>testo tttttttttttttttttttttttttttttttt</FilterOverlay>
        </ClickAwayListener>
      </Popper>
    </>
  );
});

export const bodyField = (cb: (rowData: any) => any, suffix = "") => (rowData: any) => {
  return suffix ? `${cb(rowData)} ${suffix}` : cb(rowData);
};

export const newField = (rowData: any, column: any) => {
  return <NewFlaggedField new={rowData.new}>{rowData[column.field]}</NewFlaggedField>;
};

export const dateField = (rowData: any, column: any) => {
  const d = rowData[column.field];
  if (!d || d === "0001-01-01T00:00:00Z") {
    return "";
  }

  if (d.indexOf("T") === -1) {
    return d;
  }

  return dayjs(d).format(resolveUserDateFormat());
};

export const boolField = (t: any) => (rowData: any, column: any) => {
  return t(rowData[column.field] ? "Ja" : "Nein");
};

// -----------------------------------------------------------------------------------------------------------
export const useColumns = (columns: any[], t?: any, noSelection?: boolean, expdander?: boolean): any[] => {
  const [visibleColumns, setVisibleColumns] = useState<any[]>([]);

  useEffect(() => {
    const dynamicColumns = columns.map((col) => {
      return (
        <Column
          columnKey={col.field}
          key={col.field}
          field={col.field}
          sortable={col.sortable}
          header={<ColumnHeader col={col} />}
          body={col.bodyWithTranslation ? col.bodyWithTranslation(t) : col.body ?? null}
          bodyClassName={col.bodyClassName ?? null}
          style={{width: col.width + 36}}
          className={col.filterable ? "dkv-dt-column-filterable" : col.className ?? ""}
        />
      );
    });

    if (!noSelection) {
      dynamicColumns.unshift(<Column key="sel" selectionMode="multiple" style={{width: 48}} />);
    }

    if (expdander) {
      dynamicColumns.push(<Column key="exp" expander={true} className="dkv-dt-column-expander" />);
    }

    setVisibleColumns(dynamicColumns);
  }, [columns, t, noSelection, expdander]);

  return visibleColumns;
};

// -----------------------------------------------------------------------------------------------------------
interface DataTableProps {
  items: any[];
  columns: any[];
  selected: any[];
  onSelectionChange: (e: any) => void;
  rowClassName?: any;
  onRowDoubleClick?: (e: any) => void;
  rowGroupMode?: string;
  groupField?: string;
  rowGroupHeaderTemplate?: (data: any) => any;
  rowGroupFooterTemplate?: (data: any) => any;
  noSelection?: boolean;
  expandedRows?: any[] | null;
  onRowToggle?: (e: any) => void;
  rowExpansionTemplate?: any;
}

export const DataTable: React.FC<DataTableProps> = React.memo(
  ({
    items,
    columns,
    selected,
    onSelectionChange,
    rowClassName,
    onRowDoubleClick,
    rowGroupMode,
    groupField,
    rowGroupHeaderTemplate,
    rowGroupFooterTemplate,
    noSelection,
    expandedRows,
    onRowToggle,
    rowExpansionTemplate,
  }) => {
    let selectionProps: any = {
      selectionMode: "multiple",
      selection: selected,
      onSelectionChange: onSelectionChange,
    };

    if (noSelection) {
      selectionProps = {};
    }

    // render
    return (
      <AutoSizer>
        {({height, width}) => (
          <Table
            value={items}
            scrollable={true}
            scrollHeight={`${height - 50}px`}
            style={{height: height, width: width}}
            reorderableColumns
            {...selectionProps}
            rowClassName={rowClassName}
            onRowDoubleClick={onRowDoubleClick}
            rowGroupMode={rowGroupMode}
            groupField={groupField}
            expandedRows={expandedRows}
            onRowToggle={onRowToggle}
            rowExpansionTemplate={rowExpansionTemplate}
            rowGroupHeaderTemplate={
              typeof rowGroupHeaderTemplate === "function"
                ? rowGroupHeaderTemplate
                : () => {
                    return undefined;
                  }
            }
            rowGroupFooterTemplate={
              typeof rowGroupFooterTemplate === "function"
                ? rowGroupHeaderTemplate
                : () => {
                    return undefined;
                  }
            }
          >
            {columns}
          </Table>
        )}
      </AutoSizer>
    );
  }
);

export default DataTable;
