import React from "react";
import styled from "styled-components/macro";
import {Message} from "dkv-live-frontend-ui";

// -----------------------------------------------------------------------------------------------------------
export const PageMessage = styled(Message)`
  margin: 0 24px;
`;

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  height: 100%;
  overflow: auto;
`;

// -----------------------------------------------------------------------------------------------------------
interface PageContent {
  className?: string;
}

const PageContent: React.FC<PageContent> = ({children, className}) => {
  return <Container className={className}>{children}</Container>;
};

export default PageContent;
