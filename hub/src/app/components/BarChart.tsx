import React from "react";
import styled from "styled-components/macro";
import {
  BarChart as ReChartsBarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip as RechartsTooltip,
} from "recharts";
import {useTranslation} from "react-i18next";
// import AutoSizer from "react-virtualized-auto-sizer";

// -----------------------------------------------------------------------------------------------------------
const TooltipContainer = styled.div`
  padding: 8px;
  border: solid 0.5px #707070;
  background-color: #fff;
`;

const TooltipLabel = styled.p`
  font-weight: bold;
  font-size: 1.14286em;
  color: var(--dkv-text-primary-color);
  line-height: 1.25;
`;

const TooltipData = styled.div`
  margin-top: 4px;
  font-size: 1em;

  span {
    font-weight: bold;
  }
`;

// -----------------------------------------------------------------------------------------------------------
const Tooltip = RechartsTooltip as any;

const Bars = (props: any) => {
  const {fill, x, y, width, height, dataKey} = props;

  const count = props[dataKey];

  const margin = 2;
  const barHeight = height / count;

  const rects = [];
  for (let i = 0; i < count; i++) {
    rects.push(
      <rect
        key={y + i * barHeight}
        x={x + width - width / 2 - 2}
        y={y + i * barHeight + i * (margin / count)}
        width={8}
        height={barHeight - margin}
        stroke="none"
        fill={fill}
      />
    );
  }

  return <svg>{rects}</svg>;
};

const XAxisTick = (props: any): SVGElement => {
  const {width, height, x, y, payload} = props;

  const value = payload.value.split(";");

  return ((
    <text
      orientation="bottom"
      width={width}
      height={height}
      type="category"
      x={x}
      y={y}
      stroke="none"
      fill="#909090"
      className="recharts-text recharts-cartesian-axis-tick-value recharts-cartesian-axis-tick-value-x"
      textAnchor="middle"
    >
      <tspan x={x} dy="0.71em">
        {value[0]}
      </tspan>
      <tspan x={x} dy="1.5em">
        {value[1]}
      </tspan>
    </text>
  ) as unknown) as SVGElement;
};

const YAxisTick = (props: any): SVGElement => {
  const {width, height, x, y, payload} = props;

  if (payload.value === 0) {
    return ((<text />) as unknown) as SVGElement;
  }

  return ((
    <text
      orientation="left"
      width={width}
      height={height}
      type="number"
      x={x}
      y={y}
      stroke="none"
      fill="#909090"
      className="recharts-text recharts-cartesian-axis-tick-value recharts-cartesian-axis-tick-value-x"
      textAnchor="end"
    >
      <tspan x={x} dy="0.355em">
        {payload.value}
      </tspan>
    </text>
  ) as unknown) as SVGElement;
};

const TooltipContent = (props: any) => {
  const {t} = useTranslation();

  if (!props.active) {
    return null;
  }
  const label = props.label.split(";");

  const data = {};
  props.keys.forEach((k: any) => {
    data[k.key] = 0;
  });

  props.payload.forEach((p: any) => {
    data[p.dataKey] += p.value;
  });

  return (
    <TooltipContainer>
      <TooltipLabel>
        {t(label[1])} {t(label[0])}
      </TooltipLabel>

      {props.keys.map((k: any) => (
        <TooltipData key={k.key}>
          <label>{t(k.label)}: </label>
          <span>{data[k.key]}</span>
        </TooltipData>
      ))}
    </TooltipContainer>
  );
};

// -----------------------------------------------------------------------------------------------------------
interface BarChartProps {
  name: string;
  width: number;
  height: number;
  data: any[];
  keys: any[];
}

export const BarChart: React.FC<BarChartProps> = React.memo(({name, width, height, data, keys}) => {
  return (
    <>
      <ReChartsBarChart
        height={height}
        width={width}
        data={data}
        margin={{top: 20, right: 12, left: -35, bottom: 30}}
      >
        <CartesianGrid strokeDasharray="5 5" vertical={false} />
        <XAxis dataKey="name" axisLine={false} tickLine={false} tick={XAxisTick} interval={0} />
        <YAxis domain={[0, "dataMax"]} axisLine={false} tickLine={false} tickCount={3} tick={YAxisTick} />
        <Tooltip
          cursor={{fill: "#efefef"}}
          animationDuration={100}
          animationEasing="linear"
          content={<TooltipContent keys={keys} />}
        />
        {keys.map((k) => (
          <Bar
            key={k.key}
            dataKey={k.key}
            stackId={name}
            fill={k.color}
            shape={<Bars dataKey={k.key} />}
            isAnimationActive={false}
          />
        ))}
      </ReChartsBarChart>
    </>
  );
});

export default BarChart;
