import React, {useEffect, useState} from "react";
import styled from "styled-components/macro";
import {useTranslation} from "react-i18next";
import {NavLink, useLocation, Link} from "react-router-dom";
import {Menu, MenuItem, ListItem, ListItemText} from "@material-ui/core";
import {IconButton} from "@material-ui/core";
import LogoImage from "../../assets/dkvlive.svg";
import config from "config";
import Search from "./Search";
import {hasPermission} from "app/modules/login/state/login/selectors";
import UserProfile from "app/modules/live/pages/management/components/UserProfile";
import Window from "./Window";

// -----------------------------------------------------------------------------------------------------------
export interface NavItem {
  to: string;
  exact?: boolean;
  label: string;
  icon: string;
}

// -----------------------------------------------------------------------------------------------------------
const activeNavClassName = "nav-item-active";

// -----------------------------------------------------------------------------------------------------------
const Container = styled.div`
  height: 100%;
  width: 112px;
  background-color: var(--dkv-background-primary-color);
  flex-shrink: 0;
  display: flex;
  flex-direction: column;
  z-index: 4000;
  overflow-y: auto;

  ul {
    flex-shrink: 0;
  }
`;

const NavItem = styled.li`
  font-size: 0.8571428571428571rem;
  line-height: 2;
  height: 74px;

  a,
  button {
    padding: 10px 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    color: var(--dkv-nav-primary-color);
    transition: color linear 0.1s;
    position: relative;
    border: 0;
    background-color: transparent;
    cursor: pointer;
    width: 100%;
    height: 100%;
    font-size: 0.8571428571428571rem;
    font-family: inherit;

    &.${activeNavClassName} {
      color: var(--dkv-link-primary-color);
      font-weight: 500;
      background-color: #f4faff;

      &:before {
        content: "";
        width: 3px;
        height: 100%;
        position: absolute;
        left: 0;
        top: 0;
        background-color: var(--dkv-link-primary-color);
      }
    }

    &:hover {
      color: var(--dkv-link-primary-color);
    }

    span {
      margin-top: 2px;
      overflow: hidden;
      width: 100%;
      text-align: center;
      white-space: nowrap;
      text-overflow: ellipsis;
      padding: 0 5px;
    }
  }
`;

const IconSpan = styled.span`
  font-size: 30px;
`;

const FooterNav = styled.ul`
  margin-top: auto;
`;

const LogoContainer = styled.div`
  border-bottom: 2px solid var(--dkv-background-secondary-color);
  flex-shrink: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 74px;
`;

const Logo = styled.img`
  width: 100px;
`;

const SearchButtonContainer = styled.div`
  border-bottom: 2px solid var(--dkv-background-secondary-color);
  display: flex;
  justify-content: center;
  padding: 5px 0;
`;

const SearchButton = styled(IconButton)`
  span {
    font-size: 28px;
    color: #666666;
  }
`;

// -----------------------------------------------------------------------------------------------------------
interface NavProps {
  items: NavItem[];
}

const Nav: React.FC<NavProps> = ({items}) => {
  const {t} = useTranslation();

  // search
  const [searchOpen, setSearchOpen] = React.useState<boolean>(false);

  // user menu
  const [userMenuEl, setUserMenuEl] = React.useState<null | HTMLElement>(null);

  const handleUserMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setUserMenuEl(event.currentTarget);
  };

  const handleUserMenuClose = () => {
    setUserMenuEl(null);
  };

  // help menu
  const [helpMenuEl, setHelpMenuEl] = React.useState<null | HTMLElement>(null);

  const handleHelpMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setHelpMenuEl(event.currentTarget);
  };

  const handleHelpMenuClose = () => {
    setHelpMenuEl(null);
  };

  // logout
  const logout = () => {
    localStorage.removeItem("dkv-sess");
    window.location.reload();
  };

  useEffect(() => {
    const forceLogout = () => {
      localStorage.setItem("dkv-login-err", "unauth");
      logout();
    };

    window.addEventListener("dkv-logout", forceLogout);

    return () => {
      window.removeEventListener("dkv-logout", forceLogout);
    };
  }, []);

  // update version
  const updateVersion = async () => {
    // reset browser
    const promises = [];

    // clear local storage
    localStorage.clear();

    // unregister sw
    if ("serviceWorker" in navigator) {
      const sw = navigator.serviceWorker.getRegistrations().then(function(registrations) {
        const promises = registrations.map(function(registration) {
          return registration.unregister();
        });

        return Promise.all(promises);
      });

      promises.push(sw);
    }

    // clear caches
    if (window.caches) {
      const cache = window.caches.keys().then(function(keyList) {
        const promises = keyList.map(function(key) {
          return window.caches.delete(key);
        });

        return Promise.all(promises);
      });

      promises.push(cache);
    }

    // resolve all and reload
    await Promise.all(promises).then(function() {
      console.log("Cache cleared");
      console.log("SW unregistered");
    });

    window.location.reload(true);
  };

  // show search
  const toggleSearch = () => {
    setSearchOpen(!searchOpen);
  };

  // profile
  const [showProfileWindow, setShowProfileWindow] = useState<boolean>(false);

  const showUserProfile = () => {
    setUserMenuEl(null);
    setShowProfileWindow(true);
  };

  // check module
  let activeModule = "live";

  // @todo: check permission
  const location = useLocation();
  if (hasPermission("internal:Visible")) {
    if (location.pathname.startsWith("/_internal")) {
      activeModule = "internal";
    }
  }

  // render
  return (
    <Container>
      <LogoContainer>
        {hasPermission("internal:Visible") ? (
          <Link to={activeModule === "live" ? "/_internal" : "/"}>
            <Logo src={LogoImage} alt="DKV.Live" />
          </Link>
        ) : (
          <Logo src={LogoImage} alt="DKV.Live" />
        )}
      </LogoContainer>

      {activeModule === "live" && hasPermission("search:Visible") ? (
        <>
          <SearchButtonContainer onClick={toggleSearch}>
            <SearchButton>
              <span className="icon-ico_search" />
            </SearchButton>
          </SearchButtonContainer>

          <Search open={searchOpen} toggleSearch={toggleSearch} />
        </>
      ) : null}

      <ul>
        {items.map((item) => {
          const label = t(item.label);

          return (
            <NavItem key={item.to} title={label}>
              <NavLink exact={item.exact} to={item.to} activeClassName={activeNavClassName}>
                <IconSpan className={item.icon} />
                <span>{label}</span>
              </NavLink>
            </NavItem>
          );
        })}
      </ul>

      <FooterNav>
        <NavItem title={t("Hilfe") as string}>
          <button aria-controls="helpmenu" aria-haspopup="true" onClick={handleHelpMenuClick}>
            <IconSpan className="icon-ico_help" />
            <span>{t("Hilfe")}</span>
          </button>
          <Menu
            id="helpmenu"
            keepMounted
            anchorEl={helpMenuEl}
            open={Boolean(helpMenuEl)}
            onClose={handleHelpMenuClose}
          >
            <MenuItem onClick={updateVersion}>{t("Version aktualisieren")}</MenuItem>
            <ListItem>
              <ListItemText primary={null} secondary={config.VERSION} />
            </ListItem>
          </Menu>
        </NavItem>

        <NavItem title={t("Benutzer") as string}>
          <button aria-controls="usermenu" aria-haspopup="true" onClick={handleUserMenuClick}>
            <IconSpan className="icon-ico_profil" />
            <span>{t("Benutzer")}</span>
          </button>
          <Menu
            id="usermenu"
            keepMounted
            anchorEl={userMenuEl}
            open={Boolean(userMenuEl)}
            onClose={handleUserMenuClose}
          >
            <MenuItem onClick={showUserProfile}>{t("Profil")}</MenuItem>
            <MenuItem onClick={logout}>{t("Ausloggen")}</MenuItem>
          </Menu>
        </NavItem>
      </FooterNav>

      {showProfileWindow && (
        <Window onClose={() => setShowProfileWindow(false)} headline={t("Profil bearbeiten")}>
          {(props) => <UserProfile {...props} />}
        </Window>
      )}
    </Container>
  );
};

export default Nav;
