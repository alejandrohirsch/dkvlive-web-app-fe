import styled from "styled-components/macro";

const Page = styled.section`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  position: relative;
`;

export default Page;
