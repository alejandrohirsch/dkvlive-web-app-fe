export default {
  VERSION: "0.0.0",
  API_URL: process.env.REACT_APP_API_URL || "https://frontend.dev.dkv.live",
  WS_URL: process.env.REACT_APP_WS_URL || "wss://websocket.dev.dkv.live",

  HERE_API_KEY: process.env.REACT_APP_HERE_API_KEY || "jl37dGNvlKtEEXhqpdIXErD1fTriPB5-G4njtSFnwXM",

  SUPPORT_EMAIL: process.env.REACT_APP_SUPPORT_EMAIL || "info@dkv-euroservice.com",
};
