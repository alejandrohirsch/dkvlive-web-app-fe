const wdioConfig = require("./wdio.conf.js");

//----------------------------------------------------------------------------------------------------------------------
const baseLocalConfig = Object.assign({}, wdioConfig.config, {
  path: "/",

  maxInstances: 1,
});

//----------------------------------------------------------------------------------------------------------------------
const configs = {};

configs["chrome"] = Object.assign({}, baseLocalConfig, {
  capabilities: [wdioConfig.capabilities["chrome"]],
});

configs["firefox"] = Object.assign({}, baseLocalConfig, {
  capabilities: [wdioConfig.capabilities["firefox"]],
});

configs["edge"] = Object.assign({}, baseLocalConfig, {
  capabilities: [wdioConfig.capabilities["edge"]],
});

//----------------------------------------------------------------------------------------------------------------------
let browser = "chrome";
process.argv.forEach((arg) => {
  if (!arg.startsWith("--st-browser=")) {
    return;
  }

  browser = arg.replace("--st-browser=", "");
});

if (!configs[browser]) {
  throw new Error("Browser not supported. Use --st-browser= to set a browser. [chrome|firefox|edge]");
}

exports.config = configs[browser];

// chromedriver.exe --port=4444
// geckodriver.exe --port=4444
// MicrosoftWebDriver.exe --port=4444

// ./node_modules/.bin/wdio wdio_local.conf.js --st-browser=chrome --spec=main
// --st-browser: Browser to test [chrome|firefox|edge]
