#!/bin/bash
set -e

./node_modules/.bin/wdio wdio.conf.js --hostname=$7 --baseUrl=$2 --st-build-ref-name=$1 --spec=index || test_ok=$?

# upload screenshots
# tar -czvf test_$1.tar.gz screens
# . ./ftpupload.sh test_$1.tar.gz Portal/test_$1.tar.gz
# . ./sendmail.sh "$3" $4 $5 $6 Portal/test_$1.tar.gz

# check tests
if [[ "$test_ok" -ne "0" ]]; then
    exit 1
fi
