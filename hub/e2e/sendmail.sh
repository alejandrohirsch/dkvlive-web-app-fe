#!/bin/bash
# sendmail.sh ${CI_PROJECT_NAME} ${CI_BUILD_REF_NAME} ${CI_COMMIT_SHA} ${GITLAB_USER_EMAIL}

echo -e "Version: $2\nLink: ftp://ftp.styletronic.io/$5\n" | mail -s "$2 ($3) [$1]" -aFrom:PortalBuild\<noreply@styletronic.at\> fp@styletronic.com $4