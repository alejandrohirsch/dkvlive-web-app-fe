#!/bin/bash
USER="dsgit"
PASSWD="GITfile0Up!"

FILE=$1
NAME=$2

if [ ! -f "$1" ]; then
  exit 1
fi

echo "UPLOAD: $1 -> $2"

ftp -p -n ftp.styletronic.io <<SCRIPT
user $USER $PASSWD
binary
put $FILE $NAME
quit
SCRIPT
