/// <reference types="node"/>
/// <reference types="@wdio/sync"/>
/// <reference types="@wdio/mocha-framework"/>
/// <reference types="@types/chai"/>

declare var assert: Chai.AssertStatic;
declare var expect: Chai.ExpectStatic;
declare var should: Chai.Should;
