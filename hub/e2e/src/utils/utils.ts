export const isEdge = () => {
  return browser.capabilities.browserName === "MicrosoftEdge";
};

export const insertText = (el: WebdriverIO.Element, value: string) => {
  if (isEdge()) {
    browser.execute("arguments[0].select()", el);
    browser.execute(`document.execCommand("insertText", false, "${value}")`);
  } else {
    el.setValue(value);
  }
};

export const getValue = (el: WebdriverIO.Element) => {
  if (isEdge()) {
    return el.getAttribute("value");
  }

  return el.getValue();
};
