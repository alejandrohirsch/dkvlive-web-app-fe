import {existsSync, mkdirSync} from "fs";
import {dirname} from "path";

const pathname = dirname(__filename);

let screenshotNumber = 0;

export function saveScreenshot(name: string) {
  screenshotNumber++;

  const screenshotDir = `${pathname}/../../screens/${browser.capabilities.browserName}`;
  if (!existsSync(screenshotDir)) {
    mkdirSync(screenshotDir, {recursive: true});
  }

  browser.saveScreenshot(`${screenshotDir}/${screenshotNumber}_${name}.png`);
}
