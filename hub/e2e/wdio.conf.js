const path = require("path");
const pathname = path.dirname(__filename);

//----------------------------------------------------------------------------------------------------------------------

const windowSize = {w: 1920, h: 1315};

const capabilities = {
  chrome: {
    browserName: "chrome",
    maxInstances: 1,
    "goog:chromeOptions": {
      args: [
        "--incognito",
        "--no-sandbox",
        `--window-size=${windowSize.w},${windowSize.h}`,
        "--aggressive-cache-discard",
        "--disable-cache",
        "--disable-application-cache",
        "--disable-offline-load-stale-cache",
        "--disk-cache-size=0",
        "--no-sandbox",
        "--disable-setuid-sandbox",
      ],
    },
  },
  firefox: {
    browserName: "firefox",
    maxInstances: 1,
    "moz:firefoxOptions": {
      args: [`--height=${windowSize.h}`, `--width=${windowSize.w}`],
    },
  },
  edge: {
    browserName: "MicrosoftEdge",
    maxInstances: 1,
    platformName: "windows 10",
    "ms:inPrivate": true,
  },
};

exports.capabilities = capabilities;

//----------------------------------------------------------------------------------------------------------------------
exports.config = {
  runner: "local",

  baseUrl: "http://localhost:3000",

  hostname: "localhost",
  path: "/wd/hub",
  port: 4444,

  waitforTimeout: 30000,
  connectionRetryTimeout: 10000,
  connectionRetryCount: 3,

  specs: [`${pathname}/src/__tests__/*.ts`],
  exclude: [],
  bail: 1,

  maxInstances: 2,
  capabilities: [capabilities["chrome"], capabilities["firefox"]],

  services: ["firefox-profile"],
  firefoxProfile: {
    "browser.privatebrowsing.autostart": true,
    "browser.cache.disk.enable": false,
    "browser.cache.memory.enable": false,
    "browser.cache.offline.enable": false,
    "network.http.use-cache": false,
  },

  logLevel: "warn",
  reporters: ["spec", "dot"],

  framework: "mocha",
  mochaOpts: {
    ui: "bdd",
    timeout: 60000,
    require: ["tsconfig-paths/register"],
    bail: true,
  },

  before: function() {
    // add chai to global
    const chai = require("chai");
    global.expect = chai.expect;
    global.assert = chai.assert;
    global.should = chai.should();

    // setup ts
    require("ts-node").register({files: true});
  },
};

// ./node_modules/.bin/wdio wdio.conf.js --hostname=lx01.deepscale.io --baseUrl=https://p.styletronic.io --spec=map
