#!/bin/bash
cd /e2e

if [[ ! -e screens ]]; then
    mkdir screens
fi


./node_modules/.bin/wdio wdio.conf.js --hostname=$1 --baseUrl=$2 --spec=index
if [ $? -ne 0 ]; then
    exit 1
fi